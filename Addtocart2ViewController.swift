

//
//  Addtocart2ViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 30/06/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class Addtocart2ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource  {
    var uploadfile1=NSMutableDictionary()
    var jsonresult1=NSDictionary()
    var uploadfile2=NSMutableDictionary()
    var jsonresult2=NSDictionary()
    var uploadfile3=NSMutableDictionary()
    var jsonresult3=NSDictionary()
    var uploadfile4=NSMutableDictionary()
    var jsonresult4=NSDictionary()
    @IBOutlet weak var tableview: UITableView!
    var firstTimePressed : Bool = false
    var timecal = String()
    var uploadfile=NSMutableDictionary()
    var jsonresult=NSDictionary()
    var message = String()
    var arrayTotal: [Double] = []
    var total = Double()
    override func viewDidLoad() {
        super.viewDidLoad()
      tableview.register(UINib(nibName: "addtocartTableViewCell", bundle: nil), forCellReuseIdentifier: "addtocartTableViewCell")
        tableview.tableFooterView = UIView(frame: .zero)
        tableview.separatorStyle = .none
        self.customnavigation()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        appdelegate.shouldSupportAllOrientation = false
    }
    
    func customnavigation()
        
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x, y:  self.view.frame.origin.y, width: self.view.frame.width, height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(Addtocart2ViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Addto Cart"
        
    }
    
    func buttonClicked(sender:UIButton)
    {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "spcaddtocartViewController")), animated: true)
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        if LibraryAPI.sharedInstance.globalarray.count > 0
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
       
        return LibraryAPI.sharedInstance.globalarray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "addtocartTableViewCell", for: indexPath as IndexPath) as! addtocartTableViewCell
        cell.deletebtn.addTarget(self, action: #selector(self.deletebtnclicked), for: UIControlEvents.touchUpInside)
        cell.deletebtn.tag = indexPath.row
        cell.addtocartview.layer.cornerRadius = 5
        let dic = LibraryAPI.sharedInstance.globalarray[indexPath.row] as! NSDictionary
        cell.itemcodelbl.text = dic.object(forKey: "itemcode") as? String
        
        cell.descriptionlbl.text = dic.object(forKey: "ItemDescription_out") as? String
        cell.unitpricelbl.text = dic.object(forKey: "unitprice")as? String
        let quantity = dic.object(forKey: "quantity") as! String
       // let quantity = LibraryAPI.sharedInstance.globalarray.object(at: indexPath.row).
        let vendor = dic.object(forKey: "vendorbid") as! String
        
        cell.qualitylbl.text = String(quantity)
        cell.vendorbplbl.text = String(vendor)
        if(cell.qualitylbl.text == "" && cell.vendorbplbl.text == "")
        {
            cell.qualitylbl.text = ""
            cell.vendorbplbl.text = ""
        }
        else
        {
              total = (Double(quantity))! * (Double(vendor))!
        }
        
        cell.selectionStyle = .none
        cell.totallbl.text =  String(total)
        //timecal = ((LibraryAPI.sharedInstance.globalarray.objectAtIndex(indexPath.row) as AnyObject).objectForKey("timing") as? String)!
        timecal = (dic.object(forKey: "timing") as? String)!
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         tableView.deselectRow(at: indexPath as IndexPath, animated: true)
    }
    func deletebtnclicked(sender: UIButton)
    {
        let alertController = UIAlertController(title: "Alert!", message: "Are You Sure Want to Delete Add to cart?", preferredStyle: .alert)
        let DefaultAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
            LibraryAPI.sharedInstance.globalarray.removeObject(at: sender.tag)
            self.tableview.reloadData()
        }
        let CancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
        }
        alertController.addAction(DefaultAction)
        alertController.addAction(CancelAction)
        self.present(alertController, animated: true, completion:nil)
    }

    @IBAction func SubmitbtnAction(_ sender: AnyObject)
    {
        if(LibraryAPI.sharedInstance.globalarray.count>0)
        {
            uploadfile.setObject(LibraryAPI.sharedInstance.Userid, forKey: "UserId" as NSCopying)
            let dicarray = LibraryAPI.sharedInstance.globalarray[0] as! NSDictionary
            let priceapptype = (dicarray.object(forKey: "priceapp") as? String)
            
            let price : [String] = priceapptype!.components(separatedBy: " - ")
            let pricestr : String = price[0]
            uploadfile.setObject(pricestr, forKey: "Type" as NSCopying)
            uploaddata()
            if(jsonresult.count > 0)
            {
                let errormessage = self.jsonresult.object(forKey: "ErrorMessage")
                 message      = (self.jsonresult.object(forKey: "SPCNumber")as? String)!
                
                LibraryAPI.sharedInstance.globaldic.setObject(message, forKey:"spcnumber" as NSCopying)
                
            }
            
            var value: Int = 1
            for dicword in LibraryAPI.sharedInstance.globalarray
            {
                //get the value from global array
               // let User = word.object("usernames") as? String
                let word = dicword as! NSDictionary
                let Discountnumber1 = word.object(forKey: "spcnumber")as? String
                let Discounttype1 = word.object(forKey: "priceapp")as? String
                let Quantity1 =  word.object(forKey: "Quantity")as? String
                let endcustname1 = word.object(forKey: "endcustomername")as? String
                let Customernumber1 = word.object(forKey: "customercode")as? String
                let Item1 = word.object(forKey: "itemcode")as? String
                let Unitprice1 = word.object(forKey: "unitprice")as? String
                let Multipleinvoice1 = word.object(forKey: "multi")as? String
                let Enddate1 = word.object(forKey: "validity")as? String
            
                let Vendorreference1 = word.object(forKey: "vendor")as? String
                let Vendorbidprice1 = word.object(forKey: "vendorbid")as? String
                let Orcamount1 = word.object(forKey: "orcamt")as? String
                let Cashdiscount1 = word.object(forKey: "cashdiscount")as? String
                let Frieghtprepaid1 = word.object(forKey: "frieght")as? String
                let PaymentDays1 = word.object(forKey: "payment")as? String
                let BizCode1 = word.object(forKey: "businesscode")as? String
                //DISTIDISC_out1
                let DistiDisCount1 = word.object(forKey: "DISTIDISC_out1")as? String
                let User1 = word.object(forKey: "usernames") as? String
                
                let referencenumber1 = word.object(forKey: "reference")as? NSNumber
                let eopnumber1 = word.object(forKey: "eop")as? String
                //Extra datas
                let stockroom = word.object(forKey: "stockroom")as? String
                //endcustomer(Subsreen 1)
                let address1 = word.object(forKey: "address1")as? String
                let address2 = word.object(forKey: "address2")as? String
                let city = word.object(forKey: "city")as? String
                let state = word.object(forKey: "state")as? String
                let pincode = word.object(forKey: "pincode")as? String
                let vertical = word.object(forKey: "vertical")as? String
                
                //endCustomer2(Subscreen2)
                let address21 = word.object(forKey: "address21")as? String
                let address22 = word.object(forKey: "address22")as? String
                let address23 = word.object(forKey: "address23")as? String
                let citymode = word.object(forKey: "citymode2")as? String
                let citymode2 = word.object(forKey: "city2")as? String
                let state2 = word.object(forKey: "state2")as? String
                let pincode2 = word.object(forKey: "pincode2")as? String
                let contactname2 = word.object(forKey: "contactname2")as? String
                let telephone2 = word.object(forKey: "telephone2")as? String
                let email2 = word.object(forKey: "email2")as? String
                let skutype2 = word.object(forKey: "skutype2")as? String
                let ordertype2 = word.object(forKey: "ordertype2")as? String
                let industry2 = word.object(forKey: "industry2")as? String
                
                //spcsecondscreen
                let additionalamt = word.object(forKey: "additionalamt")as? String
                
                
             /*
                if(User! = nil)
                {
                    uploadfile.setObject(User!.stringByRemovingWhitespaces, forKey: "User")
                }
                else
                {
                    uploadfile.setObject(User!, forKey: "")
                }
                
                if(Discountnumber1 != nil)
                {
                    uploadfile.setObject(Discountnumber1!.stringByRemovingWhitespaces, forKey: "Discountnumber")
                }
                else
                {
                     uploadfile.setObject("", forKey: "Discountnumber1")
                }
                */
                
                
                if(Discounttype1 != nil)
                {
                    
                    
                    let dcounttype : [String] = Discounttype1!.components(separatedBy: " - ")
                    let dcounttypestr : String = dcounttype[0]
                    uploadfile.setObject(dcounttypestr.stringByRemovingWhitespaces, forKey: "Discounttype" as NSCopying)
                }
                else
                {
                     uploadfile.setObject("", forKey: "Discounttype" as NSCopying)
                }
                
                
                if(Quantity1 != nil)
                {
                    uploadfile.setObject(Quantity1!.stringByRemovingWhitespaces, forKey: "Quantity" as NSCopying)
                    
                }
                 else
                {
                    uploadfile.setObject("", forKey: "Quantity" as NSCopying)
                }
                
                
                
                
                
                if(Quantity1 != nil)
                {
                    uploadfile.setObject(Quantity1!.stringByRemovingWhitespaces, forKey: "Quantityremaining" as NSCopying)
                    
                }
                else
                {
                    uploadfile.setObject("", forKey: "Quantityremaining" as NSCopying)
                }
                
                if(endcustname1 != nil)
                {
                    uploadfile.setObject(endcustname1!.stringByRemovingWhitespaces, forKey: "Endcustomername" as NSCopying)
                }
                else
                {
                    uploadfile.setObject("", forKey: "Endcustomername" as NSCopying)
                }
                
                
                if(Customernumber1 != nil)
                {
                    uploadfile.setObject(Customernumber1!.stringByRemovingWhitespaces, forKey: "Customernumber" as NSCopying)
                }
                else
                {
                    uploadfile.setObject("", forKey: "Customernumber" as NSCopying)
                }
               
               
                
                if(Item1 != nil)
                {
                    uploadfile.setObject(Item1!.stringByRemovingWhitespaces, forKey: "Item" as NSCopying)
                }
                else
                {
                     uploadfile.setObject("", forKey: "Item" as NSCopying)
                }
                if(Multipleinvoice1 != nil)
                {
                    uploadfile.setObject(Multipleinvoice1!.stringByRemovingWhitespaces, forKey: "Multipleinvoice" as NSCopying)
                }
                else
                
                {
                    uploadfile.setObject("", forKey: "Multipleinvoice" as NSCopying)
                }
                
                if(Enddate1 != nil)
                {
                    uploadfile.setObject(Enddate1!, forKey: "Enddate" as NSCopying)
                }
                
                
                if(Vendorreference1 != nil)
                {
                   uploadfile.setObject(Vendorreference1!.stringByRemovingWhitespaces, forKey: "Vendorreference" as NSCopying)
                }
                else
                {
                    uploadfile.setObject("", forKey: "Vendorreference" as NSCopying)
                    
                }
                
                
                
                if(Vendorbidprice1 != nil)
                {
                    uploadfile.setObject(Vendorbidprice1!.stringByRemovingWhitespaces, forKey: "Vendorbidprice" as NSCopying)
                }
                else
                {
                  uploadfile.setObject("", forKey: "Vendorbidprice" as NSCopying)
                }
                
                if(Orcamount1 != nil)
                {
                    uploadfile.setObject(Orcamount1!.stringByRemovingWhitespaces, forKey: "Orcamount" as NSCopying)
                }
                else
                {
                    uploadfile.setObject("", forKey: "Orcamount" as NSCopying)
                }
                
                if(Cashdiscount1 != nil)
                {
                    uploadfile.setObject(Cashdiscount1!.stringByRemovingWhitespaces, forKey: "Cashdiscount" as NSCopying)
                }
                else
                {
                   uploadfile.setObject("", forKey: "Cashdiscount" as NSCopying)
                }
                
                if(Frieghtprepaid1 != nil)
                {
                  uploadfile.setObject(Frieghtprepaid1!.stringByRemovingWhitespaces, forKey: "Frieghtprepaid" as NSCopying)
                }
                else
                {
                    uploadfile.setObject("", forKey: "Frieghtprepaid" as NSCopying)
                }
                
               
                if(User1 != nil)
                {
                    uploadfile.setObject(User1!.stringByRemovingWhitespaces, forKey: "User1" as NSCopying)
                }
                else
                {
                    uploadfile.setObject("", forKey: "User1" as NSCopying)
                }
                uploadfile.setObject("N", forKey: "Authorized" as NSCopying)
                 uploadfile.setObject("", forKey: "ACtiveflag" as NSCopying)
                 uploadfile.setObject("0", forKey: "Vendorbaseprice" as NSCopying)
                 uploadfile.setObject("000", forKey: "Deliverysequence" as NSCopying)
                 uploadfile.setObject("0", forKey: "Quantityachieved" as NSCopying)
                 uploadfile.setObject("0", forKey: "Quantityallocated" as NSCopying)
                 uploadfile.setObject("A", forKey: "Statusflag" as NSCopying)
                 uploadfile.setObject("", forKey: "Frombranch" as NSCopying)
                 uploadfile.setObject("", forKey: "Branchdescriptio" as NSCopying)
                 uploadfile.setObject("", forKey: "Unitmarginadjus" as NSCopying)
                 uploadfile.setObject("", forKey: "Reasoncode" as NSCopying)
                 uploadfile.setObject("0", forKey: "Backend" as NSCopying)
                 uploadfile.setObject("0", forKey: "Gross" as NSCopying)
                 uploadfile.setObject("", forKey: "Dummy1" as NSCopying)
                 uploadfile.setObject("", forKey: "Dummy2" as NSCopying)
                 uploadfile.setObject("", forKey: "Dummy3" as NSCopying)
                 uploadfile.setObject("0", forKey: "Dummy4" as NSCopying)
                 uploadfile.setObject("0", forKey: "Dummy5" as NSCopying)
                
                if(PaymentDays1 != nil)
                {
                  uploadfile.setObject(PaymentDays1!.stringByRemovingWhitespaces, forKey: "PaymentDays" as NSCopying)
                }
                else
                {
                    uploadfile.setObject("", forKey: "PaymentDays" as NSCopying)
                }
                
                if(BizCode1 != nil)
                {
                    let bcode : [String] = BizCode1!.components(separatedBy: " - ")
                    let bcodestr : String = bcode[0]
                    uploadfile.setObject(bcodestr, forKey: "BizCode" as NSCopying)
                }
                else
                {
                    uploadfile.setObject("'", forKey: "BizCode" as NSCopying)
                }
                
                if(DistiDisCount1 != nil)
                {
                    uploadfile.setObject(DistiDisCount1!.stringByRemovingWhitespaces, forKey: "DistiDisCount" as NSCopying)
                }
                else
                {
                    uploadfile.setObject("", forKey: "DistiDisCount" as NSCopying)
                }
                if(Unitprice1 != nil)
                {
                   uploadfile.setObject(Unitprice1!.stringByRemovingWhitespaces, forKey: "Unitprice" as NSCopying)
                }
                else
                {
                   uploadfile.setObject("", forKey: "Unitprice" as NSCopying)
                }
                
                uploadfile.setObject(value, forKey: "Linenumber" as NSCopying)
                uploadfile.setObject(LibraryAPI.sharedInstance.Global_PosValue1, forKey: "ApplePOSID" as NSCopying)
                
                let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)Users.asmx/SPCDetail")! as URL)
                request.httpMethod = "POST"
                request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
                request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
           
                request.httpBody = try? JSONSerialization.data(withJSONObject: self.uploadfile, options: JSONSerialization.WritingOptions(rawValue: 0))
                let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
                //  var err: NSError
                do
                {
                    let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
                    jsonresult = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                    
                   if(LibraryAPI.sharedInstance.globaldic.object(forKey: "flag1spc")as? String == "Y")
                   {
                      uploadfile1.setObject(value, forKey: "LINENUMBER" as NSCopying)
                      uploadfile1.setObject("A", forKey: "STATUSFLAG" as NSCopying)
                    
                 /*   if(User != nil)
                    {
                        uploadfile1.setObject(User!.stringByRemovingWhitespaces, forKey: "USER")
                    }
                    else
                    {
                        uploadfile1.setObject(User!, forKey: "")
                    }*/
                    
                    if(Discountnumber1 != nil)
                    {
                        uploadfile1.setObject(Discountnumber1!.stringByRemovingWhitespaces, forKey: "DISCOUNTNUMBER" as NSCopying)
                    }
                    else
                    {
                        uploadfile1.setObject(Discountnumber1!, forKey: "" as NSCopying)
                    }
                    
                    if(Discounttype1 != nil)
                    {
                        
                        
                        let dcounttype : [String] = Discounttype1!.components(separatedBy: " - ")
                        let dcounttypestr : String = dcounttype[0]
                        uploadfile1.setObject(dcounttypestr.stringByRemovingWhitespaces, forKey: "DISCOUNTTYPE" as NSCopying)
                    }
                    else
                    {
                        uploadfile1.setObject(Discounttype1!, forKey: ""  as NSCopying)
                    }
                    
                    
                    if(Customernumber1 != nil)
                    {
                        uploadfile1.setObject(Customernumber1!.stringByRemovingWhitespaces, forKey: "CUSTOMERCODE"  as NSCopying)
                    }
                    else
                    {
                        uploadfile1.setObject("", forKey: "Customernumber" as NSCopying)
                    }
                    
                    if(BizCode1 != nil)
                    {
                        let bcode : [String] = BizCode1!.components(separatedBy: " - ")
                        let bcodestr : String = bcode[0]
                        uploadfile1.setObject(bcodestr, forKey: "BUSINESSCODE" as NSCopying)
                    }
                    else
                    {
                        uploadfile1.setObject("'", forKey: "BUSINESSCODE" as NSCopying)
                    }
                    
                    
                    if(Item1 != nil)
                    {
                        uploadfile1.setObject(Item1!.stringByRemovingWhitespaces, forKey: "ITEM" as NSCopying)
                    }
                    else
                    {
                        uploadfile.setObject("", forKey: "ITEM" as NSCopying)
                    }
                    
                    if(Unitprice1 != nil)
                    {
                        uploadfile1.setObject(Unitprice1!.stringByRemovingWhitespaces, forKey: "UNITPRICE" as NSCopying)
                    }
                    else
                    {
                        uploadfile1.setObject("", forKey: "UNITPRICE" as NSCopying)
                    }
                    
                    if(additionalamt != nil)
                    {
                        uploadfile1.setObject(Unitprice1!.stringByRemovingWhitespaces, forKey: "ADDITIONALAMOUNT" as NSCopying)
                    }
                    else
                    {
                        uploadfile1.setObject("", forKey: "ADDITIONALAMOUNT" as NSCopying)
                    }

                    if(User1 != nil)
                    {
                        uploadfile1.setObject(User1!.stringByRemovingWhitespaces, forKey: "USER1" as NSCopying)
                    }
                    else
                    {
                        uploadfile1.setObject("", forKey: "USER1" as NSCopying)
                    }
                   

                     uploadfile1.setObject("0", forKey: "Filler1" as NSCopying)
                     uploadfile1.setObject("0", forKey: "Filler2" as NSCopying)
                     uploadfile1.setObject("0", forKey: "Filler3" as NSCopying)
                     uploadfile1.setObject("", forKey: "Filler4" as NSCopying)
                     uploadfile1.setObject("", forKey: "Filler5" as NSCopying)
                     uploadfile1.setObject("", forKey: "Filler6" as NSCopying)
                     uploadfile1.setObject("", forKey: "Filler7" as NSCopying)
                     uploadfile1.setObject("", forKey: "Filler8" as NSCopying)
                     uploadfile1.setObject("", forKey: "Filler9" as NSCopying)
                     uploadfile1.setObject("", forKey: "Filler10" as NSCopying)
                     uploadfile1.setObject("", forKey: "Filler11" as NSCopying)
                     uploadfile1.setObject("", forKey: "Filler12" as NSCopying)
                     uploadfile1.setObject("", forKey: "Filler13" as NSCopying)
                     uploadfile1.setObject("", forKey: "Filler14" as NSCopying)
                     uploadfile1.setObject("", forKey: "Filler15" as NSCopying)
                     uploadfile1.setObject("", forKey: "Filler16" as NSCopying)
                     uploadfile1.setObject("", forKey: "Filler17" as NSCopying)
                     uploadfile1.setObject("", forKey: "Filler18" as NSCopying)
                     uploadfile1.setObject("", forKey: "Filler19" as NSCopying)
                     uploadfile1.setObject("", forKey: "Filler20" as NSCopying)
                     uploadfile1.setObject("", forKey: "Filler21" as NSCopying)
                     uploadfile1.setObject("", forKey: "Filler22" as NSCopying)
                     uploadfile1.setObject("", forKey: "Filler23" as NSCopying)
                     uploadfile1.setObject("", forKey: "Filler24" as NSCopying)
                     uploadfile1.setObject("", forKey: "Filler25" as NSCopying)
                     uploadfile1.setObject("", forKey: "REMARKS1" as NSCopying)
                     uploadfile1.setObject("", forKey: "REMARKS2" as NSCopying)
                     uploadfile1.setObject("0", forKey: "DISTRIBUTORDISCOUNT" as NSCopying)
                     uploadfile1.setObject("", forKey: "PRIMARYBIZMNCODE" as NSCopying)
                     flaguploaddata()
                   }
                     if(LibraryAPI.sharedInstance.globaldic.object(forKey: "flag2spc")as? String == "Y")
                   {
                    
                    if(eopnumber1 != nil)
                    {
                        uploadfile2.setObject(eopnumber1!, forKey: "ApprovalCode" as NSCopying)
                    }
                    else
                    {
                        uploadfile2.setObject("", forKey: "ApprovalCode" as NSCopying)
                    }

                    
                    if(referencenumber1 != nil)
                    {
                        uploadfile2.setObject(referencenumber1!, forKey: "ReferenceNo" as NSCopying)
                    }
                    else
                    {
                        uploadfile2.setObject("", forKey: "ReferenceNo" as NSCopying)
                    }
                    if(Customernumber1 != nil)
                    {
                        uploadfile2.setObject(Customernumber1!.stringByRemovingWhitespaces, forKey: "Dealer" as NSCopying)
                    }
                    else
                    {
                        uploadfile2.setObject("", forKey: "Dealer" as NSCopying)
                    }
                    
                    if(endcustname1 != nil)
                    {
                        uploadfile2.setObject(endcustname1!.stringByRemovingWhitespaces, forKey: "EndUserName" as NSCopying)
                    }
                    else
                    {
                        uploadfile2.setObject("", forKey: "EndUserName" as NSCopying)
                    }

                    if(Item1 != nil)
                    {
                        uploadfile2.setObject(Item1!.stringByRemovingWhitespaces, forKey: "ItemCode" as NSCopying)
                    }
                    else
                    {
                        uploadfile2.setObject("", forKey: "ItemCode" as NSCopying)
                    }
                    
                    if(Quantity1 != nil)
                    {
                        uploadfile2.setObject(Quantity1!.stringByRemovingWhitespaces, forKey: "Quantity" as NSCopying)
                        
                    }
                    else
                    {
                        uploadfile2.setObject("", forKey: "Quantity" as NSCopying)
                    }
                    
                    if(Unitprice1 != nil)
                    {
                        uploadfile2.setObject(Unitprice1!.stringByRemovingWhitespaces, forKey: "SplPrice" as NSCopying)
                    }
                    else
                    {
                        uploadfile2.setObject("", forKey: "SplPrice" as NSCopying)
                    }
                    
                    
                    
                    if(Discountnumber1 != nil)
                    {
                        uploadfile2.setObject(Discountnumber1!.stringByRemovingWhitespaces, forKey: "SPCNUMBER" as NSCopying)
                    }
                    else
                    {
                        uploadfile2.setObject("", forKey: "SPCNUMBER" as NSCopying)
                    }
                     uploadfile2.setObject(value, forKey: "LINENUMBER" as NSCopying)
                     uploadfile2.setObject("A", forKey: "STATUS" as NSCopying)
                     uploadfile2.setObject("", forKey: "Fillerfield1" as NSCopying)
                     uploadfile2.setObject("", forKey: "Fillerfield2" as NSCopying)
                     uploadfile2.setObject("", forKey: "Fillerfield3" as NSCopying)
                     uploadfile2.setObject("", forKey: "Fillerfield4" as NSCopying)
                     uploadfile2.setObject("", forKey: "Fillerfield5" as NSCopying)
                     uploadfile2.setObject("", forKey: "Fillerfield6" as NSCopying)
                     uploadfile2.setObject("", forKey: "Fillerfield7" as NSCopying)
                     uploadfile2.setObject("", forKey: "Fillerfield8" as NSCopying)
                     uploadfile2.setObject("", forKey: "Fillerfield9" as NSCopying)
                    
                    if(User1 != nil)
                    {
                        uploadfile2.setObject(User1!.stringByRemovingWhitespaces, forKey: "UserId" as NSCopying)
                    }
                    else
                    {
                        uploadfile2.setObject("", forKey: "UserId" as NSCopying)
                    }
                    
                    if(BizCode1 != nil)
                    {
                        let bcode : [String] = BizCode1!.components(separatedBy: " - ")
                        let bcodestr : String = bcode[0]
                        uploadfile2.setObject(bcodestr, forKey: "Businesscode" as NSCopying)
                    }
                    else
                    {
                        uploadfile2.setObject("'", forKey: "Businesscode" as NSCopying)
                    }
                    flag2uploaddata()
                   }
                    
                }
                catch (let e)
                {
                    
                }

                
                value = value+1
                let bcode : [String] = (word.object(forKey: "businesscode")! as AnyObject).components(separatedBy: " - ")
                let bcodestr : String = bcode[0]
                
                if((bcodestr == "303") || (bcodestr == "461") )
                {
                    
                    if(User1 != nil)
                    {
                        uploadfile3.setObject(User1!.stringByRemovingWhitespaces, forKey: "USER" as NSCopying)
                    }
                    else
                    {
                        uploadfile3.setObject("", forKey: "USER" as NSCopying)
                    }
                    
                    if(Discounttype1 != nil)
                    {
                        
                        
                        let dcounttype : [String] = Discounttype1!.components(separatedBy: " - ")
                        let dcounttypestr : String = dcounttype[0]
                        uploadfile3.setObject(dcounttypestr.stringByRemovingWhitespaces, forKey: "DISCOUNTTYPE" as NSCopying)
                    }
                    else
                    {
                        uploadfile3.setObject("", forKey: "DISCOUNTTYPE" as NSCopying)
                    }
                    
                    if(Discountnumber1 != nil)
                    {
                        uploadfile3.setObject(Discountnumber1!.stringByRemovingWhitespaces, forKey: "DISCOUNTNUMBER" as NSCopying)
                    }
                    else
                    {
                        uploadfile3.setObject("", forKey: "DISCOUNTNUMBER" as NSCopying)
                    }
                    
                    if(Customernumber1 != nil)
                    {
                        uploadfile3.setObject(Customernumber1!.stringByRemovingWhitespaces, forKey: "CUSTOMERCODE"  as NSCopying)
                    }
                    else
                    {
                        uploadfile3.setObject("", forKey: "CUSTOMERCODE" as NSCopying)
                    }
                    
                    uploadfile3.setObject("", forKey: "BRANCHCODE" as NSCopying)
                    
                    
                    if(endcustname1 != nil)
                    {
                        uploadfile3.setObject(endcustname1!.stringByRemovingWhitespaces, forKey: "ENDCUSTNAME" as NSCopying)
                    }
                    else
                    {
                        uploadfile3.setObject("", forKey: "ENDCUSTNAME" as NSCopying)
                    }
                    if(address21 != nil)
                    {
                        uploadfile3.setObject(address21!.stringByRemovingWhitespaces, forKey: "ADDRESS1" as NSCopying)
                    }
                    else
                    {
                         uploadfile3.setObject("", forKey: "ADDRESS1" as NSCopying)
                    }
                    
                    if(address22 != nil)
                    {
                        uploadfile3.setObject(address22!.stringByRemovingWhitespaces, forKey: "ADDRESS2" as NSCopying)
                    }
                    else
                    {
                        uploadfile3.setObject("", forKey: "ADDRESS2" as NSCopying)
                    }
                    
                    if(address23 != nil)
                    {
                        uploadfile3.setObject(address23!.stringByRemovingWhitespaces, forKey: "ADDRESS3" as NSCopying)
                    }
                    else
                    {
                        uploadfile3.setObject("", forKey: "ADDRESS3" as NSCopying)
                    }
                    
                    if(citymode != nil)
                    {
                        uploadfile3.setObject(citymode!, forKey: "CITY" as NSCopying)
                    }
                    else
                    {
                        uploadfile3.setObject("", forKey: "CITY" as NSCopying)
                    }
                    
                    if(pincode2 != nil)
                    {
                        uploadfile3.setObject(pincode2!, forKey: "PINCODE" as NSCopying)
                    }
                    else
                    {
                        uploadfile3.setObject("", forKey: "PINCODE" as NSCopying)
                    }
                    
                    if(contactname2 != nil)
                    {
                        uploadfile3.setObject(contactname2!.stringByRemovingWhitespaces, forKey: "CONTACTNAME" as NSCopying)
                    }
                    else
                    {
                        uploadfile3.setObject("", forKey: "CONTACTNAME" as NSCopying)
                    }
                    
                    
                    if(telephone2 != nil)
                    {
                        uploadfile3.setObject(telephone2!, forKey: "TELEPHONENO" as NSCopying)
                    }
                    else
                    {
                        uploadfile3.setObject("", forKey: "TELEPHONENO" as NSCopying)
                    }

                    //EMAILID
                    
                    if(email2 != nil)
                    {
                        uploadfile3.setObject(email2!, forKey: "EMAILID" as NSCopying)
                    }
                    else
                    {
                        uploadfile3.setObject("", forKey: "EMAILID" as NSCopying)
                    }
                    
                    if(skutype2 != nil)
                    {
                        let skutype : [String] = skutype2!.components(separatedBy: " - ")
                        let skutypestr : String = skutype[0]
                        
                        uploadfile3.setObject(skutypestr, forKey: "SKUTYPE" as NSCopying)
                    }
                    else
                    {
                        uploadfile3.setObject("", forKey: "SKUTYPE" as NSCopying)
                    }
                    
                    if(ordertype2 != nil)
                    {
                        let ordertype : [String] = ordertype2!.components(separatedBy: " - ")
                        let ordertypestr : String = bcode[0]
                        uploadfile3.setObject(ordertypestr, forKey: "ORDERTYPE" as NSCopying)
                    }
                    else
                    {
                        uploadfile3.setObject("", forKey: "ORDERTYPE" as NSCopying)
                    }
                    
                    if(industry2 != nil)
                    {
                        
                        uploadfile3.setObject(industry2!.stringByRemovingWhitespaces, forKey: "INDUSTRYSEGMENT" as NSCopying)
                    }
                    else
                    {
                        uploadfile3.setObject("", forKey: "INDUSTRYSEGMENT" as NSCopying)
                    }
                    
                    
                    uploadfile3.setObject("", forKey: "BRANCHCODE" as NSCopying)
                    uploadfile3.setObject("", forKey: "STATUS" as NSCopying)
                    uploadfile3.setObject("", forKey: "ACTIVEFLAG" as NSCopying)
                    uploadfile3.setObject("", forKey: "Filler01" as NSCopying)
                    uploadfile3.setObject("", forKey: "Filler02" as NSCopying)
                    uploadfile3.setObject("", forKey: "Filler03" as NSCopying)
                    uploadfile3.setObject("", forKey: "Filler04" as NSCopying)
                    uploadfile3.setObject("", forKey: "Filler05" as NSCopying)
                    uploadfile3.setObject("", forKey: "Filler06" as NSCopying)
                    uploadfile3.setObject("", forKey: "Filler07" as NSCopying)
                    uploadfile3.setObject("", forKey: "Filler08" as NSCopying)
                    uploadfile3.setObject("", forKey: "Filler09" as NSCopying)
                    uploadfile3.setObject("", forKey: "Filler10" as NSCopying)
                    uploadfile3.setObject("", forKey: "Filler11" as NSCopying)
                    uploadfile3.setObject("", forKey: "Filler12" as NSCopying)
                    uploadfile3.setObject("", forKey: "Filler13" as NSCopying)
                    uploadfile3.setObject("", forKey: "Filler14" as NSCopying)
                    uploadfile3.setObject("", forKey: "Filler15" as NSCopying)
                    uploadfile3.setObject("", forKey: "Filler16" as NSCopying)
                    uploadfile3.setObject("", forKey: "Filler17" as NSCopying)
                    uploadfile3.setObject("", forKey: "Filler18" as NSCopying)
                    uploadfile3.setObject("", forKey: "Filler19" as NSCopying)
                    uploadfile3.setObject("", forKey:  "Filler20" as NSCopying)
                    endcustomerupdation()
                    
                    let errormessage = self.jsonresult.object(forKey: "ErrorMessage")
                    let submit      = self.jsonresult.object(forKey: "SUBMIT")
                    
                    
                     if (errormessage as? String) != ""
                    {
                       self.showToast(message: errormessage as! String)
                    }
                   else
                     {
                        
                     }
                }
                
              if((bcodestr == "012") || (bcodestr == "155") || (bcodestr == "045") )
               {
                
                uploadfile4.setObject("RT", forKey: "COMPANYCODE" as NSCopying)
                if(Vendorreference1 != nil)
                {
                    uploadfile4.setObject(Vendorreference1!.stringByRemovingWhitespaces, forKey: "DEALID" as NSCopying)
                }
                else
                {
                    uploadfile4.setObject("", forKey: "DEALID" as NSCopying)
                    
                }
                
                
                if(endcustname1 != nil)
                {
                    uploadfile4.setObject(endcustname1!.stringByRemovingWhitespaces, forKey: "ENDCUSTOMERNAME" as NSCopying)
                }
                else
                {
                    uploadfile4.setObject("", forKey: "ENDCUSTOMERNAME" as NSCopying)
                }
                //address1
                
                if(address1 != nil)
                {
                    uploadfile4.setObject(address1!.stringByRemovingWhitespaces, forKey: "ADDRESS1" as NSCopying)
                }
                else
                {
                    uploadfile4.setObject("", forKey: "ADDRESS1" as NSCopying)
                }
                
                if(address2 != nil)
                {
                    uploadfile4.setObject(address2!.stringByRemovingWhitespaces, forKey: "ADDRESS2" as NSCopying)
                }
                else
                {
                    uploadfile4.setObject("", forKey: "ADDRESS2" as NSCopying)
                }

                //city
                if(city != nil)
                {
                    uploadfile4.setObject(city!.stringByRemovingWhitespaces, forKey: "CITY" as NSCopying)
                }
                else
                {
                    uploadfile4.setObject("", forKey: "CITY" as NSCopying)
                }
                
                if(state != nil)
                {
                    uploadfile4.setObject(state!.stringByRemovingWhitespaces, forKey: "STATE" as NSCopying)
                }
                else
                {
                    uploadfile4.setObject("", forKey: "STATE" as NSCopying)
                }
                
                if(pincode != nil)
                {
                    uploadfile4.setObject(pincode!.stringByRemovingWhitespaces, forKey: "PINCODE" as NSCopying)
                }
                else
                {
                    uploadfile4.setObject("", forKey: "PINCODE" as NSCopying)
                }
              //SPCNUMBER
                
                if(Discountnumber1 != nil)
                {
                    uploadfile4.setObject(Discountnumber1!.stringByRemovingWhitespaces, forKey: "SPCNUMBER" as NSCopying)
                }
                else
                {
                    uploadfile4.setObject("", forKey: "SPCNUMBER" as NSCopying)
                }
                
                if(vertical != nil)
                {
                    let vertical : [String] = vertical!.components(separatedBy: " - ")
                    let verticalstr : String = vertical[1]
                    uploadfile4.setObject(verticalstr.stringByRemovingWhitespaces, forKey: "FILLERFIELD1" as NSCopying)
                }
                else
                {
                    uploadfile4.setObject("", forKey: "FILLERFIELD1" as NSCopying)
                }
                
                  uploadfile4.setObject("", forKey: "FILLERFIELD2" as NSCopying)
                  uploadfile4.setObject("", forKey: "FILLERFIELD3" as NSCopying)
                  uploadfile4.setObject("", forKey: "FILLERFIELD4" as NSCopying)
                  uploadfile4.setObject("", forKey: "FILLERFIELD5" as NSCopying)
                  uploadfile4.setObject("", forKey: "FILLERFIELD6" as NSCopying)
                if(BizCode1 != nil)
                {
                    let bcode : [String] = BizCode1!.components(separatedBy: " - ")
                    let bcodestr : String = bcode[0]
                    uploadfile4.setObject(bcodestr, forKey: "BUSSINESSCODE" as NSCopying)
                }
                else
                {
                    uploadfile4.setObject("'", forKey: "BUSSINESSCODE" as NSCopying)
                }
                
                if(User1 != nil)
                {
                    uploadfile4.setObject(User1!.stringByRemovingWhitespaces, forKey: "USERNAME"  as NSCopying)
                }
                else
                {
                    uploadfile4.setObject("", forKey: "USERNAME" as NSCopying)
                }
                endcustomer1updation()

               }
                
                
            }
            
            let alertController = UIAlertController(title: "Your Order is Placed Successfully", message:message , preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                LibraryAPI.sharedInstance.globalarray.removeAllObjects()
                LibraryAPI.sharedInstance.globaldic.removeAllObjects()
                
                self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)
            
            }
        
    }
    func uploaddata()
    {
        // http://edi.redingtonb2b.in/Whatsup-staging/Users.asmx/SPCNumberGeneration
        let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)Users.asmx/SPCNumberGeneration")! as URL)
        request.httpMethod = "POST"
        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
        request.httpBody = try? JSONSerialization.data(withJSONObject: self.uploadfile, options: JSONSerialization.WritingOptions(rawValue: 0))
        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
        //  var err: NSError
        do
        {
            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
            jsonresult = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            
        }
        catch (let e)
        {
        print(e)
        }
        
    }
    
    
    func flaguploaddata()
    {
        // http://edi.redingtonb2b.in/Whatsup-staging/Users.asmx/SPC_ROE46P4_Updation
        let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)Users.asmx/SPC_ROE46P4_Updation")! as URL)
        request.httpMethod = "POST"
        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
        request.httpBody = try? JSONSerialization.data(withJSONObject: self.uploadfile1, options: JSONSerialization.WritingOptions(rawValue: 0))
        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
        //  var err: NSError
        do
        {
            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
            jsonresult1 = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            
            
            
        }
        catch (let e)
        {
           print(e)
        }
        
    }
    
    
    func flag2uploaddata()
    {
         //http://edi.redingtonb2b.in/Whatsup-staging/Users.asmx/UpdationROT352PE
        let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)Users.asmx/UpdationROT352PE")! as URL)
        request.httpMethod = "POST"
        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
        request.httpBody = try? JSONSerialization.data(withJSONObject: self.uploadfile2, options: JSONSerialization.WritingOptions(rawValue: 0))
        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
        //  var err: NSError
        do
        {
            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
            jsonresult2 = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            
            
        }
        catch (let e)
        {
            print(e)
        }
        
    }
    
    func endcustomerupdation()
    {
        //http://edi.redingtonb2b.in/Whatsup-staging/Users.asmx/ROE46P2_Updation
        let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)Users.asmx/ROE46P2_Updation")! as URL)
        request.httpMethod = "POST"
        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
        request.httpBody = try? JSONSerialization.data(withJSONObject: self.uploadfile3, options: JSONSerialization.WritingOptions(rawValue: 0))
        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
        //  var err: NSError
        do
        {
            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
            jsonresult3 = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            
        }
        catch (let e)
        {
            print(e)
        }
        
    }
    
    func endcustomer1updation()
    {
        let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)Users.asmx/UPDATIONROE411L1")! as URL)
        request.httpMethod = "POST"
        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
        request.httpBody = try? JSONSerialization.data(withJSONObject: self.uploadfile4, options: JSONSerialization.WritingOptions(rawValue: 0))
        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
        //  var err: NSError
        do
        {
            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
            jsonresult4 = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            
            
        }
        catch (let e)
        {
           print(e)
        }
        
    }


}
