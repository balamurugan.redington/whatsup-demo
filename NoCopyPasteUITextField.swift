//
//  NoCopyPasteUITextField.swift
//  UITextFieldDropDownList
//
//  Created by Lawrence F MacFadyen on 2016-08-06.
//  Copyright © 2016 LawrenceM. All rights reserved.
//

import UIKit

class NoCopyPasteUITextField: UITextField {

     func canPerformAction(action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(UIResponderStandardEditActions.paste) || action == #selector(UIResponderStandardEditActions.copy) || action == #selector(UIResponderStandardEditActions.select) || action == #selector(UIResponderStandardEditActions.selectAll){
            return false
        }
        
        return canPerformAction(action, withSender: sender)
        
       // return super.canPerformAction(action, withSender: sender)
    }
}
