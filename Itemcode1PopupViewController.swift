//
//  Itemcode1PopupViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 26/07/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
class Itemcode1PopupViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UISearchDisplayDelegate {
    
    @IBOutlet weak var searchbar: UISearchBar!
    @IBOutlet weak var itemtableview: UITableView!
    
    //var responsestring = NSMutableArray()
    var responsestring1 = NSMutableArray()
    var searchActive : Bool = false
    var ItemcodeArray : NSMutableArray = []
    var item : [String] = []
    var item1 : [String] = []
    var item2 : [String] = []
    var filteritem: [String] = []
    var deliveryPageindex:Int = 1
    var closeHandler: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Itemcode1PopupViewController")
        searchbar.delegate = self
        itemtableview.register(UINib(nibName: "itemcodeTableViewCell", bundle: nil), forCellReuseIdentifier: "itemcodeTableViewCell")
        itemtableview.tableFooterView = UIView(frame: .zero)
        let toolbar = UIToolbar.init()
        toolbar.sizeToFit()
        let barbutton = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(Itemcode1PopupViewController.donebuttonclicked))
        toolbar.items = [barbutton]
        searchbar.inputAccessoryView = toolbar
        customnavigation()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.deliveryPageindex = 1
        Itemcode_Arr = [ItemCode]()
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
            self.Loaddata(deliveryPageindex: self.deliveryPageindex)
            DispatchQueue.main.async {
             
                self.itemtableview.reloadData()
            }
        }
        
        // new version dispatch Que
       

       
    }
    override func viewDidAppear(_ animated: Bool) {
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    func donebuttonclicked(sender: AnyObject) {
        searchbar.resignFirstResponder()
    }
    
    
    func customnavigation() {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65)
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        customView.MenuBtn.addTarget(self, action: #selector(Itemcode1PopupViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "ItemCode"
        
    }
    
    func buttonClicked(sender:UIButton) {
        let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "StockTransferStatusViewController") as! StockTransferStatusViewController
        self.navigationController?.pushViewController(lvc, animated: false)
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if (Itemcode_Arr.count == 0) {
             closeHandler?()
            return 0
        } else {
            return 1
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 103
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Itemcode_Arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemcodeTableViewCell", for: indexPath as IndexPath) as! itemcodeTableViewCell
        cell.itemcodelbl.text = Itemcode_Arr[indexPath.row].Item_Code
        cell.vendorpartcodelbl.text = Itemcode_Arr[indexPath.row].Vendor_part_Code
        cell.itemdescriptionlbl.text = Itemcode_Arr[indexPath.row].Item_Description
        cell.cellview.layer.borderWidth = 1
        cell.cellview.layer.borderColor = CustomColor.ShareColor.hexStringToUIColor(hex: CustomColor.ShareColor.CustomGreen, alpha: 0.5).cgColor
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        LibraryAPI.sharedInstance.Stocktransferitemcode = Itemcode_Arr[indexPath.row].Item_Code
        navigationController!.popViewController(animated: true)
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastitem  = Itemcode_Arr.count - 1
        if indexPath.row == lastitem {
            deliveryPageindex = deliveryPageindex+1
            let lastpage = Int(Itemcode_Arr[indexPath.row].Total_Page)
            if(deliveryPageindex <= lastpage) {
               Loaddata(deliveryPageindex: deliveryPageindex)
            } else {
            }
        }
    }
  
    func Loaddata(deliveryPageindex: Int) {
        self.local_activityIndicator_start()
        var Stocktransferbcode = String(LibraryAPI.sharedInstance.Stocktransferbcode)!
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/itemCodeSearch_SPC?User=\(LibraryAPI.sharedInstance.Userid.stringByRemovingWhitespaces)&PGM=\(Stocktransferbcode)&Record=20&Page=\(deliveryPageindex)") { (jsonmutablearray) in
            let  ItemcodeArray = jsonmutablearray
            if ItemcodeArray.count > 0 {
                for word in ItemcodeArray {
                    Itemcode_Arr.append(ItemCode(data: word as AnyObject))
                }
                self.local_activityIndicator_stop()
                self.itemtableview.reloadData()
            } else {
                self.local_activityIndicator_stop()
                ItemcodeArray.removeAllObjects()
                let alertController = UIAlertController(title: "Alert", message: "No Data Available", preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                    self.itemtableview.isHidden = true
                    let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "StockTransferStatusViewController") as! StockTransferStatusViewController
                    self.navigationController?.pushViewController(lvc, animated: false)
                }
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion:nil)
            }
        }
    }
}






