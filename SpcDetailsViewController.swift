//
//  SpcDetailsViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 10/05/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import KCFloatingActionButton

class SpcDetailsViewController: UIViewController,KCFloatingActionButtonDelegate,UITableViewDelegate,UITableViewDataSource
{

    @IBOutlet weak var spctableview: UITableView!
    var  responsestring = NSMutableArray()
    
    @IBOutlet weak var titlelbl: UILabel!
    @IBOutlet weak var titlevaluelbl: UILabel!
    @IBOutlet weak var companylbl: UILabel!
     var fab=KCFloatingActionButton()
  
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationmethod), name: NSNotification.Name(rawValue: "GetNotification"), object: nil)

        self.customnavigation()
          self.layoutFAB()
      //  spctableview.separatorStyle = .SingleLine
        spctableview.tableFooterView = UIView(frame: .zero)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        appdelegate.shouldSupportAllOrientation = false
        self.spctableview.reloadData()
        titlelbl.text = LibraryAPI.sharedInstance.SPClblStr
        titlevaluelbl.text =  LibraryAPI.sharedInstance.SPCtitleStr
    }
    
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            
            self.fab.close()
        }
        fab.fabDelegate = self
        
        self.view.addSubview(fab)
        
    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
        
    }
    
    func customnavigation()
        
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y:  self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(SpcDetailsViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "SPC Status"
        
    }
    
    func buttonClicked(sender:UIButton)
    {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "SPCViewController")), animated: true)
    }
    
    
    //TableView Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
            if (LibraryAPI.sharedInstance.SPCStatus.count > 0)
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return LibraryAPI.sharedInstance.SPCStatus.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell : SpcTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as! SpcTableViewCell
        if(cell == nil)
        {
            cell = Bundle.main.loadNibNamed("Cell", owner: self, options: nil)![0] as! SpcTableViewCell;
            
        }
        if(indexPath.row%2==0)
        {
            cell.contentView.backgroundColor=(UIColor.init(red: 244/255.0, green: 242/255.0, blue: 242/255.0, alpha: 1.0))
        }
        else
        {
            cell.contentView.backgroundColor=UIColor.white
        }
        let dic = LibraryAPI.sharedInstance.SPCStatus[indexPath.row] as! NSDictionary
        cell.spcnolbl.text=String(dic.object(forKey: "SPC Number") as! String)
        cell.validitydate.text=String(dic.object(forKey: "Validity Date") as! String)
        cell.Totalspcvallbl.text=String(describing: dic.object(forKey: "Total SPC Value") as! NSNumber)
        cell.statuslbl.text=String(dic.object(forKey: "SPC Status") as! String)
        
        self.companylbl.text = String(dic.object(forKey: "Customer code") as! String) + " - " + String(dic.object(forKey: "Customer Name") as! String)
        
      
        if  cell.statuslbl.text == "Approved"
        {
           cell.statusview.backgroundColor = UIColor.init(red: 176/255.0, green: 238/255.0, blue: 179/255.0, alpha: 1.0)
        }
        else
        {
            cell.statusview.backgroundColor = UIColor.init(red: 255/255.0, green: 164/255.0, blue: 119/255.0, alpha: 1.0)
        }
        cell.selectionStyle = .none
        
        cell.cellview.layer.borderColor = UIColor(red:30/255.0, green:184/255.0, blue:120/255.0, alpha: 1.0).cgColor
        cell.cellview.layer.borderWidth = 1.0
        cell.cellview.layer.cornerRadius = 5.0

        
        return cell as SpcTableViewCell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
          return 109
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       
        
    }
    
    
    func notificationmethod()
    {
        let userdefaults = Foundation.UserDefaults.standard
        let value = userdefaults.dictionary(forKey: "Notification")
        
        var title: String!
        var message: String!
        var imagestr: String!
        title = value?["title"] as! String
        message = value?["body"] as! String
        imagestr = value?["imageurl"] as! String
        
        if imagestr != nil
        {
            if imagestr != ""
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: notificationimageViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = notificationimageViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                    
                }
                popup.show(childViewController: container)
            }
            else
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: PushnotificationtextViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = PushnotificationtextViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                }
                
                popup.show(childViewController: container)
            }
        }
        else
        {
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: PushnotificationtextViewController.instance())
                
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
            }
            let container = PushnotificationtextViewController.instance()
            container.closeHandler = { _ in
                popup.dismiss()
            }
            
            popup.show(childViewController: container)
        }
    
        
    }

    

}
