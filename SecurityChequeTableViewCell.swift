//
//  SecurityChequeTableViewCell.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 17/07/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class SecurityChequeTableViewCell: UITableViewCell {
    @IBOutlet weak var chequeno: UILabel!
    
    @IBOutlet weak var receiptdate: UILabel!
    
    @IBOutlet weak var timeofreceipt: UILabel!
    
    @IBOutlet weak var bankname: UILabel!
    
    @IBOutlet weak var chequedate: UILabel!
    
    @IBOutlet weak var chequeamount: UILabel!

    @IBOutlet weak var viewpage: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
