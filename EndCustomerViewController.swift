//
//  EndCustomerViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 13/06/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit
//import  NVActivityIndicatorView
class EndCustomerViewController: UIViewController,UITextFieldDelegate
{
    @IBOutlet weak var quotetf: UITextField!
    
    @IBOutlet weak var endcusnametf: UITextField!
    
    @IBOutlet weak var address1tf: UITextField!
    
    @IBOutlet weak var address2tf: UITextField!
    
    @IBOutlet weak var citytf: UITextField!
    
    @IBOutlet weak var statetf: UITextField!
    
    @IBOutlet weak var pincodetf: UITextField!
    
    @IBOutlet weak var vertical: UITextField!
    var quotenumber = String()
    var responsearray2=NSMutableArray()
    var pickerarray2=NSMutableArray()
    var Movedup = Bool()
    

    override func viewDidLoad() {
        super.viewDidLoad()
     LibraryAPI.sharedInstance.spcstr = "01"
        self.customnavigation()
        quotetf.delegate = self
        endcusnametf.delegate = self
        address1tf.delegate = self
        address2tf.delegate = self
        citytf.delegate = self
        statetf .delegate = self
        pincodetf.delegate = self
        vertical.delegate = self
        
        citytf.tag = 1
        vertical.tag = 2
        pincodetf.tag = 3
        endcusnametf.tag = 7
        address1tf.tag = 6
        address2tf.tag = 5
        statetf.tag = 4
        cityrec()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        quotetf.text = quotenumber
        Movedup = false
        NotificationCenter.default.addObserver(self, selector: #selector(EndCustomerViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(EndCustomerViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        pincodetf.keyboardType = .numberPad
        addDoneButtonOnKeyboard()

    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
     }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
//        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        appdelegate.shouldSupportAllOrientation = false
        
        vendor()
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(EndCustomerViewController.doneButtonAction))
        
        
        let items = NSMutableArray()
        
        items.add(flexSpace)
        
        items.add(done)
        
        doneToolbar.items = items as AnyObject as? [UIBarButtonItem]
        
        doneToolbar.sizeToFit()
        
         pincodetf.inputAccessoryView = doneToolbar
    }
    func doneButtonAction()
    {
       pincodetf.resignFirstResponder()
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    if(textField.tag == 7)
        {
           // if string.rangeOfCharacterFromSet(NSCharacterSet.letterCharacterSet()) != nil {
            if string.rangeOfCharacter(from: .letters) != nil {
                guard let text = textField.text else { return true }
                let newLength = text.count + string.count - range.length
                
                let aSet = NSCharacterSet(charactersIn:" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,").inverted
               // let compSepByCharInSet = string.componentsSeparatedByCharactersInSet(aSet)
                let compSepByCharInSet = string.components(separatedBy: aSet)
            //    let numberFiltered = compSepByCharInSet.joinWithSeparator("")
                 let numberFiltered = compSepByCharInSet.joined(separator: "")
                return (string == numberFiltered) && newLength <= 30
                
            }
            else
            {
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                 
                    return true
                }
                
                return false
                
            }
            
        }
        if(textField.tag == 6)
        {
            //if string.rangeOfCharacterFromSet(NSCharacterSet.letterCharacterSet()) != nil {
                 if string.rangeOfCharacter(from: .letters) != nil {
                guard let text = textField.text else { return true }
                let newLength = text.count + string.count - range.length
                
                let aSet = NSCharacterSet(charactersIn:" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,").inverted
                //let compSepByCharInSet = string.componentsSeparatedByCharactersInSet(aSet)
                //let numberFiltered = compSepByCharInSet.joinWithSeparator("")
                    let compSepByCharInSet = string.components(separatedBy: aSet)
                    let numberFiltered = compSepByCharInSet.joined(separator: "")
                return (string == numberFiltered) && newLength <= 35
                
            }
            else
            {
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                  
                    return true
                }
                
                return false
                
            }
            
        }
        
        if(textField.tag == 5)
        {
            //if string.rangeOfCharacterFromSet(NSCharacterSet.letterCharacterSet()) != nil {
                 if string.rangeOfCharacter(from: .letters) != nil {
                guard let text = textField.text else { return true }
                let newLength = text.count + string.count - range.length
                
                let aSet = NSCharacterSet(charactersIn:" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,").inverted
              //  let compSepByCharInSet = string.componentsSeparatedByCharactersInSet(aSet)
              //  let numberFiltered = compSepByCharInSet.joinWithSeparator("")
                    let compSepByCharInSet = string.components(separatedBy: aSet)
                    let numberFiltered = compSepByCharInSet.joined(separator: "")
                return (string == numberFiltered) && newLength <= 35
                
            }
            else
            {
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
                
                return false
                
            }
            
        }


        
        if(textField.tag==3)
        {
            guard let text = textField.text else { return true }
            
            let newLength = text.count + string.count - range.length
            return newLength <= 6
        }
        return true
    }
    
    func vendor()
    {
        if  LibraryAPI.sharedInstance.getCity != nil
        {
            if  LibraryAPI.sharedInstance.getCity != ""
            {
                citytf.text =  LibraryAPI.sharedInstance.getCity
                statetf.text = LibraryAPI.sharedInstance.getState
               
            }
            else
            {
                //citytf.text = ""
            }
        }
    }
    
    
    func keyboardWillShow(notification: NSNotification)
    {
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true)
        {
           
        }
        else
        {
            if(Movedup==false)
            {
                self.view.frame.origin.y -= 70
                Movedup=true
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true)
        {
            
        }
        else
        {
            self.view.frame.origin.y = 0
            Movedup=false
        }
    }

    
    func customnavigation()
        
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: 65);
        
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(EndCustomerViewController.buttonClicked),for:UIControlEvents.touchUpInside)
            self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "End CustomerDetails"
        
    }
    
    func buttonClicked(sender:UIButton)
    {
        
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "SpcoptionentryViewController")), animated: true)
    }

    
    @IBAction func SubmitAction(_ sender: AnyObject)
    {
        if ((self.endcusnametf.text!.isEmpty))
        {
            self.NormalAlert(title: "Alert", Messsage: "Please Enter the EndCustomerName")
          
            
        }
         else if(self.vertical.text!.isEmpty)
        {
             self.NormalAlert(title: "Alert", Messsage: "Please Enter the VerticalField")
          
        }
        else
        {
        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "SpcoptionentryViewController") as! SpcoptionentryViewController
            
                
                self.tempstorage()
                self.navigationController?.pushViewController(lvc, animated: true)
        }
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
  
        if(textField.tag == 1)
        {
            LibraryAPI.sharedInstance.Currentcustomerlookup="City"
            if #available(iOS 10.0, *)
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: CustomerLookupViewController.instance())
                    
                    .didShowHandler
                    { popup in
                        
                    }
                    .didCloseHandler
                    { _ in
                       // self.vendor()
                        
                }
                
                let container = CustomerLookupViewController.instance()
                container.closeHandler =
                    { _ in
                        self.vendor()
                        popup.dismiss()
                        
                }
                
                popup.show(childViewController: container)
            }
            
                return false
        }
        else if(textField.tag == 2)
        {
            
            let alert:UIAlertController=UIAlertController(title: "Select Vertical Type", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            var alert1 = UIAlertAction()
            if pickerarray2.count != 0
            {
                for word in pickerarray2
                {
                    alert1 = UIAlertAction(title: (word as! String), style: UIAlertActionStyle.default)
                    {
                        
                        UIAlertAction in
                        self.vertical.text = (word as! String)
                        self.pincodetf.resignFirstResponder()
                        
                        
                        
                    }
                    
                    alert.addAction(alert1)
                    
                }
                
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
            {
                UIAlertAction in
                
            }
            
            // Add the actions
            //picker?.delegate = self
            
            alert.addAction(cancelAction)
            
            self.present(alert, animated: true, completion: nil)
            
            return false
            
        }
        
            return true

        }
    
    func cityrec()
    {
        //let size = CGSize(width: 30, height:30)
       self.local_activityIndicator_start()
        // new version dispatch Que
        
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
            
             self.Loaddatacity(url: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/FieldDescription?Userid=\(LibraryAPI.sharedInstance.Userid)&prmt=VR")
            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
               self.local_activityIndicator_stop()
            }
        }
       }
    
    
    func Loaddatacity(url:String)
    {
        //responsearray2 = LibraryAPI.sharedInstance.RequestUrl(url: url)
        self.getsuccess(params: [:], urlhit: url, completion: { (jsonmutable) in
            self.responsearray2 = jsonmutable
            if(self.responsearray2.count > 0)
            {
                
            }
            self.pickerarray2.removeAllObjects()
            for i in 0..<self.responsearray2.count
            {
                //  if(currenttf=="business")
                // {
                let dicres = self.responsearray2[i] as! NSDictionary
                let code = dicres.object(forKey: "Mode") as? String
                let desc = dicres.object(forKey: "Detail") as? String
                let str=code?.appending(" - \(desc!)")
                self.pickerarray2.add(str!)
                
                // }
            }
        })
       
    
    }
    func tempstorage()
    {
        LibraryAPI.sharedInstance.globaldic.setObject(quotetf.text!, forKey: "vendor" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(endcusnametf.text!, forKey: "endcustomername" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(address1tf.text!, forKey: "address1" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(address2tf.text!, forKey: "address2" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(citytf.text!, forKey: "city" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(statetf.text!, forKey: "state" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(pincodetf.text!, forKey: "pincode" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(vertical.text!, forKey: "vertical" as NSCopying)
      }
  
   }
