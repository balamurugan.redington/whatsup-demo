//
//  PartnerRegister1ViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 14/07/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

class PartnerRegister1ViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var ScrollView: UIScrollView!
    
    @IBOutlet weak var MainView: UIView!
    

    @IBOutlet weak var nextbtn: UIButton!
    @IBOutlet weak var FourthMainview: UIView!
    @IBOutlet weak var ThirdMainView: UIView!
    @IBOutlet weak var secondmainview: UIView!
    @IBOutlet weak var firstmainview: UIView!
    
   
    @IBOutlet weak var Companynametf1: UITextField!
    
    @IBOutlet weak var Address11: UITextField!
    
    @IBOutlet weak var address12: UITextField!
    @IBOutlet weak var address13: UITextField!
    
    @IBOutlet weak var city1tf: UITextField!
    
    @IBOutlet weak var state1tf: UITextField!
    
    @IBOutlet weak var pincode1tf: UITextField!
    
    @IBOutlet weak var Contactname2: UITextField!
    
    @IBOutlet weak var contactmail2tf: UITextField!
    
    @IBOutlet weak var phonenumber2tf: UITextField!
    
    @IBOutlet weak var faxnumbertf2: UITextField!
    
    @IBOutlet weak var panumbertf2: UITextField!
    
    @IBOutlet weak var itbtn3: UIButton!
    
    @IBOutlet weak var nonitbtn3: UIButton!
    
    @IBOutlet weak var gsttf3: UITextField!
    
    @IBOutlet weak var profiletf3: UITextField!
    
    @IBOutlet weak var concerntf3: UITextField!
    
    @IBOutlet weak var creditlimittf3: UITextField!
    
   
    
    @IBOutlet weak var Addresstf41: UITextField!
    
    @IBOutlet weak var Address42: UITextField!
    
    @IBOutlet weak var Address43: UITextField!
    
    @IBOutlet weak var Ctytf41: UITextField!
    
    @IBOutlet weak var statetf41: UITextField!
    
    @IBOutlet weak var pincodetf41: UITextField!
    
    
    @IBOutlet weak var addresstf421: UITextField!
    
    @IBOutlet weak var address422: UITextField!
    
    @IBOutlet weak var address423: UITextField!
    
    @IBOutlet weak var citytf421: UITextField!
    
    @IBOutlet weak var statetf422: UITextField!
    
    @IBOutlet weak var pincodetf423: UITextField!
    
    @IBOutlet weak var redaddress2: UILabel!
    
    var responsearray2=NSMutableArray()
    var pickerarray2=NSMutableArray()
    var it = String()
    var nonit = String()
    var conceern = String()
    var profiledemo = String()
    var Movedup=Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
             self.ScrollView.contentSize = MainView.frame.size
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        customnavigation()
        
        
        ScrollView.bounces = false
         MainView.layer.borderWidth = 1.0
        MainView.layer.borderColor = UIColor(red:30/255.0, green:184/255.0, blue:120/255.0, alpha: 1.0).cgColor
        
        //1st Screen
        MainView.layer.masksToBounds = true
        MainView.layer.cornerRadius = 5
        nextbtn.layer.masksToBounds=true
        nextbtn.layer.cornerRadius=5
        
        creditlimittf3.keyboardType = .decimalPad
        
        //tags
        Companynametf1.tag = 1
        Address11.tag = 2
        address12.tag = 3
        address13.tag = 4
        city1tf.tag = 5
        state1tf.tag = 6
        pincode1tf.tag = 7
        Contactname2.tag = 8
        contactmail2tf.tag = 9
        phonenumber2tf.tag = 10
        faxnumbertf2.tag = 11
        
        panumbertf2.tag = 12
        gsttf3.tag = 13
        profiletf3.tag = 14
        concerntf3.tag = 15
        creditlimittf3.tag = 16
        Addresstf41.tag = 17
        Address42.tag = 18
        Address43.tag = 19
        Ctytf41.tag = 20
        statetf41.tag = 21
        pincodetf41.tag = 22
        
        addresstf421.tag = 23
        address422.tag = 24
        address423.tag = 25
        citytf421.tag = 26
        statetf422.tag = 27
        pincodetf423.tag = 28
        
        //delegates
        Companynametf1.delegate = self
        Address11.delegate = self
        address12.delegate = self
         address13.delegate = self
         city1tf.delegate = self
         state1tf.delegate = self
         pincode1tf.delegate = self
         Contactname2.delegate = self
         contactmail2tf.delegate = self
         phonenumber2tf.delegate = self
         faxnumbertf2.delegate = self
         panumbertf2.delegate = self
         gsttf3.delegate = self
         profiletf3.delegate = self
         concerntf3.delegate = self
         creditlimittf3.delegate = self
         Addresstf41.delegate = self
         Address42.delegate = self
         Address43.delegate = self
         Ctytf41.delegate = self
         statetf41.delegate = self
         pincodetf41.delegate = self
         addresstf421.delegate = self
         address422.delegate = self
         address423.delegate = self
         citytf421.delegate = self
         statetf422.delegate = self
         pincodetf423.delegate = self
        
        pincode1tf.keyboardType = .numberPad
        pincodetf41.keyboardType = .numberPad
        pincodetf423.keyboardType = .numberPad
        phonenumber2tf.keyboardType = .numberPad
        faxnumbertf2.keyboardType = .numberPad
        
        state1tf.isUserInteractionEnabled = false
        statetf41.isUserInteractionEnabled = false
        statetf422.isUserInteractionEnabled = false
        
        
         self.cityrec()
        

        if(LibraryAPI.sharedInstance.partnerglobaldic.count > 0)
        {
            gettempstorage()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(PartnerRegister1ViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PartnerRegister1ViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        it = "IT"
        itbtn3.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
        nonitbtn3.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        
        self.addDoneButtonOnKeyboard()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
       
    }
    override func viewDidAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(PartnerRegister1ViewController.doneButtonAction))
        
    
        let items = NSMutableArray()
        
        items.add(flexSpace)
        
        items.add(done)
        
        doneToolbar.items = items as AnyObject as? [UIBarButtonItem]
        
        doneToolbar.sizeToFit()
        
        pincode1tf.inputAccessoryView = doneToolbar
        pincodetf41.inputAccessoryView = doneToolbar
        pincodetf423.inputAccessoryView = doneToolbar
        phonenumber2tf.inputAccessoryView = doneToolbar
        faxnumbertf2.inputAccessoryView = doneToolbar
        creditlimittf3.inputAccessoryView = doneToolbar
        
    }
    func doneButtonAction()
    {
        pincode1tf.resignFirstResponder()
        pincodetf41.resignFirstResponder()
        pincodetf423.resignFirstResponder()
        phonenumber2tf.resignFirstResponder()
        faxnumbertf2.resignFirstResponder()
        creditlimittf3.resignFirstResponder()
    }
    
    //KEYBOARD
    
    func keyboardWillShow(notification: NSNotification)
    {
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true)
        {
        }
        else
        {
            if(Movedup==false)
            {
                self.view.frame.origin.y -= 100
                Movedup=true
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true)
        {
        }
        else
        {
            self.view.frame.origin.y += 100
            Movedup=false
        }
    }
    
    func customnavigation()
        
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: 65)
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"),  for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(PartnerRegister1ViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Partner Registration"
        
    }
    
    func buttonClicked(sender:UIButton)
    {
       
        
        LibraryAPI.sharedInstance.partnerglobaldic.removeAllObjects()

        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
    }
    


   
    @IBAction func NextBtnAction(_ sender: AnyObject)
    {
        

        
        if((Companynametf1.text! == "")||(Companynametf1.text == nil))
        {
            self.NormalAlert(title: "Alert", Messsage: "Please Enter the Customer Name" )

        }
        
        else if((Address11.text! == "") || (Address11.text == nil))
        {
            self.NormalAlert(title: "Alert", Messsage: "Please Enter the Customer Address 1" )

            
        }
        
        
        else if((city1tf.text == "")||(city1tf.text == nil))
        {
            self.NormalAlert(title: "Alert", Messsage: "Please Enter the City")

            
        }

        else if((pincode1tf.text == "")||(pincode1tf.text == nil))
        {
            self.NormalAlert(title: "Alert", Messsage: "Please Enter the Valid Pincode")
        }
        else if(pincode1tf.text?.count != 6)
        {
            self.NormalAlert(title: "Alert", Messsage: "Please Enter the Valid Pincode")
            
        }
         else if((Contactname2.text == "")||(Contactname2.text == nil))
        {
             self.NormalAlert(title: "Alert", Messsage: "Please Enter the Contact Name")

        }
       else if((contactmail2tf.text == "")||(contactmail2tf.text == nil)||self.isValidEmail(testStr: contactmail2tf.text!)==false)
        {
            self.NormalAlert(title: "Alert", Messsage: "Please Enter the valid Email")

        }
       else if((phonenumber2tf.text! == "")||(phonenumber2tf.text == nil))
        {
            self.NormalAlert(title: "Alert", Messsage: "Please Enter the Valid Phone Number")
        }
        else if(((phonenumber2tf.text?.count)! < 9) && ((phonenumber2tf.text?.count)! > 12) )
        {
             self.NormalAlert(title: "Alert", Messsage: "Please Enter the Valid Phone Number")
        }
        else if((panumbertf2.text! == "")||self.validatePANCardNumber(strPANNumber: panumbertf2.text!)==false)
        {
            self.NormalAlert(title: "Alert", Messsage: "Please Enter the Valid Pan Number")
        }
        else if((faxnumbertf2.text! != "") && ((faxnumbertf2.text?.count)! < 9) && ((faxnumbertf2.text?.count)! > 12))
        {
            self.NormalAlert(title: "Alert", Messsage: "Please Enter the valid Fax Number ")
        }
       else if((gsttf3.text == "")||(gsttf3.text == nil))
        {
             self.NormalAlert(title: "Alert", Messsage: "Please Enter the Valid GST ")
        }
        else if(((gsttf3.text?.count)! < 15))
        {
            self.NormalAlert(title: "Alert", Messsage: "Please Enter the Valid GST ")
        }
       else if((profiletf3.text == "")||(profiletf3.text == nil))
        {
             self.NormalAlert(title: "Alert", Messsage: "Please Select the Profile")
        }
       else if((concerntf3.text == "")||(concerntf3 == nil))
        {
             self.NormalAlert(title: "Alert", Messsage: "Please Select the Type Of Concern")
        }
       else if((creditlimittf3.text == "")||(creditlimittf3 == nil))
        {
            self.NormalAlert(title: "Alert", Messsage: "Please Enter the Credit Limt")
        }
       else if(Addresstf41.text == "")||(Addresstf41.text == nil)
        {
             self.NormalAlert(title: "Alert", Messsage: "Please Enter the Residence Address 1 field 1")
        }
       else if((Address42.text == "")||(Address42.text == nil))
        {
             self.NormalAlert(title: "Alert", Messsage: "Please Enter the Residence Address 1 field 2")
        }
       else if((Address43.text == "")||(Address43.text == nil))
        {
            self.NormalAlert(title: "Alert", Messsage: "Please Enter the Residence Address 1 field 3")
        }
       else if((Ctytf41.text == "")||(Ctytf41.text == nil))
        {
             self.NormalAlert(title: "Alert", Messsage: "Please Enter the Residence Address 1 City")
        }
       else if(pincodetf41.text == "")||(pincodetf41.text == nil)
        {
             self.NormalAlert(title: "Alert", Messsage: "Please Enter the Pincode")
        }
        else if(pincodetf41.text?.count != 6)
        {
             self.NormalAlert(title: "Alert", Messsage: "Please Enter the Valid Pincode")
        }
        else
        {
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "PartnerRegister2ViewController")), animated: true)
            tempstorage()
        }
    }
    func vendor()
    {
        if  LibraryAPI.sharedInstance.getCity != nil
        {
            if  LibraryAPI.sharedInstance.getCity != ""
            {
                city1tf.text =  LibraryAPI.sharedInstance.getCity
                state1tf.text = LibraryAPI.sharedInstance.getState
                
            }
            else
            {
                //citytf.text = ""
            }
        }
    }
    func vendor1()
    {
        if  LibraryAPI.sharedInstance.getCity != nil
        {
            if  LibraryAPI.sharedInstance.getCity != ""
            {
                Ctytf41.text =  LibraryAPI.sharedInstance.getCity
                statetf41.text = LibraryAPI.sharedInstance.getState
                
            }
            else
            {
                
            }
        }
    }
    func vendor2()
    {
        if  LibraryAPI.sharedInstance.getCity != nil
        {
            if  LibraryAPI.sharedInstance.getCity != ""
            {
                citytf421.text =  LibraryAPI.sharedInstance.getCity
                statetf422.text = LibraryAPI.sharedInstance.getState
                
            }
            else
            {
               
            }
        }
    }

    
    func isValidEmail(testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func validatePANCardNumber(strPANNumber : String) -> Bool{
        let regularExpression = "[A-Z]{5}[0-9]{4}[A-Z]{1}"
        let panCardValidation = NSPredicate(format : "SELF MATCHES %@", regularExpression)
        // return panCardValidation.evaluate(with: strPANNumber)
        return panCardValidation.evaluate(with: strPANNumber)
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField.tag == 1)
        {
            
           // if string.rangeOfCharacterFromSet(NSCharacterSet.letterCharacterSet()) != nil {
            if string.rangeOfCharacter(from: NSCharacterSet.letters) != nil{
                guard let text = textField.text else { return true }
                let newLength = text.count + string.count - range.length
                let aSet = NSCharacterSet(charactersIn:" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,").inverted
                //let compSepByCharInSet = string.componentsSeparatedByCharactersInSet(aSet)
                let compSepByCharInSet  = string.components(separatedBy: aSet)
                let numberFiltered = compSepByCharInSet.joined(separator: "")
                 return (string == numberFiltered) && newLength <= 35
                
            }
            else
            {
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                   
                    return true
                }
                
                return false
                
            }
            
        }
        if(textField.tag == 2) {
            
           // if string.rangeOfCharacterFromSet(NSCharacterSet.letterCharacterSet()) != nil {
            if string.rangeOfCharacter(from: NSCharacterSet.letters) != nil {
                guard let text = textField.text else { return true }
                let newLength = text.count + string.count - range.length
                
                let aSet = NSCharacterSet(charactersIn:" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,").inverted
               // let compSepByCharInSet = string.componentsSeparatedByCharactersInSet(aSet)
                let compSepByCharInSet = string.components(separatedBy: aSet)
                let numberFiltered = compSepByCharInSet.joined(separator: "")
                return (string == numberFiltered) && newLength <= 35
                
            }
            else
            {
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                  
                    return true
                }
                
                return false
                
            }
            
        }
        
        if(textField.tag == 3) {
            
           // if string.rangeOfCharacterFromSet(NSCharacterSet.letterCharacterSet()) != nil {
            if string.rangeOfCharacter(from: NSCharacterSet.letters) != nil {
                guard let text = textField.text else { return true }
                let newLength = text.count + string.count - range.length
                
                let aSet = NSCharacterSet(charactersIn:" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,").inverted
               // let compSepByCharInSet = string.componentsSeparatedByCharactersInSet(aSet)
                let compSepByCharInSet = string.components(separatedBy: aSet)
                let numberFiltered = compSepByCharInSet.joined(separator: "")
                return (string == numberFiltered) && newLength <= 35
                
            }
            else
            {
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                  
                    return true
                }
                
                return false
                
            }
            
        }
        if(textField.tag == 4) {
            
         //   if string.rangeOfCharacterFromSet(NSCharacterSet.letterCharacterSet()) != nil {
                if string.rangeOfCharacter(from: .letters) != nil {
                guard let text = textField.text else { return true }
                let newLength = text.count + string.count - range.length
                
                let aSet = NSCharacterSet(charactersIn:" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,").inverted
                //let compSepByCharInSet = string.componentsSeparatedByCharactersInSet(aSet)
                     let compSepByCharInSet = string.components(separatedBy: aSet)
                //let numberFiltered = compSepByCharInSet.joinedWithSeparator(separator: "")
                    let numberFiltered = compSepByCharInSet.joined(separator: "")
                return (string == numberFiltered) && newLength <= 35
                
            }
            else
            {
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                   
                    return true
                }
                
                return false
                
            }
            
        }
       
        if(textField.tag==7)
        {
            guard let text = textField.text else { return true }
            
            let newLength = text.count + string.count - range.length
            return newLength <= 6
        }
        
        if(textField.tag == 8) {
            
          //  if string.rangeOfCharacterFromSet(NSCharacterSet.letterCharacterSet()) != nil {
                if string.rangeOfCharacter(from: .letters) != nil {
                guard let text = textField.text else { return true }
                let newLength = text.count + string.count - range.length
                
                let aSet = NSCharacterSet(charactersIn:" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,").inverted
            //    let compSepByCharInSet = string.componentsSeparatedByCharactersInSet(aSet)
                    let compSepByCharInSet = string.components(separatedBy: aSet)
                let numberFiltered = compSepByCharInSet.joined(separator: "")
                return (string == numberFiltered) && newLength <= 35
            } else {
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
                return false
            }
        }
        
        if(textField.tag == 13) {
         //   if string.rangeOfCharacterFromSet(NSCharacterSet.letterCharacterSet()) != nil {
            textField.autocapitalizationType = .allCharacters
            if string.rangeOfCharacter(from: .alphanumerics) != nil {
                guard let text = textField.text else { return true }
                let newLength = text.count + string.count - range.length
                let aSet = NSCharacterSet(charactersIn:" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,").inverted
               // let compSepByCharInSet = string.componentsSeparatedByCharactersInSet(aSet)
                let compSepByCharInSet = string.components(separatedBy: aSet)
               // let numberFiltered = compSepByCharInSet.joinWithSeparator("")
                let numberFiltered = compSepByCharInSet.joined(separator: "")
                return (string == numberFiltered) && newLength <= 15
            } else {
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    
                    return true
                }
                return false
            }
        }
        if(textField.tag == 16) {
            let oldString = textField.text?.count == 0 ? string  : textField.text!
            let i = oldString.count
            let range = oldString.index(oldString.startIndex, offsetBy: i-1) ..< oldString.index(oldString.startIndex, offsetBy: i)
            let candidate = oldString.replacingCharacters(in: range, with: string)
            let regex = try? NSRegularExpression(pattern: "^\\d{0,8}(\\.\\d{0,2})?$", options: [])
            return regex?.firstMatch(in: candidate, options: [], range: NSRange(location: 0, length: candidate.count)) != nil
        }
        if(textField.tag == 17) {
                if string.rangeOfCharacter(from: .letters) != nil {
                guard let text = textField.text else { return true }
                let newLength = text.count + string.count - range.length
                
                let aSet = NSCharacterSet(charactersIn:" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,").inverted
                    let compSepByCharInSet = string.components(separatedBy: aSet)
                    let numberFiltered = compSepByCharInSet.joined(separator: "")
                return (string == numberFiltered) && newLength <= 35
            } else {
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
                return false
            }
        }
        if(textField.tag == 18) {
            if string.rangeOfCharacter(from: .letters) != nil {
                guard let text = textField.text else { return true }
                let newLength = text.count + string.count - range.length
                let aSet = NSCharacterSet(charactersIn:" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,").inverted
                let compSepByCharInSet = string.components(separatedBy: aSet)
                let numberFiltered = compSepByCharInSet.joined(separator: "")
                return (string == numberFiltered) && newLength <= 35
            } else {
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
                return false
            }
        }
        if(textField.tag == 19) {
           // if string.rangeOfCharacterFromSet(NSCharacterSet.letterCharacterSet()) != nil {
                 if string.rangeOfCharacter(from: .letters) != nil {
                guard let text = textField.text else { return true }
                let newLength = text.count + string.count - range.length
                let aSet = NSCharacterSet(charactersIn:" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,").inverted
                    let compSepByCharInSet = string.components(separatedBy: aSet)
                    let numberFiltered = compSepByCharInSet.joined(separator: "")
                return (string == numberFiltered) && newLength <= 35
            } else {
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
                return false
            }
        }
        if(textField.tag == 23) {
           // if string.rangeOfCharacterFromSet(NSCharacterSet.letterCharacterSet()) != nil {
                if string.rangeOfCharacter(from: .letters) != nil {
                guard let text = textField.text else { return true }
                let newLength = text.count + string.count - range.length
                let aSet = NSCharacterSet(charactersIn:" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,").inverted
              //  let compSepByCharInSet = string.componentsSeparatedByCharactersInSet(aSet)
              //  let numberFiltered = compSepByCharInSet.joinWithSeparator("")
                    let compSepByCharInSet = string.components(separatedBy: aSet)
                    let numberFiltered = compSepByCharInSet.joined(separator: "")
                return (string == numberFiltered) && newLength <= 35
            } else {
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
                return false
            }
        }
        if(textField.tag == 24) {
           // if string.rangeOfCharacterFromSet(NSCharacterSet.letterCharacterSet()) != nil {
                if string.rangeOfCharacter(from: .letters) != nil {
                guard let text = textField.text else { return true }
                let newLength = text.count + string.count - range.length
                let aSet = NSCharacterSet(charactersIn:" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,").inverted
               // let compSepByCharInSet = string.componentsSeparatedByCharactersInSet(aSet)
               // let numberFiltered = compSepByCharInSet.joinWithSeparator("")
                    let compSepByCharInSet = string.components(separatedBy: aSet)
                    let numberFiltered = compSepByCharInSet.joined(separator: "")
                return (string == numberFiltered) && newLength <= 35
            } else {
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
                return false
            }
        }
        if(textField.tag == 25) {
           // if string.rangeOfCharacterFromSet(NSCharacterSet.letterCharacterSet()) != nil {
                 if string.rangeOfCharacter(from: .letters) != nil {
                guard let text = textField.text else { return true }
                let newLength = text.count + string.count - range.length
                let aSet = NSCharacterSet(charactersIn:" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,").inverted
               // let compSepByCharInSet = string.componentsSeparatedByCharactersInSet(aSet)
              //  let numberFiltered = compSepByCharInSet.joinWithSeparator("")
                    let compSepByCharInSet = string.components(separatedBy: aSet)
                    let numberFiltered = compSepByCharInSet.joined(separator: "")
                return (string == numberFiltered) && newLength <= 35
            } else {
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
                return false
            }
        }
        if(textField.tag == 22) {
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 6
        }
        if(textField.tag == 28) {
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 6
        }
        if(textField.tag == 12) {
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 10
        }
        if(textField.tag == 10) {
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 11
        }
        if(textField.tag == 11) {
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 11
        }
        return true
    }

    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if(textField.tag == 5)
        {
            LibraryAPI.sharedInstance.Currentcustomerlookup="City"
            if #available(iOS 10.0, *) {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: CustomerLookupViewController.instance())
                    .didShowHandler
                    { popup in
                    }
                    .didCloseHandler
                    { _ in
                        // self.vendor()
                }
                let container = CustomerLookupViewController.instance()
                container.closeHandler =
                    { _ in
                        self.vendor()
                        popup.dismiss()
                }
                popup.show(childViewController: container)
            } else {
            }
            return false
        } else if(textField.tag == 20) {
            LibraryAPI.sharedInstance.Currentcustomerlookup="City"
            if #available(iOS 10.0, *) {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: CustomerLookupViewController.instance())
                    .didShowHandler
                    { popup in
                    }
                    .didCloseHandler
                    { _ in
                }
                let container = CustomerLookupViewController.instance()
                container.closeHandler =
                    { _ in
                        self.vendor1()
                        popup.dismiss()
                }
                popup.show(childViewController: container)
            }
            return false
        } else if(textField.tag == 26) {
            LibraryAPI.sharedInstance.Currentcustomerlookup="City"
            if #available(iOS 10.0, *) {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: CustomerLookupViewController.instance())
                    .didShowHandler
                    { popup in
                    }
                    .didCloseHandler
                    { _ in
                }
                let container = CustomerLookupViewController.instance()
                container.closeHandler =
                    { _ in
                        self.vendor2()
                        popup.dismiss()
                }
                popup.show(childViewController: container)
            }
            return false
        }
        if(textField.tag == 15) {
            let alert:UIAlertController=UIAlertController(title: "Select Type of Concern", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            let head2 = ["Sole Propritorship","Partner","Company"]
            var alert1 = UIAlertAction()
            if head2.count != 0 {
                for word in head2 {
                    alert1 = UIAlertAction(title: word, style: UIAlertActionStyle.default) {
                        UIAlertAction in
                        self.concerntf3.text = word
                        if(self.concerntf3.text == "Sole proprietorship") {
                            self.conceern = "1"
                        } else if(self.concerntf3.text == "Partner") {
                             self.conceern = "2"
                        } else if(self.concerntf3.text == "Company") {
                            self.conceern = "3"
                        } else {
                        }
                    }
                    alert.addAction(alert1)
                }
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                UIAlertAction in
            }
            // Add the actions
            //picker?.delegate = self
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
            return false
        } else if(textField.tag == 14) {
            let alert:UIAlertController=UIAlertController(title: "Select Profile Type", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            var alert1 = UIAlertAction()
            if pickerarray2.count != 0 {
                for word in pickerarray2 {
                    alert1 = UIAlertAction(title: (word as! String), style: UIAlertActionStyle.default){
                        UIAlertAction in
                        self.profiletf3.text = (word as! String)
                        //let profile :[String] = self.profiletf3.text!.componentsSeparatedByString(" - ")
                        let profile : [String] = self.profiletf3.text!.components(separatedBy: " - ")
                        self.profiledemo = profile[0]
                    }
                    alert.addAction(alert1)
                }
            }
              let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                UIAlertAction in
            }
                alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
            return false
        }
        if(textField.tag == 12) {
            textField.autocapitalizationType = .allCharacters
        }
        if(textField.tag == 13) {
            textField.autocapitalizationType = .allCharacters
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func cityrec() {
       // let size = CGSize(width: 30, height:30)
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
             self.Loaddatacity(url: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/FieldDescription?Userid=\(LibraryAPI.sharedInstance.Userid)&prmt=PARP")
            DispatchQueue.main.async {
             self.local_activityIndicator_stop()
            }
        }
    }
    
    func Loaddatacity(url:String) {
        //responsearray2 = LibraryAPI.sharedInstance.RequestUrl(url: url)
        self.getsuccess(params: [:], urlhit: url, completion:  { (jsonmutable) in
            self.responsearray2 = jsonmutable
            if(self.responsearray2.count > 0) {
            }
            self.pickerarray2.removeAllObjects()
            for i in 0..<self.responsearray2.count {
                //  if(currenttf=="business")
                // {
                let dic = self.responsearray2[i] as! NSDictionary
                let code = dic.object(forKey: "Mode") as? String
                let desc = dic.object(forKey: "Detail") as? String
                let str=code?.appending(" - \(desc!)")
                self.pickerarray2.add(str!)
                
                // }
            }
            
        })
        
        
    }

    @IBAction func ItBtnAction(_ sender: AnyObject)
    {
        it = "IT"
        itbtn3.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
        nonitbtn3.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
    }
    
    
    @IBAction func NonITAction(_ sender: AnyObject)
    {
        it = "NI"
        itbtn3.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        nonitbtn3.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
    }
   
    func tempstorage()
    {
        
        LibraryAPI.sharedInstance.partnerglobaldic.setObject(Companynametf1.text!, forKey: "companyname" as NSCopying)
        LibraryAPI.sharedInstance.partnerglobaldic.setObject(Address11.text!, forKey: "Address11" as NSCopying)
        LibraryAPI.sharedInstance.partnerglobaldic.setObject(address12.text!, forKey: "address12" as NSCopying)
        LibraryAPI.sharedInstance.partnerglobaldic.setObject(address13.text!, forKey: "address13" as NSCopying)
        LibraryAPI.sharedInstance.partnerglobaldic.setObject(city1tf.text!, forKey: "city1" as NSCopying)
        LibraryAPI.sharedInstance.partnerglobaldic.setObject(state1tf.text!, forKey: "state1" as NSCopying)
        LibraryAPI.sharedInstance.partnerglobaldic.setObject(pincode1tf.text!, forKey: "pincode1" as NSCopying)
        LibraryAPI.sharedInstance.partnerglobaldic.setObject(Contactname2.text!, forKey: "Contactname2" as NSCopying)
        LibraryAPI.sharedInstance.partnerglobaldic.setObject(contactmail2tf.text!, forKey: "contactmail2" as NSCopying)
        LibraryAPI.sharedInstance.partnerglobaldic.setObject(phonenumber2tf.text!, forKey: "phonenumber2" as NSCopying)
        LibraryAPI.sharedInstance.partnerglobaldic.setObject(faxnumbertf2.text!, forKey: "faxnumber" as NSCopying)
        LibraryAPI.sharedInstance.partnerglobaldic.setObject(panumbertf2.text!, forKey: "panumber" as NSCopying)
        LibraryAPI.sharedInstance.partnerglobaldic.setObject(gsttf3.text!, forKey: "gst3" as NSCopying)
        LibraryAPI.sharedInstance.partnerglobaldic.setObject(profiledemo, forKey: "profile" as NSCopying)
        LibraryAPI.sharedInstance.partnerglobaldic.setObject(conceern, forKey: "concern" as NSCopying)
        LibraryAPI.sharedInstance.partnerglobaldic.setObject(creditlimittf3.text!, forKey: "creditlimit3" as NSCopying)
        LibraryAPI.sharedInstance.partnerglobaldic.setObject(Addresstf41.text!, forKey: "Address41" as NSCopying)
        LibraryAPI.sharedInstance.partnerglobaldic.setObject(Address42.text!, forKey: "Address42" as NSCopying)
        LibraryAPI.sharedInstance.partnerglobaldic.setObject(Address43.text!, forKey: "Address43" as NSCopying)
        LibraryAPI.sharedInstance.partnerglobaldic.setObject(Ctytf41.text!, forKey: "Ctytf41" as NSCopying)
        LibraryAPI.sharedInstance.partnerglobaldic.setObject(statetf41.text!, forKey: "statetf41" as NSCopying)
        LibraryAPI.sharedInstance.partnerglobaldic.setObject(pincodetf41.text!, forKey: "pincodetf41" as NSCopying)
        LibraryAPI.sharedInstance.partnerglobaldic.setObject(addresstf421.text!, forKey: "addresstf421" as NSCopying)
        LibraryAPI.sharedInstance.partnerglobaldic.setObject(address422.text!, forKey: "address422" as NSCopying)
        LibraryAPI.sharedInstance.partnerglobaldic.setObject(address423.text!, forKey: "address423" as NSCopying)
        LibraryAPI.sharedInstance.partnerglobaldic.setObject(citytf421.text!, forKey: "citytf421" as NSCopying)
        LibraryAPI.sharedInstance.partnerglobaldic.setObject(statetf422.text!, forKey: "statetf422" as NSCopying)
        LibraryAPI.sharedInstance.partnerglobaldic.setObject(pincodetf423.text!, forKey: "pincodetf423" as NSCopying)
        LibraryAPI.sharedInstance.partnerglobaldic.setObject(it, forKey: "division_it" as NSCopying)
       
        
        
    }
    
    func gettempstorage()
    {
       if LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "companyname") != nil
       {
        Companynametf1.text = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "companyname") as? String
        Address11.text = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "Address11") as? String
        address12.text = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "address12") as? String
        address13.text = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "address13") as? String
        city1tf.text = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "city1") as? String
        state1tf.text = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "state1") as? String
        pincode1tf.text = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "pincode1") as? String
        Contactname2.text = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "Contactname2") as? String
        contactmail2tf.text = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "contactmail2") as? String
        phonenumber2tf.text = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "phonenumber2") as? String
        faxnumbertf2.text = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "faxnumber") as? String
        panumbertf2.text = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "panumber") as? String
        gsttf3.text = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "gst3") as? String
        profiletf3.text = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "profile") as? String
        concerntf3.text = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "concern") as? String
        creditlimittf3.text = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "creditlimit3") as? String
        Addresstf41.text = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "Address41") as? String
        Address42.text = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "Address42") as? String
        Address43.text = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "Address43") as? String
        Ctytf41.text = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "Ctytf41") as? String
        statetf41.text = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "statetf41") as? String
        pincodetf41.text = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "pincodetf41") as? String
        addresstf421.text = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "addresstf421") as? String
        address422.text = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "address422") as? String
        address423.text = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "address423") as? String
        statetf422.text = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "statetf422") as? String
        pincodetf423.text = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "pincodetf423") as? String
        
        if (LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "division_it")as! String == "IT")
        {
            itbtn3.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
            nonitbtn3.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        }
        if (LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "division_it")as! String == "NI")
        {
            itbtn3.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
            nonitbtn3.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
        }
      }
    }
}
