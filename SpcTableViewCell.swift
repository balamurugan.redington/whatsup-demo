//
//  SpcTableViewCell.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 10/05/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class SpcTableViewCell: UITableViewCell {

    @IBOutlet weak var spcnolbl: UILabel!
    @IBOutlet weak var validitydate: UILabel!
    @IBOutlet weak var Totalspcvallbl: UILabel!
    @IBOutlet weak var statuslbl: UILabel!
    
    @IBOutlet weak var cellview: UIView!
    @IBOutlet weak var statusview: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
           }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

