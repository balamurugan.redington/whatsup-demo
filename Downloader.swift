//
//  Downloader.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 10/04/18.
//  Copyright © 2018 truetech. All rights reserved.
//

import Foundation
class Downloader : NSObject, URLSessionDownloadDelegate
{
    var url : NSURL?
    // will be used to do whatever is needed once download is complete
    var yourOwnObject : NSObject?
    
    init(yourOwnObject : NSObject)
    {
        self.yourOwnObject = yourOwnObject
    }
    
    //is called once the download is complete
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
    
        //copy downloaded data to your documents directory with same names as source file
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
       // let destinationUrl = documentsUrl.URLByAppendingPathComponent(url!.lastPathComponent!)
        let destinationUrl = documentsUrl?.appendingPathComponent((url?.lastPathComponent)!)
        //print(destinationUrl)
        //let dataFromURL = NSData(contentsOfURL: location as URL)
        let dataFromURL = NSData(contentsOf: location as URL)
        dataFromURL?.write(to: destinationUrl!, atomically: true)
        // let fileManager = NSFileManager.defaultManager()
        
        // Copy 'hello.swift' to 'subfolder/hello.swift'
        let documentsUrl1 =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        // let filename = String(url!.lastPathComponent!).stringByReplacingOccurrencesOfString("%20", withString: " ", options: .LiteralSearch, range: nil)
        //  let filepath = String(documentsUrl1!) +  filename
        //let destinationUrl1 = documentsUrl1.appendingPathComponent((url?.lastPathComponent)!)!
        let destinationUrl1 = documentsUrl1?.appendingPathComponent((url?.lastPathComponent)!)
        let bundle = Bundle.main.resourcePath! + "/" + (url?.lastPathComponent)!
        // let path = String(url!.lastPathComponent!)
        // let parts = path.componentsSeparatedByString(".pdf")
        //let pdfname = parts[0]
        //
        //let bundle = NSBundle.mainBundle().pathForResource(pdfname, ofType: "pdf")
        print(bundle)
        do {
            try FileManager.default.copyItem(atPath: (destinationUrl1?.path)!, toPath: bundle)
        }
        catch let error as NSError {
            print(error)
        }
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PDFcompleted"), object: nil)
    }
    
    //this is to track progress
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
     
    }
    
    // if there is an error during download this will be called
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        if(error != nil)
        {
            //handle the error
           
        }
    }
    
    //method to be called to download
    func download(url: NSURL)
    {
        self.url = url
        //download identifier can be customized. I used the "ulr.absoluteString"
       // let sessionConfig = URLSessionConfiguration.background(withIdentifier: url.absoluteString!)
        let sessionConfig = URLSessionConfiguration.background(withIdentifier: url.absoluteString!)
       // let session = URLSession(session: sessionConfig, task: self, didCompleteWithError: nil)
        let session = URLSession(configuration: sessionConfig, delegate: self, delegateQueue: nil)
        let task = session.downloadTask(with: url as URL)
        task.resume()
//        NSUserDefaults.standardUserDefaults().setObject("A", forKey: "Download")
//        NSUserDefaults.standardUserDefaults().synchronize()
    }
}
