//
//  VendorReferenceViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 12/06/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class VendorReferenceViewController: UIViewController,UITextFieldDelegate,PopupContentViewController{
    
    
    var closeHandler: (() -> Void)?
    
    
    @IBOutlet weak var itemcodetf: UITextField!
    
    @IBOutlet weak var okbtn: UIButton!
    
    let quotecode = String()
    var Movedup = Bool()
    
    let businesscode = String()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationmethod), name: NSNotification.Name(rawValue: "GetNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(VendorReferenceViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(VendorReferenceViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        itemcodetf.delegate = self
        okbtn.layer.cornerRadius=10.0
        
        okbtn.alpha=0.5
        
        
        okbtn.isUserInteractionEnabled=false
        
        okbtn.layer.cornerRadius = 15.0
        Movedup = false
        
        
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
    
    func keyboardWillShow(notification: NSNotification)
    {
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true)
        {
        }
        else
        {
            if(Movedup==false)
            {
                self.view.frame.origin.y -= 150
                Movedup=true
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true)
        {
        }
        else
        {
            self.view.frame.origin.y = 0
            Movedup=false
        }
    }

    
    class func instance() -> VendorReferenceViewController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "VendorReferenceViewController") as! VendorReferenceViewController
    }
    
    @IBAction func CloseAction(_ sender: AnyObject)
    {
        closeHandler?()
    }
    
    func sizeForPopup(popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize
    {
       // return CGSizeMake(260,260)
        return CGSize(width: 260, height: 260)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        itemcodetf.resignFirstResponder()
        return false
    }
    
    
    @IBAction func SubmitbtnAction(_ sender: AnyObject)
    {
        
        
        
        if ((itemcodetf.text == "") || (itemcodetf.text == nil))
        {
            self.NormalAlert(title: "Alert", Messsage: "Please Enter the Vendor Code")
          /*  let alertController = UIAlertController(title: "Alert", message:"Please Enter the Vendor Code" , preferredStyle: .Alert)
            let cancelAction = UIAlertAction(title: "Ok", style: .Cancel) { (action:UIAlertAction!) in
            }
            alertController.addAction(cancelAction)
            self.presentViewController(alertController, animated: true, completion:nil)*/
            
        }
            
            
        else if(LibraryAPI.sharedInstance.SPC_045 == "045")
        {
            LibraryAPI.sharedInstance.SPC_045_str = itemcodetf.text
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "VendorNotification_045"), object: nil)
            closeHandler?()
        }
            
        else
        {
            
            let lvc=self.storyboard?.instantiateViewController(withIdentifier: "EndCustomerViewController") as! EndCustomerViewController
            lvc.quotenumber = itemcodetf.text!
            
            
            self.navigationController?.pushViewController(lvc, animated: true)
            
            
        }
        
        
        
        
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
  
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        let aSet = NSCharacterSet(charactersIn:"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789").inverted
       // let compSepByCharInSet = string.componentsSeparatedByCharactersInSet(aSet)
        let compSepByCharInSet = string.components(separatedBy: aSet)
       // let numberFiltered = compSepByCharInSet.joinWithSeparator("")
        let numberFiltered = compSepByCharInSet.joined(separator: "")
         guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        if (isBackSpace == -92) {
            
            if(textField.text?.count == 6)
            {
                okbtn.alpha=1.0
                
                okbtn.isUserInteractionEnabled=true
                
                
            }
            else if(textField.text?.count == 7)
            {
                
                okbtn.alpha=1.0
                okbtn.isUserInteractionEnabled=true
                
                
            }
            else if(textField.text?.count == 12)
            {
                
                okbtn.alpha=1.0
                okbtn.isUserInteractionEnabled=true
                
                
            }
            else{
                okbtn.alpha=0.5
                
                okbtn.isUserInteractionEnabled=true
            }
            
            
        }
        else
        {
            if(textField.text?.count == 6)
            {
                okbtn.alpha=1.0
                
                okbtn.isUserInteractionEnabled=true
                
                
            }
            else if(textField.text?.count == 7)
            {
                
                okbtn.alpha=1.0
                okbtn.isUserInteractionEnabled=true
                
                
            }
            else if(textField.text?.count == 12)
            {
                
                okbtn.alpha=1.0
                okbtn.isUserInteractionEnabled=true
                
                
            }
            else{
                okbtn.alpha=0.5
                
                okbtn.isUserInteractionEnabled=true
            }
        }
        
        
        return true && string == numberFiltered && newLength <= 12
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {

        LibraryAPI.sharedInstance.poptextfieldclicked=false
        return true
    }
    func notificationmethod()
    {
        let userdefaults = Foundation.UserDefaults.standard
        let value = userdefaults.dictionary(forKey: "Notification")
        var title: String!
        var message: String!
        var imagestr: String!
        title = value?["title"] as! String
        message = value?["body"] as! String
        imagestr = value?["imageurl"] as! String
        
        if imagestr != nil
        {
            if imagestr != ""
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: notificationimageViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = notificationimageViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                    
                }
                popup.show(childViewController: container)
            }
            else
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: PushnotificationtextViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = PushnotificationtextViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                }
                
                popup.show(childViewController: container)
            }
        }
        else
        {
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: PushnotificationtextViewController.instance())
                
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
            }
            let container = PushnotificationtextViewController.instance()
            container.closeHandler = { _ in
                popup.dismiss()
            }
            
            popup.show(childViewController: container)
        }
        
        
    }
    
    
    
    
}
