//
//  PreorderpopupViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 11/05/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

@available(iOS 10.0, *)
class PreorderpopupViewController: UIViewController,PopupContentViewController
{
    
    @IBOutlet weak var proorderentrybtn: UIButton!
    @IBOutlet weak var bodentrybtn: UIButton!
    @IBOutlet weak var ecbentrybtn: UIButton!
    @IBOutlet weak var dbcentrybtn: UIButton!
    @IBOutlet weak var spcentrybtn: UIButton!
     var closeHandler: (() -> Void)?

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(close),
            name: NSNotification.Name(rawValue: "Closenotification"),
            object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(self.notificationmethod), name: NSNotification.Name(rawValue: "GetNotification"), object: nil)
        
        proorderentrybtn.layer.cornerRadius = 5
        proorderentrybtn.layer.borderWidth = 1
        
        bodentrybtn.layer.cornerRadius = 5
        bodentrybtn.layer.borderWidth = 1
        
        ecbentrybtn.layer.cornerRadius = 5
        ecbentrybtn.layer.borderWidth = 1
        
        dbcentrybtn.layer.cornerRadius = 5
        dbcentrybtn.layer.borderWidth = 1
        
        spcentrybtn.layer.cornerRadius = 5
        spcentrybtn.layer.borderWidth = 1

       
    }
    
    func close()
    {
        closeHandler?()
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
       
    }
    class func instance() -> PreorderpopupViewController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "PreorderpopupViewController") as! PreorderpopupViewController
    }
    @IBAction func CloseAction(_ sender: AnyObject)
    {
        
        closeHandler?()
    }
    
    func sizeForPopup(popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: 280, height: 250)
    }
    
    
    @available(iOS 10.0, *)
    @IBAction func preorderaction(_ sender: AnyObject)
    {
        
       // let responsestring = LibraryAPI.sharedInstance.Parsejson1(url: "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=WHATSUP&Option=3")
       self.getStringsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=WHATSUP&Option=3", completion: { (jsonstr) in
            let responsestring  = jsonstr
            if(responsestring == "Y")
            {
                LibraryAPI.sharedInstance.Currentcustomerlookup="Preorder"
                
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    
                    .show(childViewController: CustomerLookupViewController.instance())
                    
                    
                    .didShowHandler { popup in
                        
                        
                    }
                    .didCloseHandler { _ in
                        self.closeHandler?()
                        
                }
                self.closeHandler?()
                let container = CustomerLookupViewController.instance()
                container.closeHandler = { _ in
                    self.closeHandler?()
                    popup.dismiss()
                    
                    
                }
                popup.show(childViewController: container)
            }
            else
            {
                self.NormalAlert(title: "Alert", Messsage: " You are Not Authorised for this option")
            }
        })
        
        
    }

    @IBAction func BodEntryAction(_ sender: AnyObject)
    {
      
        // let responsestring = LibraryAPI.sharedInstance.Parsejson1(url: "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=PABR&Option=23")
        self.getStringsuccess(params: [:], urlhit:  "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=PABR&Option=23", completion: { (jsonstr) in
            let responsestring  = jsonstr
         if(responsestring == "Y")
         {
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "BillingwithODCustomerViewController")), animated: true)
         self.sideMenuViewController!.hideMenuViewController()
            self.closeHandler?()
         
         }
         else
         {
            self.NormalAlert(title: "Alert", Messsage: " You are Not Authorised for this option")
       
         }
        })
          }

    @IBAction func EcbEntryAction(_ sender: AnyObject)
    {
        
       //  let responsestring = LibraryAPI.sharedInstance.Parsejson1(url: "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=PABR&Option=31")
        self.getStringsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=PABR&Option=31", completion: { (jsonstr) in
            let responsestring = jsonstr
            
            if(responsestring == "Y")
            {
                self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "ECBViewController")),animated: true)
                self.sideMenuViewController!.hideMenuViewController()
                self.closeHandler?()
                
            }
            else
            {
                self.NormalAlert(title: "Alert", Messsage: " You are Not Authorised for this option")
            }
        })
    }
    
    @IBAction func DcbEntryoption(_ sender: AnyObject)
    {
        
        // let responsestring = LibraryAPI.sharedInstance.Parsejson1(url: "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=PABR&Option=30")
        self.getStringsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=PABR&Option=30", completion: { (jsonstr) in
            let responsestring = jsonstr
            if(responsestring == "Y")
            {
                self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DBCViewController")),animated: true)
                self.sideMenuViewController!.hideMenuViewController()
                self.closeHandler?()
            }
            else
            {
                self.NormalAlert(title: "Alert", Messsage: " You are Not Authorised for this option")
               
            }
        })
        

    }
    
    //SpcoptionentryViewController
    @IBAction func spcentryoption(_ sender: AnyObject)
    {
         //let responsestring = LibraryAPI.sharedInstance.Parsejson1(url: "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=PABR&Option=1")
        self.getStringsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=PABR&Option=1", completion: { (jsonstr) in
             let responsestring = jsonstr
            if(responsestring == "Y")
            {
                self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "SpcoptionentryViewController")),animated: true)
                self.sideMenuViewController!.hideMenuViewController()
                self.closeHandler?()
            }
            else
            {
                self.NormalAlert(title: "Alert", Messsage: " You are Not Authorised for this option")
               
            }
        })
        
        
    }
    
    func notificationmethod()
    {
        let userdefaults = Foundation.UserDefaults.standard
        let value = userdefaults.dictionary(forKey: "Notification")
        
        var title: String!
        var message: String!
        var imagestr: String!
        

        title = value?["title"] as! String
        message = value?["body"] as! String
        imagestr = value?["imageurl"] as! String
        
        if imagestr != nil
        {
            if imagestr != ""
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: notificationimageViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = notificationimageViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                    
                }
                popup.show(childViewController: container)
            }
            else
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: PushnotificationtextViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = PushnotificationtextViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                }
                
                popup.show(childViewController: container)
            }
        }
        else
        {
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: PushnotificationtextViewController.instance())
                
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
            }
            let container = PushnotificationtextViewController.instance()
            container.closeHandler = { _ in
                popup.dismiss()
            }
            
            popup.show(childViewController: container)
        }
        
    }
    
    
   
   }
