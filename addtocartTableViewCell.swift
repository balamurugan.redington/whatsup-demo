//
//  addtocartTableViewCell.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 27/06/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class addtocartTableViewCell: UITableViewCell {

    @IBOutlet weak var itemcodelbl: UILabel!
    
    @IBOutlet weak var descriptionlbl: UILabel!
    
    @IBOutlet weak var qualitylbl: UILabel!
    
    @IBOutlet weak var vendorbplbl: UILabel!
    
    @IBOutlet weak var totallbl: UILabel!
    
    @IBOutlet weak var addtocartview: UIView!
    @IBOutlet weak var timelbl: UILabel!
    
    @IBOutlet weak var deletebtn: UIButton!
    @IBOutlet weak var unitpricelbl: UILabel!
    
    @IBOutlet weak var contentview: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
