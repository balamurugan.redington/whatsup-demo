//
//  branchesViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 12/07/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit
//import  NVActivityIndicatorView

class branchesViewController: UIViewController,PopupContentViewController,UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var BranchTableview: UITableView!
    @IBOutlet weak var allbranchbtn: UIButton!
    
    @IBOutlet weak var view1: UIView!
     var responsestring = NSMutableArray()
    var merge = String()
    
    
    var closeHandler: (() -> Void)?
  //  var cv:Customdelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//         BranchTableview.registerNib(UINib(nibName: "AllBranchesTableViewCell", bundle: nil), forCellReuseIdentifier: "AllBranchesTableViewCell")
       
        BranchTableview.separatorStyle = .singleLine
        BranchTableview.delegate = self
        BranchTableview.dataSource = self
        view1.layer.cornerRadius = view1.frame.size.width/2
        view1.clipsToBounds = true

          
        self.loaddata()
        
//        if(LibraryAPI.sharedInstance.BranchCodeCheck == 3)
//        {
//            allbranchbtn.hidden = true
//        
//        }
        if(LibraryAPI.sharedInstance.BranchCodeCheck == 4)
        {
           allbranchbtn.isUserInteractionEnabled = true
            
        }
  }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    class func instance() -> branchesViewController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "branchesViewController") as! branchesViewController
    }
    
    @IBAction func CloseAction(_ sender: AnyObject)
    {
        closeHandler?()
    }
    func sizeForPopup(popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: 220, height: 220)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return responsestring.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : AllBranchesTableViewCell = tableView.dequeueReusableCell(withIdentifier: "AllBranchesTableViewCell") as! AllBranchesTableViewCell
        //let cell = tableView.dequeueReusableCellWithIdentifier("AllBranchesTableViewCell") as! AllBranchesTableViewCell
        let dic = responsestring[indexPath.row] as! NSDictionary
        let BranchCode = dic.object(forKey: "BranchCode") as! String
        let BranchName =  dic.object(forKey: "BranchName") as! String
         merge = BranchCode + "-" + BranchName
       // Partnerprofile!+" "+"-"+profiledesc!+" "
        cell.branchnmaelbl.text = merge
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dic = responsestring[indexPath.row] as! NSDictionary
        if(LibraryAPI.sharedInstance.BranchCodeCheck == 1)
        {
            let BranchCode = dic.object(forKey: "BranchCode") as! String
            let BranchName =  dic.object(forKey: "BranchName") as! String
            let myvalue = BranchCode + "-" + BranchName
            
            //view360secondpopupname
            LibraryAPI.sharedInstance.view360secondpopupname = BranchCode
            LibraryAPI.sharedInstance.view360secondpopupstr = myvalue
            LibraryAPI.sharedInstance.Custbranchisbtnclicked = false
            self.closeHandler?()
        }
        if(LibraryAPI.sharedInstance.BranchCodeCheck == 2)
        {
            let BranchCode = dic.object(forKey: "BranchCode") as! String
            let BranchName =  dic.object(forKey: "BranchName") as! String
            _ = BranchCode + "-" + BranchName
            LibraryAPI.sharedInstance.view360secondpopupname = BranchCode
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "ChequeBounceDetailsViewController")), animated: true)
            self.sideMenuViewController!.hideMenuViewController()
           // selectedCell.contentView.backgroundColor = self.view.backgroundColor
            self.closeHandler?()
        }
        
        if(LibraryAPI.sharedInstance.BranchCodeCheck == 3)
        {
           
            LibraryAPI.sharedInstance.PartnerBranchCodetf = dic.object(forKey: "BranchCode") as! String
            let BranchName =  dic.object(forKey: "BranchName") as! String
            _ =  LibraryAPI.sharedInstance.PartnerBranchCodetf + "-" + BranchName
          
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "PartnerRegister1ViewController")), animated: true)
            self.sideMenuViewController!.hideMenuViewController()

            self.closeHandler?()
        }
        
        if(LibraryAPI.sharedInstance.BranchCodeCheck == 4)
        {
            
            LibraryAPI.sharedInstance.PartnerBranchCodetf = dic.object(forKey: "BranchCode") as! String
            let BranchName =  dic.object(forKey: "BranchName") as! String
            let myvalue =  LibraryAPI.sharedInstance.PartnerBranchCodetf + "-" + BranchName
            
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "Dashboard360NewViewController")), animated: true)
            self.sideMenuViewController!.hideMenuViewController()
            
            self.closeHandler?()
        }
       
        
    }
    
    func loaddata()
    {
        local_activityIndicator_start()
        // new version dispatch Que
        
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
           //  self.responsestring  = LibraryAPI.sharedInstance.RequestUrl(url: "\(LibraryAPI.sharedInstance.GlobalUrl)Users.asmx/BranchDetails?UserID=\(LibraryAPI.sharedInstance.Userid)")
            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Users.asmx/BranchDetails?UserID=\(LibraryAPI.sharedInstance.Userid)", completion: { (jsonmutable) in
                self.responsestring = jsonmutable
                DispatchQueue.main.async {
                    if(self.responsestring.count>0)
                    {
                        self.local_activityIndicator_stop()
                        self.BranchTableview.reloadData()
                        
                    }
                    else
                    {
                        self.local_activityIndicator_stop()
                        
                    }
                    
                }
            })
            // Go back to the main thread to update the UI
        }
    }
        // new version dispatch Que
        
     @IBAction func AllBranchAction(_ sender: AnyObject)
    {
        LibraryAPI.sharedInstance.Custbranchisbtnclicked = true
        if(LibraryAPI.sharedInstance.BranchCodeBtnCheck == 1)
        {
            if(LibraryAPI.sharedInstance.Custbranchisbtnclicked == true)
            {
               LibraryAPI.sharedInstance.view360btnaction = "All Branches"
      
            }
            else
            {
                
            }
            self.closeHandler?()
        }
        
        if(LibraryAPI.sharedInstance.BranchCodeBtnCheck == 2)
        {
            if(LibraryAPI.sharedInstance.Custbranchisbtnclicked == true)
            {
//                LibraryAPI.sharedInstance.view360btnaction = "All Branches"
                LibraryAPI.sharedInstance.view360secondpopupname = ""
                self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "ChequeBounceDetailsViewController")), animated: true)
                self.sideMenuViewController!.hideMenuViewController()
            }
            else
            {
                
            }
            self.closeHandler?()
        }
        
        
        if(LibraryAPI.sharedInstance.BranchCodeBtnCheck == 3)
        {
            if(LibraryAPI.sharedInstance.Custbranchisbtnclicked == true)
            {
                  LibraryAPI.sharedInstance.view360btnaction = "All Branches"
                LibraryAPI.sharedInstance.view360secondpopupname = "All Branches"
                self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "PartnerRegister1ViewController")), animated: true)
                self.sideMenuViewController!.hideMenuViewController()
            }
            else
            {
                
            }
            self.closeHandler?()
        }
        
    }

}
