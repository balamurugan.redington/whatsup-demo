//
//  AddTableViewCell.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 25/07/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class AddTableViewCell: UITableViewCell {

    @IBOutlet weak var lbl: UILabel!
    
    
    @IBOutlet weak var removebtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
