//
//  PopupwindowleaveViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 22/05/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class PopupwindowleaveViewController: UIViewController,PopupContentViewController {

    @IBOutlet weak var empno: UILabel!
    
    @IBOutlet weak var empnamelbl: UILabel!
    @IBOutlet weak var leaveappnolbl: UILabel!
   
    @IBOutlet weak var status: UILabel!
    
    @IBOutlet weak var fromdatelbl: UILabel!
    
    @IBOutlet weak var todatelbl: UILabel!
    
    @IBOutlet weak var noofdayslbl: UILabel!
    
    @IBOutlet weak var leavetypelbl: UILabel!
    
    @IBOutlet weak var reasonlbl: UILabel!
    
    @IBOutlet weak var prileavebal: UILabel!
    
    @IBOutlet weak var pridayslbl: UILabel!
    
    @IBOutlet weak var priballbl: UILabel!
    
    @IBOutlet weak var sickleavebal: UILabel!
    
    @IBOutlet weak var sickdays: UILabel!
    @IBOutlet weak var sickbalancelbl: UILabel!
    @IBOutlet weak var casualleavebal: UILabel!
    @IBOutlet weak var casualdayslbl: UILabel!
    @IBOutlet weak var casualballbl: UILabel!
    
    @IBOutlet weak var typesview: UIView!
    
    @IBOutlet weak var cancelbutton: UIButton!
    
    
    
     var responsestring = NSMutableArray()
    
    var uploadfile = NSMutableDictionary()
    var jsonresult = NSDictionary()
    
    
    var closeHandler: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.typesview.layer.borderWidth = 0.5
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationmethod), name: NSNotification.Name(rawValue: "GetNotification"), object: nil)

        
        cancelbutton.layer.masksToBounds=true
        cancelbutton.layer.cornerRadius=10.0
        self.cancelbutton.layer.borderWidth = 0.5
    
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.empno.text = String(LibraryAPI.sharedInstance.leaveappnumber.object(forKey: "Employee code") as! String)
        self.empnamelbl.text = String(LibraryAPI.sharedInstance.leaveappnumber.object(forKey: "Employee name") as! String)
        self.leaveappnolbl.text = String(LibraryAPI.sharedInstance.leaveappnumber.object(forKey: "Leave Appl Number") as! String)
        self.status.text = String(LibraryAPI.sharedInstance.leaveappnumber.object(forKey: "Status") as! String)
        self.empno.text = String(LibraryAPI.sharedInstance.leaveappnumber.object(forKey: "Employee code") as! String)
        self.fromdatelbl.text = String(LibraryAPI.sharedInstance.leaveappnumber.object(forKey: "From Date") as! String)
        self.todatelbl.text = String(LibraryAPI.sharedInstance.leaveappnumber.object(forKey: "To Date") as! String)
        self.noofdayslbl.text = String(describing: LibraryAPI.sharedInstance.leaveappnumber.object(forKey: "No of Days") as! NSNumber)
        self.leavetypelbl.text = String(LibraryAPI.sharedInstance.leaveappnumber.object(forKey: "Leave Type") as! String)
        self.reasonlbl.text = String(LibraryAPI.sharedInstance.leaveappnumber.object(forKey: "Reason") as! String)
         self.prileavebal.text = String( LibraryAPI.sharedInstance.leaveappnumber.object(forKey: "Privilege Leave") as! Double)
        self.pridayslbl.text = String(LibraryAPI.sharedInstance.leaveappnumber.object(forKey: "priv DaysAppropiate") as! Double)
         self.priballbl.text = String(LibraryAPI.sharedInstance.leaveappnumber.object(forKey: "Privilege  Balance") as! Double)
        self.sickleavebal.text = String(LibraryAPI.sharedInstance.leaveappnumber.object(forKey: "Sick Leave") as! Double)
        self.sickdays.text = String(LibraryAPI.sharedInstance.leaveappnumber.object(forKey: "Sick DaysAppropiate") as! Double)
         self.sickbalancelbl.text = String(LibraryAPI.sharedInstance.leaveappnumber.object(forKey: "Sick Balance") as! Double)
         self.casualleavebal.text = String(LibraryAPI.sharedInstance.leaveappnumber.object(forKey: "Casual leave") as! Double)
        self.casualdayslbl.text = String(LibraryAPI.sharedInstance.leaveappnumber.object(forKey: "Casul DaysAppropiate") as! Double)
        self.casualballbl.text = String(LibraryAPI.sharedInstance.leaveappnumber.object(forKey: "Casual Balance") as! Double)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    class func instance() -> PopupwindowleaveViewController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "PopupwindowleaveViewController") as! PopupwindowleaveViewController
    }
    
    
    @IBAction func CloseAction(_ sender: AnyObject)
    {
        
        closeHandler?()
    }
    
    func sizeForPopup(popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
              return CGSize(width: 300, height: 500)
    }
    
    
    
    
    @IBAction func CancelbtnAction(_ sender: AnyObject)
    {
        self.local_activityIndicator_start()
       uploadfile.setObject(LibraryAPI.sharedInstance.Userid.stringByRemovingWhitespaces, forKey: "User" as NSCopying)
        uploadfile.setObject(String(LibraryAPI.sharedInstance.leaveappnumber.object(forKey: "Leave Appl Number") as! String), forKey: "LeaveApplNumber" as NSCopying)
        cancel()
    }
    
    func cancel()
    {
        //http://edi.redingtonb2b.in/Whatsup-staging/LeaveDetails.asmx/LeaveCancel
        
        let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)LeaveDetails.asmx/LeaveCancel")! as URL)
        request.httpMethod = "POST"
        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
        request.httpBody = try? JSONSerialization.data(withJSONObject: self.uploadfile, options: JSONSerialization.WritingOptions(rawValue: 0))
        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
        //  var err: NSError
        do
        {
            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
            jsonresult = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            if(jsonresult.count > 0)
            {
                let message = jsonresult.object(forKey: "Message1")as? String
                let message2 = jsonresult.object(forKey: "Message2")as? String
                
                if (message) != ""
                {
                    self.local_activityIndicator_stop()
                    let alertController = UIAlertController(title: "Alert", message: "\(self.jsonresult.object(forKey: "Message1")!)" , preferredStyle: .alert)
                    
                    let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                        self.closeHandler?()
                    }
                    alertController.addAction(cancelAction)
                    
                    
                    self.present(alertController, animated: true, completion:nil)
                }
                else
                {
                    self.local_activityIndicator_stop()
                }
                
            }
            
        }
        catch (let e)
        {
            
        }
        
    }
    
    
    func notificationmethod()
    {
        let userdefaults = Foundation.UserDefaults.standard
        let value = userdefaults.dictionary(forKey: "Notification")
        
        var title: String!
        var message: String!
        var imagestr: String!
        
              title = value?["title"] as! String
        message = value?["body"] as! String
        imagestr = value?["imageurl"] as! String
        
        if imagestr != nil
        {
            if imagestr != ""
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: notificationimageViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = notificationimageViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                    
                }
                popup.show(childViewController: container)
            }
            else
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: PushnotificationtextViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = PushnotificationtextViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                }
                
                popup.show(childViewController: container)
            }
        }
        else
        {
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: PushnotificationtextViewController.instance())
                
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
            }
            let container = PushnotificationtextViewController.instance()
            container.closeHandler = { _ in
                popup.dismiss()
            }
            
            popup.show(childViewController: container)
        }
    }

}
