//
//  TicketissueTableViewCell.swift
//  Redington
//
//  Created by RIL-ITTECH on 01/03/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class TicketissueTableViewCell: UITableViewCell {

    @IBOutlet weak var ticketnumber: UILabel!
    
    @IBOutlet weak var ticketstatus: UILabel!
    @IBOutlet weak var ticketdate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
