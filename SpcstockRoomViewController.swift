//
//  SpcstockRoomViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 09/06/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class SpcstockRoomViewController: UIViewController,PopupContentViewController,UITableViewDelegate, UITableViewDataSource {
    var responsestring = NSMutableArray()
    
    @IBOutlet weak var stockroomtableview: UITableView!
    var  response1 = NSMutableArray()
    var closeHandler: (() -> Void)?
    var responsearray1=NSMutableArray()
    var pickerarray1=NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationmethod), name: NSNotification.Name(rawValue: "GetNotification"), object: nil)
        
        stockroomtableview.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        stockroomtableview.delegate = self
        stockroomtableview.dataSource = self
        
        bizcodeRec1()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    class func instance() -> SpcstockRoomViewController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "SpcstockRoomViewController") as! SpcstockRoomViewController
    }
    
    @IBAction func CloseAction(_ sender: AnyObject)
    {
        closeHandler?()
    }
    
    
    func sizeForPopup(popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        
        return CGSize(width: 180, height: 220)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return LibraryAPI.sharedInstance.leaveappno.count
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let dicval = responsearray1[indexPath.row] as! NSDictionary
        cell.textLabel?.text = String(dicval.object(forKey: "StockRoom Code") as! String)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    func Loaddata1(url:String)
    {
        // responsearray1 = LibraryAPI.sharedInstance.RequestUrl(url: url)
        self.getsuccess(params: [:], urlhit: url, completion:  { (jsonmutable) in
            self.responsearray1 = jsonmutable
            if(self.responsearray1.count > 0)
            {
                
            }
            self.pickerarray1.removeAllObjects()
            for i in 0..<self.responsearray1.count
            {
                //  if(currenttf=="business")
                // {
                let dicres = self.responsearray1[i] as! NSDictionary
                let code = dicres.object(forKey: "StockRoom Code") as? String
                let desc = dicres.object(forKey: "StockRoom Name") as? String
                let str=code?.appending(" - \(desc!)")
                self.pickerarray1.add(str!)
                
                // }
            }
        }) 
    }
    
    
    
    func bizcodeRec1()
    {
        //let size = CGSize(width: 30, height:30)
        // new version dispatch Que
        
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
            self.Loaddata1(url: "\(LibraryAPI.sharedInstance.GlobalUrl)customer.asmx/Stockroomsearch_SPC?Userid=\(LibraryAPI.sharedInstance.Userid)")
            
            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
                
                
            }
        }
        
        // new version dispatch Que
        
        /*  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
         
         
         //   http://edi.redingtonb2b.in/Whatsup-staging/customer.asmx/Stockroomsearch_SPC?Userid=SENTHILKS
         
         self.Loaddata1("\(LibraryAPI.sharedInstance.GlobalUrl)customer.asmx/Stockroomsearch_SPC?Userid=\(LibraryAPI.sharedInstance.Userid)")
         
         dispatch_async(dispatch_get_main_queue(), {
         })
         })*/
    }
    
    
    func notificationmethod()
    {
        let userdefaults = Foundation.UserDefaults.standard
        let value = userdefaults.dictionary(forKey: "Notification")
        
        var title: String!
        var message: String!
        var imagestr: String!
        
        title = value?["title"] as! String
        message = value?["body"] as! String
        imagestr = value?["imageurl"] as! String
        
        if imagestr != nil
        {
            if imagestr != ""
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: notificationimageViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = notificationimageViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                    
                }
                popup.show(childViewController: container)
            }
            else
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: PushnotificationtextViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = PushnotificationtextViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                }
                
                popup.show(childViewController: container)
            }
        }
        else
        {
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: PushnotificationtextViewController.instance())
                
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
            }
            let container = PushnotificationtextViewController.instance()
            container.closeHandler = { _ in
                popup.dismiss()
            }
            
            popup.show(childViewController: container)
        }
    }
    
    
    
    
}
