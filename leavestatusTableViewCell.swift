//
//  leavestatusTableViewCell.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 22/05/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class leavestatusTableViewCell: UITableViewCell
{

    @IBOutlet weak var appno: UILabel!
    @IBOutlet weak var fromdate: UILabel!
    @IBOutlet weak var todate: UILabel!
    @IBOutlet weak var noofdays: UILabel!
    @IBOutlet weak var leavetype: UILabel!
    
    
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
