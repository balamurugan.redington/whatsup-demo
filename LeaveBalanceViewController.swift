//
//  LeaveBalanceViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 18/07/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class LeaveBalanceViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var SecondView: UIView!
    @IBOutlet weak var FirstView: UIView!
    @IBOutlet weak var yearlbl: UILabel!
    @IBOutlet weak var monthtf: UITextField!
    @IBOutlet weak var openpriviledgelbl: UILabel!
    @IBOutlet weak var opnsicklbl: UILabel!
    @IBOutlet weak var opencasuallbl: UILabel!
    @IBOutlet weak var totalpriviledgelbl: UILabel!
    @IBOutlet weak var totalsicklbl: UILabel!
    @IBOutlet weak var totalcasuallbl: UILabel!
    @IBOutlet weak var closingpriviledgelbl: UILabel!
    @IBOutlet weak var closingsicklbl: UILabel!
    @IBOutlet weak var closing: UILabel!
    @IBOutlet weak var leavedetailsbtn: UIButton!
    @IBOutlet weak var viewtimedetails: UIButton!
    @IBOutlet weak var loplbl: UILabel!
    var buttonstr : String = ""
    var totalmonth = NSMutableArray()
    var responseleavebalance = NSMutableDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("LeaveBalanceViewController")
        if LibraryAPI.sharedInstance.Globalflag == "N" {
            customnavigation1()
        } else {
            customnavigation()
        }
        monthtf.delegate = self
        monthtf.tag = 1
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        FirstView.layer.masksToBounds=true
        FirstView.layer.cornerRadius=5
        SecondView.layer.masksToBounds=true
        SecondView.layer.cornerRadius=5
        leavedetailsbtn.layer.cornerRadius = 5.0
        leavedetailsbtn.layer.borderWidth = 1.0
        self.leavedetailsbtn.layer.borderColor = UIColor(red:211/255.0, green:211/255.0, blue:211/255.0, alpha: 1.0).cgColor
        viewtimedetails.layer.cornerRadius = 5.0
        viewtimedetails.layer.borderWidth = 1.0
        self.viewtimedetails.layer.borderColor = UIColor(red:211/255.0, green:211/255.0, blue:211/255.0, alpha: 1.0).cgColor
        yearlbl.text = String(describing: BioMatric_Arr.last!.YEAR)
        openpriviledgelbl.text = String(describing: BioMatric_Arr.last!.PERSONAL_LEAVE)
        opnsicklbl.text = String(describing: BioMatric_Arr.last!.SICK_LEAVE)
        opencasuallbl.text = String(describing: BioMatric_Arr.last!.CASUAL_LEAVE_BAL)
        totalpriviledgelbl.text = String(describing: BioMatric_Arr.last!.PL_TAKEN)
        totalsicklbl.text = String(describing: BioMatric_Arr.last!.SL_TAKEN)
        totalcasuallbl.text = String(describing: BioMatric_Arr.last!.CL_TAKEN)
        closingpriviledgelbl.text = String(describing: BioMatric_Arr.last!.PL_CLOSING)
        closingsicklbl.text  = String(describing: BioMatric_Arr.last!.SL_CLOSING)
        closing.text =  String(describing: BioMatric_Arr.last!.CL_CLOSING)
        loplbl.text = String(describing: BioMatric_Arr.last!.LOP_LEAVE)
        monthtf.text = BioMatric_Arr.last!.MONTH
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        defaultmethod()
        buttonstr = " "
    }
    
    func customnavigation() {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width, height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(LeaveBalanceViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Attendence/LeaveBalance"
    }
    
    func customnavigation1() {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        //customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), forState: UIControlState.Normal)
        customView.MenuBtn.addTarget(self, action: #selector(LeaveBalanceViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        //customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),forControlEvents:UIControlEvents.TouchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.textAlignment = .center
        customView.HeaderLabel.font = UIFont.boldSystemFont(ofSize: 16.0)
        customView.HeaderLabel.text = "Attendence/LeaveBalance"
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if(textField.tag == 1) {
            let alert:UIAlertController=UIAlertController(title: "Select Month", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            var alert1 = UIAlertAction()
            for word in BioMatric_Arr {
                alert1 = UIAlertAction(title: word.MONTH, style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    self.yearlbl.text = String(describing: word.YEAR)
                    self.openpriviledgelbl.text = String(describing: word.PERSONAL_LEAVE)
                    self.opnsicklbl.text = String(describing: word.SICK_LEAVE)
                    self.opencasuallbl.text = String(describing: word.CASUAL_LEAVE_BAL)
                    self.totalpriviledgelbl.text = String(describing: word.PL_TAKEN)
                    self.totalsicklbl.text = String(describing: word.SL_TAKEN)
                    self.totalcasuallbl.text = String(describing: word.CL_TAKEN)
                    self.closingpriviledgelbl.text = String(describing: word.PL_CLOSING)
                    self.closingsicklbl.text  = String(describing: word.SL_CLOSING)
                    //  self.closing.text = = String(word.CL_CLOSING)
                    self.loplbl.text = String(describing: word.LOP_LEAVE)
                    self.monthtf.text = word.MONTH
                }
                alert.addAction(alert1)
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                UIAlertAction in
            }
            // Add the actions
            //picker?.delegate = self
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
            return false
        }
        return true
    }
    
    func buttonClicked(sender:UIButton) {
        // self.navigationController?.popViewControllerAnimated(true)
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "HRD1DetailsViewController")), animated: true)
    }
    
    func defaultmethod() {
        self.local_activityIndicator_start()
        let monthvalue = monthtf.text!
        let monthindex = currentmonth(word: monthvalue)
        var year = String()
        if(Int(monthindex) == 01 || Int(monthindex) == 02 || Int(monthindex) == 03) {
            year = self.yearlbl.text!
        } else {
            let value = Int(self.yearlbl.text!)! - 1
            year = String(value)
        }
        self.getjsonMutableDictionarysuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)LeaveDetails.asmx/LeaveBalance?UserID=\(LibraryAPI.sharedInstance.Userid.stringByRemovingWhitespaces)&Month=\(monthindex)&Year=\(year)" , completion: { (jsonmudic) in
            self.responseleavebalance = jsonmudic
            if(self.responseleavebalance.count > 0) {
                LeaveBalance_Month_Arr = [LeaveBalance_Month]()
                Biomatric_Time_Arr = [Biomatric_Time]()
                for data in self.responseleavebalance.object(forKey: "LeaveBalance_Month") as! NSArray {
                    LeaveBalance_Month_Arr.append(LeaveBalance_Month(data: data as AnyObject))
                }
                for data in self.responseleavebalance.object(forKey: "Biomatric") as! NSArray {
                    Biomatric_Time_Arr.append(Biomatric_Time(data: data as AnyObject))
                }
                if self.buttonstr == "leavedetails" {
                    print("leave details button pressed")
                    if LeaveBalance_Month_Arr.count > 0 {
                        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "LeaveBalanceListViewController")), animated: true)
                    } else {
                        let alert = UIAlertController(title: "Sorry..!!", message: "No Data Available", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    self.buttonstr = ""
                } else if self.buttonstr == "viewtimedetails" {
                     print("view time details button pressed")
                    if Biomatric_Time_Arr.count > 0 {
                        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "TimeBalanceViewController")), animated: true)
                    } else {
                        let alert = UIAlertController(title: "Sorry..!!", message: "No Data Available", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    self.buttonstr = ""
                } else {
                }
                 self.local_activityIndicator_stop()
            } else {
                let alert = UIAlertController(title: "Sorry..!!", message: "No Data Available", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                self.local_activityIndicator_stop()
            }
        })
    }
    
    @IBAction func leavedetailsbtnaction(_ sender: AnyObject) {
        defaultmethod()
        buttonstr = "leavedetails"
    }
    
    @IBAction func viewtimedetailsAction(_ sender: AnyObject) {
        defaultmethod()
        buttonstr = "viewtimedetails"
    }
    
    func currentmonth(word: String) -> String {
        let word = word.lowercased()
        let str  = String(word.prefix(3))
        if(str == "jan") {
            return "01"
        } else if(str == "feb") {
            return "02"
        } else if(str == "mar") {
            return "03"
        } else if(str == "apr") {
            return "04"
        } else if(str == "may") {
            return "05"
        } else if(str == "jun") {
            return "06"
        } else if(str == "jul") {
            return "07"
        } else if(str == "aug") {
            return "08"
        } else if(str == "sep") {
            return "09"
        } else if(str == "oct") {
            return "10"
        } else if(str == "nov") {
            return "11"
        } else {
            return "12"
        }
    }
}

extension Character {
    public func isUpper() -> Bool {
        let characterString = String(self)
        return (characterString == characterString.uppercased()) && (characterString != characterString.lowercased())
    }
}


