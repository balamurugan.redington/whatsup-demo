	//
//  CitylookupViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 07/06/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit


class CitylookupViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var citylookup: UITableView!
    
    var responsearray=NSMutableArray()
    
    var cv:Customdelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.Customnavigation()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationmethod), name: NSNotification.Name(rawValue: "GetNotification"), object: nil)

        
        citylookup.tableFooterView = UIView(frame: .zero)
        citylookup.separatorStyle = .none
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled=false
        self.navigationController?.setNavigationBarHidden(true, animated: false)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
      //  responsearray.removeAllObjects()
        
        responsearray = LibraryAPI.sharedInstance.Cityearray
        
        citylookup.reloadData()
    }
   
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func Customnavigation()
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 65);
        
        customView.Notificationbtn.isHidden=true
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        self.view.addSubview(customView.view)
        
        
        customView.HeaderLabel.text = "Lookup"
        
        customView.MenuBtn.addTarget(self, action: #selector(CitylookupViewController.backbuttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        
    }
    
    func backbuttonClicked(sender:UIButton)
    {
        if(LibraryAPI.sharedInstance.Currentcustomerlookup == "City")
        {
            self.navigationController?.popViewController(animated: true)
        }
            
        else
        {
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    
    
    
    
    
    
    //TableView Delegates
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        if(responsearray.count > 0)
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return responsearray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell : CityLookupTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "CityLookupTableViewCell") as! CityLookupTableViewCell
        if(cell == nil)
        {
            cell = Bundle.main.loadNibNamed("CityLookupTableViewCell", owner: self, options: nil)![0] as! CityLookupTableViewCell;
            
        }
        
        
        if(indexPath.row%2==0)
        {
            cell.contentView.backgroundColor=UIColor.white
        }
        else
        {
            cell.contentView.backgroundColor=(UIColor.init(red: 244/255.0, green: 242/255.0, blue: 242/255.0, alpha: 1.0))
            
        }
        let dic = responsearray[indexPath.row] as! NSDictionary
        cell.citymode.text = dic.object(forKey: "Mode") as? String
        cell.cityname.text = dic.object(forKey: "Detail") as? String
        
        cell.selectionStyle = .none
        return cell as CityLookupTableViewCell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        // cv?.Passvaluefrompop((responsearray.objectAtIndex(indexPath.row).objectForKey("Detail") as? String)!)
        let dic1 = responsearray[indexPath.row] as! NSDictionary
        LibraryAPI.sharedInstance.Popupgetstring = dic1.object(forKey: "Detail") as? String
        
       NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PopupNotification"), object: nil)
    
        
        if(LibraryAPI.sharedInstance.Currentcustomerlookup=="City")
        {
           
            self.navigationController?.popViewController(animated: true)
            
        }
            
        else
        {
            self.dismiss(animated: true, completion: nil)
        }
        
        
    }
    
    func notificationmethod()
    {
        let userdefaults = Foundation.UserDefaults.standard
        let value = userdefaults.dictionary(forKey: "Notification")
        
        var title: String!
        var message: String!
        var imagestr: String!
  
        title = value?["title"] as! String
        message = value?["body"] as! String
        imagestr = value?["imageurl"] as! String
        
        if imagestr != nil
        {
            if imagestr != ""
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: notificationimageViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = notificationimageViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                    
                }
                popup.show(childViewController: container)
            }
            else
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: PushnotificationtextViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = PushnotificationtextViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                }
                
                popup.show(childViewController: container)
            }
        }
        else
        {
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: PushnotificationtextViewController.instance())
                
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
            }
            let container = PushnotificationtextViewController.instance()
            container.closeHandler = { _ in
                popup.dismiss()
            }
            
            popup.show(childViewController: container)
        }
        
             
    }
    
    
    
}
