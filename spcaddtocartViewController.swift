//
//  spcaddtocartViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 29/05/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
 import KCFloatingActionButton

class spcaddtocartViewController: UIViewController,KCFloatingActionButtonDelegate,UITextFieldDelegate  {
    var fab=KCFloatingActionButton()

    @IBOutlet weak var Scrollview: UIScrollView!
    @IBOutlet weak var mainview: UIView!
    @IBOutlet weak var validitytf: UITextField!
    @IBOutlet weak var AddtoCartbtn: UIButton!
    @IBOutlet weak var titleview: UIView!
    @IBOutlet weak var submitaction: UIButton!
    @IBOutlet weak var customercodetf: UITextField!
    @IBOutlet weak var itemtf: UITextField!
    @IBOutlet weak var customernametf: UITextField!
    @IBOutlet weak var qualitytf: UITextField!
    @IBOutlet weak var vendorbidtf: UITextField!
    @IBOutlet weak var qrcamttf: UITextField!
    @IBOutlet weak var unitpricetf: UITextField!
    @IBOutlet weak var cashdiscounttf: UITextField!
    @IBOutlet weak var discountlbl: UILabel!
    @IBOutlet weak var addtionalamttf: UITextField!
    @IBOutlet weak var endcustnametf: UITextField!
    @IBOutlet weak var paymentdaystf: UITextField!
    @IBOutlet weak var frieghtyesbtn: UIButton!
    @IBOutlet weak var frieghtnobtn: UIButton!
    @IBOutlet weak var multiinvyesbtn: UIButton!
    @IBOutlet weak var multiinvnobtn: UIButton!
    @IBOutlet weak var freebieyesbtn: UIButton!
    @IBOutlet weak var freebienobtn: UIButton!
    @IBOutlet weak var AddCountBtn: UIButton!
    @IBOutlet weak var countlabel: UILabel!
    @IBOutlet weak var addtionalamountlbl: UILabel!
    @IBOutlet weak var eopnumberlbl: UILabel!
    @IBOutlet weak var referrencenumberlbl: UILabel!
    @IBOutlet weak var eopnumbertf: UITextField!
    @IBOutlet weak var referencenumbertf: UITextField!
    @IBOutlet weak var Poslabel: UILabel!
    @IBOutlet weak var postxtfield: UITextField!
    
    var Custcode : String!
    var endcustname : String!
    var multiinvallowed : String!
    var endcustponumber : String!
    var validitydate : String!
    var frieght : String!
    var payment : String!
    var Customercategory : String!
    var DISTIDISC_out1 : String!
    var DISTIDISC_out : String!
    var ItemDescription_out : String!
    var price : String!
    var uploadfile=NSMutableDictionary()
    var jsonresult=NSDictionary()
    var responsearray2=NSMutableArray()
    var pickerarray2=NSMutableArray()
    var checkchar : String!
    var checkchar1 : String!
    var checkchar2:String!
    var addtocartdic=NSMutableDictionary()
    var addcararray = NSMutableArray()
    var firstTimePressed : Bool = false
    var Flag = String()
    
   
    override func viewDidLoad()
    {
        super.viewDidLoad()
       // customnavigation()
        countlabel.layer.cornerRadius=self.countlabel.frame.size.width/2
        countlabel.layer.masksToBounds = true
        countlabel.text = String(LibraryAPI.sharedInstance.globalarray.count)
        LibraryAPI.sharedInstance.spcstr = "01"
        self.Scrollview.contentSize = mainview.frame.size
        self.layoutFAB()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        AddtoCartbtn.layer.masksToBounds=true
        self.submitaction.isHidden = true
        
        let arrow = UIImageView()
        let image = UIImage(named: "cal.png")
        arrow.image = image!.imageWithColor(tintColor: UIColor.init(red:239/255.0 , green: 88/255.0, blue: 88/255.0, alpha: 1.0)).withRenderingMode(.alwaysOriginal)
        arrow.frame = CGRect(x: 0.0,y: 0.0,width: arrow.image!.size.width+10.0,height: 15);
        arrow.contentMode = UIViewContentMode.center
        self.validitytf.rightView = arrow;
        self.validitytf.rightViewMode = UITextFieldViewMode.always
        
        let defaults1 = UserDefaults.standard
        let  getdata:NSDictionary = defaults1.dictionary(forKey: "defaultdetails")! as NSDictionary
        
           
        Custcode = getdata.object(forKey: "customercodedefault")as! String
        price = getdata.object(forKey: "paymentdaysdefault")as! String
        endcustname = getdata.object(forKey: "endcustomernaedefault")as! String
        validitydate = getdata.object(forKey: "validatedefault") as! String
        multiinvallowed = getdata.object(forKey: "multidefault") as! String
        frieght = getdata.object(forKey: "frieghtdefault")as! String
        
        
        customercodetf.text = Custcode
        customernametf.text = LibraryAPI.sharedInstance.spccustname
        endcustnametf.text = endcustname

        self.validitytf.text = validitydate
        
        paymentdaystf.text = price
        postxtfield.isHidden = true
        Poslabel.isHidden = true
        
        checkchar2 = "N"
        
        GlobalDic()
        
        //delegates
        customercodetf.delegate = self
        customernametf.delegate = self
        itemtf.delegate = self
        vendorbidtf.delegate = self
        qualitytf.delegate = self
        qrcamttf.delegate = self
        unitpricetf.delegate = self
        cashdiscounttf.delegate = self
        endcustnametf.delegate = self
        validitytf.delegate = self
        paymentdaystf.delegate = self
        addtionalamttf.delegate = self
        postxtfield.delegate = self
        
        //tags
        customercodetf.tag = 1
        customernametf.tag = 2
        itemtf.tag = 3
        vendorbidtf.tag = 4
        qualitytf.tag = 5
        qrcamttf.tag = 6
        unitpricetf.tag = 7
        cashdiscounttf.tag = 8
        endcustnametf.tag = 9
        validitytf.tag = 10
        paymentdaystf.tag = 11
        addtionalamttf.tag = 12
        postxtfield.tag = 13
        
        self.cityrec()
               
        addtionalamountlbl.isHidden = true
        addtionalamttf.isHidden = true
        
        let defaults = UserDefaults.standard
        let Bcode = defaults.string(forKey: "UserBusinesscode")
        
        //let businesscode : [String] = Bcode!.componentsSeparatedByString(" - ")
        let businesscode : [String] = Bcode!.components(separatedBy: " - ")
        let businesscodestr : String = businesscode[0]

        self.addDoneButtonOnKeyboard()
    
        if(businesscodestr == "159")
        {
            addtionalamountlbl.isHidden = false
            addtionalamttf.isHidden = false
        }
        
        
        eopnumbertf.isHidden = true
        eopnumberlbl.isHidden = true
        referrencenumberlbl.isHidden = true
        referencenumbertf.isHidden = true
        
        qualitytf.keyboardType = .decimalPad
        unitpricetf.keyboardType = .decimalPad
        vendorbidtf.keyboardType = .decimalPad
        qrcamttf.keyboardType = .decimalPad
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    
    func addDoneButtonOnKeyboard()
        
    {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0,y:  0,width: 320,height: 50))
        
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        
        
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(spcaddtocartViewController.doneButtonAction))
        
        
        
        let items = NSMutableArray()
        
        items.add(flexSpace)
        
        items.add(done)
        
        
        
        doneToolbar.items = items as AnyObject as? [UIBarButtonItem]
        
        doneToolbar.sizeToFit()
        
        
        qrcamttf.inputAccessoryView = doneToolbar
        qualitytf.inputAccessoryView = doneToolbar
        unitpricetf.inputAccessoryView = doneToolbar
        vendorbidtf.inputAccessoryView = doneToolbar
        addtionalamttf.inputAccessoryView = doneToolbar

        
    }
    
    func doneButtonAction()
    {
        qualitytf.resignFirstResponder()
        unitpricetf.resignFirstResponder()
        vendorbidtf.resignFirstResponder()
        qrcamttf.resignFirstResponder()
        addtionalamttf.resignFirstResponder()
     }


    override func viewWillAppear(_ animated: Bool)
    {
        
         freebienobtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
        
        if( multiinvallowed == "Y")
        {
            multiinvyesbtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
            multiinvnobtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        }
        else
            
        {
            multiinvyesbtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
            multiinvnobtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
        }
        if(frieght == "Y")
        {
            frieghtyesbtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
            frieghtnobtn.setImage(UIImage(named:"normal.png"),  for: UIControlState.normal)
        }
        else
        {
            frieghtyesbtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
            frieghtnobtn.setImage(UIImage(named:"green2.png"),  for: UIControlState.normal)
        }
        
        itemcode()

    }
    
    
    @IBAction func backbuttonAction(_ sender: AnyObject)
    {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "SpcoptionentryViewController")), animated: true) 
    }
    
    
    
    
    func itemcode()
     {
       if (LibraryAPI.sharedInstance.itemcodesoc != nil)
       {
        if(LibraryAPI.sharedInstance.itemcodesoc == "")
        {
            self.itemtf.text = ""
            showToast(message: "No EOP Exists for this Item. Contact Busniess")
        }
        else
        {
            self.itemtf.text = LibraryAPI.sharedInstance.itemcodesoc
            defaultcheck(FDP1: "", APOS1: "0")
            if(Flag == "Y")
            {
                postxtfield.isHidden = false
                 Poslabel.isHidden = false
            }
            else if(Flag == "N")
            {
                 postxtfield.isHidden = true
                 Poslabel.isHidden = true
                showToast(message: "")
            }
            else if(Flag == "E")
            {
                 postxtfield.isHidden = true
                 Poslabel.isHidden = true
                showToast(message: "")
            }
        }
        
        GlobalDic()
     }
    
    }
    
 func display()
 {
    
    let str = String(describing: LibraryAPI.sharedInstance.select_ciscopopup.object(forKey: "reference") as!  NSNumber!)
    eopnumbertf.text! = str
    referencenumbertf.text = LibraryAPI.sharedInstance.select_ciscopopup.object(forKey: "eop") as?  String
    
 }
    
  /*  func showalert(Title:String,Message:String)
    {
        
        let alert = UIAlertController(title: Title, message: Message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.Default, handler:
            { (ACTION :UIAlertAction!)in
                
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }*/


    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            
            LibraryAPI.sharedInstance.globaldic.removeAllObjects()
            
            self.fab.close()
        }
        
        fab.fabDelegate = self
        
        self.view.addSubview(fab)
        
    }
    func KCFABOpened(fab: KCFloatingActionButton) {
    }

    func GlobalDic()
    {
        LibraryAPI.sharedInstance.globaldic.setObject(customernametf.text!, forKey: "customername" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(itemtf.text!, forKey: "itemcode" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(qualitytf.text!, forKey: "quantity" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(unitpricetf.text!, forKey: "unitprice" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(vendorbidtf.text!, forKey: "vendorbid" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(qrcamttf.text!, forKey: "orcamt" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(cashdiscounttf.text!, forKey: "cashdiscount" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(checkchar2, forKey: "freebie" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(addtionalamttf.text!, forKey:"additionalamt" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(eopnumbertf.text!, forKey:"eopnumber" as NSCopying)
       
    }
    
    
    func defaultcheck(FDP1 : String, APOS1 : String )
    {
        let Pos = Double(APOS1)
        let defaults = UserDefaults.standard
        let Bcode = defaults.string(forKey: "UserBusinesscode")
        
        var str : String = ""
        var cat : String = ""
        var cat1 : String = ""
        
        if self.Customercategory != nil
        {
            if self.Customercategory != ""
            {
                str = self.Customercategory
            }
            else
            {
                str = ""
            }
        }
        
        if self.DISTIDISC_out != nil
        {
            if self.DISTIDISC_out != ""
            {
                 cat = self.DISTIDISC_out
            }
            else
            {
                cat = ""
            }
        }
        
        if self.DISTIDISC_out1 != nil
        {
            if self.DISTIDISC_out1 != ""
            {
                cat1 = self.DISTIDISC_out1
            }
            else
            {
                cat1 = ""
            }
        }
        
        if endcustnametf.text! == ""
        {
            endcustnametf.text = "NA"
        }
        
        self.uploadfile.setObject(customercodetf.text!, forKey: "Customercode" as NSCopying)
        self.uploadfile.setObject(LibraryAPI.sharedInstance.priceApptype, forKey: "Type" as NSCopying )
        self.uploadfile.setObject(Bcode!, forKey: "businesscode" as NSCopying)
        self.uploadfile.setObject(str, forKey: "Customercategory" as NSCopying)
        self.uploadfile.setObject("", forKey: "lenovoflag" as NSCopying)
        self.uploadfile.setObject("", forKey: "ciscoflg" as NSCopying)
        self.uploadfile.setObject("", forKey: "flag2" as NSCopying)
        self.uploadfile.setObject(cat, forKey: "DISTIDISC" as NSCopying)
        self.uploadfile.setObject(itemtf.text!, forKey: "itemcode" as NSCopying)
        self.uploadfile.setObject(vendorbidtf.text!, forKey: "VENDORBIDPRICE" as NSCopying)
        self.uploadfile.setObject(qualitytf.text!, forKey: "Quantity" as NSCopying)
        self.uploadfile.setObject(qrcamttf.text!, forKey: "ORCAMOUNT" as NSCopying)

         self.uploadfile.setObject(qrcamttf.text!, forKey: "ORCDiscount" as NSCopying)
         self.uploadfile.setObject(unitpricetf.text!, forKey: "UnitPrice" as NSCopying)
         self.uploadfile.setObject(cashdiscounttf.text!, forKey: "Cashdiscount" as NSCopying)
         self.uploadfile.setObject(multiinvallowed, forKey: "Multipleinvoice" as NSCopying)
        
         self.uploadfile.setObject(checkchar2, forKey: "freebie" as NSCopying)
         self.uploadfile.setObject(endcustnametf.text!, forKey: "endcustomername" as NSCopying)
         self.uploadfile.setObject("", forKey: "ADDAMOUNTLENOVO" as NSCopying)
         self.uploadfile.setObject(LibraryAPI.sharedInstance.Username, forKey: "UserName" as NSCopying)
         self.uploadfile.setObject(cat1, forKey: "DISTIDISC1" as NSCopying)
         self.uploadfile.setObject(frieght, forKey: "FreightPre_paid" as NSCopying)
         self.uploadfile.setObject(FDP1, forKey: "ApplePOSID" as NSCopying)
         self.uploadfile.setObject(Pos!, forKey: "FDPercent" as NSCopying)
        
        self.uploaddata()
        //changes of 07/02/2018 Remove Double Value and Conversion
        if self.jsonresult.count > 0
        {
           
            self.customercodetf.text = (self.jsonresult.object(forKey: "Customercode")as! String)
            self.itemtf.text = (self.jsonresult.object(forKey: "itemcode")as! String)
            
           // self.vendorbidtf.text = self.jsonresult.objectForKey("VENDORBIDPRICE_out")as? String
            
            let Count1 : Double = Double((self.jsonresult.object(forKey: "Quantity_out")as? String)!)!
            let Count2 : Double = Double((self.jsonresult.object(forKey: "UnitPrice_out")as? String)!)!
            let Coutn3 : Double = Double((self.jsonresult.object(forKey: "VENDORBIDPRICE_out")as? String)!)!
            let Count4 : Double = Double((self.jsonresult.object(forKey: "ORCAMOUNT_out")as? String)!)!
            
            if Count1 == 0
            {
                self.qualitytf.text = ""
            }
            else
            {
                self.qualitytf.text = self.jsonresult.object(forKey: "Quantity_out")as? String
            }
            
            if Count2 == 0
            {
                self.unitpricetf.text = ""
            }
            else
            {
                self.unitpricetf.text = self.jsonresult.object(forKey: "UnitPrice_out")as? String
            }
            
            if Coutn3 == 0
            {
                self.vendorbidtf.text = ""
            }
            else
            {
                self.vendorbidtf.text = self.jsonresult.object(forKey: "VENDORBIDPRICE_out")as? String
            }
            
            if Count4 == 0
            {
                self.qrcamttf.text = ""
            }
            else
            {
                self.qrcamttf.text = self.jsonresult.object(forKey: "ORCAMOUNT_out")as? String
            }
            
            
            self.cashdiscounttf.text = (self.jsonresult.object(forKey: "Cashdiscount_out")as! String)
            self.Customercategory = (self.jsonresult.object(forKey: "CustomerCategory_out")as! String)
            self.DISTIDISC_out1 = (self.jsonresult.object(forKey: "DISTIDISC_out1")as! String)
            self.DISTIDISC_out = (self.jsonresult.object(forKey: "DISTIDISC_out")as! String)
            //ItemDescription_out
           self.ItemDescription_out = (self.jsonresult.object(forKey: "ItemDescription_out")as! String)
           LibraryAPI.sharedInstance.globaldic.setObject(ItemDescription_out, forKey:"ItemDescription_out" as NSCopying)
            LibraryAPI.sharedInstance.globaldic.setObject(DISTIDISC_out1, forKey:"DISTIDISC_out1" as NSCopying)
            
            let errormessage = self.jsonresult.object(forKey: "ErrorMessage")
            let message      = self.jsonresult.object(forKey: "Message_out")
            
            
            if (message as? String) != ""
            {
                self.showToast(message: message as! String)
            }
            else if (errormessage as? String) != ""
            {
                self.showToast(message: errormessage as! String)
            }
            }
        
        //New Updation Pos Alert 
           Flag =  (self.jsonresult.object(forKey: "Flag")as? String)!
         LibraryAPI.sharedInstance.PosVale1 = (self.jsonresult.object(forKey: "APOS1")as? String)!
         LibraryAPI.sharedInstance.PosVale2 = (self.jsonresult.object(forKey: "APOS2")as? String)!
         LibraryAPI.sharedInstance.PosVale3 = (self.jsonresult.object(forKey: "APOS3")as? String)!
         LibraryAPI.sharedInstance.PosVale4 = (self.jsonresult.object(forKey: "APOS4")as? String)!
         LibraryAPI.sharedInstance.PosVale5 = (self.jsonresult.object(forKey: "APOS5")as? String)!
        
         LibraryAPI.sharedInstance.FDP1 = (self.jsonresult.object(forKey: "FDP1")as? String)!
         LibraryAPI.sharedInstance.FDP2 = (self.jsonresult.object(forKey: "FDP2")as? String)!
         LibraryAPI.sharedInstance.FDP3 = (self.jsonresult.object(forKey: "FDP3")as? String)!
         LibraryAPI.sharedInstance.FDP4 = (self.jsonresult.object(forKey: "FDP5")as? String)!
         LibraryAPI.sharedInstance.FDP5 = (self.jsonresult.object(forKey: "FDP5")as? String)!
        
   }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {

        if (textField.tag == 3)
        {
            
            LibraryAPI.sharedInstance.Currentitemcode = "Itemcode"
            
            self.itemcode()
                    
                    if(LibraryAPI.sharedInstance.priceApptype == "LBO")
                    {
                        self.display()
                        
                        self.eopnumbertf.isHidden = false
                        self.referencenumbertf.isHidden = false
                        self.eopnumberlbl.isHidden = false
                        self.referrencenumberlbl.isHidden = false
                    }
                    else
                    {
                        self.eopnumbertf.isHidden = true
                        self.referencenumbertf.isHidden = true
                        self.eopnumberlbl.isHidden = true
                        self.referrencenumberlbl.isHidden = true
                    }
            
        
                self.itemcode()
                if(LibraryAPI.sharedInstance.priceApptype == "LBO")
                {
                     self.display()
                    
                    self.eopnumbertf.isHidden = false
                    self.referencenumbertf.isHidden = false
                    self.eopnumberlbl.isHidden = false
                    self.referrencenumberlbl.isHidden = false
                }
                else
                {
                    self.eopnumbertf.isHidden = true
                    self.referencenumbertf.isHidden = true
                    self.eopnumberlbl.isHidden = true
                    self.referrencenumberlbl.isHidden = true
                }
                
                
            let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "ItemcodePopupViewController") as! ItemcodePopupViewController
            self.navigationController?.pushViewController(lvc, animated: false)
            
            return false
        }
        else if(textField.tag == 8)
        
        {
            
            let alert:UIAlertController=UIAlertController(title: "Select CashDiscount", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            
            
            var alert1 = UIAlertAction()
            
            if pickerarray2.count != 0
            {
                for word in pickerarray2
                {
                    alert1 = UIAlertAction(title: (word as! String), style: UIAlertActionStyle.default)
                    {
                        
                        UIAlertAction in
                         let discount : [String] = (word as AnyObject).components(separatedBy: " - ")
                       // let discount : [String] = (word as AnyObject).componentsSeparatedByString(" - ")
                        let discountstr : String = discount[0]
                        let discountdetail : String = discount[1]
                       
                        
                        self.cashdiscounttf.text = discountstr
                        self.discountlbl.text = discountdetail
                        self.defaultcheck(FDP1: "", APOS1: "0")
                        self.GlobalDic()
                        
                        
                    }
                    
                    alert.addAction(alert1)
                    
                }
                
        
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
            {
                UIAlertAction in
                
            }
            
            // Add the actions
            //picker?.delegate = self
            
            alert.addAction(cancelAction)
            
            self.present(alert, animated: true, completion: nil)
            
            return false
            
        }
        
        else if(textField.tag == 13)
        {
            let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Nature", message: "Pick an option", preferredStyle: .actionSheet)
            
            let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
                
            }
            actionSheetControllerIOS8.addAction(cancelActionButton)
            
            
            
            let saveActionButton: UIAlertAction = UIAlertAction(title:((LibraryAPI.sharedInstance.PosVale1)+"-"+(LibraryAPI.sharedInstance.FDP1)), style: .default)
            { action -> Void in
                
                self.postxtfield.text = (("APPLE POSID"+LibraryAPI.sharedInstance.PosVale1)+"-"+(LibraryAPI.sharedInstance.FDP1))
                LibraryAPI.sharedInstance.Global_PosValue1 = LibraryAPI.sharedInstance.PosVale1
                self.defaultcheck(FDP1: LibraryAPI.sharedInstance.PosVale1, APOS1: "0")
                 LibraryAPI.sharedInstance.Global_PosValue = LibraryAPI.sharedInstance.PosVale1
                
            }
            actionSheetControllerIOS8.addAction(saveActionButton)
            
            let deleteActionButton: UIAlertAction = UIAlertAction(title: ((LibraryAPI.sharedInstance.PosVale2)+"-"+(LibraryAPI.sharedInstance.FDP2)), style: .default)
            { action -> Void in
                
                self.postxtfield.text = (("APPLE POSID"+LibraryAPI.sharedInstance.PosVale2)+" - "+(LibraryAPI.sharedInstance.FDP2))
                LibraryAPI.sharedInstance.Global_PosValue1 = LibraryAPI.sharedInstance.PosVale2
                 self.defaultcheck(FDP1: LibraryAPI.sharedInstance.PosVale2, APOS1: "0")
                 LibraryAPI.sharedInstance.Global_PosValue = LibraryAPI.sharedInstance.PosVale2
                
            }
            
            actionSheetControllerIOS8.addAction(deleteActionButton)
            
            let PresentationActionButton: UIAlertAction = UIAlertAction(title: ((LibraryAPI.sharedInstance.PosVale3)+"-"+(LibraryAPI.sharedInstance.FDP3)), style: .default)
            { action -> Void in
                
                self.postxtfield.text = (("APPLE POSID"+LibraryAPI.sharedInstance.PosVale3)+"-"+(LibraryAPI.sharedInstance.FDP3))
                LibraryAPI.sharedInstance.Global_PosValue1 = LibraryAPI.sharedInstance.PosVale3
                 self.defaultcheck(FDP1: LibraryAPI.sharedInstance.PosVale3, APOS1: "0")
                LibraryAPI.sharedInstance.Global_PosValue = LibraryAPI.sharedInstance.PosVale3
                
            }
            
            actionSheetControllerIOS8.addAction(PresentationActionButton)
            
            let courtesy: UIAlertAction = UIAlertAction(title: ((LibraryAPI.sharedInstance.PosVale4)+"-"+(LibraryAPI.sharedInstance.FDP4)), style: .default)
            { action -> Void in
                
               self.postxtfield.text = (("APPLE POSID"+LibraryAPI.sharedInstance.PosVale4)+"-"+(LibraryAPI.sharedInstance.FDP4))
                 LibraryAPI.sharedInstance.Global_PosValue1 = LibraryAPI.sharedInstance.PosVale4
                 self.defaultcheck(FDP1: LibraryAPI.sharedInstance.PosVale4, APOS1: "0")
                 LibraryAPI.sharedInstance.Global_PosValue = LibraryAPI.sharedInstance.PosVale4
               
            }
            
            
            actionSheetControllerIOS8.addAction(courtesy)
            
            let poc: UIAlertAction = UIAlertAction(title: ((LibraryAPI.sharedInstance.PosVale5)+"-"+(LibraryAPI.sharedInstance.FDP5)), style: .default)
            { action -> Void in
                
                self.postxtfield.text = (("APPLE POSID"+LibraryAPI.sharedInstance.PosVale5)+"-"+(LibraryAPI.sharedInstance.FDP5))
                LibraryAPI.sharedInstance.Global_PosValue1 = LibraryAPI.sharedInstance.PosVale5
                 self.defaultcheck(FDP1: LibraryAPI.sharedInstance.PosVale5, APOS1: "0")
                 LibraryAPI.sharedInstance.Global_PosValue = LibraryAPI.sharedInstance.PosVale5
                
            }
            actionSheetControllerIOS8.addAction(poc)
            
            self.present(actionSheetControllerIOS8, animated: true, completion: nil)
            
            return false
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        //fab.hidden=false
        textField.resignFirstResponder()
        
        if(textField.tag == 9)
        {
            self.defaultcheck(FDP1: "", APOS1: "0")
        }
        else if(textField.tag == 10)
        {
            self.defaultcheck(FDP1: "", APOS1: "0")
        }
        else if(textField.tag == 11)
        {
            self.defaultcheck(FDP1: "", APOS1: "0")
        }
         else if(textField.tag == 12)
         {
            self.defaultcheck(FDP1: "", APOS1: "0")
            GlobalDic()
        }
        
        return true
    }
    
    
    func uploaddata()
    {
        //RequestDicUrl
        // http://edi.redingtonb2b.in/Whatsup-staging/Users.asmx/SPC_Screen2Updation
        let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)Users.asmx/SPC_Screen2Updation")! as URL)
        request.httpMethod = "POST"
        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
        request.httpBody = try? JSONSerialization.data(withJSONObject: self.uploadfile, options: JSONSerialization.WritingOptions(rawValue: 0))
        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
        //  var err: NSError
        do
        {
            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
            jsonresult = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            
            
        }
        catch (let e)
        {
            
        }
        
    }
    func Loaddatacity(url:String)
    {
        //responsearray2 = LibraryAPI.sharedInstance.RequestUrl(url: url)
        self.getsuccess(params: [:], urlhit: url, completion: { (jsonmutable) in
            self.responsearray2 = jsonmutable
            if(self.responsearray2.count > 0)
            {
                
            }
            self.pickerarray2.removeAllObjects()
            for i in 0  ..< self.responsearray2.count
            {
                //  if(currenttf=="business")
                // {
                let dic = self.responsearray2[i] as! NSDictionary
                let code = dic.object(forKey: "Mode") as? String
                let desc = dic.object(forKey: "Detail") as? String
                let str=code?.appending(" - \(desc!)")
                
                self.pickerarray2.add(str!)
                
                
                
                if self.responsearray2.count != 0
                {
                    for word in self.responsearray2
                    {
                        
                        let code = ((word as AnyObject).object(forKey: "Mode") as? String)
                        let desc = (word as AnyObject).object(forKey: "Detail") as? String
                        
                        if (code == "I")
                        {
                            self.cashdiscounttf.text = code
                            self.discountlbl.text = desc
                        }
                        
                        
                        
                    }
                    
                }
                
            }
        })
        
        
        
    }
    
    
    
    func cityrec()
    {
      //   http://edi.redingtonb2b.in/Whatsup-staging/Customer.asmx/FieldDescription?Userid=SENTHILKS&prmt=CSHD
      self.Loaddatacity(url: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/FieldDescription?Userid=\(LibraryAPI.sharedInstance.Userid)&prmt=CSHD")
    }
    
  
    
    @IBAction func frieghtyesbtn(_ sender: AnyObject)
    {
        checkchar = "Y"
        
        frieghtyesbtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
        frieghtnobtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        self.defaultcheck(FDP1: "", APOS1: "0")
    }
    
    @IBAction func frieghtnobtn(_ sender: AnyObject)
    {
        checkchar = "N"
        
        frieghtyesbtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        frieghtnobtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
         self.defaultcheck(FDP1: "", APOS1: "0")
    }
    
    @IBAction func multiinvyesbtn(_ sender: AnyObject)
    {
        checkchar1 = "Y"
        
        multiinvyesbtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
        multiinvnobtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        self.defaultcheck(FDP1: "", APOS1: "0")

    }
    
    @IBAction func multiinvnobtn(_ sender: AnyObject)
    {
        checkchar1 = "N"
        
        multiinvyesbtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        multiinvnobtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
        self.defaultcheck(FDP1: "", APOS1: "0")

    }
    
    @IBAction func freebieyesbtn(_ sender: AnyObject)
    {
        checkchar2 = "F"
        
        freebieyesbtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
        freebienobtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
         self.defaultcheck(FDP1: "", APOS1: "0")
        self.GlobalDic()

    }
    
    @IBAction func freebienobtn(_ sender: AnyObject)
    {
        checkchar2 = "N"
        
        freebieyesbtn.setImage(UIImage(named:"normal.png"),for: UIControlState.normal)
        freebienobtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
        self.defaultcheck(FDP1: "", APOS1: "0")
        self.GlobalDic()

    }
    
    @IBAction func AddCartBtnAcrion(_ sender: UIButton)
    {
        
    }
    
    
    @IBAction func addtocartAction(_ sender: AnyObject)
    {
        self.defaultcheck(FDP1: "", APOS1: "0")
        
        
        if self.jsonresult.count > 0
        {
            
            if (self.jsonresult.object(forKey: "Sumbit_out")as! String == "Y")
            {
                if LibraryAPI.sharedInstance.globaldic.count > 0
                {
                   
                     if(firstTimePressed == false)
                    {
                        firstTimePressed = true
                        let date = NSDate()
                        let calender = NSCalendar.current
                        let formatter = DateFormatter()
                        formatter.dateFormat = "MM-dd-yyyy hh:mm:ss"
                        let result = formatter.string(from: date as Date)
                        LibraryAPI.sharedInstance.globaldic.setObject(result, forKey: "timing" as NSCopying)
                    }
                    LibraryAPI.sharedInstance.globalarray.add(LibraryAPI.sharedInstance.globaldic)
                    
                    
                    let alertController = UIAlertController(title: "Alert!", message: "Your Cart is Added Successfully", preferredStyle: .alert)
                    
                    
                    let DefaultAction = UIAlertAction(title: "Shopping Continue", style: .default) { (action:UIAlertAction!) in
                        
                       
                        
                    }
                    
                    let CancelAction = UIAlertAction(title: "Go  to Cart", style: .cancel) { (action:UIAlertAction!) in
                        
                        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "Addtocart2ViewController")), animated: true)
                        
                    }
                    alertController.addAction(DefaultAction)
                    alertController.addAction(CancelAction)
                    self.present(alertController, animated: true, completion:nil)

                    countlabel.text = String(LibraryAPI.sharedInstance.globalarray.count)

                    self.itemtf.text = ""
                    self.qualitytf.text = ""
                    self.unitpricetf.text = ""
                    self.vendorbidtf.text = ""
                    self.qrcamttf.text = ""
                    self.checkchar2 = "N"
                    self.addtionalamttf.text = ""
                    freebieyesbtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
                    freebienobtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
                    
                }
                else
                {
                    self.showToast(message: "Cart is already Added")
                    
                } 
                
            }
            else if (self.jsonresult.object(forKey: "Message_out")as! String != "")
            {
                let message = self.jsonresult.object(forKey: "Message_out")as! String
                self.showToast(message: message)
            }
            else
            {
                let message = self.jsonresult.object(forKey: "ErrorMessage")as! String
                self.showToast(message: message)
            }
        }
        else
        {
             self.showToast(message: "Data not found")
        }
        
    }
    
    @IBAction func addcartbtnAction(_ sender: AnyObject)
    {
       
       
        if(LibraryAPI.sharedInstance.globalarray.count > 0)
        {
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "Addtocart2ViewController")), animated: true)
        }
        else
        {
            self.NormalAlert(title: "Alert", Messsage: "No Data in Cart")
            /*let alertController = UIAlertController(title: "Alert", message: "No Data in Cart", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
            }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)*/
        }
        
    
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    if(textField.tag == 4)
        {
            let oldString = textField.text?.count == 0 ? string  : textField.text!
            let i = oldString.count
            let range = oldString.index(oldString.startIndex, offsetBy: i-1) ..< oldString.index(oldString.startIndex, offsetBy: i)
            let candidate = oldString.replacingCharacters(in: range, with: string)
            let regex = try? NSRegularExpression(pattern: "^\\d{0,8}(\\.\\d{0,2})?$", options: [])
            return regex?.firstMatch(in: candidate, options: [], range: NSRange(location: 0, length: candidate.count)) != nil
        }
        if(textField.tag == 5)
        {
            let oldString = textField.text?.count == 0 ? string  : textField.text!
            let i = oldString.count
            let range = oldString.index(oldString.startIndex, offsetBy: i-1) ..< oldString.index(oldString.startIndex, offsetBy: i)
            let candidate = oldString.replacingCharacters(in: range, with: string)
            let regex = try? NSRegularExpression(pattern: "^\\d{0,8}(\\.\\d{0,2})?$", options: [])
            return regex?.firstMatch(in: candidate, options: [], range: NSRange(location: 0, length: candidate.count)) != nil
        }
        if(textField.tag == 6)
        {
            let oldString = textField.text?.count == 0 ? string  : textField.text!
            let i = oldString.count
            let range = oldString.index(oldString.startIndex, offsetBy: i-1) ..< oldString.index(oldString.startIndex, offsetBy: i)
            let candidate = oldString.replacingCharacters(in: range, with: string)
            let regex = try? NSRegularExpression(pattern: "^\\d{0,3}(\\.\\d{0,2})?$", options: [])
            return regex?.firstMatch(in: candidate, options: [], range: NSRange(location: 0, length: candidate.count)) != nil
        }
        if(textField.tag == 7)
        {
            let oldString = textField.text?.count == 0 ? string  : textField.text!
            let i = oldString.count
            let range = oldString.index(oldString.startIndex, offsetBy: i-1) ..< oldString.index(oldString.startIndex, offsetBy: i)
            let candidate = oldString.replacingCharacters(in: range, with: string)
            let regex = try? NSRegularExpression(pattern: "^\\d{0,8}(\\.\\d{0,2})?$", options: [])
            return regex?.firstMatch(in: candidate, options: [], range: NSRange(location: 0, length: candidate.count)) != nil
        }
        if(textField.tag == 12)
        {
            let oldString = textField.text?.count == 0 ? string  : textField.text!
            let i = oldString.count
            let range = oldString.index(oldString.startIndex, offsetBy: i-1) ..< oldString.index(oldString.startIndex, offsetBy: i)
            let candidate = oldString.replacingCharacters(in: range, with: string)
            let regex = try? NSRegularExpression(pattern: "^\\d{0,8}(\\.\\d{0,2})?$", options: [])
            return regex?.firstMatch(in: candidate, options: [], range: NSRange(location: 0, length: candidate.count)) != nil
        }
        
        return true
    }
    

}
