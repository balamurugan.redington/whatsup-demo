//
//  ItemCode.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 14/02/18.
//  Copyright © 2018 truetech. All rights reserved.
//

import Foundation


struct ItemCode {
   
    
    var Item_Code: String
    var Item_Description: String
    var Total_Page: NSNumber
    var Vendor_part_Code: String
   
    
    
    init(){
        self.Item_Code = ""
        self.Item_Description = ""
        self.Total_Page = 0
        self.Vendor_part_Code = ""
       
        
    }
    
    init(data : AnyObject){
        
        if let temp = data["Item Code"] as? String {
            self.Item_Code = temp
        }else {
            self.Item_Code = ""
        }
        
        if let temp = data["Item Description"] as? String {
            self.Item_Description = temp
        }else {
            self.Item_Description = ""
        }
        
        if let temp = data["Total Page"] as? NSNumber {
            self.Total_Page = temp
        }else {
            self.Total_Page = 0
        }
        
        if let temp = data["Vendor part Code"] as? String {
            self.Vendor_part_Code = temp
        }else {
            self.Vendor_part_Code = ""
        }
        
       
        
    }
}
