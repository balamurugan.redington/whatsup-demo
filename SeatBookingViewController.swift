//
//  SeatBookingViewController.swift
//  Redington
//
//  Created by RIL-ITTECH on 06/03/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import KCFloatingActionButton

class SeatBookingViewController: UIViewController,KCFloatingActionButtonDelegate,UITextFieldDelegate,UITextViewDelegate {
    
    @IBOutlet weak var seatview: UIView!
    @IBOutlet weak var submitbtn: UIButton!
    @IBOutlet weak var checkbtn: UIButton!
    
       
    @IBOutlet weak var toview: UILabel!
    @IBOutlet weak var fromview: UILabel!
    @IBOutlet weak var dateview: UILabel!
    
    @IBOutlet weak var bookhistroy: UIButton!
    var uploadfile=NSMutableDictionary()
    var jsonresult=NSDictionary()
    @IBOutlet weak var refview: UILabel!
    @IBOutlet weak var ticket: UIImageView!
    @IBOutlet weak var pickerview: UIView!
    @IBOutlet weak var datepicker: UIDatePicker!
    @IBOutlet weak var totime: UITextField!
    @IBOutlet weak var freomtime: UITextField!
    @IBOutlet weak var fromdate: UITextField!
    var currenttextfield=String()
    var frmstr = String()
    
    @IBOutlet weak var seatlabel: UILabel!
    @IBOutlet weak var reflabel: UILabel!
    @IBOutlet weak var reflbl: UILabel!
    
    @IBOutlet weak var nodatalbl: UILabel!
    @IBOutlet weak var seatlbl: UILabel!
    
    
    var responsestring = NSMutableArray()
    var fab=KCFloatingActionButton()
    
    override func viewDidLoad()
        
    {
        super.viewDidLoad()
        
        self.customnavigation()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        submitbtn.layer.masksToBounds=true
        submitbtn.layer.cornerRadius=20.0
        checkbtn.layer.masksToBounds=true
        checkbtn.layer.cornerRadius=20.0
        self.layoutFAB()
        
        freomtime.delegate = self
        totime.delegate = self
        fromdate.delegate = self
        fromdate.tag = 1
        freomtime.tag = 2
        totime.tag = 3
        
        reflabel.isHidden = true
        seatlabel.isHidden = true
        
        reflbl.isHidden = true
        seatlbl.isHidden = true
        
        seatview.isHidden = false
        ticket.isHidden = true
        self.Loaddata()
        
        let arrow = UIImageView()
        let image = UIImage(named: "cal.png")
        arrow.image = image!.imageWithColor(tintColor: UIColor.init(red:239/255.0 , green: 88/255.0, blue: 88/255.0, alpha: 1.0)).withRenderingMode(.alwaysOriginal)
        arrow.frame = CGRect(x: 0.0,y: 0.0, width: arrow.image!.size.width+10.0,height:15);
        arrow.contentMode = UIViewContentMode.center
        self.fromdate.rightView = arrow;
        self.fromdate.rightViewMode = UITextFieldViewMode.always
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        appdelegate.shouldSupportAllOrientation = false
    }
    
    
    
    
    
    @IBAction func checkbtn(_ sender: AnyObject)
    {
        if(responsestring.count > 0)
        {
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: BookhistroyViewController.instance())
                
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
            }
            let container = BookhistroyViewController.instance()
            container.closeHandler = { _ in
                popup.dismiss()
            }
            popup.show(childViewController: container)
        }
        else
        {
            
        }
        
       
        
    }
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            
            
            
            self.fab.close()
        }
        
        fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
            DispatchQueue.main.async {
                
                self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "SeatBookingViewController")), animated: true)
            }
          //  dispatch_async(DispatchQueue.main, {
               // self.sideMenuViewController!.setContentViewController(contentViewController: UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "SeatBookingViewController")), animated: true)
                
                
           // })
            
            
            
            self.fab.close()
        }
        
        
        fab.fabDelegate = self
        
        self.view.addSubview(fab)
    }
    
    
    func KCFABOpened(fab: KCFloatingActionButton) {
        
    }
    
    
    func setdateandtime()
    {
        let date = NSDate()
        let calendar = NSCalendar.current
        //let components = calendar.components([ .Hour, .Minute, .Second], fromDate: date)
         let components = calendar.dateComponents([.hour, .minute, .second], from: date as Date)
        let hour = components.hour
        let minutes = components.minute
        
        let formattedhour=String(describing: hour)
        let formattedtime=String(describing: minutes)
      //  let originaltime=formattedhour.stringByAppendingString(":\(formattedtime)")
        let originaltime=formattedhour.appending(":\(formattedtime)")
        
        
        let dateMakerFormatter1 = DateFormatter()
        dateMakerFormatter1.dateFormat = "HH:mm"
        let startTime = dateMakerFormatter1.date(from: originaltime)!
        let strtime = dateMakerFormatter1.string(from: startTime)
        //this comes back as 12:40 am not pm
        
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "MM-dd-yyyy"
        let strDate = dateFormatter.string(from: date as Date)
        
        
        
        fromdate.text=strDate
        
        freomtime.text=strtime
        totime.text = strtime
        
        
        
    }
    
    @IBAction func donebtn(_ sender: AnyObject)
    {
        datepicker.isHidden=true
        if( currenttextfield=="Date")
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = DateFormatter.Style.long
            dateFormatter.dateFormat = "yyyy-MM-dd"
            //"MM-dd-yy"
            let strDate = dateFormatter.string(from: datepicker.date)
            fromdate.text=strDate
            pickerview.isHidden = true
            
        }
        else if( currenttextfield=="Time")
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            // dateFormatter.dateStyle = NSDateFormatterStyle.LongStyle
            let strDate = dateFormatter.string(from: datepicker.date)
            
            
            
            freomtime.text=strDate
            pickerview.isHidden = true
        }
        else if( currenttextfield=="to")
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            // dateFormatter.dateStyle = NSDateFormatterStyle.LongStyle
            let strDate = dateFormatter.string(from: datepicker.date)
            
            
            
            totime.text=strDate
            pickerview.isHidden = true
        }
        seatview.isHidden = false
        
    }
    @IBAction func CancelAction(_ sender: AnyObject)
    {
        pickerview.isHidden=true
        seatview.isHidden = false
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
  
        // fab.hidden=false
        textField.resignFirstResponder()
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
   
        if(textField.tag==1)
        {
            datepicker.date = NSDate() as Date
            pickerview.isHidden=false
            datepicker.isHidden = false
            datepicker.datePickerMode = UIDatePickerMode.date
            seatview.isHidden = true
            //  datepicker.maximumDate=NSDate.distantFuture()
            // datepicker.minimumDate=NSDate.distantPast()
            currenttextfield="Date"
            datepicker.minimumDate = datepicker.date
            return false
            
        }
        
        if(textField.tag==2)
        {
            pickerview.isHidden=false
            datepicker.isHidden = false
            datepicker.datePickerMode = UIDatePickerMode.time
            datepicker.locale = NSLocale(localeIdentifier: "en_GB") as Locale
            currenttextfield="Time"
            seatview.isHidden = true
            // datepicker.maximumDate=NSDate.distantFuture()
            //datepicker.minimumDate=NSDate.distantPast()
            return false
            
        }
        if(textField.tag==3)
        {
            pickerview.isHidden=false
            datepicker.isHidden = false
            datepicker.datePickerMode = UIDatePickerMode.time
            datepicker.locale = NSLocale(localeIdentifier: "en_GB") as Locale
            //datepicker.maximumDate=NSDate.distantFuture()
            //datepicker.minimumDate=NSDate.distantPast()
            currenttextfield="to"
            seatview.isHidden = true
            return false
            
        }
        
        return true
    }
    
    @IBAction func checkavailability(_ sender: AnyObject)
    {
        if self.fromdate.text == ""
        {
            let alertController = UIAlertController(title: "Alert", message:"Select Date" , preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                
            }
            alertController.addAction(cancelAction)
            
            
            self.present(alertController, animated: true, completion:nil)
        }
        else
        {
            UserDefaults.standard.set(self.fromdate.text, forKey: "SeatBookingDate")
            UserDefaults.standard.synchronize()
           
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: SeatavailibilityViewController.instance())
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
            }
            let container = SeatavailibilityViewController.instance()
            container.closeHandler = { _ in
                popup.dismiss()
            }
            
            
            popup.show(childViewController: container)
        }
        
    }
    
    @IBAction func submitAction(_ sender: AnyObject)
    {
        if(fromdate.text!.isEmpty)
        {
            self.NormalAlert(title: "Alert", Messsage: "Please Enter Date")
           /* let alertController = UIAlertController(title: "Alert", message:"Please Enter Date" , preferredStyle: .Alert)
            
            let cancelAction = UIAlertAction(title: "Ok", style: .Cancel) { (action:UIAlertAction!) in
            }
            alertController.addAction(cancelAction)
            
            
            self.presentViewController(alertController, animated: true, completion:nil)*/

        }
        if(self.freomtime.text!.isEmpty)
        {
            self.NormalAlert(title: "Alert", Messsage: "Please Enter From Time")
            /*let alertController = UIAlertController(title: "Alert", message:"Please Enter From Time" , preferredStyle: .Alert)
            
            let cancelAction = UIAlertAction(title: "Ok", style: .Cancel) { (action:UIAlertAction!) in
            }
            alertController.addAction(cancelAction)
            
            
            self.presentViewController(alertController, animated: true, completion:nil)*/
        }
        else if(self.totime.text!.isEmpty)
        {
            self.NormalAlert(title: "Alert", Messsage: "Please Enter ToTime")
           /* let alertController = UIAlertController(title: "Alert", message:"Please Enter ToTime" , preferredStyle: .Alert)
            
            let cancelAction = UIAlertAction(title: "Ok", style: .Cancel) { (action:UIAlertAction!) in
            }
            alertController.addAction(cancelAction)
            
            
            self.presentViewController(alertController, animated: true, completion:nil)*/
        }
        else
        {
            uploadfile.setObject(LibraryAPI.sharedInstance.Userid,forKey:"UserId" as NSCopying)
            
            uploadfile.setObject(fromdate.text!,forKey:"BookDate" as NSCopying)
            
            let test = freomtime.text!
            
            //let swiftryString : String = test.stringByReplacingOccurrencesOfString(":", withString: "")
            let swiftryString : String = test.replacingOccurrences(of: ":", with: "")
            uploadfile.setObject(swiftryString,forKey:"FromTime" as NSCopying)
            
            let test1 = totime.text!
            
            let swiftyString1 = test1.replacingOccurrences(of: ":", with: "")
            
            uploadfile.setObject(swiftyString1,forKey:"ToTime" as NSCopying)
            
            
            
            let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)WorkLocation.asmx/SeatBookingChecking")! as URL)
            request.httpMethod = "POST"
            request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
            request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
            request.httpBody = try? JSONSerialization.data(withJSONObject: self.uploadfile, options: JSONSerialization.WritingOptions(rawValue: 0))
            
            let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
            
            do
            {
                let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
                jsonresult = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                let size = CGSize(width: 30, height:30)
                self.local_activityIndicator_start()
                // new version dispatch Que
                
                DispatchQueue.global(qos: .background).async {
                    
                    // Validate user input
                    
                    
                    // Go back to the main thread to update the UI
                    DispatchQueue.main.async {
                       self.local_activityIndicator_stop()
                        if self.jsonresult.object(forKey: "Message")as? String == ""
                        {
                            // LibraryAPI.sharedInstance.msg=(self.jsonresult.objectForKey("Message") as? String)!
                            LibraryAPI.sharedInstance.ref=(self.jsonresult.object(forKey: "RefNumber") as? String)!
                            LibraryAPI.sharedInstance.seat=(self.jsonresult.object(forKey: "Seat") as? String)!
                            //  LibraryAPI.sharedInstance.floor=(self.jsonresult.objectForKey("Floor") as? String)!
                            
                            self.reflabel.isHidden = false
                            self.reflabel.text = LibraryAPI.sharedInstance.ref
                            
                            // self.floorlabel.hidden = false
                            // self.floorlabel.text = LibraryAPI.sharedInstance.floor
                            
                            self.seatlabel.isHidden = false
                            self.seatlabel.text = LibraryAPI.sharedInstance.seat
                            
                            
                            self.reflbl.isHidden = false
                            
                            self.seatlbl.isHidden = false
                            // self.floorlbl.hidden = false
                            self.seatview.isHidden = false
                            self.ticket.isHidden = false
                            
                        }
                        else
                        {
                            //self.seatview.hidden = true
                            self.reflabel.isHidden = true
                            self.reflbl.isHidden = true
                            self.seatlbl.isHidden = true
                            self.seatlabel.isHidden = true
                            self.ticket.isHidden = true
                            
                            let alertController = UIAlertController(title: "Alert", message:"\(self.jsonresult.object(forKey: "Message") as! String)" , preferredStyle: .alert)
                            
                            let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                
                                
                                
                            }
                            alertController.addAction(cancelAction)
                            
                            
                            self.present(alertController, animated: true, completion:nil)
                        }
                        
                    }
                }
                
            }
            catch (let e)
            {
                
                print(e)
            }
        }
    }
    
    func customnavigation()
        
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y, width: self.view.frame.width, height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(SeatBookingViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Seat Booking"
        
    }
    func buttonClicked(sender:UIButton)
    {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
    }
    
    
    func Loaddata()
    {
        self.local_activityIndicator_start()
        
        // new version dispatch Que
        
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
            
            // self.responsestring = LibraryAPI.sharedInstance.RequestUrl(url: "\(LibraryAPI.sharedInstance.GlobalUrl)/WorkLocation.asmx/SeatBookingHistory?&Userid=\(LibraryAPI.sharedInstance.Userid)")
            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)/WorkLocation.asmx/SeatBookingHistory?&Userid=\(LibraryAPI.sharedInstance.Userid)", completion: { (jsonmutable) in
                self.responsestring = jsonmutable
            
            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
                
                if(self.responsestring.count>0)
                {
                    self.nodatalbl.isHidden = true
                    self.local_activityIndicator_stop()
                    let dic = self.responsestring[0] as! NSDictionary
                    self.refview.text=dic.object(forKey: "Booking Reference") as? String
                    self.dateview.text=dic.object(forKey: "Booking Date") as? String
                    self.fromview.text=dic.object(forKey: "From Time") as? String
                    self.toview.text=dic.object(forKey: "To Time") as? String
                }
                    
                else
                {
                   self.local_activityIndicator_stop()
                    self.nodatalbl.isHidden = false
                    
                }
            }
                })
        }
    
    }
    
}

