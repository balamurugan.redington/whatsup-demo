//
//  LeaveTableViewCell.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 22/05/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class LeaveTableViewCell: UITableViewCell
{

    @IBOutlet weak var appnolbl: UILabel!
    
    @IBOutlet weak var fromdatelbl: UILabel!
    
    @IBOutlet weak var todatelbl: UILabel!
    
    @IBOutlet weak var noofdayslbl: UILabel!
    
    @IBOutlet weak var leavetypelbl: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
