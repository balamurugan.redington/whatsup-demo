//
//  ReasonRejectViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 28/07/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class ReasonRejectViewController: UIViewController,PopupContentViewController,UITextViewDelegate {
    
    @IBOutlet weak var rejecttextview: UITextView!
    @IBOutlet weak var Submitbtn: UIButton!
    
    var uploadfile = NSMutableDictionary()
    var jsonresult = NSDictionary()
    
    var closeHandler: (() -> Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        print("ReasonRejectViewController")
        rejecttextview.layer.cornerRadius = 5.0
        rejecttextview.layer.borderWidth = 1.0
        rejecttextview.delegate = self
        self.rejecttextview.layer.borderColor = UIColor(red:211/255.0, green:211/255.0, blue:211/255.0, alpha: 1.0).cgColor
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    class func instance() -> ReasonRejectViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ReasonRejectViewController") as! ReasonRejectViewController
    }
    
    
    @IBAction func CloseAction(_ sender: UIButton) {
        
        closeHandler?()
    }
    
    func sizeForPopup(popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: 250, height: 250)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            rejecttextview.resignFirstResponder()
            return false
        }
        return true
    }
    
    
    @IBAction func SubmitbtnAction(_ sender: UIButton) {
        uploadfile.setObject(LibraryAPI.sharedInstance.Leaveapprovalhrd.object(forKey: "LEAVE APP NUMBER")!, forKey: "LeaveApplicationNumber" as NSCopying)
        uploadfile.setObject(LibraryAPI.sharedInstance.Userid, forKey: "Authoriser" as NSCopying)
        uploadfile.setObject("R", forKey: "AuthorisingFlag" as NSCopying)
        uploadfile.setObject(rejecttextview.text!, forKey: "Rejection_Text_1" as NSCopying)
        uploadfile.setObject("", forKey: "Rejection_Text_2" as NSCopying)
        uploadfile.setObject("", forKey: "Rejection_Text_3" as NSCopying)
        uploadfile.setObject("", forKey: "Rejection_Text_4" as NSCopying)
        uploadfile.setObject("", forKey: "Rejection_Text_5" as NSCopying)
        self.uploaddata()
        if(jsonresult.count > 0) {
            _ = jsonresult.object(forKey: "Status")as? String
            let Submit = jsonresult.object(forKey: "Submit")as? String
            if (Submit == "Y") {
                if LibraryAPI.sharedInstance.Globalflag == "N" {
                    let alertController = UIAlertController(title: "Alert", message: "Updated SuccessFully" , preferredStyle: .alert)
                    let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "HRD1DetailsViewController")), animated: true)
                    }
                    alertController.addAction(cancelAction)
                    self.present(alertController, animated: true, completion:nil)
                } else {
                    let alertController = UIAlertController(title: "Alert", message: "Updated SuccessFully" , preferredStyle: .alert)
                    let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
                    }
                    alertController.addAction(cancelAction)
                    self.present(alertController, animated: true, completion:nil)
                }
            } else {
                self.showalert(title: "Alert", message: "Submisson Failed! Retry", buttontxt: "Ok")
            }
        } else {
            let alertController = UIAlertController(title: "Alert", message:"Submisson Failed! Retry" , preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
            }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)
        }
    }
    
    func uploaddata() {
        let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)LeaveDetails.asmx/LeaveApproval")! as URL)
        request.httpMethod = "POST"
        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
        request.httpBody = try? JSONSerialization.data(withJSONObject: self.uploadfile, options: JSONSerialization.WritingOptions(rawValue: 0))
        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
        do {
            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
            jsonresult = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
        } catch (let e) {
            print(e)
        }
    }
}
