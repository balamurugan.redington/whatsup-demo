//
//  BackgroundLocationManager.swift
//  OfflineTrackingService
//
//  Created by ITtech on 24/06/17.
//  Copyright © 2017 Redington. All rights reserved.
//

import Foundation
import CoreLocation
import UserNotifications
import UIKit
import Alamofire

class BackgroundLocationManager :NSObject, CLLocationManagerDelegate {
    
    static let instance = BackgroundLocationManager()
    static let BACKGROUND_TIMER = 1800.0 // restart location manager every 1800.0 seconds
    static let UPDATE_SERVER_INTERVAL = (60 * 60)/2 // 1/2 hour - once every 1/2 hour send location to server
    
    let locationManager = CLLocationManager()
    var timer:Timer?
    var currentBgTaskId : UIBackgroundTaskIdentifier?
    var lastLocationDate : NSDate = NSDate()
    
    
    var uploadfile = NSMutableDictionary()
    var jsonresult = NSDictionary()
    
    
    private override init(){
        super.init()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        //locationManager.activityType = .other;
        locationManager.distanceFilter = kCLDistanceFilterNone;
        if #available(iOS 9, *){
            locationManager.allowsBackgroundLocationUpdates = true
        }
        
        NotificationCenter.default.addObserver(self,selector: #selector(self.applicationEnterBackground),name: Notification.Name.UIApplicationWillTerminate,object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(self.applicationEnterBackground),name: Notification.Name.UIApplicationDidEnterBackground,object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(self.applicationEnterBackground),name: Notification.Name.UIApplicationDidFinishLaunching,object: nil)
    }
    
    func applicationEnterBackground(){
       // FileLogger.log("applicationEnterBackground")
        start()
    }
    
    func start(){
        if(CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            if #available(iOS 9, *){
                locationManager.requestLocation()
                locationManager.startUpdatingLocation()
                locationManager.startMonitoringSignificantLocationChanges()
            } else {
                locationManager.startUpdatingLocation()
                locationManager.startMonitoringSignificantLocationChanges()
            }
        } else {
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
            locationManager.startMonitoringSignificantLocationChanges()
        }
    }
    
    func stop()
    {
        locationManager.stopUpdatingLocation()
        locationManager.stopMonitoringSignificantLocationChanges()
    }
    
    func restart (){
        timer?.invalidate()
        timer = nil
        start()
    }
    
    @nonobjc func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        switch status {
        case CLAuthorizationStatus.restricted: break
        //log("Restricted Access to location")
        case CLAuthorizationStatus.denied: break
        //log("User denied access to location")
        case CLAuthorizationStatus.notDetermined: break
        //log("Status not determined")
        default:
            //log("startUpdatintLocation")
            if #available(iOS 9, *){
                locationManager.requestLocation()
            } else {
               locationManager.requestLocation()
            }
            break
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
 
        if(timer==nil){
            // The locations array is sorted in chronologically ascending order, so the
            // last element is the most recent
            guard locations.last != nil else {return}
            
            beginNewBackgroundTask()
            locationManager.stopUpdatingLocation()
            let now = NSDate()
            let calendar = NSCalendar.current
            let hour = calendar.component(.hour, from: (now as NSDate) as Date)
            _ = calendar.component(.minute, from: (now as NSDate) as Date)
            _ = calendar.component(.second, from: (now as NSDate) as Date)
            
            let starttime : Double = 9.29
            let endtime : Double = 18.01
            
            if Double(hour) > starttime && Double(hour) < endtime
            {
                if(isItTime(now: now)){
                    
                    //TODO: Every n minutes do whatever you want with the new location. Like for example 
                    self.sendLocationToServer(location: locations.last!, now: now)
                }
            }
            else
            {
                
            }
            
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
      // CrashReporter.recordError(error)
        
        beginNewBackgroundTask()
        locationManager.stopUpdatingLocation()
    }
    
    func isItTime(now:NSDate) -> Bool {
       // let timePast = now.timeIntervalSince(lastLocationDate as NSDate)
        let timePast = now.timeIntervalSince(lastLocationDate as Date)
        let intervalExceeded = Int(timePast) > BackgroundLocationManager.UPDATE_SERVER_INTERVAL
        
        return intervalExceeded;
    }
    
    func sendLocationToServer(location:CLLocation, now:NSDate)
    {
        let formatter = DateFormatter()
          formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let result = formatter.string(from: now as Date)
        
        uploadfile.setObject(LibraryAPI.sharedInstance.Userid, forKey: "Userid" as NSCopying as NSCopying)
        uploadfile.setObject(result, forKey: "DateTimeUpdate" as NSCopying)
        uploadfile.setObject(String(location.coordinate.latitude), forKey: "latitude" as NSCopying)
        uploadfile.setObject(String(location.coordinate.longitude), forKey: "longitude" as NSCopying)
        uploadfile.setObject("I", forKey: "Flag" as NSCopying)
        loation()
    }
    
    func beginNewBackgroundTask(){
        var previousTaskId = currentBgTaskId;
        currentBgTaskId = UIApplication.shared.beginBackgroundTask(expirationHandler: {})
        if let taskId = previousTaskId{
            UIApplication.shared.endBackgroundTask(taskId)
            previousTaskId = UIBackgroundTaskInvalid
        }
        
        
        timer = Timer.scheduledTimer(timeInterval: BackgroundLocationManager.BACKGROUND_TIMER, target: self, selector: #selector(self.restart), userInfo: nil, repeats: false)
    }
    
    func loation()
    {
        
        
        let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)Users.asmx/GPSUpdation")! as URL)
        request.httpMethod = "POST"
        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
      
        request.httpBody = try? JSONSerialization.data(withJSONObject: self.uploadfile, options: JSONSerialization.WritingOptions(rawValue: 0))
        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
     
        do
        {
            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
            
            jsonresult = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
        }
        catch ( _)
        {
            
        }
        
    }

    
    
    
}
