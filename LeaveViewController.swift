
//
//  LeaveViewController.swift
//  Redington
//
//  Created by RIL-ITTECH on 13/03/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import Firebase
import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import KCFloatingActionButton
class LeaveViewController:  UIViewController,KCFloatingActionButtonDelegate,UITextFieldDelegate,UITextViewDelegate {
    @IBOutlet weak var aftnoonbtn: UIButton!
    @IBOutlet weak var frntnoon: UIButton!
    @IBOutlet weak var meternitybtn: UIButton!
    @IBOutlet weak var meternitylbl: UILabel!
    @IBOutlet weak var statusenquirybtn: UIButton!
    @IBOutlet weak var ScrollView: UIScrollView!
    @IBOutlet weak var containerview: UIView!
    @IBOutlet weak var reasontxtview: UITextView!
    @IBOutlet weak var fromdatetf: UITextField!
    @IBOutlet weak var todatetf: UITextField!
    @IBOutlet weak var totaldayslbl: UILabel!
    @IBOutlet weak var submitbtn: UIButton!
    @IBOutlet weak var pickerview: UIView!
    @IBOutlet weak var lopdayslbl: UILabel!
    @IBOutlet weak var datepicker: UIDatePicker!
    @IBOutlet weak var maternityview: UIView!
    @IBOutlet weak var viewtable: UIView!
    @IBOutlet weak var selecttf: UITextField!
   
    var currenttextfield=String()
    var type=String()
    var type1=String()
    var MeternityChange = String()
    var count = 0;
   
    @IBOutlet weak var pritf: UITextField!
    @IBOutlet weak var castf: UITextField!
    
    @IBOutlet weak var sicktf: UITextField!
    @IBOutlet weak var priviledgelbl: UILabel!
    
    @IBOutlet weak var casuallbl: UILabel!
    @IBOutlet weak var sicklbl: UILabel!
    
    @IBOutlet weak var datepicker1: UIDatePicker!
    @IBOutlet weak var micarriagebulletbtn: UIButton!
    @IBOutlet weak var maternitybulletbtn: UIButton!
    var responsestring2 = NSMutableArray()
     var responsestring3 = String()
    var leaveresponse = String()
    
    var checkbox = UIImage(named:"Rememberme.png")
    var uncheckbox = UIImage(named:"tick.png")
    var isboxclicked = Bool()
    var isboxclicked1 = Bool()
    
    
    var checkbox1 = UIImage(named:"Rememberme.png")
    var uncheckbox1 = UIImage(named:"tick.png")
    
    var date1 : NSDate!
    var date2 : NSDate!
    
    var dayscheck: Bool!
    var Movedup=Bool()
    var totaldiffer         = 0

    @IBOutlet weak var priviledgeballbl: UILabel!
    
    @IBOutlet weak var authbtn: UIButton!
    @IBOutlet weak var casualballbl: UILabel!
    @IBOutlet weak var sickballabl: UILabel!
    
     var uploadfile=NSMutableDictionary()
     var jsonresult=NSDictionary()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewtable.layer.masksToBounds=true
        viewtable.layer.cornerRadius=10.0
        selecttf.isUserInteractionEnabled = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.getnotificationmethod), name: NSNotification.Name(rawValue: "GetNotification"), object: nil)
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(LeaveViewController.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeRight)
        if LibraryAPI.sharedInstance.Globalflag == "N"
        {
            customnavigation1()
        }
        else
        {
            customnavigation()
        }
        self.addDoneButtonOnKeyboard()
        statusenquirybtn.layer.cornerRadius = 5.0
        reasontxtview.layer.cornerRadius = 5.0
        reasontxtview.layer.borderWidth = 1.0
        self.reasontxtview.layer.borderColor = UIColor(red:211/255.0, green:211/255.0, blue:211/255.0, alpha: 1.0).cgColor
        submitbtn.layer.cornerRadius = 5.0
        submitbtn.layer.borderWidth = 1.0
        self.submitbtn.layer.borderColor = UIColor(red:211/255.0, green:211/255.0, blue:211/255.0, alpha: 1.0).cgColor
        self.Loaddata2()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        selecttf.delegate = self
        fromdatetf.delegate = self
        todatetf.delegate = self
        reasontxtview.delegate = self
        pritf.delegate = self
        sicktf.delegate = self
        castf.delegate = self
        fromdatetf.tag = 1
        todatetf.tag = 2
        reasontxtview.tag = 3
        pritf.tag = 4
        sicktf.tag = 5
        castf.tag = 6
        totaldayslbl.text = "0"
        lopdayslbl.text = "0"
        MeternityChange = "0"
        
        self.pritf.isUserInteractionEnabled = true
        self.castf.isUserInteractionEnabled = true
        self.sicktf.isUserInteractionEnabled = true
        
        pritf.keyboardType = .decimalPad
        sicktf.keyboardType = .decimalPad
        castf.keyboardType = .decimalPad
        
        self.selecttf.isUserInteractionEnabled = false
        maternityview.isHidden = true
        
        type = ""
        type1 = ""
        isboxclicked = true
        isboxclicked1 = true
        dayscheck = false
        
        frntnoon.setImage(uncheckbox1, for: UIControlState.normal)
        aftnoonbtn.setImage(uncheckbox1, for: UIControlState.normal)
        
        pritf.addTarget(self, action: #selector(LeaveViewController.textFieldDidBeginEditing), for: UIControlEvents.editingChanged)
        sicktf.addTarget(self, action: #selector(LeaveViewController.textFieldDidBeginEditing), for: UIControlEvents.editingChanged)
        castf.addTarget(self, action: #selector(LeaveViewController.textFieldDidBeginEditing), for: UIControlEvents.editingChanged)
        
        NotificationCenter.default.addObserver(self, selector: #selector(LeaveViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LeaveViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        ScrollView.addSubview(containerview)
        let arrow = UIImageView()
        let image = UIImage(named: "cal.png")
        arrow.image = image!.imageWithColor(tintColor: UIColor.init(red:239/255.0 , green: 88/255.0, blue: 88/255.0, alpha: 1.0)).withRenderingMode(.alwaysOriginal)
        arrow.frame = CGRect(x: 0.0, y: 0.0, width: arrow.image!.size.width+10.0, height: 15);
        arrow.contentMode = UIViewContentMode.center
        self.fromdatetf.rightView = arrow;
        self.fromdatetf.rightViewMode = UITextFieldViewMode.always
        
        
        let arrow1 = UIImageView()
        let image1 = UIImage(named: "cal.png")
        arrow1.image = image1!.imageWithColor(tintColor: UIColor.init(red:239/255.0 , green: 88/255.0, blue: 88/255.0, alpha: 1.0)).withRenderingMode(.alwaysOriginal)
        arrow1.frame = CGRect( x: 0.0, y: 0.0, width: arrow.image!.size.width+10.0, height: 15);
        arrow1.contentMode = UIViewContentMode.center
        self.todatetf.rightView = arrow1;
        self.todatetf.rightViewMode = UITextFieldViewMode.always
        
        meternitybtn.isHidden = true
        meternitylbl.isHidden = true
        
       
    }
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right: break
            case UISwipeGestureRecognizerDirection.down: break
            case UISwipeGestureRecognizerDirection.left:
               self.Navigate()
                
            case UISwipeGestureRecognizerDirection.up: break
            default:
                break
            }
        }
    }
    
    func Navigate()
    {
        
         self.local_activityIndicator_start()
        // new version dispatch Que
        
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
            
            
            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
                
               self.local_activityIndicator_stop()
                let slideInFromLeftTransition = CATransition()
                slideInFromLeftTransition.type = kCAGravityTop
                slideInFromLeftTransition.subtype = kCATransitionFromLeft
                slideInFromLeftTransition.duration = 0.2
                slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "PermissionViewController") as! PermissionViewController
                self.navigationController?.view.layer.add(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
                self.navigationController?.pushViewController(lvc, animated: false)
            }
        }
    
    }
    func keyboardWillShow(notification: NSNotification)
    {
        
        
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true)
        {
        }
        else
        {
            if(Movedup==false)
            {
                 self.ScrollView.frame.origin.y -= self.view.frame.size.height / 4.44
                Movedup=true
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true)
        {
        }
        else
        {
           self.ScrollView.frame.origin.y  = self.view.frame.size.height / 8.89
            Movedup=false
        }
    }
    
    
    func addDoneButtonOnKeyboard()
        
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(LeaveViewController.doneButtonAction))
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        doneToolbar.items = items as AnyObject as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        pritf.inputAccessoryView = doneToolbar
        sicktf.inputAccessoryView = doneToolbar
        castf.inputAccessoryView = doneToolbar
        
    }
    func doneButtonAction()
     {
        var a : Double
        var b : Double
        var c : Double
        
        pritf.resignFirstResponder()
        sicktf.resignFirstResponder()
        castf.resignFirstResponder()
        
        if pritf.text != ""
        {
            let a = Double(pritf.text!)
          //  if ( a! % 0.5 == 0) {
            if (a! .truncatingRemainder(dividingBy: 0.5) == 0) {
                // remainder 0
                print("Remider 0 given in .5")
                lopdayslbl.text = totaldayslbl.text
                
            } else
            {
                pritf.text = ""
                showToast(message: "Please Enter either .5 or No of days")
                lopdayslbl.text = totaldayslbl.text
                priviledgeballbl.text =  priviledgelbl.text
                // b does not divide a evenly
            }
        }
        if sicktf.text != ""
        {
            let a = Double(sicktf.text!)
            if ( a! .truncatingRemainder(dividingBy: 0.5) == 0) {
                // remainder 0
                print("Remider 0 given in .5")
                lopdayslbl.text = totaldayslbl.text
            } else
            {
                sicktf.text = ""
                showToast(message: "Please Enter either .5 or No of days")
                lopdayslbl.text = totaldayslbl.text
                sickballabl.text =  sicklbl.text
                // b does not divide a evenly
            }
        }
        if castf.text != ""
        {
            let a = Double(castf.text!)
            if ( a!.truncatingRemainder(dividingBy: 0.5) == 0) {
                // remainder 0
                print("Remider 0 given in .5")
                lopdayslbl.text = totaldayslbl.text
            } else
            {
                castf.text = ""
                showToast(message: "Please Enter either .5 or No of days")
                lopdayslbl.text = totaldayslbl.text
                casualballbl.text =  casuallbl.text
                // b does not divide a evenly
            }
        }
        if pritf.text != ""
        {
            a = Double(pritf.text!)!
        }else{
            a = 0.0
        }
        if sicktf.text != ""
        {
            b = Double(sicktf.text!)!
        }else{
            b = 0.0
        }
        if castf.text != ""
        {
            c = Double(castf.text!)!
        }else{
            c = 0.0
        }
        lopdayslbl.text = String(Double(totaldayslbl.text!)! - (a + b + c))
        
        if Double(lopdayslbl.text!)! < 0.0 {
            showToast(message: "Please check the no of leaves Applied")
            lopdayslbl.text = totaldayslbl.text
            castf.text = ""
            sicktf.text = ""
            pritf.text = ""
            priviledgeballbl.text =  priviledgelbl.text
            sickballabl.text =  sicklbl.text
            casualballbl.text =  casuallbl.text
            reasontxtview.text = ""
        }

     }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
 
        if textField == fromdatetf
        {
            if fromdatetf.text != "" {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                datepicker1.minimumDate = dateFormatter.date(from: fromdatetf.text!)
            }else{
                print("sorry no text found in From date ")
            }
        }
        if textField == todatetf
        {
            if fromdatetf.text != "" {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                datepicker1.minimumDate = dateFormatter.date(from: fromdatetf.text!)
                bothuncheck()
            }else{
                print("sorry no text found in From date ")
            }
            
        }
        
        
        if(responsestring2.count > 0)
        {
            if textField == pritf
            {
                // self.pritf.userInteractionEnabled = false
                
                if fromdatetf.text != "" && todatetf.text != ""
                {
                    if(pritf.text == ".")
                    {
                        pritf.text = "0."
                        
                        
                    }
                    else if(pritf.text == "0..")
                    {
                        pritf.text = "0."
                    }
                    else
                    {
                        
                    }
                    if self.priviledgelbl.text == "0.0"
                    {
                        self.pritf.isUserInteractionEnabled = false
                    }
                    else
                    {
                        self.pritf.isUserInteractionEnabled = true
                        
                        if pritf.text == ""
                        {
                            let dic = self.responsestring2[0] as! NSDictionary
                            let total = dic.object(forKey: "PRIVILAGE LEAVE") as? Double
                            
                            let priviledge = total! + 0
                            self.priviledgeballbl.text = String(priviledge)
                            
                            
                            let total1 = Double(self.totaldayslbl.text!)
                            
                            let pri : Double!
                            var sic : Double!
                            var cas : Double!
                            
                            
                            if(pritf.text == "" )
                            {
                                pri = 0
                            }
                            else
                            {
                                pri = Double(self.pritf.text!)
                            }
                            if(sicktf.text == "" )
                            {
                                sic = 0
                            }
                            else
                            {
                                sic = Double(self.sicktf.text!)
                            }
                            if( castf.text == "")
                            {
                                cas = 0
                            }
                            else
                            {
                                cas = Double(self.castf.text!)
                            }
                            
                            let totleave = Double(pri) + Double(sic) + Double(cas)
                            
                            let priviledge1 = total1! - totleave
                            
                            if Double(totleave) > Double(total1!)
                            {
                                self.lopdayslbl.text = "0"
                            }
                            else
                            {
                                self.lopdayslbl.text = String(priviledge1)
                                
                            }
                        }
                        else{
                            let dic = self.responsestring2[0] as! NSDictionary
                            let total = dic.object(forKey: "PRIVILAGE LEAVE") as? Double
                            let myvalue = Double(self.pritf.text!)
                            
                            if Double(myvalue!) > Double(total!)
                            {
                                self.pritf.text = ""
                            }
                            else
                                
                            {
                                
                                let priviledge = Double(total!) - Double(myvalue!)
                                self.priviledgeballbl.text = String(priviledge)
                                
                                
                                let total1 = Double(self.totaldayslbl.text!)
                                let pri = Double(self.pritf.text!)
                                var sic : Double!
                                var cas : Double!
                                
                                if(sicktf.text == "" )
                                {
                                    sic = 0
                                }
                                else
                                {
                                    sic = Double(self.sicktf.text!)
                                }
                                if( castf.text == "")
                                {
                                    cas = 0
                                }
                                else
                                {
                                    cas = Double(self.castf.text!)
                                }
                                
                                let totleave = pri! + sic! + cas!
                                
                                let priviledge1 = total1! - totleave
                                
                                if Double(totleave) > Double(total1!)
                                {
                                    self.lopdayslbl.text = "0"
                                }
                                else
                                {
                                    self.lopdayslbl.text = String(priviledge1)
                                    
                                }
                                
                            }
                            
                        }
                    }
                    
                }
                else
                {
                    self.pritf.text = ""
                    
                }
                
            }
            else if textField == sicktf
            {
                
                
                if fromdatetf.text != "" && todatetf.text != ""
                {
                    if(sicktf.text == ".")
                    {
                        sicktf.text = "0."
                    }
                    else if(sicktf.text == "0..")
                    {
                        sicktf.text = "0."
                    }
                    else
                    {
                        
                    }
                    
                    if self.sicklbl.text == "0.0"
                    {
                        self.sicktf.isUserInteractionEnabled = false
                        self.sicktf.text = ""
                    }
                    else
                    {
                        self.sicktf.isUserInteractionEnabled = true
                        
                        if sicktf.text == ""
                        {
                            self.sicktf.isUserInteractionEnabled = true
                            let dic = self.responsestring2[0] as! NSDictionary
                            let total = dic.object(forKey: "SICK LEAVE") as? Double
                            
                            let priviledge = total! + 0
                            self.sickballabl.text = String(priviledge)
                            
                            
                            let total1 = Double(self.totaldayslbl.text!)
                            
                            let pri : Double!
                            var sic : Double!
                            var cas : Double!
                            
                            
                            if(pritf.text == "" )
                            {
                                pri = 0
                            }
                            else
                            {
                                pri = Double(self.pritf.text!)
                            }
                            if(sicktf.text == "" )
                            {
                                sic = 0
                            }
                            else
                            {
                                sic = Double(self.sicktf.text!)
                            }
                            if( castf.text == "")
                            {
                                cas = 0
                            }
                            else
                            {
                                cas = Double(self.castf.text!)
                            }
                            
                            let totleave = Double(pri) + Double(sic) + Double(cas)
                            
                            let Sick1 = total1! - totleave
                            
                            if Double(totleave) > Double(total1!)
                            {
                                self.lopdayslbl.text = "0"
                            }
                            else
                            {
                                self.lopdayslbl.text = String(Sick1)
                                
                            }
                        }
                        else{
                            let dic = self.responsestring2[0] as! NSDictionary
                            let total = dic.object(forKey: "SICK LEAVE") as? Double
                            let myvalue = Double(self.sicktf.text!)
                            
                            if Double(myvalue!) > Double(total!)
                            {
                                self.sicktf.text = ""
                            }
                            else
                            {
                                let priviledge = total! - myvalue!
                                self.sickballabl.text = String(priviledge)
                                
                                
                                let total1 = Double(self.totaldayslbl.text!)
                                var pri : Double!
                                let sic = Double(self.sicktf.text!)
                                var cas : Double!
                                
                                if(pritf.text == "" )
                                {
                                    pri = 0
                                }
                                else
                                {
                                    pri = Double(self.pritf.text!)
                                }
                                if( castf.text == "")
                                {
                                    cas = 0
                                }
                                else
                                {
                                    cas = Double(self.castf.text!)
                                }
                                
                                let totleave = pri! + sic! + cas!
                                
                                let sick = total1! - totleave
                                
                                if Double(totleave) > Double(total1!)
                                {
                                    self.lopdayslbl.text = "0"
                                }
                                else
                                {
                                    self.lopdayslbl.text = String(sick)
                                    
                                }
                                
                            }
                            
                        }
                    }
                    
                }
                else
                {
                    self.sicktf.text = ""
                }
                
            }
            else if textField == castf
            {
                
                if fromdatetf.text != "" && todatetf.text != ""
                {
                    if(castf.text == ".")
                    {
                        castf.text = "0."
                    }
                    else if(castf.text == "0..")
                    {
                        castf.text = "0."
                    }
                    else
                    {
                        
                    }
                    if self.casuallbl.text == "0.0"
                    {
                        self.castf.isUserInteractionEnabled = false
                        self.castf.text = ""
                    }
                    else
                    {
                        self.castf.isUserInteractionEnabled = true
                        
                        if castf.text == ""
                        {
                            self.castf.isUserInteractionEnabled = true
                            let dic = self.responsestring2[0] as! NSDictionary
                            let total = dic.object(forKey: "CASUAL LEAVE") as? Double
                            
                            let priviledge = total! + 0
                            self.casualballbl.text = String(priviledge)
                            
                            
                            let total1 = Double(self.totaldayslbl.text!)
                            
                            let pri : Double!
                            var sic : Double!
                            var cas : Double!
                            if(pritf.text == "" )
                            {
                                pri = 0
                            }
                            else
                            {
                                pri = Double(self.pritf.text!)
                            }
                            if(sicktf.text == "" )
                            {
                                sic = 0
                            }
                            else
                            {
                                sic = Double(self.sicktf.text!)
                            }
                            if( castf.text == "")
                            {
                                cas = 0
                            }
                            else
                            {
                                cas = Double(self.castf.text!)
                            }
                            
                            let totleave = Double(pri) + Double(sic) +  Double(cas)
                            
                            let casual = total1! - totleave
                            
                            if Double(totleave) > Double(total1!)
                            {
                                self.lopdayslbl.text = "0"
                            }
                            else
                            {
                                self.lopdayslbl.text = String(casual)
                                
                            }
                        }
                        else{
                            let dic = self.responsestring2[0] as! NSDictionary
                            let total = dic.object(forKey: "CASUAL LEAVE") as? Double
                            let myvalue = Double(self.castf.text!)
                            
                            if Double(myvalue!) > Double(total!)
                            {
                                self.castf.text = ""
                            }
                            else
                            {
                                let priviledge = total! - myvalue!
                                self.casualballbl.text = String(priviledge)
                                
                                
                                let total1 = Double(self.totaldayslbl.text!)
                                var pri : Double!
                                var sic : Double!
                                let cas = Double(self.castf.text!)
                                
                                if(pritf.text == "" )
                                {
                                    pri = 0
                                }
                                else
                                {
                                    pri = Double(self.pritf.text!)
                                }
                                if( sicktf.text == "")
                                {
                                    sic = 0
                                }
                                else
                                {
                                    sic = Double(self.sicktf.text!)
                                }
                                
                                let totleave = pri! + sic! + cas!
                                
                                let casal = total1! - totleave
                                
                                if Double(totleave) > Double(total1!)
                                {
                                    self.lopdayslbl.text = "0"
                                }
                                else
                                {
                                    self.lopdayslbl.text = String(casal)
                                    
                                }
                                
                            }
                            
                        }
                    }
                    
                }
                else
                {
                    self.castf.text = ""
                }
                
            }
        }
        else
        {
            self.showToast(message: "Delay in Server Response")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        self.navigationController?.setNavigationBarHidden(true, animated: false)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.Loaddata2()
        
//        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        appdelegate.shouldSupportAllOrientation = false
        
        ScrollView.isScrollEnabled=true
        ScrollView.contentSize = CGSize(width: ScrollView.frame.size.width, height:1000)
        self.navigationController?.setNavigationBarHidden(true, animated: false)

    }

    func customnavigation()
        
    {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x, y:  self.view.frame.origin.y,width: self.view.frame.width, height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(LeaveViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Leave/Permission"
        
    }
    
    func customnavigation1()
        
    {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x, y:  self.view.frame.origin.y, width: self.view.frame.width, height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        //customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), forState: UIControlState.Normal)
        customView.MenuBtn.addTarget(self, action: #selector(LeaveViewController.buttonClicked),for:UIControlEvents.touchUpInside)
       // customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),forControlEvents:UIControlEvents.TouchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.textAlignment = .center
        customView.HeaderLabel.font = UIFont.boldSystemFont(ofSize: 16.0)
        customView.HeaderLabel.text = "Leave/Permission"
        
    }
    
    func buttonClicked(sender:UIButton)
    {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "HRD1DetailsViewController")), animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
   
        textField.resignFirstResponder()
       pritf.resignFirstResponder()
        sicktf.resignFirstResponder()
        castf.resignFirstResponder()
        
        return true
    }
    @IBAction func Done(_ sender: AnyObject)
        
        
    {
        datepicker.isHidden = true
        datepicker1.isHidden = true
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        if( currenttextfield == "Date")
        {
            bothuncheck()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            fromdatetf.text = dateFormatter.string(from: datepicker.date)
            pickerview.isHidden = true
            self.Loaddata()
        }
        else if( currenttextfield=="TO")
        {
            bothuncheck()
            if fromdatetf.text != ""
            {
                pickerview.isHidden = true
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let Fdte = fromdatetf.text!
                datepicker1.minimumDate = dateFormatter.date(from: self.fromdatetf.text!)
                todatetf.text=dateFormatter.string(from: datepicker1.date)
                let Tdte2 = todatetf.text!
                date1 = dateFormatter.date(from: Fdte) as NSDate!
                date2 = dateFormatter.date(from: Tdte2) as NSDate!
                if date1.compare(date2 as Date) == .orderedAscending || date2.compare(date1 as Date) == .orderedSame{
                    self.Loaddata1()
                    let dte = fromdatetf.text!
                    let dte2 = todatetf.text!
                    date1 = dateFormatter.date(from: dte) as NSDate!
                    date2 = dateFormatter.date(from: dte2) as NSDate!
                    let diff = daysBetweenDates(startDate: date1,endDate: date2)
                    totaldiffer = diff + 1
                    totaldayslbl.text = String(Double(diff + 1))
                    lopdayslbl.text = String(Double(diff + 1))
                }else{
                    pickerview.isHidden = true
                    showToast(message: "provided worng date kingly check")
                    todatetf.text = ""
                }
                
            }
            
        }
   
    }
    func daysBetweenDates(startDate: NSDate, endDate: NSDate) -> Int
    {
        let calendar = NSCalendar.current
        let date1 = calendar.startOfDay(for: startDate as Date)
        let date2 = calendar.startOfDay(for: endDate as Date)
        let components =  calendar.dateComponents([.day], from: date1, to: date2)
        return components.day!
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
 
        
        if(textField.tag==1)
        {
            bothuncheck()
            
            fromdatetf.text = ""
            todatetf.text = ""
            pickerview.isHidden=false
            datepicker.isHidden = false
            datepicker1.isHidden = true
            datepicker.datePickerMode = UIDatePickerMode.date
            datepicker.locale = NSLocale(localeIdentifier: "en_GB") as Locale
            currenttextfield="Date"
            return false
        }
        
        if(textField.tag==2)
        {
            bothuncheck()
            if (fromdatetf.text == ""){
                print("nul value present in from date")
                pickerview.isHidden = true
                datepicker.isHidden = true
                datepicker1.isHidden = true
                self.NormalAlert(title: "Alert", Messsage: "Selece From Date First")
                return false
            }else{
                pickerview.isHidden=false
                datepicker.isHidden = true
                datepicker1.isHidden = false
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                datepicker1.minimumDate = dateFormatter.date(from: self.fromdatetf.text!)
                datepicker1.datePickerMode = UIDatePickerMode.date
                datepicker1.locale = NSLocale(localeIdentifier: "en_GB") as Locale
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                // datepicker.minimumDate = datepicker.date
                currenttextfield="TO"
                return false
            }
            
        }
        
        
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField.tag == 4)
        {
            let oldString = textField.text?.count == 0 ? string  : textField.text!
            let i = oldString.count
            let range = oldString.index(oldString.startIndex, offsetBy: i-1) ..< oldString.index(oldString.startIndex, offsetBy: i)
            let candidate = oldString.replacingCharacters(in: range, with: string)
            let regex = try? NSRegularExpression(pattern: "^\\d{0,2}(\\.\\d{0,2})?$", options: [])
            let value = regex?.firstMatch(in: candidate, options: [], range: NSRange(location: 0, length: candidate.count)) != nil
            return value
        }
        if(textField.tag == 5)
        {
            let oldString = textField.text?.count == 0 ? string  : textField.text!
            let i = oldString.count
            let range = oldString.index(oldString.startIndex, offsetBy: i-1) ..< oldString.index(oldString.startIndex, offsetBy: i)
            let candidate = oldString.replacingCharacters(in: range, with: string)
            let regex = try? NSRegularExpression(pattern: "^\\d{0,2}(\\.\\d{0,2})?$", options: [])
            let value = regex?.firstMatch(in: candidate, options: [], range: NSRange(location: 0, length: candidate.count)) != nil
            return value
        }
        if(textField.tag == 6)
        {
            let oldString = textField.text?.count == 0 ? string  : textField.text!
            let i = oldString.count
            let range = oldString.index(oldString.startIndex, offsetBy: i-1) ..< oldString.index(oldString.startIndex, offsetBy: i)
            let candidate = oldString.replacingCharacters(in: range, with: string)
            let regex = try? NSRegularExpression(pattern: "^\\d{0,2}(\\.\\d{0,2})?$", options: [])
            let value = regex?.firstMatch(in: candidate, options: [], range: NSRange(location: 0, length: candidate.count)) != nil
            return value
        }
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {

        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        let searchTerm = textView.text
        let characterset = NSCharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        if searchTerm?.rangeOfCharacter(from: characterset.inverted) != nil {
        }
        let currentCharacterCount = textView.text?.count ?? 0
        if range.length + range.location > currentCharacterCount {
            return false
        }
        let newLength = currentCharacterCount + text.count - range.length
        let allowedCharacters = " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,"
        let set = NSCharacterSet(charactersIn: allowedCharacters);
        let inverted = set.inverted;
        let filtered = text
            .components(separatedBy: inverted)
            .joined(separator: "");
        return (filtered == text)&&(newLength <= 120)
    }

    @IBAction func aftbtnclicked(_ sender: AnyObject)
    {
        if fromdatetf.text == ""
        {
            
        }
        else if todatetf.text == ""
        {
            
        }
        else if fromdatetf.text == "" && todatetf.text == ""
        {
            
        }
        else
        {
            
            let count = Double(totaldayslbl.text!)
            
            if isboxclicked == true
            {
                
                type = "1"
                aftnoonbtn.setImage(checkbox1, for:UIControlState.normal)
                print("after clciked")
                if count == 0.0
                {
                    
                    
                }
                else
                {
                    
                    totaldayslbl.text = String(count! - 0.5)
                    lopdayslbl.text = String(count! - 0.5)
                }
                
                isboxclicked = false
            }else
            {
                print("after unclciked")
                type = ""
                // dayscheck = false
                aftnoonbtn.setImage(uncheckbox1, for: UIControlState.normal)
                isboxclicked = true
                totaldayslbl.text = String(count! + 0.5)
                lopdayslbl.text = String(count! + 0.5)
            }
            if fromdatetf.text == todatetf.text{
                print("both value same")
                print(isboxclicked1)
                if isboxclicked1 == false{
                    print("forenoon Checked")
                    bothuncheck()
                }else{
                    
                }
                
            }
        }
        
        
    }
    
    
    
    
    @IBAction func forenoon(_ sender: AnyObject)
    {
        if fromdatetf.text == ""
        {
            
        }
        else if todatetf.text == ""
        {
            
        }
        else if fromdatetf.text == "" && todatetf.text == ""
        {
            
        }
        else{
            
            let count = Double(totaldayslbl.text!)
            
            if isboxclicked1 == true
            {
                
                type1="1"
                frntnoon.setImage(checkbox1, for:UIControlState.normal)
                print("fore clciked")
                
                if count == 0.0
                {
                }
                else
                {
                    totaldayslbl.text = String(count! - 0.5)
                    lopdayslbl.text = String(count! - 0.5)
                    print("pass the value")
                    
                }
                
                
                isboxclicked1 = false
            }else
            {
                print("fore unclciked")
                type1 = ""
                frntnoon.setImage(uncheckbox1, for: UIControlState.normal)
                isboxclicked1 = true
                totaldayslbl.text = String(count! + 0.5)
                lopdayslbl.text = String(count! + 0.5)
            }
            if fromdatetf.text == todatetf.text{
                print("both value same")
                print(isboxclicked1)
                if isboxclicked == false{
                    print("after Checked")
                    bothuncheck()
                }else{
                    
                }
                
            }
            
            
            
            
        }
        
        
    }
    
    func bothuncheck(){
        print("after unclciked")
        print("fore unclciked")
        print(totaldiffer)
        type1 = ""
        type = ""
        frntnoon.setImage(uncheckbox1, for: UIControlState.normal)
        isboxclicked1 = true
        aftnoonbtn.setImage(uncheckbox1, for: UIControlState.normal)
        isboxclicked = true
        self.totaldayslbl.text = String(totaldiffer)
        self.lopdayslbl.text   = String(totaldiffer)
        pritf.text = ""
        castf.text = ""
        sicktf.text = ""
        reasontxtview.text = ""
    }
    

    @IBAction func MeternityAction(_ sender: UIButton)
    {
        maternityview.isHidden = false
        if self.fromdatetf.text != ""
        {
            if isboxclicked1 == true
                
            {
                
                maternitybulletbtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
                micarriagebulletbtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
                self.pritf.isUserInteractionEnabled = false
                self.sicktf.isUserInteractionEnabled = false
                self.castf.isUserInteractionEnabled = false
                
                self.pritf.text = ""
                self.sicktf.text = ""
                self.castf.text = ""
                
                meternitybtn.setImage(checkbox1, for:UIControlState.normal)
                self.aftnoonbtn.isUserInteractionEnabled = false
                self.frntnoon.isUserInteractionEnabled = false
                
                let countDate = NSDate.changeDaysBy(days: 181)
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                self.todatetf.text = dateFormatter.string(from: countDate as Date)
                pickerview.isHidden = true
                
                isboxclicked1 = false
                totaldayslbl.text = "182"
                lopdayslbl.text = "0.0"
                
                MeternityChange = "181"
                todatetf.isUserInteractionEnabled = false
                
                
                
                
            }else
            {
                self.pritf.isUserInteractionEnabled = true
                self.sicktf.isUserInteractionEnabled = true
                self.castf.isUserInteractionEnabled = true
                maternityview.isHidden = true
                self.pritf.text = ""
                self.sicktf.text = ""
                self.castf.text = ""                                                                                                              
                
                meternitybtn.setImage(uncheckbox1, for: UIControlState.normal)
                
                self.aftnoonbtn.isUserInteractionEnabled = true
                self.frntnoon.isUserInteractionEnabled = true
                pickerview.isHidden = true
                totaldayslbl.text = "0.0"
                lopdayslbl.text = "0.0"
                self.todatetf.text = ""
                
                MeternityChange = "0"
                
                isboxclicked1 = true
            }
        }
        else
        {
            maternityview.isHidden = true
            self.NormalAlert(title: "Sorry", Messsage: "Please Enter the From Date")
        }
    }
    
    
    
    @IBAction func maternitybulletaction(_ sender: AnyObject)
    {
        maternitybulletbtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
        micarriagebulletbtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        
        
        let countDate = NSDate.changeDaysBy(days: 181)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        self.todatetf.text = dateFormatter.string(from: countDate as Date)
       // pickerview.hidden = true
        
        isboxclicked1 = false
        totaldayslbl.text = "182"
        lopdayslbl.text = "0.0"
        
        MeternityChange = "181"
        todatetf.isUserInteractionEnabled = false

        
    }
    
    
    
    
    
    @IBAction func miscarriagebulletaction(_ sender: AnyObject)
    {
        maternitybulletbtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        micarriagebulletbtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
        
        let countDate = NSDate.changeDaysBy(days: 41)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        self.todatetf.text = dateFormatter.string(from: countDate as Date)
       // pickerview.hidden = true
        
        isboxclicked1 = false
        totaldayslbl.text = "42"
        lopdayslbl.text = "0.0"
        
        MeternityChange = "42"
        todatetf.isUserInteractionEnabled = false
        


    }
    
    
    
    
    //Leave Apply
    
    @IBAction func submitAction(_ sender: AnyObject)
   {
    
 if((self.fromdatetf.text!.isEmpty || self.todatetf.text!.isEmpty ||  self.reasontxtview.text!.isEmpty))
        {
            self.NormalAlert(title: "Sorry", Messsage: "Please Enter the Fields")
          
        }
        else if((self.pritf.text!.isEmpty && self.sicktf.text!.isEmpty && self.castf.text!.isEmpty))
        {
            self.NormalAlert(title:  "Sorry", Messsage: "Please Enter Types of Leaves")
     
        }
    
        else
        {
          let noofdays = Double(self.totaldayslbl.text!)
            
            
            let pri : Double!
            var sic : Double!
            var cas : Double!
            
            
            if(pritf.text == "" )
            {
                pri = 0
            }
            else
            {
                pri = Double(self.pritf.text!)
            }
            if(sicktf.text == "" )
            {
                sic = 0
            }
            else
            {
                sic = Double(self.sicktf.text!)
            }
            if( castf.text == "")
            {
                cas = 0
            }
            else
            {
                cas = Double(self.castf.text!)
            }
            
            let totleave = Double(pri) + Double(sic) + Double(cas)
        
            if(Double(totleave) > Double(noofdays!) || Double(totleave) < Double(noofdays!))
            {
                self.NormalAlert(title: "Sorry", Messsage: "Total Days Mismatch..!")
           
            }
            
            else
            {
                
                
            //    var name = selecttf.text!.componentsSeparatedByString("-")
                    var name = selecttf.text!.components(separatedBy: "-")
                    let head: String = name[0]
                    uploadfile.setObject(LibraryAPI.sharedInstance.Userid.stringByRemovingWhitespaces as String , forKey: "UserId" as NSCopying)
                    uploadfile.setObject(fromdatetf.text!.stringByRemovingWhitespaces  as String, forKey: "FromDate" as NSCopying)
                    uploadfile.setObject(todatetf.text!.stringByRemovingWhitespaces  as String, forKey: "ToDate" as NSCopying)
                    uploadfile.setObject(totaldayslbl.text!.stringByRemovingWhitespaces  as String, forKey: "NoofDay" as NSCopying)
                    uploadfile.setObject(type1  as String, forKey: "ForenoonFlag" as NSCopying)
                    uploadfile.setObject(type  as String, forKey: "AfternoonFlag" as NSCopying)
                    uploadfile.setObject(pritf.text!.stringByRemovingWhitespaces  as String, forKey: "PrivilegeEarnedLeave" as NSCopying)
                    uploadfile.setObject(sicktf.text!.stringByRemovingWhitespaces  as String, forKey: "SickLeave" as NSCopying)
                    uploadfile.setObject(castf.text!.stringByRemovingWhitespaces  as String, forKey: "CasualLeave" as NSCopying)
                    uploadfile.setObject(MeternityChange as String, forKey: "MaternityLeave" as NSCopying)
                    uploadfile.setObject(""  as String, forKey: "TypeOfLeave" as NSCopying)
                    uploadfile.setObject(reasontxtview.text!.stringByRemovingWhitespaces  as String, forKey: "Reason" as NSCopying)
                    uploadfile.setObject(head.stringByRemovingWhitespaces  as String, forKey: "Authoriser" as NSCopying)
                    uploadfile.setObject(""  as String, forKey: "AUTHORIZED" as NSCopying)
                print("Leave Details",uploadfile)
                    self.local_activityIndicator_start()
                // new version dispatch Que
                DispatchQueue.global(qos: .background).async {
                     // self.uploaddata()
                 self.jsonresult =  CallFunction.shared.PostMethod(UrlString: "\(LibraryAPI.sharedInstance.GlobalUrl)LeaveDetails.asmx/LeaveApply", PostData: self.uploadfile)
                    DispatchQueue.main.async {
                       self.local_activityIndicator_stop()
                        print("Leave Viewcontroller upload process : ",self.jsonresult)
                            if(self.jsonresult.object(forKey: "ErrorMessage") as? String  == "")
                            {
                                let alertController = UIAlertController(title: "Success", message: "Your Ticket No : \(self.jsonresult.object(forKey: "LeaveAppNumber") as! String)" , preferredStyle: .alert)
                                
                                let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                    if LibraryAPI.sharedInstance.Globalflag == "N"
                                    {
                                        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "HRD1DetailsViewController")), animated: true)
                                    }
                                    else
                                    {
                                        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
                                    }
                                    
                                }
                                alertController.addAction(cancelAction)
                                self.present(alertController, animated: true, completion:nil)
                            }
                            else
                            {
                                let error = self.jsonresult.object(forKey: "ErrorMessage") as! String
                                let alertController = UIAlertController(title: "Alert", message:"\(error)" , preferredStyle: .alert)
                                let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                    self.fromdatetf.text = ""
                                    self.todatetf.text = ""
                                    self.pritf.text = ""
                                    self.sicktf.text = ""
                                    self.castf.text = ""
                                    self.reasontxtview.text = ""
                                    self.selecttf.text = ""
                                    self.totaldayslbl.text = "0.0"
                                    self.lopdayslbl.text = "0.0"
                                    self.frntnoon.setImage(self.uncheckbox1, for: UIControlState.normal)
                                    self.aftnoonbtn.setImage(self.uncheckbox1, for: UIControlState.normal)
                                    
                                }
                                alertController.addAction(cancelAction)
                                
                                
                                self.present(alertController, animated: true, completion:nil)
                            }
                        
                        
                       
                    }
                }
                    dismiss(animated: true, completion: nil)
                }
        }
    }
    
    
    
    func uploaddata()
    {
       // http://edi.redingtonb2b.in/whatsup-staging/LeaveDetails.asmx/LeaveApply
        
        let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)LeaveDetails.asmx/LeaveApply")! as URL)
        request.httpMethod = "POST"
        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
       
        request.httpBody = try? JSONSerialization.data(withJSONObject: self.uploadfile, options: JSONSerialization.WritingOptions(rawValue: 0))
        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
        do
        {
            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
            leaveresponse = String(data: data, encoding: String.Encoding.utf8)!
           jsonresult = try JSONSerialization.jsonObject(with: data, options:[]) as! NSDictionary
        }
        catch (let e)
        {
                print(e)
            
        }
        
    }
    func Loaddata()
    {

    // responsestring3 = LibraryAPI.sharedInstance.MyRequestUrl(url: "\(LibraryAPI.sharedInstance.GlobalUrl)LeaveDetails.asmx/HolidayCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Date=\(self.fromdatetf.text!)&Flag=")
        
        self.getStringsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)LeaveDetails.asmx/HolidayCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Date=\(self.fromdatetf.text!)&Flag=", completion: { (jsonmutable) in
         self.responsestring3 = jsonmutable
       
        if(self.responsestring3 == "")
        {
            
       
            
            
        }
        else
        {
            // new version dispatch Que
            
            DispatchQueue.global(qos: .background).async {
                
                // Validate user input
                
                
                // Go back to the main thread to update the UI
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Sorry", message:"\(self.responsestring3)", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler:
                        { (ACTION :UIAlertAction!)in
                            self.fromdatetf.text = ""
                            self.totaldayslbl.text = "0.0"
                            self.lopdayslbl.text = "0.0"
                            
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                }
            }
            
        
            
        }
         })
        
    }
    
   func Loaddata1()
    {
        
        self.getStringsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)LeaveDetails.asmx/HolidayCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Date=\(self.todatetf.text!)&Flag=1", completion: { (jsonstr) in
            self.responsestring3 = jsonstr
       
        
        if(self.responsestring3 == "")
        {
            
        }
        else
        {
            // new version dispatch Que
            
            DispatchQueue.global(qos: .background).async {
                
                // Validate user input
                
                
                // Go back to the main thread to update the UI
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Sorry", message:"\(self.responsestring3)", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler:
                        { (ACTION :UIAlertAction!)in
                            
                            self.todatetf.text = ""
                            self.totaldayslbl.text = "0.0"
                            self.lopdayslbl.text = "0.0"
                            
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                }
            }
            
            
        }
         })
    }
    
    
    //Leave Balance Enquiry
    func Loaddata2()
    {
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)LeaveDetails.asmx/LeaveBalanceEnquiry?UserID=\(LibraryAPI.sharedInstance.Userid)", completion: { (jsonmutable) in
            self.responsestring2 = jsonmutable
        if(self.responsestring2.count>0)
        {
            self.priviledge()
        }
        else
        {
                self.showToast(message: "Delay in Server Response")
        }
     })
        
    }
    
    func priviledge()
    {
        let dic = self.responsestring2[0] as! NSDictionary
        let priviledge  = dic.object(forKey: "PRIVILAGE LEAVE") as? Double
        let sick = dic.object(forKey: "SICK LEAVE")as? Double
        let casual  = dic.object(forKey: "CASUAL LEAVE") as? Double
        let gender  = dic.object(forKey: "GEND88") as? String
        self.priviledgelbl.text = String(priviledge!)
        self.sicklbl.text = String(sick!)
        self.casuallbl.text = String(casual!)
        self.priviledgeballbl.text = String(priviledge!)
        self.sickballabl.text = String(sick!)
        self.casualballbl.text = String(casual!)
        if gender == "M"
        {
            self.meternitybtn.isHidden = true
            self.meternitylbl.isHidden = true
        }
        else if gender == "F"
        {
            self.meternitybtn.isHidden = true
            self.meternitylbl.isHidden = true
        }
        else
        {
        }
    }

    @IBAction func cancelaction(_ sender: AnyObject)
    {pickerview.isHidden = true
    }

    @IBAction func permissionbtn(_ sender: AnyObject)
    {

        Navigate()
    }
    
    
    @IBAction func authbtnaction(_ sender: AnyObject)
        
    {
        self.view.endEditing(true)
        if(responsestring2.count>0)
        {
            let alert:UIAlertController=UIAlertController(title: "Choose option", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            let dic = self.responsestring2[0] as! NSDictionary
            let head1 = dic.object(forKey: "REPORTING HEAD 1") as! String
            let head2 = dic.object(forKey: "REPORTING HEAD 2") as! String
            let head3 = dic.object(forKey: "REPORTING HEAD 3") as! String
            
            var alert1 = UIAlertAction()
            var alert2 = UIAlertAction()
            var alert3 = UIAlertAction()
            
            if head1 != ""
            {
                alert1 = UIAlertAction(title: head1, style: UIAlertActionStyle.default)
                {
                    
                    UIAlertAction in
                    self.selecttf.text = head1
                }
                alert.addAction(alert1)
            }
            
            if head2 != ""
            {
                
                alert2 = UIAlertAction(title: head2, style: UIAlertActionStyle.default)
                {
                    
                    UIAlertAction in
                    self.selecttf.text = head2
                }
                alert.addAction(alert2)
            }
            
            if head3 != ""
            {
                alert3 = UIAlertAction(title: head3, style: UIAlertActionStyle.default)
                {
                    
                    UIAlertAction in
                    self.selecttf.text = head3
                }
                alert.addAction(alert3)
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
            {
                UIAlertAction in
                
                
            }
            
            alert.addAction(cancelAction)
            if UIDevice.current.userInterfaceIdiom == .phone
            {
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                
            }

        }
        else
        {
            self.showToast(message: "Delay in Server Response")
        }

    }
    
    
    @IBAction func cancellationbtnaction(_ sender: AnyObject)
    {
        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "LeaveStatusEnquiryViewController") as! LeaveStatusEnquiryViewController
        self.navigationController?.pushViewController(lvc, animated: true)
    }
    
    func notificationmethod()
    {
        let userdefaults = Foundation.UserDefaults.standard
        let value = userdefaults.dictionary(forKey: "Notification")
        
        var title: String!
        var message: String!
        var imagestr: String!

        title = value?["title"] as! String
        message = value?["body"] as! String
        imagestr = value?["imageurl"] as! String
        
        if imagestr != nil
        {
            if imagestr != ""
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: notificationimageViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = notificationimageViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                    
                }
                popup.show(childViewController: container)
            }
            else
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: PushnotificationtextViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = PushnotificationtextViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                }
                
                popup.show(childViewController: container)
            }
        }
        else
        {
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: PushnotificationtextViewController.instance())
                
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
            }
            let container = PushnotificationtextViewController.instance()
            container.closeHandler = { _ in
                popup.dismiss()
            }
            
            popup.show(childViewController: container)
        }
        
        
    }
}



extension String {
    var stringByRemovingWhitespaces: String {
        let component = components(separatedBy: .whitespaces)
        return component.joined(separator: "")
    }
}

extension NSDate {
    static func changeDaysBy(days : Int) -> NSDate {
        let currentDate = NSDate()
        let dateComponents = NSDateComponents()
        dateComponents.day = days
        //return NSCalendar.current.date(byAdding: , to: , options: )! as NSDate
        return NSCalendar.current.date(byAdding: dateComponents as DateComponents, to: currentDate as Date, wrappingComponents: true)! as NSDate
    }
}



