//
//  HrdViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 18/07/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

class HrdViewController: UIViewController,PopupContentViewController {
    @IBOutlet weak var Leavebtn: UIButton!
    @IBOutlet weak var salarybtn: UIButton!
    @IBOutlet weak var variablebtn: UIButton!
    @IBOutlet weak var leaveapprovalbtn: UIButton!
    @IBOutlet weak var biometricbtn: UIButton!
    var closeHandler: (() -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        Leavebtn.layer.cornerRadius         = 5
        Leavebtn.layer.borderWidth          = 1
        salarybtn.layer.cornerRadius        = 5
        salarybtn.layer.borderWidth         = 1
        variablebtn.layer.cornerRadius      = 5
        variablebtn.layer.borderWidth       = 1
        biometricbtn.layer.cornerRadius     = 5
        biometricbtn.layer.borderWidth      = 1
        leaveapprovalbtn.layer.cornerRadius = 5
        leaveapprovalbtn.layer.borderWidth  = 1
    }
    
    func close() {
        closeHandler?()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    class func instance() -> HrdViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "HrdViewController") as! HrdViewController
    }
    
    @IBAction func CloseAction(_ sender: AnyObject) {
        closeHandler?()
    }
    
    func sizeForPopup(popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: 230, height: 230)
    }
   
    @IBAction func leavebtnAction(_ sender: AnyObject) {
       self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
            // Validate user input
            //let responsestring = LibraryAPI.sharedInstance.Parsejson1(url: "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=WHATSUP&Option=6")
            self.getStringsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=WHATSUP&Option=6", completion: { (jsonstring) in
                let responsestring = jsonstring
                DispatchQueue.main.async {
                    if (responsestring == "Y") {
                        self.local_activityIndicator_stop()
                        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "LeaveViewController")), animated: true)
                        self.sideMenuViewController!.hideMenuViewController()
                        self.closeHandler?()
                    } else {
                        let alertController = UIAlertController(title: "Alert", message:" You are Not Authorised for this option" , preferredStyle: .alert)
                        let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(cancelAction)
                        self.local_activityIndicator_stop()
                        self.present(alertController, animated: true, completion:nil)
                        self.closeHandler?()
                        
                    }
                }
            })
        }
    }
    
    //------LeaveApproval
    @IBAction func salarybtnAction(_ sender: AnyObject) {
        self.local_activityIndicator_start()
        // new version dispatch Que
        DispatchQueue.global(qos: .background).async {
            // Validate user input
            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
                self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "LeaveApprovalViewController")),animated: true)
                self.sideMenuViewController!.hideMenuViewController()
               self.local_activityIndicator_stop()
                self.closeHandler?()
            }
        }
    }
    
     //------Biometric
    @IBAction func VariablebtnAction(_ sender: AnyObject) {
        let popup = PopupController
            .create(parentViewController: self.parent!)
            .show(childViewController: BioMetricViewController.instance())
            .didShowHandler { popup in
            }
            .didCloseHandler { _ in
                self.closeHandler?()
        }
        let container = BioMetricViewController.instance()
        container.closeHandler = { _ in
            self.closeHandler?()
            popup.dismiss()
        }
        popup.show(childViewController: container)
    }
    
     //------SalaryPayslip
    @IBAction func biometricAction(_ sender: AnyObject) {
        self.local_activityIndicator_start()
        LibraryAPI.sharedInstance.Monthyearpayslip = 1
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "PaySlipViewController")),animated: true)
         self.sideMenuViewController!.hideMenuViewController()
        self.local_activityIndicator_stop()
         closeHandler?()
    }

     //------VariablePayslip
    @IBAction func leaveApprovalClicked(_ sender: UIButton) {
             LibraryAPI.sharedInstance.Monthyearpayslip = 2
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "PaySlipViewController")),animated: true)
              self.sideMenuViewController!.hideMenuViewController()
              closeHandler?()
     }
}
