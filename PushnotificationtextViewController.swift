//
//  PushnotificationtextViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 08/06/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import Foundation
import KCFloatingActionButton

class PushnotificationtextViewController: UIViewController,PopupContentViewController,KCFloatingActionButtonDelegate
{
    
    @IBOutlet weak var titlelbl: UILabel!
    
    @IBOutlet weak var textbodytxtview: UITextView!
    
    var closeHandler: (() -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        notificationmethod()

        // Do any additional setup after loading the view.
    }
    
    func notificationmethod()
    {
        let userdefaults = Foundation.UserDefaults.standard
        let value = userdefaults.dictionary(forKey: "Notification")
        
        var title: String!
        var message: String!
        title = value?["title"] as! String
        message = value?["body"] as! String
        titlelbl.text = title
        textbodytxtview.text = message
            
    }
        
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    class func instance() -> PushnotificationtextViewController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "PushnotificationtextViewController") as! PushnotificationtextViewController
    }
    
    @IBAction func CloseAction(_ sender: AnyObject)
    {
        
        closeHandler?()
    }
    
    func sizeForPopup(popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: 300, height: 300)
    }
    
    
    @IBAction func okactionbtnAction(_ sender: AnyObject)
    {
        closeHandler?()
    }




}
