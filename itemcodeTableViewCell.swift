//
//  itemcodeTableViewCell.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 16/06/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class itemcodeTableViewCell: UITableViewCell {

    
    @IBOutlet weak var itemcodelbl: UILabel!
    
    @IBOutlet weak var vendorpartcodelbl: UILabel!
    
    @IBOutlet weak var itemdescriptionlbl: UILabel!
    
    @IBOutlet weak var cellview: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
