//
//  PartnerRegister2ViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 18/07/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class PartnerRegister2ViewController: UIViewController,UITextViewDelegate {
    
    @IBOutlet weak var SCROLLVIEW: UIScrollView!
    @IBOutlet weak var Remarkstxtview: UITextView!
    @IBOutlet weak var dealyesbtn: UIButton!
    @IBOutlet weak var dealnobtn: UIButton!
    @IBOutlet weak var panyesbtn: UIButton!
    @IBOutlet weak var pannobtn: UIButton!
    @IBOutlet weak var tinproofyesbtn: UIButton!
    @IBOutlet weak var tinnobtn: UIButton!
    @IBOutlet weak var Residenceyesbtn: UIButton!
    @IBOutlet weak var residencenobtn: UIButton!
    @IBOutlet weak var Partnershipyesbtn: UIButton!
    @IBOutlet weak var Partnernobtn: UIButton!
    @IBOutlet weak var certificateYesbtn: UIButton!
    @IBOutlet weak var certificateNobtn: UIButton!
    @IBOutlet weak var memorandumYesbtn: UIButton!
    @IBOutlet weak var MemorandumNobtn: UIButton!
    @IBOutlet weak var Articlesyesbtn: UIButton!
    @IBOutlet weak var ArticlesNobtn: UIButton!
    @IBOutlet weak var BankYesBtn: UIButton!
    @IBOutlet weak var BankNobtn: UIButton!
    @IBOutlet weak var FinanceYesBtn: UIButton!
    @IBOutlet weak var FinanceNoBtn: UIButton!
    @IBOutlet weak var MainView: UIView!
    
    var Movedup = Bool()
    var deal = String()
    var pan = String()
    var tin = String()
    var resident = String()
    var partnership = String()
    var certificate = String()
    var memorandum = String()
    var articles = String()
    var Bank = String()
    var financial = String()
    var uploadfile = NSMutableDictionary()
    var jsonresult = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customnavigation()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.SCROLLVIEW.contentSize = MainView.frame.size
        Remarkstxtview.layer.cornerRadius = 5.0
        Remarkstxtview.layer.borderWidth = 1.0
        self.Remarkstxtview.layer.borderColor = UIColor(red:211/255.0, green:211/255.0, blue:211/255.0, alpha: 1.0).cgColor
        Remarkstxtview.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(PartnerRegister2ViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PartnerRegister2ViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        //        appdelegate.shouldSupportAllOrientation = false
        deal = ""
        pan = ""
        tin = ""
        resident = ""
        partnership = ""
        certificate = ""
        memorandum = ""
        articles = ""
        Bank = ""
        financial = ""
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true) {
        } else {
            if(Movedup==false) {
                self.SCROLLVIEW.frame.origin.y -= 150
                Movedup=true
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true) {
        } else {
            self.SCROLLVIEW.frame.origin.y += 150
            Movedup=false
        }
    }
    
    func customnavigation() {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(PartnerRegister2ViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Partner Registration"
    }
    
    func buttonClicked(sender:UIButton) {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "PartnerRegister1ViewController")), animated: true)
    }
    
    
    @IBAction func dealeryesbtnAction(_ sender: AnyObject) {
        deal = "Y"
        dealyesbtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
        dealnobtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        
    }
    
    
    @IBAction func NobtnAction(_ sender: AnyObject) {
        deal = "N"
        dealyesbtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        dealnobtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
    }
    
    
    
    @IBAction func panYesbtnAction(_ sender: AnyObject) {
        pan = "Y"
        panyesbtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
        pannobtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
    }
    
    
    @IBAction func PannobtnAction(_ sender: AnyObject) {
        pan = "N"
        panyesbtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        pannobtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
    }
    
    
    @IBAction func tinYesbtnaction(_ sender: AnyObject) {
        tin = "Y"
        tinproofyesbtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
        tinnobtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
    }
    
    @IBAction func TinNobtn(_ sender: AnyObject) {
        tin = "N"
        tinproofyesbtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        tinnobtn.setImage(UIImage(named:"green2.png"),for: UIControlState.normal)
    }
    
    
    
    
    @IBAction func ResidenceYesbtnAction(_ sender: AnyObject) {
        resident = "Y"
        Residenceyesbtn.setImage(UIImage(named:"green2.png"),for: UIControlState.normal)
        residencenobtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
    }
    
    
    @IBAction func ResidenceNobtnAction(_ sender: AnyObject) {
        resident = "N"
        Residenceyesbtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        residencenobtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
    }
    
    
    @IBAction func PartneryesbtnAction(_ sender: AnyObject) {
        partnership = "Y"
        Partnershipyesbtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
        Partnernobtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
    }
    
    
    @IBAction func PartnernobtnAction(_ sender: AnyObject) {
        partnership = "N"
        Partnershipyesbtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        Partnernobtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
    }
    
    
    
    @IBAction func CertificationyesbtnAction(_ sender: AnyObject) {
        certificate = "Y"
        certificateYesbtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
        certificateNobtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        
    }
    
    
    @IBAction func CertificationNobtnAction(_ sender: AnyObject) {
        certificate = "N"
        certificateYesbtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        certificateNobtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
    }
    
    
    @IBAction func MemorandumyesbtnAction(_ sender: AnyObject) {
        memorandum = "Y"
        memorandumYesbtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
        MemorandumNobtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
    }
    
    @IBAction func MemorandumNobtnAction(_ sender: AnyObject) {
        memorandum = "N"
        memorandumYesbtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        MemorandumNobtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
    }
    @IBAction func ArticlesYesbtnAction(_ sender: AnyObject) {
        articles = "Y"
        Articlesyesbtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
        ArticlesNobtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
    }
    
    @IBAction func ArticlesNobtnAction(_ sender: AnyObject) {
        articles = "N"
        Articlesyesbtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        ArticlesNobtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
    }
    
    @IBAction func BankStatementyesbtnAction(_ sender: AnyObject) {
        Bank = "Y"
        BankYesBtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
        BankNobtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        
    }
    
    @IBAction func BankstatementnobtnAction(_ sender: AnyObject) {
        Bank = "N"
        BankYesBtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        BankNobtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
    }
    
    
    
    @IBAction func FinancialYesbtnAction(_ sender: AnyObject) {
        financial = "Y"
        FinanceYesBtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
        FinanceNoBtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        
    }
    
    @IBAction func FinanceNobtnAction(_ sender: AnyObject) {
        financial = "N"
        FinanceYesBtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        FinanceNoBtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
        
    }
    
    @IBAction func SubmitBtnAction(_ sender: AnyObject) {
        if(deal == "") {
            self.NormalAlert(title: "Alert", Messsage: "Please Select the Deal Option")
        } else if(pan == "" ) {
            self.NormalAlert(title: "Alert", Messsage: "Please Select the pan Option")
        } else if(tin == "") {
            self.NormalAlert(title: "Alert", Messsage: "Please Select the tin Option")
        } else if(resident == "") {
            self.NormalAlert(title: "Alert", Messsage: "Please Select the resident Option")
        } else if(partnership ==  "") {
            self.NormalAlert(title: "Alert", Messsage: "Please Select the partnership Option")
        } else if(certificate == "") {
            self.NormalAlert(title: "Alert", Messsage: "Please Select the certificate Option")
        } else if(memorandum == "") {
            self.NormalAlert(title: "Alert", Messsage: "Please Select the memorandum Option")
        } else if(articles == "") {
            self.NormalAlert(title: "Alert", Messsage: "Please Select the articles Option")
        } else if(Bank == "") {
            self.NormalAlert(title: "Alert", Messsage: "Please Select the Bank Option")
        } else if(financial == "") {
            self.NormalAlert(title: "Alert", Messsage: "Please Select the Financial Option")
        } else if (Remarkstxtview.text == "" || Remarkstxtview.text == nil) {
            self.NormalAlert(title: "Alert", Messsage: "Please Enter the Remarks")
        } else {
            let CustomerName = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "companyname")as! String
            let CustomerAddr1 = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "Address11")as! String
            let CustomerAddr2 = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "address12")as! String
            let CustomerAddr3 = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "address13")as! String
            let CustomerCity = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "city1")as! String
            let CustomerState = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "state1")as! String
            let PINCode = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "pincode1")as! String
            let CreditLimit = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "creditlimit3")as! String
            let PhoneNo = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "phonenumber2")as! String
            let PANNo = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "panumber")as! String
            let FaxNo1 = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "faxnumber")as! String
            let ContactPerson = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "Contactname2")as! String
            let EMailId = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "contactmail2")as! String
            let LSTNumber = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "gst3")as! String
            let CustomerProfile = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "profile")as! String
            let CustomerDivision = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "division_it")as! String
            let TypeofConcern = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "concern")as! String
            let ResidenceAddr1Ln1 = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "Address41")as! String
            let ResidenceAddr1Ln2 = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "Address42")as! String
            let ResidenceAddr1Ln3 = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "Address43")as! String
            let ResidenceCity1 = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "Ctytf41")as! String
            let ResidenceState1 = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "statetf41")as! String
            let ResidenceAddr2Ln1 = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "addresstf421")as! String
            let ResidenceAddr2Ln2 = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "address422")as! String
            let ResidenceAddr2Ln3 = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "address423")as! String
            let ResidenceCity2 = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "citytf421")as! String
            let ResidenceState2 = LibraryAPI.sharedInstance.partnerglobaldic.object(forKey: "statetf422")as! String
            uploadfile.setObject( LibraryAPI.sharedInstance.PartnerBranchCodetf, forKey: "BranchCode" as NSCopying)
            uploadfile.setObject(CustomerName, forKey: "CustomerName" as NSCopying)
            uploadfile.setObject(CustomerAddr1, forKey: "CustomerAddr1" as NSCopying)
            uploadfile.setObject(CustomerAddr2, forKey: "CustomerAddr2" as NSCopying)
            uploadfile.setObject(CustomerAddr3, forKey: "CustomerAddr3" as NSCopying)
            uploadfile.setObject(CustomerCity, forKey: "CustomerCity" as NSCopying)
            uploadfile.setObject(CustomerState, forKey: "CustomerState" as NSCopying)
            uploadfile.setObject(PINCode, forKey: "PINCode" as NSCopying)
            uploadfile.setObject(CreditLimit, forKey: "CreditLimit" as NSCopying)
            uploadfile.setObject(PhoneNo, forKey: "PhoneNo" as NSCopying)
            uploadfile.setObject(PANNo, forKey: "PANNo" as NSCopying)
            uploadfile.setObject(FaxNo1, forKey: "FaxNo1" as NSCopying)
            uploadfile.setObject(ContactPerson, forKey: "ContactPerson" as NSCopying)
            uploadfile.setObject(EMailId, forKey: "EMailId" as NSCopying)
            uploadfile.setObject(LSTNumber, forKey: "LSTNumber" as NSCopying)
            uploadfile.setObject(CustomerProfile, forKey: "CustomerProfile" as NSCopying)
            uploadfile.setObject(CustomerDivision, forKey: "CustomerDivision" as NSCopying)
            uploadfile.setObject(TypeofConcern, forKey: "TypeofConcern" as NSCopying)
            uploadfile.setObject(ResidenceAddr1Ln1, forKey: "ResidenceAddr1Ln1" as NSCopying)
            uploadfile.setObject(ResidenceAddr1Ln2, forKey: "ResidenceAddr1Ln2" as NSCopying)
            uploadfile.setObject(ResidenceAddr1Ln3, forKey: "ResidenceAddr1Ln3" as NSCopying)
            uploadfile.setObject(ResidenceCity1, forKey: "ResidenceCity1" as NSCopying)
            uploadfile.setObject(ResidenceState1, forKey: "ResidenceState1" as NSCopying)
            uploadfile.setObject(ResidenceAddr2Ln1, forKey: "ResidenceAddr2Ln1" as NSCopying)
            uploadfile.setObject(ResidenceAddr2Ln2, forKey: "ResidenceAddr2Ln2" as NSCopying)
            uploadfile.setObject(ResidenceAddr2Ln3, forKey: "ResidenceAddr2Ln3" as NSCopying)
            uploadfile.setObject(ResidenceCity2, forKey: "ResidenceCity2" as NSCopying)
            uploadfile.setObject(ResidenceState2, forKey: "ResidenceState2" as NSCopying)
            uploadfile.setObject(deal, forKey: "Form1available" as NSCopying)
            uploadfile.setObject(pan, forKey: "Form2available" as NSCopying)
            uploadfile.setObject(tin, forKey: "Form3available" as NSCopying)
            uploadfile.setObject(resident, forKey: "Form4available" as NSCopying)
            uploadfile.setObject(partnership, forKey: "Form5available" as NSCopying)
            uploadfile.setObject(certificate, forKey: "Form6available" as NSCopying)
            uploadfile.setObject(memorandum, forKey: "Form7available" as NSCopying)
            uploadfile.setObject(articles, forKey: "Form8available" as NSCopying)
            uploadfile.setObject(Bank, forKey: "Form9available" as NSCopying)
            uploadfile.setObject(financial, forKey: "Form10available" as NSCopying)
            uploadfile.setObject(Remarkstxtview.text, forKey: "FormRemarks" as NSCopying)
            uploadfile.setObject("A", forKey: "StatusFlag" as NSCopying)
            uploadfile.setObject(LibraryAPI.sharedInstance.Userid, forKey: "RequestedUser" as NSCopying)
            uploaddata()
            if(jsonresult.count > 0) {
                let Status = jsonresult.object(forKey: "Status")as! String
                let ReferenceNumber = jsonresult.object(forKey: "ReferenceNumber")as! String
                if(Status == "Success") {
                    if(ReferenceNumber != "") {
                        let alertController = UIAlertController(title: "Alert", message: "Partner Register Is updated SuccessFully....!  \(ReferenceNumber)" , preferredStyle: .alert)
                        let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
                            LibraryAPI.sharedInstance.partnerglobaldic.removeAllObjects()
                        }
                        alertController.addAction(cancelAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                } else {
                    self.NormalAlert(title: "Alert", Messsage: "Submission Failed, Retry")
                }
            }
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        let searchTerm = textView.text
        let characterset = NSCharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        if searchTerm?.rangeOfCharacter(from: characterset.inverted) != nil{
        }
        let currentCharacterCount = textView.text?.count ?? 0
        if range.length + range.location > currentCharacterCount {
            return false
        }
        let newLength = currentCharacterCount + text.count - range.length
        let allowedCharacters = " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,"
        let set = NSCharacterSet(charactersIn: allowedCharacters);
        let inverted = set.inverted;
        let filtered = text.components(separatedBy: inverted).joined(separator: "")
        return (filtered == text)&&(newLength <= 75)
    }
    
    func uploaddata() {
        //http://edi.redingtonb2b.in/Whatsup-staging/pARTNER.ASMX/PartnerCreation
        let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)pARTNER.ASMX/PartnerCreation")! as URL)
        request.httpMethod = "POST"
        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
        request.httpBody = try? JSONSerialization.data(withJSONObject: self.uploadfile, options: JSONSerialization.WritingOptions(rawValue: 0))
        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
        do {
            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
            jsonresult = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary        }
        catch (let e) {
            print(e)
        }
    }
}
