//
//  LeaveBalance_Month.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 26/07/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import Foundation  


struct LeaveBalance_Month {
  
    
    var LEAVE_DATE: String
    var DAY: NSNumber
    var OUT_TIME: String
    var REMARK: String
    var STATUS: String
    var IN_TIME: String
    
    
    init(){
        self.LEAVE_DATE = ""
        self.DAY = 0
        self.OUT_TIME = ""
        self.REMARK = ""
        self.STATUS = ""
        self.IN_TIME = ""
        
    }
    
    init(data : AnyObject){
        
        if let temp = data["LEAVE DATE"] as? String {
            self.LEAVE_DATE = temp
        }else {
            self.LEAVE_DATE = ""
        }
        
        if let temp = data["DAY"] as? NSNumber {
            self.DAY = temp
        }else {
            self.DAY = 0
        }
        
        if let temp = data["OUT TIME"] as? String {
            self.OUT_TIME = temp
        }else {
            self.OUT_TIME = ""
        }
        
        if let temp = data["REMARK"] as? String {
            self.REMARK = temp
        }else {
            self.REMARK = ""
        }
        
        if let temp = data["STATUS"] as? String {
            self.STATUS = temp
        }else {
            self.STATUS = ""
        }
        
        if let temp = data["IN TIME"] as? String {
            self.IN_TIME = temp
        }else {
            self.IN_TIME = ""
        }
        
    }
}
