//
//  qtr2ViewController.swift
//  Redington
//
//  Created by RIL-ITTECH on 13/02/17.
//  Copyright © 2017 truetech. All rights reserved.
//

//
//  BusinesswiseViewController.swift
//  Redington
//
//  Created by truetech on 12/08/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import KCFloatingActionButton

class qtr2ViewController: UIViewController,KCFloatingActionButtonDelegate,UICollectionViewDataSource,UICollectionViewDelegate {
    
    @IBOutlet var menubtn: UIButton!
    @IBOutlet var backbtn: UIButton!
    
    
    var months=[String]()
    var pievalues=[Double]()
    var PiechartPercentage=NSNumber()
    var isanimated=Bool()
    var isreloaded=Bool()
    var Isfabclicked=Bool()
    @IBOutlet var DashBoardCollectionView: UICollectionView!
    var collectionViewLayout: CustomImageFlowLayout!
    var responsestring1 = NSMutableArray()
    var fab=KCFloatingActionButton() //do it
    @IBOutlet weak var Popupview: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.customnavigation()
        
        isanimated=false
        
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(TargetAcheivementViewController.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        
        
        
        
        self.layoutFAB() //do it
        self.FetchValues()
        
        collectionViewLayout = CustomImageFlowLayout()
        DashBoardCollectionView.collectionViewLayout = collectionViewLayout
        DashBoardCollectionView.backgroundColor = UIColor.clear
        isreloaded=false
        
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.init(red:17/255.0 , green: 121/255.0, blue: 32/255.0, alpha: 1.0)], for:.selected)
      
    }
    
    func FetchValues()
    {
        self.local_activityIndicator_start()
        // new version dispatch Que
        
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
            
            //  self.responsestring1 = LibraryAPI.sharedInstance.RequestUrl(url: "\(LibraryAPI.sharedInstance.GlobalUrl)SalesDetails.asmx/BusinessSalesTargetDetails_QTR?UserID=\(LibraryAPI.sharedInstance.Userid)&BranchCode=\(LibraryAPI.sharedInstance.BranchCodeNew)&Qtr=2&Year=2017")
            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)SalesDetails.asmx/BusinessSalesTargetDetails_QTR?UserID=\(LibraryAPI.sharedInstance.Userid)&BranchCode=\(LibraryAPI.sharedInstance.BranchCodeNew)&Qtr=2&Year=2017", completion: { (jsonmutable) in
                 self.responsestring1 = jsonmutable
            })
            if(self.responsestring1.count>0)
            {
                   DispatchQueue.main.async {
                    self.DashBoardCollectionView.isHidden=false
                    self.Popupview.isHidden=true
                }
            }
            else
            {
                  DispatchQueue.main.async {
                    self.DashBoardCollectionView.isHidden=true
                    self.Popupview.isHidden=false
                    
                }
            }

            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
                self.local_activityIndicator_stop()
                self.DashBoardCollectionView.reloadData()
                
            }
        }
        
    }
    
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                
                self.Navigate()
            case UISwipeGestureRecognizerDirection.down: break
            case UISwipeGestureRecognizerDirection.left: break
                
                
            case UISwipeGestureRecognizerDirection.up: break
            default:
                break
            }
        }
    }
    
    
    override func viewWillLayoutSubviews() {
        let orientation=UIDevice.current.orientation
        if(orientation.isLandscape)
        {
            if(Isfabclicked==true)
            {
                
            }
            else
            {
                DashBoardCollectionView.reloadData()
                Isfabclicked=false
            }
        }
            
            
        else
        {
            if(Isfabclicked==true)
            {
                
            }
            else
            {
                DashBoardCollectionView.reloadData()
                Isfabclicked=false
            }
            
            
        }
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
      self.local_activityIndicator_stop()
//        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        appdelegate.shouldSupportAllOrientation = true
    }
    
       
    func KCFABOpened(fab: KCFloatingActionButton) {
        
        self.Isfabclicked=true
    }
    
    func layoutFAB() {
        
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            
            self.Isfabclicked=false
            if (self.navigationController?.topViewController!.isKind(of: DashBoardViewController.classForCoder()) != nil) {
                
                self.navigationController?.popToViewController((self.navigationController?.viewControllers.first)!, animated: true)
            }
            else
            {
                let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                self.navigationController?.pushViewController(lvc, animated: false)
                
            }
            
            
            self.fab.close()
        }
        
        fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
            
            self.Isfabclicked=false
            
            self.FetchValues()
            self.fab.close()
        }
        
        fab.fabDelegate = self
        
        self.view.addSubview(fab)
        
    }
    
    func customnavigation()
        
    {
        
        backbtn.setImage(UIImage(named:"BACK.png"), for: UIControlState.normal)
        menubtn.setImage(UIImage(named:"menu.png"), for: UIControlState.normal)
        backbtn.addTarget(self, action: #selector(BusinesswiseViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        menubtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        
    }
    
    func buttonClicked(sender:UIButton)
    {
        LibraryAPI.sharedInstance.tracker="1"
     /*   if (self.navigationController?.topViewController!.isKindOfClass(DashBoardViewController) != nil) {
            self.navigationController?.popToViewController((self.navigationController?.viewControllers.first)!, animated: true)
        }*/
        //else
        //{
            let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "qtrViewController") as! qtrViewController
            self.navigationController?.pushViewController(lvc, animated: false)
            
        //}
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //Collectionview delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return responsestring1.count
    }
    
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! DashBoardCollectionViewCell
        
        let dic = responsestring1[indexPath.row] as! NSDictionary
        let qtr = dic.object(forKey: "Quarter Wise") as? String
        
        let type = dic.object(forKey: "Financial Year") as? String
        
        // var sale = responsestring1.objectAtIndex(indexPath.row).objectForKey("Sales Achieved") as? String
        
        cell.qtr2lbl.text = "Quarter"+qtr!+" "+type!+" "
        
        
        let percentage=dic.object(forKey: "Percentage Achieved")as? NSNumber
        cell.TitleLabel.text=dic.object(forKey: "Business Description") as? String
        
        let num=NumberFormatter()
        num.numberStyle=NumberFormatter.Style.currency
        num.locale = NSLocale(localeIdentifier: "en_IN") as Locale!
        let str=num.string(from: NSNumber(value:(dic.object(forKey: "Sales Target") as! Double)))
        
        
        
        let num1=NumberFormatter()
        num1.numberStyle=NumberFormatter.Style.currency
        num1.locale = NSLocale(localeIdentifier: "en_IN") as Locale!
        let str1=num.string(from: NSNumber(value:(dic.object(forKey: "Sales Achieved") as! Double)))
        
        
        cell.SalesAcheivedLabel.text=str1
        cell.SalesTargetLabel.text=str
       // cell.piechartcell.holeRadiusPercent=0.8
        cell.setval(Floatval: pievalues)
        
        if(isanimated==false)
        {
            
            if((percentage?.intValue)!<100)
            {
                months=["",""]
                pievalues = [(percentage?.doubleValue)!,100-Double(percentage!)]
            }
            else
            {
                months=["",""]
                pievalues = [100,0]
            }
           // cell.piechartcell.centerText="\(String(dic.object(forKey: "Percentage Achieved")as! NSNumber.IntegerLiteralType))%"
           // cell.setChart(dataPoints: months, values: pievalues, isanimated: false)
          //  cell.piechartcell.descriptionText=""
            cell.setval(Floatval: pievalues)
        }
        else
        {
            if((percentage?.intValue)!<100)
            {
                months=["",""]
                pievalues = [(percentage?.doubleValue)!,100-Double(percentage!)]
            }
            else
            {
                months=["",""]
                pievalues = [100,0]
            }
          //  cell.piechartcell.centerText="\(String(dic.object(forKey: "Percentage Achieved")as! NSNumber.IntegerLiteralType))%"
         //   cell.setChart(dataPoints: months, values: pievalues, isanimated: true)
          //  cell.piechartcell.descriptionText=""
            cell.setval(Floatval: pievalues)
        }
        
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    
        let lastRowIndex = collectionView.numberOfItems(inSection: collectionView.numberOfSections-1)
        
        if (indexPath.row == lastRowIndex - 1) {
            isanimated=true
            //            cell.prepareForReuse()
        }
        
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        // return CGSize(width: collectionView.frame.size.width/2.0 - 8, height:250)
        
        let orientation = UIApplication.shared.statusBarOrientation
        if(orientation == .landscapeLeft || orientation == .landscapeRight)
        {
            return CGSize(width: collectionView.frame.size.width/2.0-8, height:250)
        }
        else{
            return CGSize(width: collectionView.frame.size.width/2.0-8, height:250)
        }
    }
    
   
    
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(5,5,5,5) // margin between cells
    }
    
    func collectionView(collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
        minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 10.0
    }
    
    override func viewDidLayoutSubviews() {
        
    }
    
    func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        
        switch item.tag {
            
        case 1:
            
            self.Navigate()
            
            // self.view.insertSubview(viewController1!.view!, belowSubview: self.mainTabBar)
            break
            
            
        case 2:
            
            
            
            //   self.view.insertSubview(viewController1!.view!, belowSubview: self.mainTabBar)
            break
            
        default:
            break
            
        }
    }
    
    func Navigate()
    {
        let slideInFromLeftTransition = CATransition()
        slideInFromLeftTransition.type = kCAGravityTop
        slideInFromLeftTransition.subtype = kCATransitionFromLeft
        slideInFromLeftTransition.duration = 0.2
        slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "TargetAcheivementViewController") as! TargetAcheivementViewController
        self.navigationController?.view.layer.add(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
        self.navigationController?.pushViewController(lvc, animated: false)
        
        
    }
    
    
  
    
}
