//
//  pdfmodel.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 06/04/18.
//  Copyright © 2018 truetech. All rights reserved.
//

import Foundation
struct pdfmodel
{
    var Filename: String
    var ActualURL: String
    var FileSize: String
    init() {
        self.Filename = ""
        self.ActualURL = ""
        self.FileSize = ""
    }
    
    init(data : AnyObject)
    {
        if let temp = data["Filename"] as? String {
            self.Filename = temp
        }else {
            self.Filename = ""
        }
        
        if let temp = data["ActualURL"] as? String {
            self.ActualURL = temp
        }else {
            self.ActualURL = ""
        }
        if let temp = data["FileSize"] as? String {
            self.FileSize = temp
        }else {
            self.FileSize = ""
        }

        
    }
    
}
