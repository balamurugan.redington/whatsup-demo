//
//  OrderTableViewCell.swift
//  Redington
//
//  Created by truetech on 25/08/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit

class OrderTableViewCell: UITableViewCell {

    @IBOutlet weak var StockLabel: UILabel!
    @IBOutlet weak var PriceTf: UITextField!
    @IBOutlet var available: UILabel!
    @IBOutlet var productnames: UILabel!
    @IBOutlet var price: UILabel!
    @IBOutlet var Invdates: UILabel!
    @IBOutlet var itemcode: UILabel!
    @IBOutlet var Cartbtn: UIButton!
    @IBOutlet var Quantitytf: UITextField!
    @IBOutlet weak var Availablelabel: UILabel!
 
    @IBOutlet weak var contentview1: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        

        // Configure the view for the selected state
    }

    
    func addDoneButtonOnKeyboardPrice()
    {
        PriceTf.keyboardType = .numbersAndPunctuation
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0,y: 0,width: 320,height: 50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(OrderTableViewCell.doneButtonPriceAction))
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as AnyObject as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        PriceTf.inputAccessoryView = doneToolbar
        
        
    }
    func doneButtonPriceAction()
    {
        
        PriceTf.resignFirstResponder()
        
    }

    
    
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0,y: 0,width: 320,height: 50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(OrderTableViewCell.doneButtonAction))
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as AnyObject as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        Quantitytf.inputAccessoryView = doneToolbar
    
        
    }
    func doneButtonAction()
    {
        
        Quantitytf.resignFirstResponder()
       
    }

}
