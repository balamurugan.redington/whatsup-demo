//
//  OrderdetailsSubTableViewCell.swift
//  Redington
//
//  Created by TRUETECHMAC3 on 28/12/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit

class OrderdetailsSubTableViewCell: UITableViewCell {

    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var labeltxtCity: UILabel!
    @IBOutlet var labelItemCode: UILabel!
    @IBOutlet var labletxtQty: UILabel!
    @IBOutlet var labelQty: UILabel!
    @IBOutlet var labletxtPartCode: UILabel!
    @IBOutlet var labelPartCode: UILabel!
    @IBOutlet var labeltxtUnitPrice: UILabel!
    @IBOutlet var labelUnitPrice: UILabel!
    @IBOutlet var labeltxtLineValue: UILabel!
    @IBOutlet var labelLineValue: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
