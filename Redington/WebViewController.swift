//
//  WebViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 06/04/18.
//  Copyright © 2018 truetech. All rights reserved.
//

import UIKit
import WebKit
import QuickLook

class WebViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIDocumentInteractionControllerDelegate,WKUIDelegate, UIWebViewDelegate, QLPreviewControllerDelegate {
    
    @IBOutlet weak var pdftableview: UITableView!
    @IBOutlet weak var downloaderview: UIView!
    @IBOutlet weak var pdfwebview: UIWebView!
    @IBOutlet weak var downloadbtn: UIButton!
    
    var pdf_dic = NSMutableDictionary()
    var documentInteractionController: UIDocumentInteractionController?
    var filesize = String()
    var datastr = String()
    var back = NSNumber()
    var filelesizearray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        downloadbtn.isHidden = true
        pdfwebview.scalesPageToFit = true
        pdfwebview.contentMode =  UIViewContentMode.scaleToFill
        pdfwebview.delegate = self
        pdfwebview.allowsLinkPreview = true
        customnavigation()
        downloadbtn.layer.cornerRadius = 5
        downloadbtn.clipsToBounds = true
        pdftableview.delegate = self
        pdftableview.dataSource = self
        downloaderview.isHidden = true
        pdftableview.register(UINib(nibName: "pdfTableViewCell", bundle: nil), forCellReuseIdentifier: "pdfTableViewCell")
        pdftableview.tableFooterView = UIView(frame: .zero)
        pdftableview.separatorStyle = .singleLine
        pdfwebview.backgroundColor = UIColor.white
        loaddata()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    func customnavigation()
    {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: 65)
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(WebViewController.buttonClicked(_:)),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = " Quick Reference Guide"
    }
    
    func buttonClicked(_ sender:UIButton)
    {
        if back == 1
        {
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "WebViewController")), animated: true)
        } else {
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pdfmodel_Arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pdfTableViewCell") as! pdfTableViewCell
        let title = pdfmodel_Arr[indexPath.row].Filename
        
        let fullName = title
        let fullNameArr = fullName.split{$0 == "."}.map(String.init)
        cell.titlelbl.text = fullNameArr[0]
        cell.btnclikced.addTarget(self, action: #selector(self.loadPdf1), for: UIControlEvents.touchUpInside)
        cell.btnclikced.tag = indexPath.row
        let byteCount = pdfmodel_Arr[indexPath.row].FileSize
        let bcf = ByteCountFormatter()
        bcf.allowedUnits = .useMB
        bcf.countStyle = .file
        let datastr = bcf.string(fromByteCount: Int64(byteCount)!)
        cell.filesizelbl.text = datastr
        return cell
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
      self.local_activityIndicator_stop()
        print("WEB view loaded")
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.local_activityIndicator_stop()
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        print("Error occur : ",error)
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        switch navigationType {
        case .linkClicked:
            return true
        default:
            return true
        }
    }
    
    func webViewDidStartLoad(_ webView: UIWebView)
    {
       self.local_activityIndicator_start()
        print("Loading the data statrted")
    }
    
    func loadPdf1(sender: UIButton) {
        self.local_activityIndicator_start()
        back = 1
        downloaderview.isHidden = false
        Selected_pdf = pdfmodel_Arr[sender.tag]
        let website = Selected_pdf.ActualURL
        let newString = website.replacingOccurrences(of: " ", with: "%20")
        pdfwebview.loadRequest(URLRequest(url: URL(string: newString)!))
    }
    
    func loaddata()
    {
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
            //self.pdf_dic = LibraryAPI.sharedInstance.RequestDicUrl1(url: "http://edi.redingtonb2b.in/Whatsup-v5/orderEntry.asmx/DocumentCollection?folder=Whatsup")
            self.getjsonMutableDictionarysuccess(params: [:], urlhit: "http://edi.redingtonb2b.in/Whatsup-v5/orderEntry.asmx/DocumentCollection?folder=Whatsup", completion: { (jsonmutadic) in
                self.pdf_dic = jsonmutadic
            
            DispatchQueue.main.async {
                if(self.pdf_dic.count>0)
                {
                    pdfmodel_Arr = [pdfmodel]()
                    
                    if((self.pdf_dic.object(forKey: "Data")! as AnyObject).count > 0)
                    {
                        for data in self.pdf_dic.object(forKey: "Data")as! NSArray
                        {
                            pdfmodel_Arr.append(pdfmodel(data: data as AnyObject))
                        }
                    }
                    print(pdfmodel_Arr)
                    self.pdftableview.reloadData()
                     self.local_activityIndicator_stop()
                }
                else
                {
                    self.local_activityIndicator_stop()
                    let alertController = UIAlertController(title: "Alert", message:"No Data Available" , preferredStyle: .alert)
                    let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                        self.navigationController?.pushViewController(lvc, animated: true)
                    }
                    alertController.addAction(cancelAction)
                    self.present(alertController, animated: true, completion:nil)
                    
                }
            }
            })
        }
    }
}
