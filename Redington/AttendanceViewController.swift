//
//  AttendanceViewController.swift
//  Whatsup
//
//  Created by truetech on 22/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
import CoreLocation
import KCFloatingActionButton

class AttendanceViewController: UIViewController,CLLocationManagerDelegate,KCFloatingActionButtonDelegate {

   
    @IBOutlet var datepicker: UIDatePicker!
    @IBOutlet var pickerview: UIView!
    @IBOutlet var fromdatef: UITextField!
    @IBOutlet var todatef: UITextField!
    @IBOutlet var locationf: UITextField!
    let manager=CLLocationManager()
    
    @IBOutlet var switcher: UISwitch!
    var currenttf=String()
    var fab = KCFloatingActionButton()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customnavigation()
        
        self.layoutFAB()
       switcher.addTarget(self, action: #selector(AttendanceViewController.switchIsChanged), for: UIControlEvents.valueChanged)
        fromdatef.isUserInteractionEnabled=false
        todatef.isUserInteractionEnabled=false
        locationf.isUserInteractionEnabled=false
        
      //  http://www.redington-india.com/whatsup/Users.asmx/AttendanceUpdation
        // Do any additional setup after loading the view.
    }

    
    func switchIsChanged(mySwitch: UISwitch) {
        if mySwitch.isOn {
            self.setdate()
            self.settime()
            self.setlocation()
            
        } else {
          
           fromdatef.text=""
          todatef.text=""
        }
    }
    
    
    func setdate()
    {
        let date = NSDate()
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.full
        dateFormatter.dateFormat = "MM-dd-yyyy"
        let strDate = dateFormatter.string(from: date as Date)

        fromdatef.text=strDate
    }
    
    func settime()
    {
    
        let date = NSDate()
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.full
        dateFormatter.dateFormat = "HH:MM"
        let strDate = dateFormatter.string(from: date as Date)
        let str=strDate.components(separatedBy: " ")
       
        todatef.text=str as! String
    
    }
    
    func setlocation() {
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func customnavigation()
        
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(AttendanceViewController.backbuttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Attendance"
        
    }
    
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            
            self.fab.close()
        }
        //  fab.fabDelegate = self
        
        self.view.addSubview(fab)
        
    }
    
    //func KCFABOpened(fab: KCFloatingActionButton) {
     
    //}
    
    override func viewWillAppear(_ animated: Bool) {
//        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        appdelegate.shouldSupportAllOrientation = false
    }
    
    
    func backbuttonClicked(sender:UIButton)
    {
        //  self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewControllerWithIdentifier("DashBoardViewController")), animated: true)
        // self.navigationController?.popViewControllerAnimated(true)
        LibraryAPI.sharedInstance.tracker="1"
        self.navigationController?.popViewController(animated: true)
    }
    
    //textfieldDelegates
    
    func textFieldDidBeginEditing(textField: UITextField!)
    {    //delegate method
        
    }
    func textFieldShouldEndEditing(textField: UITextField!) -> Bool
    {  //delegate method
        
        return true
    }
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool
    {
        if(textField.tag==2)
        {
            pickerview.isHidden=false
            currenttf="From"
            return false
            
        }
        
        if(textField.tag==3)
        {
            pickerview.isHidden=false
            let date = NSDate()
            datepicker.maximumDate=date as Date
            currenttf="to"
            return false
            
            
        }
        
        if(textField.tag==1)
        {
            pickerview.isHidden=true
            manager.desiredAccuracy = kCLLocationAccuracyBest
            manager.delegate = self
            manager.requestWhenInUseAuthorization()
            manager.startUpdatingLocation()
            currenttf="Location"
            return false
            
        }
        
        
        return true
    }
    
    
    
    func textFieldShouldReturn(textField: UITextField!) -> Bool
    {
        
        
        
        return true
    }
    
    
    func displayLocationInfo(placemark: CLPlacemark) {
        
        //stop updating location to save battery life
        manager.stopUpdatingLocation()
        locationf.text=placemark.locality
    }
    
    
 
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler:
            {(placemarks, error) in
                if (error != nil) {
                   
                    return
                }
                
                if placemarks!.count > 0 {
                    let pm = placemarks![0] as CLPlacemark
                    self.displayLocationInfo(placemark: pm)
                    }
                else
                {
                   
                }
        })
        
        let location = locations.last! as CLLocation
    }

    

    @IBAction func Doneaction(_ sender: AnyObject)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.full
        if(currenttf=="to")
        {
            dateFormatter.dateFormat = "MM-dd-yyyy"
        }
        else
        {
            dateFormatter.dateFormat = "yyyy-MM-dd"
        }
        if(currenttf=="to")
        {
            let strDate = dateFormatter.string(from: datepicker.date)
           
            todatef.text=strDate
        }
        else
        {
            let strDate = dateFormatter.string(from: datepicker.date)
           
            fromdatef.text=strDate
        }
        
        pickerview.isHidden=true;
    }
    
    @IBAction func CancelAction(_ sender: AnyObject)
    {
          pickerview.isHidden=true;
    }
    
    
    @IBAction func Submitaction(_ sender: AnyObject) {
    }

  
}
