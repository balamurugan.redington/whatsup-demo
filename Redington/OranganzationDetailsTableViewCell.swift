//
//  OranganzationDetailsTableViewCell.swift
//  Whatsup
//
//  Created by truetech on 23/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
import  MapKit
import CoreLocation


class OranganzationDetailsTableViewCell: UITableViewCell,MKMapViewDelegate {

    @IBOutlet weak var statelabel: UILabel!
    @IBOutlet weak var mapview: MKMapView!
    @IBOutlet var contact: UILabel!
    @IBOutlet var Address: UILabel!
    @IBOutlet var ContactNum: UILabel!
    @IBOutlet var Email: UILabel!
    @IBOutlet var Company: UILabel!
    @IBOutlet var Name: UILabel!
    @IBOutlet var Details: UILabel!
    @IBOutlet var Detailimage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        mapview.delegate=self
        // Initialization code
    }
    

  
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is mapkitclass {
            let pinAnnotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "myPin")
            
         
            if #available(iOS 9.0, *) {
                pinAnnotationView.pinTintColor = UIColor.purple
            } else {
                //pinAnnotationView.pinTintColor = UIColor.redColor()
            }
            pinAnnotationView.isDraggable = true
            pinAnnotationView.canShowCallout = true
            pinAnnotationView.animatesDrop = true
            // pinAnnotationView.
            
            
            let deleteButton = UIButton.init(type:UIButtonType.custom)
            deleteButton.frame.size.width = 44
            deleteButton.frame.size.height = 44
            deleteButton.backgroundColor = UIColor.white
            //deleteButton.setImage(UIImage(named: "dir.png"), forState: .normal)
            deleteButton.setImage(UIImage(named: "dir.png"), for: .normal)
            
            pinAnnotationView.leftCalloutAccessoryView = deleteButton
            
            return pinAnnotationView
            
        }
        return nil
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if (view.annotation as? mapkitclass) != nil {
            LibraryAPI.sharedInstance.ismapopened=true
            let latitute:CLLocationDegrees =  mapView.region.center.latitude
            let longitute:CLLocationDegrees =   mapView.region.center.longitude
            
            let regionDistance:CLLocationDistance = 10000
            let coordinates = CLLocationCoordinate2DMake(latitute, longitute)
           
            let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
            let options = [
                MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
                MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
            ]
           
            let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
            let mapItem = MKMapItem(placemark: placemark)
           
            mapItem.openInMaps(launchOptions: options)
            
        }
    }
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
    }


}
