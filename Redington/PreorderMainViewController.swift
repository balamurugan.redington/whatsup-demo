//
//  PreorderMainViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 05/12/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

class PreorderMainViewController: UIViewController {
    
    @IBOutlet weak var preorderbtn: UIButton!
    @IBOutlet weak var bodbtn: UIButton!
    @IBOutlet weak var ecbbtn: UIButton!
    @IBOutlet weak var dbcbtn: UIButton!
    @IBOutlet weak var spcbtn: UIButton!
    @IBOutlet weak var preorderview: UIView!
    @IBOutlet weak var bodview: UIView!
    @IBOutlet weak var Menuview: UIView!
    @IBOutlet weak var ecbview: UIView!
    @IBOutlet weak var dbcview: UIView!
    @IBOutlet weak var spcview: UIView!
    @IBOutlet weak var usernamelbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customnavigation()
        usernamelbl.text = LibraryAPI.sharedInstance.LiveUserName
        preorderbtn.layer.cornerRadius = 10
        bodbtn.layer.cornerRadius = 10
        ecbbtn.layer.cornerRadius = 10
        dbcbtn.layer.cornerRadius = 10
        spcbtn.layer.cornerRadius = 10
        Menuview.layer.cornerRadius = 5
        preorderview.layer.cornerRadius =  preorderview.frame.size.width/2
        bodview.layer.cornerRadius =  bodview.frame.size.width/2
        ecbview.layer.cornerRadius =  ecbview.frame.size.width/2
        dbcview.layer.cornerRadius =  dbcview.frame.size.width/2
        spcview.layer.cornerRadius =  spcview.frame.size.width/2
        //Menuview.layer.cornerRadius =  Menuview.frame.size.width/2
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    
    @available(iOS 10.0, *)
    @IBAction func ProOrderBtnAction(_ sender: UIButton) {
        //let responsestring = LibraryAPI.sharedInstance.Parsejson1(url: "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=WHATSUP&Option=3")
        self.getStringsuccess(params: [:], urlhit:"\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=WHATSUP&Option=3", completion: { (jsonstr) in
            let responsestring = jsonstr
        if responsestring == "Y" {
            LibraryAPI.sharedInstance.Currentcustomerlookup="Preorder"
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: CustomerLookupViewController.instance())
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
            }
            let container = CustomerLookupViewController.instance()
            container.closeHandler = { _ in
                popup.dismiss()
            }
            popup.show(childViewController: container)
        } else {
            self.showalert(title: "Alert", message: " You are Not Authorised for this option", buttontxt: "Ok")
        }
        })
    }
    
    
    @IBAction func BODBtnAction(_ sender: AnyObject) {
        //let responsestring = LibraryAPI.sharedInstance.Parsejson1(url: "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=PABR&Option=23")
        self.getStringsuccess(params: [:], urlhit:"\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=PABR&Option=23", completion: { (jsonstr) in
            let responsestring = jsonstr
        if responsestring == "Y" {
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "Bod2ViewController")), animated: true)
            self.sideMenuViewController!.hideMenuViewController()
        } else {
            self.showalert(title: "Alert", message: " You are Not Authorised for this option", buttontxt: "Ok")
        }
        })
    }
    
    @IBAction func ECbBtnAction(_ sender: AnyObject) {
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
            //let responsestring = LibraryAPI.sharedInstance.Parsejson1(url: "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=PABR&Option=31")
            self.getStringsuccess(params: [:], urlhit:"\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=PABR&Option=31", completion: { (jsonstr) in
                let responsestring = jsonstr
            DispatchQueue.main.async {
                if responsestring == "Y" {
                    self.local_activityIndicator_stop()
                    self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "ECBViewController")),animated: true)
                    self.sideMenuViewController!.hideMenuViewController()
                } else {
                    self.local_activityIndicator_stop()
                    self.showalert(title: "Alert", message: " You are Not Authorised for this option", buttontxt: "Ok")
                }
            }
            })
        }
    }
    
    
    @IBAction func DbcBtnAction(_ sender: AnyObject) {
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
            //let responsestring = LibraryAPI.sharedInstance.Parsejson1(url: "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=PABR&Option=30")
            self.getStringsuccess(params: [:], urlhit:"\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=PABR&Option=30", completion: { (jsonstr) in
                let responsestring = jsonstr
            DispatchQueue.main.async {
                if responsestring == "Y" {
                   self.local_activityIndicator_stop()
                    self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DBCViewController")),animated: true)
                    self.sideMenuViewController!.hideMenuViewController()
                } else {
                    self.local_activityIndicator_stop()
                    self.showalert(title: "Alert", message: " You are Not Authorised for this option", buttontxt: "Ok")
                }
            }
            })
        }
    }
    
    
    @IBAction func SPCBtnAction(_ sender: AnyObject) {
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
            //let responsestring = LibraryAPI.sharedInstance.Parsejson1(url: "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=PABR&Option=1")
            self.getStringsuccess(params: [:], urlhit:"\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=PABR&Option=1", completion: { (jsonstr) in
                let responsestring = jsonstr
            DispatchQueue.main.async {
                if(responsestring == "Y") {
                    self.local_activityIndicator_stop()
                    self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "SpcoptionentryViewController")),animated: true)
                    self.sideMenuViewController!.hideMenuViewController()
                } else {
                    self.local_activityIndicator_stop()
                    self.showalert(title: "Alert", message: " You are Not Authorised for this option", buttontxt: "Ok")
                }
            }
            })
        }
    }
    
    func customnavigation() {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: 65)
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(PreorderMainViewController.buttonClicked(_:)),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "PreOrder Menu"
    }
    
    func buttonClicked(_ sender:UIButton) {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
    }
}
