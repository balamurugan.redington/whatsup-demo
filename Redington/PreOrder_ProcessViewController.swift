//
//  PreOrder_ProcessViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 05/12/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class PreOrder_ProcessViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customnavigation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    @available(iOS 10.0, *)
    @IBAction func preorderbtn(_ sender: AnyObject) {
       // let responsestring = LibraryAPI.sharedInstance.Parsejson1(url: "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=WHATSUP&Option=3")
        self.getStringsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=WHATSUP&Option=3", completion: { (jsonstr) in
             let responsestring = jsonstr
            if responsestring == "Y" {
                LibraryAPI.sharedInstance.Currentcustomerlookup="Preorder"
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: CustomerLookupViewController.instance())
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = CustomerLookupViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                }
                popup.show(childViewController: container)
            } else {
                self.showalert(title: "Alert", message: " You are Not Authorised for this option", buttontxt: "Ok")
            }
        })
        
    }
    
    @IBAction func bodbtn(_ sender: AnyObject){
       // let responsestring = LibraryAPI.sharedInstance.Parsejson1(url: "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=PABR&Option=23")
         self.getStringsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=PABR&Option=23", completion: { (jsonstr) in
            let responsestring = jsonstr
        if responsestring == "Y" {
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "Bod2ViewController")), animated: true)
            self.sideMenuViewController!.hideMenuViewController()
        } else {
            self.showalert(title: "Alert", message: " You are Not Authorised for this option", buttontxt: "Ok")
            }
            
         })
    }
    
    
    @IBAction func dbcbtn(_ sender: AnyObject) {
        //let responsestring = LibraryAPI.sharedInstance.Parsejson1(url: "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=PABR&Option=30")
        self.getStringsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=PABR&Option=30", completion: { (jsonstr) in
            let responsestring = jsonstr
        if responsestring == "Y" {
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DBCViewController")),animated: true)
            self.sideMenuViewController!.hideMenuViewController()
        } else {
            self.showalert(title: "Alert", message: " You are Not Authorised for this option", buttontxt: "Ok")
        }
        })
    }
    
    @IBAction func Spcbtn(_ sender: AnyObject) {
        //let responsestring = LibraryAPI.sharedInstance.Parsejson1(url: "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=PABR&Option=1")
        self.getStringsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=PABR&Option=1", completion: { (jsonstr) in
            let responsestring = jsonstr
        if responsestring == "Y" {
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "SpcoptionentryViewController")),animated: true)
            self.sideMenuViewController!.hideMenuViewController()
        } else {
            self.showalert(title: "Alert", message: " You are Not Authorised for this option", buttontxt: "Ok")
        }
        })
    }
    
    @IBAction func ECBBtn(_ sender: AnyObject) {
       // let responsestring = LibraryAPI.sharedInstance.Parsejson1(url: "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=PABR&Option=31")
        self.getStringsuccess(params: [:], urlhit:"\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=PABR&Option=31", completion: { (jsonstr) in
            let responsestring = jsonstr
        if responsestring == "Y" {
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "ECBViewController")),animated: true)
            self.sideMenuViewController!.hideMenuViewController()
        } else {
            self.showalert(title: "Alert", message: " You are Not Authorised for this option", buttontxt: "Ok")
        }
        })
    }
    
    func customnavigation() {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: 65)
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(PreOrder_ProcessViewController.buttonClicked(_:)),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "PreOrder Menu"
    }
    
    func buttonClicked(_ sender:UIButton) {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
    }
}
