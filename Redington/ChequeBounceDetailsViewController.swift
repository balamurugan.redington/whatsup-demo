
//
//  ChequeBounceDetailsViewController.swift
//  Redington
//
//  Created by TRUETECHMAC3 on 26/12/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
import KCFloatingActionButton

class ChequeBounceDetailsViewController: UIViewController,KCFloatingActionButtonDelegate, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var bizcodelbl: UILabel!
    @IBOutlet weak var customercodelbl: UILabel!
    @IBOutlet weak var selectview: UIView!
    
    @IBOutlet weak var customercodebtn: UIButton!
    @IBOutlet weak var businesscodetn: UIButton!
    var Check : Int!
    
    @IBOutlet var ChequeTableview: UITableView!
    var fromdate=String()
    var todate=String()
    var  responsestring = NSMutableArray()
    var  response = NSMutableArray()
    var  response1 = NSMutableArray()
    var  CustomerResponse = NSMutableArray()
    let BizArray = NSMutableArray()
    
    var fab=KCFloatingActionButton()
    var businessdescription = String()
    var customercode : String!
     var deliveryPageindex:Int = 1
    // var spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Cheque pending details ChequeBounceDetailsViewController")
        ChequeTableview.register(UINib(nibName: "ChequePendingTableViewCell", bundle: nil), forCellReuseIdentifier: "ChequePendingTableViewCell")
        self.selectview.layer.masksToBounds = true
        LibraryAPI.sharedInstance.keyinfochepending = 0
        ChequeTableview.bounces = true
        ChequeTableview.layer.borderWidth = 2
        ChequeTableview.layer.borderColor = CustomColor.ShareColor.hexStringToUIColor(hex: CustomColor.ShareColor.customNavyBlue, alpha: 1.0).cgColor
        bizcodelbl.layer.borderWidth = 2
        bizcodelbl.layer.borderColor = CustomColor.ShareColor.hexStringToUIColor(hex: CustomColor.ShareColor.customNavyBlue, alpha: 1.0).cgColor
        customercodelbl.layer.borderWidth = 2
        customercodelbl.layer.borderColor = CustomColor.ShareColor.hexStringToUIColor(hex: CustomColor.ShareColor.customNavyBlue, alpha: 1.0).cgColor
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.bizcodelbl.text = "Select Business Code"
        self.customercodelbl.text = "Select Customer Code"
        deliveryPageindex = 1
        ChequePending_Arr = [Chequepending]()
        self.customnavigation()
        self.fetchdata()
        self.layoutFAB()
        
        if Check == nil
        {
            self.bizcodelbl.text = "Select "
            self.customercodelbl.text = "Select "
        }
        else
        {
            if Check == 0
            {
                self.bizcodelbl.text = LibraryAPI.sharedInstance.SelectedValue
            }
            else if Check == 1
            {
                self.customercodelbl.text = LibraryAPI.sharedInstance.CheckBending_CustomerCode
            }
            else
            {
                self.bizcodelbl.text = "Select "
                self.customercodelbl.text = "Select "
            }
        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        
    }
    
    func fetchdata()
    {
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
             self.Loaddata(deliveryPageindex: self.deliveryPageindex)
            DispatchQueue.main.async {
            }
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    func Loaddata(deliveryPageindex: Int)
    {
        LibraryAPI.sharedInstance.CheckBendingDetailArray.removeAllObjects() //CheckBendingCustomerArray
        LibraryAPI.sharedInstance.CheckBendingCustomerArray.removeAllObjects()
        let defaults = UserDefaults.standard
        let name = defaults.string(forKey: "ViewCheckBool")
        if name == "1"
        {
           
            businesscodetn.isUserInteractionEnabled = false
            customercodebtn.isUserInteractionEnabled = false
            
            //http://edi.redingtonb2b.in/whatsup-staging/Customer.asmx/Chequependingdetails?User=SENTHILKS&CustomerCode=&Branch=S1&RecordNo=20&PageNo=1
            //responsestring = LibraryAPI.sharedInstance.RequestUrl(url: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/Chequependingdetails?User=\(LibraryAPI.sharedInstance.Userid)&CustomerCode=&Branch=\(LibraryAPI.sharedInstance.view360secondpopupname)&RecordNo=20&PageNo=\(deliveryPageindex)")
            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/Chequependingdetails?User=\(LibraryAPI.sharedInstance.Userid)&CustomerCode=&Branch=\(LibraryAPI.sharedInstance.view360secondpopupname)&RecordNo=20&PageNo=\(deliveryPageindex)", completion: { (jsonmutable) in
            self.responsestring = jsonmutable
            if(self.responsestring.count > 0)
            {
                for word in self.responsestring
                {
                    ChequePending_Arr.append(Chequepending(data: word as AnyObject))
                }
                self.local_activityIndicator_stop()
                self.ChequeTableview.reloadData()
                
            }
            else
            {
                let alertController = UIAlertController(title: "Alert", message: "No Data Available" , preferredStyle: .alert)
                
                let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                    self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
                }
                alertController.addAction(cancelAction)
                
                
                self.present(alertController, animated: true, completion:nil)
                self.local_activityIndicator_stop()
            }
            self.afterresponse()
            })
            print("value 1 called in cheque bounce")
            
        }
        else if name == "2"
        {
            businesscodetn.isUserInteractionEnabled = false
            customercodebtn.isUserInteractionEnabled = false

           // http://edi.redingtonb2b.in/whatsup-staging/Customer.asmx/Chequependingdetails?User=SENTHILKS&CustomerCode=S10003&Branch=S1&RecordNo=20&PageNo=1
           
           // responsestring = LibraryAPI.sharedInstance.RequestUrl(url: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/Chequependingdetails?User=\(LibraryAPI.sharedInstance.Userid.stringByRemovingWhitespaces)&CustomerCode=\(LibraryAPI.sharedInstance.CustomerCode)&Branch=S1&RecordNo=20&PageNo=\(deliveryPageindex)")
             self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/Chequependingdetails?User=\(LibraryAPI.sharedInstance.Userid.stringByRemovingWhitespaces)&CustomerCode=\(LibraryAPI.sharedInstance.CustomerCode)&Branch=S1&RecordNo=20&PageNo=\(deliveryPageindex)", completion: { (jsonmutable) in
               self.responsestring = jsonmutable
           
                if(self.responsestring.count > 0)
            {
                for word in self.responsestring
                {
                    ChequePending_Arr.append(Chequepending(data: word as AnyObject))
                }
                
                self.ChequeTableview.reloadData()
                 self.local_activityIndicator_stop()
            }
                self.afterresponse()
            })
            print("value 2 called in cheque bounce")
        }
        else
        {
            
        }
        
    }
    func afterresponse(){
        if(responsestring.count > 0)
        {
            var value = ""
            var value1 = ""
            for Sortlist in responsestring
            {
                let dicSort = Sortlist as! NSDictionary
                let getValue = dicSort.object(forKey: "BUSINESS CODE") as! String
                let getBizCode = dicSort.object(forKey: "BUSINESS DESC") as! String
                let CusCode = dicSort.object(forKey: "CUSTOMER CODE") as! String
                let fullNameArr : [String] = self.bizcodelbl.text!.components(separatedBy: "-")
                let firstName : String = fullNameArr[0]
                if getValue == firstName
                {
                    response1.add(Sortlist)
                }
                if CusCode == self.customercodelbl.text!
                {
                    CustomerResponse.add(Sortlist)
                }
                if value != getBizCode
                {
                    value = getBizCode
                    LibraryAPI.sharedInstance.CheckBendingDetailArray.add(getBizCode)
                }
                
                if value1 != CusCode
                {
                    value1 = CusCode
                    LibraryAPI.sharedInstance.CheckBendingCustomerArray.add(CusCode)
                }
                
            }
            DispatchQueue.main.async {
                self.ChequeTableview.isHidden=false
                self.ChequeTableview.reloadData()
                
            }
        }
        else
        {
            let alertController = UIAlertController(title: "Alert", message: "No Data Available" , preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: KeyInfoViewController.classForCoder()) {
                        self.navigationController?.popToViewController(controller as UIViewController, animated: true)
                        LibraryAPI.sharedInstance.keyinfochepending = 0
                        break
                    }
                }
            }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)
        }
    }
    
    func customnavigation()
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(ChequeBounceDetailsViewController.backbuttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Cheque Pending Details"
        
    }
    
    
    
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            
            
            
            self.fab.close()
        }
        
        fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
            
            //  self.fetchdata()
            self.responsestring.removeAllObjects()
            ChequePending_Arr.removeAll()
            self.deliveryPageindex = 1
            self.Loaddata(deliveryPageindex: self.deliveryPageindex)
            self.ChequeTableview.reloadData()
            self.fab.close()
        }
        
        
        fab.fabDelegate = self
        
        self.view.addSubview(fab)
        
    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
        
    }
    
    
    
    func backbuttonClicked(sender:UIButton)
    {
        if(LibraryAPI.sharedInstance.keyinfochepending == 1)
            
        {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: NewkeyifoViewController.classForCoder()) {
                    self.navigationController?.popToViewController(controller as UIViewController, animated: true)
                    LibraryAPI.sharedInstance.keyinfochepending = 0
                    break
                }
            }
        }
        else if(LibraryAPI.sharedInstance.keyinfochepending == 0)
        {
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
        }
        
    }
    
    //TableView Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
   
        if Check == nil
        {
            if ChequePending_Arr.count == 0
            {
                return 0
            }
            else
            {
                return 1
            }
            
        }
        else
        {
            if Check == 0
            {
                if response1.count == 0
                {
                    return 0
                }
                else
                {
                    return 1
                }
            }
            else if Check == 1
            {
                if CustomerResponse.count == 0
                {
                    return 0
                }
                else
                {
                    return 1
                }
            }
            else
            {
                if ChequePending_Arr.count == 0
                {
                    return 0
                }
                else
                {
                    return 1
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        if Check == nil
        {
            return ChequePending_Arr.count
        }
        else
        {
            if Check == 0
            {
                return response1.count
            }
            else if Check == 1
            {
                return CustomerResponse.count
            }
            else
            {
                return ChequePending_Arr.count
            }
        }
        
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell : ChequePendingTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "ChequePendingTableViewCell") as! ChequePendingTableViewCell
        
        if(indexPath.row % 2 == 0)
        {
            cell.contentView.backgroundColor=(UIColor.init(red: 240/255.0, green: 246/255.0, blue: 225/255.0, alpha: 1.0))
        }
        else
        {
            //cell.contentView.backgroundColor=UIColor.whiteColor()
            cell.contentView.backgroundColor=(UIColor.init(red: 215/255.0, green: 250/255.0, blue: 252/255.0, alpha: 1.0))
        }
        
        var response : NSMutableArray!
        
        if Check == nil
        {
            response = responsestring
        }
        else
        {
            if Check == 0
            {
                response = response1
            }
            else if Check == 1
            {
                response = CustomerResponse
            }
            
        }
        
        if response.count > 0
            
            
        {
           self.bizcodelbl.text = ChequePending_Arr[indexPath.row].BUSINESS_CODE
          self.customercodelbl.text = ChequePending_Arr[indexPath.row].CUSTOMER_CODE
            
//            self.bizcodelbl.text = response.objectAtIndex(indexPath.row).objectForKey("BUSINESS DESC") as? String
//            self.customercodelbl.text = response.objectAtIndex(indexPath.row).objectForKey("CUSTOMER CODE") as? String
            
            cell.InvNo.text = ChequePending_Arr[indexPath.row].INVOICE_NUMBER
            cell.InvVal.text = String(describing: ChequePending_Arr[indexPath.row].INVOICE_VALUE);
            cell.InvDt.text = ChequePending_Arr[indexPath.row].INVOICE_DATE
            cell.OdDays.text = ChequePending_Arr[indexPath.row].OVER_DUE_DAYS
            
            
            cell.pendingdays.text = ChequePending_Arr[indexPath.row].PENDING_DAYS
            cell.duedate.text = ChequePending_Arr[indexPath.row].DUE_DATE
        }
        else
        {
            
        }
        
        
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
           let lastitem  = ChequePending_Arr.count - 1
        
        
        if indexPath.row == lastitem
        {
            deliveryPageindex = deliveryPageindex+1
            
            let lastpage = Int(ChequePending_Arr[indexPath.row].TOTAL_PAGE)
            
            if(deliveryPageindex <= lastpage)
            {
                self.local_activityIndicator_start()
                DispatchQueue.global(qos: .background).async {
                    
                    // Validate user input
                    
                     self.Loaddata(deliveryPageindex: self.deliveryPageindex)
                    // Go back to the main thread to update the UI
                    DispatchQueue.main.async {
                        
                        self.local_activityIndicator_stop()
                        self.ChequeTableview.reloadData()
                        
                    }
                }
              
                
            }
            else
            {
                
            }
            
        }
        
    }

    
    
    @IBAction func businessbtn(_ sender: AnyObject)
    {
        
        
        let popup = PopupController
            .create(parentViewController: self.parent!)
            .show(childViewController: CheckbendingViewController.instance())
            
            .didShowHandler { popup in
            }
            .didCloseHandler { _ in
                
        }
        let container = CheckbendingViewController.instance()
        container.closeHandler = { _ in
            popup.dismiss()
        }
        
        
        popup.show(childViewController: container)
        
        
    }
    
    
    @IBAction func customerbtn(_ sender: AnyObject)
    {
        let popup = PopupController
            .create(parentViewController: self.parent!)
            .show(childViewController: CustomercodeViewController.instance())
            .didShowHandler { popup in
            }
            .didCloseHandler { _ in
        }
        let container = CustomercodeViewController.instance()
        container.closeHandler = { _ in
            popup.dismiss()
        }
        
        
        popup.show(childViewController: container)
        
    }
    
}
