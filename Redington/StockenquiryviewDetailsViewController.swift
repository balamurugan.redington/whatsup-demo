//
//  StockenquiryviewDetailsViewController.swift
//  Whatsup
//
//  Created by truetech on 21/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import KCFloatingActionButton

class StockenquiryviewDetailsViewController: UIViewController,KCFloatingActionButtonDelegate,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet var Stockenquirytableview: UITableView!
    
    var  responsestring = NSMutableArray()
    var businesscode=String()
    var stockroom=String()
    var user=String()
    var fab=KCFloatingActionButton()
    
    @IBOutlet weak var NodataVIew: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customnavigation()
        self.layoutFAB()
        self.getdata()
        
        NodataVIew.isHidden = true
        // Do any additional setup after loading the view.
    }
    @IBOutlet var Unitcode: UILabel!
    
    
    func getdata()
    {
        self.local_activityIndicator_start()
        // new version dispatch Que
        
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
            
             self.Loaddata()
            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
              
                self.Stockenquirytableview.reloadData()
            }
        }
        
        // new version dispatch Que
      
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    func Loaddata()
    {
        
        // http://edi.redingtonb2b.in/whatsup/StockDetails.asmx/StockAvaliablityEnquiry?BUSINESSCODE=102&StockRoom=CS&User=SENTHILKS
        
       // responsestring = LibraryAPI.sharedInstance.RequestUrl(url: "\(LibraryAPI.sharedInstance.GlobalUrl)/StockDetails.asmx/StockAvaliablityEnquiry?BUSINESSCODE=\(businesscode)&StockRoom=\(stockroom)&User=\(user)")
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)/StockDetails.asmx/StockAvaliablityEnquiry?BUSINESSCODE=\(businesscode)&StockRoom=\(stockroom)&User=\(LibraryAPI.sharedInstance.Userid)", completion:  { (jsonmutable) in
            self.responsestring = jsonmutable
            print("value in stock details: ",self.responsestring )
            if(self.responsestring.count>0)
        {
            DispatchQueue.main.async {
                
                self.Stockenquirytableview.isHidden=false
                self.NodataVIew.isHidden=true
                 self.Stockenquirytableview.reloadData()
                 self.local_activityIndicator_stop()
            }
            
        }
        else
        {
            DispatchQueue.main.async {
                
                self.Stockenquirytableview.isHidden=true
                self.NodataVIew.isHidden=true
                let alertController = UIAlertController(title: "Alert", message:"No Data Available" , preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                    let lvc=self.storyboard?.instantiateViewController(withIdentifier: "StockEnquiryViewController") as! StockEnquiryViewController
                    self.navigationController?.pushViewController(lvc, animated: true)
                    
                }
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion:nil)
                 self.local_activityIndicator_stop()
            }
           
            
        }
        
       })
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        appdelegate.shouldSupportAllOrientation = false
    }
    
    
    func customnavigation()
        
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(StockenquiryviewDetailsViewController.backbuttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "STOCK ENQUIRY"
        
    }
    
    
    
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            
            self.fab.close()
        }
        
        fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
            
            
            self.getdata()
            
            self.fab.close()
        }
        fab.fabDelegate = self
        
        self.view.addSubview(fab)
        
    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
        
    }
    
    
    //TableView Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        if (responsestring.count > 0)
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return responsestring.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        var cell : StockEnquiryDeatilsTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as! StockEnquiryDeatilsTableViewCell
        if(cell == nil)
        {
            cell = Bundle.main.loadNibNamed("Cell", owner: self, options: nil)![0] as! StockEnquiryDeatilsTableViewCell;
            
        }
        let dic = responsestring[indexPath.row] as! NSDictionary
        
        cell.Stockname.text=dic.object(forKey: "ITEM DESCRIPTION") as? String
        cell.Itemcode.text=String(dic.object(forKey: "ITEM CODE") as! String)
        cell.Vendorpartno.text=String(dic.object(forKey: "VENDOR PART NO") as! String)
        cell.Unitavailable.text=String(Int(dic.object(forKey: "UNIT AVALIABLE") as! Double))
        
        cell.selectionStyle = .none
        return cell as StockEnquiryDeatilsTableViewCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        
    }
    
    func backbuttonClicked(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
   
    
}
