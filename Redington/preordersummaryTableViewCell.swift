//
//  preordersummaryTableViewCell.swift
//  Redington
//
//  Created by truetech on 26/08/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit

class preordersummaryTableViewCell: UITableViewCell {

    @IBOutlet var vendor: UILabel!
    @IBOutlet var code: UILabel!
    @IBOutlet var price: UILabel!
    @IBOutlet var quantity: UILabel!
    @IBOutlet var productname: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
