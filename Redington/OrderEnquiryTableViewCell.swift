//
//  OrderEnquiryTableViewCell.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 05/02/18.
//  Copyright © 2018 truetech. All rights reserved.
//

import UIKit

class OrderEnquiryTableViewCell: UITableViewCell
{
    @IBOutlet weak var sonumber: UILabel!
    @IBOutlet weak var sodate: UILabel!
    @IBOutlet weak var orderstatus: UILabel!
    
    @IBOutlet weak var invoicenumber: UILabel!
    @IBOutlet weak var invoicedate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
