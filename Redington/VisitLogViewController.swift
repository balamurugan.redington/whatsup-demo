//
//  VisitLogViewController.swift
//  Redington
//
//  Created by truetech on 13/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
import CoreLocation
//import NVActivityIndicatorView
import Firebase
import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import Contacts
import ContactsUI
import KCFloatingActionButton

class VisitLogViewController: UIViewController,UITextFieldDelegate,UITextViewDelegate,CLLocationManagerDelegate,UITableViewDelegate,UITableViewDataSource,CNContactPickerDelegate{
    
    @IBOutlet weak var bizcodetf: UITextField!
    @IBOutlet var Locationtf: UITextField!
    @IBOutlet var Datepicker: UIDatePicker!
    @IBOutlet var Datepicker1: UIDatePicker!
    @IBOutlet var Containerview: UIView!
    @IBOutlet var Scrollview: UIScrollView!
    @IBOutlet var Vendorbtn: UIButton!
    @IBOutlet var Prospectbtn: UIButton!
    @IBOutlet var Customerbtn: UIButton!
    @IBOutlet weak var NameView: UIView!
    @IBOutlet weak var visittypeview: UIView!
    @IBOutlet var SubmitBtn: UIButton!
    @IBOutlet var DatePickerview: UIView!
    @IBOutlet weak var Metwhomview: UIView!
    @IBOutlet weak var phcontactbtn: UIButton!
    @IBOutlet weak var contactbtn: UIButton!
    @IBOutlet weak var bottonview: UIView!
    @IBOutlet weak var Topview: UIView!
    
    @IBOutlet weak var businesstableview: UITableView!
    @IBOutlet var Timeupdatetf: UITextField!
    @IBOutlet var Dateupdatetf: UITextField!
    @IBOutlet var Textview2: UITextView!
    @IBOutlet var Emailtf: UITextField!
    @IBOutlet var Userupdatetf: UITextField!
    @IBOutlet var Areatf: UITextField!
    @IBOutlet var Textview1: UITextView!
    @IBOutlet var metwhomtextfield: UITextField!
    @IBOutlet var Naturetf: UITextField!
    @IBOutlet var Totime: UITextField!
    @IBOutlet var fromtimetf: UITextField!
    @IBOutlet var Codetf: UITextField!
    @IBOutlet var Datetf: UITextField!
    @IBOutlet var Nametf: UITextField!
    @IBOutlet var Phonenumbertf: UITextField!
    @IBOutlet var Designationtf: UITextField!
    @IBOutlet weak var tableviewview: UIView!
    @IBOutlet weak var Date2tf: UITextField!
    @IBOutlet weak var Datelbl: UILabel!
    @IBOutlet weak var Customercode2lbl: UILabel!
    @IBOutlet weak var customerimg: UIImageView!
    @IBOutlet weak var prospectimg: UIImageView!
    @IBOutlet weak var vendorimg: UIImageView!
    
    
    
    var currenttf=String()
    var uploadfile=NSMutableDictionary()
    var type=String()
    var jsonresult=NSDictionary()
    var fab = KCFloatingActionButton()
    var Movedup=Bool()
    var selectedrow=Int()
    var pickerarray=NSMutableArray()
    var responsearray=NSMutableArray()
    var BizcodeArray = NSMutableArray()
    var businesscode=String()
    var locationManager: CLLocationManager!
    var responsestring : NSMutableDictionary!
    var responsestring1 : NSMutableArray!
    var latitude : Double = 0.0
    var longitude : Double = 0.0
    var CustomcodeCheck : String!
    var Customername : String!
    var current_pin: String = ""
    var current_area: String = ""
    var tableviewresponsearray : NSMutableArray = []
    var Phonenumber=Bool()
    var Arr : [String] = []
    var updateContact = CNContact()
    var contactStore = CNContactStore()
    var updateContact1 = CNContact()
    var ContactNumbersArr : [String] = []
    var isUpdate: Bool = false
    var Contact_Bool: Bool = false
    var Prospect_Value: NSNumber = 1
    var Customer_Value : NSNumber = 1
    var Vendor_Value : NSNumber = 1
    var Date3_value : Bool = false
    var locationAccess              = 1
    var chkMetNameVal = 0
    var chkphnumVal = 0
    var contactAccess = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.Customnavigation()
        locationProcess()
        tableviewview.isHidden = true
        Contact_Bool = false
        Textview1.delegate = self
        Phonenumber  = false
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        phcontactbtn.layer.cornerRadius = 10
        phcontactbtn.clipsToBounds = true
        contactbtn.layer.cornerRadius = 10
        contactbtn.clipsToBounds = true
        //self.layoutFAB()
        self.addDoneButtonOnKeyboard()
        Timeupdatetf.delegate=self
        Dateupdatetf.delegate=self
        Timeupdatetf.isHidden = true
        Dateupdatetf.isHidden = true
        Emailtf.delegate=self
        Userupdatetf.delegate=self
        Areatf.delegate=self
        SubmitBtn.layer.cornerRadius=15.0
        metwhomtextfield.delegate=self
        Naturetf.delegate=self
        Totime.delegate=self
        fromtimetf.delegate=self
        Codetf.delegate=self
        Datetf.delegate=self
        // Date2tf.delegate = self
        Nametf.delegate=self
        Phonenumbertf.delegate=self
        Designationtf.delegate=self
        Textview2.delegate=self
        Textview1.delegate=self
        Locationtf.delegate=self
        bizcodetf.delegate = self
        Naturetf.tag=9
        Nametf.tag = 32
        bizcodetf.tag = 25
        Dateupdatetf.tag=1
        fromtimetf.tag=2
        Phonenumbertf.tag = 11
        Emailtf.tag = 20
        Textview1.tag = 26
        metwhomtextfield.tag = 27
        // Date2tf.tag = 30
        Locationtf.tag = 31
        Designationtf.tag = 33
        Areatf.tag = 34
        Textview1.tag = 35
        Movedup=false
        
        NotificationCenter.default.addObserver(self, selector: #selector(VisitLogViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(VisitLogViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        Phonenumbertf.addTarget(self, action: #selector(VisitLogViewController.textFieldShouldBeginEditing(_:)), for: UIControlEvents.editingChanged)
        let arrow = UIImageView()
        let image = UIImage(named: "cal.png")
        arrow.image = image!.imageWithColor(tintColor: UIColor.init(red:239/255.0 , green: 88/255.0, blue: 88/255.0, alpha: 1.0)).withRenderingMode(.alwaysOriginal)
        arrow.frame = CGRect(x: 0.0,y: 0.0,width: arrow.image!.size.width+10.0,height: 15);
        arrow.contentMode = UIViewContentMode.center
        self.Datetf.rightView = arrow;
        self.Datetf.rightViewMode = UITextFieldViewMode.always
        businesstableview.layer.borderColor = UIColor(red:30/255.0, green:184/255.0, blue:120/255.0, alpha: 1.0).cgColor
        businesstableview.layer.borderWidth = 1.0
        businesstableview.layer.cornerRadius = 5.0
        visittypeview.layer.cornerRadius = 5.0
        NameView.layer.borderColor = UIColor(red:30/255.0, green:184/255.0, blue:120/255.0, alpha: 1.0).cgColor
        NameView.layer.borderWidth = 1.0
        NameView.layer.cornerRadius = 5.0
        Metwhomview.layer.borderColor = UIColor(red:30/255.0, green:184/255.0, blue:120/255.0, alpha: 1.0).cgColor
        Metwhomview.layer.borderWidth = 1.0
        Metwhomview.layer.cornerRadius = 5.0
        let arrow2 = UIImageView()
        let image2 = UIImage(named: "cal.png")
        arrow2.image = image2!.imageWithColor(tintColor: UIColor.init(red:88/255.0 , green: 88/255.0, blue: 88/255.0, alpha: 1.0)).withRenderingMode(.alwaysOriginal)
        arrow2.frame = CGRect(x: 0.0,y: 0.0,width: arrow.image!.size.width+10.0,height: 15);
        arrow2.contentMode = UIViewContentMode.center
        self.Dateupdatetf.rightView = arrow2;
        self.Dateupdatetf.rightViewMode = UITextFieldViewMode.always
        type="empty"
        let arrow1 = UIImageView(image: UIImage(named: "loc.png"))
        arrow1.frame = CGRect(x: 0.0,y: 0.0,width: arrow1.image!.size.width+10.0,height: 15);
        arrow1.contentMode = UIViewContentMode.center
        self.Areatf.rightView = arrow1;
        self.Areatf.rightViewMode = UITextFieldViewMode.always
        Phonenumbertf.keyboardType = .numberPad
        Textview1.layer.cornerRadius = 5.0
        Textview1.layer.borderWidth = 1.0
        self.Textview1.layer.borderColor = UIColor(red:211/255.0, green:211/255.0, blue:211/255.0, alpha: 1.0).cgColor
        bizcodeRec()
        businesstableview.dataSource = self
        businesstableview.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationmethod), name: NSNotification.Name(rawValue: "GetNotification"), object: nil)
        businesstableview.register(UINib(nibName: "BillingwithodCustomerCell", bundle: nil), forCellReuseIdentifier: "BillingwithodCustomerCell")
        businesstableview.tableFooterView = UIView(frame: .zero)
        self.contactStore.requestAccess(for: CNEntityType.contacts, completionHandler: { (access, accessError) -> Void in
        })
        type="Customer"
        self.customerimg.image = UIImage(named:"green2.png")
        self.prospectimg.image = UIImage(named: "normal.png")
        self.vendorimg.image = UIImage(named: "normal.png")
    }
    
    func locationProcess(){
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                var LocationAlert = UIAlertController(title: "Location Setting", message: "Please Allow the location access", preferredStyle: UIAlertControllerStyle.alert)
                LocationAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                    self.MoveToAppSetting()
                    self.locationAccess = 1
                }))
                LocationAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                    self.locationAccess = 0
                }))
                present(LocationAlert, animated: true, completion: nil)
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
            }
        } else {
            print("Location services are not enabled")
        }
        
    }
    
    func MoveToAppSetting(){
        if let url = NSURL(string: UIApplicationOpenSettingsURLString)
        {
            if UIApplication.shared.canOpenURL(url as URL){
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url as URL)
                }
            }
        }
    }
    
    func displayContactData() {
        metwhomtextfield.text = updateContact.givenName
        for phoneNo in updateContact.phoneNumbers {
            if phoneNo.label == CNLabelPhoneNumberMobile {
                Phonenumbertf.text = String(describing: phoneNo.value)
                break
            }
        }
    }
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        self.chkMetNameVal = 0
        askForContactAccess()
        if contactAccess == 0{
            askForContactAccess()
        }else{
            self.ContactNumbersArr = []
            for number in contact.phoneNumbers
            {
                let GivenStrvalue = (number.value as! CNPhoneNumber).value(forKey: "digits") as! String
                if GivenStrvalue != "" {
                    self.ContactNumbersArr.append(GivenStrvalue)
                }
            }
            self.metwhomtextfield.text = String(contact.givenName) + " " + String(contact.familyName)
            let metwhomval = self.metwhomtextfield.text
            if (metwhomval?.count)! > 25  {
                self.metwhomtextfield.text = ""
                self.ContactNumbersArr.removeAll()
                self.chkMetNameVal = 1
            }else{
                self.Contact_Bool = true
                self.chkMetNameVal = 0
            }
            scrollSize()
        }
    }
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
       
    }
    
    func askForContactAccess() {
        let authorizationStatus = CNContactStore.authorizationStatus(for: CNEntityType.contacts)
        switch authorizationStatus {
        case .notDetermined, .restricted, .denied:
            var alertController = UIAlertController(title: "Contact Setting", message: "Please allow the app to access your contacts through the Settings", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                self.MoveToAppSetting()
                self.contactAccess = 1
            }))
            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                self.contactAccess = 0
            }))
            self.present(alertController, animated: true, completion: nil)
        case .authorized:
            print("Access")
        }
    }
    
    func bizcodeRec()
    {
        let size = CGSize(width: 30, height:30)
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
            self.Loaddata(url: "\(LibraryAPI.sharedInstance.GlobalUrl)StockDetails.asmx/BusinessAccessDetails?UserID=\(LibraryAPI.sharedInstance.Userid)")
            DispatchQueue.main.async {
                
            }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == Phonenumbertf {
            if(Phonenumbertf.text?.first == "0") {
                if(Phonenumbertf.text == "00") {
                    let alertController = UIAlertController(title: "Alert", message:"Please Enter the valid Phone number" , preferredStyle: .alert)
                    let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                    }
                    alertController.addAction(cancelAction)
                    self.present(alertController, animated: true, completion:nil)
                }  else  {
                    Phonenumber  = true
                }
            } else {
                Phonenumber = false
            }
        }
        if textField == metwhomtextfield {
            ContactNumbersArr.removeAll()
        }
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    
    func keyboardWillShow(notification: NSNotification)
    {
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true) {
        } else
        {
            if(Movedup==false) {
                Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:1020)
                Movedup=true
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true) {
        }
        else {
           scrollSize()
            Movedup=false
        }
    }
    func scrollSize(){
        print("valeu in array",self.Arr.count)
    if (self.Arr.count == 0)  {
    Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:780)
    } else {
    if (self.Arr.count == 1) {
    Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:860)
        print("data scroll incresed")
    } else if (self.Arr.count == 2) {
    Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:900)
    } else if (self.Arr.count == 3) {
    Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:940)
    } else if (self.Arr.count == 4) {
    Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:980)
    } else if (self.Arr.count == 5) {
    Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:1020)
    } else {
    Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:780)
    }
    }
    }
    
    func setdateandtime()
    {
        let date = NSDate()
        let calendar = NSCalendar.current
        let components = calendar.dateComponents([.hour, .minute, .second], from: date as Date)
        let hour = components.hour
        let minutes = components.minute
        print("Hour and time :",hour as Any,minutes)
        let formattedhour = (hour! as NSNumber).stringValue
        let formattedtime = (minutes! as NSNumber).stringValue
        let originaltime  = formattedhour.appending(":\(formattedtime)")
        let dateMakerFormatter1 = DateFormatter()
        dateMakerFormatter1.dateFormat = "HH:mm"
        let startTime = dateMakerFormatter1.date(from: originaltime)!
        let strtime = dateMakerFormatter1.string(from: startTime)
        //this comes back as 12:40 am not pm
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let strDate = dateFormatter.string(from: date as Date)
        Dateupdatetf.text=strDate
        Timeupdatetf.text=strtime
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Codetf.isUserInteractionEnabled = true
        Codetf.placeholder = "CustomerCode"
        scrollSize()
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "yyyy-MM-dd"
        self.Datetf.text = dateFormatter.string(from: Datepicker.date)
        //self.Date2tf.text = dateFormatter.stringFromDate(Datepicker.date)
        if (CLLocationManager.locationServicesEnabled())  {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
        
        if CustomcodeCheck == nil || CustomcodeCheck == "" {
            self.Codetf.text = ""
        }
        else {
            self.Codetf.text = LibraryAPI.sharedInstance.CustomerCode
            self.Nametf.text = Customername
            if metwhomtextfield.text == ""{
                getUserDetauls(code: self.Codetf.text!)
            }
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = DateFormatter.Style.long
            dateFormatter.dateFormat = "yyyy-MM-dd"
            self.Datetf.text = dateFormatter.string(from: Datepicker.date)
        }
        
        
        if(Contact_Bool == true) {
            self.setContactNumber()
        }
        if (chkMetNameVal != 0) {
            self.showToast(message: "character should not exist 25 ")
            chkMetNameVal = 0
        }
        if (chkphnumVal != 0){
            self.showToast(message: "character should not exist 25 ")
            chkphnumVal = 0
        }
        
    }
    
    func setContactNumber()
    {
        self.chkphnumVal = 0
        if(self.ContactNumbersArr.count != 0) {
            if(self.ContactNumbersArr.count == 1) {
                self.Phonenumbertf.text = self.ContactNumbersArr[0]
                let phoneval = self.Phonenumbertf.text
                if (phoneval?.count)! > 20 {
                    self.metwhomtextfield.text = ""
                    self.Phonenumbertf.text = ""
                    self.ContactNumbersArr.removeAll()
                    self.chkphnumVal = 1
                    self.showToast(message: "character should not exist 20 ")
                }else{
                    self.Contact_Bool = true
                    self.chkphnumVal = 0
                }
            }  else {
                let alert:UIAlertController=UIAlertController(title: "Select Contact", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
                var alert1 = UIAlertAction()
                for word in self.ContactNumbersArr  {
                    alert1 = UIAlertAction(title: (word ), style: UIAlertActionStyle.default) {
                        UIAlertAction in
                        self.Phonenumbertf.text = word
                        self.Contact_Bool = false
                    }
                    alert.addAction(alert1)
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                    UIAlertAction in
                }
                alert.addAction(cancelAction)
                self.present(alert, animated: true, completion: nil)
            }
            self.chkphnumVal = 0
        } else  {
            self.chkphnumVal = 0
            self.Phonenumbertf.text = ""
        }
    }
    
    
    func getUserDetauls(code: String)
    {
        let name = LibraryAPI.sharedInstance.Userid.stringByRemovingWhitespaces
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Users.asmx/OldVisitLogDetails?UserID=\(name)&CusCode=\(code)", completion:  { (jsonmutable) in
            self.responsestring1 = jsonmutable
            if(self.responsestring1.count > 0) {
                let dic = self.responsestring1[0] as! NSDictionary
                self.metwhomtextfield.text  = dic.object(forKey: "MET WHOM") as? String
                self.Designationtf.text     = dic.object(forKey: "DESIGNATION") as? String
                self.Phonenumbertf.text     = dic.object(forKey: "PHONE NUMBER") as? String
                self.Emailtf.text           = dic.object(forKey: "MAIL ID") as? String
            } else {
            }
        })
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location1 = locations.last! as CLLocation
        latitude = location1.coordinate.latitude
        longitude = location1.coordinate.longitude
        let location = CLLocation(latitude: latitude, longitude: longitude)
        if latitude == 0.0 && longitude == 0.0 {
            if LibraryAPI.sharedInstance.networkmode == LibraryAPI.sharedInstance.WIFI{
                showToast(message: "Can not fetch your current location in Wifi network. Please Switch to Mobile network")
            }else if LibraryAPI.sharedInstance.networkmode == LibraryAPI.sharedInstance.cell{
                showToast(message: "Can not fetch your current location. Please try again!")
            }
        }else{
            CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
                if error != nil {
                    //self.showToast(message: String(describing: error))
                    print("error in geocoder")
                } else {
                    if placemarks!.count > 0 {
                        let pm = placemarks![0]
                        self.displayLocationInfo(placemark: pm)
                        //self.current_area = pm.subLocality
                        self.current_area   = pm.subLocality!
                        self.current_pin    = pm.postalCode!
                        
                    } else {
                        self.showToast(message: "Delay in Server Response")
                        self.current_area = ""
                        self.current_pin  = ""
                    }
                }
                
            })
        }
        
        
    }
    func displayLocationInfo(placemark: CLPlacemark) {
        locationManager.stopUpdatingLocation()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Scrollview.isScrollEnabled=true
        scrollSize()
       // Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:780)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    @IBAction func customerAction(_ sender: AnyObject) {
        if Customer_Value == 1
        {
            let alertController = UIAlertController(title: "Alert", message: "Do you want to change the visit type?", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
                self.type="Customer"
                self.customerimg.image = UIImage(named:"green2.png")
                self.prospectimg.image = UIImage(named: "normal.png")
                self.vendorimg.image = UIImage(named: "normal.png")
                self.Codetf.isUserInteractionEnabled = true
                self.Codetf.placeholder = ""
                self.Nametf.text = ""
                self.fromtimetf.text = ""
                self.Totime.text = ""
                self.metwhomtextfield.text = ""
                self.Designationtf.text = ""
                self.Phonenumbertf.text = ""
                self.Emailtf.text = ""
                self.Locationtf.text = ""
                self.Naturetf.text = ""
                self.Textview1.text = ""
                self.tableviewresponsearray.removeAllObjects()
                self.Arr.removeAll()
                self.businesstableview.reloadData()
                self.Scrollview.isScrollEnabled=true
                self.Scrollview.contentSize = CGSize(width: self.Scrollview.frame.size.width, height:780)
                self.Codetf.isHidden = false
                // self.Customercode2lbl.hidden = false
                self.Datelbl.isHidden = false
                // self.Date2tf.hidden = true
                self.Date3_value = false
                self.Codetf.placeholder = "CustomerCode"
                
            }
            let cancelAction1 = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                UIAlertAction in
            }
            alertController.addAction(cancelAction)
            alertController.addAction(cancelAction1)
            self.present(alertController, animated: true, completion:nil)
            Customer_Value = 2
        }
        else
        {
            
        }
        
        Prospect_Value = 1
        Vendor_Value = 1
        
        
        
    }
    
    func Customnavigation()
    {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: self.view.frame.height / 10.3);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(VisitLogViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Daily Field Visit"
        
    }
    
  /*  func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            self.fab.close()
        }
        
        self.view.addSubview(fab)
        
    }*/
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0,y: 0,width: 320,height: 50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(VisitLogViewController.doneButtonAction))
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        doneToolbar.items = items as AnyObject as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        Phonenumbertf.inputAccessoryView = doneToolbar
        
    }
    
    func doneButtonAction(){
        Phonenumbertf.resignFirstResponder()
    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
        
    }
    
    
    func navigate() {
        
        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
        self.navigationController?.pushViewController(lvc, animated: true)
        
        
    }
    
    
    func buttonClicked(sender:UIButton) {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
    }
    
    
    @IBAction func Doneaction(_ sender: AnyObject) {
        DatePickerview.isHidden=true
        if( currenttf=="Date") {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = DateFormatter.Style.long
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let strDate = dateFormatter.string(from: Datepicker.date)
            Datetf.text=strDate
        } else if( currenttf=="Date2")  {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = DateFormatter.Style.long
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let strDate = dateFormatter.string(from: Datepicker.date)
        } else if( currenttf=="Time"){
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            let strDate = dateFormatter.string(from: Datepicker1.date)
            fromtimetf.text=strDate
        } else if( currenttf=="to") {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            let strDate = dateFormatter.string(from: Datepicker1.date)
            Totime.text=strDate
        } else if( currenttf=="DateUpdate") {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let strDate = dateFormatter.string(from: Datepicker.date)
            Dateupdatetf.text=strDate
        } else if( currenttf=="Timeupdate") {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            let strDate = dateFormatter.string(from: Datepicker1.date)
            Timeupdatetf.text=strDate
        }
    }
    
    
    
    
    
    
    @IBAction func SubmitAction(_ sender: AnyObject) {
        
        if locationAccess == 0{
            locationProcess()
        } else if latitude == 0.0 && longitude == 0.0 {
            if LibraryAPI.sharedInstance.networkmode == LibraryAPI.sharedInstance.WIFI {
                showToast(message: "Can not fetch your current location in Wifi network. Please Switch to Mobile network")
            } else if LibraryAPI.sharedInstance.networkmode == LibraryAPI.sharedInstance.cell {
                showToast(message: "Can not fetch your current location. Please try again!")
            }
            locationProcess()
        } else if(self.Nametf.text!.isEmpty) {
            let alertController = UIAlertController(title: "Alert", message:"Please Enter the Name" , preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
            }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)
        }
        else if(self.Datetf.text!.isEmpty)
        {
            let alertController = UIAlertController(title: "Alert", message:"Please enter the date" , preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                
            }
            alertController.addAction(cancelAction)
            
            
            self.present(alertController, animated: true, completion:nil)
        }
        else if(self.fromtimetf.text!.isEmpty)
        {
            let alertController = UIAlertController(title: "Alert", message:"Please enter the fromtime" , preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                
            }
            alertController.addAction(cancelAction)
            
            
            self.present(alertController, animated: true, completion:nil)
        }
        else if(self.Totime.text!.isEmpty)
        {
            
            
            let alertController = UIAlertController(title: "Alert", message:"Please enter to time" , preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                
            }
            alertController.addAction(cancelAction)
            
            
            self.present(alertController, animated: true, completion:nil)
        }
            
            
        else if(self.Naturetf.text!.isEmpty)
        {
            let alertController = UIAlertController(title: "Alert", message:"Please enter the Nature " , preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                
            }
            alertController.addAction(cancelAction)
            
            
            self.present(alertController, animated: true, completion:nil)
        }
            
            
        else if(self.metwhomtextfield.text!.isEmpty)
        {
            let alertController = UIAlertController(title: "Alert", message:"Please enter the metwhom Field" , preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                
            }
            alertController.addAction(cancelAction)
            
            
            self.present(alertController, animated: true, completion:nil)
        }
            
        else if(self.Locationtf.text!.isEmpty)
        {
            let alertController = UIAlertController(title: "Alert", message:"Please enter the Area Field" , preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                
            }
            alertController.addAction(cancelAction)
            
            
            self.present(alertController, animated: true, completion:nil)
        }
            
        else if(self.Textview1.text!.isEmpty)
        {
            let alertController = UIAlertController(title: "Alert", message:"Please enter the Description" , preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                
            }
            alertController.addAction(cancelAction)
            
            
            self.present(alertController, animated: true, completion:nil)
        }
            
            
        else if(self.type=="empty")
        {
            
            let alertController = UIAlertController(title: "Alert", message:"Please enter visit type" , preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                
            }
            alertController.addAction(cancelAction)
            
            
            self.present(alertController, animated: true, completion:nil)
            
        }
        else if(self.isValidEmail(testStr: Emailtf.text!)==false)
        {
            let alertController = UIAlertController(title: "Alert", message:"E-mail is not Valid" , preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                
            }
            alertController.addAction(cancelAction)
            
            
            self.present(alertController, animated: true, completion:nil)
        }
        else if((self.Phonenumbertf.text?.count)!<10)
        {
            let alertController = UIAlertController(title: "Alert", message:"Phone No is Too Short " , preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
            }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)
        }
        else
        {
            
            self.setdateandtime()
            switch self.tableviewresponsearray.count
            {
            case 0:
                
                self.uploadfile.setObject("", forKey: "BUSINESSCODE" as NSCopying)
                self.uploadfile.setObject("", forKey: "BUSINESSCODE1" as NSCopying)
                self.uploadfile.setObject("", forKey: "BUSINESSCODE2" as NSCopying)
                self.uploadfile.setObject("", forKey: "BUSINESSCODE3" as NSCopying)
                self.uploadfile.setObject("", forKey: "BUSINESSCODE4" as NSCopying)
                
                break
                
            case 1:
                
                let fullName : String = self.tableviewresponsearray.object(at: 0) as! String
                let fullNameArr : [String] = fullName.components(separatedBy: " - ")
                let firstName : String = fullNameArr[0]
                
                
                
                self.uploadfile.setObject(firstName, forKey: "BUSINESSCODE" as NSCopying)
                self.uploadfile.setObject("", forKey: "BUSINESSCODE1" as NSCopying)
                self.uploadfile.setObject("", forKey: "BUSINESSCODE2" as NSCopying)
                self.uploadfile.setObject("", forKey: "BUSINESSCODE3" as NSCopying)
                self.uploadfile.setObject("", forKey: "BUSINESSCODE4" as NSCopying)
                
                break
                
            case 2:
                
                let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                let firstName1 : String = fullNameArr1[0]
                
                let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                let firstName2 : String = fullNameArr2[0]
                
                self.uploadfile.setObject(firstName1, forKey: "BUSINESSCODE" as NSCopying)
                self.uploadfile.setObject(firstName2, forKey: "BUSINESSCODE1" as NSCopying)
                self.uploadfile.setObject("", forKey: "BUSINESSCODE2" as NSCopying)
                self.uploadfile.setObject("", forKey: "BUSINESSCODE3" as NSCopying)
                self.uploadfile.setObject("", forKey: "BUSINESSCODE4" as NSCopying)
                break
                
            case 3:
                let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                let firstName1 : String = fullNameArr1[0]
                
                let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                let firstName2 : String = fullNameArr2[0]
                
                let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                let firstName3 : String = fullNameArr3[0]
                
                self.uploadfile.setObject(firstName1, forKey: "BUSINESSCODE" as NSCopying)
                self.uploadfile.setObject(firstName2, forKey: "BUSINESSCODE1" as NSCopying)
                self.uploadfile.setObject(firstName3, forKey: "BUSINESSCODE2" as NSCopying)
                self.uploadfile.setObject("", forKey: "BUSINESSCODE3" as NSCopying)
                self.uploadfile.setObject("", forKey: "BUSINESSCODE4" as NSCopying)
                break
                
            case 4:
                
                let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                let firstName1 : String = fullNameArr1[0]
                
                let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                let firstName2 : String = fullNameArr2[0]
                
                let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                let firstName3 : String = fullNameArr3[0]
                
                let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
                let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
                let firstName4 : String = fullNameArr4[0]
                
                self.uploadfile.setObject(firstName1, forKey: "BUSINESSCODE" as NSCopying)
                self.uploadfile.setObject(firstName2, forKey: "BUSINESSCODE1" as NSCopying)
                self.uploadfile.setObject(firstName3, forKey: "BUSINESSCODE2" as NSCopying)
                self.uploadfile.setObject(firstName4, forKey: "BUSINESSCODE3" as NSCopying)
                self.uploadfile.setObject("", forKey: "BUSINESSCODE4" as NSCopying)
                break
                
            case 5:
                
                let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                let firstName1 : String = fullNameArr1[0]
                
                let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                let firstName2 : String = fullNameArr2[0]
                
                let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                let firstName3 : String = fullNameArr3[0]
                
                let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
                let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
                let firstName4 : String = fullNameArr4[0]
                
                let fullName5 : String = self.tableviewresponsearray.object(at: 4) as! String
                let fullNameArr5 : [String] = fullName5.components(separatedBy: " - ")
                let firstName5 : String = fullNameArr5[0]
                
                self.uploadfile.setObject(firstName1, forKey: "BUSINESSCODE" as NSCopying)
                self.uploadfile.setObject(firstName2, forKey: "BUSINESSCODE1" as NSCopying)
                self.uploadfile.setObject(firstName3, forKey: "BUSINESSCODE2" as NSCopying)
                self.uploadfile.setObject(firstName4, forKey: "BUSINESSCODE3" as NSCopying)
                self.uploadfile.setObject(firstName5, forKey: "BUSINESSCODE4" as NSCopying)
                break
                
            default:
                break
            }
            
            
            if (self.Date3_value == false)
            {
                uploadfile.setObject(Datetf.text!, forKey: "DATEOFVISIT" as NSCopying)
            }
            else
                
            {
                uploadfile.setObject(Datetf.text!, forKey: "DATEOFVISIT" as NSCopying)
            }
            
            uploadfile.setObject(type.stringByRemovingWhitespaces as String, forKey: "TYPE" as NSCopying)
            uploadfile.setObject(Codetf.text!.stringByRemovingWhitespaces as String, forKey: "CUSTOMERCODE" as NSCopying)
            uploadfile.setObject(Nametf.text!.stringByRemovingWhitespaces as String, forKey: "NAME" as NSCopying)
            
            let str1=Datetf.text?.appending(" \(fromtimetf.text!)")
            uploadfile.setObject(str1!, forKey: "FROMTIME" as NSCopying)
            let str=Datetf.text?.appending(" \(Totime.text!)")
            uploadfile.setObject(str!, forKey: "TOTIME" as NSCopying)
            let txt=Textview1.text!
            self.removeSpecialCharsFromString(text: txt)
            let trimmedString = txt.trimmingCharacters(in: .whitespaces)
            
            uploadfile.setObject(trimmedString.stringByRemovingWhitespaces as String, forKey: "MEETINGDESCRIPTION" as NSCopying)
            uploadfile.setObject(Naturetf.text!.stringByRemovingWhitespaces as String, forKey: "Nature" as NSCopying)
            uploadfile.setObject(metwhomtextfield.text!.stringByRemovingWhitespaces as String, forKey: "METWHOM" as NSCopying)
            uploadfile.setObject(Locationtf.text!.stringByRemovingWhitespaces as String, forKey: "AREA" as NSCopying)
            uploadfile.setObject(Designationtf.text!.stringByRemovingWhitespaces as String, forKey: "DESIGNATION" as NSCopying)
            uploadfile.setObject(Phonenumbertf.text!.stringByRemovingWhitespaces as String, forKey: "PHONENUMBER" as NSCopying)
            uploadfile.setObject(Emailtf.text!.stringByRemovingWhitespaces as String, forKey: "MAILID" as NSCopying)
            uploadfile.setObject(LibraryAPI.sharedInstance.Userid.stringByRemovingWhitespaces as String, forKey: "USERUPDATE" as NSCopying)
            uploadfile.setObject(Dateupdatetf.text!, forKey: "DATEUPDATE" as NSCopying)
            
            let business                = bizcodetf.text!
            let fullNameArr : [String]  = business.components(separatedBy: " - ")
            let firstName : String      = fullNameArr[0]
            
            uploadfile.setObject(Timeupdatetf.text!, forKey: "TimeUpdate" as NSCopying)
            
            if (self.current_pin == "") {
                uploadfile.setObject("", forKey: "PINCODE" as NSCopying)
                if(self.current_area == "")
                {
                    uploadfile.setObject("", forKey: "PLACE" as NSCopying)
                }else{
                    uploadfile.setObject(current_area, forKey: "PLACE" as NSCopying)
                }
                
            }else if(self.current_area == "")
            {
                uploadfile.setObject("", forKey: "PLACE" as NSCopying)
                if (self.current_pin == "") {
                    uploadfile.setObject("", forKey: "PINCODE" as NSCopying)
                }else{
                    uploadfile.setObject(current_pin, forKey: "PINCODE" as NSCopying)
                }
                
            }
            else{
                uploadfile.setObject(current_pin, forKey: "PINCODE" as NSCopying)
                uploadfile.setObject(current_area, forKey: "PLACE" as NSCopying)
            }
            uploadfile.setObject(latitude, forKey: "LATITUDE" as NSCopying)
            uploadfile.setObject(longitude, forKey: "LONGITUDE" as NSCopying)
            
            // uploadfile.setObject(LibraryAPI.sharedInstance.visitlogticketnumber, forKey: "VisitId")
            
            let alert = UIAlertController(title: nil, message: "Loading", preferredStyle: .alert)
            let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
            loadingIndicator.hidesWhenStopped = true
            loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
            loadingIndicator.startAnimating();
            alert.view.addSubview(loadingIndicator)
            present(alert, animated: true, completion: nil)
            
            let size = CGSize(width: 30, height:30)
            self.local_activityIndicator_start()
            DispatchQueue.global(qos: .background).async {
                self.uploaddata()
                DispatchQueue.main.async {
                    if(self.jsonresult.object(forKey: "Status") as? String  == "Success")
                    {
                        self.local_activityIndicator_stop()
                        self.dismiss(animated: false, completion: nil)
                        let alertController = UIAlertController(title: "Alert", message: "Visit Log Updated, Your ID : \(self.jsonresult.object(forKey: "VisitId")!)" , preferredStyle: .alert)
                        
                        let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                            
                            self.navigate()
                            
                        }
                        alertController.addAction(cancelAction)
                        
                        
                        self.present(alertController, animated: true, completion:nil)
                    }
                    else
                    {
                        self.dismiss(animated: false, completion: nil)
                        self.local_activityIndicator_stop()
                        let alertController = UIAlertController(title: "Alert", message:"Submisson Failed! Retry" , preferredStyle: .alert)
                        
                        let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                            
                            
                            
                        }
                        alertController.addAction(cancelAction)
                        
                        
                        self.present(alertController, animated: true, completion:nil)
                        self.SubmitBtn.backgroundColor = UIColor.red
                    }
                    
                    
                }
            }
            
            // new version dispatch Que
            
            
            
        }
        
        
        
        
    }
    
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-*=(),.:!_")
        return String(text.filter {okayChars.contains($0) })
    }
    
    func uploaddata()
    {
        self.local_activityIndicator_start()
        let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)Users.asmx/CustomerLogUpdation")! as URL)
        request.httpMethod = "POST"
        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
        request.httpBody = try? JSONSerialization.data(withJSONObject: self.uploadfile, options: JSONSerialization.WritingOptions(rawValue: 0))
        //print(uploadfile)
        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
        do
        {
            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
            jsonresult = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            
        }
        catch (let e)
        {
            print(e)
             self.local_activityIndicator_stop()
        }
        
    }
    
    
    @IBAction func ContactDetailsBtn(_ sender: AnyObject)
    {
        askForContactAccess()
        if contactAccess == 0{
            askForContactAccess()
        }else{
            let contactPickerViewController = CNContactPickerViewController()
            contactPickerViewController.delegate = self
            self.present(contactPickerViewController, animated: true, completion: nil)
        }
    }
    
    @IBAction func CancelAction(_ sender: AnyObject)
    {
        DatePickerview.isHidden=true
    }
    
    @IBAction func ProspectAction(_ sender: AnyObject)
    {
        
        if (Prospect_Value == 1)
        {
            let alertController = UIAlertController(title: "Alert", message: "Do you want to change the visit type?", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
                
                self.type="Prospect"
                self.customerimg.image  = UIImage(named: "normal.png")
                self.prospectimg.image  = UIImage(named: "green2.png")
                self.vendorimg.image    = UIImage(named: "normal.png")
                self.Codetf.isUserInteractionEnabled = false
                self.Codetf.text = ""
                self.Codetf.placeholder = "N/A"
                self.Nametf.text = ""
                self.fromtimetf.text = ""
                self.Totime.text = ""
                self.metwhomtextfield.text = ""
                self.Designationtf.text = ""
                self.Phonenumbertf.text = ""
                self.Emailtf.text = ""
                self.Locationtf.text = ""
                self.Naturetf.text = ""
                self.Textview1.text = ""
                self.tableviewresponsearray.removeAllObjects()
                self.Arr.removeAll()
                self.businesstableview.reloadData()
                self.Scrollview.isScrollEnabled=true
                self.Scrollview.contentSize = CGSize(width: self.Scrollview.frame.size.width, height:780)
                self.Codetf.isHidden = true
                self.Customercode2lbl.isHidden = true
                self.Date3_value = true
                self.Codetf.placeholder = "CustomerCode"
            }
            let cancelAction1 = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                UIAlertAction in
            }
            alertController.addAction(cancelAction)
            alertController.addAction(cancelAction1)
            self.present(alertController, animated: true, completion:nil)
            Prospect_Value = 2
        }
        else
        {
            
        }
        
        Customer_Value = 1
        Vendor_Value = 1
    }
    
    @IBAction func VendorACtion(_ sender: AnyObject)
    {
        if Vendor_Value == 1
        {
            let alertController = UIAlertController(title: "Alert", message: "Do you want to change the visit type?", preferredStyle: .alert)
            
            
            let cancelAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
                self.type="Vendor"
                //self.Vendorbtn.setImage(UIImage(named:"green2.png"), forState: UIControlState.Normal)
                //self.Prospectbtn.setImage(UIImage(named:"normal.png"), forState: UIControlState.Normal)
                //self.Customerbtn.setImage(UIImage(named:"normal.png"), forState: UIControlState.Normal)
                self.customerimg.image  = UIImage(named: "normal.png")
                self.prospectimg.image  = UIImage(named: "normal.png")
                self.vendorimg.image    = UIImage(named: "green2.png")
                self.Codetf.isUserInteractionEnabled = false
                self.Codetf.text = ""
                self.Codetf.placeholder = "N/A"
                self.Nametf.text = ""
                self.fromtimetf.text = ""
                self.Totime.text = ""
                self.metwhomtextfield.text = ""
                self.Designationtf.text = ""
                self.Phonenumbertf.text = ""
                self.Emailtf.text = ""
                self.Locationtf.text = ""
                self.Naturetf.text = ""
                self.Textview1.text = ""
                self.tableviewresponsearray.removeAllObjects()
                self.Arr.removeAll()
                self.businesstableview.reloadData()
                self.Scrollview.isScrollEnabled=true
                self.Scrollview.contentSize = CGSize(width: self.Scrollview.frame.size.width, height:780)
                self.Codetf.isHidden = true
                self.Customercode2lbl.isHidden = true
                self.Datelbl.isHidden = false
                //  self.Date2tf.hidden = false
                self.Date3_value = true
                self.Codetf.placeholder = "CustomerCode"
                
                
                
                
            }
            let cancelAction1 = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                UIAlertAction in
            }
            alertController.addAction(cancelAction)
            alertController.addAction(cancelAction1)
            self.present(alertController, animated: true, completion:nil)
            Vendor_Value = 2
        }
        else
        {
        }
        
        Customer_Value = 1
        Prospect_Value = 1
    }
    
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        
        //        let searchTerm = textView.text
        //
        //        let characterset = NSCharacterSet(charactersInString: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        //        if searchTerm.rangeOfCharacterFromSet(characterset.invertedSet) != nil {
        //        }
        
        let currentCharacterCount = textView.text?.count ?? 0
        if range.length + range.location > currentCharacterCount {
            return false
        }
        let newLength = currentCharacterCount + text.count - range.length
        
        let allowedCharacters = " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 .,"
        
        let set = NSCharacterSet(charactersIn: allowedCharacters);
        let inverted = set.inverted;
        
        let filtered = text
            .components(separatedBy: inverted)
            .joined(separator: "");
        return (filtered == text)&&(newLength <= 150)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField.tag==11)
        {
            if(Phonenumber == true)
            {
                guard let text = textField.text else { return true }
                
                let newLength = text.count + string.count - range.length
                return newLength <= 11
            }
            else
            {
                guard let text = textField.text else { return true }
                
                let newLength = text.count + string.count - range.length
                return newLength <= 10
            }
            
        }
        if(textField.tag == 35)       //EmailTf
        {
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength  <= 150
        }
        if(textField.tag == 20)       //EmailTf
        {
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength  <= 40
        }
        
        if(textField.tag == 34)       //Locationtf
        {
            // if string.rangeOfCharacterFromSet(NSCharacterSet.letterCharacterSet()) != nil {
            if string.rangeOfCharacter(from: .letters) != nil
            {
                guard let text = textField.text else { return true }
                let newLength = text.count + string.count - range.length
                return newLength  <= 25
            }
            else
            {
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
                return false
                
            }
            
        }
        
        if(textField.tag == 32)     //Nametf
        {
            print("working on Name")
            let aSet = NSCharacterSet(charactersIn:" abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789,-.'&()").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            //        return string == numberFiltered
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return true && string == numberFiltered && newLength <= 35
        }
        
        if(textField.tag == 31)     // area text field
        {
            print("working on area")
            let aSet = NSCharacterSet(charactersIn:" abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            //        return string == numberFiltered //already command
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return true && string == numberFiltered && newLength <= 25
            
        }
        
        if(textField.tag == 33)     //Designationtf
        {
            let aSet = NSCharacterSet(charactersIn:"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 ,-").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            //        return string == numberFiltered
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return true && string == numberFiltered && newLength <= 25
            
        }
        if(textField.tag == 27) {       // metwhomtextfield
            let aSet = NSCharacterSet(charactersIn:"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 ").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            //        return string == numberFiltered
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return true && string == numberFiltered && newLength <= 25
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        fab.isHidden=true
        LibraryAPI.sharedInstance.poptextfieldclicked=false
        
        var boo = false
        
        if(type == "Customer")
        {
            if textField == Codetf
            {
                LibraryAPI.sharedInstance.Currentcustomerlookup="Visitlog"
                if #available(iOS 10.0, *) {
                    let popup = PopupController
                        .create(parentViewController: self)
                        .show(childViewController: CustomerLookupViewController.instance())
                        
                        .didShowHandler { popup in
                            self.Codetf.resignFirstResponder()
                        }
                        .didCloseHandler { _ in
                            
                    }
                    
                    let container = CustomerLookupViewController.instance()
                    container.closeHandler = { _ in
                        popup.dismiss()
                    }
                    
                    popup.show(childViewController: container)
                } else {
                    // Fallback on earlier versions
                }
            }
            else
            {
                if Codetf.text == ""
                {
                    boo=false
                    
                    let alertController = UIAlertController(title: "Alert", message: "Enter Customer Code" , preferredStyle: .alert)
                    
                    let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                        
                    }
                    alertController.addAction(cancelAction)
                    
                    
                    self.present(alertController, animated: true, completion:nil)
                }
                else
                {
                    boo=true
                }
            }
            
        }
        else{
            boo=true
        }
        
        
        if(boo == true)
        {
            
            if(textField.tag==1)
            {
                DatePickerview.isHidden=false
                Datepicker1.isHidden = true
                Datepicker.isHidden = false
                let dateFormatter = DateFormatter()
                dateFormatter.dateStyle = DateFormatter.Style.long
                dateFormatter.dateFormat = "yyyy-MM-dd"
                Datepicker.maximumDate = NSDate() as Date
                let dateComponents = NSDateComponents()
                dateComponents.day = -4
                let currentCalendar = NSCalendar.current
                // let oneMonthBack = currentCalendar.date(byAdding: dateComponents as DateComponents, to: NSData)
                let oneMonthBack = currentCalendar.date(byAdding: dateComponents as! DateComponents, to: NSDate() as! Date)
                Datepicker.minimumDate = oneMonthBack
                currenttf="Date"
                return false
            }
            
            
            if(textField.tag==30)
            {
                
                DatePickerview.isHidden=false
                Datepicker1.isHidden = true
                Datepicker.isHidden = false
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateStyle = DateFormatter.Style.long
                dateFormatter.dateFormat = "yyyy-MM-dd"
                Datepicker.maximumDate = NSDate() as Date
                let dateComponents = NSDateComponents()
                dateComponents.day = -4
                let currentCalendar = NSCalendar.current
                //  let oneMonthBack = currentCalendar.date(byAdding: dateComponents, to: NSDate, option: [])!
                let oneMonthBack = currentCalendar.date(byAdding: dateComponents as! DateComponents, to: NSDate() as! Date)
                Datepicker.minimumDate = oneMonthBack
                
                currenttf="Date2"
                return false
                
            }
            
            
            if(textField.tag==2)
            {
                DatePickerview.isHidden=false
                Nametf.resignFirstResponder()
                Datepicker1.isHidden = false
                Datepicker.isHidden = true
                Datepicker1.datePickerMode = UIDatePickerMode.time
                let formatter = DateFormatter()
                formatter.dateFormat = "HH:mm"
                Datepicker1.minimumDate = formatter.defaultDate
                currenttf="Time"
                return false
                
                
            }
            if(textField.tag==3)
            {
                DatePickerview.isHidden=false
                Nametf.resignFirstResponder()
                Datepicker1.isHidden = false
                Datepicker.isHidden = true
                Datepicker1.datePickerMode = UIDatePickerMode.time
                let formatter = DateFormatter()
                formatter.dateFormat = "HH:mm"
                Datepicker1.minimumDate = Datepicker1.date
                currenttf="to"
                return false
                
            }
            
            if(textField.tag==5)
            {
                DatePickerview.isHidden=false
                Datepicker.datePickerMode = UIDatePickerMode.date
                currenttf="DateUpdate"
                return false
                
            }
            
            if(textField.tag==6)
            {
                DatePickerview.isHidden=false
                Datepicker1.datePickerMode = .time
                currenttf="Timeupdate"
                return false
                
                
            }
            
            
            
            if(textField.tag==9)
            {
                textField.resignFirstResponder()
                let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Nature", message: "Pick an option", preferredStyle: .actionSheet)
                
                let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
                    
                }
                actionSheetControllerIOS8.addAction(cancelActionButton)
                
                let saveActionButton: UIAlertAction = UIAlertAction(title: "SALES CALL", style: .default)
                { action -> Void in
                    
                    self.Naturetf.text="SALES CALL"
                }
                actionSheetControllerIOS8.addAction(saveActionButton)
                
                let deleteActionButton: UIAlertAction = UIAlertAction(title: "COLLECTIONS", style: .default)
                { action -> Void in
                    
                    self.Naturetf.text="COLLECTIONS"
                }
                
                actionSheetControllerIOS8.addAction(deleteActionButton)
                
                let PresentationActionButton: UIAlertAction = UIAlertAction(title: "PRESENTATION", style: .default)
                { action -> Void in
                    
                    self.Naturetf.text="PRESENTATION"
                }
                
                actionSheetControllerIOS8.addAction(PresentationActionButton)
                
                let courtesy: UIAlertAction = UIAlertAction(title: "COURTESY", style: .default)
                { action -> Void in
                    
                    self.Naturetf.text="COURTESY"
                }
                
                
                actionSheetControllerIOS8.addAction(courtesy)
                
                let poc: UIAlertAction = UIAlertAction(title: "POC", style: .default)
                { action -> Void in
                    
                    self.Naturetf.text="POC"
                }
                actionSheetControllerIOS8.addAction(poc)
                let training: UIAlertAction = UIAlertAction(title: "TRAINING", style: .default)
                { action -> Void in
                    
                    self.Naturetf.text="TRAINING"
                }
                
                actionSheetControllerIOS8.addAction(training)
                
                
                let DirectCustomer: UIAlertAction = UIAlertAction(title: "DIRECT CUSTOMER", style: .default)
                { action -> Void in
                    
                    self.Naturetf.text="DIRECT CUSTOMER"
                }
                
                actionSheetControllerIOS8.addAction(DirectCustomer)
                
                let Partner: UIAlertAction = UIAlertAction(title: "PARTNER", style: .default)
                { action -> Void in
                    
                    self.Naturetf.text="PARTNER"
                }
                
                actionSheetControllerIOS8.addAction(Partner)
                
                
                let Technical : UIAlertAction = UIAlertAction(title: "TECHNICAL", style: .default)
                { action -> Void in
                    
                    self.Naturetf.text="TECHNICAL"
                }
                
                actionSheetControllerIOS8.addAction(Technical )
                
                
                let Discussion: UIAlertAction = UIAlertAction(title: "DISCUSSION", style: .default)
                { action -> Void in
                    
                    self.Naturetf.text="DISCUSSION"
                }
                
                actionSheetControllerIOS8.addAction(Discussion)
                
                
                let FolowUp: UIAlertAction = UIAlertAction(title: "FOLLOW UP", style: .default)
                { action -> Void in
                    
                    self.Naturetf.text="FOLLOW UP"
                }
                
                actionSheetControllerIOS8.addAction(FolowUp)
                
                
                let Vendor: UIAlertAction = UIAlertAction(title: "VENDOR", style: .default)
                { action -> Void in
                    
                    self.Naturetf.text="VENDOR"
                }
                
                actionSheetControllerIOS8.addAction(Vendor)
                
                
                
                
                self.present(actionSheetControllerIOS8, animated: true, completion: nil)
                
                return false
            }
            if (textField.tag == 25)
            {
                textField.resignFirstResponder()
                let size = CGSize(width: 50, height:50)
                self.local_activityIndicator_start()
                let alert:UIAlertController=UIAlertController(title: "Select Business Code", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
                var alert1 = UIAlertAction()
                if pickerarray.count != 0
                {
                    self.local_activityIndicator_stop()
                    for word in pickerarray
                    {
                        alert1 = UIAlertAction(title: (word as! String), style: UIAlertActionStyle.default)
                        {
                            UIAlertAction in
                            
                            
                            let getString : String = word as! String
                            
                            if self.tableviewresponsearray.count < 5
                            {
                                if self.tableviewresponsearray.count == 0
                                {
                                    self.tableviewresponsearray.add(getString)
                                    self.Arr.append(getString)
                                    self.businesstableview.reloadData()
                                }
                                else
                                {
                                    let contains = self.tableviewresponsearray.contains
                                    {
                                        $0 as? String == getString
                                    }
                                    
                                    if contains == false
                                    {
                                        self.tableviewresponsearray.add(getString)
                                        self.Arr.append(getString)
                                        self.businesstableview.reloadData()
                                    }
                                    else
                                    {
                                        self.showToast(message: "You are Selected same Business Code")
                                    }
                                }
                            }
                            else
                            {
                                self.showToast(message: "Alreay You have Selected 5 Business code")
                            }
                            
                        }
                        
                        alert.addAction(alert1)
                        
                    }
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
                {
                    UIAlertAction in
                }
                alert.addAction(cancelAction)
                
                self.present(alert, animated: true, completion: nil)
                
                return false
                
            }
            if(textField.tag == 27)
            {
                
            }
            
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        fab.isHidden=false
        textField.resignFirstResponder()
        
        return true
    }
    
    func Loaddata(url:String)
    {
        //responsearray = LibraryAPI.sharedInstance.RequestUrl(url: url)
        self.getsuccess(params: [:], urlhit: url, completion:  { (jsonmutable) in
            self.responsearray = jsonmutable
            if(self.responsearray.count > 0) {
                
            }
            self.pickerarray.removeAllObjects()
            for i in 0  ..< self.responsearray.count {
                let dic = self.responsearray[i] as! NSDictionary
                let code = dic.object(forKey: "BUSINESS CODE") as? String
                let desc = dic.object(forKey: "BUSINESS DESC") as? String
                let str=code?.appending(" - \(desc!)")
                self.pickerarray.add(str!)
            }
            self.local_activityIndicator_stop()
            self.Datepicker.isHidden=false
        })
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if (self.Arr.count == 0) {
            businesstableview.frame = CGRect(x: 0, y: 724, width: self.businesstableview.frame.width, height: 0)
            Containerview.frame = CGRect(x: 0, y: 0, width: self.Containerview.frame.width, height: 1010)
            Topview.frame = CGRect(x: 0, y: 0, width: self.Topview.frame.width, height: self.Topview.frame.height)
            bottonview.frame = CGRect(x: 0, y: 733, width: self.bottonview.frame.width, height: self.bottonview.frame.height)
            Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:780)
            self.tableviewview.isHidden = true
            return 0
        } else {
            if (self.Arr.count == 1) {
                businesstableview.frame = CGRect(x: 0, y: 724, width: self.businesstableview.frame.width, height: 0+40)
                Containerview.frame = CGRect(x: 0, y: 0, width: self.Containerview.frame.width, height: 1010)
                Topview.frame = CGRect(x: 0, y: 0, width: self.Topview.frame.width, height: self.Topview.frame.height)
                bottonview.frame = CGRect(x: 0, y: 733 + 40, width: self.bottonview.frame.width, height: self.bottonview.frame.height)
                self.tableviewview.isHidden = false
                Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:860)
            } else if (self.Arr.count == 2) {
                businesstableview.frame = CGRect(x: 0, y: 724, width: self.businesstableview.frame.width, height: 0+80)
                Containerview.frame = CGRect(x: 0, y: 0, width: self.Containerview.frame.width, height: 1010)
                Topview.frame = CGRect(x: 0, y: 0, width: self.Topview.frame.width, height: self.Topview.frame.height)
                bottonview.frame = CGRect(x: 0, y: 733 + 80, width: self.bottonview.frame.width, height: self.bottonview.frame.height)
                self.tableviewview.isHidden = false
                Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:900)
            } else if (self.Arr.count == 3) {
                businesstableview.frame = CGRect(x: 0, y: 724, width: self.businesstableview.frame.width, height: 0 + 120)
                Containerview.frame = CGRect(x: 0, y: 0, width: self.Containerview.frame.width, height: 1010)
                Topview.frame = CGRect(x: 0, y: 0, width: self.Topview.frame.width, height: self.Topview.frame.height)
                bottonview.frame = CGRect(x: 0, y: 733 + 120, width: self.bottonview.frame.width, height: self.bottonview.frame.height)
                self.tableviewview.isHidden = false
                Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:940)
            } else if (self.Arr.count == 4) {
                businesstableview.frame = CGRect(x: 0, y: 724, width: self.businesstableview.frame.width, height: 0 + 160)
                Containerview.frame = CGRect(x: 0, y: 0, width: self.Containerview.frame.width, height: 1010)
                Topview.frame = CGRect(x: 0, y: 0, width: self.Topview.frame.width, height: self.Topview.frame.height)
                bottonview.frame = CGRect(x: 0, y: 733 + 160, width: self.bottonview.frame.width, height: self.bottonview.frame.height)
                self.tableviewview.isHidden = false
                Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:980)
            } else if (self.Arr.count == 5) {
                businesstableview.frame = CGRect(x: 0, y: 724, width: self.businesstableview.frame.width, height: 0 + 200)
                Containerview.frame = CGRect(x: 0, y: 0, width: self.Containerview.frame.width, height: 1010)
                Topview.frame = CGRect(x: 0, y: 0, width: self.Topview.frame.width, height: self.Topview.frame.height)
                bottonview.frame = CGRect(x: 0, y: 733 + 200, width: self.bottonview.frame.width, height: self.bottonview.frame.height)
                self.tableviewview.isHidden = false
                Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:1020)
            } else {
                businesstableview.frame = CGRect(x: 0, y: 724, width: self.businesstableview.frame.width, height: 0)
                Containerview.frame = CGRect(x: 0, y: 0, width: self.Containerview.frame.width, height: 1010)
                Topview.frame = CGRect(x: 0, y: 0, width: self.Topview.frame.width, height: self.Topview.frame.height)
                bottonview.frame = CGRect(x: 0, y: 733, width: self.bottonview.frame.width, height: self.bottonview.frame.height)
                self.tableviewview.isHidden = true
                businesstableview.isScrollEnabled = false
                Scrollview.isScrollEnabled=true
                Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:780)
            }
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return tableviewresponsearray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BillingwithodCustomerCell", for: indexPath) as! BillingwithodCustomerCell
        if(tableviewresponsearray.count > 0) {
            cell.deletebtn.addTarget(self, action: #selector(self.deletebtnclicked), for: UIControlEvents.touchUpInside)
            cell.deletebtn.tag = indexPath.row
            let getString = tableviewresponsearray[indexPath.row] as? String
            let fullNameArr5 : [String] = getString!.components(separatedBy: " - ")
            if(fullNameArr5.count >= 3) {
                let bizcode : String = fullNameArr5[0]
                let bizname : String = fullNameArr5[1]
                let bizname1 : String = fullNameArr5[2]
                cell.bizcode.text = bizcode
                cell.bizname.text = bizname + " - " + bizname1
            } else {
                let bizcode : String = fullNameArr5[0]
                let bizname : String = fullNameArr5[1]
                cell.bizcode.text = bizcode
                cell.bizname.text = bizname
            }
        }  else {
        }
        return cell
    }
    func deletebtnclicked(sender: UIButton)
    {
        let alertController = UIAlertController(title: "Business Code", message: "Are You Sure Want to Delete Business Code?", preferredStyle: .alert)
        let DefaultAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
            self.tableviewresponsearray.removeObject(at: sender.tag)
            self.Arr.remove(at: sender.tag)
            self.businesstableview.reloadData()
        }
        let CancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
        }
        alertController.addAction(DefaultAction)
        alertController.addAction(CancelAction)
        self.present(alertController, animated: true, completion:nil)
    }
    
    
    func notificationmethod()
    {
        let userdefaults = Foundation.UserDefaults.standard
        let value = userdefaults.dictionary(forKey: "Notification")
        var title: String!
        var message: String!
        var imagestr: String!
        title = value?["title"] as! String
        message = value?["body"] as! String
        imagestr = value?["imageurl"] as! String
        
        if imagestr != nil
        {
            if imagestr != ""
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: notificationimageViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = notificationimageViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                    
                }
                popup.show(childViewController: container)
            }
            else
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: PushnotificationtextViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = PushnotificationtextViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                }
                popup.show(childViewController: container)
            }
        }
        else
        {
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: PushnotificationtextViewController.instance())
                
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
            }
            let container = PushnotificationtextViewController.instance()
            container.closeHandler = { _ in
                popup.dismiss()
            }
            
            popup.show(childViewController: container)
        }
    }
}
