//
//  Helpdisk1ViewController.swift
//  Redington
//
//  Created by RIL-ITTECH on 22/02/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import Firebase
import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import KCFloatingActionButton


class Helpdisk1ViewController:  UIViewController,UIAlertViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPopoverControllerDelegate,UITextFieldDelegate,KCFloatingActionButtonDelegate,UITextViewDelegate
{
    
    @IBOutlet weak var categorybtn: UIButton!
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var btnClickMe: UIButton!
    @IBOutlet weak var selectionview: UIView!
    @IBOutlet weak var JbaBtn: UIButton!
    @IBOutlet weak var WhatsupBtn: UIButton!
    @IBOutlet weak var containerview: UIView!
    
    var uploadimg : String!
    var uploadimgtype :String!
    @IBOutlet weak var submitbutton: UIButton!
    @IBOutlet weak var phonenumbertf: UITextField!
    @IBOutlet weak var descriptiontv: UITextView!
    var category = NSString()
    var des = NSString()
    var picker=UIImagePickerController()
    var popover:UIPopoverController?=nil
    var uploadfile=NSMutableDictionary()
    var jsonresult=NSDictionary()
    var Movedup=Bool()
    var imagedict=NSMutableDictionary()
    var cattf  : String = ""
    var Jbabutclor      = 1
    var whabutclor      = 0
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        customnavigation() // created process to view the header tittle, back btn and side menu
        descriptiontv.layer.cornerRadius     = 5.0
        descriptiontv.layer.borderWidth      = 1.0
        self.descriptiontv.layer.borderColor = CustomColor.ShareColor.hexStringToUIColor(hex: CustomColor.ShareColor.customGray,alpha: 0.1).cgColor
        phonenumbertf.delegate               = self
        submitbutton.layer.cornerRadius      = self.submitbutton.frame.height/2
        phonenumbertf.tag                    = 11
        phonenumbertf.keyboardType           = .numberPad
        self.addDoneButtonOnKeyboard()
        
        NotificationCenter.default.addObserver(self, selector: #selector(Helpdisk1ViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(Helpdisk1ViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        let returnValue : AnyObject? = UserDefaults.standard.object(forKey: "PhoneNumber") as AnyObject?
        
        if returnValue == nil
        {
            self.phonenumbertf.text = ""
        }
        else
        {
            self.phonenumbertf.text = String(describing: returnValue!)
        }
        
        self.selectionview.layer.cornerRadius   = self.selectionview.frame.size.height/4
        self.selectionview.layer.masksToBounds  = true
        self.WhatsupBtn.layer.cornerRadius      = self.WhatsupBtn.frame.size.height/5
        self.WhatsupBtn.layer.masksToBounds     = true
        self.JbaBtn.layer.cornerRadius          = self.JbaBtn.frame.size.height/5
        self.JbaBtn.layer.masksToBounds         = true
        cattf = "" + "JBA"
        JbaBtn.backgroundColor = CustomColor.ShareColor.hexStringToUIColor(hex: CustomColor.ShareColor.CustomGreen,alpha: 1.0)
        self.selectionview.layer.borderWidth    = 2
        self.selectionview.layer.borderColor    = CustomColor.ShareColor.hexStringToUIColor(hex: CustomColor.ShareColor.CustomGreen,alpha: 1.0).cgColor
        self.containerview.layer.borderWidth = 2
        self.containerview.layer.borderColor = CustomColor.ShareColor.hexStringToUIColor(hex: CustomColor.ShareColor.CustomGreen,alpha: 1.0).cgColor
        
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
    }
    @IBAction func JbaBtn(_ sender: AnyObject) {
        //print("Jba Button pressed ","" + "JBA")
        cattf  = "" + "JBA"
        JbaBtn.backgroundColor = CustomColor.ShareColor.hexStringToUIColor(hex: CustomColor.ShareColor.CustomGreen,alpha: 1.0)
        WhatsupBtn.backgroundColor = UIColor.white
    }
    
    @IBAction func WhatsupBtn(_ sender: AnyObject) {
        //print("Whatsup Button pressed ","WHATSUP")
        Jbabutclor = 0
        whabutclor = 1
        cattf = "WHATSUP"
        WhatsupBtn.backgroundColor = CustomColor.ShareColor.hexStringToUIColor(hex: CustomColor.ShareColor.CustomGreen,alpha: 1.0)
        JbaBtn.backgroundColor = UIColor.white
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func customnavigation()
        
    {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.size.height/10.3);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Ticketbtn.setImage(UIImage(named:"tickethistroy.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(Helpdisk1ViewController.backbuttonClicked),for:UIControlEvents.touchUpInside)
        customView.Ticketbtn.addTarget(self, action: #selector(Helpdisk1ViewController.Ticket),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        customView.HeaderLabel.text = "Help Desk"
        self.view.addSubview(customView.view)
        
    }
    
    
    func backbuttonClicked(sender:UIButton)
    {
        
        
        if (self.navigationController?.topViewController!.isKind(of: DashBoardViewController.classForCoder()) != nil) {
            
            self.navigationController?.popToViewController((self.navigationController?.viewControllers.first)!, animated: true)
        }
        else
        {
            let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
            self.navigationController?.pushViewController(lvc, animated: false)
            
        }
    }
    
    @IBAction func Ticket(_ sender: AnyObject)
    {
        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "TicketissueViewController") as! TicketissueViewController
        self.navigationController?.pushViewController(lvc, animated: true)
        
    }
    
    
    func keyboardWillShow(notification: NSNotification)
    {
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true) {
        } else {
            if(Movedup==false)
            {
                self.containerview.frame.origin.y -= self.view.frame.size.height / 5.13
                Movedup=true
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true) {
        }
        else
        {
            self.containerview.frame.origin.y = self.view.frame.size.height / 9.5
            Movedup=false
        }
    }
    
    
    func addDoneButtonOnKeyboard()
        
    {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        
        
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(VisitLogViewController.doneButtonAction))
        
        
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        doneToolbar.items = items as AnyObject as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        phonenumbertf.inputAccessoryView = doneToolbar
        
    }
    func doneButtonAction()
    {
        phonenumbertf.resignFirstResponder()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == "\n"{
            textView.resignFirstResponder()
            return false
        }
        
        let searchTerm = textView.text
        let characterset = NSCharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        // if searchTerm.rangeOfCharacterFromSet(characterset.invertedSet) != nil {
        
        //  }
        let currentCharacterCount = textView.text?.count ?? 0
        if range.length + range.location > currentCharacterCount {
            return false
        }
        let newLength = currentCharacterCount + text.count - range.length
        
        let allowedCharacters = " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,"
        
        let set = NSCharacterSet(charactersIn: allowedCharacters);
        let inverted = set.inverted;
        
        let filtered = text
            .components(separatedBy: inverted)
            .joined(separator: "");
        return (filtered == text)&&(newLength <= 150)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField.tag==11)
        {
            guard let text = textField.text else { return true }
            
            let newLength = text.count + string.count - range.length
            return newLength <= 10 // Bool
        }
        
        return true
    }
    
    
    @IBAction func camerabtn(_ sender: AnyObject)
    {
        let alert = UIAlertController(title: "Alert", message:"What Would you like to do", preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.addAction(UIAlertAction(title: "TakePhoto", style: UIAlertActionStyle.destructive, handler: { (ACTION :UIAlertAction!)in
            DispatchQueue.main.async {
                self.picker.delegate = self
                self.picker.sourceType = UIImagePickerControllerSourceType.camera;
                self.picker.allowsEditing = false
                self.present(self.picker, animated: true, completion: nil)
            }
        }))
        
        
        alert.addAction(UIAlertAction(title: "Choose from Gallery", style: UIAlertActionStyle.destructive, handler: { (ACTION :UIAlertAction!)in
            self.picker.sourceType = .photoLibrary
            self.OpenImageGallery()
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (ACTION :UIAlertAction!)in
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    func OpenImageGallery()
    {
        picker.delegate=self;
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        present(picker, animated: true, completion: nil)
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        
        let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.uploadimage(pickedImage: pickedImage!)
        imageview.image = pickedImage
        imageview.contentMode = .scaleToFill
        self.dismiss(animated: true, completion: nil);
    }
    func uploadimage(pickedImage:UIImage)
    {
        let image_data = UIImageJPEGRepresentation(pickedImage,0.6)
        // let picimage = image_data!.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
        let picimage = image_data!.base64EncodedString(options: .lineLength64Characters)
        let picimagetype = ".jpg"
        uploadimg = picimage
        uploadimgtype = picimagetype
        let imageSize: Int = image_data!.count
        
    }
    
    
    
    @IBAction func SubmitAction(_ sender: AnyObject)
    {
        if((self.phonenumbertf.text!.isEmpty || self.descriptiontv.text!.isEmpty))
        {
            self.NormalAlert(title: "Alert", Messsage: "Please Enter the all fields")
            
        }
        else
        {
            if((self.descriptiontv.text?.count)!<=5)
            {
                self.NormalAlert(title: "Alert", Messsage: "Please Enter a Valid Description ")
                
            }
            else
            {
                
                if((self.phonenumbertf.text?.count)!<10)
                {
                    self.NormalAlert(title: "Alert", Messsage: "Phone No is Too Short ")
                }
                else
                {
                    
                    if(self.cattf == "")
                    {
                        self.NormalAlert(title: "Alert", Messsage: "Please Select the category option " )
                        
                    }
                        
                    else
                    {
                        
                        
                        if uploadimg != nil
                        {
                            if uploadimgtype != nil
                            {
                                uploadfile.setObject(uploadimg, forKey: "Image" as NSCopying)
                                uploadfile.setObject(uploadimgtype, forKey: "Imagetype" as NSCopying)
                            }
                        }
                        else
                        {
                            
                            uploadfile.setObject("", forKey: "Image" as NSCopying)
                            uploadfile.setObject("", forKey: "Imagetype" as NSCopying)
                        }
                        
                        UserDefaults.standard.set(self.phonenumbertf.text!, forKey: "Helpdesk_PhoneNumber")
                        UserDefaults.standard.synchronize()
                        uploadfile.setObject(descriptiontv.text!, forKey: "ticketDesc" as NSCopying)
                        uploadfile.setObject(self.cattf, forKey: "tickettype" as NSCopying)
                        uploadfile.setObject(phonenumbertf.text!, forKey: "MobileNo" as NSCopying)
                        uploadfile.setObject(LibraryAPI.sharedInstance.Userid, forKey: "UserId" as NSCopying)
                        uploadfile.setObject(LibraryAPI.sharedInstance.Ticketnumber, forKey: "TicketNo" as NSCopying)
                        let modelName = UIDevice.current.modelName
                        var systemVersion = UIDevice.current.systemVersion
                        let size = CGSize(width: 30, height:30)
                        
                        self.local_activityIndicator_start()
                        
                        // new version dispatch Que
                        
                        DispatchQueue.global(qos: .background).async {
                            
                            // Validate user input
                            
                            self.jsonresult = CallFunction.shared.PostMethod(UrlString: "\(LibraryAPI.sharedInstance.GlobalUrl)Ticket.asmx/TicketSubmission", PostData: self.uploadfile)
                            print("result of help desk",self.jsonresult)
                            // Go back to the main thread to update the UI
                            DispatchQueue.main.async {
                                self.local_activityIndicator_stop()
                                if(self.jsonresult.object(forKey: "Status") as? String  == "Success")
                                {
                                    LibraryAPI.sharedInstance.Ticketnumber=(self.jsonresult.object(forKey: "Ticket Number") as? String)!
                                    self.cattf = ""
                                    self.descriptiontv.text = ""
                                    self.phonenumbertf.text = ""
                                    self.uploadimg = nil
                                    self.uploadimgtype = nil
                                    self.imageview.image = UIImage(named: "PH.png")
                                    self.NormalAlert(title: "Alert", Messsage: "Your Ticket is Placed SuccessFully Your Ticket Number is\(LibraryAPI.sharedInstance.Ticketnumber)")
                                    
                                }
                                else
                                {
                                    self.NormalAlert(title: "Alert", Messsage: "Submisson Failed! Retry")
                                    
                                }
                            }
                        }
                        
                    }
                    
                }
            }
            
        }
        
        
    }
    
    
    @IBAction func ticketissue(_ sender: AnyObject)
    {
        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "TicketissueViewController") as! TicketissueViewController
        self.navigationController?.pushViewController(lvc, animated: true)
        
    }
    
    
    @IBAction func catbtn(_ sender: AnyObject)
    {
        let alert:UIAlertController=UIAlertController(title: "Choose option", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let jbaaction = UIAlertAction(title: "JBA", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.cattf =  "" + "JBA"
            
        }
        let whatsup = UIAlertAction(title: "WHATSUP", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.cattf = "WHATSUP"
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
            
        }
        
        picker.delegate = self
        alert.addAction(jbaaction)
        alert.addAction(whatsup)
        alert.addAction(cancelAction)
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
    
    func notificationmethod()
    {
        let userdefaults = Foundation.UserDefaults.standard
        let value = userdefaults.dictionary(forKey: "Notification")
        
        var title: String!
        var message: String!
        var imagestr: String!
        
        title = value?["title"] as! String
        message = value?["body"] as! String
        imagestr = value?["imageurl"] as! String
        
        if imagestr != nil
        {
            if imagestr != ""
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: notificationimageViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = notificationimageViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                    
                }
                popup.show(childViewController: container)
            }
            else
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: PushnotificationtextViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = PushnotificationtextViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                }
                
                popup.show(childViewController: container)
            }
        }
        else
        {
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: PushnotificationtextViewController.instance())
                
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
            }
            let container = PushnotificationtextViewController.instance()
            container.closeHandler = { _ in
                popup.dismiss()
            }
            
            popup.show(childViewController: container)
        }
        
        
    }
    
}
public extension UIDevice {
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
        case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
        case "iPhone10,3", "iPhone10,6":                return "iPhone X"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad6,11", "iPad6,12":                    return "iPad 5"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4":                      return "iPad Pro 9.7 Inch"
        case "iPad6,7", "iPad6,8":                      return "iPad Pro 12.9 Inch"
        case "iPad7,1", "iPad7,2":                      return "iPad Pro 12.9 Inch 2. Generation"
        case "iPad7,3", "iPad7,4":                      return "iPad Pro 10.5 Inch"
        case "AppleTV5,3":                              return "Apple TV"
        case "AppleTV6,2":                              return "Apple TV 4K"
        case "AudioAccessory1,1":                       return "HomePod"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
}

