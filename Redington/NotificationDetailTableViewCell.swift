//
//  NotificationDetailTableViewCell.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 25/04/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class NotificationDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var company: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var invoiceno: UILabel!

      
    @IBOutlet weak var cellview: UIView!
    

      override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
