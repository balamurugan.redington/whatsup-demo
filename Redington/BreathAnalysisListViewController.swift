//
//  BreathAnalysisListViewController.swift
//  Whatsup
//
//  Created by truetech on 24/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
import KCFloatingActionButton

class BreathAnalysisListViewController: UIViewController,KCFloatingActionButtonDelegate,UITableViewDataSource,UITableViewDelegate{
    @IBOutlet var Backbtn: UIButton!
    @IBOutlet var Bizleadingconstant: NSLayoutConstraint!
    @IBOutlet var Previousconstant: NSLayoutConstraint!
    @IBOutlet var statusleadingconstant: NSLayoutConstraint!
    @IBOutlet var Currentconstant: NSLayoutConstraint!
    @IBOutlet var BreadthTableview: UITableView!
    var responsestring1=NSMutableArray()
    @IBOutlet var Notificationbtn: UIButton!
    var fab=KCFloatingActionButton()
    var biz=String()
 
    @IBOutlet weak var Nodataview: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.layoutFAB()
        customnavigation()
        self.iPhoneScreenSizes()
        self.FetchBreadthvalues()
    }
  
    func customnavigation()
    {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(BreathAnalysisListViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Breadth Analysis"
        self.Nodataview.isHidden = true
    }
    func FetchBreadthvalues() {
        DispatchQueue.global(qos: .background).async {
            self.Loaddata()
            DispatchQueue.main.async {
                self.BreadthTableview.reloadData()
            }
        }
    }
    
    
    func iPhoneScreenSizes(){
        let bounds = UIScreen.main.bounds
        let height = bounds.size.height
        switch height {
        case 480.0: break
        case 568.0: break
        case 667.0: break
        case 736.0: break
        default: break
        }
    }

    func buttonClicked(sender:UIButton)
    {
        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
        self.navigationController?.pushViewController(lvc, animated: true)
    }
   
    
    override func viewWillAppear(_ animated: Bool) {
        self.FetchBreadthvalues()
        self.BreadthTableview.reloadData()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func Loaddata()
    {
        self.local_activityIndicator_start()
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/BreathDetails?UserID=\(LibraryAPI.sharedInstance.Userid)&Branch=\(LibraryAPI.sharedInstance.BranchCodeNew)&Biz=", completion: { (jsonmutable) in
             LibraryAPI.sharedInstance.breadthvalues = jsonmutable
            if(LibraryAPI.sharedInstance.breadthvalues.count > 0) {
                DispatchQueue.main.async {
                    self.BreadthTableview.isHidden=false
                    self.BreadthTableview.reloadData()
                    self.local_activityIndicator_stop()
                }
            } else {
                DispatchQueue.main.async {
                    self.BreadthTableview.isHidden=true
                    self.Nodataview.isHidden = false
                    self.BreadthTableview.reloadData()
                    self.local_activityIndicator_stop()
                }
            }
        })
        
    }
    
   
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            self.fab.close()
        }
        fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
            self.FetchBreadthvalues()
            self.fab.close()
        }
          fab.fabDelegate = self
        self.view.addSubview(fab)
        
    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
       
    }

    
    //TableView Delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if LibraryAPI.sharedInstance.breadthvalues.count > 0
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(LibraryAPI.sharedInstance.breadthvalues.count == 1)
        {
            let dic = LibraryAPI.sharedInstance.breadthvalues[0] as! NSDictionary
            if(dic.object(forKey: "Message") != nil)
            {
                let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.BreadthTableview.bounds.size.width, height: self.BreadthTableview.bounds.size.height))
                
                 label.text = "No data available"
                label.textAlignment = NSTextAlignment.center
                label.sizeToFit()
                label.textColor = UIColor.red
                label.font = UIFont(name: (label.font.fontName), size: 20)
                self.BreadthTableview.backgroundView = label
                return 0
            }
            else
            {
                return LibraryAPI.sharedInstance.breadthvalues.count
            }
        }
        else
        {
            return LibraryAPI.sharedInstance.breadthvalues.count
        }

        
        
       // return LibraryAPI.sharedInstance.breadthvalues.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell : BreadthanalysisTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as! BreadthanalysisTableViewCell
        
        if(LibraryAPI.sharedInstance.breadthvalues.count > 0)
        {
            let dic = LibraryAPI.sharedInstance.breadthvalues[indexPath.row] as! NSDictionary
            if(cell == nil)
            {
                cell = Bundle.main.loadNibNamed("Cell", owner: self, options: nil)![0] as! BreadthanalysisTableViewCell;
                
            }
            if(indexPath.row%2==0)
            {
                cell.contentView.backgroundColor=(UIColor.init(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0))
            }
            else
            {
                //cell.contentView.backgroundColor=UIColor.whiteColor()
                cell.contentView.backgroundColor=(UIColor.init(red: 255/255.0, green: 204/255.0, blue: 212/255.0, alpha: 1.0))
            }
            cell.Grp.text=dic.object(forKey: "SBU CODE") as? String
            cell.Biz.text=dic.object(forKey: "Business Code") as? String
            cell.Preview.text=String(dic.object(forKey: "Previous Month") as! Int)
            cell.Current.text=String(dic.object(forKey: "Current Month") as! Int)
            if(dic.object(forKey: "Flag") as! String == "D")
            {
                cell.LikeImage.image=UIImage(named:"unlike.png")
            }
            else  if(dic.object(forKey: "Flag") as! String == "U")
            {
                cell.LikeImage.image=UIImage(named:"like1.png")
            }
            else
            {
                cell.LikeImage.image=UIImage(named:"no_like.png")
            }
        }
        else
        {
            
        }
        
        
        cell.selectionStyle = .none
        return cell as BreadthanalysisTableViewCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        LibraryAPI.sharedInstance.tracker="2"
        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "BreadthanalysisViewController") as! BreadthanalysisViewController
        let dic = LibraryAPI.sharedInstance.breadthvalues[indexPath.row] as! NSDictionary
        lvc.Bizcode = (dic.object(forKey: "Business Code") as? String)!
        self.navigationController?.pushViewController(lvc, animated: true)
        
    }
    
 
   
}
