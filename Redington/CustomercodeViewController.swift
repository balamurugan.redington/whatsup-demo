//
//  CheckbendingViewController.swift
//  Redington
//
//  Created by RIL-ITTECH on 21/02/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class CustomercodeViewController:UIViewController,PopupContentViewController,UITableViewDelegate,UITableViewDataSource {
    
    var closeHandler: (() -> Void)?
    var titlearray=[String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    
    class func instance() -> CustomercodeViewController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "CustomercodeViewController") as! CustomercodeViewController
    }
    
    @IBAction func CloseAction(_ sender: AnyObject)
    {
        closeHandler?()
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func sizeForPopup(popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: 200, height: 250)
    }
    
    //TableView Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
      
        if (LibraryAPI.sharedInstance.CheckBendingCustomerArray.count > 0)
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return  LibraryAPI.sharedInstance.CheckBendingCustomerArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        let cell : CustomercodeTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as! CustomercodeTableViewCell
        
        cell.customercode.text =  LibraryAPI.sharedInstance.CheckBendingCustomerArray.object(at: indexPath.row) as? String
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        LibraryAPI.sharedInstance.CheckBending_CustomerCode = LibraryAPI.sharedInstance.CheckBendingCustomerArray.object(at: indexPath.row) as? String
        let slideInFromLeftTransition = CATransition()
        slideInFromLeftTransition.type = kCAGravityTop
        slideInFromLeftTransition.subtype = kCATransitionFromLeft
        slideInFromLeftTransition.duration = 0.2
        slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "ChequeBounceDetailsViewController") as! ChequeBounceDetailsViewController
        lvc.Check = 1
        self.navigationController?.view.layer.add(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
        closeHandler?()
        self.navigationController?.pushViewController(lvc, animated: false)
    }
    
    
    @IBAction func AllDataAction(_ sender: AnyObject)
    {
        let slideInFromLeftTransition = CATransition()
        slideInFromLeftTransition.type = kCAGravityTop
        slideInFromLeftTransition.subtype = kCATransitionFromLeft
        slideInFromLeftTransition.duration = 0.2
        slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "ChequeBounceDetailsViewController") as! ChequeBounceDetailsViewController
        self.navigationController?.view.layer.add(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
        closeHandler?()
        self.navigationController?.pushViewController(lvc, animated: false)
    }
    
}
