//
//  StockAllLocationTableViewCell.swift
//  Redington
//
//  Created by TRUETECHMAC3 on 28/12/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit

class StockAllLocationTableViewCell: UITableViewCell {

    
    @IBOutlet var labelSRCode: UILabel!
    @IBOutlet var labelSRDesc: UILabel!
    @IBOutlet var labelSRUnits: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
