//
//  NotificationDeatilsViewController.swift
//  
//
//  Created by RIL-ITTECH on 26/04/17.
//
//

import UIKit

class NotificationDeatilsViewController: UIViewController,PopupContentViewController
{

    @IBOutlet weak var invoicenolbl: UILabel!
    
    @IBOutlet weak var invoicedatelbl: UILabel!
    
    @IBOutlet weak var expecteddate: UILabel!
    
    @IBOutlet weak var expectedvalue: UILabel!
    
    @IBOutlet weak var remarkslabl: UILabel!
    
    var closeHandler: (() -> Void)?
    
    var responsestring = NSMutableArray()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.Loaddata()


        // Do a ny additional setup after loading the view.
    }
    
    class func instance() -> NotificationDeatilsViewController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "NotificationDeatilsViewController") as! NotificationDeatilsViewController
    }
    
    
    @IBAction func CloseAction(_ sender: AnyObject)
    {
        
        closeHandler?()
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    func sizeForPopup(popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: 250, height: 300)
    }
    
    func Loaddata()
    {
       
      
        
       // let responsestring1 = LibraryAPI.sharedInstance.RequestUrl(url: "\(LibraryAPI.sharedInstance.GlobalUrl)OrderStatus.asmx/Overduecollectionnotification?userId=\(LibraryAPI.sharedInstance.Userid)")
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)OrderStatus.asmx/Overduecollectionnotification?userId=\(LibraryAPI.sharedInstance.Userid)", completion: { (jsonmutable) in
            let responsestring1 = jsonmutable
            if (responsestring1.count > 0)
            {
                
                for i in 0..<responsestring1.count
                {
                    let dic = responsestring1[i] as! NSDictionary
                    self.invoicenolbl.text = dic.object(forKey: "INVOICE NUMBER") as? String
                    self.invoicedatelbl.text  = dic.object(forKey: "INVOICE DATE") as? String
                    self.expecteddate.text = dic.object(forKey: "EXPECT DT OF COLLECT") as? String
                    self.expectedvalue.text  = String(describing: dic.object(forKey: "EXPECTED VALUE") as! NSNumber)
                    self.remarkslabl.text  = dic.object(forKey: "REMARKS") as? String
                }
                
                
            }
            else
            {
                
            }
        })
       
    }
    

    



}
