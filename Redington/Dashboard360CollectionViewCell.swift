//
//  Dashboard360CollectionViewCell.swift
//  Whatsup
//
//  Created by truetech on 22/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit

class Dashboard360CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var labellogi: UIImageView!
    @IBOutlet var label: UILabel!
    @IBOutlet var logoimg: UIImageView!
}
