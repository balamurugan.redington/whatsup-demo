//
//  BarChartViewController.swift
//  Redington
//
//  Created by truetech on 18/08/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
import Charts
//import NVActivityIndicatorView
import KCFloatingActionButton

class BarChartViewController: UIViewController,ChartViewDelegate,KCFloatingActionButtonDelegate{

    @IBOutlet var ChartView: BarChartView!
    var months=[String]()
    
    @IBOutlet weak var PageControl: UIPageControl!
    @IBOutlet var notificationbtn: UIButton!
    @IBOutlet var menubtn: UIButton!
    var   Array30 = [Double]()
    var   array60 = [Double]()
    var   array90 = [Double]()
    var   array120 = [Double]()
    var   arraymorethan120=[Double]()
    var monthsbar=[String]()
    var MontnDictionary = [String: AnyObject]()
    var montharray=NSMutableArray()
    var fab=KCFloatingActionButton()
    var responsestring2 = NSMutableArray()
    var count = NSInteger()
    var timer = Timer()
    
    @IBOutlet weak var PopUpView: UIView!
    
        override func viewDidLoad() {
        super.viewDidLoad()
      
        ChartView.delegate = self;
        ChartView.descriptionText = "";
        ChartView.noDataText = "You need to provide data for the chart.";
        ChartView.pinchZoomEnabled = false;
        ChartView.drawBarShadowEnabled = false;
        ChartView.drawGridBackgroundEnabled = false;
      //  ChartView.leftAxis.startAtZeroEnabled=true
             ChartView.leftAxis.drawZeroLineEnabled=true
        ChartView.setScaleEnabled(true)
        ChartView.backgroundColor = (UIColor.init(red: 30/255.0, green: 184/255.0, blue: 120/255.0, alpha: 1.0))

        self.layoutFAB()
        self.customnavigation()
        
        
        
            let xAxis:XAxis = ChartView.xAxis
        xAxis.getLongestLabel()
       
        xAxis.avoidFirstLastClippingEnabled=true
       
       // xAxis.axisLabelModulus=1
        xAxis.labelWidth = 1
        
        let legend = ChartView.legend;
            legend.position = .belowChartRight;
        legend.enabled=false
        _ = ChartView.xAxis;
        let leftAxis = ChartView.leftAxis;
       
            leftAxis.valueFormatter = NumberFormatter.init() as! IAxisValueFormatter;
      //  leftAxis.valueFormatter!.maximumFractionDigits = 1;
        
        leftAxis.drawGridLinesEnabled = true;
        leftAxis.spaceTop = 0.25;
        ChartView.rightAxis.enabled = false;
       // ChartView.viewPortHandler.setMinimumScaleX(60.0)
        ChartView.viewPortHandler.setMaximumScaleY(60.0)
   
      // ChartView.fitScreen()
            ChartView.zoom(scaleX: 6.0, scaleY: 0.0, x: 0.0, y: 0.0)
    
        
        self.fetchvalues()
      //  timer = NSTimer.scheduledTimerWithTimeInterval(3.0, target: self, selector: #selector(BarChartViewController.setCurrentPageLabel), userInfo: nil, repeats: false)

      
      //ChartView.zoom(2.0, scaleY: 0.0, x: 0.0, y: 0.0)
        // Do any additional setup after loading the view.     
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        appdelegate.shouldSupportAllOrientation = true
    }
    
    func fetchvalues()
    {
    
      
        local_activityIndicator_start()
        // new version dispatch Que
        
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
            self.parseurl()
            
            if(self.Array30.count>0)
            {
                DispatchQueue.main.async {

                    self.ChartView.isHidden=false
                    self.PopUpView.isHidden=true
                }
            }
            else
            {
                DispatchQueue.main.async {
                    self.ChartView.isHidden=true
                    let alertController = UIAlertController(title: "Alert", message: "No Data Available" , preferredStyle: .alert)
                    
                    let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                        
                        let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                        self.navigationController?.pushViewController(lvc, animated: false)
                    }
                    alertController.addAction(cancelAction)
                    
                    
                    self.present(alertController, animated: true, completion:nil)
                    
                }
            }
            
            

            
            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
                
                self.local_activityIndicator_stop()
                
                self.setChartBarGroupDataSet(dataPoints: self.monthsbar, values: self.Array30, values2: self.array60,values3: self.array90,values4: self.array120,values5: self.arraymorethan120,sortIndex: 1)
            }
        }
        
        // new version dispatch Que
           }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
 
       func parseurl()
    {
        if(Array30.count>0)
        {
            monthsbar.removeAll()
            array60.removeAll()
            Array30.removeAll()
            array90.removeAll()
            array120.removeAll()
            arraymorethan120.removeAll()
        }
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)CreditDetails.asmx/CreditAgeingDetails?UserID=\(LibraryAPI.sharedInstance.Userid)&BranchCode=\(LibraryAPI.sharedInstance.BranchCodeNew)", completion: { (jsonmutable) in
          self.responsestring2 =  jsonmutable

            if( self.responsestring2.count>0) {
                for i in 0..<self.responsestring2.count {
                    let dic =  self.responsestring2[i] as! NSDictionary
                    if let val=dic.object(forKey: "0to30Ageing")
                    {
                         self.Array30.append(((val as AnyObject) as? Double)!)
                    }
                    
                    if let val=dic.object(forKey: "31to60Ageing")
                    {
                         self.array60.append(((val as AnyObject) as? Double)!)
                    }
                    if let val=dic.object(forKey: "61to90Ageing")
                    {
                         self.array90.append(((val as AnyObject) as? Double)!)
                    }
                    
                    if let val=dic.object(forKey: "91to120Ageing")
                    {
                         self.array120.append(((val as AnyObject) as? Double)!)
                    }
                    
                    if let val=dic.object(forKey: "Greaterthan120Ageing")
                    {
                         self.arraymorethan120.append(((val as AnyObject) as? Double)!)
                    }
                     self.monthsbar.append(dic.object(forKey: "MonthYear") as! String)
                }
            }
            
           })


    }
    
    func setCurrentPageLabel(){
        
        PageControl.isHidden=false;
        if(count<=responsestring2.count)
        {
            view.addSubview(ChartView)
            PageControl.currentPage=count;
            
        }
        count=count+1
        if(count>=responsestring2.count)
        {
            count=0;
            PageControl.currentPage=count+3;
        }
    }
    
    

    
    func KCFABOpened(fab: KCFloatingActionButton) {
     
    }
    
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            if (self.navigationController?.topViewController!.isKind(of: DashBoardViewController.classForCoder()) != nil) {
                self.navigationController?.popToViewController((self.navigationController?.viewControllers.first)!, animated: true)
            }
            else
            {
                let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                self.navigationController?.pushViewController(lvc, animated: false)
            }
            self.fab.close()
        }
        
        fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
            self.fetchvalues()
            self.fab.close()
        }
        fab.fabDelegate = self
        self.view.addSubview(fab)
        
    }
    

    func customnavigation()
    {
        notificationbtn.addTarget(self, action: #selector(BarChartViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        menubtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
    }
    
    func buttonClicked(sender:UIButton)
    {
         LibraryAPI.sharedInstance.tracker="1"
        if (self.navigationController?.topViewController!.isKind(of: DashBoardViewController) != nil) {
            self.navigationController?.popToViewController((self.navigationController?.viewControllers.first)!, animated: true)
        }
        else
        {
            let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
            self.navigationController?.pushViewController(lvc, animated: false)
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setChartBarGroupDataSet(dataPoints: [String], values: [Double], values2: [Double],values3:[Double],values4:[Double],values5:[Double],sortIndex:Int) {
        
        var dataEntries: [BarChartDataEntry] = []
        var dataEntries2: [BarChartDataEntry] = []
        var dataEntries3: [BarChartDataEntry] = []
        var dataEntries4: [BarChartDataEntry] = []
        var dataEntries5: [BarChartDataEntry] = []
        var de: [BarChartDataEntry] = []
        var colors: [UIColor] = []
        var colors1: [UIColor] = []
        var colors2: [UIColor] = []
        var colors3: [UIColor] = []
        var colors4: [UIColor] = []
        
        for i in 0..<dataPoints.count {
            let dataEntry = BarChartDataEntry(x: Double(i), y: values[0])
            dataEntries.append(dataEntry)
        }
        
        
        for i in 0..<dataPoints.count {
            let dataEntry = BarChartDataEntry(x: Double(i), y: values2[1])
            dataEntries2.append(dataEntry)
        }
        
        for i in 0..<dataPoints.count {
             let dataEntry = BarChartDataEntry(x: Double(i), y: values3[2])
            dataEntries3.append(dataEntry)
        }
        
        
        for i in 0..<dataPoints.count {
            let dataEntry = BarChartDataEntry(x: Double(i), y: values4[3])
            dataEntries4.append(dataEntry)
        }

        for i in 0..<dataPoints.count {
             let dataEntry = BarChartDataEntry(x: Double(i), y: values5[4])
            dataEntries5.append(dataEntry)
        }
      
        
        for i in 0..<dataPoints.count
        {
           // let dr=BarChartDataEntry(values:[values[i],values2[i],values3[i],values4[i],values5[i]],xIndex:i)
            let dr=BarChartDataEntry(x: Double(i), yValues: [values[i],values2[i],values3[i],values4[i],values5[i]])
            de.append(dr)
            
        }
    
        _ = BarChartDataSet(values: de, label: "Units Sold")

        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: " ")
        let chartDataSet2 = BarChartDataSet(values: dataEntries2, label: " ")
        let chartDataSet3 = BarChartDataSet(values: dataEntries3, label: " ")
        let chartDataSet4 = BarChartDataSet(values: dataEntries4, label: " ")
        let chartDataSet5 = BarChartDataSet(values: dataEntries5, label: " ")
        ChartView.leftAxis.axisMinValue=2.0
        ChartView.xAxis.labelPosition = .bottom
    
        for _ in 0..<dataPoints.count {
            
            
            colors.append(UIColor.init(red: 181/255.0, green: 255/255.0, blue: 122/255.0, alpha: 1.0))
        }
        
        for _ in 0..<dataPoints.count {
            
            
            colors1.append(UIColor.init(red: 247/255.0, green: 240/255.0, blue: 122/255.0, alpha: 1.0))
        }
        for _ in 0..<dataPoints.count {
            
            
            colors2.append(UIColor.init(red: 254/255.0, green: 198/255.0, blue: 122/255.0, alpha: 1.0))
        }
        
        for _ in 0..<dataPoints.count {
            
            
            colors3.append(UIColor.init(red: 120/255.0, green: 230/255.0, blue: 255/255.0, alpha: 1.0))
        }
        for _ in 0..<dataPoints.count {
            
            
            colors4.append(UIColor.init(red: 252/255.0, green: 111/255.0, blue: 135/255.0, alpha: 1.0))
        }
        
        
        chartDataSet.colors = colors
        chartDataSet2.colors = colors1
        chartDataSet3.colors = colors2
        chartDataSet4.colors = colors3
        chartDataSet5.colors = colors4

        
        let dataSets:[BarChartDataSet]=[chartDataSet,chartDataSet2,chartDataSet3,chartDataSet4,chartDataSet5]
       // let data = BarChartData(xVals: dataPoints, dataSets: dataSets)
        let data = BarChartData(dataSet: dataSets as! IChartDataSet)
        ChartView.data = data
        ChartView.xAxis.labelPosition = .bottom
        ChartView.rightAxis.enabled=false
        ChartView.xAxis.drawAxisLineEnabled = true
        
        ChartView.descriptionText = ""
        ChartView.rightAxis.drawGridLinesEnabled = true
        
        ChartView.data = data
        
        ChartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .easeInBounce)
    
              
    }
    

    func chartValueSelected(chartView: ChartViewBase, entry: ChartDataEntry, dataSetIndex: Int, highlight: Highlight)
  {
    let lvc=self.storyboard?.instantiateViewController(withIdentifier: "InvoicesummerydetailsViewController") as! InvoicesummerydetailsViewController
    self.navigationController?.pushViewController(lvc, animated: true)
    
  }
    
    

}

