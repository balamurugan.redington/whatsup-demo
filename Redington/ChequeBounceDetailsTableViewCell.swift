//
//  ChequeBounceDetailsTableViewCell.swift
//  Redington
//
//  Created by TRUETECHMAC3 on 26/12/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit

class ChequeBounceDetailsTableViewCell: UITableViewCell {

    @IBOutlet var Biz: UILabel!
    @IBOutlet var InvNo: UILabel!
    @IBOutlet var InvVal: UILabel!
    @IBOutlet var InvDt: UILabel!
    @IBOutlet var OdDays: UILabel!
    
    @IBOutlet weak var pendingdays: UILabel!
    @IBOutlet weak var duedate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
