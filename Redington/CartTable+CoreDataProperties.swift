//
//  CartTable+CoreDataProperties.swift
//  
//
//  Created by mac on 11/1/16.
//
//

import Foundation
import CoreData


extension CartTable {

    @nonobjc public override class func fetchRequest() -> NSFetchRequest<NSFetchRequestResult> {
        return NSFetchRequest(entityName: "CartTable");
    }

    @NSManaged public var itemcode: String?
    @NSManaged public var price: Double
    @NSManaged public var products: String?
    @NSManaged public var quantity: String?
    @NSManaged public var vendor: String?

}
