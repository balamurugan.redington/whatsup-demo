//
//  SalesAnalysisCollectionViewCell.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 04/01/18.
//  Copyright © 2018 truetech. All rights reserved.
//

import UIKit
import Charts
//import Funnel

class SalesAnalysisCollectionViewCell: UICollectionViewCell,ChartViewDelegate {

    @IBOutlet weak var chartview: BarChartView!
    
    @IBOutlet weak var titlelbl: UILabel!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        chartview.delegate = self;
        chartview.descriptionText = "";
        chartview.noDataText = "You need to provide data for the chart.";
        chartview.pinchZoomEnabled = false;
        chartview.drawBarShadowEnabled = false;
        chartview.drawGridBackgroundEnabled = false;
        //chartview.leftAxis.startAtZeroEnabled=true
        chartview.leftAxis.drawZeroLineEnabled = true
        chartview.setScaleEnabled(true)
        
        
        let xAxis:XAxis = chartview.xAxis
        xAxis.getLongestLabel()
        
        xAxis.avoidFirstLastClippingEnabled=true
        xAxis.axisLineWidth=1
        let legend = chartview.legend;
        legend.position = .belowChartRight;
        legend.enabled=false
        _ = chartview.xAxis;
        let leftAxis = chartview.leftAxis;
        leftAxis.drawGridLinesEnabled = true;
        leftAxis.spaceTop = 0.25;
        chartview.rightAxis.enabled = false;
        chartview.viewPortHandler.setMaximumScaleY(60.0)
        chartview.zoom(scaleX: 1.0, scaleY: 0.0, x: 0.0, y: 0.0)
        
        
    }
    
    
    func setChartBarGroupDataSet(dataPoints: [String], values: [Double], values2: [Double],values3:[Double],values4:[Double],values5:[Double],sortIndex:Int) {
        
        var dataEntries: [BarChartDataEntry] = []
        var dataEntries2: [BarChartDataEntry] = []
        var dataEntries3: [BarChartDataEntry] = []
        var dataEntries4: [BarChartDataEntry] = []
        var dataEntries5: [BarChartDataEntry] = []
        var de: [BarChartDataEntry] = []
        var colors: [UIColor] = []
        var colors1: [UIColor] = []
        var colors2: [UIColor] = []
        var colors3: [UIColor] = []
        var colors4: [UIColor] = []
        
        for i in 0..<dataPoints.count {
            
           // let dataEntry = BarChartDataEntry(value: values[i], xIndex: i)
            let dataEntry = BarChartDataEntry(x: Double(0), y: Double(values[i]))
            
            dataEntries.append(dataEntry)
        }
        
        
        for i in 0..<dataPoints.count {
            
           // let dataEntry = BarChartDataEntry(value: values2[i], xIndex: i)
             let dataEntry = BarChartDataEntry(x: Double(1), y: Double(values2[i]))
            dataEntries2.append(dataEntry)
        }
        
        for i in 0..<dataPoints.count {
            
            //let dataEntry = BarChartDataEntry(value: values3[i], xIndex: i)
            let dataEntry = BarChartDataEntry(x: Double(2), y: Double(values3[i]))
            dataEntries3.append(dataEntry)
        }
        
        
        for i in 0..<dataPoints.count {
            
            //let dataEntry = BarChartDataEntry(value: values4[i], xIndex: i)
            let dataEntry = BarChartDataEntry(x: Double(3), y: Double(values4[i]))
            dataEntries4.append(dataEntry)
        }
        
        for i in 0..<dataPoints.count {
             let dataEntry = BarChartDataEntry(x: Double(4), y: Double(values5[i]))
           // let dataEntry = BarChartDataEntry(value: values5[i], xIndex: i)
            dataEntries5.append(dataEntry)
        }
        
        
        for i in 0..<dataPoints.count
        {
            //let dr = BarChartDataEntry(values:[values[i],values2[i],values3[i],values4[i],values5[i]],xIndex:i)
            let dr = BarChartDataEntry(x: Double(i), yValues: [values[i],values2[i],values3[i],values4[i],values5[i]])
            de.append(dr)
            
        }
        
        _ = BarChartDataSet(values: de, label: "Units Sold")
        let chartDataSet1 = BarChartDataSet(values: de, label: "")
        chartDataSet1.stackLabels=["0-30","30-60","60-90","90-120",">120"]
        
        
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: " ")
        let chartDataSet2 = BarChartDataSet(values: dataEntries2, label: " ")
        let chartDataSet3 = BarChartDataSet(values: dataEntries3, label: " ")
        let chartDataSet4 = BarChartDataSet(values: dataEntries4, label: " ")
        let chartDataSet5 = BarChartDataSet(values: dataEntries5, label: " ")
        chartview.leftAxis.axisMinValue = 2.0
        chartview.xAxis.labelPosition = .bottom
        
        for _ in 0..<dataPoints.count {
            
            
            colors.append(UIColor.init(red: 181/255.0, green: 255/255.0, blue: 122/255.0, alpha: 1.0))
        }
        
        for _ in 0..<dataPoints.count {
            
            
            colors1.append(UIColor.init(red: 247/255.0, green: 240/255.0, blue: 122/255.0, alpha: 1.0))
        }
        for _ in 0..<dataPoints.count {
            
            
            colors2.append(UIColor.init(red: 254/255.0, green: 198/255.0, blue: 122/255.0, alpha: 1.0))
        }
        
        for _ in 0..<dataPoints.count {
            
            
            colors3.append(UIColor.init(red: 120/255.0, green: 230/255.0, blue: 255/255.0, alpha: 1.0))
        }
        for _ in 0..<dataPoints.count {
            
            
            colors4.append(UIColor.init(red: 252/255.0, green: 111/255.0, blue: 135/255.0, alpha: 1.0))
        }
        
        
        chartDataSet.colors = colors
        chartDataSet2.colors = colors1
        chartDataSet3.colors = colors2
        chartDataSet4.colors = colors3
        chartDataSet5.colors = colors4
        
        
        let dataSets:[BarChartDataSet]=[chartDataSet,chartDataSet2,chartDataSet3,chartDataSet4,chartDataSet5]
        let data = BarChartData(dataSets: dataSets)
        chartview.data = data
        chartview.xAxis.labelPosition = .bottom
        chartview.rightAxis.enabled=false
        chartview.xAxis.drawAxisLineEnabled = true
        chartview.rightAxis.drawGridLinesEnabled = true
        chartview.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .easeInBounce)
        
        
    }
   
  /*  func setChartBarGroupDataSet(dataPoints: [String], values: [Double], values2: [Double],values3:[Double],values4:[Double],values5:[Double],sortIndex:Int) {
        
        var dataEntries: [BarChartDataEntry] = []
        var dataEntries2: [BarChartDataEntry] = []
        var dataEntries3: [BarChartDataEntry] = []
        var dataEntries4: [BarChartDataEntry] = []
        var dataEntries5: [BarChartDataEntry] = []
        var de: [BarChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
            
            let dataEntry = BarChartDataEntry(value: values[i], xIndex: i)
            
            dataEntries.append(dataEntry)
        }
        
        
        for i in 0..<dataPoints.count {
            
            let dataEntry = BarChartDataEntry(value: values2[i], xIndex: i)
            dataEntries2.append(dataEntry)
        }
        
        for i in 0..<dataPoints.count {
            
            let dataEntry = BarChartDataEntry(value: values3[i], xIndex: i)
            dataEntries3.append(dataEntry)
        }
        
        
        for i in 0..<dataPoints.count {
            
            let dataEntry = BarChartDataEntry(value: values4[i], xIndex: i)
            dataEntries4.append(dataEntry)
        }
        
        for i in 0..<dataPoints.count {
            
            let dataEntry = BarChartDataEntry(value: values5[i], xIndex: i)
            dataEntries5.append(dataEntry)
        }
        
        
        for i in 0..<dataPoints.count
        {
            let dr=BarChartDataEntry(values:[values[i],values2[i],values3[i],values4[i],values5[i]],xIndex:i)
            de.append(dr)
            
        }
        
        let chartDataSet1 = BarChartDataSet(yVals: de, label: "")
        chartDataSet1.stackLabels=["0-30","30-60","60-90","90-120",">120"]
        
//        let chartDataSet = BarChartDataSet(yVals: dataEntries, label: " ")
//        chartDataSet.barSpace=0.4
//        
//        let chartDataSet2 = BarChartDataSet(yVals: dataEntries2, label: " ")
//        
//        chartDataSet2.barSpace=0.4
        
         chartview.viewPortHandler.setMaximumScaleY(10.0)
        let chartDataSet = BarChartDataSet(yVals: dataEntries, label: " ")
        _ = BarChartDataSet(yVals: dataEntries2, label: " ")
        let chartDataSet3 = BarChartDataSet(yVals: dataEntries3, label: " ")
        let chartDataSet4 = BarChartDataSet(yVals: dataEntries4, label: " ")
        _ = BarChartDataSet(yVals: dataEntries5, label: " ")
        chartDataSet.barSpace=0.4
         chartDataSet3.barSpace=0.4
        chartDataSet4.barSpace=0.4
        
        chartDataSet1.colors =  [UIColor.init(red: 181/255.0, green: 255/255.0, blue: 122/255.0, alpha: 1.0),UIColor.init(red: 255/255.0, green: 247/255.0, blue: 126/255.0, alpha: 1.0),UIColor.init(red: 254/255.0, green: 198/255.0, blue: 122/255.0, alpha: 1.0),UIColor.init(red: 120/255.0, green: 230/255.0, blue: 255/255.0, alpha: 1.0),UIColor.init(red: 252/255.0, green: 113/255.0, blue: 137/255.0, alpha: 1.0)]
        chartDataSet.colors  =   ChartColorTemplates.colorful()
        chartDataSet3.colors =  ChartColorTemplates.colorful()
        chartDataSet4.colors =  ChartColorTemplates.colorful()
        let dataSets:[BarChartDataSet]=[chartDataSet1]
        let data = BarChartData(xVals: dataPoints, dataSets: dataSets)
        chartview.descriptionText = ""
        chartview.rightAxis.drawGridLinesEnabled = false
        
        chartview.data = data
        
        chartview.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .EaseInBounce)
        
        
    }*/

}
