//
//  SeatBooking1ViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 23/03/18.
//  Copyright © 2018 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import KCFloatingActionButton

class SeatBooking1ViewController: UIViewController,KCFloatingActionButtonDelegate,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate
{
    @IBOutlet weak var Seatbookingtableview: UITableView!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var mainview: UIView!
    @IBOutlet weak var BottomView: UIView!
    @IBOutlet weak var Datetf: UITextField!
    @IBOutlet weak var contentview: UIView!
    @IBOutlet weak var checkavailabilitybtn: UIButton!
    @IBOutlet weak var fromtimetf: UITextField!
    @IBOutlet weak var Totimetf: UITextField!
    @IBOutlet weak var ticketgenview: UIView!
    @IBOutlet weak var Submitbtn: UIButton!
    @IBOutlet weak var referencenumlbl: UILabel!
    @IBOutlet weak var datepicker: UIDatePicker!
    @IBOutlet weak var pickerview: UIView!
    @IBOutlet weak var seatnolbl: UILabel!
    @IBOutlet weak var reflbl: UILabel!
    @IBOutlet weak var datelbl: UILabel!
    @IBOutlet weak var fromlbl: UILabel!
    @IBOutlet weak var seatavailabilityview: UIView!
    @IBOutlet weak var tolbl: UILabel!
    
    @IBOutlet weak var nodatalbl: UILabel!
    var uploadfile=NSMutableDictionary()
    var jsonresult=NSDictionary()
    var currenttextfield=String()
    var frmstr = String()
    var responsestring = NSMutableArray()
    var fab=KCFloatingActionButton()
    var Btn_value = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.customnavigation()
        nodatalbl.isHidden = true
        scrollview.contentSize = contentview.frame.size
        Datetf.delegate = self
        Totimetf.delegate = self
        fromtimetf.delegate = self
        Datetf.tag = 1
        fromtimetf.tag = 2
        Totimetf.tag = 3
        scrollview.isScrollEnabled = false
        
        Seatbookingtableview.isHidden = true
        Seatbookingtableview.backgroundColor = UIColor.clear
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        Submitbtn.layer.masksToBounds=true
        Submitbtn.layer.cornerRadius=10.0
        checkavailabilitybtn.layer.masksToBounds=true
        checkavailabilitybtn.layer.cornerRadius=10.0
        self.layoutFAB()
        Seatbookingtableview.delegate = self
        Seatbookingtableview.dataSource = self
        Seatbookingtableview.register(UINib(nibName: "BookinghistroyTableViewCell", bundle: nil), forCellReuseIdentifier: "BookinghistroyTableViewCell")
        
        //TextField calender Symbol
        let arrow = UIImageView()
        let image = UIImage(named: "cal.png")
        arrow.image = image!.imageWithColor(tintColor: UIColor.init(red:239/255.0 , green: 88/255.0, blue: 88/255.0, alpha: 1.0)).withRenderingMode(.alwaysOriginal)
        arrow.frame = CGRect(x: 0.0, y: 0.0, width: arrow.image!.size.width+10.0, height: 15)
        arrow.contentMode = UIViewContentMode.center
        self.Datetf.rightView = arrow;
        self.Datetf.rightViewMode = UITextFieldViewMode.always
       
        
        ticketgenview.isHidden = true
        Btn_value = 1
    }
    override func viewWillAppear(_ animated: Bool) {
         self.Loaddata()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func customnavigation() {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: 65)
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(SeatBooking1ViewController.buttonClicked(_:)),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        self.view.backgroundColor = UIColor.clear
        customView.HeaderLabel.text = "Seat Booking"
    }
    
    func buttonClicked(_ sender: UIButton) {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            
            self.fab.close()
        }
        
        fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
            DispatchQueue.main.async(execute: {
                self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "SeatBooking1ViewController")), animated: true)
            })
            self.fab.close()
        }
        
        fab.fabDelegate = self
        self.view.addSubview(fab)
    }
    
    
    func KCFABOpened(fab: KCFloatingActionButton) {
        
    }
    
    func setdateandtime() {
        let date = NSDate()
        let calendar = NSCalendar.current
        let component = calendar.dateComponents([.hour,.minute,.second], from: date as Date)
        let hour = component.hour
        let minutes = component.minute
        let formattedhour=String(describing: hour)
        let formattedtime=String(describing: minutes)
        let originaltime = formattedhour + ":\(formattedtime)"
        let dateMakerFormatter1 = DateFormatter()
        dateMakerFormatter1.dateFormat = "HH:mm"
        let startTime = dateMakerFormatter1.date(from: originaltime)!
        let strtime = dateMakerFormatter1.string(from: startTime)
        //this comes back as 12:40 am not pm
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "MM-dd-yyyy"
        let strDate = dateFormatter.string(from: date as Date)
        Datetf.text=strDate
        fromtimetf.text=strtime
        Totimetf.text = strtime
    }
    
    
    @IBAction func CheckAvailabilityAction(_ sender: AnyObject) {
        if self.Datetf.text == "" {
            self.showalert(title: "Alert", message: "Select Date", buttontxt: "Ok")
        } else {
            UserDefaults.standard.set(self.Datetf.text, forKey: "SeatBookingDate")
            UserDefaults.standard.synchronize()
            
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: SeatavailibilityViewController.instance())
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
            }
            let container = SeatavailibilityViewController.instance()
            container.closeHandler = { _ in
                self.Loaddata()
                popup.dismiss()
                self.Loaddata()
            }
            popup.show(childViewController: container)
        }
    }
    
    @IBAction func SubmitbtnAction(_ sender: AnyObject) {
        self.local_activityIndicator_start()
        if(Datetf.text!.isEmpty) {
            self.local_activityIndicator_stop()
            self.showalert(title: "Alert", message: "Please Enter Date", buttontxt: "Ok")
        } else if(self.fromtimetf.text!.isEmpty) {
            self.local_activityIndicator_stop()
            self.showalert(title: "Alert", message: "Please Enter From Time", buttontxt: "Ok")
        } else if(self.Totimetf.text!.isEmpty) {
            self.local_activityIndicator_stop()
            self.showalert(title: "Alert", message: "Please Enter ToTime", buttontxt: "Ok")
        } else {
            uploadfile.setObject(LibraryAPI.sharedInstance.Userid,forKey:"UserId" as NSCopying)
            uploadfile.setObject(Datetf.text!,forKey:"BookDate" as NSCopying)
            let test = fromtimetf.text!
            let swiftryString : String = test.replacingOccurrences(of: ":", with: "")
            uploadfile.setObject(swiftryString,forKey:"FromTime" as NSCopying)
            let test1 = Totimetf.text!
            let swiftyString1 = test1.replacingOccurrences(of: ":", with: "")
            uploadfile.setObject(swiftyString1,forKey:"ToTime" as NSCopying)
            let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)WorkLocation.asmx/SeatBookingChecking")! as URL)
            request.httpMethod = "POST"
            request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
            request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
            request.httpBody = try? JSONSerialization.data(withJSONObject: self.uploadfile, options: JSONSerialization.WritingOptions(rawValue: 0))
            let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
            do {
                let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
                jsonresult = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                let size = CGSize(width: 30, height:30)
                DispatchQueue.global(qos: .background).async {
                    DispatchQueue.main.async {
                        if self.jsonresult.object(forKey: "Message")as? String == "" {
                            LibraryAPI.sharedInstance.ref=(self.jsonresult.object(forKey: "RefNumber") as? String)!
                            LibraryAPI.sharedInstance.seat=(self.jsonresult.object(forKey: "Seat") as? String)!
                            self.referencenumlbl.isHidden = false
                            self.referencenumlbl.text = LibraryAPI.sharedInstance.ref
                            self.seatnolbl.isHidden = false
                            self.seatnolbl.text = LibraryAPI.sharedInstance.seat
                            self.referencenumlbl.isHidden = false
                            self.seatnolbl.isHidden = false
                            self.ticketgenview.isHidden = false
                            self.Seatbookingtableview.reloadData()
                            self.local_activityIndicator_stop()
                            self.Loaddata()
                        } else {
                            self.ticketgenview.isHidden = true
                             self.local_activityIndicator_stop()
                            self.showalert(title: "Alert", message: "\(self.jsonresult.object(forKey: "Message") as! String)", buttontxt: "Ok")
                        }
                    }
                }
            } catch (let e) {
                print("error ;",e)
            }
        }
        
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if(textField.tag==1) {
            datepicker.date = Date()
            pickerview.isHidden=false
            datepicker.isHidden = false
            datepicker.datePickerMode = UIDatePickerMode.date
            currenttextfield="Date"
            datepicker.minimumDate = datepicker.date
            return false
        }
        
        if(textField.tag==2) {
            pickerview.isHidden=false
            datepicker.isHidden = false
            datepicker.datePickerMode = UIDatePickerMode.time
            datepicker.locale = NSLocale(localeIdentifier: "en_GB") as Locale
            currenttextfield="Time"
            return false
        }
        
        if(textField.tag==3) {
            pickerview.isHidden=false
            datepicker.isHidden = false
            datepicker.datePickerMode = UIDatePickerMode.time
            datepicker.locale = NSLocale(localeIdentifier: "en_GB") as Locale
            currenttextfield="to"
            return false
        }
        return true
    }
    
    @IBAction func DonebtnAction(_ sender: AnyObject) {
        datepicker.isHidden=true
        if( currenttextfield=="Date") {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = DateFormatter.Style.long
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let strDate = dateFormatter.string(from: datepicker.date)
            Datetf.text=strDate
            pickerview.isHidden = true
        } else if( currenttextfield=="Time") {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            let strDate = dateFormatter.string(from: datepicker.date)
            fromtimetf.text=strDate
            pickerview.isHidden = true
        } else if( currenttextfield=="to") {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            let strDate = dateFormatter.string(from: datepicker.date)
            Totimetf.text=strDate
            pickerview.isHidden = true
        }
    }
    
    @IBAction func CancelbtnAction(_ sender: AnyObject) {
        pickerview.isHidden=true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func Loaddata() {
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
            //self.responsestring = LibraryAPI.sharedInstance.RequestUrl(url: "\(LibraryAPI.sharedInstance.GlobalUrl)/WorkLocation.asmx/SeatBookingHistory?&Userid=\(LibraryAPI.sharedInstance.Userid)")
            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)/WorkLocation.asmx/SeatBookingHistory?&Userid=\(LibraryAPI.sharedInstance.Userid)", completion:  { (jsonmutable) in
                self.responsestring = jsonmutable
            DispatchQueue.main.async {
                if(self.responsestring.count>0) {
                   self.local_activityIndicator_stop()
                    self.reflbl.text = (self.responsestring.object(at: 0) as AnyObject).object(forKey: "Booking Reference") as? String
                    self.datelbl.text = (self.responsestring.object(at: 0) as AnyObject).object(forKey: "Booking Date") as? String
                    self.fromlbl.text = self.ReEditString(Time: ((self.responsestring.object(at: 0) as AnyObject).object(forKey: "From Time") as? String!)!)
                    self.tolbl.text = self.ReEditString(Time: ((self.responsestring.object(at: 0) as AnyObject).object(forKey: "To Time") as? String!)!)
                } else {
                   self.local_activityIndicator_stop()
                    self.nodatalbl.isHidden = false
                }
            }
        })
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return responsestring.count > 0 ? 1 : 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return responsestring.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BookinghistroyTableViewCell") as! BookinghistroyTableViewCell
        cell.Refnolbl.text=(responsestring.object(at: indexPath.row) as AnyObject).object(forKey: "Booking Reference") as? String
        cell.datelbl.text=(responsestring.object(at: indexPath.row) as AnyObject).object(forKey: "Booking Date") as? String
        cell.fromlbl.text=ReEditString(Time: ((responsestring.object(at: indexPath.row) as AnyObject).object(forKey: "From Time") as? String)!)
        cell.tolbl.text=ReEditString(Time: ((responsestring.object(at: indexPath.row) as AnyObject).object(forKey: "To Time") as? String)!)
        cell.selectionStyle = .none
        return cell as BookinghistroyTableViewCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        BottomView.frame = CGRect(x: 0, y: 118, width: self.BottomView.frame.width, height: self.BottomView.frame.height)
        seatavailabilityview.isHidden = false
        Seatbookingtableview.isHidden  = true
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    func ReEditString(Time : String) -> String {
        var ETime = Time
        var rstr = ""
        var i = 0
       
        for character in ETime.characters {
            if i == 2 {
                rstr = "\(character)" + ":" + rstr
            } else {
                rstr = "\(character)" + rstr
            }
            i = i+1
        }
        var str = ""
        
        for scharacter in rstr.characters {
            str = "\(scharacter)" + str
        }
        return str
    }
    
    @IBAction func SeatbookingBtn(_ sender: AnyObject) {
        if(responsestring.count > 0) {
            if(Btn_value == 1) {
                if(responsestring.count  == 1) {
                    BottomView.frame = CGRect(x: 0, y: 118, width: self.BottomView.frame.width, height: self.BottomView.frame.height)
                    Seatbookingtableview.frame = CGRect(x: 7, y: 63, width: self.Seatbookingtableview.frame.width, height: self.Seatbookingtableview.frame.height)
                    Seatbookingtableview.isScrollEnabled = false
                    Seatbookingtableview.isHidden = true
                } else if(responsestring.count == 2) {
                    BottomView.frame = CGRect(x: 0, y: 118+30, width: self.BottomView.frame.width, height: self.BottomView.frame.height)
                    Seatbookingtableview.isScrollEnabled = false
                    seatavailabilityview.isHidden = true
                    Seatbookingtableview.isHidden = false
                    Seatbookingtableview.reloadData()
                } else if(responsestring.count == 3) {
                    BottomView.frame = CGRect(x: 0, y: 118+60, width: self.BottomView.frame.width, height: self.BottomView.frame.height)
                    Seatbookingtableview.isScrollEnabled = false
                    Seatbookingtableview.isHidden = false
                    seatavailabilityview.isHidden = true
                    Seatbookingtableview.reloadData()
                } else if(responsestring.count == 4) {
                    BottomView.frame = CGRect(x: 0, y: 118+90, width: self.BottomView.frame.width, height: self.BottomView.frame.height)
                    Seatbookingtableview.isScrollEnabled = false
                    Seatbookingtableview.isHidden = false
                    seatavailabilityview.isHidden = true
                    Seatbookingtableview.reloadData()
                } else if(responsestring.count == 5) {
                    BottomView.frame = CGRect(x: 0, y: 118+130, width: self.BottomView.frame.width, height: self.BottomView.frame.height)
                    Seatbookingtableview.isScrollEnabled = true
                    Seatbookingtableview.isHidden = false
                    seatavailabilityview.isHidden = true
                    Seatbookingtableview.reloadData()
                } else {
                    BottomView.frame = CGRect(x: 0, y: 118+130, width: self.BottomView.frame.width, height: self.BottomView.frame.height)
                    Seatbookingtableview.isScrollEnabled = true
                    Seatbookingtableview.isHidden = false
                    seatavailabilityview.isHidden = true
                    Seatbookingtableview.reloadData()
                }
                Btn_value = 2
            } else {
                BottomView.frame = CGRect(x: 0, y: 118, width: self.BottomView.frame.width, height: self.BottomView.frame.height)
                seatavailabilityview.isHidden = false
                Seatbookingtableview.isHidden  = true
                Btn_value = 1
            }
        } else {
            Seatbookingtableview.isHidden = true
            nodatalbl.isHidden = false
        }
    }
}

extension UIViewController {
    func showalert(title: String, message: String, buttontxt: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: buttontxt, style: .cancel) { (action:UIAlertAction!) in
            self.local_activityIndicator_stop()
        }
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion:nil)
    }
}
