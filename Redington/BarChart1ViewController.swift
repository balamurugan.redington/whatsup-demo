//
//  BarChart1ViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 11/12/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

import Charts
//import Funnel
import KCFloatingActionButton

class BarChart1ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, KCFloatingActionButtonDelegate {
    
    @IBOutlet weak var notificationbtn: UIButton!
    @IBOutlet weak var Barchartcollectionview: UICollectionView!
    @IBOutlet weak var menubtn: UIButton!
    @IBOutlet weak var gradientview: UIView!
    
    var responsestring2 = NSMutableArray()
    var reversearray = NSMutableArray()
    var   Array30 = [Double]()
    var   array60 = [Double]()
    var   array90 = [Double]()
    var   array120 = [Double]()
    var   arraymorethan120=[Double]()
    var monthsbar=[String]()
    var isanimated=Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        gradientview.layer.cornerRadius = 3.0
        gradientview.clipsToBounds = true
        let colorTop =  UIColor(red: 30/255.0, green: 184/255.0, blue: 120/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 2/255.0, green: 132/255.0, blue: 168/255.0, alpha: 1.0).cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        gradientLayer.locations = [ 0.0, 1.0]
        gradientLayer.frame = self.gradientview.bounds
        self.gradientview.layer.insertSublayer(gradientLayer, at: 0)
        Barchartcollectionview.delegate = self
        Barchartcollectionview.dataSource = self
        Barchartcollectionview.bounces = false
        self.self.Barchartcollectionview.register(UINib(nibName: "BarChart11CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BarChart11CollectionViewCell")
        self.Barchartcollectionview.isPagingEnabled = true
        customnavigation()
    }
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.global(qos: .background).async {
            self.parseurl()
            DispatchQueue.main.async {
                self.Barchartcollectionview.reloadData()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
     func parseurl() {
        if Array30.count > 0 {
            monthsbar.removeAll()
            array60.removeAll()
            Array30.removeAll()
            array90.removeAll()
            array120.removeAll()
            arraymorethan120.removeAll()
        }
        self.local_activityIndicator_start()
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)CreditDetails.asmx/CreditAgeingDetails?UserID=\(LibraryAPI.sharedInstance.Userid)&BranchCode=\(LibraryAPI.sharedInstance.BranchCodeNew)", completion:  { (jsonmutable) in
            self.responsestring2 = jsonmutable
        if  self.responsestring2.count > 0 {
             self.reversearray=( self.responsestring2.reversed() as NSArray).mutableCopy() as! NSMutableArray
            for i in 0..<self.responsestring2.count {
                if let val=( self.reversearray.object(at: i) as AnyObject).object(forKey: "0to30Ageing") {
                     self.Array30.append(((val as AnyObject) as? Double)!)
                }
                
                if let val=( self.reversearray.object(at: i) as AnyObject).object(forKey: "31to60Ageing") {
                     self.array60.append(((val as AnyObject) as? Double)!)
                }
                
                if let val=( self.reversearray.object(at: i) as AnyObject).object(forKey: "61to90Ageing") {
                     self.array90.append(((val as AnyObject) as? Double)!)
                }
                
                if let val=( self.reversearray.object(at: i) as AnyObject).object(forKey: "91to120Ageing") {
                     self.array120.append(((val as AnyObject) as? Double)!)
                }
                
                if let val=( self.reversearray.object(at: i) as AnyObject).object(forKey: "Greaterthan120Ageing") {
                     self.arraymorethan120.append(((val as AnyObject) as? Double)!)
                }
                 self.monthsbar.append(( self.reversearray.object(at: i) as AnyObject).object(forKey: "MonthYear") as! String)
            }
        }
        self.local_activityIndicator_stop()
        self.Barchartcollectionview.reloadData()
            })
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return reversearray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width-10, height: collectionView.frame.size.height)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BarChart11CollectionViewCell", for: indexPath as IndexPath) as! BarChart11CollectionViewCell
        if reversearray.count > 0 {
            cell.ChartView.noDataText=""
        } else {
            // cell.ChartView.noDataText = "No chart data available."
        }
        
        cell.ChartView.layer.cornerRadius = 20.0
        cell.monthlbl.text = (reversearray.object(at: indexPath.row) as AnyObject).object(forKey: "MonthYear")as? String
        cell.chartbtn.tag = indexPath.row
        
        cell.chartbtn.addTarget(self, action: #selector(BarChart1ViewController.chartbuttonClicked(_:)), for: UIControlEvents.touchUpInside)
        
        if monthsbar.count > 0 {
            cell.setChartBarGroupDataSet(dataPoints: [monthsbar.reversed()[indexPath.row]], values: [Array30[indexPath.row]], values2: [array60[indexPath.row]], values3: [array90[indexPath.row]], values4: [array120[indexPath.row]], values5: [arraymorethan120[indexPath.row]], sortIndex: 1)
            isanimated=true
        }
        return cell
    }
    
    
    func chartbuttonClicked(_ sender:UIButton) {
        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "InvoicesummerydetailsViewController") as! InvoicesummerydetailsViewController
        self.navigationController?.pushViewController(lvc, animated: true)
    }
    
    func customnavigation() {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: 65)
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(BarChart1ViewController.buttonClicked(_:)),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Customer OverDue Summary"
    }
    
    func buttonClicked(_ sender:UIButton) {
        LibraryAPI.sharedInstance.tracker="1"
        if self.navigationController?.topViewController!.isKind(of: DashBoardViewController.classForCoder()) != nil {
            self.navigationController?.popToViewController((self.navigationController?.viewControllers.first)!, animated: true)
        } else {
            let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
            self.navigationController?.pushViewController(lvc, animated: false)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
}
