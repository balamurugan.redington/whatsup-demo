//
//  OrderviewDetailsViewController.swift
//  Whatsup
//
//  Created by truetech on 21/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import KCFloatingActionButton

class OrderviewDetailsViewController: UIViewController,KCFloatingActionButtonDelegate,UITableViewDelegate,UITableViewDataSource{

    @IBOutlet weak var titlelbl: UILabel!
    @IBOutlet var OrderTabelview: UITableView!
    var fromdate=String()
    var todate=String()
    var  responsestring = NSMutableArray()
    var fab=KCFloatingActionButton()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.customnavigation()
         OrderTabelview.register(UINib(nibName: "OrderEnquiryTableViewCell", bundle: nil), forCellReuseIdentifier: "OrderEnquiryTableViewCell")
        
        self.layoutFAB()
        self.view.layer.borderWidth = 1
        self.view.layer.borderColor = UIColor(red:222/255.0, green:225/255.0, blue:227/255.0, alpha: 1.0).cgColor
     
    }
    
    func fetchdata()
    {
        
        self.local_activityIndicator_start()
        
        // new version dispatch Que
        
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
            
            self.Loaddata()

            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
                
               
            }
        }
        
        // new version dispatch Que
        
          }
    
    func BackGroundRefresh(Notification:NSNotification)
    {
        fetchdata()
    }
   
        

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
  
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.fetchdata()

        
    }
    func Loaddata()
    {
       //responsestring = LibraryAPI.sharedInstance.RequestUrl(url: "\(LibraryAPI.sharedInstance.GlobalUrl)OrderStatus.asmx/CustomerOrderSummary?CuastomerCode=\(LibraryAPI.sharedInstance.CustomerCode)&FromDate=\(LibraryAPI.sharedInstance.CustomerCodeFrom)&ToDate=\(LibraryAPI.sharedInstance.CustomerCodeTo)&UserID=\(LibraryAPI.sharedInstance.Userid)")
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)OrderStatus.asmx/CustomerOrderSummary?CuastomerCode=\(LibraryAPI.sharedInstance.CustomerCode)&FromDate=\(LibraryAPI.sharedInstance.CustomerCodeFrom)&ToDate=\(LibraryAPI.sharedInstance.CustomerCodeTo)&UserID=\(LibraryAPI.sharedInstance.Userid)", completion:  { (jsonmutable) in
            self.responsestring = jsonmutable
        
        
        if(self.responsestring.count>0)
        {
           
           // dispatch_async(dispatch_get_main_queue(), {
                self.OrderTabelview.isHidden=false
            self.local_activityIndicator_stop()
            self.OrderTabelview.reloadData()
           // })
            
            
        }
        else
        {
          //  dispatch_async(dispatch_get_main_queue(), {
                self.OrderTabelview.isHidden=true
                let alertController = UIAlertController(title: "Alert", message:"No Data Available" , preferredStyle: .alert)
                
                let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                    let lvc=self.storyboard?.instantiateViewController(withIdentifier: "OrderEnquiryViewController") as! OrderEnquiryViewController
                    self.navigationController?.pushViewController(lvc, animated: true)
                    
                }
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion:nil)
            self.local_activityIndicator_stop()
            
           // })
            
        }
       })
    }
    
    func customnavigation()
        
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(OrderviewDetailsViewController.backbuttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "ORDER DETAILS"
        
    }
    
    
    
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
                self.fab.close()
        }
        
        fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
            
            self.fetchdata()
            
            self.fab.close()
        }
        
        
         fab.fabDelegate = self
        
        self.view.addSubview(fab)
        
    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
       
    }
    

    
    func backbuttonClicked(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //TableView Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if responsestring.count > 0
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     
        return 91
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        return responsestring.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderEnquiryTableViewCell") as! OrderEnquiryTableViewCell
       // var cell : OrderEnquiryTableViewCell! = tableView.dequeueReusableCellWithIdentifier("Cell") as! OrderEnquiryTableViewCell
        
        if(responsestring.count > 0)
        {
            let dic = responsestring[indexPath.row] as! NSDictionary
            
            let qtr = dic.object(forKey: "Customer Code") as? String
            let type = dic.object(forKey: "Customer Name") as? String
            titlelbl.text = "   "+qtr!+"-"+type!+" "
            cell.sodate.text=String(dic.object(forKey: "Order Date") as! String)
            cell.sonumber.text=String(dic.object(forKey: "Order Number") as! String)
            cell.invoicenumber.text=String(dic.object(forKey: "Invoice Number") as! String)
            cell.invoicedate.text=String(dic.object(forKey: "Invoice Date") as! String)
            cell.orderstatus.text=String(dic.object(forKey: "Order Status") as! String)
            if(cell.orderstatus.text=="COMPLETED")
            {
                cell.orderstatus.textColor=UIColor.green
            }
            else
            {
                cell.orderstatus.textColor=UIColor.red
            }
            
            cell.selectionStyle = .none
        }
        else
        {
            
        }
        
 
        return cell as OrderEnquiryTableViewCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        let dic1 = responsestring[indexPath.row] as! NSDictionary
        LibraryAPI.sharedInstance.orderno = String(dic1.object(forKey: "Order Number") as! String)
         LibraryAPI.sharedInstance.sostatus = "OrderNo"
        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailsSubViewController") as! OrderDetailsSubViewController
        
        self.navigationController?.pushViewController(lvc, animated: true)
        
    }
    
    
    
    

    

}
