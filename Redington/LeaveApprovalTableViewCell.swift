//
//  LeaveApprovalTableViewCell.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 27/07/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class LeaveApprovalTableViewCell: UITableViewCell {
    
    @IBOutlet weak var leaveno: UILabel!
    @IBOutlet weak var empcode: UILabel!
    @IBOutlet weak var empname: UILabel!
    @IBOutlet weak var empdesc: UILabel!
    @IBOutlet weak var noofdays: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
