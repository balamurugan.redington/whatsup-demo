//
//  PermissionallowViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 27/12/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class PermissionallowViewController: UIViewController {
    
    @IBOutlet weak var logoimgview: UIImageView!
    @IBOutlet weak var textview: UITextView!
    @IBOutlet weak var imageoutview: UIView!
    @IBOutlet weak var loginbtn: UIButton!
    @IBOutlet weak var pagecontrol: UIPageControl!
    
    var arr = NSMutableArray()
    var timer = Timer()
    var count = NSInteger()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageoutview.layer.cornerRadius = imageoutview.frame.size.width/2
        arr.insert("Allow to access your device location for login credentials.", at: 0)
        arr.insert("Enable camera permission to take a picture of preorder  and helpdesk.", at: 1)
        arr.insert("Allow to Store your this Application information and Picture in device for  backend Process.", at: 2)
        arr.insert("To Manage  this device id for your identify to enable successfully login. ", at: 3)
        timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(PermissionallowViewController.setCurrentPageLabel), userInfo: nil, repeats: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setCurrentPageLabel() {
        pagecontrol.isHidden=false;
        if count <= arr.count {
            self.animate(Str: textview)
            self.textview.text = arr.object(at: count) as! String
            pagecontrol.currentPage=count;
        }
        count=count+1
        if count >= arr.count {
            count=0;
            pagecontrol.currentPage=count+3;
        }
    }
    
    
    func animate(Str:AnyObject) {
        let animation1 = CATransition()
        animation1.duration = 1.0
        animation1.type = kCATransitionPush
        animation1.subtype = kCATransitionFromRight
        animation1.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        Str.layer.add(animation1, forKey: "SwitchToView")
        Str.layer.add(animation1, forKey: "SwitchToView")
    }
    
    @IBAction func ProceedbtnAction(_ sender: AnyObject) {
        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "RootViewController") as! RootViewController
        self.navigationController?.pushViewController(lvc, animated: true)
    }
}
