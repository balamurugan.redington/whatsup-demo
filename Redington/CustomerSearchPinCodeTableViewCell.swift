//
//  CustomerSearchPinCodeTableViewCell.swift
//  Redington
//
//  Created by TRUETECHMAC3 on 28/12/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit

class CustomerSearchPinCodeTableViewCell: UITableViewCell {

    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var labelAddress: UILabel!
    @IBOutlet var labelCity: UILabel!
    @IBOutlet var labelState: UILabel!
    @IBOutlet var labelPinCOde: UILabel!
    @IBOutlet var labelAd: UILabel!
    @IBOutlet var labelCct: UILabel!
    @IBOutlet var labelSt: UILabel!
    @IBOutlet var labelPC: UILabel!
    
    @IBOutlet weak var cellview: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
