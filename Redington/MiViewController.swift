//
//  MiViewController.swift
//  Whatsup
//
//  Created by truetech on 22/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import KCFloatingActionButton

class MiViewController: UIViewController,KCFloatingActionButtonDelegate,UITableViewDelegate,UITableViewDataSource{
    @IBOutlet var Mitableview: UITableView!
    var  responsestring = NSMutableArray()
    var fab=KCFloatingActionButton()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customnavigation()
        self.layoutFAB()
        self.fetchvalues()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.fetchvalues()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    func BackGroundRefresh(Notification: NSNotification)
    {
        Mitableview.reloadData()
    }
    func Loaddata()
    {
        self.local_activityIndicator_start()
        self.getsuccess(params: [:], urlhit:  "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/CustomerInactive?Userid=\(LibraryAPI.sharedInstance.Userid)", completion:  { (jsonmutable) in
            self.responsestring = jsonmutable
            if(self.responsestring.count>0)  {
                DispatchQueue.main.async {
                    self.Mitableview.isHidden=false
                    self.Mitableview.reloadData()
                    self.local_activityIndicator_stop()
                }
            } else {
                DispatchQueue.main.async {
                    self.local_activityIndicator_stop()
                    self.Mitableview.isHidden=true
                    let alert = UIAlertController(title: "Sorry..!!", message: "No Data Available", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler:
                        { (ACTION :UIAlertAction!)in
                            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        })
    }
    
    func fetchvalues()
    {
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
            self.Loaddata()
            DispatchQueue.main.async {
                
            }
        }
    }
    
    func customnavigation()
    {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(MiViewController.backbuttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "3 Months Inactive Customers"
    }
    
    
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            self.fab.close()
        }
        
        fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
            self.fetchvalues()
            self.fab.close()
        }
        
        fab.fabDelegate = self
        
        self.view.addSubview(fab)
        
    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
    }
    
    func backbuttonClicked(sender:UIButton)
    {
        LibraryAPI.sharedInstance.tracker="1"
        self.navigationController?.popViewController(animated: true)
        
        
    }
    //TableView Delegates
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        if (responsestring.count > 0)
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return responsestring.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : MiTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as! MiTableViewCell
        if(cell == nil)
        {
            cell = Bundle.main.loadNibNamed("Cell", owner: self, options: nil)![0] as! MiTableViewCell;
            
        }
        
        if(responsestring.count > 0)
        {
            let dic = responsestring[indexPath.row] as! NSDictionary
            cell.Customername.text=dic.object(forKey: "Customer Name") as? String
            cell.Customcode.text=String(dic.object(forKey: "Customer Code") as! String)
            cell.Lastinvoice.text=String(dic.object(forKey: "Last Invoice Number") as! String)
            cell.Balancedue.text=String(dic.object(forKey: "Balance Due Amount") as! Double)
            cell.Lastsalevalue.text=String(dic.object(forKey: "Last Invoice Value") as! Double)
            cell.Lastsaledate.text=String(dic.object(forKey: "Last Invoice Date") as! String)
            cell.cellview.layer.borderColor = UIColor(red:30/255.0, green:184/255.0, blue:120/255.0, alpha: 1.0).cgColor
            cell.cellview.layer.borderWidth = 1.0
            cell.cellview.layer.cornerRadius = 5.0
            
        }
        else
        {
            self.local_activityIndicator_stop()
        }
        
        cell.selectionStyle = .none
        return cell as MiTableViewCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
}
