//
//  CustomHeaderViewController.swift
//  Redington
//
//  Created by truetech on 05/08/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit

class CustomHeaderViewController: UIViewController {
    @IBOutlet var HeaderLabel: UILabel!

    @IBOutlet var Notificationbtn: UIButton!
    @IBOutlet var MenuBtn: UIButton!
    @IBOutlet var backbutton: UIButton!
    
    @IBOutlet weak var Ticketbtn: UIButton!
   
    @IBOutlet weak var pincode: UIButton!
   
 
      
    override func viewDidAppear(_ animated: Bool) {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    
    func notificationmethod()
    {
        let userdefaults = Foundation.UserDefaults.standard
        let value = userdefaults.dictionary(forKey: "Notification")
        
        var title: String!
        var message: String!
        var imagestr: String!
        

            if value?["data"] as? NSDictionary != nil
            {
                let notification = value?["data"] as? NSDictionary
                
                title = notification?["title"] as! String
                message = notification?["body"] as! String
                
                if value?["imageurl"] != nil
                {
                    imagestr = value?["imageurl"] as! String
                    if imagestr != ""
                    {
                        let popup = PopupController
                            .create(parentViewController: self.parent!)
                            .show(childViewController: notificationimageViewController.instance())
                            
                            .didShowHandler { popup in
                            }
                            .didCloseHandler { _ in
                        }
                        let container = notificationimageViewController.instance()
                        container.closeHandler = { _ in
                            popup.dismiss()
                        }
                        
                        popup.show(childViewController: container)
                    }
                    else
                    {
                        let popup = PopupController
                            .create(parentViewController: self.parent!)
                            .show(childViewController: PushnotificationtextViewController.instance())
                            
                            .didShowHandler { popup in
                            }
                            .didCloseHandler { _ in
                        }
                        let container = PushnotificationtextViewController.instance()
                        container.closeHandler = { _ in
                            popup.dismiss()
                        }
                        
                        popup.show(childViewController: container)
                    }
                }
            }
            else
            {
                title = value?["title"] as! String
                message = value?["body"] as! String
                
                if value?["imageurl"] != nil
                {
                    imagestr = value?["imageurl"] as! String
                    if imagestr != ""
                    {
                        let popup = PopupController
                            .create(parentViewController: self.parent!)
                            .show(childViewController: notificationimageViewController.instance())
                            
                            .didShowHandler { popup in
                            }
                            .didCloseHandler { _ in
                        }
                        let container = notificationimageViewController.instance()
                        container.closeHandler = { _ in
                            popup.dismiss()
                            
                        }
                        popup.show(childViewController: container)
                    }
                    else
                    {
                        let popup = PopupController
                            .create(parentViewController: self.parent!)
                            .show(childViewController: PushnotificationtextViewController.instance())
                            
                            .didShowHandler { popup in
                            }
                            .didCloseHandler { _ in
                        }
                        let container = PushnotificationtextViewController.instance()
                        container.closeHandler = { _ in
                            popup.dismiss()
                        }
                        
                        popup.show(childViewController: container)
                    }
                }
                

            }
            

        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    

    @IBAction func BackAction(_ sender: AnyObject)
    {
        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
        self.navigationController?.pushViewController(lvc, animated: true)
        
    }
    

}

