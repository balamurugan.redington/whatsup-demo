//
//  OrderviewdetailTableViewCell.swift
//  Redington
//
//  Created by truetech on 04/10/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit

class OrderviewdetailTableViewCell: UITableViewCell {

    @IBOutlet weak var lineno: UILabel!
    @IBOutlet weak var productname: UILabel!
    @IBOutlet var price: UILabel!
    @IBOutlet var qnty: UILabel!
    @IBOutlet var pono: UILabel!
    @IBOutlet var itemcode: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
