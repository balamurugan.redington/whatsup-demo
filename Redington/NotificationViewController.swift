//
//  NotificationViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 25/04/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit
import KCFloatingActionButton

class NotificationViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,KCFloatingActionButtonDelegate {
    
    @IBOutlet weak var notificationTable: UITableView!
    var popupcheck : Bool!
    var responsestring = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        notificationTable.delegate = self
        notificationTable.dataSource = self
        notificationTable.register(UINib(nibName: "NotificationDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "NotificationDetailTableViewCell")
        notificationTable.tableFooterView = UIView(frame: .zero)
        notificationTable.separatorStyle = .none
        loadddata()
        if popupcheck == true {
            firsttimemobilenocheck()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.customnavigation()
        loadddata()
    }
    
    func firsttimemobilenocheck() {
        let popup = PopupController
            .create(parentViewController: self.parent!)
            .show(childViewController: MobileViewController.instance())
            .didShowHandler { popup in
            }
            .didCloseHandler { _ in
        }
        let container = MobileViewController.instance()
        container.closeHandler = { _ in
            popup.dismiss()
        }
        popup.show(childViewController: container)
    }
    
    
    func customnavigation() {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(NotificationViewController.backbuttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Notification Alert"
    }
   
    func loadddata() {
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)OrderStatus.asmx/Overduecollectionnotification?userId=\(LibraryAPI.sharedInstance.Userid)", completion: { (jsonmutable) in
            self.responsestring = jsonmutable
            if(self.responsestring.count > 0) {
            self.notificationTable.reloadData()
        } else {
        }
      })
    }
    
    func backbuttonClicked(sender:UIButton) {
        self.responsestring.removeAllObjects()
        self.navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if responsestring.count > 0 {
            return 2
        } else {
            return 1
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 103
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 1
        } else {
            return responsestring.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationDetailTableViewCell", for: indexPath) as! NotificationDetailTableViewCell
        cell.cellview.layer.cornerRadius = 5
        if indexPath.section == 0  {
            cell.message.text = "MOBILE NUMBER UPDATE"
            cell.company.text = "Click here to update your Mobile Number"
            cell.date.isHidden = true
            cell.invoiceno.isHidden = true
        } else {
            if(responsestring.count > 0) {
                cell.message.text = "OVERDUE CHEQUE COLLECTION"
                let dic = responsestring[indexPath.row] as! NSDictionary
                let name = dic.object(forKey: "CUSTOMER NAME") as? String
                let code = dic.object(forKey: "CUSTOMER CODE") as? String
                let date = dic.object(forKey: "INVOICE DATE") as? String
                let no = dic.object(forKey: "INVOICE NUMBER") as? String
                cell.company.text = code!+" - "+name!
                cell.date.text = "Invoice Date : " + date!
                cell.invoiceno.text = "Invoice no : " + no!
            } else {
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath.section == 0) {
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: MobileViewController.instance())
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
            }
            let container = MobileViewController.instance()
            container.closeHandler = { _ in
                popup.dismiss()
            }
            popup.show(childViewController: container)
        }
        else
        {
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: NotificationDeatilsViewController.instance())
                
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
            }
            let container = NotificationDeatilsViewController.instance()
            container.closeHandler = { _ in
                popup.dismiss()
            }
            
            popup.show(childViewController: container)
        }
        
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
