//
//  LookupViewController.swift
//  Redington
//
//  Created by truetech on 14/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit


protocol Customdelegate
{
    func Passvaluefrompop(text:String)
}



class LookupViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var LookupTableview: UITableView!
    @IBOutlet var LookupSearchbar: UISearchBar!
    
    @IBOutlet weak var titleHeaderview: UIView!
    
    var responsearray=NSMutableArray()
    var searchActive=Bool()
    var searcharray=NSMutableArray()
    var customercode=String()
    var Customername=String()
    var Branchcode=String()
    var BranchName=String()
    var text=String()
    var cv:Customdelegate?
    var searchtext=String()
    var customercodearray = NSMutableArray()
    var Branchcodearray = NSMutableArray()
    var BranchNamearray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad() 
        self.Customnavigation()
       // self.Loaddata()
        searchActive = false
        
        titleHeaderview.isHidden = false
        LookupSearchbar.isHidden = false
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled=false
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        responsearray.removeAllObjects()
        
        if(LibraryAPI.sharedInstance.Currentcustomerlookup=="City")
        {
            titleHeaderview.isHidden = true
            LookupSearchbar.isHidden = true
            responsearray = LibraryAPI.sharedInstance.Cityearray
        }
        else
        {
            titleHeaderview.isHidden = false
            LookupSearchbar.isHidden = false
            responsearray = LibraryAPI.sharedInstance.LookupGlobal
        }
       
    }
//    func Loaddata()
//    {
//        //http://www.redington-india.com/whatsup/Customer.asmx/SalesAnalysis?Customer=S10003&Branch=S1
//        
//        responsearray = LibraryAPI.sharedInstance.RequestUrl("\(LibraryAPI.sharedInstance.GlobalUrl)Customer360.asmx/Customersearch360?Customer=\(LibraryAPI.sharedInstance.CustomerCode)&TYPE=1")
//        if(responsearray.count>0)
//        {
//            
//        }
//        
//    }

    
    
    
    //http://www.redington-india.com/whatsup/Customer360.asmx/Customersearch360?Customer=S10003&TYPE=1

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func Customnavigation()
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: 0,y: 0,width: self.view.frame.width,height: 65);
        
        customView.Notificationbtn.isHidden=true
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        self.view.addSubview(customView.view)
     
        
        customView.HeaderLabel.text = "Lookup"
       
         customView.MenuBtn.addTarget(self, action: #selector(LookupViewController.backbuttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        
    }
    
    func backbuttonClicked(sender:UIButton)
    {
        
        if(LibraryAPI.sharedInstance.Currentcustomerlookup=="360")
        {
        
           self.navigationController?.popViewController(animated: true)
        }
        else if(LibraryAPI.sharedInstance.Currentcustomerlookup=="Visitlog")
        {
            
            self.navigationController?.popViewController(animated: true)
        }
        else if(LibraryAPI.sharedInstance.Currentcustomerlookup=="PreOrderHistory") 
        {
            
            self.navigationController?.popViewController(animated: true)
        }
        else if(LibraryAPI.sharedInstance.Currentcustomerlookup=="S/O_Invoice")
        {
            
            self.navigationController?.popViewController(animated: true)
        }
        else if(LibraryAPI.sharedInstance.Currentcustomerlookup=="spc")
        {
            
            self.navigationController?.popViewController(animated: true)
        }
            
        else if(LibraryAPI.sharedInstance.Currentcustomerlookup=="Billingodcustomer")
        {
            
            self.navigationController?.popViewController(animated: true)
        }
        else if(LibraryAPI.sharedInstance.Currentcustomerlookup=="DBC")
        {
            
            self.navigationController?.popViewController(animated: true)
        }
        else if(LibraryAPI.sharedInstance.Currentcustomerlookup=="ECB")
        {
            
            self.navigationController?.popViewController(animated: true)
        }//ECB
        else if(LibraryAPI.sharedInstance.Currentcustomerlookup=="SPCENTRYOPTION")
        {
            
            self.navigationController?.popViewController(animated: true)
        }//ECB
            
            
        else if(LibraryAPI.sharedInstance.Currentcustomerlookup == "City")
        {
            self.navigationController?.popViewController(animated: true)
        }
        
        else
        {
             self.dismiss(animated: true, completion: nil)
        }
        
    }

    
    
    
    
    
    
    //TableView Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if (searchActive == false)
        {
            if (responsearray.count > 0)
            {
                return 1
            }
            else
            {
                return 0
            }
           
        }
        else
        {
            if(searcharray.count > 0)
            {
                return 1
            }
            else
            {
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        var num=Int()
        if(searchActive==false)
        {
            num = responsearray.count
            
        }
        else
        {
            num = searcharray.count
        }
        return num
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        var cell : LookupTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as! LookupTableViewCell
        if(cell == nil)
        {
            cell = Bundle.main.loadNibNamed("Cell", owner: self, options: nil)![0] as! LookupTableViewCell;
            
        }
        
        
        if(indexPath.row%2==0)
        {
            cell.contentView.backgroundColor=UIColor.white
        }
        else
        {
            cell.contentView.backgroundColor=(UIColor.init(red: 244/255.0, green: 242/255.0, blue: 242/255.0, alpha: 1.0))
            
        }
        
        if(searchActive==true)
        {
            
            cell.CustomerCodeLabel.text=customercodearray.object(at: indexPath.row) as? String
            cell.CustomerNameLabel.text=searcharray.object(at: indexPath.row) as? String
            cell.BranchCodeLabel.text=Branchcodearray.object(at: indexPath.row) as? String
            cell.BranchNameLabel.text=BranchNamearray.object(at: indexPath.row) as? String
            
            
            let paraStyle = NSMutableParagraphStyle()
            paraStyle.firstLineHeadIndent = 15.0
            paraStyle.paragraphSpacingBefore = 10.0
            
            var para = NSMutableAttributedString()
            para =  NSMutableAttributedString(string:(searcharray.object(at: indexPath.row)) as! String)

            
            
            do {
                let regex = try NSRegularExpression(pattern: searchtext, options: NSRegularExpression.Options.caseInsensitive )
                let nsstr = searcharray.object(at: indexPath.row) as? NSString
                text=nsstr! as String
                let all = NSRange(location: 0, length: nsstr!.length)
                var matches : [String] = [String]()
                regex.enumerateMatches(in: text, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: all) {
                    (result : NSTextCheckingResult?, _, _) in
                    
                    if let r = result {
                        let results = nsstr!.substring(with: r.range) as String
                        
                        matches.append(results)
                      //  let substringrange=result!.rangeAtrangeAt(0)
                            let substringrange=result!.rangeAt(0)
                        
                        para.addAttribute(NSForegroundColorAttributeName, value:UIColor.init(red: 73/255.0, green: 206/255.0, blue: 157/255.0, alpha: 1.0), range: substringrange)
                        cell.CustomerNameLabel.attributedText=para
                    }
                }
                
                
            } catch {
                
            }


        }
        else
        {
            let dic = responsearray[indexPath.row] as! NSDictionary
            if(LibraryAPI.sharedInstance.Currentcustomerlookup=="City")
            {
                titleHeaderview.isHidden = true
                
                cell.CustomerCodeLabel.text=dic.object(forKey: "Mode") as? String
                cell.CustomerNameLabel.text=dic.object(forKey: "Detail") as? String
    
            }
            else
            {
                titleHeaderview.isHidden = false
                
                cell.CustomerCodeLabel.text=dic.object(forKey: "Customer Code") as? String
                cell.CustomerNameLabel.text=dic.object(forKey: "Customer Name") as? String
                cell.BranchCodeLabel.text=dic.object(forKey: "Branch Code") as? String
                cell.BranchNameLabel.text=dic.object(forKey: "Branch Name") as? String
            }

            
        
        }
        cell.selectionStyle = .none
        return cell as LookupTableViewCell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        if(searchActive==true)
        {
            cv?.Passvaluefrompop(text: (customercodearray.object(at: indexPath.row)) as! String)
 
        }
        else
        {
            if(LibraryAPI.sharedInstance.Currentcustomerlookup=="City")
            {
                cv?.Passvaluefrompop(text: ((responsearray.object(at: indexPath.row) as AnyObject).object(forKey: "Detail") as? String)!)
                
            }
            else
            {
                cv?.Passvaluefrompop(text: ((responsearray.object(at: indexPath.row) as AnyObject).object(forKey: "Customer Code") as? String)!)
            }
            
        }
        if(LibraryAPI.sharedInstance.Currentcustomerlookup=="360") 
        {
           self.navigationController?.popViewController(animated: true)
        }
        else if(LibraryAPI.sharedInstance.Currentcustomerlookup=="Visitlog")
        {
            self.navigationController?.popViewController(animated: true)
        }
        else if(LibraryAPI.sharedInstance.Currentcustomerlookup=="PreOrderHistory")
        {
            self.navigationController?.popViewController(animated: true)
        }
        else if(LibraryAPI.sharedInstance.Currentcustomerlookup=="S/O_Invoice")
        {
            self.navigationController?.popViewController(animated: true)
        }
        else if(LibraryAPI.sharedInstance.Currentcustomerlookup=="spc")
        {
            self.navigationController?.popViewController(animated: true)
        }
        else if(LibraryAPI.sharedInstance.Currentcustomerlookup=="Billingodcustomer")
        {
            self.navigationController?.popViewController(animated: true)
        }
        else if(LibraryAPI.sharedInstance.Currentcustomerlookup=="DBC")
        {
            self.navigationController?.popViewController(animated: true)
        }
        else if(LibraryAPI.sharedInstance.Currentcustomerlookup=="ECB")
        {
            self.navigationController?.popViewController(animated: true)
        }
        else if(LibraryAPI.sharedInstance.Currentcustomerlookup=="SPCENTRYOPTION")
        {
            self.navigationController?.popViewController(animated: true)
        }
        else if(LibraryAPI.sharedInstance.Currentcustomerlookup=="City")
        {
            self.navigationController?.popViewController(animated: true)
            
        }

        else
        {
           self.dismiss(animated: true, completion: nil)
        }
       
        
    }
    
   
    
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String)
    {
        if(searchBar.text?.count==0)
        {   searchActive = false
            LookupTableview.reloadData()
            LookupSearchbar.resignFirstResponder();
        }
        else
        {
            self.searchBarSearchButtonClicked(searchBar: UISearchBar())
            self.searchBarTextDidBeginEditing(searchBar: UISearchBar())
        }
        
    }
    
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar)
    {
        // Searchbar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar)
    {
        searchActive = true
        searcharray.removeAllObjects()
        //dict.removeAllObjects()
        // Searchbar.resignFirstResponder()
        if(LookupSearchbar.text != nil)
        {
            
            for i in 0  ..< responsearray.count
            {
                  searchtext=LookupSearchbar.text!
                let dic  = responsearray[i] as! NSDictionary
                customercode=(dic.object(forKey: "Customer Code") as? String)!
                Customername=(dic.object(forKey: "Customer Name") as? String)!
                Branchcode=(dic.object(forKey: "Branch Code") as? String)!
                BranchName=(dic.object(forKey: "Branch Name") as? String)!
                
                self.searchforpattern(text: Customername)
                
            }
            self.LookupTableview.reloadData()
        }
        else
        {
            searchActive = false
            LookupSearchbar.resignFirstResponder()
            self.LookupTableview.reloadData()
        }
        
        
    }
    
    func searchforpattern( text : String) {
        do {
            let regex = try NSRegularExpression(pattern: searchtext, options: NSRegularExpression.Options.caseInsensitive )
            let nsstr = text as NSString
            let all = NSRange(location: 0, length: nsstr.length)
            var matches : [String] = [String]()
            regex.enumerateMatches(in: text, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: all) {
                (result : NSTextCheckingResult?, _, _) in
                
                if let r = result {
                    let result = nsstr.substring(with: r.range) as String
                    matches.append(result)
                 
                    if(self.searcharray.contains(text))
                        {
                            
                            let i = (self.searcharray.index(of: text))
                            self.searcharray.remove(text)
                            self.customercodearray.removeObject(at: i)
                            self.Branchcodearray.removeObject(at: i)
                            self.BranchNamearray.removeObject(at: i)
                        }
                    self.searcharray.add(self.Customername)
                    self.customercodearray.add(self.customercode)
                    self.Branchcodearray.add(self.Branchcode)
                    self.BranchNamearray.add(self.BranchName)
                        self.LookupTableview.reloadData()

            
                    self.LookupTableview.reloadData()
                    
                }
            }
            
            
        } catch {
            searchActive = false
            LookupSearchbar.resignFirstResponder()
            self.LookupTableview.reloadData()
        }
        
        
        
    }

  
}
