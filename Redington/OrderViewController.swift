//
//  OrderViewController.swift
//  Redington
//
//  Created by truetech on 04/10/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import KCFloatingActionButton

class OrderViewController: UIViewController,KCFloatingActionButtonDelegate,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var statuslbl: UILabel!
    
    @IBOutlet var popupImageview: UIImageView!
    @IBOutlet var popupview: UIView!
    @IBOutlet var ordertableview: UITableView!
    @IBOutlet var backbtn: UIButton!
    @IBOutlet var menubtn: UIButton!
    
    var totallinevalue: Int!
    var ordno=String()
    var fab=KCFloatingActionButton()
    var keyarr=NSMutableArray()
    var listvalue = NSMutableArray()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        totallinevalue = 0
        
         self.ordertableview.separatorStyle = UITableViewCellSeparatorStyle.none
       
        self.layoutFAB()
        self.fetchdata()
        customnavigation()
        
        // Do any additional setup after loading the view.
    }
    func customnavigation()
        
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(OrderViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "OrderDetail"
    }
    
 

    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            
          
            
            self.fab.close()
        }
        
        fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
            
            self.fetchdata()
            
            self.fab.close()
        }
        
        fab.fabDelegate = self
        
        self.view.addSubview(fab)
        
    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
      
    }
    
    func fetchdata()
    {
       self.local_activityIndicator_start()
        // new version dispatch Que
        
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
            
              self.ParseVisitkeyinfo()
            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
                
               
                
            }
        }
        
        // new version dispatch Que
       
    }
    
    @IBAction func CloseButton(_ sender: AnyObject)
    {
        
        popupview.alpha = 1
        popupview.isHidden = true
        UIView.animate(withDuration: 0.7) {
            self.popupview.alpha = 0
        }
    
        
    }
    func buttonClicked(_ sender:UIButton)
    {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "OrderHistoryViewController")), animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    func ParseVisitkeyinfo()
    {
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)orderEntry.asmx/OrderDetail?CustomerCode=\(LibraryAPI.sharedInstance.CustomerCode)&Order_Number=\(LibraryAPI.sharedInstance.orderno)", completion: { (jsonmutable) in
            self.keyarr = jsonmutable
        if(self.keyarr.count>0)
        {
            DispatchQueue.main.async {
                 self.ordertableview.isHidden=false
                self.local_activityIndicator_stop()
                self.ordertableview.reloadData()
            }
        } else {
            DispatchQueue.main.async {
                self.ordertableview.isHidden=true
                self.local_activityIndicator_stop()
                let alertController = UIAlertController(title: "Alert", message: "No Data Available", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                    UIAlertAction in
                self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "OrderHistoryViewController")), animated: true)
                    NSLog("OK Pressed")
                }
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
     })
    }
    
    
    
    //TableView Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
       
        if keyarr.count > 0
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        return keyarr.count // LibraryAPI.sharedInstance.breadthvalues.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        var cell : OrderviewdetailTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as! OrderviewdetailTableViewCell
        if(cell == nil)
        {
            cell = Bundle.main.loadNibNamed("Cell", owner: self, options: nil)![0] as! OrderviewdetailTableViewCell;
            
        }
        
        if(keyarr.count > 0)
        {
            let dic = keyarr[indexPath.row] as! NSDictionary
            cell.productname.text=dic.object(forKey: "ProductName") as? String
            
            cell.itemcode.text=dic.object(forKey: "ItemCode") as? String
            cell.pono.text=dic.object(forKey: "PO_No") as? String
            
            
            cell.price.text=String(dic.object(forKey: "UnitPrice") as! Int)
            cell.qnty.text=String(dic.object(forKey: "Quantity") as! Int)
            
            //  lineno
            
            let unitprice = String(dic.object(forKey: "UnitPrice") as! Int)
            let line = dic.object(forKey: "Quantity") as! Int
            
            let total = String(Int(unitprice)! * line)
            cell.lineno.text = total
            
            totallinevalue = totallinevalue + Int(total)!
            
            self.statuslbl.text = String(totallinevalue)
        }
        else
        
        {
            
        }
        
   
       
        cell.selectionStyle = .none
        return cell as OrderviewdetailTableViewCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
    }
    
    func loadimage(urlstr:String)
    {
        popupImageview.image = UIImage(named: "noimage.png")
        
        let url:NSURL = NSURL(string: urlstr)!
      
        let data = NSData(contentsOf:url as URL)
        
        if( data==nil) {
            DispatchQueue.main.async {
                
                 self.popupImageview.image = UIImage(named: "noimage.png")
            }
           
           
        }
        else{
            DispatchQueue.main.async {
                self.popupImageview.image = UIImage(data:data! as Data)
                
            }
                      }
        
    }
    
    
    @IBAction func PushpobtnAction(_ sender: AnyObject) {
        
        
       if keyarr.count > 0
       {
        popupview.alpha = 0
        popupview.layer.shadowColor = UIColor.lightGray.cgColor
        popupview.layer.shadowOpacity = 0.4
        popupview.layer.shadowOffset =  CGSize(width: 0, height: 10)
        popupview.layer.shadowRadius = 10
        
        popupview.layer.shouldRasterize = true
        popupview.isHidden = false
        UIView.animate(withDuration: 0.5) {
            self.popupview.alpha = 1
        }
        
        
        //  popupview.hidden=false
        self.local_activityIndicator_start()
        
        // new version dispatch Que
        
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
            if((self.keyarr.object(at: 0) as AnyObject).object(forKey: "POURL") as? String=="")
            {
                DispatchQueue.main.async {
                    
                    self.popupImageview.image = UIImage(named: "noimage.png")
                }
                              
            }
            else
            {
                self.loadimage(urlstr: ((self.keyarr.object(at: 0) as AnyObject).object(forKey: "POURL") as? String)!)
            }
            
            
            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
            self.local_activityIndicator_stop()

                
            }
        }
        
        // new version dispatch Que
        
        
        }
        else
        
       {
         showToast(message: "No Data Available")
        }
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
