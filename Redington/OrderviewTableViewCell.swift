//
//  OrderviewTableViewCell.swift
//  Whatsup
//
//  Created by truetech on 21/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit

class OrderviewTableViewCell: UITableViewCell {

  
    @IBOutlet weak var invoicenumber: UILabel!
    @IBOutlet weak var sonumber: UILabel!
    
    @IBOutlet weak var orderstatus: UILabel!
    @IBOutlet weak var sodate: UILabel!
    
    @IBOutlet weak var invoicedate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
