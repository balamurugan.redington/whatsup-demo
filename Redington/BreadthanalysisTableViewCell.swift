//
//  BreadthanalysisTableViewCell.swift
//  Whatsup
//
//  Created by truetech on 24/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit

class BreadthanalysisTableViewCell: UITableViewCell {

    @IBOutlet var Grp: UILabel!
    @IBOutlet var Biz: UILabel!
    @IBOutlet var Preview: UILabel!
    @IBOutlet var Current: UILabel!
    @IBOutlet var LikeImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
