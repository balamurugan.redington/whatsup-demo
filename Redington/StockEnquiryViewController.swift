//
//  StockEnquiryViewController.swift
//  Redington
//
//  Created by truetech on 29/08/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import KCFloatingActionButton

class StockEnquiryViewController: UIViewController,KCFloatingActionButtonDelegate,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource {
    
    @IBOutlet var Locationtf: UITextField!
    @IBOutlet var Businesstf: UITextField!
    @IBOutlet var Picker: UIPickerView!
    @IBOutlet var PickerView: UIView!
    @IBOutlet var Submitbtn: UIButton!
    var fab = KCFloatingActionButton()
    var selectedrow=Int()
    var currentpicker=String()
    var pickerarray=NSMutableArray()
    var responsearray=NSMutableArray()
    var businesscode=String()
    var stock=String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.Submitbtn.layer.cornerRadius=5
        self.customnavigation()
        self.layoutFAB()
        Locationtf.layer.cornerRadius = 5.0
        Locationtf.layer.borderWidth = 1.0
        self.Locationtf.layer.borderColor = UIColor(red:211/255.0, green:211/255.0, blue:211/255.0, alpha: 1.0).cgColor
        Businesstf.layer.cornerRadius = 5.0
        Businesstf.layer.borderWidth = 1.0
        self.Businesstf.layer.borderColor = UIColor(red:211/255.0, green:211/255.0, blue:211/255.0, alpha: 1.0).cgColor
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)

    }
    
    func customnavigation()
    {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        customView.MenuBtn.addTarget(self, action: #selector(OrderEnquiryViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Stock Enquiry"
    }
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            self.fab.close()
        }
        fab.fabDelegate = self
        self.view.addSubview(fab)
        
    }
    
    func Loaddata(url:String)
    {
        self.local_activityIndicator_start()
        self.getsuccess(params: [:], urlhit: url, completion:  { (jsonmutable) in
            self.responsearray = jsonmutable
        if(self.responsearray.count>0)
        {
        self.pickerarray.removeAllObjects()
        for i in 0  ..< self.responsearray.count
        {
            let dic = self.responsearray[i] as! NSDictionary
            if(self.currentpicker=="business") {
                let code = dic.object(forKey: "BUSINESS CODE") as? String
                let desc = dic.object(forKey: "BUSINESS DESC") as? String
                let str=code?.appending(" - \(desc!)")
                self.pickerarray.add(str!)
                self.Picker.reloadAllComponents()
                self.PickerView.isHidden = false
                self.local_activityIndicator_stop()
            } else {
                
                let code = dic.object(forKey: "STOCK ROOM CODE") as? String
                let desc = dic.object(forKey: "STOCK ROOM DESC") as? String
                let str=code?.appending(" - \(desc!)")
                self.pickerarray.add(str!)
                self.Picker.reloadAllComponents()
                self.PickerView.isHidden = false
                self.local_activityIndicator_stop()
            }
        }
        }else{
            self.local_activityIndicator_stop()
            self.PickerView.isHidden = true
            let alertController = UIAlertController(title: "Alert", message:"No Data Available" , preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
            }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)
            }
       })
       
        
    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
        
    }
    override func viewWillAppear(_ animated: Bool) {
    }
    
    @IBAction func submitaction(_ sender: AnyObject)
    {
        if Businesstf.text! as String == ""
        {
            let alert = UIAlertController(title: "Sorry..!!", message: "Enter Business Field", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else if Locationtf.text! as String == "" {
            let alert = UIAlertController(title: "Sorry..!!", message: "Enter location Field", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else if Locationtf.text! as String == "" && Businesstf.text! as String == "" {
            let alert = UIAlertController(title: "Sorry..!!", message: "Enter Business & location Field", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            let fullName = Businesstf.text
            let fullNameArr = fullName!.split{$0 == " "}.map(String.init)
            businesscode=(fullNameArr[0])
            let fullName1 = Locationtf.text
            let fullNameArr1 = fullName1!.split{$0 == " "}.map(String.init)
            stock=(fullNameArr1[0])
            let lvc=self.storyboard?.instantiateViewController(withIdentifier: "StockenquiryviewDetailsViewController") as! StockenquiryviewDetailsViewController
            lvc.businesscode=businesscode
            lvc.stockroom=stock
            self.navigationController?.pushViewController(lvc, animated: true)
        }
    }
    
    func buttonClicked(sender:UIButton)
    {
        let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
        self.navigationController?.pushViewController(lvc, animated: false)
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
       if(pickerarray.count > 0)
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var num = Int()
        if pickerarray.count > 0
        {
            num=pickerarray.count
            return num
        }
        else
        {
            return 0
        }
       
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
       if(pickerarray.count > 0)
       {
         return  pickerarray.object(at: row) as! String
       }
        else
       {
        return ""
        }
        
        
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
     
        selectedrow = row
    }
    
    @IBAction func CancelAction(_ sender: AnyObject)
    {
        PickerView.isHidden=true
    }
    
    @IBAction func DoneAction(_ sender: AnyObject)
    {
        
        if(currentpicker=="Location")
        {
            Locationtf.text = pickerarray.object(at: selectedrow) as? String
        }
        else
        {
            Businesstf.text = pickerarray.object(at: selectedrow) as? String
        }
        PickerView.isHidden=true
    }
    
    
    
    //textfieldDelegates
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
      
        fab.isHidden=true
        if(pickerarray.count>0)
        {
            pickerarray.removeAllObjects()
        }
        if(textField.tag==2)
        {
            selectedrow=0
            let size = CGSize(width: 30, height:30)
            self.currentpicker="business"
            self.local_activityIndicator_start()
            DispatchQueue.global(qos: .background).async {
                 self.Loaddata(url: "\(LibraryAPI.sharedInstance.GlobalUrl)StockDetails.asmx/BusinessAccessDetails?UserID=\(LibraryAPI.sharedInstance.Userid)")
                DispatchQueue.main.async {
                }
            }
            return false
            
        }
        
        if(textField.tag==1)
        {
            selectedrow=0
            self.currentpicker="Location"
            let size = CGSize(width: 30, height:30)
            self.local_activityIndicator_start()
            DispatchQueue.global(qos: .background).async {
                self.Loaddata(url: "\(LibraryAPI.sharedInstance.GlobalUrl)StockDetails.asmx/StockAccessDetails?UserID=\(LibraryAPI.sharedInstance.Userid)")
                DispatchQueue.main.async {
                    
                    
                }
            }
            return false
            
        }
        
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
     
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        fab.isHidden=false
        textField.resignFirstResponder()
        
        
        return true
    }
    
    
}
