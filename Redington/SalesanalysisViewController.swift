//
//  SalesanalysisViewController.swift
//  Whatsup
//
//  Created by truetech on 22/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
import Charts
import KCFloatingActionButton

class SalesanalysisViewController: UIViewController,ChartViewDelegate,KCFloatingActionButtonDelegate,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var nodataview: UIView!
    @IBOutlet var graphbtn: UIButton!
    @IBOutlet var tablebtn: UIButton!
    @IBOutlet var Chartview: CombinedChartView!
    @IBOutlet var salestableview: UITableView!
    
    @IBOutlet weak var tablecontentview: UIView!
    var responsestring=NSMutableArray()
    var netbreadth=[Double]()
    var salesbreadth=[Double]()
    var month=[String]()
    var Linechartarray=[Double]()
    @IBOutlet var backbtn: UIButton!
    @IBOutlet var menubtn: UIButton!
    var reversearray=NSMutableArray()
    var fab=KCFloatingActionButton()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.customnavigation()
        nodataview.isHidden = true
        self.layoutFAB()
        Chartview.delegate=self
        Chartview.zoom(scaleX: 2.5, scaleY: 0.0, x: 1.0, y: 1.0)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.fetchvalues()
    }
    func fetchvalues()
    {
       self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
             self.Loaddata()
            DispatchQueue.main.async {
               
            }
        }
    }
    
    
    func setChart(xValues: [String], yValuesLineChart: [Double], value2:[Double],yValuesBarChart: [Double]) {
        Chartview.noDataText = "Please provide data for the chart."
        
        var yVals1 : [ChartDataEntry] = [ChartDataEntry]()
        var yVals2 : [BarChartDataEntry] = [BarChartDataEntry]()
        var yVals3: [BarChartDataEntry] =  [BarChartDataEntry]()
        var de: [BarChartDataEntry] = []
        var val = 0
        for i in 0..<xValues.count {
            yVals1.append(ChartDataEntry(x: Double(val), y: yValuesLineChart[i]))
            val = val + 1
            print("val in yval2 :",val)
            yVals2.append(BarChartDataEntry(x: Double(val), y: value2[i] - 1))
            val = val + 1
             print("val in yval3 :",val)
            yVals3.append(BarChartDataEntry(x: Double(val), y: yValuesBarChart[i] - 1))
            val = val + 1
            print("val in yVals1 :",val)
        }
        
        for i in 0..<xValues.count
        {
            //let dr = BarChartDataEntry(x: Double(i), yValues: [value2[i],yValuesBarChart[i]])
            let dr = BarChartDataEntry(x: Double(i), yValues: [value2[i]])
            de.append(dr)
        }
        print("yvals ; ", yVals1)
        print("******************")
        print("yvals2 : ", yVals2)
        print("******************")
        print("yvals3 : ", yVals3)
        print("******************")
        print("deval :", de)
        print("xvalue details :", xValues)
        let lineChartSet = LineChartDataSet(values: yVals1, label: "")
        lineChartSet.colors=[UIColor.red]
        lineChartSet.circleColors=[UIColor.red]
        lineChartSet.circleRadius = 3.0 // the radius of the node circle
        lineChartSet.fillAlpha = 65 / 255.0
        lineChartSet.circleHoleColor = UIColor.white
        lineChartSet.highlightColor = UIColor.white
        lineChartSet.label="Overdue"
        lineChartSet.drawCircleHoleEnabled = true
        let barChartSet: BarChartDataSet = BarChartDataSet(values:yVals2 , label: "Sales Breadth")
        let barChartSet1: BarChartDataSet = BarChartDataSet(values: yVals3, label: "Net Breadth")
        let dataSets: [BarChartDataSet] = [barChartSet, barChartSet1]
        let data : CombinedChartData = CombinedChartData(dataSets: yVals3 as! [IChartDataSet])
        Chartview.rightAxis.enabled=false
        Chartview.legend.labels=["Sales Breadth","Net Breadth","Overdue"]
        Chartview.legend.enabled=true
        barChartSet.colors =  [UIColor.init(red: 35/255.0, green: 150/255.0, blue: 204/255.0, alpha: 1.0)]
        barChartSet.stackLabels=["Sales Breadth","Net Breadth"]
       barChartSet1.colors =  [UIColor.init(red: 154/255.0, green: 181/255.0, blue: 73/255.0, alpha: 1.0)]
        data.barData = BarChartData(dataSets: dataSets)
        data.lineData = LineChartData(dataSets: [lineChartSet])
        Chartview.data = data
    }
    
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {() -> Void in
            view.isHidden = hidden
            }, completion: { _ in })
    }
    
    
    
    @IBAction func Graphaction(_ sender: AnyObject) {
        self.tablebtn.backgroundColor=UIColor.white
        self.graphbtn.setTitleColor(UIColor.white, for: UIControlState.normal)
        self.graphbtn.backgroundColor = UIColor.init(red:30/255.0 , green: 184/255.0, blue: 120/255.0, alpha: 1.0)
        self.tablebtn.setTitleColor(UIColor.gray, for: UIControlState.normal)
        self.tablebtn.setImage(UIImage(named: "graylist.png"), for: UIControlState.normal)
        self.graphbtn.setImage(UIImage(named: "whitechart.png"), for: UIControlState.normal)
       
        self.setView(view: Chartview, hidden: false)
        self.Chartview.isHidden=false
        self.tablecontentview.isHidden = true
        self.salestableview.isHidden=true
        self.setChart(xValues: month.reversed(), yValuesLineChart:  Linechartarray.reversed(), value2: salesbreadth.reversed(),yValuesBarChart: netbreadth.reversed())
        //self.setChartBarGroupDataSet(month, values: salesbreadth, values2: netbreadth, values3: Linechartarray,sortIndex: 0)
        
        
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func TablaACtion(_ sender: AnyObject)
    {
         
        self.tablebtn.setTitleColor(UIColor.white, for: UIControlState.normal)
        self.tablebtn.backgroundColor = UIColor.init(red:30/255.0 , green: 184/255.0, blue: 120/255.0, alpha: 1.0)
        self.tablebtn.setImage(UIImage(named: "whitelist.png"), for: UIControlState.normal)
        
        self.graphbtn.setTitleColor(UIColor.gray, for: UIControlState.normal)
         self.graphbtn.backgroundColor = UIColor.init(red:30/255.0 , green: 184/255.0, blue: 120/255.0, alpha: 1.0)
        self.graphbtn.setImage(UIImage(named: "graychart.png"), for: UIControlState.normal)
        self.graphbtn.backgroundColor=UIColor.white
        
        self.setView(view: salestableview, hidden: false)
        self.Chartview.isHidden=true
        self.tablecontentview.isHidden = false
        self.salestableview.isHidden=false
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    
    
    func Loaddata()
    {
        
        if(reversearray.count>0)
        {
            reversearray.removeAllObjects()
            responsestring.removeAllObjects()
            salesbreadth.removeAll()
            netbreadth.removeAll()
            Linechartarray.removeAll()
            month.removeAll()
        }
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)/Customer.asmx/SalesAnalysis?Customer=\(LibraryAPI.sharedInstance.CustomerCode)&Branch=\(LibraryAPI.sharedInstance.branchcode)&UserId=\(LibraryAPI.sharedInstance.Userid)", completion:  { (jsonmutable) in
            self.reversearray = jsonmutable
        
        
        if(self.reversearray.count>0)
        {
            self.responsestring=(self.reversearray.reversed() as NSArray).mutableCopy() as! NSMutableArray
            
            for i in 0..<self.responsestring.count
            {
                let dicres = self.responsestring[i] as! NSDictionary
                self.salesbreadth.append((dicres.object(forKey: "Sales") as? Double)!)
                self.netbreadth.append((dicres.object(forKey: "Sales Margin") as? Double)!)
                self.Linechartarray.append((dicres.object(forKey: "Over Due") as? Double)!)
            }
            
            
            for i in 0..<self.salesbreadth.count
            {
                let dicsales = self.responsestring[i] as! NSDictionary
                self.month.append((dicsales.object(forKey: "MonthYear") as? String)!)
            }
            
            DispatchQueue.main.async {
                
                self.salestableview.isHidden=false
                self.graphbtn.isUserInteractionEnabled=true
                self.local_activityIndicator_stop()
                self.salestableview.reloadData()
            }
           
            
            
        }
        else
        {
            DispatchQueue.main.async {
                self.local_activityIndicator_stop()
                self.nodataview.isHidden = false
                self.salestableview.isHidden=true
                self.graphbtn.isUserInteractionEnabled=false
            }
            
            
        }
        
        })
        
    }
    
       
    
    
    func customnavigation()
        
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(SalesanalysisViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Sales Analysis"
    }

    
    
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            
            self.fab.close()
        }
        fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
            
            
            self.fetchvalues()
            
            self.fab.close()
        }
        fab.fabDelegate = self
        
        self.view.addSubview(fab)
        
    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
        
    }
    
    
    func buttonClicked(sender:UIButton)
    {
        LibraryAPI.sharedInstance.tracker="1"
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    //TableView Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return responsestring.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : SalesTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as! SalesTableViewCell
        if(cell == nil)
        {
            cell = Bundle.main.loadNibNamed("Cell", owner: self, options: nil)![0] as! SalesTableViewCell;
            
        }
        if(indexPath.row%2==0)
        {
            cell.contentView.backgroundColor=(UIColor.init(red: 240/255.0, green: 246/255.0, blue: 225/255.0, alpha: 1.0))
            
        }
        else
        {
            //cell.contentView.backgroundColor=UIColor.whiteColor()
            cell.contentView.backgroundColor=(UIColor.init(red: 215/255.0, green: 250/255.0, blue: 252/255.0, alpha: 1.0))
        }
        let dic1res = responsestring[indexPath.row] as! NSDictionary
        cell.Month.text=String(dic1res.object(forKey: "MonthYear") as! String)
        cell.Sale.text=String(dic1res.object(forKey: "Sales") as! Double)
        cell.Margin.text=String(dic1res.object(forKey: "Sales Margin") as! Double)
        cell.Return.text=String(dic1res.object(forKey: "Sales Return") as! Double)
        cell.Overdue.text=String(dic1res.object(forKey: "Over Due") as! Double)
        
        cell.selectionStyle = .none
        
        return cell as SalesTableViewCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
