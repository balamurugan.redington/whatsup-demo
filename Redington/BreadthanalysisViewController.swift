//
//  BreadthanalysisViewController.swift
//  Whatsup
//
//  Created by truetech on 24/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
import Charts
import KCFloatingActionButton

class BreadthanalysisViewController: UIViewController,ChartViewDelegate,KCFloatingActionButtonDelegate,IAxisValueFormatter{
    
    
    
    @IBOutlet weak var Gradientview: UIView!
    @IBOutlet var Notificationbtn: UIButton!
    @IBOutlet var Menubtn: UIButton!
    @IBOutlet var Headerview: UIView!
    @IBOutlet var Chartview: BarChartView!
    var month=[String]()
    var netbreadth=[Double]()
    var salesbreadth=[Double]()
    var responsestring=NSMutableArray()
    var fab=KCFloatingActionButton()
    var Bizcode=String()
    var chartno = Int()
    var Cdata = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        customnavigation()
        Gradientview.layer.cornerRadius = 3.0
        Gradientview.clipsToBounds = true
        let colorTop =  UIColor(red: 255/255.0, green: 135/255.0, blue: 153/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 2/255.0, green: 132/255.0, blue: 168/255.0, alpha: 1.0).cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        gradientLayer.locations = [ 0.0, 1.0]
        gradientLayer.frame = self.view.bounds
        self.Gradientview.layer.insertSublayer(gradientLayer, at: 0)
        self.layoutFAB()
        Chartview.delegate=self
        chartno = 0
    }
    
    func customnavigation()
    {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(BreadthanalysisViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Breadth Analysis"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.global(qos: .background).async {
            self.Chartview.invalidateIntrinsicContentSize()
            self.Chartview.notifyDataSetChanged()
            self.Chartview.barData?.clearValues()
            self.Chartview.barData?.notifyDataChanged()
            self.Chartview.reloadInputViews()
            self.Chartview.data?.clearValues()
            self.month.removeAll()
            self.salesbreadth.removeAll()
            self.netbreadth.removeAll()
            self.Loaddata()
            DispatchQueue.main.async {
                
            }
        }
    }
    override func viewWillLayoutSubviews() {
        Chartview.reloadInputViews()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func buttonClicked(sender:UIButton)
    {
        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "BreathAnalysisListViewController") as! BreathAnalysisListViewController
        self.navigationController?.pushViewController(lvc, animated: true)
    }
    
    func Loaddata()
    {
        local_activityIndicator_start()
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/BreathDetails?UserID=\(LibraryAPI.sharedInstance.Userid)&Branch=\(LibraryAPI.sharedInstance.BranchCodeNew)&Biz=\(Bizcode)", completion:  { (jsonmutable) in
            self.responsestring = jsonmutable
            self.salesbreadth.removeAll()
            self.month.removeAll()
            self.netbreadth.removeAll()
            if(self.responsestring.count>0)
            {
                if(self.responsestring.count == 1)
                {
                    let dic = self.responsestring[0] as! NSDictionary
                    if(dic.object(forKey: "Message") != nil)
                    {
                        let alertController = UIAlertController(title: "Alert", message: "No Data Available" , preferredStyle: .alert)
                        
                        let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                            self.navigate()
                        }
                        alertController.addAction(cancelAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                    else
                    {
                        for i in 0..<self.responsestring.count
                        {
                            let dic1 = self.responsestring[i] as! NSDictionary
                            self.salesbreadth.append((dic1.object(forKey:"Breadth Achieved") as? Double)!)
                            self.netbreadth.append((dic1.object(forKey:"Net Breadth Achieved") as? Double)!)
                            let str=String(dic1.object(forKey:"Month") as! Int)
                            let str1=String(dic1.object(forKey:"Year") as! Int)
                            let formattedstring=str.appending(", \(str1)")
                            self.month.append(formattedstring)
                        }
                        self.setChartBarGroupDataSet(dataPoints: self.month, values: self.salesbreadth, values2: self.netbreadth, sortIndex: 1)
                    }
                    
                }
                else
                {
                    
                    
                    for i in 0..<self.responsestring.count
                    {
                        let dic2 = self.responsestring[i] as! NSDictionary
                        self.salesbreadth.append((dic2.object(forKey: "Breadth Achieved") as? Double)!)
                        self.netbreadth.append((dic2.object(forKey: "Net Breadth Achieved") as? Double)!)
                        let str=String(dic2.object(forKey: "Month") as! Int)
                        let str1=String(dic2.object(forKey: "Year") as! Int)
                        let formattedstring=str.appending(", \(str1)")
                        
                        self.month.append(formattedstring)
                        
                    }
                    self.setChartBarGroupDataSet(dataPoints: self.month, values: self.salesbreadth, values2: self.netbreadth, sortIndex: 1)
                }
                self.local_activityIndicator_stop()
            }else{
                self.local_activityIndicator_stop()
            }
        })
    }
    func navigate() {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "BreathAnalysisListViewController")), animated: true)
    }
    
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            self.fab.close()
        }
        fab.fabDelegate = self
        self.view.addSubview(fab)
    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
        
    }
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        var muti = self.month.count * 2
        if Int(value) > muti{
            return ""
        } else {
            if Int(value) == 0{
                Cdata = 0
                return self.month[Int(Cdata)]
            }else{
                if Int(value) % 2 == 0{
                    if self.month.count  <  Cdata {
                        return ""
                    }else{
                        Cdata = Cdata + 1
                        if self.month.count >= Cdata{
                            if Cdata == 1 {
                                print("cdata 1 ",Cdata,Int(value))
                                return self.month[Int(Cdata)]
                            }else{
                                Cdata = Int(value) / 2
                                print("cdata value:",Cdata)
                                if Cdata == 1 {
                                    return " "
                                } else {
                                    return self.month[Int(Cdata)]
                                }
                            }
                        } else {
                            return ""
                        }
                    }
                }else{
                    return ""
                }
            }
            
        }
    }
    
    func setChartBarGroupDataSet(dataPoints: [String], values: [Double], values2: [Double],sortIndex:Int) {
        var dataEntries: [BarChartDataEntry] = []
        var dataEntries2: [BarChartDataEntry] = []
        //var dataEntries3: [BarChartDataEntry] = []
        let xAxis:XAxis = Chartview.xAxis
        xAxis.getLongestLabel()
        xAxis.avoidFirstLastClippingEnabled=true
        Chartview.legend.enabled=false
        Chartview.viewPortHandler.setMaximumScaleY(60.0)
        Chartview.leftAxis.axisMinValue=0.0
        Chartview.zoom(scaleX: 4.0, scaleY: 0.0, x: 0.0, y: 0.0)
        for i in 0..<dataPoints.count {
            let dataEntry = BarChartDataEntry(x: Double(chartno), yValues: [values[i]], data: dataPoints[i] as AnyObject)
            dataEntries.append(dataEntry)
            chartno = chartno + 1
            let dataEntry1 = BarChartDataEntry(x: Double(chartno), yValues: [values2[i]])
            dataEntries2.append(dataEntry1)
            chartno = chartno + 1
            //  let dataEntry2 = BarChartDataEntry(x: Double(chartno), yValues: [0.0])
            //  dataEntries3.append(dataEntry2)
            // chartno = chartno + 1
        }
        print("data in month",self.month)
        let chartDataSet = BarChartDataSet(values: dataEntries, label: " ")
        xAxis.valueFormatter = self
        chartDataSet.barBorderWidth = 0.4
        let chartDataSet2 = BarChartDataSet(values: dataEntries2, label: "")
        chartDataSet2.barBorderWidth=0.4
        // let chartDataSet3 = BarChartDataSet(values: dataEntries2, label: "")
        // chartDataSet3.barBorderWidth=0.4
        let dataSets: [BarChartDataSet] = [chartDataSet,chartDataSet2]
        let data = BarChartData(dataSets: dataSets)
        var colors: [UIColor] = []
        var colors1: [UIColor] = []
        // var colors2: [UIColor] = []
        for _ in 0..<dataPoints.count {
            colors.append(UIColor.init(red: 35/255.0, green: 150/255.0, blue: 204/255.0, alpha: 1.0))
        }
        for _ in 0..<dataPoints.count {
            colors1.append(UIColor.init(red: 154/255.0, green: 181/255.0, blue: 73/255.0, alpha: 1.0))
        }
        // for _ in 0..<dataPoints.count {
        //   colors1.append(UIColor.init(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0))
        //}
        chartDataSet.colors = colors
        chartDataSet2.colors = colors1
        Chartview.data = data
        Chartview.setScaleEnabled(false)
        Chartview.descriptionText = ""
        Chartview.xAxis.labelPosition = .bottom
        Chartview.rightAxis.enabled=false
        Chartview.xAxis.drawGridLinesEnabled = true
        Chartview.xAxis.drawAxisLineEnabled = true
        Chartview.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .easeInBounce)
    }
    
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "BreathAnalysisInvoiceViewController") as! BreathAnalysisInvoiceViewController
        lvc.code=Bizcode
        self.navigationController?.pushViewController(lvc, animated: true)
    }
}
