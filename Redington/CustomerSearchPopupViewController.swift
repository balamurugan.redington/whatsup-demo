//
//  CustomerSearchPopupViewController.swift
//  Redington
//
//  Created by TRUETECHMAC3 on 27/12/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
import Speech
//import  NVActivityIndicatorView
class CustomerSearchPopupViewController: UIViewController,PopupContentViewController,SFSpeechRecognizerDelegate,UITextFieldDelegate {
    @IBOutlet var textFieldPinCode: UITextField!
    @IBOutlet var microphonebtn: UIButton!
    @IBOutlet var Closebtn: UIButton!
    @IBOutlet var Okbtn: UIButton!
     var responsestring=NSMutableArray()
    var closeHandler: (() -> Void)?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 10.0, *) {
            let sf=SpeechFile()
            microphonebtn.isHidden=false
            sf.speechRecognizer.delegate = self
            
            SFSpeechRecognizer.requestAuthorization { (authStatus) in
                
                var isButtonEnabled = false
                
                switch authStatus {
                case .authorized:
                    isButtonEnabled = true
                    
                case .denied:
                    isButtonEnabled = false
                    
                case .restricted:
                    isButtonEnabled = false
                    
                case .notDetermined:
                    isButtonEnabled = false
                }
                
                OperationQueue.main.addOperation({
                    
                    self.microphonebtn.isEnabled = isButtonEnabled
                    
                })
            }
            
        } else {
            microphonebtn.isHidden=true
            
        }
        
        microphonebtn.isEnabled = false
        
        
        
        Okbtn.layer.cornerRadius=10.0
        
                Okbtn.alpha=0.5
        
        
        Okbtn.isUserInteractionEnabled=false
        textFieldPinCode.tag = 1
        Okbtn.layer.cornerRadius = 15.0
        textFieldPinCode.keyboardType = .numberPad
        addDoneButtonOnKeyboard()
        // Do any additional setup after loading the view.
        
    }
    
    func addDoneButtonOnKeyboard()
        
    {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0,y: 0,width: 320,height: 50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(CustomerSearchPopupViewController.doneButtonAction))
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        doneToolbar.items = items as AnyObject as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
         textFieldPinCode.inputAccessoryView = doneToolbar
    }
    func doneButtonAction()
    {
        textFieldPinCode.resignFirstResponder()
    }

    
    @IBAction func CloseBtnAction(_ sender: AnyObject) {
        closeHandler?()
    }
    @IBAction func CloseAction(_ sender: AnyObject)
    {
        
        closeHandler?()
    }
    
    @IBAction func okbtnAction(_ sender: AnyObject) {
        
        LibraryAPI.sharedInstance.Pincode = textFieldPinCode.text!
        
        if  LibraryAPI.sharedInstance.Pincode == ""{
            self.showalert(Title: "Sorry!", Message: "PinCode is empty")
            
        }else{
            self.LoadCustomerSearchPincode()
        }
        
    }
    
    @available(iOS 10.0, *)
    @IBAction func speechAction(_ sender: AnyObject) {
        let speech=SpeechFile()
        if speech.audioEngine.isRunning {
            speech.audioEngine.stop()
            speech.recognitionRequest?.endAudio()
            microphonebtn.isEnabled = false
            let arrow = UIImageView()
            let image = UIImage(named: "speech.png")
            arrow.image = image!.imageWithColor(tintColor: UIColor.init(red:30/255.0 , green: 184/255.0, blue: 120/255.0, alpha: 1.0)).withRenderingMode(.alwaysOriginal)
            microphonebtn.setImage(arrow.image, for: UIControlState.normal)
            // microphoneButton.setTitle("Start Recording", for: .normal)
        } else {
            self.textFieldPinCode.text=""
            self.startRecording()
            Okbtn.alpha=0.5
            self.Okbtn.isUserInteractionEnabled=false
            let arrow = UIImageView()
            let image = UIImage(named: "speechon.png")
            arrow.image = image!.imageWithColor(tintColor: UIColor.init(red:30/255.0 , green: 184/255.0, blue: 120/255.0, alpha: 1.0)).withRenderingMode(.alwaysOriginal)
            microphonebtn.setImage(arrow.image, for: UIControlState.normal)
           
        }
        

        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
       
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        guard let text = textField.text else { return true }
        
        let newLength = text.count + string.count - range.length
    
        if (isBackSpace == -92) {
            
            if((textField.text?.count)!>6)
            {
                Okbtn.alpha=1.0
                
                Okbtn.isUserInteractionEnabled=true
                
            }
            else
            {
                
                Okbtn.alpha=0.5
                Okbtn.isUserInteractionEnabled=false
                
            }
            
        }
        else
        {
            if((textField.text?.count)!>=5)
            {
                Okbtn.alpha=1.0
                Okbtn.isUserInteractionEnabled=true
                
            }
            else
            {
                Okbtn.alpha=0.5
                Okbtn.isUserInteractionEnabled=false
                
            }
        }
        
        
        return true && newLength <= 6
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
      
        
        textField.resignFirstResponder()
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
      
        LibraryAPI.sharedInstance.poptextfieldclicked=false
        return true
    }
         
    
    
    @available(iOS 10.0, *)
    func startRecording() {
        
        let speech=SpeechFile()
        
        if speech.recognitionTask != nil {  //1
            speech.recognitionTask?.cancel()
            speech.recognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()  //2
        do {
            try audioSession.setCategory(AVAudioSessionCategoryRecord)
            try audioSession.setMode(AVAudioSessionModeMeasurement)
            try audioSession.setActive(true)
        } catch {
        }
        
        speech.recognitionRequest = SFSpeechAudioBufferRecognitionRequest()  //3
        
        guard let inputNode = speech.audioEngine.inputNode else {
            fatalError("Audio engine has no input node")
        }  //4
        
        guard let recognitionRequest = speech.recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        } //5
        
        speech.recognitionRequest!.shouldReportPartialResults = true  //6
        
        speech.recognitionTask = speech.speechRecognizer.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in  //7
            
            var isFinal = false  //8
            
            if result != nil {
                
                self.textFieldPinCode.text = result?.bestTranscription.formattedString.uppercased() //9
                
                if((self.textFieldPinCode.text?.count)! >= 4)
                {
                    self.Okbtn.alpha=1.0
                    self.Okbtn.isUserInteractionEnabled=true
                    //self.lookupbtn.userInteractionEnabled=true
                }
                else
                {
                    self.Okbtn.isUserInteractionEnabled=false
                    //self.lookupbtn.userInteractionEnabled=false
                }
                
                isFinal = (result?.isFinal)!
            }
            
            if error != nil || isFinal {  //10
                speech.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                
                speech.recognitionRequest = nil
                speech.recognitionTask = nil
                
                self.microphonebtn.isEnabled = true
            }
        })
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)  //11
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            speech.recognitionRequest?.append(buffer)
        }
        
        speech.audioEngine.prepare()  //12
        do {
            try speech.audioEngine.start()
        } catch {
        }
        
        textFieldPinCode.text = "Speak Now!"
        
    }
    
    @available(iOS 10.0, *)
    func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            microphonebtn.isEnabled = true
        } else {
            microphonebtn.isEnabled = false
        }
    }
    
    
    func showalert(Title:String,Message:String)
    {
        
        let alert = UIAlertController(title: Title, message: Message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.default, handler:
            { (ACTION :UIAlertAction!)in
                
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func sizeForPopup(popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: 260, height: 260)
    }
    
    class func instance() -> CustomerSearchPopupViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "CustomerSearchPopupViewController") as! CustomerSearchPopupViewController
    }
    
    func LoadCustomerSearchPincode(){
        
       self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
                self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/CustomerSearchPincodewise?Pincode=\(LibraryAPI.sharedInstance.Pincode)&Userid=\(LibraryAPI.sharedInstance.Userid)&RecordNo=20&PageNo=1", completion:  { (jsonmutable) in
                self.responsestring = jsonmutable
                
                if(self.responsestring.count>0)
                {
                    self.local_activityIndicator_stop()
                    self.closeHandler?()
                    LibraryAPI.sharedInstance.PincodedataArry = self.responsestring
                    self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "CustomerSearchPinCodeViewController")), animated: true)
                    self.sideMenuViewController?.hideMenuViewController()
                }else{
                    self.local_activityIndicator_stop()
                    self.showalert(Title: "Sorry!", Message: "No data found")
                }
            
        })
        }
        
        // new version dispatch Que
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
