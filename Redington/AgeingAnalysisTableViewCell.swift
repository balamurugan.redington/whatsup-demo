//
//  AgeingAnalysisTableViewCell.swift
//  Whatsup
//
//  Created by truetech on 20/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit

class AgeingAnalysisTableViewCell: UITableViewCell {

    @IBOutlet var month: UILabel!
    @IBOutlet var rowfirst: UILabel!
    @IBOutlet var rowsecond: UILabel!
    @IBOutlet var rowthird: UILabel!
    @IBOutlet var rowfourth: UILabel!
    @IBOutlet var rowfifth: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
