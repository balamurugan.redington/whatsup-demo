//
 //  PopupController.swift
 //  PopupController
 //
 //  Created by 佐藤 大輔 on 2/4/16.
 //  Copyright © 2016 Daisuke Sato. All rights reserved.
 //
 
 import UIKit
 
 public enum PopupCustomOption {
    case Layout(PopupController.PopupLayout)
    case Animation(PopupController.PopupAnimation)
    case BackgroundStyle(PopupController.PopupBackgroundStyle)
    case Scrollable(Bool)
    case DismissWhenTaps(Bool)
    case MovesAlongWithKeyboard(Bool)
 }
 
 typealias PopupAnimateCompletion =  () -> ()
 
 // MARK: - Protocols
 /** PopupContentViewController:
  Every ViewController which is added on the PopupController must need to be conformed this protocol.
  */
 public protocol PopupContentViewController {
    
    /** sizeForPopup(popupController: size: showingKeyboard:):
     return view's size
     */
    func sizeForPopup(popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize
 }
 
 public class PopupController: UIViewController {
    
    public enum PopupLayout {
        case Top, Center, Bottom
        
        func origin(view: UIView, size: CGSize = UIScreen.main.bounds.size) -> CGPoint {
            switch self {
            case .Top: return CGPoint(x: (size.width - view.frame.width) / 2, y: 0)
            case .Center: return CGPoint(x: (size.width - view.frame.width) / 2, y: (size.height - view.frame.height) / 2)
            case .Bottom: return CGPoint(x: (size.width - view.frame.width) / 2, y: size.height - view.frame.height)
            }
        }
    }
    
    public enum PopupAnimation {
        case FadeIn, SlideUp
    }
    
    public enum PopupBackgroundStyle {
        case BlackFilter(alpha: CGFloat)
    }
    
    // MARK: - Public variables
    public var popupView: UIView!
    
    // MARK: - Private variables
    public var movesAlongWithKeyboard: Bool = true
    public var scrollable: Bool = true {
        didSet {
            updateScrollable()
        }
    }
    public var dismissWhenTaps: Bool = true {
        didSet {
            if dismissWhenTaps {
                registerTapGesture()
            }
        }
    }
    public var backgroundStyle: PopupBackgroundStyle = .BlackFilter(alpha: 0.4) {
        didSet {
            updateBackgroundStyle(style: backgroundStyle)
        }
    }
    public var layout: PopupLayout = .Center
    public var animation: PopupAnimation = .FadeIn
    
    public let margin: CGFloat = 16
    public let baseScrollView = UIScrollView()
    public var isShowingKeyboard: Bool = false
    public var defaultContentOffset = CGPoint.zero
    public var closedHandler: ((PopupController) -> Void)?
    public var showedHandler: ((PopupController) -> Void)?
    
    
    public var maximumSize: CGSize {
        get {
            return CGSize(width: UIScreen.main.bounds.size.width - margin * 2, height: UIScreen.main.bounds.size.height - margin * 2)
        }
    }
    
    deinit {
        self.removeFromParentViewController()
    }
    
    // MARK: Overrides
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        registerNotification()
    }
    
    public override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        unregisterNotification()
    }
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateLayouts()
    }
    
 }
 
 // MARK: - Publics
 public extension PopupController {
    
    // MARK: Classes
    public class func create(parentViewController: UIViewController) -> PopupController {
        let controller = PopupController()
        controller.defaultConfigure()
        
        parentViewController.addChildViewController(controller)
        parentViewController.view.addSubview(controller.view)
        controller.didMove(toParentViewController: parentViewController)
        
        return controller
    }
    
    public func customize(options: [PopupCustomOption]) -> PopupController {
        customOptions(options: options)
        return self
    }
    
    public func show(childViewController: UIViewController) -> PopupController {
        self.addChildViewController(childViewController)
        popupView = childViewController.view
        configure()
        
        childViewController.didMove(toParentViewController: self)
        
        show(layout: layout, animation: animation) { _ in
            self.defaultContentOffset = self.baseScrollView.contentOffset
            self.showedHandler?(self)
        }
        
        return self
    }
    
    public func didShowHandler(handler: @escaping (PopupController) -> Void) -> PopupController {
        self.showedHandler = handler
        return self
    }
    
    public func getParam(handler: @escaping (PopupController) -> Void) -> PopupController {
        self.showedHandler = handler
        return self
    }
    
    public func didCloseHandler(handler: @escaping (PopupController) -> Void) -> PopupController {
        self.closedHandler = handler
        return self
    }
    
    public func dismiss(completion: (() -> Void)? = nil) {
        if isShowingKeyboard {
            popupView.endEditing(true)
        }
        self.closePopup(completion: completion)
    }
 }
 
 // MARK: Privates
 private extension PopupController {
    
    func defaultConfigure() {
        scrollable = true
        dismissWhenTaps = true
        backgroundStyle = .BlackFilter(alpha: 0.4)
    }
    
    func configure() {
        view.isHidden = true
        view.frame = UIScreen.main.bounds
        
        baseScrollView.frame = view.frame
        view.addSubview(baseScrollView)
        
        popupView.layer.cornerRadius = 10
        popupView.layer.masksToBounds = true
        popupView.frame.origin.y = 0
        baseScrollView.addSubview(popupView)
    }
    
    func registerNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(PopupController.popupControllerWillShowKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PopupController.popupControllerWillHideKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PopupController.popupControllerDidHideKeyboard(_:)), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
    }
    
    func unregisterNotification() {
        NotificationCenter.default.removeObserver(self)
    }
    
    func customOptions(options: [PopupCustomOption]) {
        for option in options {
            switch option {
            case .Layout(let layout):
                self.layout = layout
            case .Animation(let animation):
                self.animation = animation
            case .BackgroundStyle(let style):
                self.backgroundStyle = style
            case .Scrollable(let scrollable):
                self.scrollable = scrollable
            case .DismissWhenTaps(let dismiss):
                self.dismissWhenTaps = dismiss
            case .MovesAlongWithKeyboard(let moves):
                self.movesAlongWithKeyboard = moves
            }
        }
    }
    
    func registerTapGesture() {
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PopupController.didTapGesture(_:)))
        gestureRecognizer.delegate = self
        baseScrollView.addGestureRecognizer(gestureRecognizer)
    }
    
    func updateLayouts() {
        guard let child = self.childViewControllers.last as? PopupContentViewController else { return }
        popupView.frame.size = child.sizeForPopup(popupController: self, size: maximumSize, showingKeyboard: isShowingKeyboard)
        popupView.frame.origin.x = layout.origin(view: popupView).x
        popupView.layer.cornerRadius=10
        popupView.layer.masksToBounds=true
        baseScrollView.frame = view.frame
        baseScrollView.contentInset.top = layout.origin(view: popupView).y
        defaultContentOffset.y = -baseScrollView.contentInset.top
    }
    
    func updateBackgroundStyle(style: PopupBackgroundStyle) {
        switch style {
        case .BlackFilter(let alpha):
            baseScrollView.backgroundColor = UIColor.black.withAlphaComponent(alpha)
        }
    }
    
    func updateScrollable() {
        if scrollable {
            baseScrollView.isScrollEnabled = scrollable
            baseScrollView.alwaysBounceHorizontal = false
            baseScrollView.alwaysBounceVertical = true
            baseScrollView.delegate = self
        }
    }
    
    @objc func popupControllerWillShowKeyboard(_ notification: NSNotification) {
        isShowingKeyboard = true
        let obj = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as! NSValue
        
        if needsToMoveFrom(origin: obj.cgRectValue.origin) {
            move(origin: obj.cgRectValue.origin)
        }
    }
    
    @objc func popupControllerWillHideKeyboard(_ notification: NSNotification) {
        back()
    }
    
    @objc func popupControllerDidHideKeyboard(_ notification: NSNotification) {
        self.isShowingKeyboard = false
    }
    
    // Tap Gesture
    @objc func didTapGesture(_ sender: UITapGestureRecognizer) {
        self.closePopup { _ in }
    }
    
    func closePopup(completion: (() -> Void)?) {
        hide(animation: animation) { _ in
            completion?()
            self.didClosePopup()
        }
    }
    
    func didClosePopup() {
        
        popupView.endEditing(true)
        popupView.removeFromSuperview()
        
        childViewControllers.forEach { $0.removeFromParentViewController() }
        
        view.isHidden = true
        self.closedHandler?(self)
        
        self.removeFromParentViewController()
    }
    
    func show(layout: PopupLayout, animation: PopupAnimation, completion: @escaping PopupAnimateCompletion) {
        guard let childViewController = childViewControllers.last as? PopupContentViewController else {
            return
        }
        
        popupView.frame.size = childViewController.sizeForPopup(popupController: self, size: maximumSize, showingKeyboard: isShowingKeyboard)
        popupView.frame.origin.x = layout.origin(view: popupView!).x
        
        switch animation {
        case .FadeIn:
            fadeIn(layout: layout, completion: { () -> Void in
                completion()
            })
        case .SlideUp:
            slideUp(layout: layout, completion: { () -> Void in
                completion()
            })
        }
    }
    
    func hide(animation: PopupAnimation, completion: @escaping PopupAnimateCompletion) {
        guard let child = childViewControllers.last as? PopupContentViewController else {
            return
        }
        
        popupView.frame.size = child.sizeForPopup(popupController: self, size: maximumSize, showingKeyboard: isShowingKeyboard)
        popupView.frame.origin.x = layout.origin(view: popupView).x
        
        switch animation {
        case .FadeIn:
            self.fadeOut(completion: { () -> Void in
                self.clean()
                completion()
            })
        case .SlideUp:
            self.slideOut(completion: { () -> Void in
                self.clean()
                completion()
            })
        }
        
    }
    
    func needsToMoveFrom(origin: CGPoint) -> Bool {
        guard movesAlongWithKeyboard else {
            return false
        }
        return (popupView.frame.maxY + layout.origin(view: popupView).y) > origin.y
    }
    
    func move(origin: CGPoint) {
        guard let child = childViewControllers.last as? PopupContentViewController else {
            return
        }
        popupView.frame.size = child.sizeForPopup(popupController: self, size: maximumSize, showingKeyboard: isShowingKeyboard)
        baseScrollView.contentInset.top = origin.y - popupView.frame.height
        baseScrollView.contentOffset.y = -baseScrollView.contentInset.top
        defaultContentOffset = baseScrollView.contentOffset
    }
    
    func back() {
        guard let child = childViewControllers.last as? PopupContentViewController else {
            return
        }
        popupView.frame.size = child.sizeForPopup(popupController: self, size: maximumSize, showingKeyboard: isShowingKeyboard)
        baseScrollView.contentInset.top = layout.origin(view: popupView).y
        defaultContentOffset.y = -baseScrollView.contentInset.top
    }
    
    func clean() {
        popupView.endEditing(true)
        popupView.removeFromSuperview()
        baseScrollView.removeFromSuperview()
    }
    
 }
 
 // MARK: Animations
 private extension PopupController {
    
    func fadeIn(layout: PopupLayout, completion: @escaping () -> Void) {
        baseScrollView.contentInset.top = layout.origin(view: popupView).y
        
        view.isHidden = false
        popupView.alpha = 0.0
        popupView.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        baseScrollView.alpha = 0.0
        
        UIView.animate(withDuration: 0.3, delay: 0.1, options: .curveEaseInOut, animations: { () -> Void in  //CurveEaseInOut
            self.popupView.alpha = 1.0
            self.baseScrollView.alpha = 1.0
            self.popupView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }) { (finished) -> Void in
            completion()
        }
    }
    
    func slideUp(layout: PopupLayout, completion: @escaping () -> Void) {
        view.isHidden = false
        baseScrollView.backgroundColor = UIColor.clear
        baseScrollView.contentInset.top = layout.origin(view: popupView).y
        baseScrollView.contentOffset.y = -UIScreen.main.bounds.height
        
        UIView.animate(
            withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0.0, options: .curveLinear, animations: { () -> Void in
                
                self.updateBackgroundStyle(style: self.backgroundStyle)
                self.baseScrollView.contentOffset.y = -layout.origin(view: self.popupView).y
                self.defaultContentOffset = self.baseScrollView.contentOffset
        }, completion: { (isFinished) -> Void in
            completion()
        })
    }
    
    func fadeOut(completion: @escaping () -> Void) {
        
        UIView.animate(
            withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: { () -> Void in //CurveEaseInOut
                self.popupView.alpha = 0.0
                self.baseScrollView.alpha = 0.0
                self.popupView.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        }) { (finished) -> Void in
            completion()
        }
    }
    
    func slideOut(completion: @escaping () -> Void) {
        
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 0.0, options: .curveLinear, animations: { () -> Void in
            self.popupView.frame.origin.y = UIScreen.main.bounds.height
            self.baseScrollView.alpha = 0.0
        }, completion: { (isFinished) -> Void in
            completion()
        })
    }
 }
 
 // MARK: UIScrollViewDelegate methods
 extension PopupController: UIScrollViewDelegate {
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let delta: CGFloat = defaultContentOffset.y - scrollView.contentOffset.y
        if delta > 20 && isShowingKeyboard {
            popupView.endEditing(true)
            return
        }
    }
    
    public func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let delta: CGFloat = defaultContentOffset.y - scrollView.contentOffset.y
        if delta > 50 {
            baseScrollView.contentInset.top = -scrollView.contentOffset.y
            animation = .SlideUp
            self.closePopup { _ in }
        }
    }
    
 }
 
 extension PopupController: UIGestureRecognizerDelegate {
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return gestureRecognizer.view == touch.view
    }
 }
 
 extension UIViewController {
    func popupController() -> PopupController? {
        var parent = self.parent
        while !(parent is PopupController || parent == nil) {
            parent = parent!.parent
        }
        return parent as? PopupController
    }
 }
