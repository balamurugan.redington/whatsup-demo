//
//  MobileViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 26/04/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class MobileViewController: UIViewController,PopupContentViewController,UITextFieldDelegate
{

    @IBOutlet weak var phnumbertf: UITextField!
    var Movedup=Bool()
    var uploadfile=NSMutableDictionary()
    var jsonresult=NSDictionary()
    var AppVersion : String!
    var StoreVersion  :String!
    var Phonenumber=Bool()
    var closeHandler: (() -> Void)?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
            AppVersion = ""
            StoreVersion  = ""
            Phonenumber  = false
            phnumbertf.delegate = self
            phnumbertf.keyboardType = .numberPad
            phnumbertf.tag = 1
        self.addDoneButtonOnKeyboard()
        NotificationCenter.default.addObserver(self, selector: #selector(MobileViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(MobileViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
           phnumbertf.addTarget(self, action: #selector(MobileViewController.textFieldDidBeginEditing), for: UIControlEvents.editingChanged)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        let returnValue : AnyObject? = UserDefaults.standard.object(forKey: "Helpdesk_PhoneNumber") as AnyObject?
        if returnValue == nil
        {
            let returnValue1 : AnyObject? = UserDefaults.standard.object(forKey: "PhoneNumber") as AnyObject?
            if returnValue1 == nil {
                self.phnumbertf.text = ""
                let notification = UILocalNotification()
                notification.fireDate = NSDate.init(timeIntervalSinceNow: 5) as Date
                notification.alertTitle    = "Mobile Number is Empty!"
                notification.alertBody     = "Update the Mobile Number"
                notification.alertAction   = "open"
                notification.hasAction     = true
                UIApplication.shared.scheduleLocalNotification(notification)
                let badgeCount: Int = LibraryAPI.sharedInstance.NotificationArray.count + 1
                let application = UIApplication.shared
                application.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .alert, .sound], categories: nil))
                application.applicationIconBadgeNumber = badgeCount
            } else {
                self.phnumbertf.text = UserDefaults.standard.object(forKey: "PhoneNumber") as! String
                let badgeCount: Int = LibraryAPI.sharedInstance.NotificationArray.count
                let application = UIApplication.shared
                application.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .alert, .sound], categories: nil))
                application.applicationIconBadgeNumber = badgeCount
            }
        } else {
            self.phnumbertf.text = UserDefaults.standard.object(forKey: "PhoneNumber") as! String
            let badgeCount: Int = LibraryAPI.sharedInstance.NotificationArray.count
            let application = UIApplication.shared
            application.registerUserNotificationSettings(UIUserNotificationSettings(types:  [.badge, .alert, .sound], categories: nil))
            application.applicationIconBadgeNumber = badgeCount
        }
    }

    
    class func instance() -> MobileViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "MobileViewController") as! MobileViewController
    }
    
    
    @IBAction func CloseAction(_ sender: AnyObject) {
        closeHandler?()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
           if textField == phnumbertf {
            if(phnumbertf.text?.first == "0") {
                if(phnumbertf.text == "00") {
                    self.NormalAlert(title: "Alert", Messsage: "Please Enter the valid Phone number")
                    phnumbertf.text = ""
                } else {
                    Phonenumber  = true
                }
            } else {
                Phonenumber = false
            }
        }
    }



    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func sizeForPopup(popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        //return CGSizeMake(250,200)
        return CGSize(width: 250, height: 250)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        phnumbertf.resignFirstResponder()
        return true;
    }
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(MobileViewController.doneButtonAction))
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        doneToolbar.items = items as AnyObject as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        phnumbertf.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction() {
        phnumbertf.resignFirstResponder()
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true) {
        } else {
            if(Movedup==false) {
                self.view.frame.origin.y -= 150
                Movedup=true
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true) {
        } else {
            self.view.frame.origin.y = 0
            Movedup=false
        }
    }
    

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField.tag==1) {
            if(Phonenumber == true) {
                guard let text = textField.text else { return true }
                let newLength = text.count + string.count - range.length
                return newLength <= 11
            } else {
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 10
            }
     }
     return true
     }

    @IBAction func submit(_ sender: AnyObject)
    {
        if ((self.phnumbertf.text?.count)! < 10) {
            self.NormalAlert(title: "Alert", Messsage: "Please Enter the Valid PhoneNumber")
        } else if(self.phnumbertf.text == "") {
             self.NormalAlert(title: "Alert", Messsage: "Please Enter the PhoneNumber")
        } else {
            LibraryAPI.sharedInstance.push = 1
            UserDefaults.standard.set(self.phnumbertf.text!, forKey: "PhoneNumber")
            UserDefaults.standard.synchronize()
            popupController()?.dismiss()
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
        }
    }
}
