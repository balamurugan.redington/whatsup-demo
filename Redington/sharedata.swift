//
//  sharedata.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 12/07/18.
//  Copyright © 2018 truetech. All rights reserved.
import UIKit
import Foundation
import Alamofire

class dashboardapidata{
   
    static let sharevariable = dashboardapidata()
    
    //chkin ******** ParsePiechartdata data save details
    
    var months1             = [String]()
    var pievalues           = [0.0]
    var PiechartPercentage  = NSNumber()
    var pieresponsestring   = NSMutableArray()
    
    //chkout ******** ParsePiechartdata data save details
    //chkin ******** Loadbreadthda data save details
    
    var breadthvaluearray   = NSMutableArray()
    var sbucode             = String()
    var BusinessCode        = String()
    var PreviousMonth       = Int()
    var CurrentMonth        = Int()
    var Flag                = String()
    var Breathmessage       = String()
    var Breathcount         = Int()
    
    //chkout ******** Loadbreadthda data save details
    
    //chkin ******** ParseBarchartdata data save details
    
    var responsestring2     = NSMutableArray()
    var Array30             = [Double]()
    var array60             = [Double]()
    var array90             = [Double]()
    var array120            = [Double]()
    var arraymorethan120    = [Double]()
    var monthsbar           = [String]()
    var monthvalue          =  Int()
    var year                =  Int()
    var typeasmoRSm         =  Int()
    var bartotalindex       =  Int()
    var months              = [String]()
    
    //chkout ******** ParseBarchartdata data save details
    
    //chkin customer details
    
    var brnchcode           = [String]()
    var brnchname           = [String]()
    
    //chkout customer details
    
    var responseArray       = NSMutableArray()
    var responsestring3     = NSMutableArray()
    
    //chkin userdetails
    
    var EmployeeName        = String()
    var EmployeeCode        = String()
    
    //chkout userdetails
    
    // chkin map
    var latitude            : Double!
    var longitude           : Double!
    
    //chkout map
    var responsestring      : NSMutableDictionary!
    var current_area        : String!
    var current_pin         : String!
    
    var LoadingValue        = 0 // start Loading Process
}


class CustomColor{
    static let ShareColor = CustomColor()
    
    let CustomGreen         = "#1EB878"  // static Green for Redington
    let customGray          = "#D3D3D3"  // Border gray for text field
    let customGradientBlue  = "#0284A8"  // Used for gradient Color
    let customBlack         = "#000000"  // Black Color
    let ticktissueCel1Color = "#cbdbd7"  // Ticketissue table cell color 1
    let ticktissueCel2Color = "#fcfff5"  // Ticketissue table cell color 2
    let graphbrown          = "#CFDBB2"  // Graph border Color
    let customNavyBlue      = "#3E606F"  // Ticket issue color Blue
    
    // custom color picker
    
    func hexStringToUIColor (hex:String,alpha: CGFloat) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if (cString.hasPrefix("#")) {
            cString = cString.substring(from: cString.startIndex.advanced(by: 1))
        }
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: alpha
        )
    }
    
    // custom color picker
}

class sharedata{
    static let shared = sharedata()
    
    let createAccountErrorAlert: UIAlertView = UIAlertView()
    
    func alert() {
        createAccountErrorAlert.delegate = self
        createAccountErrorAlert.title = "Oops"
        createAccountErrorAlert.message = "connection Failed / Time Out"
        createAccountErrorAlert.addButton(withTitle: "ok")
        createAccountErrorAlert.show()
    }
    
    func alertView(_ View: UIAlertView, clickedButtonAt buttonIndex: Int) {
        switch buttonIndex {
        case 0:
            createAccountErrorAlert.dismiss(withClickedButtonIndex: 1, animated: true)
        case 1:
            createAccountErrorAlert.dismiss(withClickedButtonIndex: 1, animated: true)
        default:
            NSLog("Default");
        }
    }
}





