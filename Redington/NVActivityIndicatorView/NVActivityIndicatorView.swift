

import UIKit


public enum NVActivityIndicatorType: Int {
    
    case blank
    
    case ballPulse
    
    case ballGridPulse
    
    case ballClipRotate
    
    case squareSpin
    
    case ballClipRotatePulse
    
    case ballClipRotateMultiple
    
    case ballPulseRise
    
    case ballRotate
    
    case cubeTransition
    
    case ballZigZag
    
    case ballZigZagDeflect
    
    case ballTrianglePath
    
    case ballScale
    
    case lineScale
    
    case lineScaleParty
    
    case ballScaleMultiple
   
    case ballPulseSync
   
    case ballBeat
    
    case lineScalePulseOut
   
    case lineScalePulseOutRapid
    
    case ballScaleRipple
    
    case ballScaleRippleMultiple
    
    case ballSpinFadeLoader
    
    case lineSpinFadeLoader
    
    case triangleSkewSpin
    
    case pacman
    
    case ballGridBeat
   
    case semiCircleSpin
    
    case ballRotateChase

    case orbit
    
    case audioEqualizer

    static let allTypes = (blank.rawValue ... audioEqualizer.rawValue).map { NVActivityIndicatorType(rawValue: $0)! }

    func animation() -> NVActivityIndicatorAnimationDelegate {
        switch self {
        case .blank:
            return NVActivityIndicatorAnimationBallSpinFadeLoader()
        case .ballPulse:
            return NVActivityIndicatorAnimationBallGridPulse()
        case .ballGridPulse:
            return NVActivityIndicatorAnimationBallSpinFadeLoader()
        case .ballClipRotate:
            return NVActivityIndicatorAnimationBallSpinFadeLoader()
        case .squareSpin:
            return NVActivityIndicatorAnimationBallSpinFadeLoader()
        case .ballClipRotatePulse:
            return NVActivityIndicatorAnimationBallSpinFadeLoader()
        case .ballClipRotateMultiple:
            return NVActivityIndicatorAnimationBallSpinFadeLoader()
        case .ballPulseRise:
            return NVActivityIndicatorAnimationBallSpinFadeLoader()
        case .ballRotate:
            return NVActivityIndicatorAnimationBallSpinFadeLoader()
        case .cubeTransition:
            return NVActivityIndicatorAnimationBallSpinFadeLoader()
        case .ballZigZag:
            return NVActivityIndicatorAnimationBallSpinFadeLoader()
        case .ballZigZagDeflect:
            return NVActivityIndicatorAnimationBallSpinFadeLoader()
        case .ballTrianglePath:
            return NVActivityIndicatorAnimationBallSpinFadeLoader()
        case .ballScale:
            return NVActivityIndicatorAnimationBallSpinFadeLoader()
        case .lineScale:
            return NVActivityIndicatorAnimationBallSpinFadeLoader()
        case .lineScaleParty:
            return NVActivityIndicatorAnimationBallSpinFadeLoader()
        case .ballScaleMultiple:
            return NVActivityIndicatorAnimationBallSpinFadeLoader()
        case .ballPulseSync:
            return NVActivityIndicatorAnimationBallSpinFadeLoader()
        case .ballBeat:
            return NVActivityIndicatorAnimationBallSpinFadeLoader()
        case .lineScalePulseOut:
            return NVActivityIndicatorAnimationBallSpinFadeLoader()
        case .lineScalePulseOutRapid:
            return NVActivityIndicatorAnimationBallSpinFadeLoader()
        case .ballScaleRipple:
            return NVActivityIndicatorAnimationBallSpinFadeLoader()
        case .ballScaleRippleMultiple:
            return NVActivityIndicatorAnimationBallSpinFadeLoader()
        case .ballSpinFadeLoader:
            return NVActivityIndicatorAnimationBallSpinFadeLoader()
        case .lineSpinFadeLoader:
            return NVActivityIndicatorAnimationBallSpinFadeLoader()
        case .triangleSkewSpin:
            return NVActivityIndicatorAnimationBallSpinFadeLoader()
        case .pacman:
            return NVActivityIndicatorAnimationBallSpinFadeLoader()
        case .ballGridBeat:
            return NVActivityIndicatorAnimationBallSpinFadeLoader()
        case .semiCircleSpin:
            return NVActivityIndicatorAnimationBallSpinFadeLoader()
        case .ballRotateChase:
            return NVActivityIndicatorAnimationBallSpinFadeLoader()
        case .orbit:
            return NVActivityIndicatorAnimationBallSpinFadeLoader()
        case .audioEqualizer:
            return NVActivityIndicatorAnimationBallSpinFadeLoader()
        }
    }
}

/// Activity indicator view with nice animations
public final class NVActivityIndicatorView: UIView {
    /// Default type. Default value is .BallSpinFadeLoader.
    public static var DEFAULT_TYPE: NVActivityIndicatorType = .ballSpinFadeLoader

    /// Default color of activity indicator. Default value is UIColor.white.
    public static var DEFAULT_COLOR = UIColor.white

    /// Default color of text. Default value is UIColor.white.
    public static var DEFAULT_TEXT_COLOR = UIColor.white

    /// Default padding. Default value is 0.
    public static var DEFAULT_PADDING: CGFloat = 0

    /// Default size of activity indicator view in UI blocker. Default value is 60x60.
    public static var DEFAULT_BLOCKER_SIZE = CGSize(width: 60, height: 60)

    /// Default display time threshold to actually display UI blocker. Default value is 0 ms.
    public static var DEFAULT_BLOCKER_DISPLAY_TIME_THRESHOLD = 0

    /// Default minimum display time of UI blocker. Default value is 0 ms.
    public static var DEFAULT_BLOCKER_MINIMUM_DISPLAY_TIME = 0

    /// Default message displayed in UI blocker. Default value is nil.
    public static var DEFAULT_BLOCKER_MESSAGE: String?

    /// Default font of message displayed in UI blocker. Default value is bold system font, size 20.
    public static var DEFAULT_BLOCKER_MESSAGE_FONT = UIFont.boldSystemFont(ofSize: 20)

    /// Default background color of UI blocker. Default value is UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
    public static var DEFAULT_BLOCKER_BACKGROUND_COLOR = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
   // public static var DEFAULT_BLOCKER_BACKGROUND_COLOR = UIColor(red: 155, green: 20, blue: 10, alpha: 0.5)
    /// Animation type.
    public var type: NVActivityIndicatorType = NVActivityIndicatorView.DEFAULT_TYPE

    @available(*, unavailable, message: "This property is reserved for Interface Builder. Use 'type' instead.")
    @IBInspectable var typeName: String {
        get {
            return getTypeName()
        }
        set {
            _setTypeName(newValue)
        }
    }

    /// Color of activity indicator view.
    @IBInspectable public var color: UIColor = NVActivityIndicatorView.DEFAULT_COLOR

    /// Padding of activity indicator view.
    @IBInspectable public var padding: CGFloat = NVActivityIndicatorView.DEFAULT_PADDING

    /// Current status of animation, read-only.
    @available(*, deprecated: 3.1)
    public var animating: Bool { return isAnimating }

    /// Current status of animation, read-only.
    private(set) public var isAnimating: Bool = false

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        backgroundColor = UIColor.clear
        isHidden = true
    }

    public init(frame: CGRect, type: NVActivityIndicatorType? = nil, color: UIColor? = nil, padding: CGFloat? = nil) {
        self.type = type ?? NVActivityIndicatorView.DEFAULT_TYPE
        self.color = color ?? NVActivityIndicatorView.DEFAULT_COLOR
        self.padding = padding ?? NVActivityIndicatorView.DEFAULT_PADDING
        super.init(frame: frame)
        isHidden = true
    }

    public override var intrinsicContentSize: CGSize {
        return CGSize(width: bounds.width, height: bounds.height)
    }

    /**
     Start animating.
     */
    public final func startAnimating() {
        isHidden = false
        isAnimating = true
        layer.speed = 1
        setUpAnimation()
    }

    /**
     Stop animating.
     */
    public final func stopAnimating() {
        isHidden = true
        isAnimating = false
        layer.sublayers?.removeAll()
    }

    // MARK: Internal

    func _setTypeName(_ typeName: String) {
        for item in NVActivityIndicatorType.allTypes {
            if String(describing: item).caseInsensitiveCompare(typeName) == ComparisonResult.orderedSame {
                type = item
                break
            }
        }
    }

    func getTypeName() -> String {
        return String(describing: type)
    }

    // MARK: Privates

    private final func setUpAnimation() {
        let animation: NVActivityIndicatorAnimationDelegate = type.animation()
        var animationRect = UIEdgeInsetsInsetRect(frame, UIEdgeInsetsMake(padding, padding, padding, padding))
        let minEdge = min(animationRect.width, animationRect.height)

        layer.sublayers = nil
        animationRect.size = CGSize(width: minEdge, height: minEdge)
        animation.setUpAnimation(in: layer, size: animationRect.size, color: color)
    }
}
