
//
//  ViewController.swift
//  Redington
//Tabbars
//  Created by truetech on 04/08/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
import FillableLoaders
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import Alamofire


class ViewController: UIViewController, UITextFieldDelegate,UITextViewDelegate {
    
    @IBOutlet var Activityview: UIActivityIndicatorView!
    @IBOutlet var LoginImage: UIImageView!
    @IBOutlet var textSlider: UITextView!
    @IBOutlet var SliderView: UIView!
    @IBOutlet var LoginView: UIView!
    @IBOutlet var RememberMebtn: UIButton!
    @IBOutlet var LoginAction: UIButton!
    @IBOutlet weak var TextfieldPassword: UITextField!
    @IBOutlet weak var TextfieldEmail: UITextField!
    @IBOutlet var Pagecontrol: UIPageControl!
    @IBOutlet weak var Permissionview: UIView!
    @IBOutlet weak var imageoutview: UIView!
    @IBOutlet weak var pagecontrol2: UIPageControl!
    @IBOutlet weak var textview1: UITextView!
    
    var firstLogo: Bool = false
    var LoginWidth: NSLayoutConstraint!
    var LoginHeight: NSLayoutConstraint!
    let arr = NSMutableArray()
    var buttonframe=CGRect()
    var timer = Timer()
    var count = NSInteger()
    var color=UIColor()
    var Movedup=Bool()
    var constant=CGFloat()
    var LoginHeightPotrait=CGFloat()
    var LoginHeightLandscape=CGFloat()
    var LoginLandscapeconstant=CGFloat()
    var loginimagetop=CGFloat()
    var loginimagelandscape=CGFloat()
    var tickcount=NSInteger();
    var uploadfile = NSMutableDictionary()
    var uploadfile1=NSMutableDictionary()
    var jsonresult=NSArray()
    var jsonresult1 = NSDictionary()
    var AppVersion : String!
    var StoreVersion  :String!
    var response = String()
    var arr1 = NSMutableArray()
    var timer1 = Timer()
    var count1 = NSInteger()
    var loadercount = [""]
    var loaderval = 0
    
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: Bundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        //Global declaration in view
        count = 0
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        //chkin get the calender details
        LibraryAPI.sharedInstance.fetchdate()
        //chkin get the calender details
    }
    

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)!
    }
    
  /* func path() -> CGPath{
        
        return firstLogo ? Paths.twitterPath() : Paths.githubPath()
        
    }*/
    
    
    override func viewWillAppear(_ animated: Bool) {
        LibraryAPI.sharedInstance.NotificationDashboardCheck = 0
        if LibraryAPI.sharedInstance.LoginCredentials.object(forKey: "IsClicked") != nil {
            LibraryAPI.sharedInstance.isloggedout=false
        } else {
            LibraryAPI.sharedInstance.isloggedout=true
        }
        
        if LibraryAPI.sharedInstance.isloggedout == true {
            tickcount=0
        } else {
            tickcount=1
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // one time process while login as first after install
        
        AppVersion = ""
        StoreVersion = ""
        
        /*chkin**********  value is already set on root View Controller*/
        
        let returnValue = UserDefaults.standard.object(forKey: "PermissionScreen") as? String
        if returnValue == "A" {
            Permissionview.isHidden = false
            UserDefaults.standard.set("B", forKey: "PermissionScreen")
            UserDefaults.standard.synchronize()
        } else {
            Permissionview.isHidden = true
        }
        
        self.Activityview.isHidden=true
        self.Activityview.frame.origin.x=self.view.center.x-10
        self.Activityview.frame.origin.y=self.view.center.y
        
        // activity loading procress
        // UIDevice.currentDevice().setValue(UIInterfaceOrientation.Portrait.rawValue, forKey: "orientation")
        // process for Remember BTN
       // print("value for login credentials for clciked button :",LibraryAPI.sharedInstance.LoginCredentials.object(forKey: "IsClicked"))
       // print("value in user details as :",LibraryAPI.sharedInstance.emailcredential.object(forKey: "email") as? String,LibraryAPI.sharedInstance.Passcodeceredentials.object(forKey: "Passcode") as? String)
        if LibraryAPI.sharedInstance.LoginCredentials.object(forKey: "IsClicked") != nil {
            self.TextfieldEmail.text = LibraryAPI.sharedInstance.emailcredential.object(forKey: "email") as? String
            self.TextfieldPassword.text = LibraryAPI.sharedInstance.Passcodeceredentials.object(forKey: "Passcode") as? String
            self.RememberMebtn.setImage(UIImage(named:"chk_tick.png"), for: UIControlState.normal)
            self.count = 1
        } else {
            self.RememberMebtn.setImage(UIImage(named:"chk_untick.png"), for: UIControlState.normal)
            self.count=0;
        }
        
        // process for Remember BTN
        
        Movedup=false
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillResignActive), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
        
        //Loading text
        
        // Running TEXT
        
        arr.insert("Redington is a leading supply chain solutions provider for global brands of IT hardware and software products.", at: 0)
        arr.insert("Our highly advanced and comprehensive supply chain solutions enable organizations to create customer experiences consistent with their brand values and meet dynamically changing market requirements.", at: 1)
        arr.insert("Redington, commencing its Indian operations in 1993, is today positioned as the largest Supply Chain Solution Provider in emerging markets.", at: 2)
        arr.insert("As a group, Redington is present in India, Middle East,Africa, Turkey, Srilanka, Bangladesh and CIS countries.", at: 3)
        
        timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(ViewController.setCurrentPageLabel), userInfo: nil, repeats: true)
        LoginView.layer.shadowColor = CustomColor.ShareColor.hexStringToUIColor(hex: CustomColor.ShareColor.CustomGreen, alpha: 1.0).cgColor
        LoginView.layer.shadowOpacity = 0.6
        LoginView.layer.shadowOffset = CGSize.zero
        LoginView.layer.shadowRadius = 6
        LoginView.layer.rasterizationScale = UIScreen.main.scale
        LoginView.layer.shouldRasterize = true
        LoginView.layer.cornerRadius = 5;
        LoginView.layer.masksToBounds = false;
        LoginAction.layer.cornerRadius = 15;
        LoginAction.layer.masksToBounds = true;
        TextfieldEmail.attributedPlaceholder = NSAttributedString(string:"Userid",
                                                                  attributes:[NSForegroundColorAttributeName:  UIColor.lightGray])
        TextfieldPassword.attributedPlaceholder = NSAttributedString(string:"*******",
                                                                     attributes:[NSForegroundColorAttributeName:  UIColor.lightGray])
        buttonframe=LoginAction.frame
        NotificationCenter.default.addObserver(self, selector: #selector(self.getnotificationmethod), name: NSNotification.Name(rawValue: "GetNotification"), object: nil)
        
        // login process view
        
        //permission view
        
        imageoutview.layer.cornerRadius = imageoutview.frame.size.width/2
        imageoutview.clipsToBounds = true
        arr1.insert("Allow to access your device location for login credentials.", at: 0)
        arr1.insert("Enable camera permission to take a picture of preorder  and helpdesk.", at: 1)
        arr1.insert("Allow to Store your this Application information and Picture in device for  backend Process.", at: 2)
        arr1.insert("To Manage  this device id for your identify to enable successfully login. ", at: 3)
        timer1 = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(ViewController.setCurrentPageLabel1), userInfo: nil, repeats: true)
        
        //permission view
    }
    
    // loading view  process
  
    func setCurrentPageLabel1() {
        pagecontrol2.isHidden = false
        if count1 <= arr1.count {
            self.animate2(Str: textview1)
            self.textview1.text = arr1.object(at: count1) as! String
            pagecontrol2.currentPage=count1
        }
        count1=count1+1
        if count1 >= arr1.count {
            count1=0;
            pagecontrol2.currentPage=count+3
        }
    }
    
    //permission view (label visible porcess)
    
    //permission view (label visible porcess)
    
    func animate2(Str:AnyObject) {
        let animation2 = CATransition()
        animation2.duration = 1.0
        animation2.type = kCATransitionPush
        animation2.subtype = kCATransitionFromRight
        animation2.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        Str.layer.add(animation2, forKey: "SwitchToView")
        Str.layer.add(animation2, forKey: "SwitchToView")
    }
    
    //permission view (label visible porcess)
    
    // proceed view (one time process)
    
    
    @IBAction func proceedbtnAction(_ sender: Any) {
        Permissionview.isHidden = true
        timer1.invalidate()
    }
    
    dynamic private func applicationWillResignActive() {
        self.TextfieldPassword.resignFirstResponder()
        self.TextfieldEmail.resignFirstResponder()
        LibraryAPI.sharedInstance.internetcheck()
    }
    
    
    func keyboardWillShow(notification: NSNotification) {
        if LibraryAPI.sharedInstance.poptextfieldclicked == true {
        }
        else {
            if Movedup == false {
                self.LoginView.frame.origin.y = (self.view.frame.size.height/3.60)
                Movedup=true
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        if LibraryAPI.sharedInstance.poptextfieldclicked == true {
        } else {
            self.LoginView.frame.origin.y = self.view.frame.size.height-(self.view.frame.size.height/2.5)
            Movedup=false
        }
    }
    
    func setCurrentPageLabel() {
        Pagecontrol.isHidden=false;
        if count <= arr.count {
            self.animate(Str: textSlider)
            self.textSlider.text = arr.object(at: count) as! String
            Pagecontrol.currentPage=count;
        }
        count=count+1
        if count >= arr.count {
            count=0;
            Pagecontrol.currentPage=count+3;
        }
    }
    
    func animate(Str:AnyObject) {
        let animation1 = CATransition()
        animation1.duration = 1.0
        animation1.type = kCATransitionPush
        animation1.subtype = kCATransitionFromRight
        animation1.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        Str.layer.add(animation1, forKey: "SwitchToView")
        Str.layer.add(animation1, forKey: "SwitchToView")
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func RememberMeFunction(_ sender: Any) {
        if tickcount == 0 {
            RememberMebtn.setImage(UIImage(named:"chk_tick.png"), for: UIControlState.normal)
            tickcount=1;
            if TextfieldEmail.text! == "" || TextfieldPassword.text! == "" {
                LibraryAPI.sharedInstance.LoginCredentials.set("Enables", forKey:"IsClicked")
                LibraryAPI.sharedInstance.emailcredential.set(TextfieldEmail.text, forKey: "email")
                LibraryAPI.sharedInstance.Passcodeceredentials.set(TextfieldPassword.text, forKey: "Passcode")
                LibraryAPI.sharedInstance.emailcredential.synchronize()
                LibraryAPI.sharedInstance.Passcodeceredentials.synchronize()
            } else {
                LibraryAPI.sharedInstance.LoginCredentials.set("Enables", forKey:"IsClicked")
                LibraryAPI.sharedInstance.emailcredential.set(TextfieldEmail.text, forKey: "email")
                LibraryAPI.sharedInstance.Passcodeceredentials.set(TextfieldPassword.text, forKey: "Passcode")
                LibraryAPI.sharedInstance.emailcredential.synchronize()
                LibraryAPI.sharedInstance.Passcodeceredentials.synchronize()
            }
        } else {
            tickcount=0;
            RememberMebtn.setImage(UIImage(named:"chk_untick.png"), for: UIControlState.normal)
            LibraryAPI.sharedInstance.emailcredential.removeObject(forKey: "email")
            LibraryAPI.sharedInstance.Passcodeceredentials.removeObject(forKey: "Passcode")
            LibraryAPI.sharedInstance.LoginCredentials.removeObject(forKey: "IsClicked")
            LibraryAPI.sharedInstance.emailcredential.synchronize()
            LibraryAPI.sharedInstance.Passcodeceredentials.synchronize()
        }
        
    }
    
    @IBAction func LoginAction(_ sender: Any) {
        TextfieldEmail.resignFirstResponder()
        TextfieldPassword.resignFirstResponder()
        if self.TextfieldEmail.text!.trimmingCharacters(in: .whitespaces) != "" {
            if TextfieldEmail.text! == "" || TextfieldPassword.text! == "" {      //chkin if text field empty
                let bounds = self.LoginAction.bounds
                UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 15, options:[] , animations:
                    {
                        self.LoginAction.bounds = CGRect(x: bounds.origin.x+13 , y: bounds.origin.y, width: bounds.size.width+13 , height: bounds.size.height)
                        self.LoginAction.isEnabled = true
                }, completion:
                    {(value: Bool) in
                        self.showalert(Title: "Sorry!", Message: "Username or Password is empty")
                })
                self.LoginAction.bounds = buttonframe
            } else {
                self.local_activityIndicator_start()
                DispatchQueue.global(qos: .background).async {
                    self.uploadfile1.setObject(self.TextfieldEmail.text!, forKey: "UserID" as NSCopying)
                    self.uploadfile1.setObject(self.TextfieldPassword.text!, forKey: "Password" as NSCopying)
                    self.LoginDetails()
                    DispatchQueue.main.async {
                        if self.jsonresult1.object(forKey: "Status") != nil {
                            self.response = self.jsonresult1.object(forKey: "Status")as! String
                            if self.response == "Y" {
                                let date = NSDate()
                                _ = NSCalendar.current
                                let formatter = DateFormatter()
                                formatter.dateFormat = "MM-dd-yyyy hh:mm:ss"
                                _ = formatter.string(from: date as Date)
                                LibraryAPI.sharedInstance.Userid=self.TextfieldEmail.text!
                                self.uploadfile.setObject(self.TextfieldEmail.text!.stringByRemovingWhitespaces, forKey: "UserID" as NSCopying)
                                self.uploadfile.setObject("IOS", forKey: "Source" as NSCopying)
                                let UUIDValue =  NSUUID().uuidString
                                self.uploadfile.setObject(UUIDValue, forKey: "IMEI_Number" as NSCopying)
                                let bundle = Bundle.main
                                let infoDictionary = bundle.infoDictionary
                                self.AppVersion = infoDictionary!["CFBundleShortVersionString"] as? String
                                self.uploadfile.setObject(self.AppVersion, forKey: "App_Version" as NSCopying)
                                if InstanceID.instanceID().token() != nil {
                                   // self.uploadfile.setObject(InstanceID.instanceID().token()!, forKey: "Deviceid" as NSCopying)
                                    self.uploadfile.setObject("", forKey: "Deviceid" as NSCopying)
                                } else {
                                    self.uploadfile.setObject("", forKey: "Deviceid" as NSCopying)
                                }
                                let returnValue : AnyObject? = UserDefaults.standard.object(forKey: "PhoneNumber") as AnyObject?
                                if returnValue == nil {
                                    self.uploadfile.setObject("", forKey: "Mobile" as NSCopying)
                                } else {
                                    self.uploadfile.setObject(returnValue!, forKey: "Mobile" as NSCopying)
                                    print("Mobile value inserted : ",self.uploadfile)
                                }
                                self.deviceuploaddata()
                                if self.jsonresult.count > 0 {
                                    let jsonRD = self.jsonresult[0] as! NSDictionary
                                    LibraryAPI.sharedInstance.Globalflag = jsonRD.object(forKey: "Flag") as! String
                                    LibraryAPI.sharedInstance.LiveUserName = jsonRD.object(forKey: "Employee Name")as! String
                                    LibraryAPI.sharedInstance.LiveUserCode = jsonRD.object(forKey: "Employee_Code")as! String
                                    _ = jsonRD.object(forKey: "Type") as! NSNumber
                                    if LibraryAPI.sharedInstance.Globalflag == "Y" {
                                        self.local_activityIndicator_stop()
                                        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
                                    } else if LibraryAPI.sharedInstance.Globalflag == "N" {
                                       self.local_activityIndicator_stop()
                                        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "HRD1DetailsViewController")), animated: true)
                                    } else {
                                        self.local_activityIndicator_stop()
                                        let alert = UIAlertController(title: "Alert", message: "You are not Authorized Person", preferredStyle: UIAlertControllerStyle.alert)
                                        alert.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.default, handler:
                                            { (ACTION :UIAlertAction!)in
                                        }))
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                }else{
                                    self.showalert(Title: "Oops", Message: "Server Connection Failed Try Again Later...!")
                                }  // chkout processing with second api value
                            } else if self.response == "N" {
                                self.local_activityIndicator_stop()
                                
                                let bounds = self.LoginAction.bounds
                                UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 15, options:[] , animations:
                                    {
                                        self.LoginAction.bounds = CGRect(x: bounds.origin.x+13 , y: bounds.origin.y, width: bounds.size.width+13 , height: bounds.size.height)
                                        self.LoginAction.isEnabled = true
                                }, completion:
                                    {(value: Bool) in
                                       self.showalert(Title: "oops!", Message: "Username or Password is Invalid")
                                })
                                self.LoginAction.bounds = self.buttonframe
                            } else {
                                self.local_activityIndicator_stop()
                                let bounds = self.LoginAction.bounds
                                UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 15, options:[] , animations:
                                    {
                                        self.LoginAction.bounds = CGRect(x: bounds.origin.x+13 , y: bounds.origin.y, width: bounds.size.width+13 , height: bounds.size.height)
                                        self.LoginAction.isEnabled = true
                                }, completion:
                                    {(value: Bool) in
                                        if String(self.response) != nil{
                                            self.showalert(Title: "oops!", Message: self.response )
                                        }else{
                                            self.showalert(Title: "oops!", Message: "There was a problem in connecting to the server. Please try again later..!" )
                                        }
                                })
                                self.LoginAction.bounds = self.buttonframe
                            }
                        } else {
                           self.local_activityIndicator_stop()
                            let bounds = self.LoginAction.bounds
                            UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 15, options:[] , animations:
                                {
                                    self.LoginAction.bounds = CGRect(x: bounds.origin.x+13 , y: bounds.origin.y, width: bounds.size.width+13 , height: bounds.size.height)
                                    self.LoginAction.isEnabled = true
                            }, completion:
                                {(value: Bool) in
                                    self.showalert(Title: "oops!", Message: "Could not connect to server. Please try after again" ) /*self.response*/
                            })
                            self.LoginAction.bounds = self.buttonframe
                        }
                        
                    }
                }
            }
        } else {
           self.local_activityIndicator_stop()
            let bounds = self.LoginAction.bounds
            UIView.animate(withDuration: 0.4, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 15, options:[] , animations:
                {
                    self.LoginAction.bounds = CGRect(x: bounds.origin.x+13 , y: bounds.origin.y, width: bounds.size.width+13 , height: bounds.size.height)
                    self.LoginAction.isEnabled = true
            }, completion:
                {(value: Bool) in
                    self.showalert(Title: "oops!", Message: "Username or Password is empty")
            }
            )
            self.LoginAction.bounds = buttonframe
            
        }
        
    }
    /*func LoaderSteup() {
        if dashboardapidata.sharevariable.LoadingValue == 0 {
            Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.LoaderSteup), userInfo: nil, repeats: false)
        }else{
            self.local_activityIndicator_stop()
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
        }
    }*/
    
    func showSuccessalert(Title:String,Message:String) {
        
        let alert = UIAlertController(title: Title, message: Message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Welcome", style: UIAlertActionStyle.default, handler:
            { (ACTION :UIAlertAction!)in
                self.timer.invalidate()
                let lvc=self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                self.navigationController?.pushViewController(lvc, animated: true)
                
                self.timer.invalidate()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showalert(Title:String,Message:String) {
        let alert = UIAlertController(title: Title, message: Message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:
            { (ACTION :UIAlertAction!)in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    //textfield Delegates

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if tickcount == 0 {
        } else {
            RememberMebtn.setImage(UIImage(named:"chk_untick.png"), for: UIControlState.normal)
            LibraryAPI.sharedInstance.emailcredential.removeObject(forKey: "email")
            LibraryAPI.sharedInstance.Passcodeceredentials.removeObject(forKey: "Passcode")
            LibraryAPI.sharedInstance.LoginCredentials.removeObject(forKey: "IsClicked")
            LibraryAPI.sharedInstance.emailcredential.synchronize()
            LibraryAPI.sharedInstance.Passcodeceredentials.synchronize()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == 1 {
            TextfieldEmail.resignFirstResponder()
        }
        
        if textField.tag == 2 {
            TextfieldPassword.resignFirstResponder()
        }
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 2 {
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 10 // Bool
        }
        
        if textField.tag == 1 {
            if string.rangeOfCharacter(from: .alphanumerics) != nil {
                guard let text = textField.text else { return true }
                let newLength = text.count + string.count - range.length
                return newLength  <= 10
                
            } else {
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
                return false
            }
        }
        return true
    }
    
    
    
    @IBAction func ForgotPasswordAction(_ sender: AnyObject)
    {
        
    }
    
    func deviceuploaddata() {
        let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)Users.asmx/UserDetails")! as URL)
        print("Request for Mobile update : \(request)")
        request.httpMethod = "POST"
        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
        request.httpBody = try? JSONSerialization.data(withJSONObject: self.uploadfile, options: JSONSerialization.WritingOptions(rawValue: 0))
        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
        do
        {
            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
            self.jsonresult = try JSONSerialization.jsonObject(with: data, options: []) as! NSArray
        }
        catch (let e)
        {
             print("error",e)
            self.local_activityIndicator_stop()
            showalert(Title: "oops", Message: "Connection Failed Try again later...!")
        }
        
    }
    
    func LoginDetails() {
        //try NSURLConnection.sendSynchronousRequest(request, returningResponse: &response)
        let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)Users.asmx/LoginDetails?")! as URL)
        request.httpMethod = "POST"
        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
        request.httpBody = try? JSONSerialization.data(withJSONObject: self.uploadfile1, options: JSONSerialization.WritingOptions(rawValue: 0))
        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
        var _: NSError
        do {
            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
            jsonresult1 = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            
        } catch (_) {
            print("Error")
            self.local_activityIndicator_stop()
            showalert(Title: "oops", Message: "Connection Failed Try again later...!")
        }
    }
    

    func notificationmethod() {
        let userdefaults = Foundation.UserDefaults.standard
        let value = userdefaults.dictionary(forKey: "Notification")
        var title: String!
        var message: String!
        var imagestr: String!
        title = value?["title"] as! String
        message = value?["body"] as! String
        imagestr = value?["imageurl"] as! String
        if imagestr != nil {
            if imagestr != "" {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: notificationimageViewController.instance())
                    
                    .didShowHandler { popup in
                        
                    }
                    .didCloseHandler { _ in
                        
                }
                let container = notificationimageViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                    
                }
                popup.show(childViewController: container)
            } else {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: PushnotificationtextViewController.instance())
                    
                    .didShowHandler { popup in
                        
                    }
                    .didCloseHandler { _ in
                        
                }
                let container = PushnotificationtextViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                }
                
                popup.show(childViewController: container)
            }
        } else {
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: PushnotificationtextViewController.instance())
                
                .didShowHandler { popup in
                    
                }
                .didCloseHandler { _ in
                    
            }
            let container = PushnotificationtextViewController.instance()
            container.closeHandler = { _ in
                popup.dismiss()
            }
            
            popup.show(childViewController: container)
        }
    }
}
extension UIViewController{
    func calldashboarddata(){
        DispatchQueue.global(qos: .background).async {
            dashboardapidata.sharevariable.LoadingValue = 0
            self.ParseBarchartdata()
            self.Loadbreadthda()
            self.ParsePiechartdata()
            DispatchQueue.main.async {
            }
        }
    }
    func refereshapidata(){
        TargetService.shared.get()
        TargetService.shared.getTargetDetails()
        TargetService.shared.ParseBarsget()
    }
    
    // chkout call all the api for refersh purpose
    //chkin call 1st api in dashboard
    
    func Loadbreadthda()
    {
        getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/BreathDetails?UserID=\(LibraryAPI.sharedInstance.Userid)&Branch=\(LibraryAPI.sharedInstance.BranchCodeNew)&Biz=") { (breadthvalue) in
            dashboardapidata.sharevariable.breadthvaluearray = breadthvalue as! NSMutableArray
            //print("value in Breath:",breadthvalue)
            
            if(dashboardapidata.sharevariable.breadthvaluearray.count > 0)
            {
                //******** check when data avail
                //chkin**********31/07/2018 coredateProcess
                TargetService.shared.deleteAllData(entity: TargetService.shared.EntityBreath)
                let dicBreath = dashboardapidata.sharevariable.breadthvaluearray[0] as! NSDictionary
                if(dicBreath.object(forKey: "Message") as? String == "No records found")
                {
                    dashboardapidata.sharevariable.BusinessCode = ""
                    dashboardapidata.sharevariable.CurrentMonth =  0
                    dashboardapidata.sharevariable.PreviousMonth = 0
                    dashboardapidata.sharevariable.sbucode = ""
                    dashboardapidata.sharevariable.Flag = ""
                    dashboardapidata.sharevariable.Breathmessage = dicBreath.object(forKey: "Message") as! String
                    dashboardapidata.sharevariable.Breathcount = 0
                    TargetService.shared.create(bizcode: "", currmth: 0, premth: 0, sbuco: "", flag: "", message:String(dicBreath.object(forKey: "Message") as! String),count: 0, entityname: TargetService.shared.EntityBreath)
                }
                else
                {
                    dashboardapidata.sharevariable.BusinessCode = (dicBreath.object(forKey: "Business Code") as! String?)!
                    dashboardapidata.sharevariable.CurrentMonth = (dicBreath.object(forKey: "Current Month") as! Int?)!
                    dashboardapidata.sharevariable.PreviousMonth = (dicBreath.object(forKey: "Previous Month") as! Int?)!
                    dashboardapidata.sharevariable.sbucode = (dicBreath.object(forKey: "SBU CODE") as! String?)!
                    dashboardapidata.sharevariable.Flag = (dicBreath.object(forKey: "Flag") as! String?)!
                    dashboardapidata.sharevariable.Breathmessage = ""
                    dashboardapidata.sharevariable.Breathcount = dashboardapidata.sharevariable.breadthvaluearray.count
                    TargetService.shared.create( bizcode: dashboardapidata.sharevariable.BusinessCode, currmth: dashboardapidata.sharevariable.CurrentMonth , premth: dashboardapidata.sharevariable.PreviousMonth, sbuco: dashboardapidata.sharevariable.sbucode, flag: dashboardapidata.sharevariable.Flag , message:"",count: dashboardapidata.sharevariable.Breathcount, entityname: TargetService.shared.EntityBreath)
                }
                //chkin**********31/07/2018 coredateProcess
                //******** check when data avail
                TargetService.shared.get()
            }
            else
            {
                TargetService.shared.get()
                print("No Data Found")
            }
        }
        
    }
    //chkout call 1st api in dashboard
    //chkin call 2nd api in dashboard
    
    func ParseBarchartdata()
    {
        getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)CreditDetails.asmx/CreditAgeingDetails?UserID=\(LibraryAPI.sharedInstance.Userid.stringByRemovingWhitespaces)&BranchCode=\(LibraryAPI.sharedInstance.BranchCodeNew)") { (json) in
            dashboardapidata.sharevariable.responsestring2 = json  as! NSMutableArray
            if( dashboardapidata.sharevariable.responsestring2.count>0)
            {
                let dicresLast = dashboardapidata.sharevariable.responsestring2.lastObject! as! NSDictionary
                dashboardapidata.sharevariable.Array30.append(((dicresLast.object(forKey: "0to30Ageing")! as AnyObject) as? Double)!)
                print("VAlue in array 30 ",dashboardapidata.sharevariable.Array30)
                dashboardapidata.sharevariable.array60.append(((dicresLast.object(forKey: "31to60Ageing")! as AnyObject) as? Double)!)
                dashboardapidata.sharevariable.array90.append(((dicresLast.object(forKey: "61to90Ageing")! as AnyObject) as? Double)!)
                dashboardapidata.sharevariable.array120.append(((dicresLast.object(forKey: "91to120Ageing")! as AnyObject) as? Double)!)
                dashboardapidata.sharevariable.arraymorethan120.append(((dicresLast.object(forKey: "Greaterthan120Ageing")! as AnyObject) as? Double)!)
                dashboardapidata.sharevariable.monthsbar.append((dicresLast.object(forKey: "MonthYear") as! String?)!)
                dashboardapidata.sharevariable.monthvalue      = (dicresLast.object(forKey: "Month") as! Int?)!
                dashboardapidata.sharevariable.typeasmoRSm     = (dicresLast.object(forKey: "Type ASMorRSM") as! Int?)!
                dashboardapidata.sharevariable.year            = (dicresLast.object(forKey: "Year") as! Int?)!
                dashboardapidata.sharevariable.bartotalindex   = dashboardapidata.sharevariable.responsestring2.count
                TargetService.shared.deleteAllData(entity: TargetService.shared.EntityBarChart)
                TargetService.shared.createBarChart(age0to30: dashboardapidata.sharevariable.Array30.last!, age30to60: dashboardapidata.sharevariable.array60.last!, age60to90: dashboardapidata.sharevariable.array90.last!, age90to120: dashboardapidata.sharevariable.array120.last!, greaterage120: dashboardapidata.sharevariable.arraymorethan120.last!, month: dashboardapidata.sharevariable.monthvalue , monthYear: dashboardapidata.sharevariable.monthsbar.last! , typeAsmoRSm: dashboardapidata.sharevariable.typeasmoRSm, Year:  dashboardapidata.sharevariable.year,TotalIndex:  dashboardapidata.sharevariable.bartotalindex, entityname: TargetService.shared.EntityBarChart)
                //chkout *************31/07/18 coredateProcess
                dashboardapidata.sharevariable.LoadingValue = 1
                TargetService.shared.ParseBarsget()
            }
            else
            {
                TargetService.shared.ParseBarsget()
                print("No Data Available")
                dashboardapidata.sharevariable.bartotalindex = 0
                dashboardapidata.sharevariable.LoadingValue = 1
            }
        }
        
        // print("ParseBarchartdata reponse string value array30",dashboardapidata.sharevariable.Array30)
    }
    //chkout call 2nd api in dashboard
    
    //chkin call 3rd api in dashboard
    
    func ParsePiechartdata()
    {
        //chkinapicallmade
        getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)SalesDetails.asmx/CurrentFinancialYearBusinessWiseSalesDetails?UserID=\(LibraryAPI.sharedInstance.Userid)&BranchCode=\(LibraryAPI.sharedInstance.BranchCodeNew)&Type=1") { (pieresponse) in
            dashboardapidata.sharevariable.pieresponsestring = pieresponse as! NSMutableArray
            print("value in parsePie",pieresponse)
            if( dashboardapidata.sharevariable.pieresponsestring.count>0)
            {
                
                let dicpiec = dashboardapidata.sharevariable.pieresponsestring[0] as! NSDictionary
                dashboardapidata.sharevariable.PiechartPercentage = NSNumber(value: (dicpiec.object(forKey: "Percentage Achieved") as? Double!)!)
                LibraryAPI.sharedInstance.SalesAcheived = NSNumber(value: (dicpiec.object(forKey: "Sales Achieved") as? Double!)!)
                LibraryAPI.sharedInstance.SalesTarget =  NSNumber(value: (dicpiec.object(forKey: "Sales Target")as? Double!)!)
                TargetService.shared.deleteAllData(entity: TargetService.shared.EntityTarget)
                TargetService.shared.createTarget(PercentageAchieved: ( dicpiec.object(forKey: "Percentage Achieved") as? Double)!, SalesAchieved: (dicpiec.object(forKey: "Sales Achieved") as? Double)!, SalesTarget: (dicpiec.object(forKey: "Sales Target") as? Double)!, entityname: TargetService.shared.EntityTarget)
                //chkin old data
                //chkout old data
                if(dashboardapidata.sharevariable.PiechartPercentage.intValue<100)
                {
                    dashboardapidata.sharevariable.months1=["",""]
                    dashboardapidata.sharevariable.pievalues = [(dashboardapidata.sharevariable.PiechartPercentage.doubleValue),100-Double(dashboardapidata.sharevariable.PiechartPercentage)]
                    print("viewcall: ",dashboardapidata.sharevariable.PiechartPercentage,dashboardapidata.sharevariable.pievalues)
                }
                else
                {
                    dashboardapidata.sharevariable.months1=["",""]
                    dashboardapidata.sharevariable.pievalues = [100,0]
                    print("viewcall: ",dashboardapidata.sharevariable.PiechartPercentage,dashboardapidata.sharevariable.pievalues)
                }
                LibraryAPI.sharedInstance.pievalues=dashboardapidata.sharevariable.pievalues
                print("viewcall: ",dashboardapidata.sharevariable.PiechartPercentage,dashboardapidata.sharevariable.pievalues)
                dashboardapidata.sharevariable.months1=["",""]
                TargetService.shared.getTargetDetails()
            }
            else
            {
                dashboardapidata.sharevariable.LoadingValue = 1
                TargetService.shared.getTargetDetails()
                print("No data Available")
                
            }
        }
        
    }
}


extension UIApplication {
    class func topViewController(base: UIViewController? = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}
