//
//  InvoiceViewController.swift
//  Redington
//
//  Created by truetech on 19/08/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import KCFloatingActionButton

class InvoiceViewController: UIViewController ,KCFloatingActionButtonDelegate,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var titlelbl: UILabel!
    @IBOutlet var popupview: UIView!
    @IBOutlet var backbtn: UIButton!
    @IBOutlet var menubtn: UIButton!
    @IBOutlet var Invoicetableview: UITableView!
    var month = Int()
    var customercode=String()
    var Year = Int()
    var responsestring=NSMutableArray()
    var fab=KCFloatingActionButton()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        customnavigation()
        self.layoutFAB()
        self.popupview.isHidden=true
      
    }
    override func viewWillAppear(_ animated: Bool) {
        self.fetchvalues()
         self.popupview.isHidden=true
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
      func BackGroundRefresh(notification: NSNotification)
    {
    fetchvalues()
    }
    
    func fetchvalues()
    {
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
             self.Loaddata()
            DispatchQueue.main.async {
                self.Invoicetableview.reloadData()
            }
        }
    }
    
    
    
    func Loaddata()
    {
       self.getsuccess(params: [:], urlhit:"\(LibraryAPI.sharedInstance.GlobalUrl)/Customer.asmx/CustomerCreditSummeryDetails?Customer=\(customercode)&Userid=\(LibraryAPI.sharedInstance.Userid)&Month=\(LibraryAPI.sharedInstance.CurrentMonth)&Year=\(LibraryAPI.sharedInstance.CurrentYear)&BranchCode=\(LibraryAPI.sharedInstance.BranchCodeNew)", completion:  { (jsonmutable) in
            self.responsestring = jsonmutable
        if(self.responsestring.count>0) {
            DispatchQueue.main.async {
                self.popupview.isHidden=true
                self.Invoicetableview.isHidden=false
                 self.Invoicetableview.reloadData()
                 self.local_activityIndicator_stop()
            }
        } else {
            DispatchQueue.main.async {
                self.Invoicetableview.isHidden=true
                self.popupview.isHidden=false
                 self.local_activityIndicator_stop()
            }
        }
        })
    }
    
    
    
    
    func KCFABOpened(fab: KCFloatingActionButton) {
        
    }
    
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            
            if (self.navigationController?.topViewController!.isKind(of: DashBoardViewController.classForCoder()) != nil) {
                
                self.navigationController?.popToViewController((self.navigationController?.viewControllers.first)!, animated: true)
            }
            else
            {
                let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                self.navigationController?.pushViewController(lvc, animated: false)
                
            }
            self.fab.close()
        }
        
        fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
            
            self.fetchvalues()
            
            self.fab.close()
        }
        
        
        fab.fabDelegate = self
        
        self.view.addSubview(fab)
        
    }
    
    
    
    
    func customnavigation()
        
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(InvoiceViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Invoice List"
        
    }
    
    func buttonClicked(sender:UIButton)
    {
        if (self.navigationController?.topViewController!.isKind(of: InvoicesummerydetailsViewController.classForCoder()) != nil) {
            
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: InvoicesummerydetailsViewController.classForCoder()) {
                    self.navigationController?.popToViewController(controller as UIViewController, animated: true)
                    break
                }
            }
        }
        else
        {
            let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
            self.navigationController?.pushViewController(lvc, animated: false)
            
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //TableView Delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
      
        if responsestring.count > 0
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return responsestring.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        var cell : InvoiceTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as! InvoiceTableViewCell
        
        
        if(responsestring.count > 0)
        {
            if(cell == nil)
            {
                cell = Bundle.main.loadNibNamed("Cell", owner: self, options: nil)![0] as! InvoiceTableViewCell;
                
            }
            let dic = responsestring[indexPath.row] as! NSDictionary
            cell.InvoiceNumberLabel.text=dic.object(forKey: "Invoice Number") as? String
            cell.InvoiceNumberLabel.sizeToFit()
            cell.InvoiceDateLabel.text=dic.object(forKey: "Invoice Date") as? String
            cell.InvoiceDateLabel.sizeToFit()
            cell.InvoiceValueLabel.text=String(dic.object(forKey: "Invoice Value") as! Double)
            cell.InvoiceValueLabel.sizeToFit()
            cell.InvoiceBaldueLabel.text=String(dic.object(forKey: "Due Amount")as! Double)
         //   cell.oddays.text=String(responsestring.objectAtIndex(0).objectForKey("Over Due Days") as! Int)
            cell.oddays.text=String(dic.object(forKey: "Over Due Days") as! Int)
            cell.oddays.sizeToFit()
            cell.InvoiceBaldueLabel.sizeToFit()
            let qtr = dic.object(forKey: "Customer Code") as? String
            let type = dic.object(forKey: "Customer Name") as? String
            self.titlelbl.text = qtr!+" "+"-"+type!+" "
        }
        else
            
        {
            
        }
   
        cell.selectionStyle = .none
        return cell as InvoiceTableViewCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.getStringsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=WHATSUP&Option=2", completion: { (jsonstr) in
            let responsestring1 = jsonstr
       
        if (responsestring1 == "Y")
        {
            let dicRes = self.responsestring[indexPath.row] as! NSDictionary
            let lvc=self.storyboard?.instantiateViewController(withIdentifier: "InvoiceSummaryViewController") as! InvoiceSummaryViewController
            lvc.CustomerName=(dicRes.object(forKey: "Customer Name") as? String)!
            lvc.cc=(dicRes.object(forKey:  "Customer Code") as? String)!
            lvc.Invoicedate=(dicRes.object(forKey:  "Invoice Date") as? String)!
            lvc.duedate=(dicRes.object(forKey:  "Due Date") as? String)!
            lvc.Invoicenum=(dicRes.object(forKey:  "Invoice Number") as? String)!
            lvc.InvoiceVal=(dicRes.object(forKey:  "Invoice Value") as! Double)
            lvc.Balancelval=(dicRes.object(forKey:  "Due Amount")as! Double)
            lvc.overdueday=(dicRes.object(forKey:  "Over Due Days") as! Int)
            lvc.Temparray = self.responsestring
            self.navigationController?.pushViewController(lvc, animated: true)
            
        }
        else
        {
            self.NormalAlert(title: "Alert", Messsage: " You are Not Authorised for this option")
        }
             })
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
    }
    
    
    
}
