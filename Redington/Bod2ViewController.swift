//
//  Bod2ViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 08/01/18.
//  Copyright © 2018 truetech. All rights reserved.
//


import UIKit
//import NVActivityIndicatorView
import Firebase
import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging


class Bod2ViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate {
    
    @IBOutlet weak var Scrollview: UIScrollView!
    @IBOutlet weak var mainview: UIView!
    @IBOutlet weak var TopView: UIView!
    @IBOutlet weak var samelblbzcode: UILabel!
    @IBOutlet weak var exceedlbl: UILabel!
    @IBOutlet weak var approvalbtn: UIButton!
    @IBOutlet weak var biztableview: UITableView!
    @IBOutlet weak var additionalview: UIView!
    @IBOutlet weak var tableviewtitle: UIView!
    @IBOutlet weak var BottomView: UIView!
    @IBOutlet weak var remarkstxtview: UITextView!
    @IBOutlet weak var Customercodetxtfield: UITextField!
    @IBOutlet weak var businesscodetxtfield: UITextField!
    @IBOutlet weak var creditdaystxtfield: UITextField!
    @IBOutlet weak var orderamttxtfield: UITextField!
    @IBOutlet weak var tableviewview: UIView!
    @IBOutlet weak var totaloutstandinglbl: UILabel!
    @IBOutlet weak var groupexposurelbl: UILabel!
    @IBOutlet weak var reasonforstoplbl: UILabel!
    @IBOutlet weak var overdue: UILabel!
    @IBOutlet weak var groupodlbl: UILabel!
    @IBOutlet weak var validitytilllbl: UILabel!
    
    var responsearray=NSMutableArray()
    var pickerarray=NSMutableArray()
    var currenttf = String()
    var CustomcodeCheck : String!
    var tableviewresponsearray : NSMutableArray = []
    var uploadfile=NSMutableDictionary()
    var jsonresult=NSDictionary()
    var TotallOutStanding : String!
    var GroupExposure: String!
    var OverDue : String!
    var GroupOverDue : String!
    var reasonforstop : String!
    var messsage: String!
    var BODRef: String!
    var ordermsg: String!
    var timer = Timer()
    var Movedup=Bool()
    var CheckInt : Int!
    var Arr : [String] = []
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        tableviewview.isHidden = true
        orderamttxtfield.keyboardType = .decimalPad
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationmethod), name: NSNotification.Name(rawValue: "GetNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(Bod2ViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(Bod2ViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        self.customnavigation()
        Scrollview.bounces = false
        biztableview.delegate = self
        biztableview.dataSource = self
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        additionalview.layer.masksToBounds=true
        additionalview.layer.cornerRadius=10.0
        self.additionalview.layer.borderWidth = 0.5
        CheckInt = 0
        approvalbtn.layer.masksToBounds=true
        approvalbtn.layer.cornerRadius=10.0
        remarkstxtview.layer.masksToBounds=true
        remarkstxtview.layer.cornerRadius=5.0
        self.remarkstxtview.layer.borderWidth = 0.5
        TopView.layer.borderColor = UIColor(red:30/255.0, green:184/255.0, blue:120/255.0, alpha: 1.0).cgColor
        TopView.layer.borderWidth = 1.0
        TopView.layer.cornerRadius = 5.0
        BottomView.layer.borderColor = UIColor(red:30/255.0, green:184/255.0, blue:120/255.0, alpha: 1.0).cgColor
        BottomView.layer.borderWidth = 1.0
        BottomView.layer.cornerRadius = 5.0
        biztableview.layer.cornerRadius = 5.0
        self.biztableview.layer.borderWidth = 1.0
        biztableview.layer.borderColor = UIColor(red:30/255.0, green:184/255.0, blue:120/255.0, alpha: 1.0).cgColor
        
        Customercodetxtfield.delegate = self
        businesscodetxtfield.delegate = self
        creditdaystxtfield.delegate = self
        orderamttxtfield.delegate = self
        remarkstxtview.delegate = self
        
        //tags
        Customercodetxtfield.tag = 1
        businesscodetxtfield.tag = 2
        creditdaystxtfield.tag = 3
        orderamttxtfield.tag = 4
        remarkstxtview.tag = 5
        biztableview.register(UINib(nibName: "BillingwithodCustomerCell", bundle: nil), forCellReuseIdentifier: "BillingwithodCustomerCell")
        biztableview.tableFooterView = UIView(frame: .zero)
        bizcodeRec()
        samelblbzcode.isHidden = true
        exceedlbl.isHidden = true
        
        //keyboard type
        creditdaystxtfield.keyboardType = .numberPad
        self.addDoneButtonOnKeyboard()
        let dateFormatter:DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        self.validitytilllbl.text = dateFormatter.string(from: Date())
        orderamttxtfield.addTarget(self, action: #selector(Bod2ViewController.textFieldDidBeginEditing(_:)), for: UIControlEvents.editingChanged)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(orderamttxtfield.text?.first == "0") {
            orderamttxtfield.text = ""
        } else if(orderamttxtfield.text == "0.0") {
            orderamttxtfield.text = "0."
        } else if(orderamttxtfield.text == ".") {
            orderamttxtfield.text = "0."
        } else {
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        Scrollview.isScrollEnabled=true
        Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:600)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func keyboardWillShow(_ notification: NSNotification) {
        if LibraryAPI.sharedInstance.poptextfieldclicked == false {
            if Movedup == false {
                self.view.frame.origin.y -= 100
                Movedup=true
            }
        }
    }
    
    func keyboardWillHide(_ notification: NSNotification) {
        if LibraryAPI.sharedInstance.poptextfieldclicked == false {
            self.view.frame.origin.y = 0
            Movedup=false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if CustomcodeCheck == nil || CustomcodeCheck == "" {
            self.Customercodetxtfield.text = ""
        } else {
            self.Customercodetxtfield.text = LibraryAPI.sharedInstance.CustomerCode
        }
        
        tableviewresponsearray.removeAllObjects()
        if TotallOutStanding != nil {
            self.totaloutstandinglbl.text = TotallOutStanding
        } else {
            self.totaloutstandinglbl.text = ""
        }
        
        if GroupExposure != nil {
            self.groupexposurelbl.text = GroupExposure
        } else {
            self.groupexposurelbl.text = ""
        }
        
        if OverDue != nil {
            self.overdue.text = OverDue
        } else {
            self.overdue.text = ""
        }
        
        if GroupOverDue != nil {
            self.groupodlbl.text = GroupOverDue
        } else {
            self.groupodlbl.text = ""
        }
        
        if reasonforstop != nil {
            self.reasonforstoplbl.text = reasonforstop
        } else {
            self.reasonforstoplbl.text = ""
        }
        
        if messsage != nil {
            if messsage != "" {
                showToast(message: messsage)
            } else  if BODRef != "" {
                showToast(message: BODRef)
            } else {
            }
        } else {
        }
    }
    
    
    func customnavigation() {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: 65)
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(Bod2ViewController.buttonClicked(_:)),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Billing With OD Customer"
    }
    
    func buttonClicked(_ sender:UIButton) {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "PreorderMainViewController")), animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
 
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.tag == 1 {
            LibraryAPI.sharedInstance.Currentcustomerlookup="Billingodcustomer"
            if #available(iOS 10.0, *) {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: CustomerLookupViewController.instance())
                    
                    .didShowHandler
                    { popup in
                        self.Customercodetxtfield.resignFirstResponder()
                    }
                    .didCloseHandler
                    { _ in
                }
                
                let container = CustomerLookupViewController.instance()
                container.closeHandler =
                    { _ in
                        popup.dismiss()
                }
                popup.show(childViewController: container)
            }
            return false
        } else if textField.tag == 2 {
            let size = CGSize(width: 50, height:50)
            self.local_activityIndicator_start()
            
            self.exceedlbl.isHidden = true
            self.samelblbzcode.isHidden = true
            self.samelblbzcode.isHidden = true
            self.exceedlbl.isHidden = true
            
            let alert:UIAlertController=UIAlertController(title: "Select Business Code", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            
            var alert1 = UIAlertAction()
            if pickerarray.count != 0 {
                self.local_activityIndicator_stop()
                for word in pickerarray {
                    alert1 = UIAlertAction(title: (word as! String), style: UIAlertActionStyle.default) {
                        UIAlertAction in
                        let getString : String = word as! String
                        if self.tableviewresponsearray.count < 5 {
                            if self.tableviewresponsearray.count == 0 {
                                self.tableviewresponsearray.add(getString)
                                self.Arr.append(getString)
                                self.biztableview.reloadData()
                            } else {
                                let contains = self.tableviewresponsearray.contains {
                                    $0 as? String == getString
                                }
                                
                                if contains == false {
                                    self.tableviewresponsearray.add(getString)
                                    self.Arr.append(getString)
                                    self.biztableview.reloadData()
                                } else {
                                    self.samelblbzcode.isHidden = false
                                    self.exceedlbl.isHidden = true
                                    self.samelblbzcode.text = "You are Selected same Business Code"
                                }                        }
                        } else {
                            self.exceedlbl.isHidden = false
                            self.samelblbzcode.isHidden = true
                            self.exceedlbl.text = "Alreay You have Selected 5 Business code"
                        }
                    }
                    alert.addAction(alert1)
                    
                }
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
            {
                UIAlertAction in
            }
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
            return false
        } else if (textField.tag == 3) {
            CheckInt = 3
            return true
        } else if (textField.tag == 4) {
            CheckInt = 4
            return true
        }
        return true
    }
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(BillingwithODCustomerViewController.doneButtonAction))
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        doneToolbar.items = items as AnyObject as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        orderamttxtfield.inputAccessoryView = doneToolbar
        creditdaystxtfield.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction() {
        if CheckInt == 3 {
            if (creditdaystxtfield.text! != "" ) {
                switch self.tableviewresponsearray.count {
                case 0:
                    uploadfile.setObject("", forKey: "BizCode1" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode2" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode3" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode4" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode5" as NSCopying)
                case 1:
                    let fullName : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr : [String] = fullName.components(separatedBy: " - ")
                    let firstName : String = fullNameArr[0]
                    
                    uploadfile.setObject(firstName, forKey: "BizCode1" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode2" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode3" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode4" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode5" as NSCopying)
                case 2:
                    let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                    let firstName1 : String = fullNameArr1[0]
                    
                    let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                    let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                    let firstName2 : String = fullNameArr2[0]
                    
                    uploadfile.setObject(firstName1, forKey: "BizCode1" as NSCopying)
                    uploadfile.setObject(firstName2, forKey: "BizCode2" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode3" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode4" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode5" as NSCopying)
                case 3:
                    let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                    let firstName1 : String = fullNameArr1[0]
                    
                    let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                    let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                    let firstName2 : String = fullNameArr2[0]
                    
                    let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                    let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                    let firstName3 : String = fullNameArr3[0]
                    
                    uploadfile.setObject(firstName1, forKey: "BizCode1" as NSCopying)
                    uploadfile.setObject(firstName2, forKey: "BizCode2" as NSCopying)
                    uploadfile.setObject(firstName3, forKey: "BizCode3" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode4" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode5" as NSCopying)
                case 4:
                    let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                    let firstName1 : String = fullNameArr1[0]
                    
                    let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                    let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                    let firstName2 : String = fullNameArr2[0]
                    
                    let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                    let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                    let firstName3 : String = fullNameArr3[0]
                    
                    let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
                    let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
                    let firstName4 : String = fullNameArr4[0]
                    
                    uploadfile.setObject(firstName1, forKey: "BizCode1" as NSCopying)
                    uploadfile.setObject(firstName2, forKey: "BizCode2" as NSCopying)
                    uploadfile.setObject(firstName3, forKey: "BizCode3" as NSCopying)
                    uploadfile.setObject(firstName4, forKey: "BizCode4" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode5" as NSCopying)
                case 5:
                    let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                    let firstName1 : String = fullNameArr1[0]
                    
                    let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                    let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                    let firstName2 : String = fullNameArr2[0]
                    
                    let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                    let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                    let firstName3 : String = fullNameArr3[0]
                    
                    let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
                    let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
                    let firstName4 : String = fullNameArr4[0]
                    
                    let fullName5 : String = self.tableviewresponsearray.object(at: 4) as! String
                    let fullNameArr5 : [String] = fullName5.components(separatedBy: " - ")
                    let firstName5 : String = fullNameArr5[0]
                    
                    uploadfile.setObject(firstName1, forKey: "BizCode1" as NSCopying)
                    uploadfile.setObject(firstName2, forKey: "BizCode2" as NSCopying)
                    uploadfile.setObject(firstName3, forKey: "BizCode3" as NSCopying)
                    uploadfile.setObject(firstName4, forKey: "BizCode4" as NSCopying)
                    uploadfile.setObject(firstName5, forKey: "BizCode5" as NSCopying)
                default:
                    print("")
                }
                
                self.uploadfile.setObject(Customercodetxtfield.text!, forKey: "CustomerCode" as NSCopying)
                self.uploadfile.setObject(orderamttxtfield.text!, forKey: "OrderValue" as NSCopying)
                self.uploadfile.setObject(creditdaystxtfield.text!, forKey: "CreditDays" as NSCopying)
                self.uploadfile.setObject(remarkstxtview.text, forKey: "Remarks" as NSCopying)
                self.uploadfile.setObject("", forKey: "Submit" as NSCopying)
                self.uploadfile.setObject(LibraryAPI.sharedInstance.Userid, forKey: "UserId" as NSCopying)
                self.uploaddata()
                
                if self.jsonresult.count > 0 {
                    self.Customercodetxtfield.text = LibraryAPI.sharedInstance.CustomerCode
                    self.totaloutstandinglbl.text = self.jsonresult.object(forKey: "TotallOutStanding") as? String
                    self.groupexposurelbl.text = self.jsonresult.object(forKey: "GroupExposure") as? String
                    self.overdue.text = self.jsonresult.object(forKey: "OverDue") as? String
                    self.groupodlbl.text = self.jsonresult.object(forKey: "GroupOverDue") as? String
                    self.reasonforstoplbl.text = self.jsonresult.object(forKey: "ReasonForStop") as? String
                    
                    let msg = self.jsonresult.object(forKey: "Message")as! String
                    let bod = self.jsonresult.object(forKey: "BODRef")as! String
                    if msg != "" {
                        self.creditdaystxtfield.resignFirstResponder()
                        showToast(message: msg)
                    } else if bod != "" {
                        self.creditdaystxtfield.resignFirstResponder()
                        showToast(message: bod)
                    } else{
                        self.creditdaystxtfield.resignFirstResponder()
                    }
                } else {
                    
                }
            } else {
                self.creditdaystxtfield.resignFirstResponder()
            }
        } else if CheckInt == 4 {
            if orderamttxtfield.text! != "" {
                switch self.tableviewresponsearray.count {
                case 0:
                    uploadfile.setObject("", forKey: "BizCode1" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode2" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode3" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode4" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode5" as NSCopying)
                case 1:
                    let fullName : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr : [String] = fullName.components(separatedBy: " - ")
                    let firstName : String = fullNameArr[0]
                    
                    uploadfile.setObject(firstName, forKey: "BizCode1" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode2" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode3" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode4" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode5" as NSCopying)
                case 2:
                    let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                    let firstName1 : String = fullNameArr1[0]
                    
                    let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                    let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                    let firstName2 : String = fullNameArr2[0]
                    
                    uploadfile.setObject(firstName1, forKey: "BizCode1" as NSCopying)
                    uploadfile.setObject(firstName2, forKey: "BizCode2" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode3" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode4" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode5" as NSCopying)
                case 3:
                    let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                    let firstName1 : String = fullNameArr1[0]
                    
                    let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                    let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                    let firstName2 : String = fullNameArr2[0]
                    
                    let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                    let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                    let firstName3 : String = fullNameArr3[0]
                    
                    uploadfile.setObject(firstName1, forKey: "BizCode1" as NSCopying)
                    uploadfile.setObject(firstName2, forKey: "BizCode2" as NSCopying)
                    uploadfile.setObject(firstName3, forKey: "BizCode3" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode4" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode5" as NSCopying)
                case 4:
                    let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                    let firstName1 : String = fullNameArr1[0]
                    
                    let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                    let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                    let firstName2 : String = fullNameArr2[0]
                    
                    let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                    let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                    let firstName3 : String = fullNameArr3[0]
                    
                    let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
                    let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
                    let firstName4 : String = fullNameArr4[0]
                    uploadfile.setObject(firstName1, forKey: "BizCode1" as NSCopying)
                    uploadfile.setObject(firstName2, forKey: "BizCode2" as NSCopying)
                    uploadfile.setObject(firstName3, forKey: "BizCode3" as NSCopying)
                    uploadfile.setObject(firstName4, forKey: "BizCode4" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode5" as NSCopying)
                case 5:
                    let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                    let firstName1 : String = fullNameArr1[0]
                    
                    let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                    let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                    let firstName2 : String = fullNameArr2[0]
                    
                    let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                    let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                    let firstName3 : String = fullNameArr3[0]
                    
                    let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
                    let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
                    let firstName4 : String = fullNameArr4[0]
                    
                    let fullName5 : String = self.tableviewresponsearray.object(at: 4) as! String
                    let fullNameArr5 : [String] = fullName5.components(separatedBy: " - ")
                    let firstName5 : String = fullNameArr5[0]
                    
                    uploadfile.setObject(firstName1, forKey: "BizCode1" as NSCopying)
                    uploadfile.setObject(firstName2, forKey: "BizCode2" as NSCopying)
                    uploadfile.setObject(firstName3, forKey: "BizCode3" as NSCopying)
                    uploadfile.setObject(firstName4, forKey: "BizCode4" as NSCopying)
                    uploadfile.setObject(firstName5, forKey: "BizCode5" as NSCopying)
                default:
                    print("")
                }
                
                self.uploadfile.setObject(Customercodetxtfield.text!, forKey: "CustomerCode" as NSCopying)
                self.uploadfile.setObject(orderamttxtfield.text!, forKey: "OrderValue" as NSCopying)
                self.uploadfile.setObject(creditdaystxtfield.text!, forKey: "CreditDays" as NSCopying)
                self.uploadfile.setObject(remarkstxtview.text, forKey: "Remarks" as NSCopying)
                self.uploadfile.setObject(LibraryAPI.sharedInstance.Userid, forKey: "UserId" as NSCopying)
                self.uploadfile.setObject("", forKey: "Submit" as NSCopying)
                self.uploaddata()
                
                if self.jsonresult.count > 0 {
                    self.Customercodetxtfield.text = LibraryAPI.sharedInstance.CustomerCode
                    self.totaloutstandinglbl.text = self.jsonresult.object(forKey: "TotallOutStanding") as? String
                    self.groupexposurelbl.text = self.jsonresult.object(forKey: "GroupExposure") as? String
                    self.overdue.text = self.jsonresult.object(forKey: "OverDue") as? String
                    self.groupodlbl.text = self.jsonresult.object(forKey: "GroupOverDue") as? String
                    self.reasonforstoplbl.text = self.jsonresult.object(forKey: "ReasonForStop") as? String
                    let msg = self.jsonresult.object(forKey: "Message")as! String
                    let bod = self.jsonresult.object(forKey: "BODRef")as! String
                    if msg != "" {
                        self.orderamttxtfield.resignFirstResponder()
                        showToast(message: msg)
                    } else if bod != "" {
                        self.orderamttxtfield.resignFirstResponder()
                        showToast(message: bod)
                    } else {
                        self.orderamttxtfield.resignFirstResponder()
                    }
                } else {
                    
                }
            } else {
                self.orderamttxtfield.resignFirstResponder()
            }
        }
    }
    
    func bizcodeRec() {
        let size = CGSize(width: 30, height:30)
        self.local_activityIndicator_start()
        
        DispatchQueue.global(qos: .background).async {
            self.Loaddata(url: "\(LibraryAPI.sharedInstance.GlobalUrl)StockDetails.asmx/BusinessAccessDetails?UserID=\(LibraryAPI.sharedInstance.Userid)")
            DispatchQueue.main.async {
              self.local_activityIndicator_stop()
            }
        }
    }
    
    
    func Loaddata(url:String) {
       // responsearray = LibraryAPI.sharedInstance.RequestUrl(url: url)
       self.getsuccess(params: [:], urlhit: url, completion:  { (jsonmutable) in
            self.responsearray  = jsonmutable
        if(self.responsearray.count > 0) {
            
        }
        self.pickerarray.removeAllObjects()
        for i in 0  ..< self.responsearray.count
        {
            let code = (self.responsearray.object(at: i) as AnyObject).object(forKey: "BUSINESS CODE") as? String
            let desc = (self.responsearray.object(at: i) as AnyObject).object(forKey: "BUSINESS DESC") as? String
            let str = code! + " - \(desc!)"
            self.pickerarray.add(str)
        }
        })
    }
    
    //TableView Delegates
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if self.Arr.count == 0 {
            biztableview.frame = CGRect(x: 0, y: 117, width: self.biztableview.frame.width, height: 0)
            mainview.frame = CGRect(x: 0, y: 0, width: self.mainview.frame.width, height: 722)
            TopView.frame = CGRect(x: 0, y: 2, width: self.TopView.frame.width, height: self.TopView.frame.height)
            BottomView.frame = CGRect(x: 0, y: 125, width: self.BottomView.frame.width, height: self.BottomView.frame.height)
            Scrollview.isScrollEnabled=true
            Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:600)
            tableviewview.isHidden = true
            return 0
        } else {
            if self.Arr.count == 1 {
                biztableview.frame = CGRect(x: 0, y: 117, width: self.biztableview.frame.width, height: 0+40)
                mainview.frame = CGRect(x: 0, y: 0, width: self.mainview.frame.width, height: 722)
                TopView.frame = CGRect(x: 0, y: 2, width: self.TopView.frame.width, height: self.TopView.frame.height)
                BottomView.frame = CGRect(x: 0, y: 125 + 40, width: self.BottomView.frame.width, height: self.BottomView.frame.height)
                tableviewview.isHidden = false
                Scrollview.isScrollEnabled=true
                Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:800)
            } else if self.Arr.count == 2 {
                biztableview.frame = CGRect(x: 0, y: 117, width: self.biztableview.frame.width, height: 0+80)
                mainview.frame = CGRect(x: 0, y: 0, width: self.mainview.frame.width, height: 722)
                TopView.frame = CGRect(x: 0, y: 2, width: self.TopView.frame.width, height: self.TopView.frame.height)
                BottomView.frame = CGRect(x: 0, y: 125 + 80, width: self.BottomView.frame.width, height: self.BottomView.frame.height)
                tableviewview.isHidden = false
                Scrollview.isScrollEnabled=true
                Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:800)
            } else if self.Arr.count == 3 {
                biztableview.frame = CGRect(x: 0, y: 117, width: self.biztableview.frame.width, height: 0 + 120)
                mainview.frame = CGRect(x: 0, y: 0, width: self.mainview.frame.width, height: 722)
                TopView.frame = CGRect(x: 0, y: 2, width: self.TopView.frame.width, height: self.TopView.frame.height)
                BottomView.frame = CGRect(x: 0, y: 125 + 120, width: self.BottomView.frame.width, height: self.BottomView.frame.height)
                tableviewview.isHidden = false
                Scrollview.isScrollEnabled=true
                Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:800)
            } else if self.Arr.count == 4 {
                biztableview.frame = CGRect(x: 0, y: 117, width: self.biztableview.frame.width, height: 0 + 160)
                mainview.frame = CGRect(x: 0, y: 0, width: self.mainview.frame.width, height: 722)
                TopView.frame = CGRect(x: 0, y: 2, width: self.TopView.frame.width, height: self.TopView.frame.height)
                BottomView.frame = CGRect(x: 0, y: 125 + 160, width: self.BottomView.frame.width, height: self.BottomView.frame.height)
                tableviewview.isHidden = false
                Scrollview.isScrollEnabled=true
                Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:800)
            } else if (self.Arr.count == 5) {
                biztableview.frame = CGRect(x: 0, y: 117, width: self.biztableview.frame.width, height: 0 + 200)
                mainview.frame = CGRect(x: 0, y: 0, width: self.mainview.frame.width, height: 722)
                TopView.frame = CGRect(x: 0, y: 2, width: self.TopView.frame.width, height: self.TopView.frame.height)
                BottomView.frame = CGRect(x: 0, y: 125 + 200, width: self.BottomView.frame.width, height: self.BottomView.frame.height)
                tableviewview.isHidden = false
                Scrollview.isScrollEnabled=true
                Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:800)
            } else {
                biztableview.frame = CGRect(x: 0, y: 117, width: self.biztableview.frame.width, height: 0)
                mainview.frame = CGRect(x: 0, y: 0, width: self.mainview.frame.width, height: 722)
                TopView.frame = CGRect(x: 0, y: 2, width: self.TopView.frame.width, height: self.TopView.frame.height)
                BottomView.frame = CGRect(x: 0, y: 125, width: self.BottomView.frame.width, height: self.BottomView.frame.height)
                tableviewview.isHidden = true
                Scrollview.isScrollEnabled=true
                Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:1000)
            }
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableviewresponsearray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BillingwithodCustomerCell", for: indexPath) as! BillingwithodCustomerCell
        biztableview.isHidden = false
        cell.deletebtn.addTarget(self, action: #selector(self.deletebtnclicked), for: UIControlEvents.touchUpInside)
        cell.deletebtn.tag = indexPath.row
        let getString = tableviewresponsearray[indexPath.row] as? String
        uploadfile.setObject(Customercodetxtfield.text!, forKey: "CustomerCode" as NSCopying)
        uploadfile.setObject(LibraryAPI.sharedInstance.Userid, forKey: "UserId" as NSCopying)
        uploadfile.setObject(orderamttxtfield.text!, forKey: "OrderValue" as NSCopying)
        uploadfile.setObject(creditdaystxtfield.text!, forKey: "CreditDays" as NSCopying)
        uploadfile.setObject(remarkstxtview.text!, forKey: "Remarks" as NSCopying)
        self.uploadfile.setObject("", forKey: "Submit" as NSCopying)
        
        switch self.tableviewresponsearray.count {
        case 0:
            uploadfile.setObject("", forKey: "BizCode1" as NSCopying)
            uploadfile.setObject("", forKey: "BizCode2" as NSCopying)
            uploadfile.setObject("", forKey: "BizCode3" as NSCopying)
            uploadfile.setObject("", forKey: "BizCode4" as NSCopying)
            uploadfile.setObject("", forKey: "BizCode5" as NSCopying)
        case 1:
            let fullName : String = self.tableviewresponsearray.object(at: 0) as! String
            let fullNameArr : [String] = fullName.components(separatedBy: " - ")
            let firstName : String = fullNameArr[0]
            
            uploadfile.setObject(firstName, forKey: "BizCode1" as NSCopying)
            uploadfile.setObject("", forKey: "BizCode2" as NSCopying)
            uploadfile.setObject("", forKey: "BizCode3" as NSCopying)
            uploadfile.setObject("", forKey: "BizCode4" as NSCopying)
            uploadfile.setObject("", forKey: "BizCode5" as NSCopying)
        case 2:
            let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
            let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
            let firstName1 : String = fullNameArr1[0]
            
            let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
            let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
            let firstName2 : String = fullNameArr2[0]
            
            uploadfile.setObject(firstName1, forKey: "BizCode1" as NSCopying)
            uploadfile.setObject(firstName2, forKey: "BizCode2" as NSCopying)
            uploadfile.setObject("", forKey: "BizCode3" as NSCopying)
            uploadfile.setObject("", forKey: "BizCode4" as NSCopying)
            uploadfile.setObject("", forKey: "BizCode5" as NSCopying)
        case 3:
            let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
            let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
            let firstName1 : String = fullNameArr1[0]
            
            let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
            let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
            let firstName2 : String = fullNameArr2[0]
            
            let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
            let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
            let firstName3 : String = fullNameArr3[0]
            
            uploadfile.setObject(firstName1, forKey: "BizCode1" as NSCopying)
            uploadfile.setObject(firstName2, forKey: "BizCode2" as NSCopying)
            uploadfile.setObject(firstName3, forKey: "BizCode3" as NSCopying)
            uploadfile.setObject("", forKey: "BizCode4" as NSCopying)
            uploadfile.setObject("", forKey: "BizCode5" as NSCopying)
        case 4:
            let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
            let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
            let firstName1 : String = fullNameArr1[0]
            
            let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
            let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
            let firstName2 : String = fullNameArr2[0]
            
            let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
            let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
            let firstName3 : String = fullNameArr3[0]
            
            let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
            let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
            let firstName4 : String = fullNameArr4[0]
            
            uploadfile.setObject(firstName1, forKey: "BizCode1" as NSCopying)
            uploadfile.setObject(firstName2, forKey: "BizCode2" as NSCopying)
            uploadfile.setObject(firstName3, forKey: "BizCode3" as NSCopying)
            uploadfile.setObject(firstName4, forKey: "BizCode4" as NSCopying)
            uploadfile.setObject("", forKey: "BizCode5" as NSCopying)
        case 5:
            let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
            let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
            let firstName1 : String = fullNameArr1[0]
            
            let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
            let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
            let firstName2 : String = fullNameArr2[0]
            
            let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
            let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
            let firstName3 : String = fullNameArr3[0]
            
            let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
            let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
            let firstName4 : String = fullNameArr4[0]
            
            let fullName5 : String = self.tableviewresponsearray.object(at: 4) as! String
            let fullNameArr5 : [String] = fullName5.components(separatedBy: " - ")
            let firstName5 : String = fullNameArr5[0]
            
            uploadfile.setObject(firstName1, forKey: "BizCode1" as NSCopying)
            uploadfile.setObject(firstName2, forKey: "BizCode2" as NSCopying)
            uploadfile.setObject(firstName3, forKey: "BizCode3" as NSCopying)
            uploadfile.setObject(firstName4, forKey: "BizCode4" as NSCopying)
            uploadfile.setObject(firstName5, forKey: "BizCode5" as NSCopying)
        default:
            print("")
        }
        
        uploaddata()
        if self.jsonresult.count > 0 {
            let fullNameArr5 : [String] = getString!.components(separatedBy: " - ")
            if fullNameArr5.count >= 3 {
                let bizcode : String = fullNameArr5[0]
                let bizname : String = fullNameArr5[1]
                let bizname1 : String = fullNameArr5[2]
                cell.bizcode.text = bizcode
                cell.bizname.text = bizname + " - " + bizname1
            } else {
                let bizcode : String = fullNameArr5[0]
                let bizname : String = fullNameArr5[1]
                cell.bizcode.text = bizcode
                cell.bizname.text = bizname
            }
            self.Customercodetxtfield.text = LibraryAPI.sharedInstance.CustomerCode
            self.totaloutstandinglbl.text = self.jsonresult.object(forKey: "TotallOutStanding") as? String
            self.groupexposurelbl.text = self.jsonresult.object(forKey: "GroupExposure") as? String
            self.overdue.text = self.jsonresult.object(forKey: "OverDue") as? String
            self.groupodlbl.text = self.jsonresult.object(forKey: "GroupOverDue") as? String
            self.reasonforstoplbl.text = self.jsonresult.object(forKey: "ReasonForStop") as? String
            LibraryAPI.sharedInstance.BODRefnumber = (self.jsonresult.object(forKey: "BODRef") as? String)!
            
            let msg = self.jsonresult.object(forKey: "Message")as! String
            let bod = (self.jsonresult.object(forKey: "BODRef") as? String)!
            
            if msg != "" {
                showToast(message: msg)
            } else if bod != "" {
                showToast(message: bod)
            } else {
                
            }
        } else {
            let fullNameArr5 : [String] = getString!.components(separatedBy: " - ")
            if(fullNameArr5.count >= 3) {
                let bizcode : String = fullNameArr5[0]
                let bizname : String = fullNameArr5[1]
                let bizname1 : String = fullNameArr5[2]
                cell.bizcode.text = bizcode
                cell.bizname.text = bizname + " - " + bizname1
            } else {
                let bizcode : String = fullNameArr5[0]
                let bizname : String = fullNameArr5[1]
                cell.bizcode.text = bizcode
                cell.bizname.text = bizname
            }
        }
        return cell
    }
    
    func deletebtnclicked(sender: UIButton) {
        let alertController = UIAlertController(title: "Business Code", message: "Are You Sure Want to Delete Business Code?", preferredStyle: .alert)
        self.creditdaystxtfield.resignFirstResponder()
        let DefaultAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
            self.tableviewresponsearray.removeObject(at: sender.tag)
            self.Arr.remove(at: sender.tag)
            self.biztableview.reloadData()
        }
        
        let CancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
            
        }
        alertController.addAction(DefaultAction)
        alertController.addAction(CancelAction)
        self.present(alertController, animated: true, completion:nil)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 5 {
            guard let text = textField.text else { return true }
            
            let newLength = text.count + string.count - range.length
            return newLength <= 300
        }
        if textField.tag == 3 {
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 3
        }
        if textField.tag == 4 {
            
            let candidate = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
            let regex = try? NSRegularExpression(pattern: "^\\d{0,8}(\\.\\d{0,2})?$", options: [])
            return regex?.firstMatch(in: candidate, options: [], range: NSRange(location: 0, length: candidate.count)) != nil
        }
        return true
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        
        let currentCharacterCount = textView.text?.count ?? 0
        if range.length + range.location > currentCharacterCount {
            return false
        }
        let newLength = currentCharacterCount + text.count - range.length
        
        let cs = NSCharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 .,").inverted
        let filtered = text.components(separatedBy: cs).joined(separator: "")
        
        return filtered == text && newLength <= 100
    }
    
    
    @IBAction func Approvebtn(_ sender: AnyObject) {
        if self.Customercodetxtfield.text!.isEmpty && self.businesscodetxtfield.text!.isEmpty && self.creditdaystxtfield.text!.isEmpty && self.orderamttxtfield.text!.isEmpty && remarkstxtview.text!.isEmpty {
            self.showalert(title: "Alert", message: "Please Enter the all fields", buttontxt: "Ok")
        } else {
            if (self.remarkstxtview.text?.count)! < 4 {
                self.showalert(title: "Alert", message: "Please Enter valid Remarks", buttontxt: "Ok")
            } else {
                uploadfile.setObject(Customercodetxtfield.text!, forKey: "CustomerCode" as NSCopying)
                uploadfile.setObject(LibraryAPI.sharedInstance.Userid, forKey: "UserId" as NSCopying)
                uploadfile.setObject(orderamttxtfield.text!, forKey: "OrderValue" as NSCopying)
                uploadfile.setObject(creditdaystxtfield.text!, forKey: "CreditDays" as NSCopying)
                uploadfile.setObject(remarkstxtview.text!, forKey: "Remarks" as NSCopying)
                self.uploadfile.setObject("y", forKey: "Submit" as NSCopying)
                switch self.tableviewresponsearray.count {
                case 0:
                    uploadfile.setObject("", forKey: "BizCode1" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode2" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode3" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode4" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode5" as NSCopying)
                case 1:
                    let fullName : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr : [String] = fullName.components(separatedBy: " - ")
                    let firstName : String = fullNameArr[0]
                    
                    uploadfile.setObject(firstName, forKey: "BizCode1" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode2" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode3" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode4" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode5" as NSCopying)
                case 2:
                    let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                    let firstName1 : String = fullNameArr1[0]
                    
                    let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                    let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                    let firstName2 : String = fullNameArr2[0]
                    
                    uploadfile.setObject(firstName1, forKey: "BizCode1" as NSCopying)
                    uploadfile.setObject(firstName2, forKey: "BizCode2" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode3" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode4" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode5" as NSCopying)
                case 3:
                    let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                    let firstName1 : String = fullNameArr1[0]
                    
                    let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                    let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                    let firstName2 : String = fullNameArr2[0]
                    
                    let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                    let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                    let firstName3 : String = fullNameArr3[0]
                    
                    uploadfile.setObject(firstName1, forKey: "BizCode1" as NSCopying)
                    uploadfile.setObject(firstName2, forKey: "BizCode2" as NSCopying)
                    uploadfile.setObject(firstName3, forKey: "BizCode3" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode4" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode5" as NSCopying)
                case 4:
                    let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                    let firstName1 : String = fullNameArr1[0]
                    
                    let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                    let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                    let firstName2 : String = fullNameArr2[0]
                    
                    let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                    let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                    let firstName3 : String = fullNameArr3[0]
                    
                    let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
                    let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
                    let firstName4 : String = fullNameArr4[0]
                    
                    uploadfile.setObject(firstName1, forKey: "BizCode1" as NSCopying)
                    uploadfile.setObject(firstName2, forKey: "BizCode2" as NSCopying)
                    uploadfile.setObject(firstName3, forKey: "BizCode3" as NSCopying)
                    uploadfile.setObject(firstName4, forKey: "BizCode4" as NSCopying)
                    uploadfile.setObject("", forKey: "BizCode5" as NSCopying)
                case 5:
                    let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                    let firstName1 : String = fullNameArr1[0]
                    
                    let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                    let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                    let firstName2 : String = fullNameArr2[0]
                    
                    let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                    let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                    let firstName3 : String = fullNameArr3[0]
                    
                    let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
                    let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
                    let firstName4 : String = fullNameArr4[0]
                    
                    let fullName5 : String = self.tableviewresponsearray.object(at: 4) as! String
                    let fullNameArr5 : [String] = fullName5.components(separatedBy: " - ")
                    let firstName5 : String = fullNameArr5[0]
                    
                    uploadfile.setObject(firstName1, forKey: "BizCode1" as NSCopying)
                    uploadfile.setObject(firstName2, forKey: "BizCode2" as NSCopying)
                    uploadfile.setObject(firstName3, forKey: "BizCode3" as NSCopying)
                    uploadfile.setObject(firstName4, forKey: "BizCode4" as NSCopying)
                    uploadfile.setObject(firstName5, forKey: "BizCode5" as NSCopying)
                default:
                    print("")
                }
                
                uploaddata()
                
                if self.jsonresult.count > 0 {
                    self.Customercodetxtfield.text = LibraryAPI.sharedInstance.CustomerCode
                    self.totaloutstandinglbl.text = self.jsonresult.object(forKey: "TotallOutStanding") as? String
                    self.groupexposurelbl.text = self.jsonresult.object(forKey: "GroupExposure") as? String
                    self.overdue.text = self.jsonresult.object(forKey: "OverDue") as? String
                    self.groupodlbl.text = self.jsonresult.object(forKey: "GroupOverDue") as? String
                    self.reasonforstoplbl.text = self.jsonresult.object(forKey: "ReasonForStop") as? String
                    self.uploadfile.setObject("y", forKey: "Submit" as NSCopying)
                    let msg = self.jsonresult.object(forKey: "Message")as? String
                    let bod = self.jsonresult.object(forKey: "BODRef")as? String
                    
                    if msg != "" {
                        self.showalert(title: "Alert", message: msg!, buttontxt: "Ok")
                    } else if bod != "" {
                        let alertController = UIAlertController(title: "Alert", message:"BOD is updated, Your ID : \(String(describing: bod))" , preferredStyle: .alert)
                        let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                            self.navigate()
                        }
                        alertController.addAction(cancelAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        
                    }
                } else {
                    
                }
            }
        }
    }
    
    func navigate() {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
    }
    
    func uploaddata() {
        let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)Users.asmx/BODUpdation")! as URL)
        request.httpMethod = "POST"
        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
        request.httpBody = try? JSONSerialization.data(withJSONObject: self.uploadfile, options: JSONSerialization.WritingOptions(rawValue: 0))
        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
        do {
            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
            jsonresult = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
        }
        catch (let e) {
            print(e)
        }
    }
    
    
    func notificationmethod() {
        let userdefaults = UserDefaults.standard
        let value = userdefaults.dictionary(forKey: "Notification")
        var imagestr: String!
        imagestr = value?["imageurl"] as! String
        
        if imagestr != nil {
            if imagestr != "" {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: notificationimageViewController.instance())
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = notificationimageViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                }
                popup.show(childViewController: container)
            } else {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: PushnotificationtextViewController.instance())
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = PushnotificationtextViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                }
                popup.show(childViewController: container)
            }
        } else {
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: PushnotificationtextViewController.instance())
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
            }
            let container = PushnotificationtextViewController.instance()
            container.closeHandler = { _ in
                popup.dismiss()
            }
            popup.show(childViewController: container)
        }
    }
}
