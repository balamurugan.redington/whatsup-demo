//
//  mapkitclass.swift
//  Redington
//
//  Created by TRUETECHMAC3 on 14/10/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import MapKit
import Foundation
import UIKit

class mapkitclass : NSObject, MKAnnotation {
   
    private var coord: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 0, longitude: 0)
    private var _title: String = String("Test")
    private var _subtitle: String = String("")
    
    var title: String? {
        get {
            return _title
        }
        set (value) {
            self._title = value!
        }
    }
    
    var subtitle: String? {
        get {
            return _subtitle
        }
        set (value) {
            self._subtitle = value!
        }
    }
    
    var coordinate: CLLocationCoordinate2D {
        get {
            return coord
        }
    }
    
    func setCoordinate(newCoordinate: CLLocationCoordinate2D) {
        self.coord = newCoordinate
    }
}
