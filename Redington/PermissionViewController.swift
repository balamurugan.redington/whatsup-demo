//
//  PermissionViewController.swift
//  Redington
//
//  Created by RIL-ITTECH on 24/03/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import KCFloatingActionButton

class PermissionViewController: UIViewController,KCFloatingActionButtonDelegate,UITextFieldDelegate,UITextViewDelegate{
    
    @IBOutlet weak var requiredtf: UITextField!
    
    @IBOutlet weak var datepicker: UIDatePicker!
    @IBOutlet weak var Timedatepicker: UIDatePicker!
    
    
    @IBOutlet weak var pickerview: UIView!
    @IBOutlet weak var submitbtn: UIButton!
    @IBOutlet weak var reasontv: UITextView!
    @IBOutlet weak var tohhmmtf: UITextField!
    @IBOutlet weak var fromhhmmtf: UITextField!
    //var pickerDataSource = ["1", "2", "3", "5","6","7","8","9","4"];
    var currenttextfield=String()
    var Movedup=Bool()
    var uploadfile=NSMutableDictionary()
    var jsonresult=NSDictionary()
    var responsestring3 = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //swipe
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(PermissionViewController.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        reasontv.layer.cornerRadius = 5.0
        reasontv.layer.borderWidth = 1.0
        self.reasontv.layer.borderColor = UIColor(red:211/255.0, green:211/255.0, blue:211/255.0, alpha: 1.0).cgColor
        submitbtn.layer.cornerRadius = 5.0
        submitbtn.layer.borderWidth = 1.0
        self.submitbtn.layer.borderColor = UIColor(red:211/255.0, green:211/255.0, blue:211/255.0, alpha: 1.0).cgColor
        //delegate
        requiredtf.delegate = self
        fromhhmmtf.delegate = self
        tohhmmtf.delegate = self
        reasontv.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(PermissionViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PermissionViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        //tag
        
        requiredtf.tag = 1
        fromhhmmtf.tag = 2
        tohhmmtf.tag = 3
        
        if LibraryAPI.sharedInstance.Globalflag == "N"
        {
            customnavigation1()
            
        }
        else
        {
            customnavigation()
            
        }
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        
        let arrow = UIImageView()
        let image = UIImage(named: "cal.png")
        arrow.image = image!.imageWithColor(tintColor: UIColor.init(red:239/255.0 , green: 88/255.0, blue: 88/255.0, alpha: 1.0)).withRenderingMode(.alwaysOriginal)
        arrow.frame = CGRect(x: 0.0, y: 0.0, width: arrow.image!.size.width+10.0, height: 15);
        arrow.contentMode = UIViewContentMode.center
        self.requiredtf.rightView = arrow;
        self.requiredtf.rightViewMode = UITextFieldViewMode.always
        
        
        
    }
    
    func keyboardWillShow(notification: NSNotification)
    {
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true)
        {
        }
        else
        {
            if(Movedup==false)
            {
                self.view.frame.origin.y -= 150
                Movedup=true
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification)
    {
        
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true)
        {
        }
        else
        {
            self.view.frame.origin.y = 0
            Movedup=false
        }
        
    }
    
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                self.Navigate()
            case UISwipeGestureRecognizerDirection.down: break
            case UISwipeGestureRecognizerDirection.left: break
            case UISwipeGestureRecognizerDirection.up: break
            default:
                break
            }
        }
    }
    
    func Navigate()
    {
        let slideInFromLeftTransition = CATransition()
        slideInFromLeftTransition.type = kCAGravityTop
        slideInFromLeftTransition.subtype = kCATransitionFromRight
        slideInFromLeftTransition.duration = 0.2
        slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "LeaveViewController") as! LeaveViewController
        self.navigationController?.view.layer.add(slideInFromLeftTransition, forKey: "slideInFromRightTransition")
        self.navigationController?.pushViewController(lvc, animated: false)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        appdelegate.shouldSupportAllOrientation = false
    }
    func customnavigation()
        
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: 65);
        
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(PermissionViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Leave/Permission"
        
    }
    
    
    func customnavigation1()
        
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y, width: self.view.frame.width, height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        //customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), forState: UIControlState.Normal)
        customView.MenuBtn.addTarget(self, action: #selector(PermissionViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        //customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),forControlEvents:UIControlEvents.TouchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.textAlignment = .center
        customView.HeaderLabel.font = UIFont.boldSystemFont(ofSize: 16.0)
        customView.HeaderLabel.text = "Leave/Permission"
        
    }
    
    
    
    
    func buttonClicked(sender:UIButton)
    {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "HRD1DetailsViewController")), animated: true)
    }
    
    
    @IBAction func leavecontrollerbtn(_ sender: AnyObject)
    {
        Navigate()
    }
    
    
    @IBAction func donebtn(_ sender: AnyObject)
    {
        datepicker.isHidden=true
        Timedatepicker.isHidden = true
        
        if( currenttextfield=="Date")
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = DateFormatter.Style.long
            dateFormatter.dateFormat = "yyyy-MM-dd"
            //"MM-dd-yy"
            let strDate = dateFormatter.string(from: datepicker.date)
            
            requiredtf.text=strDate
            pickerview.isHidden = true
            self.Loaddata1()
            
        }
        else if( currenttextfield=="Time")
        {
            pickerview.isHidden = true
        }
        else if( currenttextfield=="to")
        {
            pickerview.isHidden = true
        }
        
        
    }
    @IBAction func CancelAction(_ sender: AnyObject)
    {
        pickerview.isHidden=true
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
 
        // fab.hidden=false
        textField.resignFirstResponder()
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    
        if(textField.tag==1)
        {
            datepicker.date = NSDate() as Date
            pickerview.isHidden=false
            datepicker.isHidden = false
            Timedatepicker.isHidden = true
            datepicker.datePickerMode = UIDatePickerMode.date
            
            //  datepicker.maximumDate=NSDate.distantFuture()
            // datepicker.minimumDate=NSDate.distantPast()
            currenttextfield="Date"
            //datepicker.minimumDate = datepicker.date
            
            
            return false
            
        }
        
        if(textField.tag==2)
        {
            
            let alert:UIAlertController=UIAlertController(title: "Select Hours", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let head1 = ["0","1","2","3","4","5","6","7","8"]
            
            var alert1 = UIAlertAction()
            
            if head1.count != 0
            {
                for word in head1
                {
                    alert1 = UIAlertAction(title: word, style: UIAlertActionStyle.default)
                    {
                        
                        UIAlertAction in
                        self.fromhhmmtf.text = word
                        self.tohhmmtf.text = "00"
                    }
                    
                    alert.addAction(alert1)
                    
                }
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
            {
                UIAlertAction in
            }
            
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
            
            return false
            
        }
        if(textField.tag == 3)
        {
            let alert:UIAlertController=UIAlertController(title: "Select Minutes", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let head2 = ["15","20","25","30","35","40","45","50","55"]
            
            var alert1 = UIAlertAction()
            
            if head2.count != 0
            {
                for word in head2
                {
                    alert1 = UIAlertAction(title: word, style: UIAlertActionStyle.default)
                    {
                        
                        UIAlertAction in
                        let hrValue = Int(self.fromhhmmtf.text!)
                        if hrValue == 0
                        {
                            if word != "00"
                            {
                                self.tohhmmtf.text = word
                            }
                            else
                            {
                                self.tohhmmtf.text = ""
                            }
                            
                        }
                        else if hrValue == 8
                        {
                            self.tohhmmtf.text = "00"
                        }
                        else
                        {
                            self.tohhmmtf.text = word
                        }
                        
                    }
                    
                    alert.addAction(alert1)
                    
                }
                
            }
            
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
            {
                UIAlertAction in
                
                
            }
            
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
            return false
            
        }
        
        
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
   
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        
        let allowedCharacters = " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,"
        
        let set = NSCharacterSet(charactersIn: allowedCharacters);
        let inverted = set.inverted;
        
        let filtered = text
            .components(separatedBy: inverted)
            .joined(separator: "");
        return filtered == text;
    }
    
    
    func Loaddata1()
    {
        // From Date
        // http://edi.redingtonb2b.in/whatsup-staging/LeaveDetails.asmx/HolidayCheck?UserID=SENTHILKS&Date=1-3-2017&Flag=2
        
       // responsestring3 = LibraryAPI.sharedInstance.MyRequestUrl(url: "\(LibraryAPI.sharedInstance.GlobalUrl)LeaveDetails.asmx/HolidayCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Date=\(self.requiredtf.text!)&Flag=")
        self.getStringsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)LeaveDetails.asmx/HolidayCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Date=\(self.requiredtf.text!)&Flag=", completion: { (jsonstr) in
            self.responsestring3 = jsonstr
        
        
        if(self.responsestring3 == "")
        {
          //  dispatch_async(dispatch_get_main_queue(),
            //               {
            //})
        }
        else
        {
             DispatchQueue.main.async {
                let alert = UIAlertController(title: "Sorry", message:"\(self.responsestring3)", preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler:
                                { (ACTION :UIAlertAction!)in
                                    self.requiredtf.text = ""
                            }))
                            self.present(alert, animated: true, completion: nil)
            }
            
        }
            })
    }
    func uploaddata()
    {
        
        let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)LeaveDetails.asmx/LeavePermission")! as URL)
        request.httpMethod = "POST"
        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
        request.httpBody = try? JSONSerialization.data(withJSONObject: self.uploadfile, options: JSONSerialization.WritingOptions(rawValue: 0))
        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
        
        do
        {
            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
            jsonresult = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
        }
        catch (let e)
        {
            print(e)
        }
    }
    
    @IBAction func submitbtn(_ sender: AnyObject)
    {
        LibraryAPI.sharedInstance.internetcheck()
        
        if((self.reasontv.text!.isEmpty || self.requiredtf.text!.isEmpty||self.fromhhmmtf.text!.isEmpty ))
        {
            let alertController = UIAlertController(title: "Alert", message:"Please Enter the all fields" , preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                
                
                
            }
            alertController.addAction(cancelAction)
            
            
            self.present(alertController, animated: true, completion:nil)
        }
        else
        {
            if((self.requiredtf.text?.count)!<=5)
            {
                let alertController = UIAlertController(title: "Alert", message:"Please Enter a Required Date" , preferredStyle: .alert)
                
                let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                    
                    
                    
                }
                alertController.addAction(cancelAction)
                
                
                self.present(alertController, animated: true, completion:nil)
            }
            else
            {
                
                if((self.reasontv.text?.count)!<5)
                {
                    let alertController = UIAlertController(title: "Alert", message:"Please Enter the valid Reason " , preferredStyle: .alert)
                    
                    let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                        
                        
                        
                    }
                    alertController.addAction(cancelAction)
                    
                    
                    self.present(alertController, animated: true, completion:nil)
                }
                else
                {
                    
                    if(self.fromhhmmtf.text!.isEmpty)
                    {
                        let alertController = UIAlertController(title: "Alert", message:"Please Select the Hours option" , preferredStyle: .alert)
                        
                        let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                            
                            
                            
                        }
                        alertController.addAction(cancelAction)
                        
                        
                        self.present(alertController, animated: true, completion:nil)
                    }
                        
                    else if(self.tohhmmtf.text!.isEmpty)
                    {
                        let alertController = UIAlertController(title: "Alert", message:"Please Select the Minutes option " , preferredStyle: .alert)
                        
                        let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                            
                            
                            
                        }
                        alertController.addAction(cancelAction)
                        
                        
                        self.present(alertController, animated: true, completion:nil)
                    }
                        
                    else
                    {
                        uploadfile.setObject(requiredtf.text!, forKey: "Date" as NSCopying)
                        //let str1 = fromhhmmtf.text?.stringByAppendingString(" \(tohhmmtf.text!)")
                        let str1 = fromhhmmtf.text?.appending(" \(tohhmmtf.text!)")
                       // let time = str1!.stringByReplacingOccurrencesOfString(" ", withString: ".")
                        let time = str1!.replacingOccurrences(of: " ", with: ".")
                        uploadfile.setObject(time, forKey: "NoofHours" as NSCopying)
                        uploadfile.setObject(reasontv.text!, forKey: "Reason" as NSCopying)
                        uploadfile.setObject(LibraryAPI.sharedInstance.Userid, forKey: "UserId" as NSCopying)
                        let size = CGSize(width: 30, height:30)
                        self.local_activityIndicator_start()
                        
                        // new version dispatch Que
                        
                        DispatchQueue.global(qos: .background).async {
                            
                            // Validate user input
                            
                               self.uploaddata()
                            // Go back to the main thread to update the UI
                            DispatchQueue.main.async {
                               
                                if(self.jsonresult.count > 0)
                                {
                                    if self.jsonresult.object(forKey: "LeaveAppNumber") as? String  == ""
                                    {
                                        LibraryAPI.sharedInstance.errormsg=(self.jsonresult.object(forKey: "ErrorMessage") as? String)!
                                        let alertController = UIAlertController(title: "Alert", message: "\(LibraryAPI.sharedInstance.errormsg)", preferredStyle: .alert)
                                        let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                            self.requiredtf.text = ""
                                            self.fromhhmmtf.text = ""
                                            self.tohhmmtf.text = ""
                                            self.reasontv.text = ""
                                        }
                                        alertController.addAction(cancelAction)
                                        
                                        
                                        self.present(alertController, animated: true, completion:nil)
                                    }
                                        
                                    else  if((self.jsonresult.object(forKey: "LeaveAppNumber") as? String  == nil) || (self.jsonresult.object(forKey: "ErrorMessage") as? String  == ""))
                                    {
                                        LibraryAPI.sharedInstance.appnumber=(self.jsonresult.object(forKey: "LeaveAppNumber") as! String)
                                        let alertController = UIAlertController(title: "Alert", message: "Your Ticket is Placed SuccessFully Your Ticket Number is  \(LibraryAPI.sharedInstance.appnumber)!", preferredStyle: .alert)
                                        let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "HRD1DetailsViewController")), animated: true)
                                        }
                                        alertController.addAction(cancelAction)
                                        self.present(alertController, animated: true, completion:nil)
                                    }
                                        
                                    else
                                    {
                                        let alertController = UIAlertController(title: "Alert", message:"Submisson Failed! Retry" , preferredStyle: .alert)
                                        let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                        }
                                        alertController.addAction(cancelAction)
                                        
                                        
                                        self.present(alertController, animated: true, completion:nil)
                                    }
                                    self.local_activityIndicator_stop()
                                }
                                else
                                {
                                     self.local_activityIndicator_stop()
                                    let alertController = UIAlertController(title: "Alert", message:"Submisson Failed! Retry" , preferredStyle: .alert)
                                    let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                    }
                                    alertController.addAction(cancelAction)
                                   
                                    
                                    self.present(alertController, animated: true, completion:nil)
                                }
                                
                                
                                
                                
                            }
                        }
                        
                    }
                    
                }
            }
            
        }
        
        
    }
    
}
