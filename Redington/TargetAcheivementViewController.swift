//
//  TargetAcheivementViewController.swift
//  Redington
//
//  Created by truetech on 12/08/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
import Charts
//import NVActivityIndicatorView
import KCFloatingActionButton


extension UIImage {
    func imageWithColor(tintColor: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        let context = UIGraphicsGetCurrentContext()! as CGContext
        context.translateBy(x: 0, y: self.size.height)
        context.scaleBy(x: 1.0, y: -1.0);
        context.setBlendMode(CGBlendMode.normal)
        let rect = CGRect(x: 0,y:  0, width: self.size.width,height: self.size.height) as CGRect
        context.clip(to: rect, mask: self.cgImage!)
        tintColor.setFill()
        context.fill(rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()! as UIImage
        UIGraphicsEndImageContext()
        return newImage
    }
}


class TargetAcheivementViewController: UIViewController ,ChartViewDelegate,KCFloatingActionButtonDelegate,UITableViewDataSource,UITableViewDelegate,UITabBarDelegate{
    
    
    @IBOutlet weak var Piechartviews: UIView!
    @IBOutlet var Acheivementsitem: UITabBarItem!
    @IBOutlet var tabbars: UITabBar!
    @IBOutlet var Menubtn: UIButton!
    @IBOutlet var backbtn: UIButton!
    @IBOutlet weak var titlelabel: UILabel!
    var PieVal = Float()
    var val = Float()
    var months=[String]()
    var pievalues=[Double]()
    var PiechartPercentage=NSNumber()
    var isanimated=Bool()
    var tmparray=[String]()
    var salesarray=[String]()
    var fab=KCFloatingActionButton()
    let shapelayer = CAShapeLayer()
    let shapelayer1 = CAShapeLayer()
     var responsestring1 = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.local_activityIndicator_start()
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(TargetAcheivementViewController.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeRight)
        self.titlelabel.text = "YTD (Year To date)"
        self.customnavigation()
        self.layoutFAB()
        tabbars.selectedItem=Acheivementsitem
        months=["",""]
        let num=NumberFormatter()
        num.numberStyle=NumberFormatter.Style.currency
        num.locale = NSLocale(localeIdentifier: "en_IN") as Locale!
        let str=num.string(from: NSNumber(value:LibraryAPI.sharedInstance.SalesTarget.doubleValue))
        let num1=NumberFormatter()
        num1.numberStyle=NumberFormatter.Style.currency
        num1.locale = NSLocale(localeIdentifier: "en_IN") as Locale!
        let str1=num.string(from: NSNumber(value:LibraryAPI.sharedInstance.SalesAcheived.doubleValue))
        salesarray=[str!,str1!]
        tmparray=["Sales Target  ","Sales Acheived"]
        pievalues = LibraryAPI.sharedInstance.pievalues
        //print("Pievalue: ",pievalues)
        self.setChart(values: pievalues)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.init(red:30/255.0 , green: 184/255.0, blue: 120/255.0, alpha: 1.0)], for:.selected)
        for item in self.tabbars.items! as [UITabBarItem] {
            if let image = item.image {
                item.selectedImage = image.imageWithColor(tintColor: UIColor.init(red:30/255.0 , green: 184/255.0, blue: 120/255.0, alpha: 1.0)).withRenderingMode(.alwaysOriginal)
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.local_activityIndicator_start()
        FetchValues()
    }
    func FetchValues()
    {
        DispatchQueue.global(qos: .background).async {
            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)SalesDetails.asmx/BusinessSalesTargetDetails?UserID=\(LibraryAPI.sharedInstance.Userid)&BranchCode=\(LibraryAPI.sharedInstance.BranchCodeNew)&Qtr=&Year=", completion: { (jsonmutable) in
                self.responsestring1 = jsonmutable
                if(self.responsestring1.count>0) {
                    self.local_activityIndicator_stop()
                } else {
                    self.local_activityIndicator_stop()
                }
                
            })
        }
    }
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right: break
            case UISwipeGestureRecognizerDirection.down: break
            case UISwipeGestureRecognizerDirection.left:
                self.Navigate()
            case UISwipeGestureRecognizerDirection.up: break
            default:
                break
            }
        }
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
       
    }
    
    
    
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            if (self.navigationController?.topViewController!.isKind(of: DashBoardViewController.classForCoder()) != nil) {
                self.navigationController?.popToViewController((self.navigationController?.viewControllers.first)!, animated: true)
            }
            else
            {
                let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                self.navigationController?.pushViewController(lvc, animated: false)
            }
            self.fab.close()
        }
        fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
            self.setChart(values: self.pievalues)
            self.fab.close()
        }
        fab.fabDelegate = self
        self.view.addSubview(fab)
    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
        
    }
    
    func setChart(values: [Double]) {
        PieVal = Float(values[0])
        let circlepath = UIBezierPath(arcCenter: CGPoint(x: view.frame.size.width / 2 ,y: view.frame.size.height / 4), radius: Piechartviews.frame.size.width / 4, startAngle: -CGFloat.pi / 2, endAngle: 2 * CGFloat.pi, clockwise: true)
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
        label.center = CGPoint(x: view.frame.size.width / 2 ,y: view.frame.size.height / 4)
        label.textColor = .red
        label.textAlignment = .center
        label.text = String(PieVal) + "%"
        shapelayer.path = circlepath.cgPath
        shapelayer.strokeColor = CustomColor.ShareColor.hexStringToUIColor(hex: CustomColor.ShareColor.CustomGreen, alpha: 1.0).cgColor
        shapelayer.lineWidth = 10
        shapelayer.strokeEnd = 0
        shapelayer.fillColor = UIColor.clear.cgColor
        shapelayer1.path = circlepath.cgPath
        shapelayer1.strokeColor = UIColor.red.cgColor
        shapelayer1.lineWidth = 10
        shapelayer1.strokeEnd = 1
        shapelayer1.fillColor = UIColor.clear.cgColor
        self.Piechartviews.layer.addSublayer(shapelayer1)
        self.Piechartviews.layer.addSublayer(shapelayer)
        self.Piechartviews.addSubview(label)
        animatepieview()
    }
    
    func animatepieview(){
        let baseanimate = CABasicAnimation(keyPath: "strokeEnd")
        let divVal = PieVal / 100
        print("div val: ", divVal)
        let con = Float(divVal)
        if con == 0.0 {
            val = 0.0
        }else if con < 0.1 {
            val = 0.10
        }else if con < 0.2 {
            val = 0.20
        }else if con < 0.3 {
            val = 0.30
        }else if con < 0.5 {
            val = 0.50
        } else if con < 0.6 {
            val = 0.60
        } else if con < 0.7 {
            val = 0.65
        } else if con < 0.8 {
            val = 0.70
        } else if con < 1.0 {
            val = 0.75
        } else {
            val = 1.0
        }
        baseanimate.toValue = val
        print("my val :",val)
        baseanimate.duration = 2
        baseanimate.fillMode = kCAFillModeForwards
        baseanimate.isRemovedOnCompletion = false
        shapelayer.add(baseanimate, forKey: "urSoBasic")
    }
    
    func customnavigation()
    {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y : self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(TargetAcheivementViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Sales/TargetAchievement"
    }
    
    func buttonClicked(sender:UIButton)
    {
        LibraryAPI.sharedInstance.tracker="1"
        if (self.navigationController?.topViewController!.isKind(of: DashBoardViewController.classForCoder()) != nil) {
            
            self.navigationController?.popToViewController((self.navigationController?.viewControllers.first)!, animated: true)
        } else  {
            let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
            self.navigationController?.pushViewController(lvc, animated: false)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : TargetAcheivemntTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as! TargetAcheivemntTableViewCell
        if(cell == nil) {
            cell = Bundle.main.loadNibNamed("Cell", owner: self, options: nil)![0] as! TargetAcheivemntTableViewCell;
        }
        cell.selectionStyle = .none
        cell.AcheivedLabel.text=tmparray[indexPath.row]
        cell.AmountLabel.text = "\((salesarray[indexPath.row]))"
        cell.AcheivedLabel.sizeToFit()
        if(isanimated==false) {
        }
        else
        {
        }
        return cell as TargetAcheivemntTableViewCell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastRowIndex = 1
        if(indexPath.row==lastRowIndex)
        {
            isanimated=true
        }
    }
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch item.tag {
        case 1:
            break
        case 2:
            self.Navigate()
            break
        default:
            break
        }
    }
    func Navigate()
    {
        let slideInFromLeftTransition = CATransition()
        slideInFromLeftTransition.type = kCAGravityTop
        slideInFromLeftTransition.subtype = kCATransitionFromLeft
        slideInFromLeftTransition.duration = 0.2
        slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "BusinesswiseViewController") as! BusinesswiseViewController
        if responsestring1.count > 0 {
        lvc.responsestring1 = responsestring1
        }else{
            print("response data is empty")
        }
        self.navigationController?.view.layer.add(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
        self.navigationController?.pushViewController(lvc, animated: false)
    }
   
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
