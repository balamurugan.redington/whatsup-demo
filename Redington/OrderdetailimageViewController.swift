//
//  OrderdetailimageViewController.swift
//  Whatsup
//
//  Created by truetech on 26/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import  CoreData
import KCFloatingActionButton

class OrderdetailimageViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,KCFloatingActionButtonDelegate,UITableViewDelegate,UITableViewDataSource {
    var imagePicker=UIImagePickerController()
    var responsestring = NSMutableArray()
    var imagedict=NSMutableDictionary()
    var orderno = ""
    var fab = KCFloatingActionButton()
    @IBOutlet var ordertableview: UITableView!
    var jsonresult=NSDictionary()
    @IBOutlet weak var vieworderbtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.layoutFAB()
        self.customnavigation()
        //vieworderbtn.hidden = true
        vieworderbtn.layer.cornerRadius = 5
        
              // Do any additional setup after loading the view.
        ordertableview.separatorStyle = .none
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
       
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    

    
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)

            self.fab.close()
        }
      //  fab.fabDelegate = self
        
        self.view.addSubview(fab)
        
    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
    }

    
    
    
    func Loaddata()
    {
      //  responsestring = LibraryAPI.sharedInstance.RequestUrl(url: "\(LibraryAPI.sharedInstance.GlobalUrl)orderEntry.asmx/OrderDetail?CustomerCode=\(LibraryAPI.sharedInstance.CustomerCode)&Order_Number=\(LibraryAPI.sharedInstance.orderno)")
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)orderEntry.asmx/OrderDetail?CustomerCode=\(LibraryAPI.sharedInstance.CustomerCode)&Order_Number=\(LibraryAPI.sharedInstance.orderno)", completion:  { (jsonmutable) in
            self.responsestring = jsonmutable
        })
        
            if(responsestring.count>0)
            {
                DispatchQueue.main.async {
                    
                     self.ordertableview.isHidden=false
                }
               
                
                
            }
            else
            {
               self.NormalAlert(title: "Alert", Messsage: " No Data Available")
               /* let alertController = UIAlertController(title: "Alert", message:" No Data Available" , preferredStyle: .Alert)
                
                let cancelAction = UIAlertAction(title: "Ok", style: .Cancel) { (action:UIAlertAction!) in
                    
                }
                alertController.addAction(cancelAction)
                self.presentViewController(alertController, animated: true, completion:nil)*/
        }


        
        
    }
    
    @IBAction func UploadAction(_ sender: AnyObject)
    {
        
        let alert = UIAlertController(title: "Alert", message:"What Would you like to do", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        alert.addAction(UIAlertAction(title: "TakePhoto", style: UIAlertActionStyle.destructive, handler: { (ACTION :UIAlertAction!)in
            DispatchQueue.main.async {
                
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }

                   }))
        
        
        alert.addAction(UIAlertAction(title: "Choose from Gallery", style: UIAlertActionStyle.destructive, handler: { (ACTION :UIAlertAction!)in
            self.imagePicker.sourceType = .photoLibrary
            self.OpenImageGallery()
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (ACTION :UIAlertAction!)in
            
            
            
        }))
        
        self.present(alert, animated: true, completion: nil)
 
        
        
    }
    
    
    
    func OpenImageGallery()
    {
        imagePicker.delegate=self;
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            let size = CGSize(width: 30, height:30)
            
            self.local_activityIndicator_start()

            // new version dispatch Que
            
            DispatchQueue.global(qos: .background).async {
                
                // Validate user input
                
                 self.uploadimage(pickedImage: pickedImage)
                // Go back to the main thread to update the UI
                DispatchQueue.main.async {
                    self.local_activityIndicator_stop()
                    if(self.jsonresult.object(forKey: "Status") as? String  == "Success")
                    {
                        self.DeletedatafromDb()
                        let alertController = UIAlertController(title: "Alert", message:"Order Placed successfully"  , preferredStyle: .alert)
                        
                        let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                            LibraryAPI.sharedInstance.isvalueloaded=false
                            self.navigate()
                            
                            
                        }
                        alertController.addAction(cancelAction)
                        
                        
                        
                        self.present(alertController, animated: true, completion:nil)
                    }
                    else
                        
                    {
                        let alertController = UIAlertController(title: "Alert", message:"Image Uploading Failed" , preferredStyle: .alert)
                        
                        let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                            
                            
                            
                            
                        }
                        alertController.addAction(cancelAction)
                        
                        
                        self.present(alertController, animated: true, completion:nil)
                    }
                    
                    
                    
                }
            }
            
            // new version dispatch Que
            
            
            dismiss(animated: true, completion: nil)
        }
    }
    
    
   
    func DeletedatafromDb()
    {
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let context:NSManagedObjectContext = appDel.managedObjectContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CartTable")
        
        let entityDescription = NSEntityDescription.entity(forEntityName: "CartTable", in:context)
        
        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        
        fetchRequest.returnsObjectsAsFaults = false
        
        //        let resultPredicate2 = NSPredicate(format: "itemcode = %@", "XIMP0004")
        
        
        
        do {
            let result = try context.fetch(fetchRequest)  as! [NSManagedObject]
            
            if(result.count>0)
            {
                for r in result
                {
                    context.delete(r)
                }
                
                try!context.save()
                
            }
            else
            {
            }
            
        } catch {
            let fetchError = error as NSError
        }
        
    }
    
    
    func fetchdbvalues()
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entity(forEntityName: "CartTable", in: managedContext)
        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        
        fetchRequest.returnsObjectsAsFaults = false
        
        //        let resultPredicate2 = NSPredicate(format: "itemcode = %@", "XIMP0004")
        //  fetchRequest.predicate=NSPredicate(format: "price == %@", "12")
        
        
        do {
            let result = try managedContext.fetch(fetchRequest)
            
            let itemList = result as! [NSManagedObject]
            
            
            LibraryAPI.sharedInstance.badgeCount=String(itemList.count)
            
            
            
        } catch {
            let fetchError = error as NSError
        }
        
        
        
    }

    
    
    func navigate()
    {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
    }


    func uploadimage(pickedImage:UIImage)
    {

        

       let image_data = UIImageJPEGRepresentation(pickedImage,0.6)
       //let strBase64:String = image_data!.base64EncodedStringWithOptions(.Encoding64CharacterLineLength)
        let strBase64:String = image_data!.base64EncodedString(options: .lineLength64Characters)
       imagedict.setObject(strBase64, forKey: "byt" as NSCopying)
       imagedict.setObject("test.jpeg", forKey: "fileName" as NSCopying)
       imagedict.setObject("\(LibraryAPI.sharedInstance.orderno)Photo", forKey: "ReferenceNumber" as NSCopying)
        
        let imageSize: Int = image_data!.count
       // http://edi.redingtonb2b.in/whatsup-staging/Ticket.asmx/TicketSubmission"
        
        let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)orderEntry.asmx/UploadImages?")! as URL)
        request.httpMethod = "POST"
        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
      
        request.httpBody = try? JSONSerialization.data(withJSONObject: self.imagedict, options: JSONSerialization.WritingOptions(rawValue: 0))
        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
        //  var err: NSError
        do
        {
            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
            jsonresult = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
        }
        catch (let e)
        {
            
        }

    }
    
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth,height: newHeight))
        image.draw(in: CGRect(x: 0,y: 0,width: newWidth,height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }


    
    
    func customnavigation()
        
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(OrderviewDetailsViewController.backbuttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Push Po"
        
    }
    
    func backbuttonClicked(sender:UIButton)
    {
          self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
    }
    
    //TableView Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if responsestring.count > 0
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        return responsestring.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
        var cell : OrderdetailimageTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as! OrderdetailimageTableViewCell
        if(cell == nil)
        {
            cell = Bundle.main.loadNibNamed("Cell", owner: self, options: nil)![0] as! OrderdetailimageTableViewCell;
            
        }
        let dic1 = responsestring[indexPath.row] as! NSDictionary
        if(indexPath.row%2==0)
        {
            cell.contentView.backgroundColor=(UIColor.init(red: 244/255.0, green: 242/255.0, blue: 242/255.0, alpha: 1.0))
        }
        else
        {
            cell.contentView.backgroundColor=UIColor.white
        }
        
        cell.Lineno.text=dic1.object(forKey: "Line_No") as? String
        cell.Orderno.text=dic1.object(forKey: "OrderNo") as? String
        cell.Pono.text=String( dic1.object(forKey: "PO_No") as! String)
        cell.Quantity.text=String(describing: dic1.object(forKey: "Quantity") as! NSNumber)
        cell.Orderdate.text = dic1.object(forKey: "OrderDate") as? String
        cell.Unitprice.text = String(describing: dic1.object(forKey: "UnitPrice") as! NSNumber)
        cell.Orderstatus.text = dic1.object(forKey: "OrderStatus") as? String
        cell.Itemcode.text = dic1.object(forKey: "ItemCode") as? String
        cell.Productname.text = dic1.object(forKey: "ProductName") as? String

        if(cell.Orderstatus.text=="COMPLETED")
        {
            cell.Orderstatus.textColor=UIColor.green
        }
        else
        {
            cell.Orderstatus.textColor=UIColor.red
        }
       
        cell.selectionStyle = .none
        return cell as OrderdetailimageTableViewCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
    }
    
    
    @IBAction func vieworderaction(_ sender: AnyObject)
    
    {
        //self.Loaddata()
        let size = CGSize(width: 30, height:30)
        self.local_activityIndicator_start()

        // new version dispatch Que
        
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
             self.Loaddata()
            
            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
               self.local_activityIndicator_stop()
                self.ordertableview.reloadData()
                
            }
        }
        
        // new version dispatch Que
        
    }
    
}
