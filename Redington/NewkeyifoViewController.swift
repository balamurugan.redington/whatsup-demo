//
//  NewkeyifoViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 25/01/18.
//  Copyright © 2018 truetech. All rights reserved.
//

import UIKit
import Cosmos
//import NVActivityIndicatorView
import KCFloatingActionButton

class NewkeyifoViewController: UIViewController,KCFloatingActionButtonDelegate {
    
    @IBOutlet weak var Rateview: CosmosView!
    @IBOutlet weak var salesrankinglabel: UILabel!
    @IBOutlet weak var Marginrankinglabel: UILabel!
    @IBOutlet weak var saleshealthlabel: UILabel!
    @IBOutlet weak var creditlimitlabel: UILabel!
    @IBOutlet weak var Productspreadlabel: UILabel!
    @IBOutlet weak var BusinessHealthLabel: UILabel!
    @IBOutlet weak var partner: UILabel!
    @IBOutlet weak var Insurancelabel: UILabel!
    @IBOutlet weak var exposure: UILabel!
    @IBOutlet weak var exposuredate: UILabel!
    @IBOutlet weak var lastpaymentvalue: UILabel!
    @IBOutlet weak var labelInvoceno: UILabel!
    @IBOutlet weak var highestsaleslbl: UILabel!
    @IBOutlet weak var labelInvocevalue: UILabel!
    @IBOutlet weak var labelInvocedate: UILabel!
    @IBOutlet weak var lastpaymentdate: UILabel!
    @IBOutlet weak var marginrankingview: UIView!
    @IBOutlet weak var securitychequebtn: UIButton!
    @IBOutlet weak var chequependingbtn: UIButton!
    @IBOutlet weak var Orderslabel: UILabel!
    @IBOutlet weak var Lastdate: UILabel!
    @IBOutlet weak var visitlogbtn: UIButton!
    @IBOutlet weak var AverageCreditlbl: UILabel!
    @IBOutlet weak var Spreadbtn: UIButton!
    @IBOutlet weak var Branchhold1: UILabel!
    @IBOutlet weak var branchhold2: UILabel!
    @IBOutlet weak var creditholdlbl: UILabel!
    @IBOutlet weak var credithold2lbl: UILabel!
    @IBOutlet weak var boundschq1lbl: UILabel!
    @IBOutlet weak var boundschq2lbl: UILabel!
    @IBOutlet weak var boundschqbtn: UIButton!
    @IBOutlet weak var Scrollview: UIScrollView!
    @IBOutlet weak var mainview: UIView!
    @IBOutlet weak var firstmainview: UIView!
    @IBOutlet weak var partnermainview: UIView!
    @IBOutlet weak var lastpaymentview: UIView!
    @IBOutlet weak var visitlogview: UIView!
    @IBOutlet weak var lastinvoiceview: UIView!
    @IBOutlet weak var marginrankimgview: UIImageView!
    @IBOutlet weak var salesrankimgview: UIImageView!
    @IBOutlet weak var holdview: UIView!
    
    var responsestring=NSMutableArray()
    var totDic = NSMutableDictionary()
    var responseString1 = NSMutableArray()
    var keyarr=NSMutableArray()
    var fab=KCFloatingActionButton()
    var timer = Timer()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customnavigation()
        //self.layoutFAB()
        
        Scrollview.isScrollEnabled=true
        Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:930)
        firstmainview.layer.borderColor = UIColor(red:30/255.0, green:184/255.0, blue:120/255.0, alpha: 1.0).cgColor
        firstmainview.layer.borderWidth = 1.0
        firstmainview.layer.cornerRadius = 5.0
        
        partnermainview.layer.borderColor = UIColor(red:30/255.0, green:184/255.0, blue:120/255.0, alpha: 1.0).cgColor
        partnermainview.layer.borderWidth = 1.0
        partnermainview.layer.cornerRadius = 5.0
        
        lastpaymentview.layer.borderColor = UIColor(red:30/255.0, green:184/255.0, blue:120/255.0, alpha: 1.0).cgColor
        lastpaymentview.layer.borderWidth = 1.0
        lastpaymentview.layer.cornerRadius = 5.0
        
        visitlogview.layer.borderColor = UIColor(red:30/255.0, green:184/255.0, blue:120/255.0, alpha: 1.0).cgColor
        visitlogview.layer.borderWidth = 1.0
        visitlogview.layer.cornerRadius = 5.0
        
        lastinvoiceview.layer.borderColor = UIColor(red:30/255.0, green:184/255.0, blue:120/255.0, alpha: 1.0).cgColor
        lastinvoiceview.layer.borderWidth = 1.0
        lastinvoiceview.layer.cornerRadius = 5.0
        
        holdview.layer.borderColor = UIColor(red:30/255.0, green:184/255.0, blue:120/255.0, alpha: 1.0).cgColor
        holdview.layer.borderWidth = 1.0
        holdview.layer.cornerRadius = 5.0
        
        marginrankingview.layer.borderColor = UIColor(red:30/255.0, green:184/255.0, blue:120/255.0, alpha: 1.0).cgColor
        marginrankingview.layer.borderWidth = 1.0
        marginrankingview.layer.cornerRadius = 5.0
        
        boundschqbtn.layer.cornerRadius=boundschqbtn.frame.size.width/2
        securitychequebtn.layer.cornerRadius=securitychequebtn.frame.size.width/2
        chequependingbtn.layer.cornerRadius=chequependingbtn.frame.size.width/2
        visitlogbtn.layer.cornerRadius = visitlogbtn.frame.size.width/2
        Spreadbtn.layer.cornerRadius = Spreadbtn.frame.size.width/2
        self.Loaddata()
        ParseVisitkeyinfo()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
   
    
    func BackGroundRefresh(notification: NSNotification) {
        Loaddata()
    }
    
    
    /*func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            self.fab.close()
        }
        
        fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
            self.Loaddata()
            self.fab.close()
        }
        fab.fabDelegate = self
        self.view.addSubview(fab)
    }*/
    
    
    func customnavigation() {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: 65)
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(NewkeyifoViewController.buttonClicked(_:)),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Key Information"
    }
    
    func buttonClicked(_ sender:UIButton) {
        LibraryAPI.sharedInstance.tracker="1"
        self.navigationController?.popViewController(animated: true)
    }
    
    func ParseVisitkeyinfo() {
        if(self.connectedToNetwork()) {
            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/ProductSpread?Customer=\(LibraryAPI.sharedInstance.CustomerCode)&User=\(LibraryAPI.sharedInstance.Userid)") { (jsonmutable) in
                self.keyarr = jsonmutable
            if(self.keyarr.count>0) {
                
            } else {
            }
            LibraryAPI.sharedInstance.valuearray=self.keyarr
            }
        } else {
            
        }
    }
    
    @IBAction func productSpreadbtnAction(_ sender: AnyObject) {
        if LibraryAPI.sharedInstance.valuearray.count > 0 {
            let lvc=self.storyboard?.instantiateViewController(withIdentifier: "VistlogpopupViewController") as! VistlogpopupViewController
            self.navigationController?.pushViewController(lvc, animated: true)
        } else {
            self.showalert(title: "Alert", message: "No Data Available", buttontxt: "Ok")
        }
    }
    
    
    @IBAction func visitlogbtrnAction(_ sender: AnyObject) {
        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "VisitlogdetailViewController") as! VisitlogdetailViewController
        self.navigationController?.pushViewController(lvc, animated: true)
    }
    
    
    @IBAction func chequepending(_ sender: AnyObject) {
        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "ChequeBounceDetailsViewController") as! ChequeBounceDetailsViewController
        LibraryAPI.sharedInstance.keyinfochepending = 1
        let defaults = UserDefaults.standard
        defaults.set("2", forKey: "ViewCheckBool")
        self.navigationController?.pushViewController(lvc, animated: true)
    }
    
    
    @IBAction func securitychequeAction(_ sender: AnyObject) {
        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "SecurityViewController") as! SecurityViewController
        self.navigationController?.pushViewController(lvc, animated: true)
    }
    
    @IBAction func chequebouncebtn(_ sender: AnyObject) {
        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "ChequeBounceViewController") as! ChequeBounceViewController
        self.navigationController?.pushViewController(lvc, animated: true)
    }
    
    
    func Loaddata() {
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
           // self.responsestring = LibraryAPI.sharedInstance.Keyinfoparse(url: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/Customer360degreeview?Customer=\(LibraryAPI.sharedInstance.CustomerCode)&User=\(LibraryAPI.sharedInstance.Userid)")
            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/Customer360degreeview?Customer=\(LibraryAPI.sharedInstance.CustomerCode)&User=\(LibraryAPI.sharedInstance.Userid)", completion:  { (jsonmutable) in
                self.responsestring = jsonmutable
                print("value of key information",self.responsestring)
            DispatchQueue.main.async {
                if(self.responsestring.count > 0) {
                    self.local_activityIndicator_stop()
                    let rating=((self.responsestring.object(at: 0) as AnyObject).object(forKey: "SALES FREQUENCY") as? Double)!
                    self.Rateview.settings.starSize=20
                    self.Rateview.settings.totalStars=5
                    self.Rateview.rating=rating
                    self.Rateview.settings.updateOnTouch=false
                    
                    let num=NumberFormatter()
                    num.numberStyle=NumberFormatter.Style.currency
                    num.locale = NSLocale(localeIdentifier: "en_IN") as Locale!
                    let str=num.string(from: NSNumber(value:((self.responsestring.object(at: 0) as AnyObject).object(forKey: "HIGHEST SALES VALUE") as! Double)))
                    
                    self.highestsaleslbl.text = str
                    self.Marginrankinglabel.text = String((self.responsestring.object(at: 0) as AnyObject).object(forKey: "MARGIN RANKING")as! Int)
                    self.salesrankinglabel.text = String((self.responsestring.object(at: 0) as AnyObject).object(forKey: "TURNOVER RANKING")as! Int)
                    let Partnerprofile = (self.responsestring.object(at: 0) as AnyObject).object(forKey: "PARTNER PROFILE") as? String
                    let profiledesc = (self.responsestring.object(at: 0) as AnyObject).object(forKey: "PROFILE DESC") as? String
                    self.partner.text = Partnerprofile!+" "+"-"+profiledesc!+" "
                    self.saleshealthlabel.text = (self.responsestring.object(at: 0) as AnyObject).object(forKey: "SALES HEALTH")as? String
                    self.BusinessHealthLabel.text  = (self.responsestring.object(at: 0) as AnyObject).object(forKey: "BUSINESS HEALTH")as? String
                    self.Productspreadlabel.text = String((self.responsestring.object(at: 0) as AnyObject).object(forKey: "PRODUCT SPREAD")as! Int)
                    
                    let num1=NumberFormatter()
                    num1.numberStyle=NumberFormatter.Style.currency
                    num1.locale = NSLocale(localeIdentifier: "en_IN") as Locale!
                    let str1=num1.string(from: NSNumber(value:((self.responsestring.object(at: 0) as AnyObject).object(forKey: "CREDIT LIMIT") as? Double)!))
                    self.creditlimitlabel.text = str1
                    let str2 = num1.string(from: NSNumber(value:((self.responsestring.object(at: 0) as AnyObject).object(forKey: "INSURANCE LIMIT") as? Double)!))
                    self.Insurancelabel.text = str2
                    self.exposuredate.text = (self.responsestring.object(at: 0) as AnyObject).object(forKey: "HIGHST EXPOSURE DATE")as? String
                    self.exposure.text = String((self.responsestring.object(at: 0) as AnyObject).object(forKey: "HIGHEST EXPOSURE")as! Double)
                    self.lastpaymentvalue.text = String((self.responsestring.object(at: 0) as AnyObject).object(forKey: "LAST PAYMENT VALUE") as! Double)
                    self.lastpaymentdate.text = (self.responsestring.object(at: 0) as AnyObject).object(forKey: "LAST PAYMENT DATE") as! String
                    self.labelInvoceno.text = (self.responsestring.object(at: 0) as AnyObject).object(forKey: "LAST INVOICE NO")as! String
                    self.labelInvocedate.text = (self.responsestring.object(at: 0) as AnyObject).object(forKey: "LAST INVOICE DATE")as? String
                    self.labelInvocevalue.text =  String((self.responsestring.object(at: 0) as AnyObject).object(forKey: "LAST INVOICE VALUE") as! Double)
                    //self.lastpaymentdate.text =
                    self.Lastdate.text = (self.responsestring.object(at: 0) as AnyObject).object(forKey: "LAST PAYMENT DATE") as! String
                    
                    let time = (self.responsestring.object(at: 0) as AnyObject).object(forKey:"FTME7W") as? String
                    let date=(self.responsestring.object(at: 0) as AnyObject).object(forKey:"DATV7W") as? String
                    if time == "" {
                        self.visitlogbtn.isHidden=true
                    } else {
                        self.visitlogbtn.isHidden=false
                    }
                    self.lastpaymentdate.text =  date! + " " + time!
                    self.Orderslabel.text = String((self.responsestring.object(at: 0) as AnyObject).object(forKey:"TOTAL ORDER COUNT")as! Int)
                    self.AverageCreditlbl.text = String((self.responsestring.object(at: 0) as AnyObject).object(forKey:"AVERAGE CREDIT DAYS")as! Int)
                    self.Branchhold1.text = String((self.responsestring.object(at: 0) as AnyObject).object(forKey:"NO OF BRANCH HOLD")as! Int)
                    var str7 = String((self.responsestring.object(at: 0) as AnyObject).object(forKey:"NO OF BRANCH HOLD %") as! Double)
                    str7 = str7 + "%"
                    self.branchhold2.text=str7
                    self.creditholdlbl.text = String((self.responsestring.object(at: 0) as AnyObject).object(forKey:"NO OF CREDIT HOLD")as! Int)
                    
                    var str5 = String((self.responsestring.object(at: 0) as AnyObject).object(forKey: "NO OF CREDIT HOLD %") as! Double)
                    str5 = str5 + "%"
                    self.credithold2lbl.text=str5
                    
                    self.boundschq1lbl.text = String((self.responsestring.object(at: 0) as AnyObject).object(forKey: "BOUNCE CHEQUE COUNT")as! Int)
                    
                    var str6 = String((self.responsestring.object(at: 0) as AnyObject).object(forKey: "BOUNCE CHEQUE COUNT%") as! Int)
                    
                    str6 = str6 + "%"
                    self.boundschq2lbl.text=str6
                    
                    if((self.responsestring.object(at: 0) as AnyObject).object(forKey: "MARGIN FLAG") as? String == "U") {
                        self.marginrankimgview.image=UIImage(named: "1like.png")
                    } else {
                        self.marginrankimgview.image=UIImage(named: "1unlike.png")
                    }
                    
                    if((self.responsestring.object(at: 0) as AnyObject).object(forKey: "TURNOVER FLAG") as? String == "U") {
                        self.salesrankimgview.image=UIImage(named: "1like.png")
                    } else {
                        self.salesrankimgview.image=UIImage(named: "1unlike.png")
                    }
                } else {
                    self.local_activityIndicator_stop()
                    let alertController = UIAlertController(title: "Alert", message: "No Data Available" , preferredStyle: .alert)
                    let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                        let lvc = self.storyboard?.instantiateViewController(withIdentifier: "Dashboard360NewViewController") as! Dashboard360NewViewController
                        self.navigationController?.pushViewController(lvc, animated: true)
                    }
                    alertController.addAction(cancelAction)
                    self.present(alertController, animated: true, completion:nil)
                }
            }
                 })
        }
    }
}
