//
//  MiTableViewCell.swift
//  Whatsup
//
//  Created by truetech on 22/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit

class MiTableViewCell: UITableViewCell {

   
    @IBOutlet weak var cellview: UIView!
    @IBOutlet var Customername: UILabel!
    @IBOutlet var Customcode: UILabel!
    @IBOutlet var Lastinvoice: UILabel!
    @IBOutlet var Balancedue: UILabel!
    @IBOutlet var Lastsalevalue: UILabel!
    @IBOutlet var Lastsaledate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
