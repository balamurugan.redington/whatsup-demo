//
//  InvoicesummerydetailsTableViewCell.swift
//  Redington
//
//  Created by truetech on 28/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit

class InvoicesummerydetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var cellview: UIView!
    @IBOutlet var Name: UILabel!
    @IBOutlet var Customercode: UILabel!
    @IBOutlet var invoice: UILabel!
    @IBOutlet var due: UILabel!
    @IBOutlet var comitteddate: UILabel!
    @IBOutlet var comittedamount: UILabel!
    @IBOutlet var value: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
