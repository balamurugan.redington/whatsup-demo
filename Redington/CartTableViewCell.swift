//
//  CartTableViewCell.swift
//  Redington
//
//  Created by truetech on 31/08/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit

class CartTableViewCell: UITableViewCell {

    @IBOutlet var vendorid: UILabel!
    @IBOutlet var Itemcode: UILabel!
    @IBOutlet var Pricetf: UILabel!
    @IBOutlet var Qntytf: UITextField!
    @IBOutlet var Removebtn: UIButton!
    @IBOutlet var Productlabel: UILabel!
    
   // @IBOutlet weak var Pricetf: UITextField!
    @IBOutlet weak var stockroom: UILabel!
    
   
    @IBOutlet weak var Sview: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func addDoneButtonOnKeyboardPrice()
    {
        Qntytf.keyboardType = .numbersAndPunctuation
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0,y: 0,width: 320,height: 50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(OrderTableViewCell.doneButtonPriceAction))
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as AnyObject as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        Qntytf.inputAccessoryView = doneToolbar
        
        
    }
    func doneButtonPriceAction()
    {
        
        Qntytf.resignFirstResponder()
        
    }


}
