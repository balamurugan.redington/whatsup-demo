//
//  BarChart11CollectionViewCell.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 11/12/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit
import Charts
//import Funnel

class BarChart11CollectionViewCell: UICollectionViewCell,ChartViewDelegate {
    
    @IBOutlet weak var ChartView: BarChartView!
    
    @IBOutlet weak var chartbtn: UIButton!
   
    @IBOutlet weak var monthlbl: UILabel!
    var   Array30 = [Double]()
    var   array60 = [Double]()
    var   array90 = [Double]()
    var   array120 = [Double]()
    var   arraymorethan120=[Double]()
    var monthsbar=[String]()
    var MontnDictionary = [String: AnyObject]()
    var montharray=NSMutableArray()
    var responsestring2 = NSMutableArray()
    var count = NSInteger()
    var timer = Timer()


    override func awakeFromNib() {
        super.awakeFromNib()
        ChartView.delegate = self;
        //ChartView.descriptionText = "";
        ChartView.noDataText = "You need to provide data for the chart."
        ChartView.drawBarShadowEnabled = false;
        ChartView.drawGridBackgroundEnabled = false;
        ChartView.leftAxis.drawZeroLineEnabled = true
        ChartView.setScaleEnabled(true)
        let xAxis: XAxis = ChartView.xAxis
        xAxis.getLongestLabel()
        xAxis.avoidFirstLastClippingEnabled=true
        xAxis.axisLineWidth = 1
        let legend = ChartView.legend;
        legend.position = .belowChartRight;
        legend.enabled=false
        _ = ChartView.xAxis;
        let leftAxis = ChartView.leftAxis;
        leftAxis.drawGridLinesEnabled = true;
        leftAxis.spaceTop = 0.25;
        ChartView.rightAxis.enabled = false;
    }
    
    
    func setChartBarGroupDataSet(dataPoints: [String], values: [Double], values2: [Double],values3:[Double],values4:[Double],values5:[Double],sortIndex:Int) {
        
        var dataEntries: [BarChartDataEntry] = []
        var dataEntries2: [BarChartDataEntry] = []
        var dataEntries3: [BarChartDataEntry] = []
        var dataEntries4: [BarChartDataEntry] = []
        var dataEntries5: [BarChartDataEntry] = []
        var de: [BarChartDataEntry] = []
        var colors: [UIColor] = []
        var colors1: [UIColor] = []
        var colors2: [UIColor] = []
        var colors3: [UIColor] = []
        var colors4: [UIColor] = []
        
        for i in 0..<dataPoints.count {
            let dataEntry = BarChartDataEntry(x: Double(0), yValues: [values[i]])
            dataEntries.append(dataEntry)
        }
        
        
        for i in 0..<dataPoints.count {
            let dataEntry = BarChartDataEntry(x: Double(1), yValues: [values2[i]])
            dataEntries2.append(dataEntry)
        }
        
        for i in 0..<dataPoints.count {
            let dataEntry = BarChartDataEntry(x: Double(2), yValues: [values3[i]])
            dataEntries3.append(dataEntry)
        }
        
        
        for i in 0..<dataPoints.count {
            let dataEntry = BarChartDataEntry(x: Double(3), yValues: [values4[i]])
            dataEntries4.append(dataEntry)
        }
        
        for i in 0..<dataPoints.count {
            let dataEntry = BarChartDataEntry(x: Double(4), yValues: [values5[i]])
            dataEntries5.append(dataEntry)
        }
        
        
        for i in 0..<dataPoints.count
        {
            let dr = BarChartDataEntry(x: Double(i), yValues: [values[i],values2[i],values3[i],values4[i],values5[i]])
            de.append(dr)
            
        }
        
        _ = BarChartDataSet(values: de, label: "Units Sold")
        let chartDataSet1 = BarChartDataSet(values: de, label: "")
        chartDataSet1.stackLabels=["0-30","30-60","60-90","90-120",">120"]
        let chartDataSet = BarChartDataSet(values: dataEntries, label: " ")
        let chartDataSet2 = BarChartDataSet(values: dataEntries2, label: " ")
        let chartDataSet3 = BarChartDataSet(values: dataEntries3, label: " ")
        let chartDataSet4 = BarChartDataSet(values: dataEntries4, label: " ")
        let chartDataSet5 = BarChartDataSet(values: dataEntries5, label: " ")
        ChartView.leftAxis.axisMinValue=2.0
        ChartView.xAxis.labelPosition = .bottom
        
        for _ in 0..<dataPoints.count {
            
            
            colors.append(UIColor.init(red: 181/255.0, green: 255/255.0, blue: 122/255.0, alpha: 1.0))
        }
        
        for _ in 0..<dataPoints.count {
            
            
            colors1.append(UIColor.init(red: 247/255.0, green: 240/255.0, blue: 122/255.0, alpha: 1.0))
        }
        for _ in 0..<dataPoints.count {
            
            
            colors2.append(UIColor.init(red: 254/255.0, green: 198/255.0, blue: 122/255.0, alpha: 1.0))
        }
        
        for _ in 0..<dataPoints.count {
            
            
            colors3.append(UIColor.init(red: 120/255.0, green: 230/255.0, blue: 255/255.0, alpha: 1.0))
        }
        for _ in 0..<dataPoints.count {
            
            
            colors4.append(UIColor.init(red: 252/255.0, green: 111/255.0, blue: 135/255.0, alpha: 1.0))
        }
        
        
        chartDataSet.colors = colors
        chartDataSet2.colors = colors1
        chartDataSet3.colors = colors2
        chartDataSet4.colors = colors3
        chartDataSet5.colors = colors4
        
        
        let dataSets:[BarChartDataSet]=[chartDataSet,chartDataSet2,chartDataSet3,chartDataSet4,chartDataSet5]
        let data = BarChartData(dataSets: dataSets)
        ChartView.data = data
        ChartView.xAxis.labelPosition = .bottom
        ChartView.data?.setValueTextColor(UIColor.white)
        ChartView.rightAxis.enabled=false
        ChartView.xAxis.drawAxisLineEnabled = true
        ChartView.descriptionText = ""
        ChartView.rightAxis.drawGridLinesEnabled = true
        
        ChartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .easeInBounce)
        
        
    }

}


