//
//  StockEnquiryAllLocationViewController.swift
//  Redington
//
//  Created by TRUETECHMAC3 on 28/12/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit

class StockEnquiryAllLocationViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var labelItemCode: UILabel!
    @IBOutlet var labelVendorNo: UILabel!
    @IBOutlet var labelDesc: UILabel!
    @IBOutlet var tabelViewLocation: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.customnavigation()
        let dic = LibraryAPI.sharedInstance.StockAllLocationArry[0] as! NSDictionary

        labelItemCode.text = (dic.object(forKey: "ITEM CODE") as? String)!
        labelDesc.text = (dic.object(forKey: "ITEM DESCRIPTION") as? String)!
        labelVendorNo.text = (dic.object(forKey: "VENDOR PARTNER NO") as? String)!
        self.tabelViewLocation.reloadData()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    //TableView Delegates
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        if (LibraryAPI.sharedInstance.StockAllLocationArry.count > 0)
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return LibraryAPI.sharedInstance.StockAllLocationArry.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell : StockAllLocationTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as! StockAllLocationTableViewCell
        if(cell == nil)
        {
            cell = Bundle.main.loadNibNamed("Cell", owner: self, options: nil)![0] as! StockAllLocationTableViewCell;
            
        }
        let dicSarray = LibraryAPI.sharedInstance.StockAllLocationArry[indexPath.row] as! NSDictionary
        if(indexPath.row % 2 == 0)
        {
            cell.contentView.backgroundColor=(UIColor.init(red: 240/255.0, green: 246/255.0, blue: 225/255.0, alpha: 1.0))
        }
        else
        {
            //cell.contentView.backgroundColor=UIColor.whiteColor()
            cell.contentView.backgroundColor=(UIColor.init(red: 215/255.0, green: 250/255.0, blue: 252/255.0, alpha: 1.0))
        }
        
        cell.labelSRCode.text = String(dicSarray.object(forKey: "STOCKROOM CODE") as! String)
        cell.labelSRDesc.text = String(dicSarray.object(forKey: "STOCKROOM DESC") as! String)
        cell.labelSRUnits.text = String(dicSarray.object(forKey: "UNIT AVALIABLE") as! Double)
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    
    func customnavigation()
    {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(OrderviewDetailsViewController.backbuttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Stock Enquiry all Location"
        
    }
    
    func backbuttonClicked(sender:UIButton)
    {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
    }



    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
