import UIKit
import CoreData
import Foundation
import CoreLocation
import UserNotifications
import UserNotificationsUI
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import Fabric
import Crashlytics
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate,MessagingDelegate,CLLocationManagerDelegate  {
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        let dirPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        print("App Path: \(dirPaths)")
            LibraryAPI.sharedInstance.internetcheck()
            BackgroundLocationManager.instance.start()
            Fabric.with([Crashlytics.self])
            self.initializeFCM(application)
        return true
    }
    func initializeFCM(_ application: UIApplication) {
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.badge, .alert, .sound]) { (accepted, _) in
                if !accepted {
                    //  print("Notification access denied.")
                } else {
                    //print("Notification access accepted.")
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        } else {
            let type: UIUserNotificationType = [UIUserNotificationType.badge, UIUserNotificationType.alert, UIUserNotificationType.sound]
            let setting = UIUserNotificationSettings(types: type, categories: nil)
            UIApplication.shared.registerUserNotificationSettings(setting)
            UIApplication.shared.registerForRemoteNotifications()
        }
         UIApplication.shared.registerForRemoteNotifications()
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        Messaging.messaging().shouldEstablishDirectChannel = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification),
                                               name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
        
      
    }
    
   
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        if notificationSettings.types == .alert || notificationSettings.types == .badge || notificationSettings.types == .sound {
            application.registerForRemoteNotifications()
        }
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        //let tokenChars = UnsafePointer<CChar>(deviceToken.bytes)
        let token = String(format: "%@", deviceToken as CVarArg)
        debugPrint("*** deviceToken: \(token)")
        Messaging.messaging().apnsToken = deviceToken as Data
      
    }
    
    func tokenRefreshNotification(_ notification: NSNotification) {
        // NOTE: It can be nil here
       // _ = InstanceID.instanceID().token()
        connectToFcm()
        if let token = Messaging.messaging().fcmToken {
            print("InstanceFCMID token: \(token)")
        }
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
            }
        }
       /* if let refreshedToken =   InstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
        }*/
    }
    
    func connectToFcm() {
        
        guard InstanceID.instanceID().token() != nil else {
            return
        }
        
        // Messaging.messaging().disconnect()
        Messaging.messaging().shouldEstablishDirectChannel = false
        Messaging.messaging().connect { (error) in
            if (error != nil) {
            
            } else {
        
            }
        }
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        Messaging.messaging().appDidReceiveMessage(userInfo)
        let userdefaults = Foundation.UserDefaults.standard
        userdefaults.set(userInfo, forKey: "Notification")
        //NotificationCenter.default.postNotificationName(NSNotification.Name(rawValue: "GetNotification"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GetNotification"), object: nil)
    }
    func applicationReceivedRemoteMessage(remoteMessage: MessagingRemoteMessage)
        
    {
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
               }
    func applicationDidEnterBackground(_ application: UIApplication) {
         connectToFcm()
         BackgroundLocationManager.instance.start()
    }
    func applicationWillEnterForeground(_ application: UIApplication) {
        connectToFcm()
    }
    func applicationDidBecomeActive(_ application: UIApplication) {
        connectToFcm()
        application.applicationIconBadgeNumber = 0;
        LibraryAPI.sharedInstance.internetcheck()
    }
    func applicationWillTerminate(_ application: UIApplication) {
         BackgroundLocationManager.instance.start()
    }
    lazy var applicationDocumentsDirectory: NSURL = {
        
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.karya.CoreDataTask" in the application's documents Application Support directory.
        
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        
        return urls[urls.count-1] as NSURL
        
    }()
    lazy var managedObjectModel: NSManagedObjectModel = {
        
        
        let modelURL = Bundle.main.url(forResource: "Coredatamodel", withExtension: "momd")!
        
        return NSManagedObjectModel(contentsOf: modelURL)!
        
    }()
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("Coredatamodel.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            let options = [NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: true]
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject
            dict[NSUnderlyingErrorKey] = error as? NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
    return coordinator
    }()
    lazy var managedObjectContext: NSManagedObjectContext = {
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
}
