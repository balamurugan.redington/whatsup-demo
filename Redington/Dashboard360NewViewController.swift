//
//  Dashboard360NewViewController.swift
//  Whatsup
//
//  Created by truetech on 22/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import KCFloatingActionButton
class Dashboard360NewViewController: UIViewController,KCFloatingActionButtonDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet var CustomerName: UILabel!
    @IBOutlet var DashLogo: UIImageView!
    @IBOutlet var backbtn: UIButton!
    @IBOutlet var menubtn: UIButton!
    @IBOutlet var Ageinganalysisview: UIView!
    @IBOutlet var Salesanalysisview: UIView!
    @IBOutlet var Organisationview: UIView!
    @IBOutlet var Keyinfoview: UIView!
    var collectionViewLayout: CustomImageFlowLayout!
    @IBOutlet var Dashboard360Collectionviewcell: UICollectionView!
    var imagearray=[String]()
    var logoimagearray=[String]()
    var titlearray=[String]()
    var responsestring=NSMutableArray()
    var responsestring1=NSMutableArray()
    
    @IBOutlet var Balancelabel: UILabel!
    @IBOutlet var Credilimitlabel: UILabel!
    @IBOutlet var Statuslabel: UILabel!
    @IBOutlet var Customercode: UILabel!
    @IBOutlet var orgWithconst: NSLayoutConstraint!
    var fab=KCFloatingActionButton()
    
    @IBOutlet var ageingxcontsant: NSLayoutConstraint!
    @IBOutlet var keycenterxconstant: NSLayoutConstraint!
    override func viewDidDisappear(_ animated: Bool) {
        Ageinganalysisview.alpha=0.0
        Salesanalysisview.alpha=0.0
        Organisationview.alpha=0.0
        Keyinfoview.alpha=0.0
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.iPhoneScreenSizes()
        self.layoutFAB()
        LibraryAPI.sharedInstance.tracker="1"
        //self.Loaddata()
        Customercode.text=LibraryAPI.sharedInstance.CustomerCode
        collectionViewLayout = CustomImageFlowLayout()
        Dashboard360Collectionviewcell.collectionViewLayout = collectionViewLayout
        Dashboard360Collectionviewcell.backgroundColor = UIColor.clear
        self.customnavigation()
        if(UIScreen.main.bounds.size.height == 480)
        {
            
        }
        else if(UIScreen.main.bounds.size.height == 568)
        {
            //            orgWithconst.constant = 110
        }
        imagearray.insert("org.png", at: 0)
        imagearray.insert("keyy.png", at: 1)
        imagearray.insert("ag.png", at: 2)
        imagearray.insert("saa.png",at: 3)
        
        logoimagearray.insert("orglogo.png", at: 0)
        logoimagearray.insert("keylogo.png", at: 1)
        logoimagearray.insert("aslogo.png", at: 2)
        logoimagearray.insert("salogo.png",at: 3)
        
        
        titlearray.insert("Organisation Details", at: 0)
        titlearray.insert("key Information", at: 1)
        titlearray.insert("Ageing Analysis", at: 2)
        titlearray.insert("Sales Analysis", at: 3)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(Dashboard360NewViewController.touchHappen))
        Organisationview.addGestureRecognizer(tap)
        
        let tap1: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(Dashboard360NewViewController.touchHappen1))
        Keyinfoview.addGestureRecognizer(tap1)
        
        
        let tap2: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(Dashboard360NewViewController.touchHappen2))
        Ageinganalysisview.addGestureRecognizer(tap2)
        
        
        let tap3: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(Dashboard360NewViewController.touchHappen3))
        Salesanalysisview.addGestureRecognizer(tap3)
        
        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    func iPhoneScreenSizes(){
        let bounds = UIScreen.main.bounds
        let height = bounds.size.height
        
        switch height {
        case 480.0: break
        case 568.0: break
        case 667.0:
            keycenterxconstant.constant=28
            ageingxcontsant.constant = -28
        case 736.0:
            keycenterxconstant.constant=45
            ageingxcontsant.constant = -45
            
            
        default: break
            
        }
        
        
    }
    
    func Loaddata()
    {
        
        // new version dispatch Que
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
            // self.responsestring = LibraryAPI.sharedInstance.Dashboardurlcall(url: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer360.asmx/Customersearch360?Customer=\(LibraryAPI.sharedInstance.CustomerCode)&Type=1&Userid=\(LibraryAPI.sharedInstance.Userid)&Branch=\(LibraryAPI.sharedInstance.PartnerBranchCodetf)")
            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer360.asmx/Customersearch360?Customer=\(LibraryAPI.sharedInstance.CustomerCode)&Type=1&Userid=\(LibraryAPI.sharedInstance.Userid)&Branch=\(LibraryAPI.sharedInstance.PartnerBranchCodetf)", completion: { (jsonmutable) in
                self.responsestring = jsonmutable
            
            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
                
                
                if(self.responsestring.count>0)
                {
                    self.local_activityIndicator_stop()
                    let dic = self.responsestring[0] as! NSDictionary
                    self.Customercode.text=dic.object(forKey: "Customer Code") as? String
                    // self.Statuslabel.text=self.responsestring.objectAtIndex(0).objectForKey("STAT5W") as? String
                    
                    self.Credilimitlabel.text=String(describing: dic.object(forKey: "CRLM5W") as! NSNumber)
                    self.CustomerName.text=String(dic.object(forKey: "Customer Name") as! String)
                    if(dic.object(forKey: "STAT5W") as? String == "")
                    {
                        self.DashLogo.image=UIImage(named:"60-x-60.png")
                        self.CustomerName.textColor=UIColor.white
                        self.Statuslabel.text = "Open"
                        
                    }
                    else
                    {
                        self.DashLogo.image=UIImage(named:"statusred.png")
                        self.CustomerName.textColor=UIColor.red
                        self.Statuslabel.text=dic.object(forKey: "STAT5W") as? String
                    }
                    
                    LibraryAPI.sharedInstance.tracker="1"
                    // responsestring=LibraryAPI.sharedInstance.Organisationarray
                    self.Balancelabel.text=String(dic.object(forKey: "AVCR5W")  as!  Double)
                    self.local_activityIndicator_stop()
                    
                }else
                {
                    self.local_activityIndicator_stop()
                    //                    self.showToast("No Data Found")
                }
            }
                })
        }
        
        // new version dispatch Que
        
        
        
    }
    
    func customnavigation()
        
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(Dashboard360NewViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "360 View"
    }
    
    
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            
            
            
            self.fab.close()
        }
        
        fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
            
            self.fab.close()
        }
        
        
        self.view.addSubview(fab)
        
    }
    
    //func KCFABOpened(fab: KCFloatingActionButton) {
    
    // }
    
    
    
    func touchHappen() {
        let lvc = self.storyboard?.instantiateViewController(withIdentifier: "OranganzationDetailsViewController") as! OranganzationDetailsViewController
        LibraryAPI.sharedInstance.tracker="2"
        self.navigationController?.pushViewController(lvc, animated: true)
        
    }
    
    func touchHappen1()
        
    {
        self.local_activityIndicator_start()
        // new version dispatch Que
        
        DispatchQueue.global(qos: .background).async {
            
            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/Customer360degreeview?Customer=\(LibraryAPI.sharedInstance.CustomerCode)&User=\(LibraryAPI.sharedInstance.Userid)", completion:  { (jsonmutable) in
                self.responsestring1 = jsonmutable
                DispatchQueue.main.async {
                    if(self.responsestring1.count>0)
                    {
                        let lvc = self.storyboard?.instantiateViewController(withIdentifier: "NewkeyifoViewController") as! NewkeyifoViewController
                        LibraryAPI.sharedInstance.tracker="2"
                        self.navigationController?.pushViewController(lvc, animated: true)
                    }
                    else
                    {
                        self.local_activityIndicator_stop()
                        let alertController = UIAlertController(title: "Alert", message: "No Data Available" , preferredStyle: .alert)
                        let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                            
                        }
                        alertController.addAction(cancelAction)
                        self.present(alertController, animated: true, completion:nil)
                        
                    }
                    
                }
            })
        }
        
        // new version dispatch Que
        
        
        
        
    }
    
    
    func touchHappen2() {
        let lvc = self.storyboard?.instantiateViewController(withIdentifier: "AgeingAnalysisViewController") as! AgeingAnalysisViewController
        LibraryAPI.sharedInstance.tracker="2"
        self.navigationController?.pushViewController(lvc, animated: true)
    }
    
    
    func touchHappen3() {
        let lvc = self.storyboard?.instantiateViewController(withIdentifier: "SalesanalysisViewController") as! SalesanalysisViewController
        LibraryAPI.sharedInstance.tracker="2"
        self.navigationController?.pushViewController(lvc, animated: true)
    }
    
    func buttonClicked(sender:UIButton)
    {
        if (LibraryAPI.sharedInstance.tracker1 == 1)
        {
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "CustomerSearchPinCodeViewController")), animated: true)
            LibraryAPI.sharedInstance.lastviewcontroller="CustomerSearchPinCodeViewController"
            self.sideMenuViewController!.hideMenuViewController()
        }
        else if(LibraryAPI.sharedInstance.BranchCodeCheck  == 4)
        {
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "CustomerSearchPinCodeViewController")), animated: true)
            LibraryAPI.sharedInstance.lastviewcontroller="CustomerSearchPinCodeViewController"
            self.sideMenuViewController!.hideMenuViewController()
            
        }
        else
        {
            LibraryAPI.sharedInstance.tracker="1"
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            LibraryAPI.sharedInstance.lastviewcontroller="Dashboardviewcontroller"
            self.sideMenuViewController!.hideMenuViewController()
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.Loaddata()
        
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if(LibraryAPI.sharedInstance.tracker=="1")
        {
            
            //let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            //appdelegate.shouldSupportAllOrientation = false
            
            
            
            let orientation = UIDevice.current.orientation
            if (UIDeviceOrientationIsLandscape(orientation)){
                UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
                
                
            }
        }
        
        
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        //        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        //        appdelegate.shouldSupportAllOrientation = false
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        Ageinganalysisview.alpha=1.0
        Salesanalysisview.alpha=1.0
        Organisationview.alpha=1.0
        Keyinfoview.alpha=1.0
    }
    
    //Collectionview delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! Dashboard360CollectionViewCell
        cell.labellogi.image=UIImage(named:imagearray[indexPath.row])
        cell.logoimg.image=UIImage(named:logoimagearray[indexPath.row])
        cell.label.text=titlearray[indexPath.row]
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(indexPath.row==0)
        {
            return CGSize(width: 182, height:250)
        }
        else  if(indexPath.row==1)
        {
            return CGSize(width: 135, height:250)
        }
        else  if(indexPath.row==2)
        {
            return CGSize(width: 135, height:250)
        }
        else
        {
            return CGSize(width: 182, height:250)
        }
        
    }
    
    
    
    
    
    
}
