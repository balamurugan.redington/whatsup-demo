//  DashBoardViewController.swift
//  Redington
//  Created by truetech on 05/08/16.
//  Copyright © 2016 truetech. All rights reserved.
import UIKit
import Charts
import CoreData
import CoreLocation
import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import KCFloatingActionButton
import SystemConfiguration
import Alamofire

class DashBoardViewController: UIViewController, ChartViewDelegate, UINavigationControllerDelegate, KCFloatingActionButtonDelegate, UIGestureRecognizerDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var notificationcountlbl: UILabel!
    @IBOutlet var Hideview1: UIView!
    @IBOutlet var HomeTableview: UITableView!
    @IBOutlet var DateLabel: UILabel!
    @IBOutlet var btn1: UIButton!
    @IBOutlet weak var Notificaitonbtn: UIButton!
    @IBOutlet var menubtn: UIButton!
    @IBOutlet weak var btnDropDown: UIButton!
    @IBOutlet weak var viewDropDown: UIView!
    @IBOutlet weak var tableViewBranches: UITableView!
    @IBOutlet var branchImage: UIImageView!
    @IBOutlet var viewdropdownSubView: UIView!
    @IBOutlet var viewTapDropdown: UIView!
    @IBOutlet var labelBranchecode: UILabel!
    @IBOutlet weak var brlabel: UILabel!
    @IBOutlet weak var branchesbtn: UIButton!
    @IBOutlet weak var cusview: UIView!
    @IBOutlet weak var miSbtn: UIButton!
    @IBOutlet weak var attnbtn: UIButton!
    @IBOutlet weak var Crmbtn: UIButton!
    var unitsBought = [20.0, 4.0, 6.0, 3.0, 12.0,6.0, 3.0, 12.0]
    var fab = KCFloatingActionButton()
    var pieresponsestring = NSMutableArray()
    var responseArray = NSMutableArray()
    var responsestring2 = NSMutableArray()
    var responsestring3 = NSMutableArray()
    var viewControllerToInsertBelow : UIViewController?
    var locationManager=CLLocationManager()
    var latitude : Double!
    var longitude : Double!
    var responsestring : NSMutableDictionary!
    var current_area: String!
    var current_pin: String!
    var months=[String]()
    var monthsbar=[String]()
    var brnchcode=[String]()
    var brnchname=[String]()
    var months1=[String]()
    var unitsSold = [Double]()
    var UnitsforbarChart=[Double]()
    var Array30 = [Double]()
    var array60 = [Double]()
    var array90 = [Double]()
    var array120 = [Double]()
    var arraymorethan120=[Double]()
    var pievalues=[Double]()
    var isanimated=Bool()
    var timestamp = String()
    var PiechartPercentage=NSNumber()
    var breadthvaluearray=NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        HomeTableview.register(UINib(nibName: "TagetAchieveTableViewCell", bundle: nil), forCellReuseIdentifier: "TagetAchieveTableViewCell")
        HomeTableview.register(UINib(nibName: "OverDueTableViewCell", bundle: nil), forCellReuseIdentifier: "OverDueTableViewCell")
        HomeTableview.register(UINib(nibName: "BreadthTableViewCell", bundle: nil), forCellReuseIdentifier: "BreadthTableViewCell")
        HomeTableview.tableFooterView = UIView(frame: .zero)
        HomeTableview.separatorStyle = .none
        cusview.shadowView(scale: true)
        HomeTableview.backgroundColor = UIColor.white
        if (dashboardapidata.sharevariable.PiechartPercentage == 0 && dashboardapidata.sharevariable.Breathcount == 0 && dashboardapidata.sharevariable.bartotalindex == 0) {
            dashboardapidata.sharevariable.Breathcount = 1
        }
        miSbtn.shadowbutton(scale: true)
        attnbtn.shadowbutton(scale: true)
        btn1.shadowbutton(scale: true)
        Crmbtn.shadowbutton(scale: true)
        NotificationCenter.default.addObserver(self, selector: #selector(self.getnotificationmethod), name: NSNotification.Name(rawValue: "GetNotification"), object: nil)
        viewDropDown.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
        btn1.imageView!.transform = CGAffineTransform(scaleX: -1.0, y: 1.0);
        tableViewBranches.layer.cornerRadius = 5
        viewDropDown.isHidden = true
        viewdropdownSubView.layer.cornerRadius = 10.0
        labelBranchecode.text = LibraryAPI.sharedInstance.BranchCodeNew
        let tap = UITapGestureRecognizer(target: self, action: #selector(DashBoardViewController.handleTap2))
        tap.delegate = self
        tap.numberOfTapsRequired = 2
        viewDropDown.addGestureRecognizer(tap)
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(DashBoardViewController.handleTap))
        tap2.delegate = self
        viewTapDropdown.addGestureRecognizer(tap2)
        self.layoutFAB()
        LibraryAPI.sharedInstance.tracker="1"
        isanimated=false
        timestamp = DateFormatter.localizedString(from: NSDate() as Date, dateStyle: .medium, timeStyle:.none)
        self.navigationController?.delegate=self
        if LibraryAPI.sharedInstance.getphonenumberpopupCheck == "Check" {
            let returnValue : AnyObject? = UserDefaults.standard.object(forKey: "Helpdesk_PhoneNumber") as AnyObject?
            if returnValue == nil {
                let returnValue1 : AnyObject? = UserDefaults.standard.object(forKey: "PhoneNumber") as AnyObject?
                if returnValue1 == nil {
                    let alertController = UIAlertController(title: "Alert", message: "Please Update the Mobile Number", preferredStyle: .alert)
                    let cancelAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
                        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
                        lvc.popupcheck = true
                        self.navigationController?.pushViewController(lvc, animated: true)
                    }
                    let cancelAction1 = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                        UIAlertAction in
                    }
                    alertController.addAction(cancelAction)
                    alertController.addAction(cancelAction1)
                    self.present(alertController, animated: true, completion:nil)
                } else {
                }
            } else {
            }
            LibraryAPI.sharedInstance.getphonenumberpopupCheck = ""
        } else {
        }
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
        self.HomeTableview.reloadData()
        self.tableViewBranches.reloadData()
        self.local_activityIndicator_stop()
    }
    
    func reloadtable(){
        self.HomeTableview.reloadData()
    }
    
    @IBAction func notificationClicked(_ sender: UIButton) {
        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
        lvc.popupcheck = false
        self.navigationController?.pushViewController(lvc, animated: true)
        
    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
    }
    
    //chkout not used (purpose used for floating button )
    //chkin floating point button that is used for the refresh button
    //chkin *************************
    
    func layoutFAB() {
        fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
            DispatchQueue.global(qos: .background).async {
                DispatchQueue.main.async {
                    self.calldashboarddata()
                    self.local_activityIndicator_start()
                    Timer.scheduledTimer(timeInterval: 6.0, target: self, selector: #selector(self.LoaderSteup), userInfo: nil, repeats: false)
                }
            }
            self.fab.close()
        }
        fab.fabDelegate = self
        self.view.addSubview(fab)
    }
    //chkin floating point button that is used for the refresh button
    //chkin not used
    
    override func viewWillAppear(_ animated: Bool) {
        self.Customnavigation()
        self.Helpdesk_PhoneNumber_Notification()
        if LibraryAPI.sharedInstance.dashIn == 0{
            self.calldashboarddata()
            self.ParseBranchesdata1()
            LibraryAPI.sharedInstance.dashIn = 1
            Timer.scheduledTimer(timeInterval: 6.0, target: self, selector: #selector(self.LoaderSteup), userInfo: nil, repeats: false)
        }
        if (LibraryAPI.sharedInstance.push == 1) {
            showToast(message: "Mobile Number Updated Successfully")
            LibraryAPI.sharedInstance.push = 0
        }
    }
    
    //chkin not used
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        if (LibraryAPI.sharedInstance.push == 1) {
            showToast(message: "Mobile Number Updated Successfully")
            LibraryAPI.sharedInstance.push = 0
        }
    }
    
    @IBAction func MIAction(_ sender: UIButton) {
        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "MiViewController") as! MiViewController
        self.navigationController?.pushViewController(lvc, animated: true)
    }
    
    // chkout show the 3mi button view controlelr
    //chkin not used
    //chkin not used
    //chkin attendanceBTN is used to move to attendance view controller
    
    @IBAction func Attendancebtn(_ sender: UIButton) {
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
            self.getStringsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=WHATSUP&Option=1", completion: { (jsonmutable) in
                let responsestring = jsonmutable
                DispatchQueue.main.async {
                    self.local_activityIndicator_stop()
                  if (responsestring == "Y") {
                        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "ClockViewController") as! ClockViewController
                        self.navigationController?.pushViewController(lvc, animated: true)
                    } else {
                       self.local_activityIndicator_stop()
                        self.NormalAlert(title: "Alert", Messsage: " You are Not Authorised for this option")
                    }
                }
            })
        }
    }
    
    //chkout attendanceBTN is used to move to attendance view controller
    //chkin get the location by placemark of the pincode for location btn
    
    func displayLocationInfo(placemark: CLPlacemark) {
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location1 = locations.last! as CLLocation
        latitude = location1.coordinate.latitude
        longitude = location1.coordinate.longitude
        let location = CLLocation(latitude: latitude, longitude: longitude)
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            if error != nil {
                //self.showToast(message: String(describing: error))
            } else {
                if placemarks!.count > 0 {
                    let pm = placemarks![0]
                    self.displayLocationInfo(placemark: pm)
                    //self.current_area = pm.subLocality
                        LibraryAPI.sharedInstance.Pincode = pm.postalCode!
                } else {
                    self.showToast(message: "Delay in Server Response")
                }
            }
            
        })
    }
    
    //chkout get the location by placemark of the pincode  for location btn
    // chkin used to close the viewdropdownbtn2
    
    @IBAction func viewDropDownClose(_ sender: UIButton) {
        viewDropDown.isHidden = true
    }
    
    // chkout used to close the viewdropdownbtn2
    
    //chkin used to view all the data from the branches list that is listed in viewdropdownbtn2
    
    @IBAction func btndropdown(_ sender: AnyObject) {
        viewDropDown.isHidden = false
        tableViewBranches.reloadData()
    }
    
    //chkout used to view all the data from the branches list that is listed in viewdropdownbtn
    //chkin used to hide the viewdropdown for the branches
    func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        viewDropDown.isHidden = true
    }
    //chkout used to hide the viewdropdown for the branches
    //chkin used to view and reload data in the viewdropdown for the branches
    
    func handleTap2(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        viewDropDown.isHidden = false
        tableViewBranches.reloadData()
        
    }
    
    //chkout used to view and reload data in the viewdropdown for the branches
    //chkin branchaction btn in dashboard to view the branches view in viewdropdown view list as table
    
    @IBAction func branchesAction(_ sender: UIButton) {
        print("Branches clicked")
        if self.connectedToNetwork(){
            if dashboardapidata.sharevariable.responsestring3.count > 0 {
                viewDropDown.isHidden = false
                tableViewBranches.reloadData()
            } else {
                self.ParseBranchesdata()
            }
        } else {
            self.showalert(title: "Oops ", message: "Your are not connected to internet activate WIFI or Mobile data to access", buttontxt: "ok")
        }
    }
    
    //chkout branchaction btn in dashboard to view the branches view in viewdropdown view list as table
    //chkin used to set the orientaion for all the device
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    //chkout used to set the orientaion for all the device
    //chkin process while location btn clicked and get the location as pincode  to get the data of all brances in branch list as table
    
    @IBAction func locationbtn(_ sender: UIButton) {
        if  (LibraryAPI.sharedInstance.Pincode == "" ) {
            let alertController = UIAlertController (title: "Alert", message: "Please allow the Location Access", preferredStyle: .alert)
            let settingsAction = UIAlertAction(title: "Alert", style: .default) { (_) -> Void in
                let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                if let url = settingsUrl {
                    UIApplication.shared.openURL(url as URL)
                }
            }
            alertController.addAction(settingsAction)
            present(alertController, animated: true, completion: nil)
        } else {
            let alertController = UIAlertController(title: "Location", message:"Your Pincode is \(LibraryAPI.sharedInstance.Pincode) " , preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                self.local_activityIndicator_start()
                DispatchQueue.global(qos: .background).async {
                    self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/CustomerSearchPincodewise?Pincode=\(LibraryAPI.sharedInstance.Pincode)&Userid=\(LibraryAPI.sharedInstance.Userid)&RecordNo=20&PageNo=1", completion:  { (jsonmutable) in
                        self.responseArray = jsonmutable
                        DispatchQueue.main.async {
                            if(self.responseArray.count > 0) {
                                self.local_activityIndicator_stop()
                                let lvc=self.storyboard?.instantiateViewController(withIdentifier: "CustomerSearchPinCodeViewController") as! CustomerSearchPinCodeViewController
                                self.navigationController?.pushViewController(lvc, animated: true)
                            }else{
                                self.local_activityIndicator_stop()
                                self.NormalAlert(title: "Sorry!", Messsage: "No Data Found!")
                            }
                        }
                    })
                }
                // new version dispatch Que
            }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)
        }
    }
    
    //chkout process while location btn clicked and get the location as pincode  to get the data of all brances in branch list as table
    //chkin show and close the loader view while getting the data or some process going on in background
    
    
    //chkout show and close the loader view while getting the data or some process going on in background
    
    @available(iOS 10.0, *)
    
    //chkout not known
    //chkin 360view btn process
    
    
    @IBAction func Action(_ sender: UIButton) {
        LibraryAPI.sharedInstance.Currentcustomerlookup="360"
        let popup = PopupController
            .create(parentViewController: self)
            .show(childViewController: CustomerLookupViewController.instance()) // send the view controller for pop view with value as in LibraryAPI.sharedInstance.Currentcustomerlookup
            .didShowHandler { popup in
            }
            .didCloseHandler { _ in
        }
        let container = CustomerLookupViewController.instance()
        container.closeHandler = { _ in
            popup.dismiss()
        }
        popup.show(childViewController: container)
    }
    
    func parseLinechartdata()
    {
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)BreadthAchievement.asmx/ConsolidatedBreadth?UserID=\(LibraryAPI.sharedInstance.Userid)&BranchCode=\(LibraryAPI.sharedInstance.BranchCodeNew)") { (jsonMutablearray) in
            let  responsestring1 = jsonMutablearray
            if(responsestring1.count>0) {
                for i in 0..<responsestring1.count {
                    let dicResS = responsestring1[i] as! NSDictionary
                    let val=dicResS.object(forKey: "Breadth Achieved")
                    self.unitsSold.append(((val! as AnyObject) as? Double)!)
                }
                for i in 0..<self.unitsSold.count {
                    let dicunits =  self.unitsSold[i] as! NSDictionary
                    let val=dicunits.object(forKey: "Branch Description") as? String
                    self.months.append(val!)
                }
            } else {
                self.local_activityIndicator_stop()
                self.showToast(message: "No data Available")
            }
            self.HomeTableview.reloadData()
        }
    }
    
    //chkin not used
    //chkin call 3rd api in dashboardi
    /* moved to share data file */
    //chkout call 3rd api in dashboard
    // chkin used while update phonenumber (popup) in helpdesk
    // and call to get the branches details
    
    func ParseBranchesdata()  {
        self.local_activityIndicator_start()
        dashboardapidata.sharevariable.brnchcode.removeAll()
        dashboardapidata.sharevariable.brnchname.removeAll()
        DispatchQueue.main.async {
            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Users.asmx/BranchDetails?UserID=\(LibraryAPI.sharedInstance.Userid)", completion:  { (jsonmutable) in
                dashboardapidata.sharevariable.responsestring3 = jsonmutable
                let val=""
                dashboardapidata.sharevariable.brnchcode.append(((val as AnyObject) as? String)!)
                let val2="All Branches"
                dashboardapidata.sharevariable.brnchname.append(((val2 as AnyObject) as? String)!)
                if(dashboardapidata.sharevariable.responsestring3.count>0) {
                    for i in 0..<dashboardapidata.sharevariable.responsestring3.count {
                        let dicRes = dashboardapidata.sharevariable.responsestring3[i] as! NSDictionary
                        let val=dicRes.object(forKey: "BranchCode")
                        
                        dashboardapidata.sharevariable.brnchcode.append(((val! as AnyObject) as? String)!)
                        print(dashboardapidata.sharevariable.brnchcode)
                    }
                    for i in 0..<dashboardapidata.sharevariable.responsestring3.count {
                        let dicRes1 = dashboardapidata.sharevariable.responsestring3[i] as! NSDictionary
                        let val=dicRes1.object(forKey: "BranchName")
                        dashboardapidata.sharevariable.brnchname.append(((val! as AnyObject) as? String)!)
                    }
                    self.local_activityIndicator_stop()
                    self.tableViewBranches.reloadData()
                    if dashboardapidata.sharevariable.brnchname.count == 2 {
                        LibraryAPI.sharedInstance.BranchCodeNew = dashboardapidata.sharevariable.brnchcode[0]
                        LibraryAPI.sharedInstance.branchname = dashboardapidata.sharevariable.brnchname[0]
                        self.viewTapDropdown.isHidden  = true
                        self.branchesbtn.isHidden      = true
                    } else {
                        self.viewTapDropdown.isHidden  = false
                        self.branchesbtn.isHidden      = false
                    }
                }else{
                    Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.ParseBranchesdata), userInfo: nil, repeats: false)
                }
            })
        }
    }
    func ParseBranchesdata1()  {
        self.local_activityIndicator_start()
        dashboardapidata.sharevariable.brnchcode.removeAll()
        dashboardapidata.sharevariable.brnchname.removeAll()
        DispatchQueue.main.async {
            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Users.asmx/BranchDetails?UserID=\(LibraryAPI.sharedInstance.Userid)", completion:  { (jsonmutable) in
                dashboardapidata.sharevariable.responsestring3 = jsonmutable
                let val=""
                dashboardapidata.sharevariable.brnchcode.append(((val as AnyObject) as? String)!)
                let val2="All Branches"
                dashboardapidata.sharevariable.brnchname.append(((val2 as AnyObject) as? String)!)
                if(dashboardapidata.sharevariable.responsestring3.count>0) {
                    for i in 0..<dashboardapidata.sharevariable.responsestring3.count {
                        let dicRes = dashboardapidata.sharevariable.responsestring3[i] as! NSDictionary
                        let val=dicRes.object(forKey: "BranchCode")
                        
                        dashboardapidata.sharevariable.brnchcode.append(((val! as AnyObject) as? String)!)
                        print(dashboardapidata.sharevariable.brnchcode)
                    }
                    for i in 0..<dashboardapidata.sharevariable.responsestring3.count {
                        let dicRes1 = dashboardapidata.sharevariable.responsestring3[i] as! NSDictionary
                        let val=dicRes1.object(forKey: "BranchName")
                        dashboardapidata.sharevariable.brnchname.append(((val! as AnyObject) as? String)!)
                    }
                    //self.local_activityIndicator_stop()
                    self.tableViewBranches.reloadData()
                    if dashboardapidata.sharevariable.brnchname.count == 2 {
                        LibraryAPI.sharedInstance.BranchCodeNew = dashboardapidata.sharevariable.brnchcode[0]
                        LibraryAPI.sharedInstance.branchname = dashboardapidata.sharevariable.brnchname[0]
                        self.viewTapDropdown.isHidden  = true
                        self.branchesbtn.isHidden      = true
                    } else {
                        self.viewTapDropdown.isHidden  = false
                        self.branchesbtn.isHidden      = false
                    }
                }else{
                    Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.ParseBranchesdata), userInfo: nil, repeats: false)
                }
            })
        }
    }
    // chkout used while update phonenumber (popup) in helpdesk
    // and call to get the branches details
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    func LoaderSteup(){
        if dashboardapidata.sharevariable.LoadingValue == 0 {
            self.local_activityIndicator_start()
            LibraryAPI.sharedInstance.internetcheck()
            if  LibraryAPI.sharedInstance.networkReachable == 0 {
                Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.LoaderSteup), userInfo: nil, repeats: false)
            } else {
                self.local_activityIndicator_stop()
                TargetService.shared.deleteAllData(entity: TargetService.shared.EntityBranchDetails)
                TargetService.shared.createBranchDetails(branName: LibraryAPI.sharedInstance.branchname, branCode: LibraryAPI.sharedInstance.BranchCodeNew, entityname: TargetService.shared.EntityBranchDetails)
                self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
                dashboardapidata.sharevariable.LoadingValue = 1
                self.HomeTableview.reloadData()
            }
        } else{
            self.local_activityIndicator_stop()
            TargetService.shared.deleteAllData(entity: TargetService.shared.EntityBranchDetails)
            TargetService.shared.createBranchDetails(branName: LibraryAPI.sharedInstance.branchname, branCode: LibraryAPI.sharedInstance.BranchCodeNew, entityname: TargetService.shared.EntityBranchDetails)
            LibraryAPI.sharedInstance.networkReachable = 0
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            dashboardapidata.sharevariable.LoadingValue = 1
            self.HomeTableview.reloadData()
        }
        print("Reload Process")
    }
    
    //chkin side menu nav bar
    
    func Customnavigation() {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: 0,y: 0,width: self.view.frame.width,height: 65);
        menubtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        menubtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
    }
    
    //chkin side menu nav bar
    
    // chkin not used
    
    func buttonClicked(_ sender:UIButton) {
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "ViewController")), animated: true)
                self.local_activityIndicator_stop()  
            }
        }
    }
    
    override func willAnimateRotation(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
    }
    
    // chkout not used
    // chkin main table view to show all the details that is downloaded by hitting api (table used for tableViewBranches(branches btn), target achevied, quater chart view, breath view)
    
    func overduebtnclikcked(_ sender: AnyObject) {
        if (dashboardapidata.sharevariable.Array30.count > 0) {
            let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "BarChart1ViewController") as! BarChart1ViewController
            LibraryAPI.sharedInstance.tracker="2"
            self.navigationController?.pushViewController(lvc, animated: false)
        } else {
            self.showToast(message: "No data Available")
            
        }
    }
    
    // chkout customer overdue invoice summary
    // chkin YTD (year to date - Target/Acheivement ) process
    
    @IBAction func ytdbtn(_ sender: UIButton) {
        let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "TargetAcheivementViewController") as! TargetAcheivementViewController
        lvc.PiechartPercentage=dashboardapidata.sharevariable.PiechartPercentage   // changes made on 25/07/18
        LibraryAPI.sharedInstance.tracker="2"
        self.navigationController?.pushViewController(lvc, animated: false)
    }
    
    // chkout YTD (year to date - Target/Acheivement ) process
    // chkin QTR (quarter - Target/Acheivement ) process
    
    @IBAction func qtrbtn(_ sender: UIButton) {
        let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "qtrViewController") as! qtrViewController
        lvc.PiechartPercentage=dashboardapidata.sharevariable.PiechartPercentage
        lvc.pievalues=LibraryAPI.sharedInstance.pievalues
        LibraryAPI.sharedInstance.tracker="2"
        self.navigationController?.pushViewController(lvc, animated: false)
    }
    
    // chkout QTR (quarter - Target/Acheivement ) process
    //chkin move to Helpdisk1ViewController
    
    @IBAction func helpdesk(_ sender: UIButton) {
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                let lvc=self.storyboard?.instantiateViewController(withIdentifier: "Helpdisk1ViewController") as! Helpdisk1ViewController
                self.navigationController?.pushViewController(lvc, animated: true)
                self.local_activityIndicator_stop()
            }
        }
    }
    
    //chkout move to Helpdisk1ViewController
    //chkin move to CRM app in itune
    
    @IBAction func CRMButtonClicked(_ sender: UIButton) {
        let url = NSURL(string: "CratioCRMLite://")!
        let response = UIApplication.shared.openURL(url as URL)
        if !response {
            let appurl = NSURL(string: "https://itunes.apple.com/us/app/cratio-crm-lite/id1217006767?mt=8")!
            UIApplication.shared.openURL(appurl as URL)
        }
    }
    
    func Helpdesk_PhoneNumber_Notification()
    {
        let returnValue : AnyObject? = UserDefaults.standard.object(forKey: "Helpdesk_PhoneNumber") as AnyObject?
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)OrderStatus.asmx/Overduecollectionnotification?userId=\(LibraryAPI.sharedInstance.Userid)", completion: { (jsonmutable) in
            LibraryAPI.sharedInstance.NotificationArray = jsonmutable
            if returnValue == nil {
                let returnValue1 : AnyObject? = UserDefaults.standard.object(forKey: "PhoneNumber") as AnyObject?
                print("Phone number : ",returnValue1)
                if returnValue1 == nil {
                    if(LibraryAPI.sharedInstance.NotificationArray.count > 0) {
                        let notification = UILocalNotification()
                        notification.fireDate = NSDate.init(timeIntervalSinceNow: 5) as Date
                        notification.alertTitle  = "Mobile Number is Empty!"
                        notification.alertBody   = "Update the Mobile Number"
                        notification.alertAction = "open"
                        notification.hasAction   = true
                        UIApplication.shared.scheduleLocalNotification(notification)
                        let badgeCount: Int = LibraryAPI.sharedInstance.NotificationArray.count + 1
                        let application = UIApplication.shared
                        application.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge,.alert, .sound], categories: nil))
                        application.applicationIconBadgeNumber = badgeCount
                        self.notificationcountlbl.text = String(badgeCount)
                        self.Notificaitonbtn.setImage(UIImage(named:"notificationnew.png"), for: UIControlState.normal)
                        self.notificationcountlbl.isHidden = false
                    } else {
                        let notification = UILocalNotification()
                        notification.fireDate    = NSDate.init(timeIntervalSinceNow: 5) as Date
                        notification.alertTitle  = "Mobile Number is Empty!"
                        notification.alertBody   = "Update the Mobile Number"
                        notification.alertAction = "open"
                        notification.hasAction   = true
                        UIApplication.shared.scheduleLocalNotification(notification)
                        let badgeCount: Int = LibraryAPI.sharedInstance.NotificationArray.count + 1
                        let application     = UIApplication.shared
                        application.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge,.alert, .sound], categories: nil))
                        application.applicationIconBadgeNumber = badgeCount
                        self.notificationcountlbl.text         = String(badgeCount)
                        self.Notificaitonbtn.setImage(UIImage(named:"notificationnew.png"), for: UIControlState.normal)
                        self.notificationcountlbl.isHidden     = false
                    }
                } else {
                    if(LibraryAPI.sharedInstance.NotificationArray.count > 0) {
                        let badgeCount: Int = LibraryAPI.sharedInstance.NotificationArray.count
                        let application = UIApplication.shared
                        application.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge,.alert, .sound], categories: nil))
                        application.applicationIconBadgeNumber = badgeCount
                        self.notificationcountlbl.text = String(badgeCount)
                        self.Notificaitonbtn.setImage(UIImage(named:"notificationnew.png"), for: UIControlState.normal)
                        self.notificationcountlbl.isHidden = false
                        [self.view .setNeedsDisplay()]
                    } else {
                        self.Notificaitonbtn.setImage(UIImage(named:"newnotification2.png"), for: UIControlState.normal)
                    }
                }
            } else {
                if(LibraryAPI.sharedInstance.NotificationArray.count > 0) {
                    let badgeCount: Int = LibraryAPI.sharedInstance.NotificationArray.count
                    let application     = UIApplication.shared
                    application.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge,.alert, .sound], categories: nil))
                    application.applicationIconBadgeNumber = badgeCount
                    self.notificationcountlbl.text         = String(badgeCount)
                    self.Notificaitonbtn.setImage(UIImage(named:"notificationnew.png"), for: UIControlState.normal)
                    self.notificationcountlbl.isHidden = false
                    [self.view .setNeedsDisplay()]
                } else {
                    self.Notificaitonbtn.setImage(UIImage(named:"newnotification2.png"), for: UIControlState.normal)
                }
            }
        })
    }
}

extension DashBoardViewController : UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tableViewBranches {
            if dashboardapidata.sharevariable.brnchname.count == 0 {
                return 0
            } else {
                return 1
            }
            
        }else{
            if((dashboardapidata.sharevariable.pieresponsestring.count > 0) || (dashboardapidata.sharevariable.Breathcount > 0) ||  (dashboardapidata.sharevariable.bartotalindex > 0)) {
                return 3
            } else {
                //return 0
                return 3
                
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableViewBranches {
            return dashboardapidata.sharevariable.brnchname.count
        } else {
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tableViewBranches {
            let cell : BranchesTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! BranchesTableViewCell
            if(dashboardapidata.sharevariable.brnchname.count > 0) {
                if dashboardapidata.sharevariable.brnchcode[indexPath.row] == "" {
                    cell.lableBranches.text = "   "+dashboardapidata.sharevariable.brnchname[indexPath.row]
                } else {
                    cell.lableBranches.text = "   "+dashboardapidata.sharevariable.brnchcode[indexPath.row]+" - "+dashboardapidata.sharevariable.brnchname[indexPath.row]
                }
            } else {
            }
            return cell
        } else {
            if(indexPath.section == 0) {
                let cell = tableView.dequeueReusableCell(withIdentifier: "TagetAchieveTableViewCell") as! TagetAchieveTableViewCell
                //let val=(dashboardapidata.sharevariable.PiechartPercentage.intValue)
                // cell.contentView
                // cell.contentView.layer.shadowColor = CustomColor.ShareColor.hexStringToUIColor(hex: CustomColor.ShareColor.CustomGreen, alpha: 1.0).cgColor
                cell.view.shadowView(scale: true)
                cell.defaultbtn1.tag = indexPath.row;
                cell.defaultbtn2.tag = indexPath.row;
                cell.defaultbtn1.addTarget(self, action: #selector(DashBoardViewController.ytdbtn), for: UIControlEvents.touchUpInside)
                cell.defaultbtn2.addTarget(self, action: #selector(DashBoardViewController.qtrbtn), for: UIControlEvents.touchUpInside)
                print("value in pie chart data : ",dashboardapidata.sharevariable.months1, dashboardapidata.sharevariable.pievalues)
                
                if(dashboardapidata.sharevariable.pievalues.count>0)
                {
                    cell.setChart(DouVal: dashboardapidata.sharevariable.pievalues)
                } else {
                }
                cell.selectionStyle = .none
                return cell
                
            } else if(indexPath.section==1) {
                let cell = tableView.dequeueReusableCell(withIdentifier: "OverDueTableViewCell") as! OverDueTableViewCell
                cell.Barchartviews.backgroundColor = CustomColor.ShareColor.hexStringToUIColor(hex: CustomColor.ShareColor.customNavyBlue, alpha: 1.0)
                cell.subview.shadowView(scale: true)
                if dashboardapidata.sharevariable.bartotalindex > 0 {
                    if((dashboardapidata.sharevariable.Array30.last == 0.0)&&(dashboardapidata.sharevariable.array60.last == 0.0)&&(dashboardapidata.sharevariable.array90.last == 0.0)&&(dashboardapidata.sharevariable.array120.last == 0.0)&&(dashboardapidata.sharevariable.arraymorethan120.last == 0.0))
                    {
                        cell.setChartBarGroupDataSetfordashboard(dataPoints: dashboardapidata.sharevariable.monthsbar, values: dashboardapidata.sharevariable.Array30, values2: dashboardapidata.sharevariable.array60, values3: dashboardapidata.sharevariable.array90, values4: dashboardapidata.sharevariable.array120, values5: dashboardapidata.sharevariable.arraymorethan120, sortIndex: 1)
                        cell.nodatalbl.isHidden = false
                    } else if (dashboardapidata.sharevariable.monthsbar.count > 0 ) {
                        cell.Barchartviews.noDataText=""
                        cell.setChartBarGroupDataSetfordashboard(dataPoints: dashboardapidata.sharevariable.monthsbar, values: dashboardapidata.sharevariable.Array30, values2: dashboardapidata.sharevariable.array60, values3: dashboardapidata.sharevariable.array90, values4: dashboardapidata.sharevariable.array120, values5: dashboardapidata.sharevariable.arraymorethan120, sortIndex: 1)
                        cell.nodatalbl.isHidden = true
                    } else {
                        dashboardapidata.sharevariable.Array30.append(0.0)
                        dashboardapidata.sharevariable.array60.append(0.0)
                        dashboardapidata.sharevariable.array90.append(0.0)
                        dashboardapidata.sharevariable.array120.append(0.0)
                        dashboardapidata.sharevariable.arraymorethan120.append(0.0)
                        cell.setChartBarGroupDataSetfordashboard(dataPoints: dashboardapidata.sharevariable.monthsbar, values: dashboardapidata.sharevariable.Array30, values2: dashboardapidata.sharevariable.array60, values3: dashboardapidata.sharevariable.array90, values4: dashboardapidata.sharevariable.array120, values5: dashboardapidata.sharevariable.arraymorethan120, sortIndex: indexPath.row)
                        cell.nodatalbl.isHidden = false
                        
                    }
                    //chkout coredate process
                } else {
                    // cell.Barchartviews.noDataText="No chart data available"
                }
                cell.Barchartviews.pinchZoomEnabled=false
                cell.Barchartviews.xAxis.labelPosition = .bottom
                cell.Barchartviews.descriptionText = ""
                cell.Barchartviews.descriptionTextColor = UIColor.white
                cell.Barchartviews.isUserInteractionEnabled=false
                cell.Barchartviews.rightAxis.enabled=false
                cell.Barchartviews.xAxis.drawGridLinesEnabled = false
                cell.Barchartviews.leftAxis.drawLabelsEnabled = false
                cell.Barchartviews.legend.enabled = false
                let xAxis:XAxis = cell.Barchartviews.xAxis
                xAxis.drawAxisLineEnabled = false
                xAxis.drawGridLinesEnabled = false
                xAxis.drawLabelsEnabled=false
                cell.Barchartviews.layer.cornerRadius = 20.0
                cell.btnclicked.tag = indexPath.row;
                cell.btnclicked.addTarget(self, action: #selector(DashBoardViewController.overduebtnclikcked), for: UIControlEvents.touchUpInside)
                cell.selectionStyle = .none
                return cell
                } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "BreadthTableViewCell") as! BreadthTableViewCell
                cell.subview.shadowView(scale: true)
                if(dashboardapidata.sharevariable.Breathcount>0) {
                    let dicBreath = dashboardapidata.sharevariable.breadthvaluearray[indexPath.row] as! NSDictionary
                    let dicMes = dashboardapidata.sharevariable.breadthvaluearray[0] as! NSDictionary
                    if((dicMes.object(forKey: "Message") as? String) == "No records found") {
                        cell.Group.text = ""
                        cell.Biz.text = ""
                        cell.Previous.text = ""
                        cell.Current.text = ""
                    } else {
                        cell.Group.text=dicBreath.object(forKey: "SBU CODE") as? String
                        cell.Biz.text=dicBreath.object(forKey:"Business Code") as? String
                        cell.Previous.text=String(dicBreath.object(forKey:"Previous Month") as! Int)
                        cell.Current.text=String(dicBreath.object(forKey:"Current Month") as! Int)
                        if( dicMes.object(forKey: "Flag") as! String == "U") {
                            cell.likeimage.image=UIImage(named:"like1.png")
                        } else if(dicMes.object(forKey: "Flag") as! String == "D") {
                            cell.likeimage.image=UIImage(named:"unlike.png")
                        }else {
                            cell.likeimage.image=UIImage(named:"no_like.png")
                        }
                    }
                    if(dashboardapidata.sharevariable.months.count>0) {
                        if(isanimated==false) {
                            isanimated=true
                        } else {
                            isanimated=true
                        }
                    }
                    cell.Nodataavail.isHidden = true
                    
                } else {
                    cell.Nodataavail.isHidden = false
                }
                cell.selectionStyle = .none
                return cell
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tableViewBranches {
            return 45
        } else {
            return 185
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tableViewBranches {
            viewDropDown.isHidden = true
            LibraryAPI.sharedInstance.BranchCodeNew = dashboardapidata.sharevariable.brnchcode[indexPath.row];
            LibraryAPI.sharedInstance.branchname    = dashboardapidata.sharevariable.brnchname[indexPath.row];
            DispatchQueue.global(qos: .background).async {
                DispatchQueue.main.async {
                    self.calldashboarddata()
                    self.local_activityIndicator_start()
                    Timer.scheduledTimer(timeInterval: 6.0, target: self, selector: #selector(self.LoaderSteup), userInfo: nil, repeats: false)
                }
            }
        } else {
            if(indexPath.section==0) {
                DispatchQueue.global(qos: .background).async {
                    DispatchQueue.main.async {
                        let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "TargetAcheivementViewController") as! TargetAcheivementViewController
                        lvc.PiechartPercentage=dashboardapidata.sharevariable.PiechartPercentage
                        lvc.pievalues=LibraryAPI.sharedInstance.pievalues
                        LibraryAPI.sharedInstance.tracker="2"
                        self.navigationController?.pushViewController(lvc, animated: false)
                    }
                }
            }
            if(indexPath.section==1) {
                if dashboardapidata.sharevariable.bartotalindex > 0 {
                    DispatchQueue.global(qos: .background).async {
                        DispatchQueue.main.async {
                            let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "BarChart1ViewController") as! BarChart1ViewController
                            LibraryAPI.sharedInstance.tracker="2"
                            self.navigationController?.pushViewController(lvc, animated: false)
                        }
                    }
                } else {
                    self.showToast(message: "No data Available")
                    
                }
            }
            if(indexPath.section==2)
            {
                if(dashboardapidata.sharevariable.Breathmessage == "No records found") {
                    self.showToast(message: "No data Available")
                } else {
                    if (dashboardapidata.sharevariable.breadthvaluearray.count > 0) {
                        DispatchQueue.global(qos: .background).async {
                            DispatchQueue.main.async {
                                let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "BreathAnalysisListViewController") as! BreathAnalysisListViewController
                                LibraryAPI.sharedInstance.tracker="2"
                                self.navigationController?.pushViewController(lvc, animated: false)
                                // print("called did select")
                            }
                        }
                    }else{
                        self.showToast(message: "No data Available")
                    }
                }
            }
        }
    }
}

extension UIViewController {
    
    func local_activityIndicator_start() {
        let activity = ActivityData(size: CGSize(width: 30, height: 30), message: "Loading...", type: NVActivityIndicatorType(rawValue: 0)!)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activity)
    }
    
    func local_activityIndicator_stop() {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
    
    func showToast(message : String) {
        let textWidth : CGFloat = CGFloat((message.count*10)+50)
        let ViewWidth : CGFloat = self.view.frame.size.width
        let setwidth  : CGFloat
        if textWidth  > ViewWidth {
            setwidth  = self.view.frame.size.width
        }else{
            setwidth  = textWidth
        }
        let toastLabel = UILabel(frame: CGRect(x:(self.view.frame.size.width-setwidth)/2, y: self.view.frame.size.height - self.view.frame.size.height/5, width: setwidth, height: 50))
        toastLabel.backgroundColor           = UIColor.black
        toastLabel.textColor                 = UIColor.white
        toastLabel.textAlignment             = NSTextAlignment.center
        toastLabel.font                      = UIFont(name: "Montserrat-Light", size: 8.0)
        toastLabel.text                      = message
        toastLabel.adjustsFontSizeToFitWidth = true
        toastLabel.numberOfLines             = 5
        toastLabel.clipsToBounds             = true
        toastLabel.layer.cornerRadius        = toastLabel.frame.height/2.0
        toastLabel.lineBreakMode             = NSLineBreakMode.byWordWrapping
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 2.0, delay: 0.1, options: UIViewAnimationOptions.curveEaseOut, animations: {
            toastLabel.alpha = 0.75
        }, completion: nil)
        UIView.animate(withDuration: 0.3, delay: 2.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            toastLabel.frame.origin.y = toastLabel.frame.origin.y + self.view.frame.size.height/2
            toastLabel.alpha          = 0.0
        }, completion: {(isCompleted) in toastLabel.removeFromSuperview()})
    }
    
    
    func getnotificationmethod() {
        let userdefaults = Foundation.UserDefaults.standard
        let value = userdefaults.dictionary(forKey: "Notification")
        var title: String!
        var message: String!
        var imagestr: String!
        title = value?["title"] as! String
        message = value?["body"] as! String
        imagestr = value?["imageurl"] as! String
        if imagestr != nil {
            if imagestr != "" {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: notificationimageViewController.instance())
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = notificationimageViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                }
                popup.show(childViewController: container)
            } else {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: PushnotificationtextViewController.instance())
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = PushnotificationtextViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                }
                popup.show(childViewController: container)
            }
        } else {
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: PushnotificationtextViewController.instance())
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
            }
            let container = PushnotificationtextViewController.instance()
            container.closeHandler = { _ in
                popup.dismiss()
            }
            popup.show(childViewController: container)
        }
    }
    
    func NormalAlert(title: String,Messsage: String){
        let alertController = UIAlertController(title: title, message: Messsage, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
        }
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion:nil)
    }
    
    func connectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
    func getStringsuccess(params:[String:Any],urlhit: String, completion: @escaping LibraryAPI.jsonString) {
        let urllink = urlhit
        print("URl:-",urllink)
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 2
        manager.session.configuration.timeoutIntervalForResource = 30
        if self.connectedToNetwork(){
            manager.request(urllink).responseJSON(queue: nil)
            { response in
                var responseArray:String!
                if let error = response.result.error {
                    if error._code == NSURLErrorTimedOut {
                        self.showalert(title: "Alert", message: "Delay in Server Response Try Again Later...!", buttontxt: "Ok")
                        self.local_activityIndicator_stop()
                    } else {
//                        self.showalert(title: "Alert", message: "Delay in Server Response", buttontxt: "Try Again Later...!")
                    }
                } else {
                    switch (response.result) {
                    case .success:
                        if response.result.value != nil {
                            let val = response.result.value as! String
                            responseArray = val as! String
                            completion(responseArray)
                        }else{
                            print("Issue in Server Response")
                        }
                        break
                    case .failure(let error):
                        if error._code == NSURLErrorTimedOut {
                            print("Connect to Server / TimeOut")
                        }
                        print("Can't Connect to Server / TimeOut")
                        break
                    }
                }
            }
        } else {
            self.local_activityIndicator_stop()
            self.NormalAlert(title: "Oops", Messsage: "You are not connected to internet activate WIFI or Mobile data to access")
        }
    }
    
    func getsuccess(params:[String:Any],urlhit: String, completion: @escaping LibraryAPI.jsonMutablearray) {
        let urllink = urlhit
        print("URl:-",urllink)
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 2
        manager.session.configuration.timeoutIntervalForResource = 30
        if self.connectedToNetwork(){
            manager.request(urllink).responseJSON(queue: nil)
            { response in
                var responseArray:NSMutableArray!
                if let error = response.result.error {
                    if error._code == NSURLErrorTimedOut {
                        dashboardapidata.sharevariable.LoadingValue = 1
                        self.showalert(title: "Alert", message: "Delay in Server Response Try Again Later...!", buttontxt: "Ok")
                        self.local_activityIndicator_stop()
                    } else {
                        dashboardapidata.sharevariable.LoadingValue = 1
                        self.showalert(title: "Alert", message: "Delay in Server Response Try Again Later...!", buttontxt: "Ok")
                        self.local_activityIndicator_stop()
                    }
                } else {
                    switch (response.result) {
                    case .success:
                        if response.result.value != nil {
                            let val = response.result.value as! NSArray
                            responseArray = val.mutableCopy() as! NSMutableArray
                            completion(responseArray)
                        } else {
                            print("Error in alamofire")
                        }
                        break
                    case .failure(let error):
                        print("Can't Connect to Server / TimeOut")
                        break
                    }
                }
            }
        } else {
            self.local_activityIndicator_stop()
            self.NormalAlert(title: "Oops", Messsage: "You are not connected to internet activate WIFI or Mobile data to access")
        }
    }
    
    func getjsonArraysuccess(params:[String:Any],urlhit: String, completion: @escaping LibraryAPI.jsonArray) {
        let urllink = urlhit
        print("URl:-",urllink)
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 2
        manager.session.configuration.timeoutIntervalForResource = 30
        if self.connectedToNetwork(){
            manager.request(urllink).responseJSON(queue: nil) { response in
                var responseArray:NSArray!
                if let error = response.result.error {
                    if error._code == NSURLErrorTimedOut {
                        self.showalert(title: "Alert", message: "Delay in Server Response Try Again Later...!", buttontxt: "Ok")
                        self.local_activityIndicator_stop()
                    } else {
                        // self.showalert(title: "Alert", message: "Delay in Server Response", buttontxt: "Try Again Later...!")
                    }
                } else {
                    switch (response.result) {
                    case .success:
                        if response.result.value != nil {
                            let val = response.result.value as! NSArray
                            responseArray = val
                            completion(responseArray)
                        }else{
                            print("Error in alamofire")
                        }
                        break
                    case .failure(let error):
                        if error._code == NSURLErrorTimedOut {
                            print("Connect to Server / TimeOut")
                        }
                        print("Can't Connect to Server / TimeOut")
                        break
                    }
                }
            }
        } else {
            self.local_activityIndicator_stop()
            self.NormalAlert(title: "Oops", Messsage: "You are not connected to internet activate WIFI or Mobile data to access")
        }
    }
    
    func getjsonDictionarysuccess(params:[String:Any],urlhit: String, completion: @escaping LibraryAPI.jsonDictionary) {
        let urllink = urlhit
        print("URl:-",urllink)
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 2
        manager.session.configuration.timeoutIntervalForResource = 30
        if self.connectedToNetwork(){
            manager.request(urllink).responseJSON(queue: nil) { response in
                var responseArray:NSDictionary!
                if let error = response.result.error {
                    if error._code == NSURLErrorTimedOut {
                        self.showalert(title: "Alert", message: "Delay in Server Response Try Again Later...!", buttontxt: "Ok")
                        self.local_activityIndicator_stop()
                    } else{
                        // self.showalert(title: "Alert", message: "Delay in Server Response", buttontxt: "Try Again Later...!")
                    }
                } else {
                    switch (response.result) {
                    case .success:
                        if response.result.value != nil {
                            let val = response.result.value as! NSDictionary
                            responseArray = val
                            completion(responseArray)
                        } else {
                            print("Error in alamofire")
                        }
                        break
                    case .failure(let error):
                        if error._code == NSURLErrorTimedOut {
                            print("Connect to Server / TimeOut")
                        }
                        print("Can't Connect to Server / TimeOut")
                        break
                    }
                }
            }
        } else {
            self.local_activityIndicator_stop()
            self.NormalAlert(title: "Oops", Messsage: "You are not connected to internet activate WIFI or Mobile data to access")
        }
    }
    
    func getjsonMutableDictionarysuccess(params:[String:Any],urlhit: String, completion: @escaping LibraryAPI.jsonMutableDictionary) {
        let urllink = urlhit
        print("URl:-",urllink)
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 2
        manager.session.configuration.timeoutIntervalForResource = 30
        if self.connectedToNetwork(){
            manager.request(urllink).responseJSON(queue: nil) { response in
                var responseArray:NSDictionary!
                if let error = response.result.error {
                    if error._code == NSURLErrorTimedOut {
                        self.showalert(title: "Alert", message: "Delay in Server Response Try Again Later...!", buttontxt: "Ok")
                        self.local_activityIndicator_stop()
                    } else {
                        // self.showalert(title: "Alert", message: "Delay in Server Response", buttontxt: "Try Again Later...!")
                    }
                } else {
                    switch (response.result) {
                    case .success:
                        if response.result.value != nil {
                            let val = response.result.value as! NSDictionary
                            responseArray = val.mutableCopy() as! NSMutableDictionary
                            completion(responseArray as! NSMutableDictionary)
                        }else{
                            print("Error in alamofire")
                        }
                        break
                    case .failure(let error):
                        print("",error)
                        break
                    }
                }
            }
        } else {
            self.local_activityIndicator_stop()
            self.NormalAlert(title: "Oops", Messsage: "You are not connected to internet activate WIFI or Mobile data to access")
        }
    }
}
//chkout********25/07/18
extension UIView{
    func shadowView(scale: Bool = true){
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOpacity = 0.6
        layer.shadowOffset = CGSize.zero
        layer.shadowRadius = 4
        layer.rasterizationScale = UIScreen.main.scale
        layer.shouldRasterize = true
    }
}
extension UIButton{
    func shadowbutton(scale: Bool = true){
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOpacity = 0.6
        layer.shadowOffset = CGSize.zero
        layer.shadowRadius = 4
        layer.rasterizationScale = UIScreen.main.scale
        layer.shouldRasterize = true
    }
}

