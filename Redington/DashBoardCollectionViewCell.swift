//
//  DashBoardCollectionViewCell.swift
//  Redington
//
//  Created by truetech on 12/08/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
import Charts

class DashBoardCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var TitleLabel: UILabel!
    
    @IBOutlet weak var pieval: UILabel!
    @IBOutlet weak var piechartcell: UIView!
    @IBOutlet var SalesTargetLabel: UILabel!
    @IBOutlet var SalesAcheivedLabel: UILabel!
    @IBOutlet weak var ytdlbl: UILabel!
    @IBOutlet weak var qtrlbl: UILabel!
    @IBOutlet weak var qtr2lbl: UILabel!
    @IBOutlet weak var qtr3lbl: UILabel!
    @IBOutlet weak var qtr4lbl: UILabel!
    @IBOutlet var Greencolourlabel: UILabel!
    let shapelayer = CAShapeLayer()
    let shapelayer1 = CAShapeLayer()
    var PieVal = Float()
    var val = Float()
    override func prepareForReuse()
    {
       SalesTargetLabel.text=""
       SalesAcheivedLabel.text=""
    }
    func setval(Floatval: [Double]){
        PieVal = Float(Floatval[0])
       
        let center = contentView.center
        let circlepath = UIBezierPath(arcCenter: center, radius: 50, startAngle: -CGFloat.pi / 2, endAngle: 2 * CGFloat.pi, clockwise: true)
        shapelayer.path = circlepath.cgPath
        shapelayer.strokeColor = CustomColor.ShareColor.hexStringToUIColor(hex: CustomColor.ShareColor.CustomGreen, alpha: 1.0).cgColor
        shapelayer.lineWidth = 10
        shapelayer.strokeEnd = 0
        shapelayer.fillColor = UIColor.clear.cgColor
        shapelayer1.path = circlepath.cgPath
        shapelayer1.strokeColor = UIColor.red.cgColor
        shapelayer1.lineWidth = 10
        shapelayer1.strokeEnd = 1
        shapelayer1.fillColor = UIColor.clear.cgColor
        self.contentView.layer.addSublayer(shapelayer1)
        self.contentView.layer.addSublayer(shapelayer)
       
        self.animatepieview()
        //self.contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(animatepieview)))
    }
    
    @objc private func animatepieview(){
        let baseanimate = CABasicAnimation(keyPath: "strokeEnd")
        let divVal = PieVal / 100
        print("div val: ", divVal)
        let con = Float(divVal)
        if con <= 0.0 {
            val = 0.0
        }else if con < 0.1 {
            val = 0.10
        }else if con < 0.2 {
            val = 0.20
        }else if con < 0.3 {
            val = 0.30
        }else if con < 0.4 {
            val = 0.35
        }else if con < 0.5 {
            val = 0.45
        } else if con < 0.6 {
            val = 0.55
        } else if con < 0.7 {
            val = 0.65
        } else if con < 0.8 {
            val = 0.70
        } else if con < 0.9 {
            val = 0.75
        } else if con < 1.0 {
            val = 0.78
        }else {
            val = 1.0
        }
        baseanimate.toValue = val
        print("my val :",val)
        baseanimate.duration = 2
        baseanimate.fillMode = kCAFillModeForwards
        baseanimate.isRemovedOnCompletion = false
        shapelayer.add(baseanimate, forKey: "urSoBasic")
    }
}
 
