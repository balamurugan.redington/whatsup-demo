//
//  StockEnquiryDeatilsTableViewCell.swift
//  Whatsup
//
//  Created by truetech on 21/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit

class StockEnquiryDeatilsTableViewCell: UITableViewCell {

    @IBOutlet var Stockname: UILabel!
    @IBOutlet var Itemcode: UILabel!
    @IBOutlet var Vendorpartno: UILabel!
    @IBOutlet var Unitavailable: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
