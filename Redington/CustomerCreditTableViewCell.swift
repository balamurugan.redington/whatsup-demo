//
//  CustomerCreditTableViewCell.swift
//  Redington
//
//  Created by truetech on 05/08/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
import Charts
//import Funnel



class CustomerCreditTableViewCell: UITableViewCell {
    
    @IBOutlet var likeimage: UIImageView!
    @IBOutlet var Current: UILabel!
    @IBOutlet var Previous: UILabel!
    @IBOutlet var Biz: UILabel!
    @IBOutlet var HideGreen: UILabel!
    @IBOutlet var HideGreeb: UILabel!
    @IBOutlet var Hideacheived: UILabel!
    
    
    @IBOutlet weak var qtrlabel: UILabel!
    @IBOutlet  var ytdlabel: UILabel!
    @IBOutlet var Hidetarget: UILabel!
    @IBOutlet var Hidelabel: UILabel!
    @IBOutlet var Hideview1: UIView!
    @IBOutlet var Hideview: UIView!
    @IBOutlet var CircleView: UIView!
    @IBOutlet var DateLabel: UILabel!
   // @IBOutlet var FunnelCharts: FunnelChart!
    @IBOutlet var View4: UIView!
    @IBOutlet var Labeltext: UILabel!
    @IBOutlet var View1: UIView!
    @IBOutlet var View2: UIView!
    @IBOutlet var View3: UIView!
    @IBOutlet var Group: UILabel!
  
   
    @IBOutlet  var defaultbtn1: UIButton!
    @IBOutlet weak var qtrbutton: UIButton!
   
    @IBOutlet  var defaultbtn2: UIButton!
    
    private let dirSelectorHeight: CGFloat = 50
    var dollars1 = [Double]()
    

   @IBOutlet var PieChartViews: PieChartView!
    
    @IBOutlet var Barchartviews: BarChartView!
    var months=[String]()
    
    
    @IBOutlet var Linecharts: LineChartView!
   
//    @IBOutlet var RingChartview: RingGraphView!
   
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
    }
  
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
           Barchartviews.layer.cornerRadius = 5.0
           Barchartviews.clipsToBounds = true
           Barchartviews.backgroundColor = (UIColor.init(red: 2/255.0, green: 132/255.0, blue: 168/255.0, alpha: 1.0))
        
        // Initialization code
        }
   /* func syncFromChart()
    {
       
        let  _ = FunnelCharts!.coneLipHeightAsFractionOfViewHeight * 100
       
        let  _ = FunnelCharts!.stemHeightAsFractionOfViewHeight * 100
        
        let  _ = FunnelCharts!.stemWidthAsFractionOfViewWidth * 100
       
        let  _ = FunnelCharts!.sliceSpacingAsFractionOfViewHeight * 100
        
        let _ = FunnelCharts!.funnelLeftShadowWidthAsFractionOfViewWidth * 100
        
    }*/
    
    func configureButton()
    {
        let borderAlpha : CGFloat = 0.7
        let cornerRadius : CGFloat = 5.0
        
        defaultbtn1.layer.borderWidth = 1.0
        defaultbtn1.layer.borderColor = UIColor(white: 1.0, alpha: borderAlpha).cgColor
        defaultbtn1.layer.cornerRadius = cornerRadius
        
        defaultbtn2.layer.borderWidth = 1.0
        defaultbtn2.layer.borderColor = UIColor(white: 1.0, alpha: borderAlpha).cgColor
        defaultbtn2.layer.cornerRadius = cornerRadius

        
    }
    
    func optionbutton1()
    
    {
       //  defaultbtn1.setImage(UIImage(named:"green.png"), forState: UIControlState.Normal)
         defaultbtn1.setImage(UIImage(named:"green.png"), for: .normal)
        defaultbtn2.setImage(UIImage(named:"normal.png"), for: .normal)
    }
    
    func optionbutton2()
        
    {
        defaultbtn1.setImage(UIImage(named:"normal.png"), for: .normal)
        defaultbtn2.setImage(UIImage(named:"normal.png"), for: .normal)
    }


    
    func setChartData(months : [String],isanimated:Bool,units:[Double])
    {
        var yVals1 : [ChartDataEntry] = [ChartDataEntry]()
       
        
        dollars1=units
        for i in 0 ..< months.count {
          //  yVals1.append(ChartDataEntry(x: dollars1[i], y: Double(i)))
            yVals1.append(ChartDataEntry(x: Double(i), y: dollars1[i]))
        }
        
        // 2 - create a data set with our array
        
        
        let set1: LineChartDataSet = LineChartDataSet(values: yVals1, label: "First Set")
        set1.axisDependency = .left // Line will correlate with left axis values
        set1.setColor(UIColor.init(red: 75/255.0, green: 206/255.0, blue: 163/255.0, alpha: 1.0)) // our line's opacity is 50%
        set1.setCircleColor(UIColor.init(red: 75/255.0, green: 206/255.0, blue: 163/255.0, alpha: 1.0)) // our circle will be dark red
        set1.lineWidth = 2.0
        set1.circleRadius = 3.0 // the radius of the node circle
        set1.fillAlpha = 65 / 255.0
        set1.fillColor = UIColor.init(red: 75/255.0, green: 206/255.0, blue: 163/255.0, alpha: 1.0)
        set1.highlightColor = UIColor.cyan
        set1.drawCircleHoleEnabled = true
        set1.circleHoleColor=UIColor.init(red: 75/255.0, green: 206/255.0, blue: 163/255.0, alpha: 1.0)
        Linecharts.legend.enabled=false
        
      //  set1.drawValuesEnabled=true
        //3 - create an array to store our LineChartDataSets
        
        
        var dataSets : [LineChartDataSet] = [LineChartDataSet]()
        dataSets.append(set1)
        
        //4 - pass our months in for our x-axis label value along with our dataSets
        
        
      //  let data: LineChartData = LineChartData(xVals: months, dataSets: dataSets)
        let data: LineChartData = LineChartData(dataSet: dataSets as! IChartDataSet)
        data.setValueTextColor(UIColor.clear)
        
        //5 - finally set our data
      
        self.Linecharts.data = data
        if(isanimated==false)
        {
            
        }
        else
        {
            Linecharts.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: ChartEasingOption.easeOutQuad)
        }
    }
    
   
    

    func setbarcharts(dataPoints: [String], values: [Double],isanimated:Bool) {
     
        Barchartviews.noDataText = "You need to provide data for the chart."
        
        var dataEntries: [BarChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
              //  let dataEntry = BarChartDataEntry(value: values[i], xIndex: i)
            let dataEntry = BarChartDataEntry(x: values[i], y:  Double(i))
                dataEntries.append(dataEntry)
                months.append("")
            }
        Barchartviews.legend.enabled=false
        
        if(isanimated==false)
        {
            
        }
        else
        {
            Barchartviews.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: ChartEasingOption.easeOutQuad)
        }
      
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: "Units Sold")
      
        
        
        chartDataSet.colors = ChartColorTemplates.colorful()
       // let chartData = BarChartData(xVals:months, dataSet:chartDataSet)
        let chartData = BarChartData(dataSet: chartDataSet)
        Barchartviews.data = chartData
    
      
        
    }
    
    func setChartBarGroupDataSetfordashboard(dataPoints: [String], values: [Double], values2: [Double],values3:[Double],values4:[Double],values5:[Double],sortIndex:Int) {
        
        var dataEntries: [BarChartDataEntry] = []
        var dataEntries2: [BarChartDataEntry] = []
        var dataEntries3: [BarChartDataEntry] = []
        var dataEntries4: [BarChartDataEntry] = []
        var dataEntries5: [BarChartDataEntry] = []
      
//        var de: [BarChartDataEntry] = []
        var colors: [UIColor] = []
        var colors1: [UIColor] = []
        var colors2: [UIColor] = []
        var colors3: [UIColor] = []
        var colors4: [UIColor] = []
        
        for i in 0..<dataPoints.count {
            
         //   let dataEntry = BarChartDataEntry(value: values[i], xIndex: i)
            
          let dataEntry = BarChartDataEntry(x: Double(i), y: values[i])
            dataEntries.append(dataEntry)
          
        }
        
        
        for i in 0..<dataPoints.count {
            
           // let dataEntry = BarChartDataEntry(value: values2[i], xIndex: i)
            let dataEntry = BarChartDataEntry(x: Double(i), y: values2[i])
            dataEntries2.append(dataEntry)
        }
        
        for i in 0..<dataPoints.count {
            
          //  let dataEntry = BarChartDataEntry(value: values3[i], xIndex: i)
             let dataEntry = BarChartDataEntry(x: Double(i), y: values3[i])
            dataEntries3.append(dataEntry)
        }
        
        
        for i in 0..<dataPoints.count {
            
           // let dataEntry = BarChartDataEntry(value: values4[i], xIndex: i)
            let dataEntry = BarChartDataEntry(x: Double(i), y: values4[i])
            dataEntries4.append(dataEntry)
        }
        
        for i in 0..<dataPoints.count {
            
           // let dataEntry = BarChartDataEntry(value: values5[i], xIndex: i)
            let dataEntry = BarChartDataEntry(x: Double(i), y: values5[i])
            dataEntries5.append(dataEntry)
        }
        
        
//        for i in 0..<dataPoints.count
//        {
//            let dr=BarChartDataEntry(values:[values[i],values2[i],values3[i],values4[i],values5[i]],xIndex:i)
//            de.append(dr)
//            
//        }
        
//        let chartDataSet1 = BarChartDataSet(yVals: de, label: "Units Sold")
         var de: [BarChartDataEntry] = []
        
        for i in 0..<dataPoints.count
        {
            //let dr = BarChartDataEntry(values:[values[i],values2[i],values3[i],values4[i],values5[i]],xIndex:i)
            let dr = BarChartDataEntry(x: Double(i), yValues: [values[i],values2[i],values3[i],values4[i],values5[i]])
            de.append(dr)
            
        }

        let chartDataSet1 = BarChartDataSet(values: de, label: "")
        chartDataSet1.stackLabels=["0-30","30-60","60-90","90-120",">120"]
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: " ")
        let chartDataSet2 = BarChartDataSet(values: dataEntries2, label: " ")
        let chartDataSet3 = BarChartDataSet(values: dataEntries3, label: " ")
        let chartDataSet4 = BarChartDataSet(values: dataEntries4, label: " ")
        let chartDataSet5 = BarChartDataSet(values: dataEntries5, label: " ")
        
        for _ in 0..<dataPoints.count {
            
            
            colors.append(UIColor.init(red: 181/255.0, green: 255/255.0, blue: 122/255.0, alpha: 1.0))
        }
        
        for _ in 0..<dataPoints.count {
            
            
            colors1.append(UIColor.init(red: 247/255.0, green: 240/255.0, blue: 122/255.0, alpha: 1.0))
        }
        for _ in 0..<dataPoints.count {
            
            
            colors2.append(UIColor.init(red: 254/255.0, green: 198/255.0, blue: 122/255.0, alpha: 1.0))
        }
        
        for _ in 0..<dataPoints.count {
            
            
            colors3.append(UIColor.init(red: 120/255.0, green: 230/255.0, blue: 255/255.0, alpha: 1.0))
        }
        for _ in 0..<dataPoints.count {
            
            
            colors4.append(UIColor.init(red: 252/255.0, green: 111/255.0, blue: 135/255.0, alpha: 1.0))
        }
        
        Barchartviews.descriptionTextColor = UIColor.white
        chartDataSet.colors = colors
        chartDataSet2.colors = colors1
        chartDataSet3.colors = colors2
        chartDataSet4.colors = colors3
        chartDataSet5.colors = colors4
        let dataSets:[BarChartDataSet]=[chartDataSet,chartDataSet2,chartDataSet3,chartDataSet4,chartDataSet5]
        //let data = BarChartData(xVals: dataPoints, dataSets: dataSets)
        let data = BarChartData(dataSet: dataSets as! IChartDataSet)
        Barchartviews.data = data
        Barchartviews.xAxis.labelPosition = .bottom
        Barchartviews.rightAxis.enabled=false
        Barchartviews.xAxis.drawAxisLineEnabled = true
        Barchartviews.descriptionText = ""
        Barchartviews.rightAxis.drawGridLinesEnabled = true
       
        
        Barchartviews.data = data
        
        Barchartviews.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .easeInBounce)
        
//        chartDataSet.colors  =   ChartColorTemplates.colorful()
//        chartDataSet3.colors =  ChartColorTemplates.colorful()
//        chartDataSet4.colors =  ChartColorTemplates.colorful()
//        
//        let dataSets:[BarChartDataSet]=[chartDataSet1]
//        chartDataSet1.barSpace=0.7
//        let mnths=[""]
//        
//        let data = BarChartData(xVals: mnths, dataSets: dataSets)
//        
//        
//        Barchartviews.descriptionText = ""
//        
//        
//        Barchartviews.rightAxis.drawGridLinesEnabled = false
//        Barchartviews.rightAxis.drawAxisLineEnabled = false
//        Barchartviews.rightAxis.drawLabelsEnabled = false
//        Barchartviews.data = data
//        Barchartviews.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .EaseInBounce)

        
        
    }
    

    
   
    func setChart(dataPoints: [String], values: [Double],isanimated:Bool) {
            var dataEntries: [ChartDataEntry] = []
            for i in 0..<dataPoints.count {
                
                let dataEntry = ChartDataEntry(x: values[i], y: Double(i))
                dataEntries.append(dataEntry)
                
            }
        
    
        let pieChartDataSet = PieChartDataSet(values: dataEntries, label: "Target/Sales")
            
            //let pieChartData = PieChartData(xVals: dataPoints, dataSet: pieChartDataSet)
            let pieChartData = PieChartData(dataSet: pieChartDataSet)
            pieChartData.setDrawValues(false)
           
            PieChartViews.data = pieChartData
            PieChartViews.holeRadiusPercent=0.8
            PieChartViews.holeColor=UIColor.clear
            if(isanimated==false)
            {
              
            }
            else
            {
                PieChartViews.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: ChartEasingOption.easeOutQuad)
            }
            PieChartViews.legend.enabled = false
            var colors: [UIColor] = []
            
            for _ in 0..<dataPoints.count {
                
             
                colors.append(UIColor.init(red: 30/255.0, green: 184/255.0, blue: 120/255.0, alpha: 1.0))
                colors.append(UIColor.init(red: 239/255.0, green: 88/255.0, blue: 88/255.0, alpha: 1.0))
            }
    
            pieChartDataSet.colors = colors
       

    }

    
    func setChartBarGroupDataSet(dataPoints: [String], values: [Double], values2: [Double],sortIndex:Int) {
        
        var dataEntries: [BarChartDataEntry] = []
        var dataEntries2: [BarChartDataEntry] = []
       
        let xAxis:XAxis = Barchartviews.xAxis
         xAxis.axisLineWidth=10
        
        
        
        for i in 0..<dataPoints.count {
            
           // let dataEntry = BarChartDataEntry(value: values[i], xIndex: i)
            let dataEntry = BarChartDataEntry(x: Double(i), y: values[i])
            dataEntries.append(dataEntry)
        }
        
        
        for i in 0..<dataPoints.count {
            
           // let dataEntry = BarChartDataEntry(value: values2[i], xIndex: i)
              let dataEntry = BarChartDataEntry(x: Double(i), y: values2[i])
            dataEntries2.append(dataEntry)
        }
        
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: " ")
        let chartDataSet2 = BarChartDataSet(values: dataEntries2, label: " ")
        
       
        
        
        let dataSets: [BarChartDataSet] = [chartDataSet,chartDataSet2]
        
       // let data = BarChartData(xVals: months, dataSets: dataSets)
        let data = BarChartData(dataSet: dataSets as! IChartDataSet)
        chartDataSet.colors = ChartColorTemplates.colorful()
       
        chartDataSet2.colors = ChartColorTemplates.colorful()
    
        Barchartviews.data = data
        
        Barchartviews.descriptionText = ""
        
        //Barchartviews.barData?.groupSpace=0.8
        Barchartviews.barData?.barWidth = 0.8
        Barchartviews.rightAxis.drawGridLinesEnabled = false
        Barchartviews.rightAxis.drawAxisLineEnabled = false
        Barchartviews.rightAxis.drawLabelsEnabled = false
        
        
        Barchartviews.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .easeInBounce)
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
