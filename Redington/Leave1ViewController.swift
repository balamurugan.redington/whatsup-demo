//
//  Leave1ViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 29/12/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import Firebase
import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import KCFloatingActionButton

class Leave1ViewController: UIViewController, KCFloatingActionButtonDelegate, UITextFieldDelegate, UITextViewDelegate {
    
    @IBOutlet weak var aftnoonbtn: UIButton!
    @IBOutlet weak var frntnoon: UIButton!
    @IBOutlet weak var meternitybtn: UIButton!
    @IBOutlet weak var meternitylbl: UILabel!
    @IBOutlet weak var statusenquirybtn: UIButton!
    @IBOutlet weak var ScrollView: UIScrollView!
    @IBOutlet weak var containerview: UIView!
    @IBOutlet weak var reasontxtview: UITextView!
    @IBOutlet weak var fromdatetf: UITextField!
    @IBOutlet weak var todatetf: UITextField!
    @IBOutlet weak var totaldayslbl: UILabel!
    @IBOutlet weak var submitbtn: UIButton!
    @IBOutlet weak var pickerview: UIView!
    @IBOutlet weak var lopdayslbl: UILabel!
    @IBOutlet weak var datepicker: UIDatePicker!
    @IBOutlet weak var maternityview: UIView!
    @IBOutlet weak var viewtable: UIView!
    @IBOutlet weak var selecttf: UITextField!
    @IBOutlet weak var pritf: UITextField!
    @IBOutlet weak var castf: UITextField!
    @IBOutlet weak var sicktf: UITextField!
    @IBOutlet weak var priviledgelbl: UILabel!
    @IBOutlet weak var casuallbl: UILabel!
    @IBOutlet weak var sicklbl: UILabel!
    @IBOutlet weak var micarriagebulletbtn: UIButton!
    @IBOutlet weak var maternitybulletbtn: UIButton!
    @IBOutlet weak var priviledgeballbl: UILabel!
    @IBOutlet weak var authbtn: UIButton!
    @IBOutlet weak var casualballbl: UILabel!
    @IBOutlet weak var sickballabl: UILabel!
    
    var currenttextfield=String()
    var type=String()
    var type1=String()
    var MeternityChange = String()
    var count = 0
    var responsestring2 = NSMutableArray()
    var responsestring3 = String()
    var leaveresponse = String()
    var checkbox = UIImage(named:"Rememberme.png")
    var uncheckbox = UIImage(named:"tick.png")
    var isboxclicked = Bool()
    var isboxclicked1 = Bool()
    var checkbox1 = UIImage(named:"Rememberme.png")
    var uncheckbox1 = UIImage(named:"tick.png")
    var date1 : NSDate!
    var date2 : NSDate!
    var dayscheck: Bool!
    var Movedup=Bool()
    var uploadfile=NSMutableDictionary()
    var jsonresult=NSDictionary()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewtable.layer.masksToBounds=true
        viewtable.layer.cornerRadius=10.0
        self.viewtable.layer.borderWidth = 0.5
        selecttf.isUserInteractionEnabled = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationmethod), name: NSNotification.Name(rawValue: "GetNotification"), object: nil)
        //swipe
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(Leave1ViewController.respondToSwipeGesture(_:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeRight)
        self.customnavigation()
        self.addDoneButtonOnKeyboard()
        statusenquirybtn.layer.cornerRadius = 5.0
        statusenquirybtn.layer.borderWidth = 1.0
        reasontxtview.layer.cornerRadius = 5.0
        reasontxtview.layer.borderWidth = 1.0
        self.reasontxtview.layer.borderColor = UIColor(red:211/255.0, green:211/255.0, blue:211/255.0, alpha: 1.0).cgColor
        submitbtn.layer.cornerRadius = 5.0
        submitbtn.layer.borderWidth = 1.0
        self.submitbtn.layer.borderColor = UIColor(red:211/255.0, green:211/255.0, blue:211/255.0, alpha: 1.0).cgColor
        
        self.Loaddata2()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        selecttf.delegate = self
        fromdatetf.delegate = self
        todatetf.delegate = self
        reasontxtview.delegate = self
        pritf.delegate = self
        sicktf.delegate = self
        castf.delegate = self
        fromdatetf.tag = 1
        todatetf.tag = 2
        reasontxtview.tag = 3
        pritf.tag = 4
        sicktf.tag = 5
        castf.tag = 6
        pritf.tag = 7
        sicktf.tag = 8
        castf.tag = 9
        
        totaldayslbl.text = "0"
        lopdayslbl.text = "0"
        MeternityChange = "0"
        
        self.pritf.isUserInteractionEnabled = true
        self.castf.isUserInteractionEnabled = true
        self.sicktf.isUserInteractionEnabled = true
        
        pritf.keyboardType = .decimalPad
        sicktf.keyboardType = .decimalPad
        castf.keyboardType = .decimalPad
        
        type = ""
        type1 = ""
        isboxclicked = true
        isboxclicked1 = true
        dayscheck = false
        
        frntnoon.setImage(uncheckbox1, for: UIControlState.normal)
        aftnoonbtn.setImage(uncheckbox1, for: UIControlState.normal)
        
        pritf.addTarget(self, action: #selector(Leave1ViewController.textFieldDidBeginEditing(_:)), for: UIControlEvents.editingChanged)
        sicktf.addTarget(self, action: #selector(Leave1ViewController.textFieldDidBeginEditing(_:)), for: UIControlEvents.editingChanged)
        castf.addTarget(self, action: #selector(Leave1ViewController.textFieldDidBeginEditing(_:)), for: UIControlEvents.editingChanged)
        
        NotificationCenter.default.addObserver(self, selector: #selector(Leave1ViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(Leave1ViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        ScrollView.addSubview(containerview)
        let arrow = UIImageView()
        let image = UIImage(named: "cal.png")
        arrow.image = image!.imageWithColor(tintColor: UIColor.init(red:30/255.0 , green: 184/255.0, blue: 120/255.0, alpha: 1.0)).withRenderingMode(.alwaysOriginal)
        arrow.frame = CGRect(x: 0.0, y: 0.0, width: arrow.image!.size.width+10.0, height: 15)
        arrow.contentMode = UIViewContentMode.center
        self.fromdatetf.rightView = arrow;
        self.fromdatetf.rightViewMode = UITextFieldViewMode.always
        
        let arrow1 = UIImageView()
        let image1 = UIImage(named: "cal.png")
        arrow1.image = image1!.imageWithColor(tintColor: UIColor.init(red:30/255.0 , green: 184/255.0, blue: 120/255.0, alpha: 1.0)).withRenderingMode(.alwaysOriginal)
        arrow1.frame = CGRect(x: 0.0, y: 0.0, width: arrow.image!.size.width+10.0, height: 15)
        arrow1.contentMode = UIViewContentMode.center
        self.todatetf.rightView = arrow1;
        self.todatetf.rightViewMode = UITextFieldViewMode.always
    }
    
    func respondToSwipeGesture(_ gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right: break
            case UISwipeGestureRecognizerDirection.down: break
            case UISwipeGestureRecognizerDirection.left:
                self.Navigate()
            case UISwipeGestureRecognizerDirection.up: break
            default:
                print("")
            }
        }
    }
   
    func Navigate() {
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
               self.local_activityIndicator_stop()
                let slideInFromLeftTransition = CATransition()
                slideInFromLeftTransition.type = kCAGravityTop
                slideInFromLeftTransition.subtype = kCATransitionFromLeft
                slideInFromLeftTransition.duration = 0.2
                slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "PermissionViewController") as! PermissionViewController
                self.navigationController?.view.layer.add(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
                self.navigationController?.pushViewController(lvc, animated: false)
            }
        }
    }
    
    func keyboardWillShow(_ notification: NSNotification) {
        if LibraryAPI.sharedInstance.poptextfieldclicked == false {
            if Movedup == false {
                self.view.frame.origin.y -= 40
                Movedup=true
            }
        }
    }
    
    func keyboardWillHide(_ notification: NSNotification) {
        if LibraryAPI.sharedInstance.poptextfieldclicked == true {
            self.view.frame.origin.y += 0
            Movedup=false
        }
    }
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(LeaveViewController.doneButtonAction))
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        doneToolbar.items = items as AnyObject as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        pritf.inputAccessoryView = doneToolbar
        sicktf.inputAccessoryView = doneToolbar
        castf.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction() {
        pritf.resignFirstResponder()
        sicktf.resignFirstResponder()
        castf.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if responsestring2.count > 0 {
            if textField == pritf {
                if fromdatetf.text != "" && todatetf.text != "" {
                    if pritf.text == "." {
                        pritf.text = "0."
                    } else if pritf.text == "0.." {
                        pritf.text = "0."
                    } else {
                        
                    }
                    
                    if self.priviledgelbl.text == "0.0" {
                        self.pritf.isUserInteractionEnabled = false
                    } else {
                        self.pritf.isUserInteractionEnabled = true
                        
                        if pritf.text == "" {
                            let total = (self.responsestring2.object(at: 0) as AnyObject).object(forKey: "PRIVILAGE LEAVE") as? Double
                            
                            let priviledge = total! + 0
                            self.priviledgeballbl.text = String(priviledge)
                            let total1: Double = Double(self.totaldayslbl.text!)!
                            
                            var pri = Double()
                            var sic = Double()
                            var cas = Double()
                            
                            if pritf.text == "" {
                                pri = 0
                            } else {
                                pri = Double(self.pritf.text!)!
                            }
                            
                            if sicktf.text == "" {
                                sic = 0
                            } else {
                                sic = Double(self.sicktf.text!)!
                            }
                            
                            if castf.text == "" {
                                cas = 0
                            } else {
                                cas = Double(self.castf.text!)!
                            }
                            
                            let totleave = pri + sic + cas
                            let priviledge1 = total1 - totleave
                            if totleave > total1 {
                                self.lopdayslbl.text = "0"
                            } else {
                                self.lopdayslbl.text = String(priviledge1)
                            }
                        } else {
                            let total = (self.responsestring2.object(at: 0) as AnyObject).object(forKey: "PRIVILAGE LEAVE") as! Double
                            let myvalue: Double = Double(self.pritf.text!)!
                            
                            if myvalue > total {
                                self.pritf.text = ""
                            } else {
                                let priviledge = Double(total) - Double(myvalue)
                                self.priviledgeballbl.text = String(priviledge)
                                
                                let total1: Double = Double(self.totaldayslbl.text!)!
                                let pri = Double(self.pritf.text!)
                                var sic = Double()
                                var cas = Double()
                                
                                if sicktf.text == "" {
                                    sic = 0
                                } else {
                                    sic = Double(self.sicktf.text!)!
                                }
                                
                                if castf.text == "" {
                                    cas = 0
                                } else {
                                    cas = Double(self.castf.text!)!
                                }
                                
                                let totleave: Double = pri! + sic + cas
                                let priviledge1 = total1 - totleave
                                
                                if totleave > total1 {
                                    self.lopdayslbl.text = "0"
                                } else{
                                    self.lopdayslbl.text = String(priviledge1)
                                }
                            }
                        }
                    }
                } else {
                    self.pritf.text = ""
                }
            } else if textField == sicktf {
                if fromdatetf.text != "" && todatetf.text != "" {
                    if sicktf.text == "." {
                        sicktf.text = "0."
                    } else if sicktf.text == "0.." {
                        sicktf.text = "0."
                    } else {
                        
                    }
                    
                    if self.sicklbl.text == "0.0" {
                        self.sicktf.isUserInteractionEnabled = false
                        self.sicktf.text = ""
                    } else {
                        self.sicktf.isUserInteractionEnabled = true
                        if sicktf.text == "" {
                            self.sicktf.isUserInteractionEnabled = true
                            let total = (self.responsestring2.object(at: 0) as AnyObject).object(forKey: "SICK LEAVE") as? Double
                            
                            let priviledge = total! + 0
                            self.sickballabl.text = String(priviledge)
                            
                            let total1: Double = Double(self.totaldayslbl.text!)!
                            var pri = Double()
                            var sic = Double()
                            var cas = Double()
                            
                            if pritf.text == "" {
                                pri = 0
                            } else {
                                pri = Double(self.pritf.text!)!
                            }
                            
                            if sicktf.text == "" {
                                sic = 0
                            } else {
                                sic = Double(self.sicktf.text!)!
                            }
                            
                            if castf.text == "" {
                                cas = 0
                            } else {
                                cas = Double(self.castf.text!)!
                            }
                            
                            let totleave = pri + sic + cas
                            let Sick1 = total1 - totleave
                            if totleave > total1 {
                                self.lopdayslbl.text = "0"
                            } else {
                                self.lopdayslbl.text = String(Sick1)
                            }
                        } else {
                            let total: Double = (self.responsestring2.object(at: 0) as AnyObject).object(forKey: "SICK LEAVE") as! Double
                            let myvalue: Double = Double(self.sicktf.text!)!
                            
                            if myvalue > total {
                                self.sicktf.text = ""
                            } else {
                                let priviledge = total - myvalue
                                self.sickballabl.text = String(priviledge)
                                
                                let total1: Double = Double(self.totaldayslbl.text!)!
                                var pri = Double()
                                var sic = Double(self.sicktf.text!)
                                var cas = Double()
                                
                                if pritf.text == "" {
                                    pri = 0
                                } else {
                                    pri = Double(self.pritf.text!)!
                                }
                                
                                if castf.text == "" {
                                    cas = 0
                                } else {
                                    cas = Double(self.castf.text!)!
                                }
                                
                                let totleave: Double = pri + sic! + cas
                                let sick = total1 - totleave
                                if totleave > total1 {
                                    self.lopdayslbl.text = "0"
                                } else{
                                    self.lopdayslbl.text = String(sick)
                                }
                            }
                        }
                    }
                } else {
                    self.sicktf.text = ""
                }
            } else if textField == castf {
                if fromdatetf.text != "" && todatetf.text != "" {
                    if castf.text == "." {
                        castf.text = "0."
                    } else if castf.text == "0.." {
                        castf.text = "0."
                    } else {
                        
                    }
                    
                    if self.casuallbl.text == "0.0" {
                        self.castf.isUserInteractionEnabled = false
                        self.castf.text = ""
                    } else {
                        self.castf.isUserInteractionEnabled = true
                        if castf.text == "" {
                            self.castf.isUserInteractionEnabled = true
                            let total = (self.responsestring2.object(at: 0) as AnyObject).object(forKey: "CASUAL LEAVE") as? Double
                            
                            let priviledge = total! + 0
                            self.casualballbl.text = String(priviledge)
                            let total1: Double = Double(self.totaldayslbl.text!)!
                            
                            var pri = Double()
                            var sic = Double()
                            var cas = Double()
                            
                            if pritf.text == "" {
                                pri = 0
                            } else {
                                pri = Double(self.pritf.text!)!
                            }
                            
                            if sicktf.text == "" {
                                sic = 0
                            } else {
                                sic = Double(self.sicktf.text!)!
                            }
                            
                            if castf.text == "" {
                                cas = 0
                            } else {
                                cas = Double(self.castf.text!)!
                            }
                            
                            let totleave: Double = pri + sic + cas
                            let casual = total1 - totleave
                            if totleave > total1 {
                                self.lopdayslbl.text = "0"
                            } else {
                                self.lopdayslbl.text = String(casual)
                            }
                        } else {
                            let total: Double = (self.responsestring2.object(at: 0) as AnyObject).object(forKey: "CASUAL LEAVE") as! Double
                            let myvalue: Double = Double(self.castf.text!)!
                            
                            if myvalue > total {
                                self.castf.text = ""
                            } else {
                                let priviledge = total - myvalue
                                self.casualballbl.text = String(priviledge)
                                
                                let total1: Double = Double(self.totaldayslbl.text!)!
                                var pri = Double()
                                var sic = Double()
                                var cas = Double(self.castf.text!)
                                
                                if pritf.text == "" {
                                    pri = 0
                                } else {
                                    pri = Double(self.pritf.text!)!
                                }
                                
                                if sicktf.text == "" {
                                    sic = 0
                                } else {
                                    sic = Double(self.sicktf.text!)!
                                }
                                
                                let totleave: Double = pri + sic + cas!
                                let casal = total1 - totleave
                                if totleave > total1 {
                                    self.lopdayslbl.text = "0"
                                } else {
                                    self.lopdayslbl.text = String(casal)
                                }
                            }
                        }
                    }
                } else {
                    self.castf.text = ""
                }
            }
        } else {
            self.showToast(message: "Delay in Server Response")
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.Loaddata2()
        ScrollView.isScrollEnabled=true
        ScrollView.contentSize = CGSize(width: ScrollView.frame.size.width, height:700)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    
    func customnavigation() {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: 65)
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(Leave1ViewController.buttonClicked(_:)),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Leave/Permission"
        
    }
    
    func buttonClicked(_ sender:UIButton) {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "HRD1DetailsViewController")), animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        pritf.resignFirstResponder()
        sicktf.resignFirstResponder()
        castf.resignFirstResponder()
        return true
    }
    
    @IBAction func Done(_ sender: AnyObject) {
        datepicker.isHidden=true
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        if currenttextfield == "Date" {
            fromdatetf.text = dateFormatter.string(from: datepicker.date)
            pickerview.isHidden = true
            self.Loaddata()
        } else if currenttextfield=="TO" {
            if fromdatetf.text == "" {
                self.pickerview.isHidden = true
                self.showalert(title: "Alert", message: "Selece From Date First", buttontxt: "Ok")
            } else {
                todatetf.text=dateFormatter.string(from: datepicker.date)
                pickerview.isHidden = true
                self.Loaddata1()
                let dte = fromdatetf.text!
                let dte2 = todatetf.text!
                date1 = dateFormatter.date(from: dte)! as NSDate
                date2 = dateFormatter.date(from: dte2)! as NSDate
                let diff = daysBetweenDates(startDate: date1,endDate: date2)
                totaldayslbl.text = String(Double(diff + 1))
                lopdayslbl.text = String(Double(diff + 1))
            }
        }
    }
    
    func daysBetweenDates(startDate: NSDate, endDate: NSDate) -> Int {
        let calendar = NSCalendar.current
        let unitFlags = Set<Calendar.Component>([.day])
        let anchorComponents = calendar.dateComponents(unitFlags, from: startDate as Date,  to: endDate as Date)
        return anchorComponents.day!
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.tag == 1 {
            pickerview.isHidden=false
            datepicker.isHidden = false
            datepicker.datePickerMode = UIDatePickerMode.date
            currenttextfield="Date"
            return false
        }
        
        if textField.tag == 2 {
            pickerview.isHidden=false
            datepicker.isHidden = false
            datepicker.datePickerMode = UIDatePickerMode.date
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            currenttextfield="TO"
            return false
        }
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            reasontxtview.resignFirstResponder()
            return false
        }
        return true
    }
    
    @IBAction func aftbtnclicked(_ sender: AnyObject) {
        if fromdatetf.text == "" {
            
        } else if todatetf.text == "" {
            
        } else if fromdatetf.text == "" && todatetf.text == "" {
            
        } else {
            let count = Double(totaldayslbl.text!)
            if isboxclicked == true {
                type = "1"
                aftnoonbtn.setImage(checkbox1, for:UIControlState.normal)
                if count == 0 {
                    
                } else {
                    totaldayslbl.text = String(count! - 0.5)
                    lopdayslbl.text = String(count! - 0.5)
                }
                isboxclicked = false
            } else {
                type = ""
                aftnoonbtn.setImage(uncheckbox1, for: UIControlState.normal)
                isboxclicked = true
                totaldayslbl.text = String(count! + 0.5)
                lopdayslbl.text = String(count! + 0.5)
            }
        }
    }
    
    
    @IBAction func forenoon(_ sender: AnyObject) {
        if fromdatetf.text == "" {
            
        } else if todatetf.text == "" {
            
        } else if fromdatetf.text == "" && todatetf.text == "" {
            
        } else {
            let count = Double(totaldayslbl.text!)
            if isboxclicked1 == true {
                type1="1"
                frntnoon.setImage(checkbox1, for:UIControlState.normal)
                if count == 0 {
                    
                } else {
                    totaldayslbl.text = String(count! - 0.5)
                    lopdayslbl.text = String(count! - 0.5)
                }
                isboxclicked1 = false
            } else {
                type1 = ""
                frntnoon.setImage(uncheckbox1, for: UIControlState.normal)
                isboxclicked1 = true
                totaldayslbl.text = String(count! + 0.5)
                lopdayslbl.text = String(count! + 0.5)
            }
        }
    }
    
    
    @IBAction func MeternityAction(_ sender: UIButton) {
        if self.fromdatetf.text != "" {
            if isboxclicked1 == true {
                self.pritf.isUserInteractionEnabled = false
                self.sicktf.isUserInteractionEnabled = false
                self.castf.isUserInteractionEnabled = false
                
                self.pritf.text = ""
                self.sicktf.text = ""
                self.castf.text = ""
                
                self.aftnoonbtn.isUserInteractionEnabled = false
                self.frntnoon.isUserInteractionEnabled = false
                let countDate = NSDate.changeDaysBy(days: 181)
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                self.todatetf.text = dateFormatter.string(from: countDate as Date)
                pickerview.isHidden = true
                isboxclicked1 = false
                totaldayslbl.text = "182"
                lopdayslbl.text = "0.0"
                MeternityChange = "181"
                todatetf.isUserInteractionEnabled = false
            } else {
                self.pritf.isUserInteractionEnabled = true
                self.sicktf.isUserInteractionEnabled = true
                self.castf.isUserInteractionEnabled = true
                self.pritf.text = ""
                self.sicktf.text = ""
                self.castf.text = ""
                
                self.aftnoonbtn.isUserInteractionEnabled = true
                self.frntnoon.isUserInteractionEnabled = true
                pickerview.isHidden = true
                totaldayslbl.text = "0.0"
                lopdayslbl.text = "0.0"
                self.todatetf.text = ""
                
                MeternityChange = "0"
                isboxclicked1 = true
            }
        } else {
            self.showalert(title: "Sorry", message: "Please Enter the From Date", buttontxt: "Retry")
        }
    }
    
    
    
    @IBAction func maternitybulletaction(_ sender: AnyObject) {
        let countDate = NSDate.changeDaysBy(days: 181)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        self.todatetf.text = dateFormatter.string(from: countDate as Date)
        
        isboxclicked1 = false
        totaldayslbl.text = "182"
        lopdayslbl.text = "0.0"
        
        MeternityChange = "181"
        todatetf.isUserInteractionEnabled = false
    }
    
    @IBAction func miscarriagebulletaction(_ sender: AnyObject) {
        let countDate = NSDate.changeDaysBy(days: 41)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        self.todatetf.text = dateFormatter.string(from: countDate as Date)
        isboxclicked1 = false
        totaldayslbl.text = "42"
        lopdayslbl.text = "0.0"
        MeternityChange = "42"
        todatetf.isUserInteractionEnabled = false
    }
    
    //Leave Apply
    @IBAction func submitAction(_ sender: AnyObject) {
        
        if self.fromdatetf.text!.isEmpty || self.todatetf.text!.isEmpty ||  self.reasontxtview.text!.isEmpty {
            self.showalert(title: "Sorry", message: "Please Enter the Fields", buttontxt: "Ok")
        } else if((self.pritf.text!.isEmpty && self.sicktf.text!.isEmpty && self.castf.text!.isEmpty)) {
            self.showalert(title: "Sorry", message: "Please Enter Types of Leaves", buttontxt: "Ok")
        } else {
            let noofdays: Double = Double(self.totaldayslbl.text!)!
            var pri = Double()
            var sic = Double()
            var cas = Double()
            
            if pritf.text == "" {
                pri = 0
            } else {
                pri = Double(self.pritf.text!)!
            }
            
            if sicktf.text == "" {
                sic = 0
            } else {
                sic = Double(self.sicktf.text!)!
            }
            
            if castf.text == "" {
                cas = 0
            } else {
                cas = Double(self.castf.text!)!
            }
            
            let totleave: Double = pri + sic + cas
            if(totleave > noofdays || totleave < noofdays) {
                self.showalert(title: "Sorry", message: "Total Days Mismatch..!", buttontxt: "Retry")
            } else {
                var name = selecttf.text!.components(separatedBy: " - ")
                let head: String = name[0]
                
                uploadfile.setObject(LibraryAPI.sharedInstance.Userid.stringByRemovingWhitespaces as String , forKey: "UserId" as NSCopying)
                uploadfile.setObject(fromdatetf.text!.stringByRemovingWhitespaces  as String, forKey: "FromDate" as NSCopying)
                uploadfile.setObject(todatetf.text!.stringByRemovingWhitespaces  as String, forKey: "ToDate" as NSCopying)
                uploadfile.setObject(totaldayslbl.text!.stringByRemovingWhitespaces  as String, forKey: "NoofDay" as NSCopying)
                uploadfile.setObject(type1  as String, forKey: "ForenoonFlag" as NSCopying)
                uploadfile.setObject(type  as String, forKey: "AfternoonFlag" as NSCopying)
                uploadfile.setObject(pritf.text!.stringByRemovingWhitespaces  as String, forKey: "PrivilegeEarnedLeave" as NSCopying)
                uploadfile.setObject(sicktf.text!.stringByRemovingWhitespaces  as String, forKey: "SickLeave" as NSCopying)
                uploadfile.setObject(castf.text!.stringByRemovingWhitespaces  as String, forKey: "CasualLeave" as NSCopying)
                uploadfile.setObject(MeternityChange as String, forKey: "MaternityLeave" as NSCopying)
                uploadfile.setObject(""  as String, forKey: "TypeOfLeave" as NSCopying)
                uploadfile.setObject(reasontxtview.text!.stringByRemovingWhitespaces  as String, forKey: "Reason" as NSCopying)
                uploadfile.setObject(head.stringByRemovingWhitespaces  as String, forKey: "Authoriser" as NSCopying)
                uploadfile.setObject(""  as String, forKey: "AUTHORIZED" as NSCopying)
                
                let size = CGSize(width: 30, height:30)
                self.local_activityIndicator_start()
                
                DispatchQueue.global(qos: .background).async {
                    self.uploaddata()
                    DispatchQueue.main.async {
                       self.local_activityIndicator_stop()
                        if self.jsonresult.object(forKey: "ErrorMessage") as? String  == "" {
                            let alertController = UIAlertController(title: "Success", message: "Your Ticket No : \(self.jsonresult.object(forKey: "LeaveAppNumber") as! String)" , preferredStyle: .alert)
                            let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
                            }
                            alertController.addAction(cancelAction)
                            self.present(alertController, animated: true, completion:nil)
                        } else {
                            let error = self.jsonresult.object(forKey: "ErrorMessage") as? String
                            let leave = self.jsonresult.object(forKey: "LeaveAppNumber") as? String
                            
                            let alertController = UIAlertController(title: "Alert", message:"\(error!)" , preferredStyle: .alert)
                            
                            let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                self.fromdatetf.text = ""
                                self.todatetf.text = ""
                                self.pritf.text = ""
                                self.sicktf.text = ""
                                self.castf.text = ""
                                self.reasontxtview.text = ""
                                self.selecttf.text = ""
                            }
                            alertController.addAction(cancelAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                }
                dismiss(animated: true, completion: nil)
            }
        }
    }
    
    
    func uploaddata() {
        let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)LeaveDetails.asmx/LeaveApply")! as URL)
        request.httpMethod = "POST"
        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
        
        request.httpBody = try? JSONSerialization.data(withJSONObject: self.uploadfile, options: JSONSerialization.WritingOptions(rawValue: 0))
        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
        
        do {
            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
            leaveresponse = String(data: data, encoding: String.Encoding.utf8)!
            jsonresult = try JSONSerialization.jsonObject(with: data, options:[]) as! NSDictionary
        } catch (let e) {
            print(e)
            let fullNameArr = leaveresponse.split(separator: ",")
            let firstName = String(fullNameArr[1])
            let full = firstName.split(separator: ":")
            let Name = String(full[1])
            let full1 = Name.split(separator: "}")
            let Name1 = String(full1[0])
            
            self.showalert(title: "Sorry", message: "\(Name1)", buttontxt: "Ok")
        }
    }
    
    func Loaddata() {
       // responsestring3 = LibraryAPI.sharedInstance.MyRequestUrl(url: "\(LibraryAPI.sharedInstance.GlobalUrl)LeaveDetails.asmx/HolidayCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Date=\(self.fromdatetf.text!)&Flag=")
        self.getStringsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)LeaveDetails.asmx/HolidayCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Date=\(self.fromdatetf.text!)&Flag=", completion: { (jsonstr) in
            self.responsestring3 = jsonstr
        
            if self.responsestring3 == "" {
            
        } else {
            let alert = UIAlertController(title: "Sorry", message:"\(self.responsestring3)", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler:
                { (ACTION :UIAlertAction!)in
                    self.fromdatetf.text = ""
            }))
            self.present(alert, animated: true, completion: nil)
        }
            })
    }
    
    
    func Loaddata1() {
        //responsestring3 = LibraryAPI.sharedInstance.MyRequestUrl(url: "\(LibraryAPI.sharedInstance.GlobalUrl)LeaveDetails.asmx/HolidayCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Date=\(self.todatetf.text!)&Flag=1")
        self.getStringsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)LeaveDetails.asmx/HolidayCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Date=\(self.todatetf.text!)&Flag=1", completion: { (jsonstr) in
            self.responsestring3 = jsonstr
        
        if self.responsestring3 == "" {
            
        } else {
            let alert = UIAlertController(title: "Sorry", message:"\(self.responsestring3)", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Retry", style: UIAlertActionStyle.default, handler:
                { (ACTION :UIAlertAction!)in
                    self.todatetf.text = ""
                    
            }))
            self.present(alert, animated: true, completion: nil)
        }
        })
    }
    
    
    //Leave Balance Enquiry
    func Loaddata2() {
        //responsestring2 = LibraryAPI.sharedInstance.RequestUrl(url: "\(LibraryAPI.sharedInstance.GlobalUrl)LeaveDetails.asmx/LeaveBalanceEnquiry?UserID=\(LibraryAPI.sharedInstance.Userid)")
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)LeaveDetails.asmx/LeaveBalanceEnquiry?UserID=\(LibraryAPI.sharedInstance.Userid)", completion:  { (jsonmutable) in
            self.responsestring2 = jsonmutable
        
        
        if(self.responsestring2.count>0) {
            self.priviledge()
        } else {
            DispatchQueue.main.async {
                self.showToast(message: "Delay in Server Response")
            }
        }
            })
    }
    
    func priviledge() {
        let priviledge  = (self.responsestring2.object(at: 0) as AnyObject).object(forKey: "PRIVILAGE LEAVE") as? Double
        let sick = (self.responsestring2.object(at: 0) as AnyObject).object(forKey: "SICK LEAVE")as? Double
        let casual  = (self.responsestring2.object(at: 0) as AnyObject).object(forKey: "CASUAL LEAVE") as? Double
        let gender  = (self.responsestring2.object(at: 0) as AnyObject).object(forKey: "GEND88") as? String
        
        self.priviledgelbl.text = String(priviledge!)
        self.sicklbl.text = String(sick!)
        self.casuallbl.text = String(casual!)
        
        self.priviledgeballbl.text = String(priviledge!)
        self.sickballabl.text = String(sick!)
        self.casualballbl.text = String(casual!)
        
        if gender == "M" {
            // self.meternitybtn.hidden = true
            //self.meternitylbl.hidden = true
        } else if gender == "F" {
            //  self.meternitybtn.hidden = true
            // self.meternitylbl.hidden = true
        } else {
            
        }
    }
    
    @IBAction func cancelaction(_ sender: AnyObject) {
        pickerview.isHidden = true
    }
    
    @IBAction func permissionbtn(_ sender: AnyObject) {
        Navigate()
    }
    
    
    @IBAction func authbtnaction(_ sender: AnyObject) {
        if responsestring2.count > 0 {
            let alert:UIAlertController=UIAlertController(title: "Choose option", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let head1 = (self.responsestring2.object(at: 0) as AnyObject).object(forKey: "REPORTING HEAD 1") as! String
            let head2 = (self.responsestring2.object(at: 0) as AnyObject).object(forKey: "REPORTING HEAD 2") as! String
            let head3 = (self.responsestring2.object(at: 0) as AnyObject).object(forKey: "REPORTING HEAD 3") as! String
            
            var alert1 = UIAlertAction()
            var alert2 = UIAlertAction()
            var alert3 = UIAlertAction()
            
            if head1 != "" {
                alert1 = UIAlertAction(title: head1, style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    self.selecttf.text = head1
                }
                alert.addAction(alert1)
            }
            
            if head2 != "" {
                alert2 = UIAlertAction(title: head2, style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    self.selecttf.text = head2
                }
                alert.addAction(alert2)
            }
            
            if head3 != "" {
                alert3 = UIAlertAction(title: head3, style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    self.selecttf.text = head3
                }
                alert.addAction(alert3)
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
            {
                UIAlertAction in
            }
            
            alert.addAction(cancelAction)
            if UIDevice.current.userInterfaceIdiom == .phone {
                self.present(alert, animated: true, completion: nil)
            } else {
                
            }
        } else {
            self.showToast(message: "Delay in Server Response")
        }
    }
    
    
    @IBAction func cancellationbtnaction(_ sender: AnyObject) {
        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "LeaveStatusEnquiryViewController") as! LeaveStatusEnquiryViewController
        self.navigationController?.pushViewController(lvc, animated: true)
    }
    
    func notificationmethod() {
        let userdefaults = UserDefaults.standard
        let value = userdefaults.dictionary(forKey: "Notification")
        
        var title: String!
        var message: String!
        var imagestr: String!
        
        title = value?["title"] as! String
        message = value?["body"] as! String
        imagestr = value?["imageurl"] as! String
        
        if imagestr != nil {
            if imagestr != "" {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: notificationimageViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = notificationimageViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                    
                }
                popup.show(childViewController: container)
            } else {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: PushnotificationtextViewController.instance())
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = PushnotificationtextViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                }
                popup.show(childViewController: container)
            }
        } else {
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: PushnotificationtextViewController.instance())
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
            }
            let container = PushnotificationtextViewController.instance()
            container.closeHandler = { _ in
                popup.dismiss()
            }
            popup.show(childViewController: container)
        }
    }
}
