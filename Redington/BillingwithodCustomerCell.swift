//
//  BillingwithodCustomerCell.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 30/05/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class BillingwithodCustomerCell: UITableViewCell {
    
    @IBOutlet weak var bizcode: UILabel!
    @IBOutlet weak var bizname: UILabel!
    @IBOutlet weak var deletebtn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
