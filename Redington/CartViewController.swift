
//
//  CartViewController.swift
//  Redington
//
//  Created by truetech on 31/08/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
import  CoreData
import KCFloatingActionButton
class CartViewController: UIViewController,ChildViewControllerDelegate,KCFloatingActionButtonDelegate,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet var carttableview: UITableView!
    var texts = [Int:String]()
     var newtexts = [String:String]()
    @IBOutlet var totallabel: UILabel!
    var listfromorder=[Element]()
    var tot=Double()
    var str1=String()
    var cv=PreOrderViewController()
    var fab = KCFloatingActionButton()
    var isbacktobackmode=Bool()
    var sampleArray = NSMutableArray()
    
    let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext

    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.customnavigation()
        self.layoutFAB()
        cv.delegate=self
      
       for i in 0..<listfromorder.count
       {
          let Temptxt=listfromorder[i].vendor
          newtexts[Temptxt]=listfromorder[i].Quantities
        
       }
        
        if(listfromorder.count>0)
        {
         self.UpdateDb()
        self.fetchdbvalues()
        }
        else
        {
        self.fetchdbvalues()
        }
        let cnt = FetchCount()
       
        
       
        LibraryAPI.sharedInstance.badgeCount=String(cnt)
         self.computetotal()

        carttableview.delegate=self
        carttableview.reloadData()
        
     
        // Do any additional setup after loading the view.
    }
    
    func UpdateDb()
    {
        
        
        for i in 0..<listfromorder.count
        {
            let str=listfromorder[i].vendor
            
            let val = Double(listfromorder[i].prices)
            
            
            let val1 = Int(listfromorder[i].Quantities)
            
            
            
            
            let  prices = (Double(val) * Double(val1!)) //Prices * Quantity
         
            let prd=listfromorder[i].Productnames
            
            let vnd=listfromorder[i].Invoicedates
            
             let avl=listfromorder[i].availabilty
            
            let stk=listfromorder[i].Stockroom
            
            
            self.SaveDatatoSQlite(val: str, price: val, Qnty: val1! , products: prd, vendor:vnd,available: avl,Stock:stk)
        }

    }
    
    func UpdateDbfromArray(Quantity:Int)
    {
      
        
        for i in 0..<sampleArray.count
        {
            let dic = sampleArray[i] as! NSDictionary
            let str = dic.object(forKey: "itemcode") as! String
            
            let val = Double(dic.object(forKey: "price") as! Double)
           
            let val1 = newtexts[str]
 
            _ = (Double(val) * Double(val1!)!) //Prices * Quantity
         
            
            let prd = dic.object(forKey: "products") as! String
            
            let vnd = dic.object(forKey: "vendor") as! String
            
            let avl = dic.object(forKey: "availability") as! Int
            
            let Stk = dic.object(forKey: "stock") as! String
            
            self.SaveDatatoSQlite(val: str, price: val, Qnty:Int(val1!)!  , products: prd, vendor:vnd,available: avl,Stock:Stk)
        }
         sampleArray = self.itemMutableArray()

    }

    func itemMutableArray() -> NSMutableArray {
        let set = NSSet(array: sampleArray as [AnyObject])
        return NSMutableArray(array: (set.allObjects))
    }
   
    func customnavigation()
        
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.Notificationbtn.isHidden=true
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(CartViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "My Cart"
        
    }
    
    
    func SaveDatatoSQlite(val:String,price:Double,Qnty:Int,products:String,vendor:String,available:Int,Stock:String)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CartTable")
        
        let entityDescription = NSEntityDescription.entity(forEntityName: "CartTable", in:managedContext)
        
        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        
        fetchRequest.returnsObjectsAsFaults = false
        
        //        let resultPredicate2 = NSPredicate(format: "itemcode = %@", "XIMP0004")
        fetchRequest.predicate=NSPredicate(format: "itemcode == %@", "\(val)")
        
        do {
            
            let result = try managedContext.fetch(fetchRequest) as! [NSManagedObject]
            
            
            
            
                    if(result.count>0)
                    {
                      
                        
                        let dict = NSMutableDictionary()
                        
                        
                        result[0].setValue(price, forKey: "price")
                        
                        result[0].setValue(products, forKey: "products")
                        
                        result[0].setValue(Qnty , forKey: "quantity")
                        
                        result[0].setValue(vendor, forKey: "vendor")
                        
                        result[0].setValue(available, forKey: "availability")
                        
                        result[0].setValue(available, forKey: "availability")
                        
                        result[0].setValue(Stock, forKey: "stock")
                     
                        
                        do {
                            
                            try result[0].managedObjectContext?.save()
                            
                        } catch {
                            
                           
                            
                        }
                        dict.setValue(val, forKeyPath: "itemcode")
                        dict.setValue(price, forKeyPath: "price")
                        dict.setValue(products, forKeyPath: "products")
                        dict.setValue(Qnty, forKeyPath: "quantity")
                        dict.setValue(vendor, forKeyPath: "vendor")
                        dict.setValue(available, forKeyPath: "availability")
                        dict.setValue(Stock, forKeyPath: "stock")
                    }
                    else
                    {
                        
                        let newPerson = NSManagedObject(entity: entityDescription!, insertInto: managedContext)
                       // let newPerson=NSEntityDescription.insertNewObjectForEntityForName("CartTable", inManagedObjectContext: managedContext) as! CartTable
                        newPerson.setValue(val, forKey: "itemcode")
                        newPerson.setValue(price, forKey: "price")
                        newPerson.setValue(products, forKey: "products")
                        newPerson.setValue(Qnty, forKey: "quantity")
                        newPerson.setValue(vendor, forKey: "vendor")
                         newPerson.setValue(available, forKey: "availability")
                          newPerson.setValue(Stock, forKey: "stock")
                     
                        
                        let dict = NSMutableDictionary()
                        dict.setValue(val, forKeyPath: "itemcode")
                        dict.setValue(price, forKeyPath: "price")
                        dict.setValue(products, forKeyPath: "products")
                        dict.setValue(Qnty, forKeyPath: "quantity")
                        dict.setValue(vendor, forKeyPath: "vendor")
                        dict.setValue(available, forKeyPath: "availability")
                        dict.setValue(Stock, forKey: "stock")

                        do {
                            
                            try newPerson.managedObjectContext?.save()
                            
                        } catch {
                            
                          
                            
                        }
                        
                        sampleArray.add(dict)
                        
                        
                       
                       
                    }
         
            
        } catch {
            
            let fetchError = error as NSError
            
            }
 
        do {
            
            let result = try managedContext.fetch(fetchRequest)
            
        } catch {
            
            let fetchError = error as NSError
            
        }
        
        
        
    }
    
   /* func uniq<S : SequenceType, T : Hashable where S.Generator.Element == T>(source: S) -> [T] {
        var buffer = [T]()
        var added = Set<T>()
        for elem in source {
            if !added.contains(elem) {
                buffer.append(elem)
                added.insert(elem)
            }
        }
        return buffer
    }*/
    
    
    
    
    
    func FetchCount()->Int
    {
        do {
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            
            let managedContext = appDelegate.managedObjectContext
        
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
            
            let entityDescription = NSEntityDescription.entity(forEntityName: "CartTable", in: managedContext)
            
            // Configure Fetch Request
            fetchRequest.entity = entityDescription
            
            fetchRequest.returnsObjectsAsFaults = false

            
            let count = try managedContext.count(for: fetchRequest)
            return count
            
           
        } catch let error as NSError {
            return 0
            
        }    
        
    }
    
    
    func fetchdbvaluesafterRemove()
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entity(forEntityName: "CartTable", in: managedContext)
        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        
        fetchRequest.returnsObjectsAsFaults = false
        
        
        do {
            let result = try managedContext.fetch(fetchRequest)
            
            let itemList = result as! [NSManagedObject]
            
              if(itemList.count>0)
            {
                
                for i in 0..<itemList.count
                {
                    let dict = NSMutableDictionary()
                    dict.setValue(itemList[i].value(forKey: "itemcode"), forKeyPath: "itemcode")
                    dict.setValue(itemList[i].value(forKey: "price"), forKeyPath: "price")
                    dict.setValue(itemList[i].value(forKey: "products"), forKeyPath: "products")
                    dict.setValue(itemList[i].value(forKey: "quantity"), forKeyPath: "quantity")
                    dict.setValue(itemList[i].value(forKey: "vendor"), forKeyPath: "vendor")
                    dict.setValue(itemList[i].value(forKey: "availability"), forKeyPath: "availability")
                    dict.setValue(itemList[i].value(forKey: "stock"), forKeyPath: "stock")
                    
                    
                    sampleArray.add(dict)
                    
                    
                    sampleArray=self.itemMutableArray()
                    
    
                }
                
                for i in 0..<sampleArray.count
                {
                    let dic = sampleArray[i] as! NSDictionary
                    let val = dic.object(forKey: "itemcode")as! String
                    newtexts[val]=String(dic.object(forKey: "quantity") as! Int)
                }
                
                
                
                
                carttableview.reloadData()
            }
            else
            {
               
            }
            
            
        } catch {
            let fetchError = error as NSError
        }
        
        
        
    }
    
    func fetchdbvalues()
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entity(forEntityName: "CartTable", in: managedContext)
        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        
        fetchRequest.returnsObjectsAsFaults = false
        
//        let resultPredicate2 = NSPredicate(format: "itemcode = %@", "XIMP0004")
      //  fetchRequest.predicate=NSPredicate(format: "price == %@", "12")
        
        
        do {
            let result = try managedContext.fetch(fetchRequest)
            
            let itemList = result as! [NSManagedObject]
            
            if(itemList.count>0)
            {
            
                for i in 0..<itemList.count
                {
                    let dict = NSMutableDictionary()
                    let dicitem = itemList[i]
                    dict.setValue(dicitem.value(forKey: "itemcode"), forKeyPath: "itemcode")
                    dict.setValue(dicitem.value(forKey:  "price"), forKeyPath: "price")
                    dict.setValue(dicitem.value(forKey: "products"), forKeyPath: "products")
                    dict.setValue(dicitem.value(forKey: "quantity"), forKeyPath: "quantity")
                    dict.setValue(dicitem.value(forKey: "vendor"), forKeyPath: "vendor")
                    dict.setValue(dicitem.value(forKey: "availability"), forKeyPath: "availability")
                    dict.setValue(dicitem.value(forKey: "stock"), forKeyPath: "stock")
                    
                    
                    sampleArray.add(dict)
                    
                    sampleArray=self.itemMutableArray()
                    
                }
            
            for i in 0..<sampleArray.count
            {
                let DicSample = sampleArray[i] as! NSDictionary
                let val = DicSample.object(forKey: "itemcode")as! String
                newtexts[val]=String(DicSample.object(forKey: "quantity") as! Int)
            }
 
            carttableview.reloadData()
            }
            else
            {
             
            }
            
            
        } catch {
            let fetchError = error as NSError
        }

        
        
    }
    
    func Deletedata(itemcode:String)
    {
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let context:NSManagedObjectContext = appDel.managedObjectContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CartTable")
        
        let entityDescription = NSEntityDescription.entity(forEntityName: "CartTable", in:context)
        
        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        
        fetchRequest.returnsObjectsAsFaults = false
        
        //        let resultPredicate2 = NSPredicate(format: "itemcode = %@", "XIMP0004")
        fetchRequest.predicate=NSPredicate(format: "itemcode == %@", "\(itemcode)")
        
        
        do {
            let result = try context.fetch(fetchRequest)  as! [NSManagedObject]
            
            if(result.count>0)
            {
 
           for r in result
           {
            let str = r.value(forKey: "itemcode") as! String
            if (LibraryAPI.sharedInstance.Colourtexts.value(forKey: str ) as? String) != nil // [Itemcodearray[Colour] as! String]
                {
                                LibraryAPI.sharedInstance.Colourtexts.setValue("RedColor", forKey: str )
                }
                else
                {
         
                }
            
          
            
             context.delete(r)
            try!context.save()
            
            
           }
         
            }
            else
            {
              
            }
        
        } catch {
            let fetchError = error as NSError
            
        }
        
        
        
        if(sampleArray.count>0)
        {
            sampleArray.removeAllObjects()
        }
        
        
        self.fetchdbvaluesafterRemove()


    }
    
    
    func layoutFAB() {
       
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
             LibraryAPI.sharedInstance.lastviewcontroller="Dashboardviewcontroller"
            self.fab.close()
        }
        fab.fabDelegate = self
        
        self.view.addSubview(fab)
        
    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
       
    }
    
    func childViewControllerResponse(textfromcart: [String : String]) {
           }
    
    
    func childViewControllerResponse1(textfromcart: [String : String]) {
        LibraryAPI.sharedInstance.texts=newtexts
        
    }
    
    
    func sendlists(list:[Element]) {
       
        LibraryAPI.sharedInstance.globallist=listfromorder
     
    }
    
    @IBAction func ContinuebtnAction(_ sender: AnyObject)
    {
      if(sampleArray.count>0)
      {
      let  lvc=self.storyboard?.instantiateViewController(withIdentifier: "ConfirmOrderViewController") as! ConfirmOrderViewController
        lvc.listfromorder = listfromorder
        lvc.PersistentArray=sampleArray
       self.navigationController?.pushViewController(lvc, animated: true)
        }
       else
        {
            let alert = UIAlertController(title: "Sorry", message:"Add carts and come back", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler:
                { (ACTION :UIAlertAction!)in
                    
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
   
    func buttonClicked(sender:UIButton)
    {
       // LibraryAPI.sharedInstance.temptexorder=LibraryAPI.sharedInstance.texts
        
        childViewControllerResponse(textfromcart: LibraryAPI.sharedInstance.temptexorder)
         childViewControllerResponse(textfromcart: LibraryAPI.sharedInstance.PriceValue)
        sendlists(list: listfromorder)
        LibraryAPI.sharedInstance.lastviewcontroller="Cartviewcontoller"
        LibraryAPI.sharedInstance.globallist=listfromorder
        childViewControllerResponse1(textfromcart: newtexts)
        
        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "PreOrderViewController") as! PreOrderViewController
        lvc.list = LibraryAPI.sharedInstance.globallist
        lvc.texts = texts

        self.navigationController?.popViewController(animated: true)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //TableView Delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
   
        if sampleArray.count > 0
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return sampleArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell : CartTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as! CartTableViewCell
        if(cell == nil)
        {
            cell = Bundle.main.loadNibNamed("Cell", owner: self, options: nil)![0] as! CartTableViewCell;
            
        }
        cell.addDoneButtonOnKeyboardPrice()
        cell.doneButtonPriceAction()
        cell.Qntytf.delegate = self
        cell.Qntytf.tag = indexPath.row
        cell.Removebtn.tag = indexPath.row
        let dic = sampleArray[indexPath.row] as! NSDictionary
        
       // cell.Itemcode.text=listfromorder[indexPath.row].vendor
        cell.Itemcode.text=(dic.object(forKey: "itemcode") as? String)
        cell.selectionStyle = .none
        if(newtexts.keys.contains(cell.Itemcode.text!))
        {
            let str=newtexts[cell.Itemcode.text!]
            cell.Qntytf.text=str
        }
        else
        {
            cell.Qntytf.text=""
        }
        let val = (dic.object(forKey: "price") as? Double)

      
        
        
        if(String(dic.object(forKey: "quantity") as! Int) == "")
        {
            let val1 = Double("0")
            tot = tot + (Double(val!) * Double(val1!)) //Prices * Quantity
            let num=NumberFormatter()
            num.numberStyle=NumberFormatter.Style.currency
            num.locale = NSLocale(localeIdentifier: "en_IN") as Locale!
            num.maximumFractionDigits=2
            str1 = num.string(from: NSNumber(value:tot))!
            let pricestr=num.string(from: NSNumber(value: (Double(val!) * 1)))
            cell.Pricetf.text = pricestr
            
        }
        else
        {
        let val1 = Int(cell.Qntytf.text!)
       
        tot = tot + (Double(val!) * Double(val1!)) //Prices * Quantity
       
        let num=NumberFormatter()
        num.numberStyle=NumberFormatter.Style.currency
        num.locale = NSLocale(localeIdentifier: "en_IN") as Locale!
        num.maximumFractionDigits=2
        str1 = num.string(from: NSNumber(value:tot))!
      
       // let pricestr=num.stringFromNumber(NSNumber(double: (Double(val!) * Double(val1!))))
        let pricestr=num.string(from: NSNumber(value: (Double(val!) * 1)))
        
        cell.Pricetf.text = pricestr
        
        }
        cell.Sview.layer.borderWidth = 2
        cell.Sview.layer.borderColor = CustomColor.ShareColor.hexStringToUIColor(hex: CustomColor.ShareColor.CustomGreen, alpha: 1.0).cgColor
        cell.Sview.layer.cornerRadius = cell.Sview.frame.height/8
        cell.Productlabel.text = dic.object(forKey: "products") as? String
        cell.vendorid.text = dic.object(forKey: "vendor") as? String
        
        //cell.Removebtn.layer.borderWidth = 1
        //cell.Removebtn.layer.borderColor = CustomColor.ShareColor.hexStringToUIColor(CustomColor.ShareColor.CustomGreen, alpha: 1.0).CGColor
        //cell.Removebtn.layer.cornerRadius = cell.Removebtn.frame.height/2
        cell.Removebtn.addTarget(self, action: #selector(self.Removecart), for: UIControlEvents.touchUpInside)
        let stockvalue = (dic.object(forKey: "stock") as! String)
        let stockvaluefil = stockvalue.components(separatedBy: ":")
        cell.stockroom.text = stockvaluefil[1]
        //  cell.contentView.alpha = 0
        
        return cell as CartTableViewCell
    }
    
    func computetotal()
    {
        tot=0
        for i in 0..<sampleArray.count
        {
            let dic = sampleArray[i] as! NSDictionary
            //let val = ((sampleArray.object(at: i) as AnyObject).object("price") as! Double)
            let val = (dic.object(forKey: "price") as! Double)
            if(String((dic.object(forKey: "quantity") as! Int)) == "")
            {
            let val1 = Double("0")
           
            tot = tot + (Double(val) * Double(val1!)) //Prices * Quantity
            }
            else
            {
            let val1 = (dic.object(forKey: "quantity") as! Int)
           
            tot = tot + (Double(val) * Double(val1)) //Prices * Quantity
            }
        }
        let num=NumberFormatter()
        num.numberStyle=NumberFormatter.Style.currency
        num.locale = NSLocale(localeIdentifier: "en_IN") as Locale!
        num.maximumFractionDigits=2
        str1 = num.string(from: NSNumber(value:tot))!
        totallabel.text=String("TOTAL:\(str1)")
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
 
   
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
           return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
        
        textField.resignFirstResponder()
        
        
        return true
    }
    
    func Removecart(sender:UIButton) {
        
        //let buttonPosition = sender.convert(CGPointZero, to: self.carttableview)
        let buttonPosition = sender.convert(CGPoint.zero, to: self.carttableview)
        let indexPath = self.carttableview.indexPathForRow(at: buttonPosition)
       
        
        _ = carttableview.cellForRow(at: indexPath!) as! CartTableViewCell
        
       let dic = sampleArray[(indexPath?.row)!] as! NSDictionary
        let str=(dic.object(forKey: "itemcode") as! String)
        LibraryAPI.sharedInstance.temptexorder[str] = ""
        LibraryAPI.sharedInstance.PriceValue[str] = ""
        
        newtexts[str]=""
       // listfromorder.removeAtIndex((indexPath?.row)!)
        self.Deletedata(itemcode: dic.object(forKey: "itemcode") as! String)
//        sampleArray.removeObjectAtIndex((indexPath?.row)!)
    
        texts.removeAll()
        newtexts.removeAll()
       
        
        
        
        let cnt = FetchCount()
        LibraryAPI.sharedInstance.badgeCount=String(cnt)
        if(listfromorder.count>0)
        {
          
            listfromorder.removeAll()
            
            for i in 0..<sampleArray.count
            {
             let dic1 = sampleArray[i] as! NSDictionary
                 let elm = Element(Quantities:String(dic1.object(forKey: "quantity") as! Int),Productnames:dic1.object(forKey: "products") as! String,Invoicedates:dic1.object(forKey: "vendor") as! String,vendor:dic1.object(forKey: "itemcode") as! String,prices: Float(dic1.object(forKey: "price") as! Double) ,availabilty:dic1.object(forKey: "availability") as! Int,Stockroom:dic1.object(forKey: "stock") as! String)
               
               listfromorder.append(elm)

            }
            
      
        }
        else
        {
        }
                for i in 0..<sampleArray.count
                {
                   let dic2 = sampleArray[i] as! NSDictionary
                    texts[i] = dic2.object(forKey: "quantity") as? String
                     LibraryAPI.sharedInstance.temptexorder[str]=dic2.object(forKey: "quantity") as? String
                    let str=dic2.object(forKey: "itemcode") as? String
                    newtexts[str!]=String(dic2.object(forKey: "quantity") as! Int)
                   
                }
        

        if(sampleArray.count==0)
        {
           LibraryAPI.sharedInstance.temptexorder.removeAll()
           LibraryAPI.sharedInstance.PriceValue.removeAll()
            newtexts[str]="0"
            totallabel.text="TOTAL:\("0")"
        }
        
        self.computetotal()
        self.carttableview.reloadData()
      
        //  self.animate()
        
      
    
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
            guard let text = textField.text else { return true }
            
            let newLength = text.count + string.count - range.length
            return newLength <= 5 // Bool
        
        return true
    }
    
  
    func Globalalert(title:String,Message:String)
    {
        let alert = UIAlertController(title:title, message: Message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler:
            { (ACTION :UIAlertAction!)in
                
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

extension CartViewController : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
   
        // save the text in the map using the stored row in the tag field
        let dic3 = sampleArray[textField.tag] as! NSDictionary
        let available=dic3.object(forKey: "availability") as! Int
        let Quantity=Int(textField.text!)
        let Temptxt=dic3.object(forKey: "itemcode") as! String
        
        
        if(isbacktobackmode==true)
        {
            texts[textField.tag] = textField.text
            newtexts[Temptxt]=textField.text
            if(listfromorder.count>0)
            {
                
                listfromorder[textField.tag].Quantities=texts[textField.tag]!
            }
            else
            {
                
            }
            
            let dict = NSMutableDictionary()
            dict.setValue(dic3.object(forKey: "itemcode"), forKeyPath: "itemcode")
            dict.setValue(dic3.object(forKey: "price"), forKeyPath: "price")
            dict.setValue(dic3.object(forKey: "products"), forKeyPath: "products")
            dict.setValue(Quantity, forKeyPath: "quantity")
            dict.setValue(dic3.object(forKey: "vendor"), forKeyPath: "vendor")
            dict.setValue(dic3.object(forKey: "availability"), forKeyPath: "availability")
            dict.setValue(dic3.object(forKey: "stock"), forKeyPath: "stock")
                        
            
            //   let str=sampleArray.objectAtIndex(textField.tag).objectForKey("products") as? String
            LibraryAPI.sharedInstance.temptexorder = newtexts
            
            let code=dic3.object(forKey: "itemcode") as? String
            
           
            for i in 0..<sampleArray.count
            {
                if(dic3.object(forKey: "itemcode") as? String == code)
                {
                    sampleArray.removeObject(at: i)
                    sampleArray.add(dict)
                    
                }
                else
                {
                    
                }
            }
 
            self.UpdateDbfromArray(Quantity: Quantity!)
        }
        else
        {
        
        if(Quantity!<available)
        {
            texts[textField.tag] = textField.text
            newtexts[Temptxt]=textField.text
            if(listfromorder.count>0)
            {
            
            listfromorder[textField.tag].Quantities=texts[textField.tag]!
            }
            else
            {
                
            }
            
            let dict = NSMutableDictionary()
            let dic4 = sampleArray[textField.tag] as! NSDictionary
            dict.setValue(dic4.object(forKey: "itemcode"), forKeyPath: "itemcode")
            dict.setValue(dic4.object(forKey: "price"), forKeyPath: "price")
            dict.setValue(dic4.object(forKey: "products"), forKeyPath: "products")
            dict.setValue(Quantity, forKeyPath: "quantity")
            dict.setValue(dic4.object(forKey: "vendor"), forKeyPath: "vendor")
            dict.setValue(dic4.object(forKey: "availability"), forKeyPath: "availability")
            dict.setValue(dic4.object(forKey: "stock"), forKeyPath: "stock")
            
            
         //   let str=sampleArray.objectAtIndex(textField.tag).objectForKey("products") as? String
            LibraryAPI.sharedInstance.temptexorder = newtexts
            
            let code=dic4.object(forKey: "itemcode") as? String
            
            for i in 0..<sampleArray.count
            {
                let SampleDic = sampleArray[i] as! NSDictionary
                if(SampleDic.object(forKey: "itemcode") as? String == code)
                {
                    sampleArray.removeObject(at: i)
                    sampleArray.add(dict)
                    
                }
                else
                {
                    
                }
            }
        
            self.UpdateDbfromArray(Quantity: Quantity!)
            
          

        }
        else
        {
            let dic5 = sampleArray[textField.tag] as! NSDictionary
            textField.text=dic5.object(forKey : "quantity") as? String
             texts[textField.tag] = textField.text
            self.Globalalert(title: "oops", Message: "\(dic5.object(forKey : "products") as? String) is available for \(available) PCE only")
        }
    }

        self.computetotal()
        carttableview.reloadData()
    }
}


extension String {
    func convertcurrency(str:String)->String
    {
        //yet to be deifned
        return str
    }
}

