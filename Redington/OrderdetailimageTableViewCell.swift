//
//  OrderdetailimageTableViewCell.swift
//  Redington
//
//  Created by truetech on 27/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit

class OrderdetailimageTableViewCell: UITableViewCell {

    @IBOutlet var Lineno: UILabel!
    @IBOutlet var Orderno: UILabel!
    @IBOutlet var Pono: UILabel!
    @IBOutlet var Quantity: UILabel!
    @IBOutlet var Orderdate: UILabel!
    @IBOutlet var Unitprice: UILabel!
    @IBOutlet var Orderstatus: UILabel!
    @IBOutlet var Itemcode: UILabel!
    @IBOutlet var Productname: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
