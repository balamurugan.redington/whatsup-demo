//
//  OverDueTableViewCell.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 08/12/17.
//  Copyright © 2017 truetech. All rights reserved.
//
import UIKit
import Charts
//import Funnel

class OverDueTableViewCell: UITableViewCell {

    @IBOutlet weak var Barchartviews:  BarChartView!
    var months=[String]()
    
    @IBOutlet weak var subview: UIView!
    @IBOutlet weak var btnclicked: UIButton!
    
    @IBOutlet weak var nodatalbl: UILabel!
   
    @IBOutlet weak var gradientview: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        gradientview.layer.cornerRadius = 3.0
        gradientview.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    func setbarcharts(dataPoints: [String], values: [Double],isanimated:Bool) {
        
        Barchartviews.noDataText = "You need to provide data for the chart."
        
        var dataEntries: [BarChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
           // let dataEntry = BarChartDataEntry(value: values[i], xIndex: i)
            let dataEntry = BarChartDataEntry(x: Double(i), yValues: [values[i]])
            dataEntries.append(dataEntry)
            months.append("")
        }
        Barchartviews.legend.enabled=false
        
        if(isanimated==false)
        {
            
        }
        else
        {
           // Barchartviews.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: CharteaseOutQuadn.EaseOutQuad)
            Barchartviews.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: ChartEasingOption.easeOutQuad)
        }
        
        
       // let chartDataSet = BarvaluesDataSet(yVals: dataEntries, label: "Units Sold")
        let chartDataSet = BarChartDataSet(values: dataEntries, label: "Units Sold")
        
        
        
        chartDataSet.colors = ChartColorTemplates.colorful()
       // let chartData = BarChartData(xVals:months, dataSet:chartDataSet)
         let chartData = BarChartData(dataSet: chartDataSet)
        Barchartviews.data = chartData
        
        
        
    }
    
    func setChartBarGroupDataSetfordashboard(dataPoints: [String], values: [Double], values2: [Double],values3:[Double],values4:[Double],values5:[Double],sortIndex:Int) {
        var dataEntries: [BarChartDataEntry] = []
        var dataEntries2: [BarChartDataEntry] = []
        var dataEntries3: [BarChartDataEntry] = []
        var dataEntries4: [BarChartDataEntry] = []
        var dataEntries5: [BarChartDataEntry] = []
        var de: [BarChartDataEntry] = []
       
       for i in 0..<dataPoints.count {
            
           // let dataEntry = BarChartDataEntry(value: values[i], xIndex: i)
            let dataEntry = BarChartDataEntry(x: Double(0), yValues: [values[i]])
            dataEntries.append(dataEntry)
        }
        
        
        for i in 0..<dataPoints.count {
            
           
            let dataEntry = BarChartDataEntry(x: Double(1), yValues: [values2[i]])
            dataEntries2.append(dataEntry)
        }
        
        for i in 0..<dataPoints.count {
            
          
            let dataEntry = BarChartDataEntry(x: Double(2), yValues: [values3[i]])
            dataEntries3.append(dataEntry)
        }
        
        
        for i in 0..<dataPoints.count {
            
           // let dataEntry = BarChartDataEntry(value: values4[i], xIndex: i)
             let dataEntry = BarChartDataEntry(x: Double(3), yValues: [values4[i]])
            dataEntries4.append(dataEntry)
        }
        
        for i in 0..<dataPoints.count {
            
           // let dataEntry = BarChartDataEntry(value: values5[i], xIndex: i)
            let dataEntry = BarChartDataEntry(x: Double(4), yValues: [values5[i]])
            dataEntries5.append(dataEntry)
        }
        
        
       
        
        
        for i in 0..<dataPoints.count
        {
         //   let dr=BarChartDataEntry(values:[values[i],values2[i],values3[i],values4[i],values5[i]],xIndex:i)
             let dr = BarChartDataEntry(x: Double(i), yValues: [values[i],values2[i],values3[i],values4[i],values5[i]])
            de.append(dr)
            
        }
        
        var colors: [UIColor] = []
        var colors1: [UIColor] = []
        var colors2: [UIColor] = []
        var colors3: [UIColor] = []
        var colors4: [UIColor] = []

        
        //let chartDataSet1 = BavaluestDataSet(yVals: de, label: "")
         let chartDataSet1 = BarChartDataSet(values: de, label: "")
        chartDataSet1.stackLabels=["0-30","30-60","60-90","90-120",">120"]
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: " ")
        let chartDataSet2 = BarChartDataSet(values: dataEntries2, label: " ")
        let chartDataSet3 = BarChartDataSet(values: dataEntries3, label: " ")
        let chartDataSet4 = BarChartDataSet(values: dataEntries4, label: " ")
        let chartDataSet5 = BarChartDataSet(values: dataEntries5, label: " ")
        
        for _ in 0..<dataPoints.count {
            
            
            colors.append(UIColor.init(red: 181/255.0, green: 255/255.0, blue: 122/255.0, alpha: 1.0))
        }
        
        for _ in 0..<dataPoints.count {
            
            
            colors1.append(UIColor.init(red: 247/255.0, green: 240/255.0, blue: 122/255.0, alpha: 1.0))
        }
        for _ in 0..<dataPoints.count {
            
            colors2.append(UIColor.init(red: 254/255.0, green: 198/255.0, blue: 122/255.0, alpha: 1.0))
        }
        
        for _ in 0..<dataPoints.count {
            
            colors3.append(UIColor.init(red: 120/255.0, green: 230/255.0, blue: 255/255.0, alpha: 1.0))
        }
        for _ in 0..<dataPoints.count {
            
            colors4.append(UIColor.init(red: 252/255.0, green: 111/255.0, blue: 135/255.0, alpha: 1.0))
        }
        
        Barchartviews.descriptionTextColor = UIColor.white
        chartDataSet.colors = colors
        chartDataSet2.colors = colors1
        chartDataSet3.colors = colors2
        chartDataSet4.colors = colors3
        chartDataSet5.colors = colors4
        let dataSets:[BarChartDataSet]=[chartDataSet,chartDataSet2,chartDataSet3,chartDataSet4,chartDataSet5]
       // let data = BarChartData(xVals: dataPoints, dataSets: dataSets)
        let data = BarChartData(dataSets: dataSets)
        Barchartviews.data = data
        Barchartviews.data?.setValueTextColor(UIColor.white)
        Barchartviews.xAxis.labelPosition = .bottom
        Barchartviews.xAxis.labelWidth = 30
      //  Barchartviews.xAxis.getLongestLabel()
        Barchartviews.rightAxis.enabled=false
        Barchartviews.xAxis.drawAxisLineEnabled = true
        Barchartviews.descriptionText = ""
        Barchartviews.rightAxis.drawGridLinesEnabled = true
        
        Barchartviews.data = data
        
        Barchartviews.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .easeInBounce)
        
              
        
        
    }
    
    

    
}
