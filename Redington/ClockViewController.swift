//
//  ClockViewController.swift
//  Clock
//
//  Created by TRUETECHMAC3 on 28/09/16.
//  Copyright © 2016 com.truetech.alshaqb. All rights reserved.
//

import UIKit
import CoreLocation
//import NVActivityIndicatorView
import KCFloatingActionButton
import Alamofire


class ClockViewController: UIViewController,CLLocationManagerDelegate,KCFloatingActionButtonDelegate{
    let manager=CLLocationManager()
    @IBOutlet var Menubtn: UIButton!
    @IBOutlet var backbtn: UIButton!
    @IBOutlet weak var Date: UILabel!
    @IBOutlet weak var Settime: UILabel!
    @IBOutlet weak var Time: UILabel!
    @IBOutlet weak var viewtime: UIView!
    @IBOutlet weak var clockiamge: UIImageView!
    @IBOutlet weak var clock: UIButton!
    var Uploadjson=NSMutableDictionary()
    var jsonresult=NSDictionary()
    var ti=String()
    var count=Int()
    var  datatime = String()
    var  datadate = String()
    
    var GetTimeurl = String()
    var Date1 = String()
    var time1 = String()
    var fab=KCFloatingActionButton()
    
    
    @IBOutlet var locationtf: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        count=0
        
        self.setlocation()
        GetTime()
        self.layoutFAB()
        customnavigation()
//        Menubtn.setImage(UIImage(named:"menu.png"), forState: UIControlState.Normal)
//        backbtn.setImage(UIImage(named:"BACK.png"), forState: UIControlState.Normal)
//        backbtn.addTarget(self, action: #selector(ClockViewController.backbuttonClicked(_:)),forControlEvents:UIControlEvents.TouchUpInside)
//        Menubtn.addTarget(self, action: #selector(self.presentRightMenuViewController),forControlEvents:UIControlEvents.TouchUpInside)
        
        clock.layer.cornerRadius = 20.0
       
      
        let date = NSDate()
        let calender1 = NSCalendar.current
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "YYYY-MM-dd"
        let result = formatter1.string(from: date as Date)

      
        self.Date.text = String(result)
        
        let date1 = NSDate()
        let calender = NSCalendar.current
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm"
        let result1 = formatter.string(from: date1 as Date)
        
         self.Time.text = result1
        
//        serverTimeReturn { (getResDate) -> Void in
//
//            let dFormatter = NSDateFormatter()
//            let dFormatter1 = NSDateFormatter()
//            dFormatter.dateFormat="HH:mm"
//            let dateGet = dFormatter.stringFromDate(getResDate)
//            
//            
//            let uploaddf = NSDateFormatter()
//            uploaddf.dateFormat="HH:mm"
//            self.datatime = uploaddf.stringFromDate(getResDate)
//            
//            
//            let uploaddfdate = NSDateFormatter()
//            uploaddfdate.dateFormat="YYYY-MM-dd"
//            self.datadate = uploaddfdate.stringFromDate(getResDate)
//            
//            
//            
//            
//            
//            dFormatter1.dateFormat = "dd/MM/yyyy"
//            let dateGet1 = dFormatter1.stringFromDate(getResDate)
//            
//            dispatch_async(dispatch_get_main_queue(),{
//                
//                self.Time.text = dateGet
//                self.Date.text = dateGet1
//                
//                
//                
//            })
//            
//            
//            
//            let datStr :  String = self.Date.text!
//            
//            let uploaddfdate1 = NSDateFormatter()
//            uploaddfdate1.dateFormat="dd/MM/yyyy"
//            let datadate1 = uploaddfdate1.dateFromString(datStr)
//            
//
//            
//        }
        
        
    }
    
    func customnavigation()
        
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect( x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(ClockViewController.backbuttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Attendence"
    }
    
    
    
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            
            self.fab.close()
        }
        fab.fabDelegate = self
        
        self.view.addSubview(fab)
        
    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
        
    }
    
    
    
    
    func setlocation() {
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
       // let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
       // appdelegate.shouldSupportAllOrientation = false
        
    
        
        if let strtime = UserDefaults.standard.value(forKey: "ClockPressedDate") as? String
        {
            
            
            let uploaddfdate = DateFormatter()
            uploaddfdate.dateFormat="YYYY-MM-dd"
            let datadate = uploaddfdate.date(from: strtime)
            if compareDate(date1: NSDate(), date2: datadate! as NSDate) {
                // The two dates are on the same day.
                
                
                
            }
            else
            {
                
                
                if let clckDefValue = UserDefaults.standard.value(forKey: "ClockPressed") as? Int
                {
                    if(clckDefValue == 2)
                    {
                        UserDefaults.standard.set(1, forKey: "ClockPressed")
                    }
                    else if(clckDefValue == 3)
                    {
                        UserDefaults.standard.set(1, forKey: "ClockPressed")
                    }
                }
                
                
            }
        }
        else
        {
            
            
            UserDefaults.standard.set(1, forKey: "ClockPressed")
        }
        
        if let clckValue = UserDefaults.standard.value(forKey: "ClockPressed") as? Int
        {
            if(clckValue == 1)
            {
                //CLOCK IN
                
                clock.tag = clckValue
                self.clock.setTitle("Clock In", for: UIControlState.normal)
                self.clock.backgroundColor=(UIColor.init(red: 30/255.0, green: 184/255.0, blue: 120/255.0, alpha: 1.0))
                
                clockiamge.isHidden=false
                viewtime.isHidden=true
            }
            else if(clckValue == 2)
            {
                //CLOCK OUT
                clock.tag = clckValue
                self.clock.setTitle("Clock Out", for: UIControlState.normal)
                self.clock.backgroundColor=(UIColor.init(red: 245/255.0, green: 60/255.0, blue: 73/255.0, alpha: 1.0))
                
                
                clockiamge.isHidden=true
                viewtime.isHidden=false
                
                let clockedTime = UserDefaults.standard.value(forKey: "ClockPressedTime") as? String
                
                Settime.text=clockedTime
                
                
            }
            else if(clckValue == 3)
            {
                // CLOCK IN - BUT ALERT SHOWN THAT NOT TO ACCESS
                clock.tag = clckValue
                
                self.clock.setTitle("Clock In", for: UIControlState.normal)
                self.clock.backgroundColor=(UIColor.init(red: 30/255.0, green: 184/255.0, blue: 120/255.0, alpha: 1.0))
                
                UserDefaults.standard.set(3, forKey: "ClockPressed")
                clock.tag = 3
                
                clockiamge.isHidden=false
                viewtime.isHidden=true
                
                //ALERT SHOWN
            }
            
            
        }
        else
        {
            
            //CLOCK IN
            clock.tag = 1
            self.clock.setTitle("Clock In", for: UIControlState.normal)
            self.clock.backgroundColor=(UIColor.init(red: 30/255.0, green: 184/255.0, blue: 120/255.0, alpha: 1.0))
            
            
            clockiamge.isHidden=false
            viewtime.isHidden=true
        }
        
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
    }
    
    func displayLocationInfo(placemark: CLPlacemark) {
        
        //stop updating location to save battery life
        manager.stopUpdatingLocation()
        locationtf.text=placemark.locality
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler:
            {(placemarks, error) in
                if (error != nil) {
                    return
                }
                
                if placemarks!.count > 0 {
                    let pm = placemarks![0] as CLPlacemark
                    self.displayLocationInfo(placemark: pm)
                    
                    
                    
                } else {
                }
        })
        
        let location = locations.last! as CLLocation
        
        let loclat = Double(round(1000*location.coordinate.latitude)/1000)
        
        let loclon = Double(round(1000*location.coordinate.longitude)/1000)
        
        self.Uploadjson.setObject(loclat, forKey: "latitude" as NSCopying)
        self.Uploadjson.setObject(loclon, forKey: "longitude" as NSCopying)
        
    }
    
    
    
    
    func backbuttonClicked(sender:UIButton)
    {
        
        LibraryAPI.sharedInstance.tracker="1"
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    
    
//    func serverTimeReturn(completionHandler:(getResDate: NSDate!) -> Void){
//        
//        let url = NSURL(string: "http://www.google.com")
//        let task = NSURLSession.sharedSession().dataTaskWithURL(url!) {(data, response, error) in
//            let httpResponse = response as? NSHTTPURLResponse
//            if let contentType = httpResponse!.allHeaderFields["Date"] as? String {
//                
//                let dFormatter = NSDateFormatter()
//                dFormatter.dateFormat = "EEE, dd MMM yyyy HH:mm:ss z"
//                let serverTime = dFormatter.dateFromString(contentType)
//                completionHandler(getResDate: serverTime)
//            }
//        }
//        
//        task.resume()
//    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func GetTime()
    {
        let urllink = "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/GetDateTime"
        let  deviceToken = urllink
        print("url :", urllink)
        var url = deviceToken
        //url=url.stringByReplacingOccurrencesOfString("(", withString:"")
        // url=url.stringByReplacingOccurrencesOfString(")", withString:"")
        //let urlStr : NSString = url.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        let urlreq = NSURL(string:url as String);
        let requestURL = NSMutableURLRequest(url:urlreq! as URL);
        requestURL.httpMethod="GET";
        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
        do
        {
            let data = try NSURLConnection.sendSynchronousRequest(requestURL as URLRequest, returning: response)
            let returnString = String(data: data, encoding: String.Encoding.utf8)!
            
            print("time:",returnString)
            self.GetTimeurl = returnString
            print("get time: ",self.GetTimeurl)
            if(self.GetTimeurl.count > 0)
            {
                var myStringArr = self.GetTimeurl.components(separatedBy: "T")
                self.Date1 = myStringArr [0]
                let dateAsString = myStringArr[1]
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "HH:mm:ss"
                let date = dateFormatter.date(from: dateAsString)
                dateFormatter.dateFormat = "h:mm"
                let Date12 = dateFormatter.string(from: date!)
                self.time1 = Date12
                print("time :",self.time1)
            }
            else
            {
                self.showToast(message: "Delay in Server Response")
            }
        }
        catch (let e)
        {
            print(e)
            
        }
    }
    
    @IBAction func Clock(_ sender: AnyObject) {
        GetTime()
        print("sender tag:", sender.tag)
        if(sender.tag == 1)
        {
            self.clock.setTitle("Clock Out", for: UIControlState.normal)
            self.clock.backgroundColor=(UIColor.init(red: 245/255.0, green: 60/255.0, blue: 73/255.0, alpha: 1.0))
            UserDefaults.standard.set(2, forKey: "ClockPressed")
            let datStr :  String = Date.text!
            let uploaddfdate = DateFormatter()
            uploaddfdate.dateFormat="dd/MM/yyyy"
            _ = uploaddfdate.date(from: datStr)
            // new version dispatch Que
            DispatchQueue.main.async {
                let date = NSDate()
                _ = NSCalendar.current
                let formatter = DateFormatter()
                formatter.dateFormat = "YYYY-MM-dd"
                let result = formatter.string(from: date as Date)
                UserDefaults.standard.set(result, forKey: "ClockPressedDate")
            }
            // new version dispatch Que
            let formatteddate=Date1.appending(" \(self.time1)")
            self.Uploadjson.setObject(LibraryAPI.sharedInstance.Userid, forKey: "Userid" as NSCopying)
            self.Uploadjson.setObject(formatteddate, forKey: "DateTimeUpdate" as NSCopying)
            self.Uploadjson.setObject("I", forKey: "Flag" as NSCopying)
            print("upload format:",self.Uploadjson)
            let size = CGSize(width: 30, height:30)
            self.local_activityIndicator_start()
            // new version dispatch Que
            DispatchQueue.global(qos: .background).async {
                // Validate user input
                self.UpdateAttendance()
                // Go back to the main thread to update the UI
                DispatchQueue.main.async {
                    self.local_activityIndicator_stop()
                    if(self.jsonresult.object(forKey: "Status") as? String  == "Success")
                    {
                        let alertController = UIAlertController(title: "Alert", message: "Logged in! Have a Pleasant Day  \(LibraryAPI.sharedInstance.Userid)!" , preferredStyle: .alert)
                        
                        let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                            
                            
                            
                        }
                        alertController.addAction(cancelAction)
                        
                        
                        self.present(alertController, animated: true, completion:nil)
                    }
                    else
                        
                    {
                        let alertController = UIAlertController(title: "Alert", message:"Attendance Updation Failed" as? String , preferredStyle: .alert)
                        
                        let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                            
                        }
                        alertController.addAction(cancelAction)
                        
                        
                        self.present(alertController, animated: true, completion:nil)
                    }
                    
                }
            }
            
            
            dismiss(animated: true, completion: nil)
            //            LibraryAPI.sharedInstance.lastlogouttime.setObject(self.datadate, forKey: "Date")
            
            UserDefaults.standard.set(Time.text, forKey: "ClockPressedTime")
            
            clock.tag = 2
            
            clockiamge.isHidden=true
            viewtime.isHidden=false
            
            Settime.text=Time.text
            
            
        }
        else if(sender.tag == 2)
        {
            self.clock.setTitle("Clock In", for: UIControlState.normal)
            self.clock.backgroundColor=(UIColor.init(red: 30/255.0, green: 184/255.0, blue: 120/255.0, alpha: 1.0))
            
            UserDefaults.standard.set(1, forKey: "ClockPressed")
            clock.tag = 1
            let formatteddate=Date1.appending(" \(self.time1)")
            self.Uploadjson.setObject(LibraryAPI.sharedInstance.Userid, forKey: "Userid" as NSCopying)
            self.Uploadjson.setObject(formatteddate, forKey: "DateTimeUpdate" as NSCopying)
            self.Uploadjson.setObject("O", forKey: "Flag" as NSCopying)
            print("upload format:",self.Uploadjson)
            let size = CGSize(width: 30, height:30)
            self.local_activityIndicator_start()
            // new version dispatch Que
            
            DispatchQueue.global(qos: .background).async {
                
                // Validate user input
                
                self.UpdateAttendance()
                // Go back to the main thread to update the UI
                DispatchQueue.main.async {
                    
                    self.local_activityIndicator_stop()
                    if(self.jsonresult.object(forKey: "Status") as? String  == "Success")
                    {
                        
                        let alertController = UIAlertController(title: "Alert", message: "Logged out! Have a Pleasant Day  \(LibraryAPI.sharedInstance.Userid)!" , preferredStyle: .alert)
                        
                        let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                            
                            
                            
                        }
                        alertController.addAction(cancelAction)
                        
                        
                        self.present(alertController, animated: true, completion:nil)
                    }
                    else
                        
                    {
                        let alertController = UIAlertController(title: "Alert", message:"Attendance Updation Failed" as? String , preferredStyle: .alert)
                        let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(cancelAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            }
            // new version dispatch Que
            clockiamge.isHidden=false
            viewtime.isHidden=true
        }
        else
        {
           /* self.clock.setTitle("Clock In", for: UIControlState.normal)
            self.clock.backgroundColor=(UIColor.init(red: 20/255.0, green: 104/255.0, blue: 22/255.0, alpha: 1.0))
            UserDefaults.standard.set(3, forKey: "ClockPressed")
            clock.tag = 3
            clockiamge.isHidden=false
            viewtime.isHidden=true
            //ALERT SHOWN
            let alertController = UIAlertController(title: "Alert", message: "Only One time can Clock In per day  \(LibraryAPI.sharedInstance.Userid)!" , preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
            }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)*/
        }
        
    }
    
    func UpdateAttendance()
    {
        let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)Users.asmx/AttendanceUpdation")! as URL)
        request.httpMethod = "POST"
        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
        print("upload data:", self.Uploadjson)
        request.httpBody = try? JSONSerialization.data(withJSONObject: self.Uploadjson, options: JSONSerialization.WritingOptions(rawValue: 0))
        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
        //  var err: NSError
        do
        {
            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
            jsonresult = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            print("result in clock : ", jsonresult)
        }
        catch (let e)
        {
            print(e)
        }
    }
    
    
    func compareDate(date1:NSDate, date2:NSDate) -> Bool {
        //let order = NSCalendar.currentCalendar.compareDate(date1, toDate: date2,toUnitGranularity;: .Day)
        let order = NSCalendar.current.compare(date1 as Date, to: date2 as Date, toGranularity: .day)
        switch order {
        case .orderedSame:
            return true
        default:
            return false
        }
    }
    
}
