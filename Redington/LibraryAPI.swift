 /*
  * Copyright (c) 2014 Razeware LLC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */
 
 import CoreLocation
 import UIKit
 import SystemConfiguration
 import ReachabilitySwift
 import Speech
 import Foundation
 import Alamofire
 import KCFloatingActionButton
 
 
 class LibraryAPI: NSObject, CLLocationManagerDelegate, UIAlertViewDelegate, UIApplicationDelegate, KCFloatingActionButtonDelegate, NVActivityIndicatorViewable {
    
    var globallist = [Element]()
    var breadthvalues = NSMutableArray()
    var NotificationDashboardCheck: Int!
    var deviceToken = String()
    var returnarray = NSMutableArray()
    var returnDic = NSMutableDictionary()
    var returnString = String()
    var salesarray = NSMutableArray()
    var deliveryreturnarray = NSMutableArray()
    var LoginCredentials = UserDefaults.standard
    var emailcredential = UserDefaults.standard
    var Passcodeceredentials = UserDefaults.standard
    var Rememberme = String()
    private var reachability: Reachability!
    var temparray = NSMutableArray()
    var CurrentYear = NSNumber()
    var CurrentMonth = NSNumber()
    var Userid = String()
    var TicketNumber = String()
    var BranchCodeNew = String()
    var branchname = String()
    var SkipVerified = String()
    var placesid = NSMutableArray()
    var manager = CLLocationManager()
    var FBApiURL = String();
    var Globlatimer = Timer()
    var connectionState = "Connected"
    let connectedState = "Connected"
    let notConnectedState = "notConnected"
    let createAccountErrorAlert: UIAlertView = UIAlertView()
    var GlobalUrl = String()
    var GoogleUrl = String()
    var lastviewcontroller = String()
    var preorderarray = NSMutableArray()
    var preorderarray_Storage = NSMutableArray()
    var LoginResponse = String()
    var userpermission = String()
    var imgdata = NSData()
    var lastlogouttime = UserDefaults()
    var Colourtexts = NSMutableDictionary()
    var Loginclikced = UserDefaults()
    var logoutclicked = UserDefaults()
    var lastMenuviewcontroller = String()
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    var Textfieldarray = NSMutableArray()
    var  jsonresult = NSMutableArray()
    var SalesAcheived = NSNumber()
    var SalesTarget = NSNumber()
    var pievalues = [Double]()
    var Centertext = String()
    var texts = [String: String]()
    var PriceValue = [String: String]()
    var temptexorder = [String: String]()
    var temptexttfcart = [Int: String]()
    var textarray = [Int]()
    var previousmontn = NSNumber()
    var fab = KCFloatingActionButton()
    
    var Currentcustomerlookup = String()
    var Currentcustomerbranch = String()
    var Currentitemcode: String!
    
    var response_ParseBranchesData = NSMutableArray()
    var response_PieChartData = NSMutableArray()
    var response_BarChartData = NSMutableArray()
    var response_BreadthData = NSMutableArray()
    var response_LocationBtnData = NSMutableArray()
    var response_AttendenceData = NSMutableArray()
    var response_NotificationData = NSMutableArray()
    
    var monthsarray = NSMutableArray()
    var CustomerCode = String()
    var CustomerCodeFrom = String()
    var CustomerCodeTo = String()
    var getphonenumberpopupCheck = String()
    var ItemCode = String()
    var Listarray = NSMutableArray()
    var tracker = String()
    var tracker1 = Int()
    var Username = String()
    var OrdersubmissonData = NSMutableDictionary()
    var orderdetails = NSMutableArray()
    var orderno = String()
    var sostatus = String()
    var Organisationarray = NSMutableArray()
    var Orderarray = NSMutableArray()
    var OrderReturnarray = NSMutableArray()
    var CityResponsearray = NSMutableArray()
    var Cityearray = NSMutableArray()
    var customercodearray = NSMutableArray()
    var customersearchbranchcode = String()
    
    var getCity: String!
    var getState: String!
    var getmode: String!
    var itemcodesoc: String!
    var getcustomercode: String!
    var customercode: String!
    
    var Lookuparray = NSMutableArray()
    var LookupGlobal = NSMutableArray()
    var Breadtharr = NSMutableArray()
    var OverDueArray = NSMutableArray()
    var Dashboardarr = NSMutableArray()
    var logintracker = String()
    var logincountval = Int()
    var Keyinfoarray = NSMutableArray()
    var VisitKeyinfoarray = NSMutableArray()
    var valuearray = NSMutableArray()
    var customerarray = NSMutableArray()
    var Orderhistoryarray = NSMutableArray()
    var Productspreadarray = NSMutableArray()
    var Invoicearray = NSMutableArray()
    var commitedamount = Int()
    var lastcommitedamount = String()
    var numberoftimesClockinclicked = Int()
    var numberoftimesClockoutclicked = Int()
    var poptextfieldclicked = Bool()
    var firsttimecheckdashboard = Bool()
    var isloggedout = Bool()
    var ismapopened = Bool()
    var badgeCount = String()
    var preorderstockarray = NSMutableArray()
    var Stockroom = String()
    var StockAllLocationArry = NSMutableArray()
    var CheckBendingDetailArray = NSMutableArray()
    var CheckBendingCustomerArray = NSMutableArray()
    var leaveappno = NSMutableArray()
    
    var PincodedataArry = NSMutableArray()
    var isvalueloaded = Bool()
    var Stockroomstring = String()
    var Pincode = String()
    var arr = NSMutableArray()
    var ticketstatus = String()
    var Ticketnumber = String()
    var msg = String()
    var ref = String()
    var floor = String()
    var seat = String()
    var pri = String()
    var bookarray = NSMutableArray()
    var NotificationArray = NSMutableArray()
    var SPCStatus = NSMutableArray()
    var feedbackstars = String()
    var appnumber = String()
    var errormsg  = String()
    var qtrvalue = Int()
    var ItemCheck = Int()
    var year = Int()
    var qtryear = NSNumber()
    var qtrtype = String()
    var CustomSearchCheck: Bool = false
    var Popupgetstring: String!
    var popupcustomerstring: String!
    var popupvendor: String!
    var SelectedValue: String!
    var CheckBending_CustomerCode: String!
    var BizCode: String!
    var SPClblStr: String!
    var SPC_045: String!
    var SPC_045_str: String!
    var spcstr: String!
    var SPCtitleStr: String!
    var PoptoPop: String!
    var billingmsg: String!
    var spcnumber = String()
    var leaveappnumber: NSDictionary!
    var responsestringleavedetails = NSMutableArray()
    var visitlogticketnumber = String()
    var BODRefnumber = String()
    var addtocartglobal: NSMutableArray!
    var spccustname = String()
    var priceApptype : String!
    var view360secondpopupstr = String()
    var view360secondpopupname = String()
    var view360btnaction: String!
    var ciscopopup = NSMutableArray()
    var select_ciscopopup = NSMutableDictionary()
    var branchcode = String()
    var CustomerBranchcode = String()
    var CustomerBranchcoderesponse = NSMutableArray()
    var Custbranchisbtnclicked = Bool()
    var BranchCodeCheck = Int()
    var BranchCodeBtnCheck = Int()
    var keyinfochepending = Int()
    var payslipmonth = String()
    var payslipyear = String()
    var Monthyearpayslip = Int()
    var PartnerBranchCodetf = String()
    //temporarystorage
    //spcentrypage
    var globaldic = NSMutableDictionary()
    var partnerglobaldic = NSMutableDictionary()
    var globalarray = NSMutableArray()
    var controller = UIViewController()
    var itemcodeglobal: String!
    var Stocktransferbcode: String!
    var Stocktransferitemcode: String!
    var Stocktransferdatas = NSMutableArray()
    var Stocktransfepopuporderno: NSDictionary!
    var Leaveapprovalhrd: NSDictionary!
    var permissioncheck = String()
    var Globalflag = String()
    var CurrentMonth2 = String()
    var LiveUserName = String()
    var item: [String] = []
    var item1: [String] = []
    var item2: [String] = []
    var filteritem: [String] = []
    var searchActive: Bool = false
    var internetchecking = String()
    var PosVale1 = String()
    var PosVale2 = String()
    var PosVale3 = String()
    var PosVale4 = String()
    var PosVale5 = String()
    var FDP1 = String()
    var FDP2 = String()
    var FDP3 = String()
    var FDP4 = String()
    var FDP5 = String()
    var Global_PosValue = String()
    var Global_PosValue1 = String()
    var Poc_demounitlbl = String()
    var Poc_partnerlocationlbl = String()
    var Bio_Metric = String()
    var pdf_data = String()
    var destinationUrl = String()
    var networkReachable = 0
    var networkmode     = ""
    var WIFI            = "WiFi"
    var cell            = "Cell"
    var LiveUserCode    = String()
    var perorderno = ""
    var push = 0
    var dashIn = 0
    typealias jsonMutablearray = (NSMutableArray) -> ()
    typealias jsonArray = (NSArray) -> ()
    typealias jsonDictionary = (NSDictionary) -> ()
    typealias jsonMutableDictionary = (NSMutableDictionary) -> ()
    typealias jsonString = (String) -> ()
    class var sharedInstance: LibraryAPI {
        struct Singleton {
            static let instance = LibraryAPI()
        }
        return Singleton.instance
    }
    
    override init() {
        SalesAcheived=0
        pievalues=[0.0]
        SalesTarget=0

        //GlobalUrl="http://edi.redingtonb2b.in/whatsup-staging/"
        GlobalUrl="http://edi.redingtonb2b.in/Whatsup-v5/"

        GoogleUrl = "http://maps.googleapis.com/maps/api/"
        SkipVerified="0";
        Rememberme = "Disabled"
        lastMenuviewcontroller=""
        logintracker=""
        isloggedout=false
        temparray.insert("JAN 16", at: 0)
        temparray.insert("FEB 16", at: 1)
        temparray.insert("MAR 16", at: 2)
        temparray.insert("APR 16", at: 3)
        temparray.insert("MAY 16", at: 4)
        temparray.insert("JUN 16", at: 5)
        temparray.insert("JUL 16", at: 4)
        temparray.insert("AUG 16", at: 5)
        Userid = ""
        super.init()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func fetchdate() {
        let date = Date()
        let calendar = NSCalendar.current
        let components = calendar.dateComponents([.day, .month, .year], from: date)
        let year =  components.year
        let month: Int = components.month!
        let previousMonth = NSCalendar.current.date(byAdding: .month, value: -1, to: Date())
        let fullNameArr5: [String] = String(describing: previousMonth).components(separatedBy: "-")
        CurrentMonth2 = fullNameArr5[1]
        CurrentYear = year! as NSNumber
        CurrentMonth = month as NSNumber
        previousmontn = NSNumber(value: month - 1)
    }
    
    func internetcheck() {
        if NetworkReachabilityManager()!.isReachable {
            if NetworkReachabilityManager()!.isReachableOnEthernetOrWiFi{
                networkmode = "WiFi"
                connectionState = connectedState
            } else{
                networkmode = "Cell"
                connectionState = connectedState
            }
        } else{
            print("Connection lost")
            self.connectionState = self.notConnectedState
            alert()
            print("connection state :- ",self.connectionState)
        }
    }
    
    func alert() {
        internetchecking = "A"
        createAccountErrorAlert.delegate = self
        createAccountErrorAlert.title = "Oops"
        createAccountErrorAlert.message = "You are not connected to Internet!Activate Wifi or Mobile data to Access"
        createAccountErrorAlert.addButton(withTitle: "ok")
        createAccountErrorAlert.show()
    }
    
    func alertView(_ View: UIAlertView, clickedButtonAt buttonIndex: Int) {
        switch buttonIndex {
        case 0:
           createAccountErrorAlert.dismiss(withClickedButtonIndex: 1, animated: true)
        case 1:
            createAccountErrorAlert.dismiss(withClickedButtonIndex: 1, animated: true)
        default:
            NSLog("Default");
        }
    }
    
    func CustomRootController(StoryboardId:String) {
        let appDelegate = UIApplication.shared.delegate! as! AppDelegate
        let initialViewController = self.storyboard.instantiateViewController(withIdentifier: StoryboardId)
        appDelegate.window?.rootViewController = initialViewController
        appDelegate.window?.makeKeyAndVisible()
    }
    
 }
 
