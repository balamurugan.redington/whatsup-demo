//
//  CityPopupViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 06/06/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class CityPopupViewController: UIViewController,PopupContentViewController,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var appnotableview: UITableView!
    var cv:Customdelegate?
    var closeHandler: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        appnotableview.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    class func instance() -> CityPopupViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "CityPopupViewController") as! CityPopupViewController
    }
    
    @IBAction func CloseAction(_ sender: AnyObject)
    {
        closeHandler?()
    }
    func sizeForPopup(popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: 180, height: 220)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  LibraryAPI.sharedInstance.Cityearray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let mode = (LibraryAPI.sharedInstance.Cityearray.object(at: indexPath.row) as AnyObject).object(forKey: "Mode") as? String
        let detail = (LibraryAPI.sharedInstance.Cityearray.object(at: indexPath.row) as AnyObject).object(forKey: "Detail") as? String
        cell.textLabel?.text =  mode! + " - " + detail!
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mode = (LibraryAPI.sharedInstance.Cityearray.object(at: indexPath.row) as AnyObject).object(forKey: "Mode") as? String
        let detail = (LibraryAPI.sharedInstance.Cityearray.object(at: indexPath.row) as AnyObject).object(forKey: "Detail") as? String
        LibraryAPI.sharedInstance.getCity = mode! + " - " + detail!
        if(LibraryAPI.sharedInstance.Currentcustomerlookup=="City")
        {
            self.closeHandler?()
        }
        else
        {
            self.dismiss(animated: true, completion: nil)
        }
    }
}
