//
//  ConfirmAlertViewController.swift
//  Redington
//
//  Created by truetech on 31/08/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import  CoreData

class ConfirmAlertViewController: UIViewController ,PopupContentViewController
{
    var closeHandler: (() -> Void)?
   
    @IBOutlet var yesbtn: UIButton!
    var  jsonresult = NSDictionary()
    var allowaction = 0
    @IBOutlet var nobtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        yesbtn.layer.cornerRadius=10.0
        nobtn.layer.cornerRadius=10.0
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func sizeForPopup(popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: 260, height: 260)
    }
    
    class func instance() -> ConfirmAlertViewController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        
        return storyboard.instantiateViewController(withIdentifier: "ConfirmAlertViewController") as! ConfirmAlertViewController
    }
    
    
   
    func Uploadvalues()
    {
  
        
        let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)orderEntry.asmx/OrderSubmission")! as URL)
        request.httpMethod = "POST"
        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
       
        request.httpBody = try? JSONSerialization.data(withJSONObject: LibraryAPI.sharedInstance.OrdersubmissonData , options: JSONSerialization.WritingOptions(rawValue: 0))
        
        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
        //  var err: NSError
        do
        {
            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
            jsonresult = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            
            if(self.jsonresult.object(forKey: "Status") as? String  == "Success")
            {

            LibraryAPI.sharedInstance.orderno=(self.jsonresult.object(forKey: "OrderNumber") as? String)!
           
            }
        }
        catch (let e)
        {
            
        }

        
    }
    
    

    
    @IBAction func Noaction(_ sender: AnyObject)
    {
       closeHandler?()

    }
    @IBAction func Yesaction(_ sender: AnyObject)
    {
        let size = CGSize(width: 30, height:30)
        self.local_activityIndicator_start()
        // new version dispatch Que
        
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
            self.Uploadvalues()
            
            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
                self.local_activityIndicator_stop()
                self.closeHandler?()
                if(self.jsonresult.object(forKey: "Status") as? String  == "Success")
                {
                    self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "OrderdetailimageViewController")), animated: true)
                    let twop = self.storyboard?.instantiateViewController(withIdentifier: "OrderdetailimageViewController") as! OrderdetailimageViewController
                    let alertController = UIAlertController(title: "Your Order Id", message: "Number is \(LibraryAPI.sharedInstance.orderno)", preferredStyle: .alert)
                    let ok = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                        // self.navigationController?.showViewController(twop, sender: self)
                    }
                    alertController.addAction(ok)
                    self.present(alertController, animated: true, completion: nil)
                }
                else
                {
                    let alertController = UIAlertController(title: "Alert", message:"Order Submisson Failed" , preferredStyle: .alert)
                    let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                    }
                    alertController.addAction(cancelAction)
                    self.present(alertController, animated: true, completion:nil)
                }
                
            }
        }
    
    }
    
    func navigate()
    {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "OrderdetailimageViewController")), animated: true)
    }
   
 }
