//
//  StockInquiryPopupController.swift
//  Redington
//
//  Created by TRUETECHMAC3 on 27/12/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
//import  NVActivityIndicatorView

class StockInquiryPopupController: UIViewController,PopupContentViewController,UITextFieldDelegate{
    @IBOutlet var Submitbtn: UIButton!
    @IBOutlet var Closebtn: UIButton!
    @IBOutlet var ItemcodetxtField: UITextField!
    var responsearray=NSMutableArray()
    var responsestring=NSMutableArray()
    var closeHandler: (() -> Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Submitbtn.layer.cornerRadius = 15.0
         ItemcodetxtField.delegate = self
        // Do any additional setup after loading the view.
    }
    
    @IBAction func SubmitLocation(_ sender: AnyObject) {
        
        LibraryAPI.sharedInstance.ItemCode = ItemcodetxtField.text!
        
        if  LibraryAPI.sharedInstance.ItemCode == ""{
            self.showalert(Title: "Sorry!", Message: "ItemCode is empty")

        }else{
            self.LoadAllLocation()
        }
        
        
    }
    
    func showalert(Title:String,Message:String)
    {
        
        let alert = UIAlertController(title: Title, message: Message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.default, handler:
            { (ACTION :UIAlertAction!)in
                
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func SubmitbtnAction(_ sender: AnyObject) {
        

    }
    
    @IBAction func ClosebtnAction(_ sender: AnyObject) {
        closeHandler?()
    }

    @IBAction func CloseAction(_ sender: AnyObject)
    {
        
        closeHandler?()
    }

    
    func sizeForPopup(popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: 260, height: 260)
    }
    
    class func instance() -> StockInquiryPopupController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        
        return storyboard.instantiateViewController(withIdentifier: "StockInquiryPopupController") as! StockInquiryPopupController
    }

    
    func LoadAllLocation(){
        
        self.local_activityIndicator_start()
        // new version dispatch Que
        
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
            //http://edi.redingtonb2b.in/Whatsup/StockDetails.asmx/AllStockstockRoom?Itemcode=C003059&UserId=SENTHILKS
            //self.responsestring = LibraryAPI.sharedInstance.Dashboardurlcall(url: "\(LibraryAPI.sharedInstance.GlobalUrl)StockDetails.asmx/AllStockstockRoom?Itemcode=\(LibraryAPI.sharedInstance.ItemCode)&UserId=\(LibraryAPI.sharedInstance.Userid)")
            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)StockDetails.asmx/AllStockstockRoom?Itemcode=\(LibraryAPI.sharedInstance.ItemCode)&UserId=\(LibraryAPI.sharedInstance.Userid)", completion:  { (jsonmutable) in
                self.responsestring  = jsonmutable
            DispatchQueue.main.async {
                
                if(self.responsestring.count>0)
                {
                    self.local_activityIndicator_stop()
                    self.closeHandler?()
                    LibraryAPI.sharedInstance.StockAllLocationArry = self.responsestring
                    self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "StockEnquiryAllLocationViewController")), animated: true)
                    self.sideMenuViewController?.hideMenuViewController()
                    
                    
                }else{
                    self.local_activityIndicator_stop()
                    self.showalert(Title: "Sorry!", Message: "No Item found")
                }

            }
        })
        }
        
        // new version dispatch Que
       

       
        
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        ItemcodetxtField.resignFirstResponder()
        return true;
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == ItemcodetxtField)
        {
            
            let aSet = NSCharacterSet(charactersIn:" abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
           // let numberFiltered = compSepByCharInSet.joinWithSeparator("")
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            //        return string == numberFiltered
            guard let text = textField.text else { return true }
            
            let newLength = text.count + string.count - range.length
            return true && string == numberFiltered && newLength <= 8
            
        }
        return true
    }

}
