//
//  ChequeBounceViewController.swift
//  Redington
//
//  Created by TRUETECHMAC3 on 26/12/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import KCFloatingActionButton

class ChequeBounceViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,KCFloatingActionButtonDelegate {

    @IBOutlet var ChequeTableview: UITableView!
    
    var fromdate=String()
    var todate=String()
    var  responsestring = NSMutableArray()
    var fab=KCFloatingActionButton()
    override func viewDidLoad() {
        super.viewDidLoad()
          print("Cheque pending details ChequeBounceViewController")
        self.customnavigation()
        self.fetchdata()
        ChequeTableview.separatorStyle = .none
        self.layoutFAB()
    }
    
    func fetchdata()
    {
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
            
            self.Loaddata()

            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
                
               
            }
        }
       
    }
    
 
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        
    }
    func Loaddata()
    {
        
       // responsestring = LibraryAPI.sharedInstance.RequestUrl(url: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/ChequeBounce?Customer=\(LibraryAPI.sharedInstance.CustomerCode)&User=\(LibraryAPI.sharedInstance.Userid)")
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/ChequeBounce?Customer=\(LibraryAPI.sharedInstance.CustomerCode)&User=\(LibraryAPI.sharedInstance.Userid)", completion:  { (jsonmuatble) in
           self.responsestring = jsonmuatble
        
            if(self.responsestring.count>0)
        {
            DispatchQueue.main.async {
                 self.ChequeTableview.isHidden=false
                self.local_activityIndicator_stop()
                self.ChequeTableview.reloadData()
            }
          //  dispatch_async(dispatch_get_main_queue(), {
               // self.ChequeTableview.hidden=false
         //   })
            
            
        }
        else
        {
            self.local_activityIndicator_stop()
            DispatchQueue.main.async {
                let alertController = UIAlertController(title: "Alert", message:" No Data Available" , preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                    self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(cancelAction)
                
                self.present(alertController, animated: true, completion:nil)
                
            }
          
            
        }
            })
        
    }
    
    func customnavigation()
        
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(ChequeBounceViewController.backbuttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Cheque Bounce"
        
    }
    
    
    
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            
            
            
            self.fab.close()
        }
        
        fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
           
            
            self.fetchdata()
            
            self.fab.close()
        }
        
        
        fab.fabDelegate = self
        
        self.view.addSubview(fab)
        
    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
        
    }
    
    
    
    func backbuttonClicked(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
  
        if(responsestring.count > 0)
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return responsestring.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell : ChequeBounceTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as! ChequeBounceTableViewCell
        let dicTable = responsestring[indexPath.row] as! NSDictionary
        if(cell == nil)
        {
            cell = Bundle.main.loadNibNamed("Cell", owner: self, options: nil)![0] as! ChequeBounceTableViewCell;
            
        }
        if(indexPath.row%2==0)
        {
            cell.contentView.backgroundColor=(UIColor.init(red: 244/255.0, green: 242/255.0, blue: 242/255.0, alpha: 1.0))
        }
        else
        {
            cell.contentView.backgroundColor=UIColor.white
        }
        
        
        cell.Customercode.text=dicTable.object(forKey: "CUSTOMER CODE") as? String
        cell.UcReferenceno.text=dicTable.object(forKey: "UC REFERENCE NO") as? String
        cell.Reason.text=dicTable.object(forKey: "REASON") as? String
        
        
        
        cell.selectionStyle = .none
        return cell as ChequeBounceTableViewCell
    }
    
   // func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
   // }
        
    
    
    

  
}
