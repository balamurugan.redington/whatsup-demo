//
//  CoredataConnection.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 28/08/18.
//  Copyright © 2018 truetech. All rights reserved.
//


import  UIKit
import  CoreData
//chkin Class Declaration Part

class Breaths{
    
    var businesscode    : String
    var currentmonth    : Int
    var previousmonth   : Int
    var sbucode         : String
    
    init(bizcode: String, sbuco: String, currmth: Int, premth: Int){
        
        self.businesscode   = bizcode
        self.currentmonth   = currmth
        self.sbucode        = sbuco
        self.previousmonth  = premth
        
    }
    
}

class ParseBars{
    
    var age0to30        : Double
    var age30to60       : Double
    var age60to90       : Double
    var age90to120      : Double
    var greaterage120   : Double
    var month           : Int
    var monthYear       : String
    var typeAsmoRSm     : Int
    var year            : Int
    
    init(age0to30: Double, age30to60: Double, age60to90: Double, age90to120: Double, greaterage120: Double, month: Int, monthYear: String, typeAsmoRsm: Int, year: Int){
        
        self.age0to30       = age0to30
        self.age30to60      = age30to60
        self.age60to90      = age60to90
        self.age90to120     = age90to120
        self.greaterage120  = greaterage120
        self.month          = month
        self.monthYear      = monthYear
        self.typeAsmoRSm    = typeAsmoRsm
        self.year           = year
    }
}

//chkin Class Declaration Part


class TargetService{
    
    var BreathsValue    = [Breaths]()
    var ParseBarsvalue  = [ParseBars]()
    static let shared   = TargetService()
    
    private init(){}
    
    //chkin entity of coredata Names
    //chkin Important data
    
    var EntityTarget        = "TargetDetails"
    var EntityBarChart      = "ParseBar"
    var EntityBreath        = "Breath"
    var EntityUserDetails   = "UserDetails"
    var EntityBranchDetails = "BranchDetails"
    
    //chkout Important data
    //chkout entity of coredata Names
    //chkin Create of data
    
    func createUserDetails(EmployeeName: String, EmployeeCode: String, entityname: String) {
        let appDel  :AppDelegate            = UIApplication.shared.delegate as! AppDelegate
        let context :NSManagedObjectContext = appDel.managedObjectContext
        let newuser                         = NSEntityDescription.insertNewObject(forEntityName: entityname, into: context)
        
        newuser.setValue(EmployeeName, forKey: "employeeName")
        newuser.setValue(EmployeeCode, forKey: "employeecode")
        
        if (context.hasChanges) {
            do {
                try context.save()
                print("data saved sucuess fully")
            } catch {
                
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func createBranchDetails(branName: String, branCode: String, entityname: String){
        
        let appDel  :AppDelegate            = UIApplication.shared.delegate as! AppDelegate
        let context :NSManagedObjectContext = appDel.managedObjectContext
        let newuser                         = NSEntityDescription.insertNewObject(forEntityName: entityname, into: context)
        
        newuser.setValue(branName, forKey: "branName")
        newuser.setValue(branCode, forKey: "branCode")
        
        if (context.hasChanges) {
            do {
                try context.save()
                print("data saved sucuess fully")
            } catch {
                
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func createTarget(PercentageAchieved: Double, SalesAchieved: Double, SalesTarget: Double, entityname: String){
        
        let appDel  :AppDelegate            = UIApplication.shared.delegate as! AppDelegate
        let context :NSManagedObjectContext = appDel.managedObjectContext
        //let newuser                         = NSEntityDescription.insertNewObjectforEntityName: intoForEntityForName(entityname, inManagedObjectContext: context)
        let newuser                         = NSEntityDescription.insertNewObject(forEntityName: entityname, into: context)
        
        newuser.setValue(PercentageAchieved, forKey: "percentageAchieved")
        newuser.setValue(SalesAchieved, forKey: "salesAchieved")
        newuser.setValue(SalesTarget, forKey: "salesTarget")
        
        //print("$$$$$$$$$$$$$$$$$$$")
        // print("target value in createtarget",PercentageAchieved,SalesAchieved,SalesTarget)
        // print("$$$$$$$$$$$$$$$$$$$")
        
        if (context.hasChanges) {
            do {
                try context.save()
                print("data saved sucuess fully")
            } catch {
                
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    
    func create(bizcode: String, currmth: Int, premth: Int, sbuco: String,flag: String,message: String,count: Int, entityname: String){
        let appDel  :AppDelegate            = UIApplication.shared.delegate as! AppDelegate
        let context :NSManagedObjectContext = appDel.managedObjectContext
        let newuser                         = NSEntityDescription.insertNewObject(forEntityName: entityname, into: context)
        newuser.setValue(bizcode, forKey: "businesscode")
        newuser.setValue(sbuco, forKey: "sbucode")
        newuser.setValue(currmth, forKey: "currentmonth")
        newuser.setValue(premth, forKey: "previousmonth")
        newuser.setValue(flag, forKey: "flag")
        newuser.setValue(count, forKey: "count")
        newuser.setValue(message, forKey: "message")
        
        
        
        if (context.hasChanges) {
            do {
                try context.save()
                print("data saved sucuess fully")
            } catch {
                
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func createBarChart(age0to30: Double, age30to60: Double, age60to90: Double, age90to120: Double, greaterage120: Double, month: Int, monthYear: String,typeAsmoRSm: Int, Year: Int, TotalIndex: Int , entityname: String){
        let appDel:AppDelegate                  = UIApplication.shared.delegate as! AppDelegate
        let context:NSManagedObjectContext      = appDel.managedObjectContext
        let newuser                            = NSEntityDescription.insertNewObject(forEntityName: entityname, into: context)
        newuser.setValue(age0to30, forKey: "ageing0to30")
        newuser.setValue(age30to60, forKey: "ageing31to60")
        newuser.setValue(age60to90, forKey:"ageing61to90")
        newuser.setValue(age90to120, forKey: "ageing91to120")
        newuser.setValue(greaterage120, forKey: "greaterthan120Ageing")
        newuser.setValue(month, forKey: "month")
        newuser.setValue(monthYear, forKey: "monthYear")
        newuser.setValue(typeAsmoRSm, forKey: "typeAsmoRSm")
        newuser.setValue(Year, forKey: "year")
        newuser.setValue(TotalIndex, forKey: "totalIndex")
        
        if (context.hasChanges) {
            do {
                try context.save()
                // print("data saved sucuess fully")
            } catch {
                
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    //chkout Create of data
    //chkin get data from coredata
    
    func getTargetDetails(){
        BreathsValue.removeAll()
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let context:NSManagedObjectContext = appDel.managedObjectContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: self.EntityTarget)
        do{
            let result = try context.fetch(request)
            //print(result)
            for results in result as! [NSManagedObject]
            {
                
                
                let percentageAchieved = results.value(forKey: "percentageAchieved") as? NSNumber
                let salesAchieved = results.value(forKey: "salesAchieved") as? NSNumber
                let salesTarget = results.value(forKey: "salesTarget") as? NSNumber
                
                //print("***********************")
                //print(percentageAchieved, salesAchieved,salesTarget)
                //print("***********************")
                
                
                if(dashboardapidata.sharevariable.PiechartPercentage.intValue<100)
                {
                    dashboardapidata.sharevariable.months1=["",""]
                    dashboardapidata.sharevariable.pievalues = [(dashboardapidata.sharevariable.PiechartPercentage.doubleValue),100-Double(dashboardapidata.sharevariable.PiechartPercentage)]
                }
                else
                {
                    dashboardapidata.sharevariable.months1=["",""]
                    dashboardapidata.sharevariable.pievalues = [100,0]
                }
                LibraryAPI.sharedInstance.pievalues=dashboardapidata.sharevariable.pievalues
                
                dashboardapidata.sharevariable.months1              = ["",""]
                LibraryAPI.sharedInstance.SalesAcheived             = salesAchieved!
                LibraryAPI.sharedInstance.SalesTarget               = salesTarget!
                dashboardapidata.sharevariable.PiechartPercentage   = percentageAchieved!
                
            }
            
        }catch
        {
            print("data done")
            
        }
    }
    
    func getUserDetails(){
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let context:NSManagedObjectContext = appDel.managedObjectContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: self.EntityUserDetails)
        do{
            let result = try context.fetch(request)
            //print(result)
            if result.count > 0 {
                for results in result as! [NSManagedObject]
                {
                    dashboardapidata.sharevariable.EmployeeName = (results.value(forKey: "employeeName") as? String)!
                    dashboardapidata.sharevariable.EmployeeCode = (results.value(forKey: "employeecode") as? String)!
                    
                }
                
            }else{
                print("validation done")
                
            }
            
        }catch
        {
            print("data done")
           
        }
    }
    func getBranchDetails(){
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let context:NSManagedObjectContext = appDel.managedObjectContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: self.EntityBranchDetails)
        do{
            let result = try context.fetch(request)
            //print(result)
            if result.count > 0 {
                for results in result as! [NSManagedObject]
                {
                    LibraryAPI.sharedInstance.branchname = (results.value(forKey: "branName") as? String)!
                    LibraryAPI.sharedInstance.BranchCodeNew = (results.value(forKey: "branCode") as? String)!
                }
                
            }else{
                print("validation done")
                
            }
           
        }catch
        {
            print("data done")
           dashboardapidata.sharevariable.LoadingValue = 1
        }
    }
    
    
    
    func get(){
        BreathsValue.removeAll()
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let context:NSManagedObjectContext = appDel.managedObjectContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: self.EntityBreath)
        do{
            let result = try context.fetch(request)
            // print(result)
            for results in result as! [NSManagedObject]
            {
                let busicode = results.value(forKey: "businesscode") as? String
                let currmonth = results.value(forKey: "currentmonth") as? Int
                let prevmonth = results.value(forKey: "previousmonth") as? Int
                let scode = results.value(forKey: "sbucode") as? String
                let flag = results.value(forKey: "flag") as? String
                let messgae  = results.value(forKey: "message") as? String
                let count = results.value(forKey: "count") as? Int
                
                //BreathsValue.append(Breaths.init(bizcode: busicode!, sbuco: scode!, currmth: currmonth!, premth: prevmonth!))
                
                dashboardapidata.sharevariable.BusinessCode     = busicode!
                dashboardapidata.sharevariable.CurrentMonth     = currmonth!
                dashboardapidata.sharevariable.PreviousMonth    = prevmonth!
                dashboardapidata.sharevariable.sbucode          = scode!
                dashboardapidata.sharevariable.Flag             = flag!
                dashboardapidata.sharevariable.Breathmessage    = messgae!
                dashboardapidata.sharevariable.Breathcount      = count!
                
                //print("//****************************************************")
                //print("value in Breath analysis",busicode!,scode!,currmonth!, prevmonth!)
                //print("//****************************************************")
            }
           
        }catch
        {
            print("data done")
            dashboardapidata.sharevariable.LoadingValue = 1
        }
    }
    
    func removeparsedata(){
        dashboardapidata.sharevariable.Array30.removeAll()
        dashboardapidata.sharevariable.array60.removeAll()
        dashboardapidata.sharevariable.array90.removeAll()
        dashboardapidata.sharevariable.array120.removeAll()
        dashboardapidata.sharevariable.arraymorethan120.removeAll()
        dashboardapidata.sharevariable.monthsbar.removeAll()
    }
    
    func ParseBarsget(){
        
        ParseBarsvalue.removeAll()
        
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let context:NSManagedObjectContext = appDel.managedObjectContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: self.EntityBarChart)
        
        do{
            let result = try context.fetch(request)
            for results in result as! [NSManagedObject]
            {
                let age0to30        = results.value(forKey: "ageing0to30") as? Double
                let age30to60       = results.value(forKey: "ageing31to60") as? Double
                let age60to90       = results.value(forKey: "ageing61to90") as? Double
                let age90to120      = results.value(forKey: "ageing91to120") as? Double
                let greaterage120   = results.value(forKey: "greaterthan120Ageing") as? Double
                let month           = results.value(forKey: "month") as? Int
                let monthYear       = results.value(forKey: "monthYear") as? String
                let typeAsmorRSm    = results.value(forKey: "typeAsmoRSm") as? Int
                let Year            = results.value(forKey: "year") as? Int
                let totalindex      = results.value(forKey: "totalIndex")
                
                ParseBarsvalue.append(ParseBars.init(age0to30: age0to30!, age30to60: age30to60!, age60to90: age60to90!, age90to120: age90to120!, greaterage120: greaterage120!, month: month!, monthYear: monthYear!, typeAsmoRsm: typeAsmorRSm!, year: Year!))
            }
            
            dashboardapidata.sharevariable.LoadingValue = 1
            
        }catch
        {
            print("data done")
            dashboardapidata.sharevariable.LoadingValue = 1
        }
        self.removeparsedata()
        
        if ParseBarsvalue.count > 0 {
            dashboardapidata.sharevariable.bartotalindex = ParseBarsvalue.count
            for nlist in 0...ParseBarsvalue.count-1{
                let Parselist: ParseBars
                Parselist = ParseBarsvalue[nlist]
                //print("**********************************")
                // print("index ",nlist," /value",Parselist.age0to30,Parselist.age30to60,Parselist.age60to90,Parselist.age90to120,Parselist.greaterage120)
                //print("**********************************")
                dashboardapidata.sharevariable.Array30.append(Parselist.age0to30)
                dashboardapidata.sharevariable.array60.append(Parselist.age30to60)
                dashboardapidata.sharevariable.array90.append(Parselist.age60to90)
                dashboardapidata.sharevariable.array120.append(Parselist.age90to120)
                dashboardapidata.sharevariable.arraymorethan120.append(Parselist.greaterage120)
                dashboardapidata.sharevariable.monthsbar.append(Parselist.monthYear)
                dashboardapidata.sharevariable.monthvalue = Parselist.month
                dashboardapidata.sharevariable.typeasmoRSm = Parselist.typeAsmoRSm
                dashboardapidata.sharevariable.year = Parselist.year
                 dashboardapidata.sharevariable.LoadingValue = 1
            }
            
        }else{
            dashboardapidata.sharevariable.LoadingValue = 1
            print("No data available in File")
            
        }
        
    }
    //chkin get data from coredata
    
    //chkin deletion process
    func deleteAllData(entity: String)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false
        do
        {
            let results = try managedContext.fetch(fetchRequest)
            for managedObject in results
            {
                let managedObjectData:NSManagedObject = managedObject as! NSManagedObject
                managedContext.delete(managedObjectData)
                print("Deleted all the data")
            }
        } catch let error as NSError {
            print("Detele all data in \(entity) error : \(error) \(error.userInfo)")
        }
    }
    //chkout deletion process
    func Clearalldata(){
        deleteAllData(entity: self.EntityBarChart)
        deleteAllData(entity: self.EntityBreath)
        deleteAllData(entity: self.EntityTarget)
        deleteAllData(entity: self.EntityUserDetails)
        deleteAllData(entity: self.EntityBranchDetails)
    }
}
