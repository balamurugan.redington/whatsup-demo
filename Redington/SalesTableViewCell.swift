
//
//  SalesTableViewCell.swift
//  Whatsup
//
//  Created by truetech on 22/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit

class SalesTableViewCell: UITableViewCell {
  
     @IBOutlet var Month: UILabel!
      @IBOutlet var Sale: UILabel!
      @IBOutlet var Margin: UILabel!
      @IBOutlet var Return: UILabel!
      @IBOutlet var Overdue: UILabel!
    
   
    override func awakeFromNib() {
        super.awakeFromNib()
         Sale.sizeToFit()
        Return.sizeToFit()
        Margin.sizeToFit()
        // Initialization code
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
