//
//  DeliveryViewController.swift
//  Redington
//
//  Created by truetech on 26/08/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import KCFloatingActionButton

class DeliveryViewController: UIViewController,KCFloatingActionButtonDelegate,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    
    @IBOutlet weak var Creditsdays: UITextField!
    @IBOutlet var proceedbtn: UIButton!
    var Buttontitle = [Int:String]()
    @IBOutlet var deliverytable:UITableView!
    @IBOutlet var Selectbutton: UIButton!
    var fab = KCFloatingActionButton()
    var responsestring=NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customnavigation()
        self.layoutFAB()
        
        Creditsdays.keyboardType = .numberPad
        self.addDoneButtonOnKeyboard()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.fetchdata()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func addDoneButtonOnKeyboard()
        
    {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0,y: 0,width: 320,height: 50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(BillingwithODCustomerViewController.doneButtonAction))
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        doneToolbar.items = items as AnyObject as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        Creditsdays.inputAccessoryView = doneToolbar
        
    }
    func doneButtonAction()
    {
        Creditsdays.resignFirstResponder()
    }
    

    func fetchdata()
    {
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
              self.Loaddata1()
            
            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
              
            }
        }
        
        // new version dispatch Que

    
    }
    
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            LibraryAPI.sharedInstance.lastviewcontroller="Dashboardviewcontroller"
            self.fab.close()
        }
        
        fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
            
            
            self.fetchdata()
            
            self.fab.close()
        }
        
        fab.fabDelegate = self
        
        self.view.addSubview(fab)
        
    }
    
    func Loaddata1()
    {
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)orderEntry.asmx/AddressSequence?CustomerCode=\(LibraryAPI.sharedInstance.CustomerCode)", completion: { (jsonmutable) in
            self.responsestring = jsonmutable
        if(self.responsestring.count>0)
        {
           DispatchQueue.main.async {
                  self.deliverytable.isHidden=false
            self.local_activityIndicator_stop()
            self.deliverytable.reloadData()
            }
        }
        else
        {
            DispatchQueue.main.async {
                  self.deliverytable.isHidden=true
                self.local_activityIndicator_stop()
            }
        }
        })
        
        
    }
    
    @IBAction func ProceedAction(_ sender: AnyObject)
    {
        
        if(Buttontitle.count>0)
        {
            
            
            if(Creditsdays.text=="")
            {
                self.NormalAlert(title: "Sorry..!!", Messsage:  "Enter the Credit Days")
               /* let alert = UIAlertController(title: "Sorry..!!", message: "Enter the Credit Days", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)*/
            }
            else
            {
                LibraryAPI.sharedInstance.OrdersubmissonData.setObject(Creditsdays.text!, forKey: "Payment_Days" as NSCopying)
                
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: ConfirmAlertViewController.instance())
                    
                    .didShowHandler { popup in
                        
                    }
                    .didCloseHandler { _ in
                }
                let container = ConfirmAlertViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                }
                
                popup.show(childViewController: container)
            }
            
        }
        else
        {
            self.NormalAlert(title: "Sorry..!!", Messsage: "No address Selected")
        /*   let alert = UIAlertController(title: "Sorry..!!", message: "No address Selected", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)*/
        }
        
    }
    func customnavigation()
        
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        
        //    customView.backbutton.hidden=false
        customView.Notificationbtn.isHidden=true
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        // customView.Notificationbtn.addTarget(self, action: #selector(SSASideMenu.presentRightMenuViewController),forControlEvents:UIControlEvents.TouchUpInside)
        customView.MenuBtn.addTarget(self, action: #selector(DeliveryViewController.backbuttonClicked),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Delivery Address"
        
    }
    
    
    func KCFABOpened(fab: KCFloatingActionButton) {
        
    }
    
    
    @IBAction func SelectbtnAction(_ sender: AnyObject)
    {
    }
    
    
    
       
    //TableView Delegates
    
    func numberOfSections(in tableView: UITableView) -> Int {
       
        if responsestring.count > 0
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return responsestring.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        var cell : DeliveryTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as! DeliveryTableViewCell
        
        if(cell == nil)
        {
            cell = Bundle.main.loadNibNamed("Cell", owner: self, options: nil)![0] as! DeliveryTableViewCell;
            
        }
        let dic = responsestring[indexPath.row] as! NSDictionary
        cell.selectbtn.addTarget(self, action: #selector(DeliveryViewController.buttonClicked), for: UIControlEvents.touchUpInside)
        cell.selectionStyle = .none
        cell.selectbtn.tag=indexPath.row
        
        cell.Customername.text=dic.object(forKey: "Customer Name") as? String
        
        let add=dic.object(forKey:"Address1") as? String
        let base64DecodedData = NSData(base64Encoded:add!, options: [])!
        let base64DecodedString = String(data: base64DecodedData as Data, encoding: String.Encoding.utf8)!
        
        
        let add1=dic.object(forKey:"Address2") as? String
        let base64DecodedData1 = NSData(base64Encoded:add1!, options: [])!
        let base64DecodedString1 = String(data: base64DecodedData1 as Data, encoding: String.Encoding.utf8)!
        
        
        let add2=dic.object(forKey:"Address3") as? String
        let base64DecodedData2 = NSData(base64Encoded:add2!, options: [])!
        let base64DecodedString2 = String(data: base64DecodedData2 as Data, encoding: String.Encoding.utf8)!
        
        
      //  let str=base64DecodedString.stringByAppendingString(",\(base64DecodedString1)")
        let str = base64DecodedString.appending(",\(base64DecodedString1)")
        let formattedadd=str.appending(base64DecodedString2)
        
        
        cell.addresslabel.text=formattedadd
        cell.Statelabel.text=dic.object(forKey:"Address5") as? String
        cell.pincodelabel.text=dic.object(forKey:"Pincode") as? String
        cell.citylabel.text=dic.object(forKey:"Address4") as? String
        
        
        if let titletext = Buttontitle[indexPath.row] {
            //  cell.selectbtn.setTitle(titletext, forState: UIControlState.Normal)
            cell.selectbtn.setImage(UIImage(named:"select.png"), for: UIControlState.normal)
        }
        else {
            // cell.selectbtn.setTitle("Select", forState: UIControlState.Normal)
            
            cell.selectbtn.setImage(UIImage(named:"unselect.png"), for: UIControlState.normal)
            
        }
        
        
        //  cell.contentView.alpha = 0
        
        return cell as DeliveryTableViewCell
    }
    
    
    
    
    
    
    func buttonClicked(sender:UIButton) {
        
        _ = sender.convert(CGPoint.zero, to: self.deliverytable)
        
        if(Buttontitle.count>0)
        {
            Buttontitle.removeAll()
        }
        Buttontitle[sender.tag]="Selected"
        
        //        //"Address_Sequence": "000",
        //        "Address_Line1": "test",
        //        "Address_Line2": "test",
        //        "Address_Line3": "test",
        //        "City": "test",
        //        "state": "test",
        //        "Pincode": "test",
        //        "Order_Number": "test",
        let dicreS1 = responsestring[sender.tag] as! NSDictionary
        LibraryAPI.sharedInstance.OrdersubmissonData.setObject((dicreS1.object(forKey: "Address Sequence") as? String)!, forKey: "Address_Sequence" as NSCopying)
        LibraryAPI.sharedInstance.OrdersubmissonData.setObject((dicreS1.object(forKey:  "Address1") as? String)!, forKey: "Address_Line1" as NSCopying)
        LibraryAPI.sharedInstance.OrdersubmissonData.setObject((dicreS1.object(forKey:  "Address2") as? String)!, forKey: "Address_Line2" as NSCopying)
        LibraryAPI.sharedInstance.OrdersubmissonData.setObject((dicreS1.object(forKey:  "Address3") as? String)!, forKey: "Address_Line3" as NSCopying)
        LibraryAPI.sharedInstance.OrdersubmissonData.setObject((dicreS1.object(forKey:  "Address4") as? String)!, forKey: "City" as NSCopying)
        LibraryAPI.sharedInstance.OrdersubmissonData.setObject((dicreS1.object(forKey:  "Address5") as? String)!, forKey: "state" as NSCopying)
        LibraryAPI.sharedInstance.OrdersubmissonData.setObject((dicreS1.object(forKey:  "Pincode") as? String)!, forKey: "Pincode" as NSCopying)
        LibraryAPI.sharedInstance.OrdersubmissonData.setObject(LibraryAPI.sharedInstance.Stockroom, forKey: "Stock_Room" as NSCopying)
        //        LibraryAPI.sharedInstance.OrdersubmissonData.setObject(Creditsdays.text!, forKey: "Payment_Days")
        //   LibraryAPI.sharedInstance.OrdersubmissonData.setObject("Test", forKey: "Order_Number")
        
        
        LibraryAPI.sharedInstance.OrdersubmissonData.setObject( LibraryAPI.sharedInstance.orderdetails, forKey: "LineItems" as NSCopying)
        
        
        
        
        self.deliverytable.reloadData()
        
        
        
        
    }
    
    
    //textfield Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
       
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
      
        fab.isHidden=true
        
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        fab.isHidden=false
        textField.resignFirstResponder()
        
        
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        
        let newLength = text.count + string.count - range.length
        return newLength <= 3 // Bool
        
    }
    
    
    
    
    func backbuttonClicked(sender:UIButton) {
        
        LibraryAPI.sharedInstance.OrdersubmissonData.removeObject(forKey: "Address_Sequence")
        LibraryAPI.sharedInstance.OrdersubmissonData.removeObject(forKey: "Address_Line1")
        LibraryAPI.sharedInstance.OrdersubmissonData.removeObject(forKey: "Address_Line2")
        LibraryAPI.sharedInstance.OrdersubmissonData.removeObject(forKey: "Address_Line3")
        LibraryAPI.sharedInstance.OrdersubmissonData.removeObject(forKey: "City")
        LibraryAPI.sharedInstance.OrdersubmissonData.removeObject(forKey: "state")
        LibraryAPI.sharedInstance.OrdersubmissonData.removeObject(forKey: "Pincode")
        LibraryAPI.sharedInstance.OrdersubmissonData.removeObject(forKey: "LineItems")
        
        
        
        self.navigationController?.popViewController(animated: true)
        //  self.animate()
    }
    

}
