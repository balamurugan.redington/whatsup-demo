//
//  BusinesswiseViewController.swift
//  Redington
//
//  Created by truetech on 12/08/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
import KCFloatingActionButton

class BusinesswiseViewController: UIViewController,KCFloatingActionButtonDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITabBarDelegate {
    
    @IBOutlet var menubtn: UIButton!
    @IBOutlet var backbtn: UIButton!
    @IBOutlet var Businesswise: UITabBarItem!
    @IBOutlet var Tabbars: UITabBar!
    @IBOutlet var DashBoardCollectionView: UICollectionView!
    @IBOutlet weak var Popupview: UIView!
    
    var collectionViewLayout: CustomImageFlowLayout!
    var responsestring1 = NSMutableArray()
    var fab=KCFloatingActionButton() //do it
    var months=[String]()
    var pievalues=[Double]()
    var PiechartPercentage=NSNumber()
    var isanimated=Bool()
    var isreloaded=Bool()
    var Isfabclicked=Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customnavigation()
        isanimated=false
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(TargetAcheivementViewController.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        self.layoutFAB()
        collectionViewLayout = CustomImageFlowLayout()
        DashBoardCollectionView.collectionViewLayout = collectionViewLayout
        DashBoardCollectionView.backgroundColor = UIColor.clear
        Tabbars.selectedItem=Businesswise
        isreloaded=false
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.init(red:30/255.0 , green: 184/255.0, blue: 120/255.0, alpha: 1.0)], for:.selected)
        for item in self.Tabbars.items! as [UITabBarItem] {
            if let image = item.image {
                item.selectedImage = image.imageWithColor(tintColor: UIColor.init(red:30/255.0 , green: 184/255.0, blue: 120/255.0, alpha: 1.0)).withRenderingMode(.alwaysOriginal)
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        if responsestring1.count > 0{
           // print("data in",responsestring1)
            self.DashBoardCollectionView.isHidden=false
            self.Popupview.isHidden=true
            self.DashBoardCollectionView.reloadData()
        }else{
             FetchValues()
        }
       
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    func FetchValues()
    {
        DispatchQueue.global(qos: .background).async {
            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)SalesDetails.asmx/BusinessSalesTargetDetails?UserID=\(LibraryAPI.sharedInstance.Userid)&BranchCode=\(LibraryAPI.sharedInstance.BranchCodeNew)&Qtr=&Year=", completion: { (jsonmutable) in
                  self.responsestring1 = jsonmutable
                if(self.responsestring1.count>0) {
                        self.DashBoardCollectionView.isHidden=false
                        self.Popupview.isHidden=true
                        self.DashBoardCollectionView.reloadData()
                        self.local_activityIndicator_stop()
                } else {
                        self.DashBoardCollectionView.isHidden=true
                        let alertController = UIAlertController(title: "Alert", message: "No Data Available" , preferredStyle: .alert)
                        let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                            let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                            self.navigationController?.pushViewController(lvc, animated: false)
                        }
                        alertController.addAction(cancelAction)
                        self.present(alertController, animated: true, completion:nil)
                        self.DashBoardCollectionView.reloadData()
                        self.local_activityIndicator_stop()
                }
            })
        }
    }
    
    func BackGroundRefresh(Notification:NSNotification)
    {
        self.FetchValues()
    }
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                self.Navigate()
            case UISwipeGestureRecognizerDirection.down: break
            case UISwipeGestureRecognizerDirection.left: break
            case UISwipeGestureRecognizerDirection.up: break
            default:
                break
            }
        }
    }

    override func viewWillLayoutSubviews() {
        let orientation=UIDevice.current.orientation
        if(orientation.isLandscape) {
            if(Isfabclicked==true) {
            } else {
                DashBoardCollectionView.reloadData()
                Isfabclicked=false
            }
        } else {
            if(Isfabclicked==true) {
            } else {
                DashBoardCollectionView.reloadData()
                Isfabclicked=false
            }
        }
    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
        self.Isfabclicked=true
    }
    
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            self.Isfabclicked=false
            if (self.navigationController?.topViewController!.isKind(of: DashBoardViewController.classForCoder()) != nil) {
                self.navigationController?.popToViewController((self.navigationController?.viewControllers.first)!, animated: true)
            } else {
                let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                self.navigationController?.pushViewController(lvc, animated: false)
            }
            self.fab.close()
        }
        fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
            self.Isfabclicked=false
            self.FetchValues()
            self.fab.close()
        }
        fab.fabDelegate = self
        self.view.addSubview(fab)
    }
    
    func customnavigation() {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: 65)
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(self.buttonClicked), for: .touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "SalesTarget/Achieved"
    }
    func buttonClicked(sender:UIButton)
    {
        let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
        self.navigationController?.pushViewController(lvc, animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       // print("count in cell",responsestring1.count)
        return responsestring1.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! DashBoardCollectionViewCell
        let dic = responsestring1[indexPath.row] as! NSDictionary
        let percentage = ((dic.object(forKey: "Percentage Achieved") as! NSNumber))
        let Business = dic.object(forKey: "Business Description") as? String
        cell.TitleLabel.text = Business
        let num = NumberFormatter()
        num.numberStyle=NumberFormatter.Style.currency
        num.locale = NSLocale(localeIdentifier: "en_IN") as Locale?
        let str=num.string(from: NSNumber(value:((dic.object(forKey: "Sales Target") as! Double))))
        let num1=NumberFormatter()
        num1.numberStyle = NumberFormatter.Style.currency
        num1.locale = NSLocale(localeIdentifier: "en_IN") as Locale?
        let str1=num.string(from: NSNumber(value:(dic.object(forKey: "Sales Achieved") as! Double)))
        cell.SalesAcheivedLabel.text=str1
        cell.SalesTargetLabel.text=str
        cell.ytdlbl.text = "YTD (Year To Date)"
        
        if(isanimated==false) {
                pievalues = [(percentage.doubleValue),100-Double(percentage)]
                cell.pieval.text = String(Int(pievalues[0])) + " %"
                cell.setval(Floatval: pievalues)
        } else {
                months=["",""]
                pievalues = [(percentage.doubleValue),100-Double(percentage)]
                cell.pieval.text = String(Int(pievalues[0])) + " %"
                 cell.setval(Floatval: pievalues)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let lastRowIndex = collectionView.numberOfItems(inSection: collectionView.numberOfSections-1)
        if (indexPath.row == lastRowIndex - 1) {
            isanimated=true
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let orientation = UIApplication.shared.statusBarOrientation
        if(orientation == .landscapeLeft || orientation == .landscapeRight) {
            return CGSize(width: collectionView.frame.size.width/2.0-8, height:250)
        } else{
            return CGSize(width: collectionView.frame.size.width/2.0-8, height:250)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(5,5,5,5) // margin between cells
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 1.0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    override func viewDidLayoutSubviews() {
    }
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        switch item.tag {
        case 1:
            self.Navigate()
            break
        case 2:
            break
        default:
            break
        }
    }
    
    func Navigate() {
        let slideInFromLeftTransition = CATransition()
        slideInFromLeftTransition.type = kCAGravityTop
        slideInFromLeftTransition.subtype = kCATransitionFromLeft
        slideInFromLeftTransition.duration = 0.2
        slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "TargetAcheivementViewController") as! TargetAcheivementViewController
        self.navigationController?.view.layer.add(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
        self.navigationController?.pushViewController(lvc, animated: false)
    }
}
