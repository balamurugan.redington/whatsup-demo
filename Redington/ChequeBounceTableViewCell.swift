//
//  ChequeBounceTableViewCell.swift
//  Redington
//
//  Created by TRUETECHMAC3 on 26/12/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit

class ChequeBounceTableViewCell: UITableViewCell {

    @IBOutlet var Customercode: UILabel!
    @IBOutlet var UcReferenceno: UILabel!
    @IBOutlet var Reason: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
