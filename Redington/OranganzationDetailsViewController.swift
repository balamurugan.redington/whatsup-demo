//
//  OranganzationDetailsViewController.swift
//  Whatsup
//
//  Created by truetech on 23/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import KCFloatingActionButton

class OranganzationDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,KCFloatingActionButtonDelegate,CLLocationManagerDelegate,MKMapViewDelegate {
    var imagearray=[String]()
    var titlearray=[String]()
    var responsestring=NSMutableArray()
    var responsestring1=NSMutableArray()
    var fab=KCFloatingActionButton()
    var globaladdress=String()
    var lat=Double()
    var long=Double()
    var address=String()
    
    @IBOutlet weak var nodataview: UIView!
    @IBOutlet var OrganisationTableview: UITableView!
    @IBOutlet var backbtn: UIButton!
    @IBOutlet var menubtn: UIButton!
    
    var addressarray=NSMutableArray()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.layoutFAB()
        self.customnavigation()
        nodataview.isHidden = true
       //  NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(OranganzationDetailsViewController.BackGroundRefresh(_:)), name: UIApplicationWillEnterForegroundNotification, object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.loaddata()
        
    }
    
    
    func BackGroundRefresh(notification: NSNotification)
    {
        self.local_activityIndicator_start()
        // new version dispatch Que
        
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
            
             self.loaddata()
            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
                self.local_activityIndicator_stop()
                self.OrganisationTableview.reloadData()
                
            }
        }
        
        // new version dispatch Que
       
    }
   
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    
    func loaddata()
    {
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)/Customer.asmx/CompanyDetails?Customer=\(LibraryAPI.sharedInstance.CustomerCode)", completion:  { (jsonmutable) in
                self.responsestring = jsonmutable
            DispatchQueue.main.async {
                
                if(self.responsestring.count>0)
                {
                    self.local_activityIndicator_stop()
                    let dic = self.responsestring[0] as! NSDictionary
                    let city = dic.object(forKey: "City") as? String
                    let add1=dic.object(forKey: "Address Line2") as? String
                    let add2=dic.object(forKey: "Address Line3") as? String
                    self.address = add1! + ", " + add2! + ", " + city!
                    self.globaladdress=self.address
                    let geocoder = CLGeocoder()
                    geocoder.geocodeAddressString(self.address, completionHandler: {(placemarks, error) -> Void in
                        if((error) != nil){
                            print("Error", error ?? "")
                            self.OrganisationTableview.reloadData()
                        }
                        if let placemark = placemarks?[0] as? CLPlacemark {
                            let coordinates:CLLocationCoordinate2D = placemark.location!.coordinate
                            print("Lat: \(coordinates.latitude) -- Long: \(coordinates.longitude)")
                            self.lat = coordinates.latitude
                            self.long = coordinates.longitude
                            self.OrganisationTableview.reloadData()
                        }
                    })
                    
                }
                    
                else
                {
                   self.local_activityIndicator_stop()
                    self.OrganisationTableview.isHidden=true
                    self.nodataview.isHidden = false
                    //  })
                    
                }
            }
          })
        }
    }
    
   
    
    func customnavigation()
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(OranganzationDetailsViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Organization Details"
    }
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            self.fab.close()
        }
        
        fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
            self.loaddata()
            self.fab.close()
        }
        
        fab.fabDelegate = self
        self.view.addSubview(fab)
        
    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
        
    }
    
    func buttonClicked(sender:UIButton)
    {
        
        OperationQueue.main.addOperation({
            
            LibraryAPI.sharedInstance.tracker="1"
            LibraryAPI.sharedInstance.ismapopened=false
            self.responsestring.removeAllObjects()
            self.navigationController?.popViewController(animated: true)

            
        })
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //TableView Delegates
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        if responsestring.count > 0
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
            return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var cell : OranganzationDetailsTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as! OranganzationDetailsTableViewCell
        if(responsestring.count > 0)
        {
            let dicres1 = responsestring[0] as! NSDictionary
            if(cell == nil)
            {
                cell = Bundle.main.loadNibNamed("Cell", owner: self, options: nil)![0] as! OranganzationDetailsTableViewCell;
                
            }
            cell.Company.text=dicres1.object(forKey: "Customer name") as? String
            cell.Email.text=dicres1.object(forKey: "City") as? String
            let city = dicres1.object(forKey: "City") as? String
            let pincode=dicres1.object(forKey: "Pincode") as? String
            let state=dicres1.object(forKey: "State") as? String
            let formattedpincode=state?.appending("-\(pincode!)")
            if(formattedpincode=="")
            {
                cell.statelabel.text=""
            }
            else
            {
                cell.statelabel.text=formattedpincode
            }
            let add=dicres1.object(forKey: "Address Line1") as? String
            let add1=dicres1.object(forKey: "Address Line2") as? String
            let add2=dicres1.object(forKey: "Address Line3") as? String
            let formattedadd = add! + " " + add1! + "," + add2! + city! + ", " + state! + ", " + pincode!
            cell.Address.text=formattedadd
            cell.mapview.delegate=self
            print("latitude and longitutde", self.lat,self.long)
            let coordinateRegion = CLLocationCoordinate2DMake(self.lat, self.long)
            var mapRegion = MKCoordinateRegion ()
            mapRegion.center = coordinateRegion;
            mapRegion.span.latitudeDelta = 0.2;
            mapRegion.span.longitudeDelta = 0.2;
            cell.mapview.setRegion(mapRegion, animated: true)
            let cv=mapkitclass()
            cv.title=globaladdress
            cell.mapview.regionThatFits(mapRegion)
            let pinAnnotation = mapkitclass()
            pinAnnotation.title=self.globaladdress
            pinAnnotation.setCoordinate(newCoordinate: coordinateRegion)
            cell.mapview.addAnnotation(pinAnnotation)
        }
        else
        {
            
        }
        
       
        
        cell.selectionStyle = .none
        
        return cell as OranganzationDetailsTableViewCell
    }
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
   
        mapView.delegate=self
        if annotation is mapkitclass {
            let pinAnnotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "myPin")
            if #available(iOS 10.0, *) {
                pinAnnotationView.pinTintColor = UIColor.purple
            } else {
                // Fallback on earlier versions
            }
            pinAnnotationView.isDraggable = true
            pinAnnotationView.canShowCallout = true
            pinAnnotationView.animatesDrop = true
            // pinAnnotationView.
            
            let deleteButton = UIButton.init(type:UIButtonType.custom)
            deleteButton.frame.size.width = 44
            deleteButton.frame.size.height = 44
            deleteButton.backgroundColor = UIColor.white
            deleteButton.setImage(UIImage(named: "dir.png"), for: .normal)
            pinAnnotationView.leftCalloutAccessoryView = deleteButton
            return pinAnnotationView
            
        }
        return nil
    }
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if (view.annotation as? mapkitclass) != nil {
            LibraryAPI.sharedInstance.ismapopened=true
            let latitute:CLLocationDegrees =  mapView.region.center.latitude
            let longitute:CLLocationDegrees =   mapView.region.center.longitude
            
            let regionDistance:CLLocationDistance = 200
            let coordinates = CLLocationCoordinate2DMake(latitute, longitute)
            
            let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
            let options = [
                MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
                // MKLaunchOptionsMapSpanKey: NSValue(MKCoordinateSpan: regionSpan.span)
            ]
            let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
            let mapItem = MKMapItem(placemark: placemark)
            mapItem.name = globaladdress
            mapItem.openInMaps(launchOptions: options)
            
        }
    }
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
      
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(responsestring.count > 0)
        {
        return 700;
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
    }
    
    
    
    
    
}
