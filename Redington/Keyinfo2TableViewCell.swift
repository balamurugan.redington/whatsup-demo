//
//  Keyinfo2TableViewCell.swift
//  Whatsup
//
//  Created by truetech on 23/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit

class Keyinfo2TableViewCell: UITableViewCell {

    @IBOutlet var Bonusdislike: UIImageView!
    @IBOutlet var Creditdislike: UIImageView!
    @IBOutlet var SalesDislike: UIImageView!
    @IBOutlet var Bonuschequespercentage: UILabel!
    @IBOutlet var creditpercentage: UILabel!
    @IBOutlet var Salespercentage: UILabel!
    @IBOutlet var Bonuschequehold: UILabel!
    @IBOutlet var creditholdscore: UILabel!
    @IBOutlet var saleshealthscore: UILabel!
    @IBOutlet var Orderslabel: UILabel!
    @IBOutlet var Creditdayslabel: UILabel!
    @IBOutlet var Boundschqsbtn: UIButton!
    
    @IBOutlet weak var securitybtn: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
         securitybtn.layer.cornerRadius=securitybtn.frame.size.width/2
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
