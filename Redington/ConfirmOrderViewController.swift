//
//  ConfirmOrderViewController.swift
//  Redington
//
//  Created by truetech on 26/08/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
import KCFloatingActionButton


class ConfirmOrderViewController: UIViewController,KCFloatingActionButtonDelegate,UITableViewDataSource,UITableViewDelegate{

    @IBOutlet var backbtn: UIButton!
    @IBOutlet var menubtn: UIButton!
    @IBOutlet var orderTableview: UITableView!
    @IBOutlet var Footerview: UIView!
    @IBOutlet var Totallabel: UILabel!
    var popup = PopupController()
    var PersistentArray=NSMutableArray()
    var fab = KCFloatingActionButton()
    var str1=String()
    @IBOutlet var cartbtn: UIButton!
   
    var cartarray=NSMutableArray()
    var listfromorder=[Element]()
     var tot=Double()
      @IBOutlet var menuAction: UIButton!
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.layoutFAB()
        super.viewWillAppear(true)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tot=0;
        Totallabel.text=String(tot)
       
       
        self.computetotal()
       // cartbtn.addTarget(self, action: #selector(SSASideMenu.presentRightMenuViewController),forControlEvents:UIControlEvents.TouchUpInside)
        backbtn.addTarget(self, action: #selector(ConfirmOrderViewController.backaction),for:UIControlEvents.touchUpInside)
        // Do any additional setup after loading the view.
    }
    
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
             LibraryAPI.sharedInstance.lastviewcontroller="Dashboardviewcontroller"
            self.fab.close()
        }
        fab.fabDelegate = self
        
        self.view.addSubview(fab)
        
    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
       
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cartaction(_ sender: AnyObject)
    {
        
        
    }
    
    
    func backaction(sender:UIButton)
    {
        
       self.navigationController?.popViewController(animated: true)
        
    }
    
    //TableView Delegates
    
   func numberOfSections(in tableView: UITableView) -> Int {
        if(PersistentArray.count > 0)
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return PersistentArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {        var cell : preordersummaryTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as! preordersummaryTableViewCell
        if(cell == nil)
        {
            cell = Bundle.main.loadNibNamed("Cell", owner: self, options: nil)![0] as! preordersummaryTableViewCell;
            
        }
      let dicPersist = PersistentArray[indexPath.row] as! NSDictionary
        let val = dicPersist.object(forKey: "price")as! Double
        let val1 = dicPersist.object(forKey: "quantity")as! Int
        cell.productname.text = dicPersist.object(forKey: "products") as? String
        
        cell.selectionStyle = .none
   
        
        tot = tot + (Double(val) * Double(val1)) //Prices * Quantity
        

   
        let num=NumberFormatter()
        num.numberStyle=NumberFormatter.Style.currency
        num.locale = NSLocale(localeIdentifier: "en_IN") as Locale!
       
        let str=num.string(from: NSNumber(value:Float(val) * 1))
       
        
        cell.price.text = str
        
        cell.vendor.text=dicPersist.object(forKey: "vendor") as? String
        cell.quantity.text=String(dicPersist.object(forKey: "quantity") as! Int)
        cell.code.text=dicPersist.object(forKey: "itemcode") as? String
        
        return cell as preordersummaryTableViewCell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    func computetotal()
    {
        tot=0
        for i in 0..<PersistentArray.count
        {
           // let val = (listfromorder[i].prices)
            let dicpersist1 = PersistentArray[i] as! NSDictionary
            let val = dicpersist1.object(forKey: "price")as! Double
           
            let val1 = dicpersist1.object(forKey: "quantity")as! Double
           
            tot = tot + (Double(val) * Double(val1)) //Prices * Quantity
        }
        let num=NumberFormatter()
        num.numberStyle=NumberFormatter.Style.currency
        num.locale = NSLocale(localeIdentifier: "en_IN") as Locale!
        num.maximumFractionDigits=2
        str1 = num.string(from: NSNumber(value:tot))!
        
        Totallabel.text=String("\(str1)")
        
        if(LibraryAPI.sharedInstance.orderdetails.count>0)
        {
            LibraryAPI.sharedInstance.orderdetails.removeAllObjects()
        }
        
        for i in 0..<PersistentArray.count
        {
        let cartdict=NSMutableDictionary()
        let dicpersist2 = PersistentArray[i] as! NSDictionary
        cartdict.setObject(i+1, forKey: "Line_Number" as NSCopying)
            cartdict.setObject(dicpersist2.object(forKey: "itemcode")as! String, forKey: "Item_Code" as NSCopying)
        cartdict.setObject(dicpersist2.object(forKey: "quantity")as! Int, forKey: "Quantity" as NSCopying)
        cartdict.setObject(dicpersist2.object(forKey: "price")as! Double, forKey: "Unit_Pice" as NSCopying)
           
        LibraryAPI.sharedInstance.orderdetails.add(cartdict)
      
        }
    }

    
    @IBAction func menuacton(_ sender: AnyObject)
    {
        self.action()
    }
  
    func action()
    {
    }

    

    @IBAction func ProceedAction(_ sender: AnyObject)
    {
        
       let lvc = self.storyboard?.instantiateViewController(withIdentifier: "DeliveryViewController") as! DeliveryViewController
       self.navigationController?.pushViewController(lvc, animated: true)
        
    }
    
   

   

}
