//
//  CustomerSearchPinCodeViewController.swift
//  Redington
//
//  Created by TRUETECHMAC3 on 28/12/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
import Speech
import KCFloatingActionButton


class CustomerSearchPinCodeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,KCFloatingActionButtonDelegate,SFSpeechRecognizerDelegate {
    
    
    @IBOutlet var tableViewPinCode: UITableView!
    var fab=KCFloatingActionButton()
    var refreshControl = UIRefreshControl()
    let arr = NSMutableArray()
    var responseArray = NSMutableArray()
     var deliveryPageindex:Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customnavigation()
        self.layoutFAB()
        LibraryAPI.sharedInstance.BranchCodeCheck  = 4
        deliveryPageindex = 1
    }
    override func viewWillAppear(_ animated: Bool) {
        PinCode_Arr = [PinCodeSearch]()
        self.local_activityIndicator_start()
        LoadData(deliveryPageindex: deliveryPageindex)
        
    }
    
    func LoadData(deliveryPageindex: Int)
    {
        
        print("data in the pincode location :",LibraryAPI.sharedInstance.Pincode)
        DispatchQueue.global(qos: .background).async {
            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/CustomerSearchPincodewise?Pincode=\(LibraryAPI.sharedInstance.Pincode)&Userid=\(LibraryAPI.sharedInstance.Userid)&RecordNo=20&PageNo=\(self.deliveryPageindex)", completion:  { (jsonmutable) in
                 let  PinCodeArray_List = jsonmutable
                print("data in cust pin code:",PinCodeArray_List)
                DispatchQueue.main.async {
                    if(PinCodeArray_List.count > 0)
                    {
                        self.local_activityIndicator_stop()
                        for word in PinCodeArray_List
                        {
                            PinCode_Arr.append(PinCodeSearch(data: word as AnyObject))
                            PinCode_Arr1.append(PinCodeSearch(data: word as AnyObject))
                        }
                        self.tableViewPinCode.reloadData()
                         self.local_activityIndicator_stop()
                    }else{
                        self.local_activityIndicator_stop()
                        
                    }
                }
            })
            }
            }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
        
    }
    
    func layoutFAB() {
        fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "CustomerSearchPinCodeViewController")), animated: true)
            
            self.fab.close()
        }
        fab.fabDelegate = self
        
        self.view.addSubview(fab)
        
    }
    
    

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    //TableView Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if(PinCode_Arr.count > 0)
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PinCode_Arr.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell : CustomerSearchPinCodeTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as! CustomerSearchPinCodeTableViewCell
        
        if(PinCode_Arr.count > 0)
        {
            cell.cellview.layer.borderColor = UIColor(red:30/255.0, green:184/255.0, blue:120/255.0, alpha: 1.0).cgColor
            cell.cellview.layer.borderWidth = 1.0
            cell.cellview.layer.cornerRadius = 5.0
            cell.selectionStyle = .none
            cell.labelTitle.text = PinCode_Arr[indexPath.row].CUSTOMER_CODE+"-"+PinCode_Arr[indexPath.row].CUSTOMER_NAME
            cell.labelAd.text = "Address"
            cell.labelAddress.text = PinCode_Arr[indexPath.row].CUSTOMER_ADD1+"-"+PinCode_Arr[indexPath.row].CUSTOMER_ADD2+"-"+PinCode_Arr[indexPath.row].CUSTOMER_ADD3
            cell.labelCct.text = "City"
            cell.labelCity.text = PinCode_Arr[indexPath.row].CUSTOMER_ADD4
            cell.labelSt.text = "State"
            cell.labelPC.text = "Pincode"
            cell.labelState.text = PinCode_Arr[indexPath.row].CUSTOMER_ADD5
            cell.labelPinCOde.text = PinCode_Arr[indexPath.row].PINCODE
            
            
        }
        else
        {
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        LibraryAPI.sharedInstance.CustomerCode = PinCode_Arr[indexPath.row].CUSTOMER_CODE
        LibraryAPI.sharedInstance.tracker="2"
        LibraryAPI.sharedInstance.tracker1 = 1
        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "Dashboard360NewViewController") as! Dashboard360NewViewController
        self.navigationController?.pushViewController(lvc, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastitem  = PinCode_Arr.count - 1
        if indexPath.row == lastitem
        {
            deliveryPageindex = deliveryPageindex+1
            
            let lastpage = Int(PinCode_Arr[indexPath.row].TOTAL_PAGE)
            
            if(deliveryPageindex <= lastpage)
            {
                LoadData(deliveryPageindex: deliveryPageindex)
            }
            else
            {
                
            }
            
        }
        
    }
    
    func customnavigation()
    {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.pincode.setImage(UIImage(named:"pincode_search (1).png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(CustomerSearchPinCodeViewController.backbuttonClicked),for:UIControlEvents.touchUpInside)
        customView.pincode.addTarget(self, action: #selector(CustomerSearchPinCodeViewController.mymap),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        customView.HeaderLabel.text = "Customer Search(Pin Code)"
        self.view.addSubview(customView.view)
        
    }
    func backbuttonClicked(sender:UIButton)
    {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
    }
    
    func mymap()
    {
        let popup = PopupController
            .create(parentViewController: self.parent!)
            .show(childViewController: MapViewController.instance())
            
            .didShowHandler { popup in
            }
            .didCloseHandler { _ in
                self.LoadData(deliveryPageindex: self.deliveryPageindex)
        }
        let container = MapViewController.instance()
        container.closeHandler = { _ in
            self.LoadData(deliveryPageindex: self.deliveryPageindex)
            popup.dismiss()
        }
        
        
        popup.show(childViewController: container)
    }
    
    
}
