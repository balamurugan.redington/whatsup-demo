//
//  LeaveApprovalViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 27/07/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class LeaveApprovalViewController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var LeaveApprovalTable: UITableView!
    
    var responsestring = NSMutableArray()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        if LibraryAPI.sharedInstance.Globalflag == "N" {
            customnavigation1()
        } else {
            customnavigation()
        }
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        LeaveApprovalTable.register(UINib(nibName: "LeaveApprovalTableViewCell", bundle: nil), forCellReuseIdentifier: "LeaveApprovalTableViewCell")
        LeaveApprovalTable.tableFooterView = UIView(frame: .zero)
        LeaveApprovalTable.separatorStyle = .none
        uploaddata()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    func customnavigation() {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(LeaveApprovalViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Leave Approval"
    }
    
    func customnavigation1() {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(LeaveApprovalViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.textAlignment = .center
        customView.HeaderLabel.font = UIFont.boldSystemFont(ofSize: 16.0)
        customView.HeaderLabel.text = "Leave Approval"
    }
    
    func buttonClicked(sender:UIButton) {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "HRD1DetailsViewController")), animated: true)
    }
    func navigate() {
        let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "HRD1DetailsViewController") as! HRD1DetailsViewController
        self.navigationController?.pushViewController(lvc, animated: false)

    }

    func uploaddata() {
        self.local_activityIndicator_start()
         self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)LeaveDetails.asmx/LeaveApprovalDetails?UserID=\(LibraryAPI.sharedInstance.Userid)", completion: { (jsonmutable) in
            self.responsestring = jsonmutable
        if(self.responsestring.count > 0)
        {
            let dic = self.responsestring[0] as! NSDictionary
            if(dic.object(forKey: "Message") != nil) {
            let value = dic.object(forKey: "Message") as! String
            let alertController = UIAlertController(title: "Alert", message:value , preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
            self.navigate()
            }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)           }
            else {
            }
            self.local_activityIndicator_stop()
        }
        else
        {
            let alert = UIAlertController(title: "Sorry..!!", message: "No Data Available", preferredStyle: UIAlertControllerStyle.alert)
            let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                self.navigate()
                }
                alert.addAction(cancelAction)
                self.present(alert, animated: true, completion: nil)
             self.local_activityIndicator_stop()
        }
            })
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 41
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
       if (responsestring.count > 0) {
        return 1
        } else {
        return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(responsestring.count == 1)
        {
            let dic  = responsestring[0] as! NSDictionary
            if(dic.object(forKey: "Message") != nil)
            {
                return 0
            } else {
                return responsestring.count
            }
        } else {
            return responsestring.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeaveApprovalTableViewCell") as! LeaveApprovalTableViewCell
        if(indexPath.row % 2 == 0) {
            cell.contentView.backgroundColor=(UIColor.init(red: 240/255.0, green: 246/255.0, blue: 225/255.0, alpha: 1.0))
        } else {
            cell.contentView.backgroundColor=(UIColor.init(red: 215/255.0, green: 250/255.0, blue: 252/255.0, alpha: 1.0))
        }
        let dic = responsestring[indexPath.row] as! NSDictionary
        cell.leaveno.text = dic.object(forKey: "LEAVE APP NUMBER")as? String
        cell.empcode.text =  dic.object(forKey:"EMPLOY CODE")as? String
        cell.empname.text = dic.object(forKey:"EMPLOY NAME")as? String
        cell.empdesc.text = dic.object(forKey:"EMP DESIGNATION")as? String
        cell.noofdays.text = dic.object(forKey:"NO:OF DAYS")as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         LibraryAPI.sharedInstance.Leaveapprovalhrd = responsestring.object(at: indexPath.row) as! NSDictionary
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "LeaveApplicationViewController")), animated: true)
    }
}
