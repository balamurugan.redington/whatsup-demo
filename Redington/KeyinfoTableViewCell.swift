//
//  KeyinfoTableViewCell.swift
//  Whatsup
//
//  Created by truetech on 23/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
import Cosmos

class KeyinfoTableViewCell: UITableViewCell {
   
    @IBOutlet weak var Rateview: CosmosView!
    @IBOutlet var statusbtn: UIButton!
    @IBOutlet var spreadbtn: UIButton!

    @IBOutlet  var partner: UILabel!
    @IBOutlet  var exposuredate: UILabel!
   
    @IBOutlet  var lastpaymentvalue: UILabel!
   
    @IBOutlet weak var chequepending: UIButton!
   
    @IBOutlet  var lastpaymentdate: UILabel!
    @IBOutlet  var exposure: UILabel!
    @IBOutlet var updatebtn: UIButton!
    @IBOutlet var time: UILabel!
    @IBOutlet var date: UILabel!
    @IBOutlet var Userid: UILabel!
    @IBOutlet var Insurancelabel: UILabel!
    @IBOutlet var creditlimitlabel: UILabel!
    @IBOutlet var Productspreadlabel: UILabel!
    @IBOutlet var BusinessHealthLabel: UILabel!
    @IBOutlet var saleshealthlabel: UILabel!
    @IBOutlet var Likesales: UIImageView!
    @IBOutlet var DislikeMArgin: UIImageView!
    @IBOutlet var Marginrankinglabel: UILabel!
    @IBOutlet var salesrankinglabel: UILabel!
    @IBOutlet var Salelabel: UILabel!
    @IBOutlet var labelInvoceno: UILabel!
    @IBOutlet var labelInvocedate: UILabel!
    @IBOutlet var labelInvocevalue: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        spreadbtn.layer.cornerRadius=spreadbtn.frame.size.width/2
        updatebtn.layer.cornerRadius=spreadbtn.frame.size.width/2
        chequepending.layer.cornerRadius=spreadbtn.frame.size.width/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
