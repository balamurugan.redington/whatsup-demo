//
//  InvoiceSummaryViewController.swift
//  Redington
//
//  Created by truetech on 22/08/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import KCFloatingActionButton

class InvoiceSummaryViewController: UIViewController ,UIScrollViewDelegate,KCFloatingActionButtonDelegate, UITextFieldDelegate, UITextViewDelegate{
    
    @IBOutlet var backbtn: UIButton!
    @IBOutlet var menubtn: UIButton!
    @IBOutlet var AmountCommitedtf: UITextField!
    @IBOutlet var lastcommiteddate: UILabel!
   /* @IBOutlet var comittedbytf: UITextField!*/
      @IBOutlet var due: UILabel!
    @IBOutlet var CustomerCode: UILabel!
    @IBOutlet var textviewremarks: UITextView!
    @IBOutlet var MainVIew: UIView!
   /* @IBOutlet var valuetf: UITextField!*/
    @IBOutlet var Datetf: UITextField!
    @IBOutlet var DatePIcker: UIDatePicker!
    @IBOutlet var Cancelbtn: UIButton!
    @IBOutlet var Donebtn: UIButton!
    @IBOutlet var Pickerview: UIView!
    var CustomerName=String()
     var cc=String()
    var CommittedAmount=Double()
    var Balancelval=0.0
    var InvoiceVal=Double()
    var Invoicedate=String()
    var duedate=String()
    var Invoicenum=String()
    var overdueday=Int()
     @IBOutlet var SubmitAction: UIButton!
    @IBOutlet var InvoiceNumberLabel: UILabel!
    @IBOutlet var CustomerLabel: UILabel!
    @IBOutlet var BalanceDueLabel: UILabel!
    @IBOutlet var InoviceValueLabel: UILabel!
 
    @IBOutlet weak var overduedays: UILabel!
    @IBOutlet weak var totalcommitted: UILabel!
    @IBOutlet weak var duedatelabel: UILabel!
    @IBOutlet weak var InvoiceDateLabel: UILabel!
    var Temparray=NSMutableArray()
    var fab=KCFloatingActionButton()
    @IBOutlet var Scroll: UIScrollView!
    var uploadfile=NSMutableDictionary()
    var jsonresult=NSDictionary()
    var totalamnt=Double()
    
    var Movedup=Bool()
   

    @IBOutlet var Mainview: UIView!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        AmountCommitedtf.delegate = self
        self.layoutFAB()
         customnavigation()
        SubmitAction.layer.masksToBounds=true
        SubmitAction.layer.cornerRadius=20.0
        InvoiceNumberLabel.text=Invoicenum
        overduedays.text=String(overdueday)
        CustomerLabel.text=CustomerName
        CustomerCode.text=cc
        let num1=NumberFormatter()
        num1.numberStyle=NumberFormatter.Style.currency
        num1.locale = NSLocale(localeIdentifier: "en_IN") as Locale!
        let str1=num1.string(from: NSNumber(value:Balancelval))
        BalanceDueLabel.text=String(str1!)
       InvoiceDateLabel.text=Invoicedate
        duedatelabel.text=duedate
        InoviceValueLabel.text=String(InvoiceVal)
        totalcommitted.text=String(LibraryAPI.sharedInstance.commitedamount)
        lastcommiteddate.text=LibraryAPI.sharedInstance.lastcommitedamount
        /*let image = UIImage(named: "like.png")
         cell.DislikeMArgin.image = image!.imageWithColor(UIColor.init(red:30/255.0 , green: 184/255.0, blue: 120/255.0, alpha: 1.0)).imageWithRenderingMode(.AlwaysOriginal)*/
        
        let arrow = UIImageView()
        let image = UIImage(named: "cal.png")
        arrow.image = image!.imageWithColor(tintColor: UIColor.init(red:239/255.0 , green: 88/255.0, blue: 88/255.0, alpha: 1.0)).withRenderingMode(.alwaysOriginal)
        arrow.frame = CGRect(x: 0.0,y:  0.0,width: arrow.image!.size.width+10.0,height: 15);
        arrow.contentMode = UIViewContentMode.center
        self.Datetf.rightView = arrow;
        self.Datetf.rightViewMode = UITextFieldViewMode.always
        let todayDate = NSDate()
        DatePIcker.minimumDate = todayDate as Date
       
        Movedup=false
        NotificationCenter.default.addObserver(self, selector: #selector(InvoiceSummaryViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(InvoiceSummaryViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        let screenBounds = UIScreen.main.bounds
        let height = screenBounds.height
        
        if(height == 480)
        {
          
            let scroll=UIScrollView()
            scroll.delegate=self
            scroll.isScrollEnabled=true;
            scroll.showsVerticalScrollIndicator=false
            
        }
        
        AmountCommitedtf.tag=20
        AmountCommitedtf.keyboardType = .decimalPad
        
        AmountCommitedtf.addTarget(self, action: #selector(InvoiceSummaryViewController.textFieldDidBeginEditing), for: UIControlEvents.editingChanged)
        
        textviewremarks.layer.cornerRadius = 15.0
        textviewremarks.layer.borderWidth = 1.0
        self.textviewremarks.layer.borderColor = UIColor(red:211/255.0, green:211/255.0, blue:211/255.0, alpha: 1.0).cgColor
        
        self.addDoneButtonOnKeyboard()

    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
       
    }
    
    func addDoneButtonOnKeyboard()
        
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,width: 320,height: 50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(InvoiceSummaryViewController.doneButtonAction))
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as AnyObject as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
    
        AmountCommitedtf.inputAccessoryView = doneToolbar
    
    }
    
    func doneButtonAction()
    {
        AmountCommitedtf.resignFirstResponder()
    }
    
    override func viewDidLayoutSubviews() {
        
       
    }
    
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            
            if (self.navigationController?.topViewController!.isKind(of: DashBoardViewController.classForCoder()) != nil) {
              
                self.navigationController?.popToViewController((self.navigationController?.viewControllers.first)!, animated: true)
            }
            else
            {
                let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                self.navigationController?.pushViewController(lvc, animated: false)
                
            }
            self.fab.close()
        }
        fab.fabDelegate = self
        
        self.view.addSubview(fab)
        
    }
   
       func keyboardWillShow(notification: NSNotification)
    {
        
        if(Movedup==false)
        {
            self.view.frame.origin.y -= 100
            Movedup=true
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        self.view.frame.origin.y = 0
        Movedup=false
    }

    
    @IBAction func Submitaction(_ sender: AnyObject)
    {

       if((self.Datetf.text!.isEmpty || self.AmountCommitedtf.text!.isEmpty ||  self.textviewremarks.text!.isEmpty))
        {
            self.NormalAlert(title: "Alert", Messsage: "Kindly enter all the values")
        }
        else
       {
       let myInt: Double = Double(AmountCommitedtf.text!)!
        totalamnt=myInt
        let str1=Double(Balancelval)
        if(totalamnt>str1)
        {
            self.NormalAlert(title: "Alert", Messsage: "Expected amount is greater than OverDue")
        }
            else if(totalamnt == 0)
        {
            self.NormalAlert(title: "Alert", Messsage: "Please Enter the Valid Amount")
        }
        else
        {
        
        self.setdateandtime()
         uploadfile.setObject(LibraryAPI.sharedInstance.Userid, forKey: "UserID" as NSCopying)
         uploadfile.setObject(Invoicenum, forKey: "InvoiceNumber" as NSCopying)
         uploadfile.setObject(overdueday, forKey: "OverDueDays" as NSCopying)
         uploadfile.setObject(cc, forKey: "CustomerCode" as NSCopying)
         uploadfile.setObject(Datetf.text!, forKey: "ExpectTofCollect" as NSCopying)
         uploadfile.setObject(AmountCommitedtf.text!, forKey: "Expectedvalue" as NSCopying)
         uploadfile.setObject(textviewremarks.text!, forKey: "Remarks" as NSCopying)
         let size = CGSize(width: 30, height:30)
            
        self.local_activityIndicator_start()
            
            
            // new version dispatch Que
            
            DispatchQueue.global(qos: .background).async {
                
                // Validate user input
                self.uploaddata()

                
                // Go back to the main thread to update the UI
                DispatchQueue.main.async {
                    
                   self.local_activityIndicator_stop()
                    if(self.jsonresult.object(forKey: "Status") as? String  == "Success")
                    {
                        let alertController = UIAlertController(title: "Alert", message:"Submitted Successfully" , preferredStyle: .alert)
                        
                        let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                            
                            
                            for controller in self.navigationController!.viewControllers as Array {
                                if controller.isKind(of: DashBoardViewController.classForCoder()) {
                                    LibraryAPI.sharedInstance.tracker="1"
                                    self.navigationController?.popToViewController(controller as UIViewController, animated: true)
                                    break
                                }
                            }
                            
                        }
                        alertController.addAction(cancelAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                    else
                        
                    {
                        self.NormalAlert(title: "Alert", Messsage: "Submission Failed")
                       /* let alertController = UIAlertController(title: "Alert", message:"Submission Failed" , preferredStyle: .Alert)
                        let cancelAction = UIAlertAction(title: "Ok", style: .Cancel) { (action:UIAlertAction!) in
                        }
                        alertController.addAction(cancelAction)
                        self.presentViewController(alertController, animated: true, completion:nil)*/
                    }

                }
            }
            
            // new version dispatch Que
            
        
        
        
        dismiss(animated: true, completion: nil)
        }
        }
    }
    
   
    func setdateandtime()
    {
        let date = NSDate()
        let calendar = NSCalendar.current
       // let components = calendar.components([ .Hour, .Minute, .Second], fromDate: date)
        let components = calendar.dateComponents([.hour, .minute, .second], from: date as Date)
        let hour = components.hour
        let minutes = components.minute
        
        let formattedhour=String(describing: hour)
        let formattedtime=String(describing: minutes)
        //let originaltime=formattedhour.stringByAppendingString(":\(formattedtime)")
       let originaltime=formattedhour.appending(":\(formattedtime)")
        
        
        let dateMakerFormatter1 = DateFormatter()
        dateMakerFormatter1.dateFormat = "HH:mm"
       // let startTime = dateMakerFormatter1.date(from: originaltime)!
        //let strtime = dateMakerFormatter1.stringFromDate(startTime)
       // let strtime = dateMakerFormatter1.string(from: startTime)
        //this comes back as 12:40 am not pm
        
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "MM/dd/yy"
        let strDate = dateFormatter.string(from: date as Date)
        
        //let str=strDate.componentsSeparatedByString(" ")
     
        //let str=strDate.components(separatedBy: "")
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateStyle = DateFormatter.Style.long
        dateFormatter1.dateFormat = "YYYY/MM/dd"
        let strDate2 = dateFormatter1.string(from: date as Date)
        
        //let dateFormatter2 = DateFormatter()
        dateFormatter1.dateStyle = DateFormatter.Style.long
        dateFormatter1.dateFormat = "YYYY/MM/dd"
       // let strDate3 = dateFormatter1.string(from: date as Date)
        
        //let str1=strDate2.componentsSeparatedByString(" ")
       // let str1=strDate2.components(separatedBy: "")
        self.uploadfile.setObject(LibraryAPI.sharedInstance.Userid, forKey: "USERUPDATE" as NSCopying)
        self.uploadfile.setObject(strDate, forKey: "DATEUPDATE" as NSCopying)
        self.uploadfile.setObject(strDate2, forKey: "InvoiceDate" as NSCopying)
    }

    
  func uploaddata()
  {
    let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)OrderStatus.asmx/CustomerOutstandingCollection")! as URL)
    request.httpMethod = "POST"
    request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
    request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
    request.httpBody = try? JSONSerialization.data(withJSONObject: self.uploadfile, options: JSONSerialization.WritingOptions(rawValue: 0))
    let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
    //  var err: NSError
    do
    {
    let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
    jsonresult = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
    
    
    }
    catch (let e)
    {
    
    
    }

    }
    func customnavigation()
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y, width: self.view.frame.width, height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(InvoiceSummaryViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)   
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Summary-Commitment"
        
    }
    
    func buttonClicked(sender:UIButton)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: InvoiceViewController.classForCoder()) {
                self.navigationController?.popToViewController(controller as UIViewController, animated: true)
                break
            }
        }
    }
    
    @IBAction func Doneaction(_ sender: AnyObject)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "YYYY-MM-dd"

        let strDate = dateFormatter.string(from: DatePIcker.date)
        Datetf.text=strDate
        Pickerview.isHidden=true;
    }
    
    
    @IBAction func CancelAction(_ sender: AnyObject)
    {
        Pickerview.isHidden=true;
    }

    
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
     
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
     
        if(textField.tag==2)
        {
            Pickerview.isHidden=false
            Datetf.resignFirstResponder()
            return false
            
        }
        
        
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
        if(textField.tag==1)
        {
           // valuetf.resignFirstResponder()
            
        }
        
        if(textField.tag==20)
        {
            AmountCommitedtf.resignFirstResponder()
        }
       
        
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
      if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
  
        if(textField.tag == 20)
        {
            let oldString = textField.text?.count == 0 ? string  : textField.text!
            let i = oldString.count
            let range = oldString.index(oldString.startIndex, offsetBy: i-1) ..< oldString.index(oldString.startIndex, offsetBy: i)
            //let candidate = oldString.replacingCharacters(in: range, with: String)
            let candidate = oldString.replacingCharacters(in: range, with: string)
            let regex = try? NSRegularExpression(pattern: "^\\d{0,8}(\\.\\d{0,2})?$", options: [])
            return regex?.firstMatch(in: candidate, options: [], range: NSRange(location: 0, length: candidate.count)) != nil
        }
       
        return true
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
     
        if textField == AmountCommitedtf
        {
                if(AmountCommitedtf.text == ".")
                {
                    AmountCommitedtf.text = "0."
                    
                }
                else if(AmountCommitedtf.text == "0..")
                {
                    AmountCommitedtf.text = "0."
                }
                else
                {
                    let amount : Int
                    if AmountCommitedtf.text == ""{
                        amount = 0
                    }else{
                        amount = Int(AmountCommitedtf.text!)!
                    }
                        let availableamt = Int(Balancelval)
                        print("data in amount and availamt:",amount,"-",availableamt)
                    if amount <= availableamt
                        {
                        
                        }
                        else
                        {
                        let alertController = UIAlertController(title: "Alert", message:"please fill below or equel overdue amount" , preferredStyle: .alert)
                        
                        let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                            self.AmountCommitedtf.text = ""
                        }
                        alertController.addAction(cancelAction)
                        self.present(alertController, animated: true, completion:nil)
                 }
                  
                }
        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    

  
}
