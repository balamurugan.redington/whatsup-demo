//
//  PreOrderViewController.swift
//  Redington
//
//  Created by truetech on 25/08/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import CoreData
import KCFloatingActionButton

struct Element {
    var Quantities: String
    var Productnames: String
    var Invoicedates: String
    var vendor:String
    var prices: Float
    var availabilty:Int
    var Stockroom:String
}

struct searchElement {
    var availabilities:Int
    var Productnames: String
    var Invoicedates: String
    var vendor:String
    var prices: Float
}

protocol ChildViewControllerDelegate
{
    func childViewControllerResponse(textfromcart:[String:String])
}


class PreOrderViewController: UIViewController,KCFloatingActionButtonDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate{
    @IBOutlet weak var PickerView: UIView!
    @IBOutlet weak var Picker: UIPickerView!
    @IBOutlet weak var Stockroom: UITextField!
    @IBOutlet weak var Product: UITextField!
    @IBOutlet weak var normalbtn: UIButton!
    @IBOutlet weak var b2bbtn: UIButton!
    @IBOutlet weak var NoRecordBtn: UILabel!
    @IBOutlet weak var FilteBtn: UIButton!
    @IBOutlet weak var ordersegmentview: UISegmentedControl!
    var delegate: ChildViewControllerDelegate?
    @IBOutlet var cartbtn: UIButton!
    @IBOutlet var backbtn: UIButton!
    @IBOutlet var menubtn: UIButton!
    @IBOutlet var Searchbar: UISearchBar!
    @IBOutlet var badgelabel: UILabel!
    @IBOutlet var cartbadge: UIView!
    @IBOutlet var Cartbutton: UIButton!
    
    var texts = [Int:String]()
    var Newtexts = [String:String]()
    var Buttontitle = [Int:String]()
    var searchActive=Bool()
    var searchtext=String()
    var searcharray=NSMutableArray()
    var dict=NSMutableDictionary()
    var text = String()
    var availability = Int()
    var price=Float()
    var vendorname=String()
    var code=String()
    var searchelementarray=[searchElement]()
    var fab = KCFloatingActionButton()
    var NewResponsestring=NSMutableArray()
    var Pricearray=[String:String]()
    var set = NSCharacterSet()
    var isdotentered=Bool()
    var proceedarray=NSMutableArray()
    @IBOutlet weak var Submitbtn: UIButton!
    @IBOutlet var QuantityTableview: UITableView!
    
    var responseJSONstring=NSMutableArray()
    var newSearchArray=NSMutableArray()
    
    @IBOutlet var proceedbtn: UIButton!
    
    var list = [Element]()
    var issegemntClicked=Bool()
    
    @IBOutlet weak var popupview: UIView!
    
    var selectedrow = Int()
    var pickerarray = NSMutableArray()
    var responsearray = NSMutableArray()
    var Itemcodearray = NSMutableArray()
    var stckroomval : String = ""
    var isb2b = Bool()
    var Movedup=Bool()
    var deliveryPageindex:Int = 1
    var deliveryPageindex1 = 1
    var Normalorder = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.DeletedatafromDb()
        issegemntClicked=false
        LibraryAPI.sharedInstance.isvalueloaded=false
        isdotentered=false
        badgelabel.text=String(list.count)
        // self.Loaddata()
        self.layoutFAB()
        fab.isHidden=true
        FilteBtn.isHidden = true
        Searchbar.isHidden = true
        searchActive=false
        Picker.delegate=self
        let cnt = FetchCount()
        self.fetchdbvalues()
        isb2b=false
        NoRecordBtn.isHidden = true
        LibraryAPI.sharedInstance.badgeCount=String(cnt)
        badgelabel.text = LibraryAPI.sharedInstance.badgeCount
        backbtn.addTarget(self, action: #selector(PreOrderViewController.backaction),for:UIControlEvents.touchUpInside)
        self.normalbtn.backgroundColor = CustomColor.ShareColor.hexStringToUIColor(hex: CustomColor.ShareColor.CustomGreen, alpha: 1.0)
        self.b2bbtn.backgroundColor = UIColor.white
        NotificationCenter.default.addObserver(self, selector: #selector(PreOrderViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(PreOrderViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        deliveryPageindex = 1
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
         self.fetchdbvalues()
        Preorder_Arr = [Preorder]()
        self.Normalorder = 1
        self.local_activityIndicator_start()
        self.PreorderLoaddata(deliveryPageindex: self.deliveryPageindex)
        
    }
    
    
    
    func FetchCountforStock()->Int
    {
        do {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let managedContext = appDelegate.managedObjectContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
            let entityDescription = NSEntityDescription.entity(forEntityName: "CartTable", in: managedContext)
            fetchRequest.entity = entityDescription
            fetchRequest.returnsObjectsAsFaults = false
            let count = try managedContext.count(for: fetchRequest)
            return count
        } catch let error as NSError {
            return 0
        }
    }
    
    
    func FetchCount()->Int
    {
        do {
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let managedContext = appDelegate.managedObjectContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
            let entityDescription = NSEntityDescription.entity(forEntityName: "CartTable", in: managedContext)
            fetchRequest.entity = entityDescription
            fetchRequest.returnsObjectsAsFaults = false
            do {
                let count = try managedContext.fetch(fetchRequest) as! [NSManagedObject]
                if(Itemcodearray.count>0)
                {
                    Itemcodearray.removeAllObjects()
                }
                for cou in count
                {
                    let str = cou.value(forKey: "itemcode") as! String
                     let stock  = cou.value(forKey: "stock") as! String
                    if Itemcodearray.contains(str)
                    {
                        
                    }
                    else
                    {
                        Itemcodearray.add(str)
                        stckroomval = stock

                    }
                    
                }
                for Colour in 0..<Itemcodearray.count
                {
                    let str = Itemcodearray[Colour] as! String
                    if (LibraryAPI.sharedInstance.Colourtexts.value(forKey: str) as? String) != nil // [Itemcodearray[Colour] as! String]
                    {
                        LibraryAPI.sharedInstance.Colourtexts.setValue("GreenColor", forKey: str)
                    }
                    else
                    {
                    }
                }
                return count.count
            }
            catch {
                let fetchError = error as NSError
            }
        } catch let error as NSError {
            return 0
        }
        return Itemcodearray.count
    }
    
    
    override func viewDidLayoutSubviews()
    {
        cartbadge.layer.cornerRadius=self.cartbadge.frame.size.width/2
        Submitbtn.layer.cornerRadius=10
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NewResponsestring =  responseJSONstring
        
    }
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            LibraryAPI.sharedInstance.temptexorder.removeAll()
            LibraryAPI.sharedInstance.PriceValue.removeAll()
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            
            self.fab.close()
        }
        
        fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
            
            
            self.refresh()
            
            self.fab.close()
        }
        
        
        fab.fabDelegate = self
        
        self.view.addSubview(fab)
        
    }
    func KCFABOpened(fab: KCFloatingActionButton) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        if(LibraryAPI.sharedInstance.lastviewcontroller=="Cartviewcontoller")
        {
            
            if(LibraryAPI.sharedInstance.temptexorder.count>0)
            {
                list.removeAll()
                Newtexts.removeAll()
                list=LibraryAPI.sharedInstance.globallist
                Pricearray.removeAll()
                
                self.fetchdbvalues()
                badgelabel.text=LibraryAPI.sharedInstance.badgeCount
                QuantityTableview.reloadData()
                
            }
            else
            {
                texts.removeAll()
                list.removeAll()
                badgelabel.text=LibraryAPI.sharedInstance.badgeCount
                Pricearray.removeAll()
                
                Newtexts.removeAll()
                Buttontitle.removeAll()
                self.fetchdbvalues()
                QuantityTableview.reloadData()
                
            }
        }
        else if(LibraryAPI.sharedInstance.lastviewcontroller=="Dashboardviewcontroller")
        {
            badgelabel.text = LibraryAPI.sharedInstance.badgeCount
            self.fetchdbvalues()
        }
        else
        {
        }
        
        
    }
    @IBAction func NormalAction(_ sender: AnyObject) {
        let val = FetchCountforStock()
        if(val>0)
        {
            issegemntClicked=true
        }
        else
        {
            issegemntClicked=false
        }
        if(issegemntClicked==false)
        {
            view.endEditing(true)
            isb2b=false
            searchActive=false
            Searchbar.resignFirstResponder()
            Searchbar.text=""
            self.normalbtn.backgroundColor = CustomColor.ShareColor.hexStringToUIColor(hex: CustomColor.ShareColor.CustomGreen, alpha: 1.0)
            self.b2bbtn.backgroundColor = UIColor.white
            FilteBtn.alpha = 0
            FilteBtn.isHidden = true
            self.closePopup()
            responseJSONstring.removeAllObjects()
            QuantityTableview.isHidden=false
            let size = CGSize(width: 30, height:30)
            self.local_activityIndicator_start()

            
            DispatchQueue.global(qos: .background).async {
                
                // Validate user input
                self.FilteBtn.isHidden = true
                self.PreorderLoaddata(deliveryPageindex: self.deliveryPageindex)
               
                
                // Go back to the main thread to update the UI
                DispatchQueue.main.async {
                 
                    
                }
            }
            
            // new version dispatch Que
           
            UIView.animate(withDuration: 0.7) {
                self.FilteBtn.alpha = 1
            }
        }
        else
        {
            let alert = UIAlertController(title:"Alert", message: "Order can be placed either with Normal (or) Back to back!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:
                { (ACTION :UIAlertAction!)in
                    
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func b2bAction(_ sender: AnyObject) {
        let val = FetchCountforStock()
        if(val>0)
        {
            issegemntClicked=true
        }
        else
        {
            issegemntClicked=false
        }
        if(issegemntClicked==false)
        {
            view.endEditing(true)
            Preorder_Arr.removeAll()
            searchActive=false
            Searchbar.resignFirstResponder()
            Searchbar.text=""
            isb2b=true
            self.b2bbtn.backgroundColor = CustomColor.ShareColor.hexStringToUIColor(hex: CustomColor.ShareColor.CustomGreen, alpha: 1.0)
            self.normalbtn.backgroundColor = UIColor.white
            FilteBtn.alpha = 0
            FilteBtn.isHidden = false
            self.QuantityTableview.isHidden=true
            UIView.animate(withDuration: 0.7) {
                self.FilteBtn.alpha = 1
            }
        }
        else
        {
            let alert = UIAlertController(title:"Alert", message: "Order can be placed either with Normal (or) Back to back!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:
                { (ACTION :UIAlertAction!)in
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    
    
    func backaction(sender:UIButton)
    {
        let val = FetchCountforStock()
        if(val>0)
        {
            issegemntClicked=true
        }
        else
        {
            issegemntClicked=false
        }
        if(issegemntClicked==false)
        {
            self.view.endEditing(true)
            LibraryAPI.sharedInstance.temptexorder.removeAll()
            self.DeletedatafromDb()
            LibraryAPI.sharedInstance.Colourtexts.removeAllObjects()
            self.navigate()
        }
        else{
            let alert = UIAlertController(title:"Alert", message: "Are you sure you want to cancel the order?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler:
                { (ACTION :UIAlertAction!)in
                    self.view.endEditing(true)
                    LibraryAPI.sharedInstance.temptexorder.removeAll()
                    self.DeletedatafromDb()
                    LibraryAPI.sharedInstance.Colourtexts.removeAllObjects()
                    self.navigate()
                    
            }))
            
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler:
                { (ACTION :UIAlertAction!)in
                    
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    func DeletedatafromDb()
    {
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let context:NSManagedObjectContext = appDel.managedObjectContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CartTable")
        
        let entityDescription = NSEntityDescription.entity(forEntityName: "CartTable", in:context)
        
        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        
        fetchRequest.returnsObjectsAsFaults = false
        
        //        let resultPredicate2 = NSPredicate(format: "itemcode = %@", "XIMP0004")
        
        
        
        do {
            let result = try context.fetch(fetchRequest)  as! [NSManagedObject]
            
            if(result.count>0)
            {
                
                for r in result
                {
                    context.delete(r)
                }
                
                try!context.save()
                
            }
            else
            {
            }
            
        } catch {
            let fetchError = error as NSError
        }
        
        // self.fetchdbvalues()
    }
    
    func LoadRefreshdata()
    {
       // responseJSONstring = LibraryAPI.sharedInstance.RequestUrl(url: "\(LibraryAPI.sharedInstance.GlobalUrl)orderEntry.asmx/ListItemcode?CustomerCode=\(LibraryAPI.sharedInstance.CustomerCode)")
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)orderEntry.asmx/ListItemcode?CustomerCode=\(LibraryAPI.sharedInstance.CustomerCode)", completion:  { (jsonmutable) in
            self.responseJSONstring = jsonmutable
            if(self.responseJSONstring.count>0)
        {
            self.NoRecordBtn.isHidden = true
        }else{
            self.NoRecordBtn.isHidden = false
        }
        })
    }
    
    func refresh()
    {
        self.local_activityIndicator_start()
        
        // new version dispatch Que
        
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
             self.LoadRefreshdata()
            
            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
                
               self.local_activityIndicator_stop()
                self.QuantityTableview.reloadData()
                
            }
        }
        
        // new version dispatch Que
        
        
    }
    
    
    func Nam1BarBtnKlkFnc(BtnPsgVar: UIBarButtonItem)
    {
        
    }
    
    func Nam2BarBtnKlkFnc(BtnPsgVar: UIBarButtonItem)
    {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cartAction(_ sender: AnyObject)
    {
        let val=Int(LibraryAPI.sharedInstance.badgeCount)
        if(val!>0)
        {
            let lvc=self.storyboard?.instantiateViewController(withIdentifier: "CartViewController") as! CartViewController
            
            // self.navigationController?.pushViewController(lvc, animated: true)
            
            if(list.count>0)
            {
                lvc.listfromorder=list
                
                
                
                self.UpdateDb()
                let cnt = FetchCount()
                
                badgelabel.text=String(cnt)
            }
            else
            {
            }
            //lvc.texts=LibraryAPI.sharedInstance.texts
            lvc.newtexts=Newtexts
            lvc.isbacktobackmode=isb2b
            LibraryAPI.sharedInstance.lastviewcontroller="Cartviewcontoller"
            LibraryAPI.sharedInstance.texts.removeAll()
            self.navigationController?.pushViewController(lvc, animated: true)
            //sideMenuViewController?.contentViewController = lvc
            // sideMenuViewController?.hideMenuViewController()
        }
        else
        {
            self.Globalalert(title: "oops", Message: "Cart is empty")
        }
        
    }
    
    
    func UpdateDb()
    {
        
        for i in 0..<list.count
        {
            
            let str=list[i].vendor
            
            let val = Double(list[i].prices)
            
            
            let val1 = Int(list[i].Quantities)
            
            
            
            let prd=list[i].Productnames
            
            let vnd=list[i].Invoicedates
            
            let avl=list[i].availabilty
            
            let Stk=list[i].Stockroom
            
            if(isb2b==false)
            {
                LibraryAPI.sharedInstance.Stockroom=list[i].Stockroom
            }
            var stk=LibraryAPI.sharedInstance.Stockroom
            //stk=stk.stringByReplacingOccurrencesOfString("SR:", withString: "")
            stk=stk.replacingOccurrences(of: "SR:", with: "")
            LibraryAPI.sharedInstance.Stockroom=stk
            
            self.SaveDatatoSQlite(val: str, price: val, Qnty: val1! , products: prd, vendor:vnd,available: avl,Stock: Stk)
        }
        
    }
    
    
    
    func SaveDatatoSQlite(val:String,price:Double,Qnty:Int,products:String,vendor:String,available:Int,Stock:String)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CartTable")
        
        let entityDescription = NSEntityDescription.entity(forEntityName: "CartTable", in:managedContext)
        
        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        
        fetchRequest.returnsObjectsAsFaults = false
        
        //        let resultPredicate2 = NSPredicate(format: "itemcode = %@", "XIMP0004")
        fetchRequest.predicate=NSPredicate(format: "itemcode == %@", "\(val)")
        
        do {
            
            let result = try managedContext.fetch(fetchRequest) as! [NSManagedObject]
            
            
            
            
            if(result.count>0)
            {
                // newPerson.setValue(listfromorder[i].vendor, forKey: "itemcode")
                
                result[0].setValue(price, forKey: "price")
                
                result[0].setValue(products, forKey: "products")
                
                result[0].setValue(Qnty , forKey: "quantity")
                
                result[0].setValue(vendor, forKey: "vendor")
                
                result[0].setValue(available, forKey: "availability")
                
                result[0].setValue(Stock, forKey: "stock")
                
                
                
                do {
                    
                    try result[0].managedObjectContext?.save()
                    
                } catch {
                    
                    
                }
            }
            else
            {
                
                
                
                let newPerson = NSManagedObject(entity: entityDescription!, insertInto: managedContext)
                // let newPerson=NSEntityDescription.insertNewObjectForEntityForName("CartTable", inManagedObjectContext: managedContext) as! CartTable
                newPerson.setValue(val, forKey: "itemcode")
                newPerson.setValue(price, forKey: "price")
                newPerson.setValue(products, forKey: "products")
                newPerson.setValue(Qnty, forKey: "quantity")
                newPerson.setValue(vendor, forKey: "vendor")
                newPerson.setValue(available, forKey: "availability")
                
                newPerson.setValue(Stock, forKeyPath: "stock")
                
                
                let dict = NSMutableDictionary()
                dict.setValue(val, forKeyPath: "itemcode")
                dict.setValue(price, forKeyPath: "price")
                dict.setValue(products, forKeyPath: "products")
                dict.setValue(Qnty, forKeyPath: "quantity")
                dict.setValue(vendor, forKeyPath: "vendor")
                dict.setValue(available, forKeyPath: "availability")
                
                dict.setValue(Stock, forKeyPath: "stock")
                
                
                
                let cnt =  self.FetchCount()
                
                if(cnt>0)
                {
                    issegemntClicked=true
                    LibraryAPI.sharedInstance.isvalueloaded=true
                    
                }
                
                
                
                do {
                    
                    try newPerson.managedObjectContext?.save()
                    
                } catch {
                    
                    
                }
                
            }
            
            
        } catch {
            
            let fetchError = error as NSError
            
            
        }
        
        do {
            
            _ = try managedContext.fetch(fetchRequest)
            
            
            
            // Where Locations = your NSManaged Class
            
            
            // Then you can use your properties.
            
            
        } catch {
            
            let fetchError = error as NSError
            
            
        }
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
      
        if(Preorder_Arr.count > 0)
        {
            return 1
        }
        else
            
        {
            //self.NoRecordBtn.hidden = false
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        var num=Int()
        if(searchActive==true)
        {
            num=newSearchArray.count
            
        }
        else
        {
            num=Preorder_Arr.count
        }
        
        
        return num
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        var cell : OrderTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as! OrderTableViewCell
        if(cell == nil)
        {
            cell = Bundle.main.loadNibNamed("Cell", owner: self, options: nil)![0] as! OrderTableViewCell;
            
        }
        
        cell.addDoneButtonOnKeyboard()
        cell.addDoneButtonOnKeyboardPrice()
        cell.PriceTf.keyboardType = .numberPad
        cell.Quantitytf.delegate = self
        cell.Quantitytf.tag = indexPath.row
        cell.PriceTf.delegate = self
        cell.PriceTf.tag = indexPath.row
        cell.selectionStyle = .none
        cell.Cartbtn.tag = indexPath.row
        cell.Cartbtn.addTarget(self, action: #selector(PreOrderViewController.buttonClicked), for: UIControlEvents.touchUpInside)
        cell.selectionStyle = .none
        if(searchActive==true)
        {
            let dic = newSearchArray[indexPath.row] as! NSDictionary
            cell.available.text=String(dic.object(forKey: "AVAILQTY") as! Int)
            let num=NumberFormatter()
            num.numberStyle=NumberFormatter.Style.currency
            num.locale = NSLocale(localeIdentifier: "en_IN") as Locale!
            
            let str=num.string(from: NSNumber(value:(dic.object(forKey: "UNITPRIC") as! Float)))
            
            cell.Cartbtn.setImage(UIImage(named:"caart.png"), for:UIControlState.normal)
            
            cell.price.text=str
            cell.itemcode.text=dic.object(forKey:  "ITEMCODE") as? String
            let stockrooom = (dic.object(forKey: "STKROOM") as? String)
            cell.StockLabel.text="SR:"+stockrooom!
           
            if(Newtexts.keys.contains(cell.itemcode.text!) && stckroomval == cell.StockLabel.text)
            {
                cell.Quantitytf.layer.borderWidth = 1
                cell.Quantitytf.layer.borderColor = CustomColor.ShareColor.hexStringToUIColor(hex: CustomColor.ShareColor.customBlack, alpha: 0.7).cgColor
                cell.Quantitytf.text=Newtexts[cell.itemcode.text!]
                
                
            }
            else
            {
                cell.Quantitytf.layer.borderWidth = 1
                cell.Quantitytf.layer.borderColor = CustomColor.ShareColor.hexStringToUIColor(hex: CustomColor.ShareColor.customBlack, alpha: 0.7).cgColor
                cell.Quantitytf.text=""
                
                
            }
            
            
            if(Pricearray.keys.contains(cell.itemcode.text!) && stckroomval == cell.StockLabel.text)
            {
                cell.PriceTf.layer.borderWidth = 1
                cell.PriceTf.layer.borderColor = CustomColor.ShareColor.hexStringToUIColor(hex: CustomColor.ShareColor.customBlack, alpha: 0.7).cgColor
                cell.PriceTf.text=Pricearray[cell.itemcode.text!]
                
            }
            else
            {
                cell.PriceTf.layer.borderWidth = 1
                cell.PriceTf.layer.borderColor = CustomColor.ShareColor.hexStringToUIColor(hex: CustomColor.ShareColor.customBlack, alpha: 0.7).cgColor
                cell.PriceTf.text=""
                
            }
            
            let val = LibraryAPI.sharedInstance.Colourtexts.value(forKey: cell.itemcode.text!) as! String
            if(val == "RedColor")
            {
                
                cell.Cartbtn.setImage(UIImage(named:"caart.png"), for:UIControlState.normal)
                cell.Quantitytf.isUserInteractionEnabled = true
                cell.PriceTf.isUserInteractionEnabled = true
                cell.Cartbtn.isUserInteractionEnabled=true
            }
            else
            {
                
                cell.Cartbtn.setImage(UIImage(named:"cartgreen.png"), for:UIControlState.normal)
                cell.Quantitytf.isUserInteractionEnabled = false
                cell.PriceTf.isUserInteractionEnabled = false
                cell.Cartbtn.isUserInteractionEnabled=false
            }
            
            
            
            cell.Invdates.text=dic.object(forKey: "VNDRCODE") as? String
            // cell.productnames.text=newSearchArray.objectAtIndex(indexPath.row).objectForKey("ITEMDESC") as? String
            
            
            let paraStyle = NSMutableParagraphStyle()
            paraStyle.firstLineHeadIndent = 15.0
            paraStyle.paragraphSpacingBefore = 10.0
            
            var para = NSMutableAttributedString()
            para =  NSMutableAttributedString(string:(dic.object(forKey: "ITEMDESC") as? String)!)
            // Apply paragraph styles to paragraph
            
            
            
            do {
                let regex = try NSRegularExpression(pattern: searchtext, options: NSRegularExpression.Options.caseInsensitive )
                let nsstr = dic.object(forKey: "ITEMDESC") as? NSString
                text=nsstr! as String
                let all = NSRange(location: 0, length: nsstr!.length)
                var matches : [String] = [String]()
                regex.enumerateMatches(in: text, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: all) {
                    (result : NSTextCheckingResult?, _, _) in
                    
                    if let r = result {
                        let results = nsstr!.substring(with: r.range) as String
                        
                        matches.append(results)
                        let substringrange=result!.rangeAt(0)
                        
                        
                        para.addAttribute(NSForegroundColorAttributeName, value:UIColor.init(red: 73/255.0, green: 206/255.0, blue: 157/255.0, alpha: 1.0), range: substringrange)
                        cell.productnames.attributedText=para
                    }
                }
                
                
            } catch {
                
            }
        }
        else
        {
            //cell.available.text=String(describing: Preorder_Arr[indexPath.row].AVAILQTY)
            cell.available.text = String(Int(Preorder_Arr[indexPath.row].AVAILQTY))
            let num=NumberFormatter()
            num.numberStyle=NumberFormatter.Style.currency
            num.locale = NSLocale(localeIdentifier: "en_IN") as Locale!
            if(isb2b==true)
            {
                cell.available.isHidden=true
                cell.Availablelabel.isHidden = true
            }
            else
            {
                cell.available.isHidden=false
                cell.Availablelabel.isHidden = false
            }
            cell.Cartbtn.setImage(UIImage(named:"caart.png"), for:UIControlState.normal)
            cell.price.text=String(Preorder_Arr[indexPath.row].UNITPRIC)
            cell.itemcode.text=Preorder_Arr[indexPath.row].ITEMCODE
            let stockrooom = Preorder_Arr[indexPath.row].STKROOM
            cell.StockLabel.text="SR:"+stockrooom
            if(Newtexts.keys.contains(cell.itemcode.text!) && stckroomval == cell.StockLabel.text)
            {
                cell.Quantitytf.text=Newtexts[cell.itemcode.text!]
                
                
                
            }
            else
            {
                cell.Quantitytf.text=""
                
            }
            
            
            if(Pricearray.keys.contains(cell.itemcode.text!) && stckroomval == cell.StockLabel.text)
            {
                cell.PriceTf.text=Pricearray[cell.itemcode.text!]
                
                if(cell.PriceTf.text=="")
                {
                    
                }
                else
                {
                }
                
                
            }
            else
            {
                cell.PriceTf.text=""
                
                
            }
            
            if(cell.PriceTf.text != "" && cell.Quantitytf.text != "" )
            {
                cell.Cartbtn.setImage(UIImage(named:"cartgreen.png"), for:UIControlState.normal)
                cell.Quantitytf.isUserInteractionEnabled = false
                cell.PriceTf.isUserInteractionEnabled = false
                cell.Cartbtn.isUserInteractionEnabled=false
                
            }
            else
            {
                
                cell.Cartbtn.setImage(UIImage(named:"caart.png"), for:UIControlState.normal)
                cell.Quantitytf.isUserInteractionEnabled = true
                cell.PriceTf.isUserInteractionEnabled = true
                cell.Cartbtn.isUserInteractionEnabled=true
                
            }
            
            
            cell.Invdates.text=Preorder_Arr[indexPath.row].VNDRCODE
            cell.productnames.text=Preorder_Arr[indexPath.row].ITEMDESC
            
        }
        
        return cell as OrderTableViewCell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        
    }
    //    //Country PickerView Delegates
    //
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        var num = Int()
        num=pickerarray.count
        return num
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return  pickerarray.object(at: row) as? String
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedrow = row
    }
    
    
    @IBAction func Cancelaction(_ sender: AnyObject) {
        PickerView.isHidden=true
    }
    
    
    @IBAction func Doneaction(_ sender: AnyObject) {
        Stockroom.text=pickerarray.object(at: selectedrow) as? String
        LibraryAPI.sharedInstance.Stockroom = Stockroom.text!
        LibraryAPI.sharedInstance.Stockroomstring = Stockroom.text!
        let str = LibraryAPI.sharedInstance.Stockroom.components(separatedBy: "-")
        LibraryAPI.sharedInstance.Stockroom=str[0]
        PickerView.isHidden=true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if(pickerarray.count>0)
        {
            pickerarray.removeAllObjects()
        }
        if(textField.tag==20)
        {
            selectedrow=0
            
            let val = FetchCountforStock()
            
            if(val>0)
            {
                LibraryAPI.sharedInstance.isvalueloaded=true
            }
            else
            {
                LibraryAPI.sharedInstance.isvalueloaded=false
            }
            
            if(LibraryAPI.sharedInstance.isvalueloaded==true)
            {
                self.Stockroom.text=LibraryAPI.sharedInstance.Stockroomstring
                
            }
            else
            {
                
                let size = CGSize(width: 30, height:30)
                self.local_activityIndicator_start()

                
                DispatchQueue.global(qos: .background).async {
                    
                    // Validate user input
                    
                     self.Loaddata(url: "\(LibraryAPI.sharedInstance.GlobalUrl)StockDetails.asmx/StockAccessDetailsb2b?UserID=\(LibraryAPI.sharedInstance.Userid)")
                    // Go back to the main thread to update the UI
                    DispatchQueue.main.async {
                        
                        
                    }
                }
                
                // new version dispatch Que
                
               
            }
            
            
            
            
            
            return false
            
        }
        return true
    }
    
    
    func keyboardWillShow(notification: NSNotification)
    {
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true)
        {
        }
        else
        {
            if(Movedup==false)
            {
                if(popupview.isHidden==false)
                {
                    
                    self.view.frame.origin.y -= 150
                    Movedup=false
                }
                
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true)
        {
        }
        else
        {
            if(popupview.isHidden==false)
            {
                self.view.frame.origin.y = 0
                Movedup=false
            }else if(popupview.isHidden==true)
            {
                self.view.frame.origin.y = 0
                Movedup=false
            }else{
                
            }
            
        }
    }
    
    func Loaddata(url:String)
    {
        //responsearray = LibraryAPI.sharedInstance.RequestUrlforPreorderstock(url: url)
        self.getsuccess(params: [:], urlhit: url, completion:  { (jsonmutable) in
            self.responsearray = jsonmutable
        if(self.responsearray.count>0)
        {
            self.pickerarray.removeAllObjects()
            for i in 0 ..< self.responsearray.count
            {
                let dicres = self.responsearray[i] as! NSDictionary
                let code = dicres.object(forKey: "STOCK ROOM CODE") as? String
                let desc = dicres.object(forKey: "STOCK ROOM DESC") as? String
                let str=code?.appending(" - \(desc!)")
                self.pickerarray.add(str!)
                
            }
            self.Picker.reloadAllComponents()
            self.local_activityIndicator_stop()
            self.PickerView.isHidden=false
            self.NoRecordBtn.isHidden = true
        }else{
            self.NoRecordBtn.isHidden = false
        }
        
        
             })
    }
    
    
    func loadb2bdata(url:String)
    {
        self.getsuccess(params: [:], urlhit: url, completion:  { (jsonmutable) in
            LibraryAPI.sharedInstance.preorderarray=jsonmutable
            self.responseJSONstring = LibraryAPI.sharedInstance.preorderarray
            if (self.responseJSONstring.count > 0) {
                self.NoRecordBtn.isHidden = true
            }
            else{
                self.NoRecordBtn.isHidden = false
            }
        })
    }
    
    //    func textFieldShouldEndEditing(textField: UITextField) -> Bool
    //    {  //delegate method
    //
    //        return true
    //    }
    //
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       
        if(textField.tag==21)
        {
            Product.resignFirstResponder()
            
        }
        else if(textField.tag==20)
        {
            Stockroom.resignFirstResponder()
        }
        else
            
        {
            
            textField.resignFirstResponder()
        }
        
        
        return true
    }
    
    //    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
    //        textView.resignFirstResponder()
    //    }
    //
    
    
    @IBAction func MenuAction(_ sender: AnyObject)
    {
        self.action()
    }
    
    func action()
    {
        
    }
    
    
    func fetchdbvalues()
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entity(forEntityName: "CartTable", in: managedContext)
        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        
        fetchRequest.returnsObjectsAsFaults = false
        
        //        let resultPredicate2 = NSPredicate(format: "itemcode = %@", "XIMP0004")
        //  fetchRequest.predicate=NSPredicate(format: "price == %@", "12")
        
        
        do {
            let result = try managedContext.fetch(fetchRequest)
            
            let itemList = result as! [NSManagedObject]
            
            print("It_Li_coun", itemList.count)
            Newtexts.removeAll()
            Pricearray.removeAll()
            
            if(itemList.count>0)
            {
                
                for i in 0..<itemList.count
                {
                    let dict = NSMutableDictionary()
                    dict.setValue(itemList[i].value(forKey: "itemcode"), forKeyPath: "itemcode")
                    dict.setValue(itemList[i].value(forKey:"price"), forKeyPath: "price")
                    dict.setValue(itemList[i].value(forKey:"products"), forKeyPath: "products")
                    dict.setValue(itemList[i].value(forKey:"quantity"), forKeyPath: "quantity")
                    dict.setValue(itemList[i].value(forKey:"vendor"), forKeyPath: "vendor")
                    dict.setValue(itemList[i].value(forKey:"availability"), forKeyPath: "availability")
                    
                    let code=itemList[i].value(forKey:"itemcode") as! String
                    let qnty=String(itemList[i].value(forKey:"quantity") as! Int)
                    let Price=String(itemList[i].value(forKey:"price")as! Double)
                    
                    
                    Newtexts[code]=qnty
                    Pricearray[code]=Price
                    
                }
                
            }
            else
            {
                
            }
            
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
    }
    
    
    
    func buttonClicked(sender:UIButton) {
        
        let buttonPosition = sender.convert(CGPoint.zero, to: self.QuantityTableview)
        let indexPath = self.QuantityTableview.indexPathForRow(at: buttonPosition)
        let currentCell = QuantityTableview.cellForRow(at: indexPath!) as! OrderTableViewCell
        if(currentCell.PriceTf.text=="")
        {
            self.Globalalert(title: "Alert", Message:"Enter Price")
        }
        else
        {
            if(currentCell.Quantitytf.text=="")
            {
                self.Globalalert(title: "Alert", Message:"Enter Quantity")
                currentCell.PriceTf.text=""
                
                
                
            }
            else
            {
                let quantity=Int(currentCell.Quantitytf.text!)
                let available=Int(currentCell.available.text!)
                let productname = currentCell.productnames.text
                
                if(currentCell.PriceTf.text=="0")
                {
                    
                    self.Globalalert(title: "Alert", Message:"Value should be greater than zero")
                    let str = Preorder_Arr[(indexPath?.row)!].ITEMCODE
                    // let str=responseJSONstring.objectAtIndex((indexPath?.row)!).objectForKey("ITEMCODE") as? String
                    LibraryAPI.sharedInstance.PriceValue[str] = ""
                }
                else
                {
                    
                    if(isb2b==true)
                    {
                        if(searchActive==false)
                        {
                            let str=Preorder_Arr[(indexPath?.row)!].ITEMCODE
                            LibraryAPI.sharedInstance.temptexorder[str] = currentCell.Quantitytf.text
                            LibraryAPI.sharedInstance.PriceValue[str] = currentCell.PriceTf.text
                        }
                        else
                        {
                            let dicstr = newSearchArray[(indexPath?.row)!] as! NSDictionary
                            let str=dicstr.object(forKey: "ITEMCODE") as? String
                            LibraryAPI.sharedInstance.temptexorder[str!] = currentCell.Quantitytf.text
                            LibraryAPI.sharedInstance.PriceValue[str!] = currentCell.PriceTf.text
                            
                            
                        }
                        
                        
                        Buttontitle[sender.tag]="Added"
                        _ = Float(Preorder_Arr[(indexPath?.row)!].UNITPRIC)
                        //var val = (responseJSONstring.objectAtIndex(indexPath!.row).objectForKey("UNITPRIC")as! Float)
                        // val.roundTo(".2")
                        
                        if(searchActive==false)
                        {
                            //responseJSONstring.objectAtIndex(indexPath!.row).objectForKey("UNITPRIC")as! NSNumber
                            let elm = Element(Quantities:currentCell.Quantitytf.text!,Productnames:currentCell.productnames.text!,Invoicedates:currentCell.Invdates.text!,vendor:currentCell.itemcode.text!,prices:Float(currentCell.PriceTf.text!)!,availabilty:Int(currentCell.available.text!)!,Stockroom: currentCell.StockLabel.text!)
                            
                            if(list.count>0)
                            {
                                // chk for item code
                                if(list.first?.Stockroom == currentCell.StockLabel.text)
                                {
                                    // chk for item code
                                    
                                    list.append(elm)
                                    
                                    self.UpdateDb()
                                    
                                }
                                else
                                {
                                    
                                    
                                    if(searchActive==false)
                                    {
                                        let str=Preorder_Arr[(indexPath?.row)!].ITEMCODE
                                        LibraryAPI.sharedInstance.temptexorder[str] = ""
                                        LibraryAPI.sharedInstance.PriceValue[str] = ""
                                        
                                        Newtexts[str]=""
                                        Pricearray[str]=""
                                        
                                        
                                        //Newtexts[str!]=""
                                    }
                                    else
                                    {
                                        let dicstr1  = newSearchArray[(indexPath?.row)!] as! NSDictionary
                                        let str=dicstr1.object(forKey: "ITEMCODE") as? String
                                        
                                        LibraryAPI.sharedInstance.temptexorder[str!] = ""
                                        LibraryAPI.sharedInstance.PriceValue[str!] = ""
                                        Newtexts[str!]=""
                                        Pricearray[str!]=""
                                        // Newtexts[str!]=""
                                    }
                                    
                                    
                                    self.Globalalert(title: "Alert",Message:"Please Select Products with the same Stock Room")
                                }
                                
                            }
                            else
                            {
                                list.append(elm)
                                
                                self.UpdateDb()
                                
                                
                            }
                            
                            
                            
                        }
                        else
                        {
                            //(newSearchArray.objectAtIndex(indexPath!.row).objectForKey("UNITPRIC")as! NSNumber)
                            let elm = Element(Quantities:currentCell.Quantitytf.text!,Productnames:currentCell.productnames.text!,Invoicedates:currentCell.Invdates.text!,vendor:currentCell.itemcode.text!,prices: Float(currentCell.PriceTf.text!)! ,availabilty:Int(currentCell.available.text!)!,Stockroom: currentCell.StockLabel.text!)
                            
                            if(list.count>0)
                            {
                                // chk for item code
                                if(list.first?.Stockroom == currentCell.StockLabel.text)
                                {
                                    // chk for item code
                                    
                                    list.append(elm)
                                    
                                    self.UpdateDb()
                                    
                                }
                                else
                                {
                                    
                                    
                                    if(searchActive==false)
                                    {
                                        let str=Preorder_Arr[(indexPath?.row)!].ITEMCODE
                                        LibraryAPI.sharedInstance.temptexorder[str] = ""
                                        LibraryAPI.sharedInstance.PriceValue[str] = ""
                                        //Newtexts[str!]=""
                                    }
                                    else
                                    {
                                        let dicstr2 = newSearchArray[(indexPath?.row)!] as! NSDictionary
                                        let str=dicstr2.object(forKey: "ITEMCODE") as? String
                                        
                                        LibraryAPI.sharedInstance.temptexorder[str!] = ""
                                        LibraryAPI.sharedInstance.PriceValue[str!] = ""
                                        // Newtexts[str!]=""
                                    }
                                    
                                    
                                    self.Globalalert(title: "Alert",Message:"Please Select Products with the same Stock Room")
                                }
                                
                            }
                                
                            else
                            {
                                list.append(elm)
                                
                                
                            }
                            
                        }
                        
                        self.flip()
                        
                        
                    }
                    else
                    {
                        
                        if(quantity!>available!)
                        {
                            
                            self.Globalalert(title: "Alert",Message:"\(currentCell.productnames.text!) is available for \(available!) PCE only")
                            
                            if(searchActive==false)
                            {
                                let str=Preorder_Arr[(indexPath?.row)!].ITEMCODE
                                //  LibraryAPI.sharedInstance.temptexorder[str!] = ""
                                LibraryAPI.sharedInstance.PriceValue[str] = currentCell.PriceTf.text
                                
                                Newtexts[str]=""
                            }
                            else
                            {
                                let str3 = newSearchArray[(indexPath?.row)!] as! NSDictionary
                                let str=str3.object(forKey: "ITEMCODE") as? String
                                
                                //    LibraryAPI.sharedInstance.temptexorder[str] = ""
                                LibraryAPI.sharedInstance.PriceValue[str!] = currentCell.PriceTf.text
                                Newtexts[str!]=""
                            }
                            
                        }
                        else
                        {
                            
                            
                            if(searchActive==false)
                            {
                                let str=Preorder_Arr[(indexPath?.row)!].ITEMCODE
                                LibraryAPI.sharedInstance.temptexorder[str] = currentCell.Quantitytf.text
                                LibraryAPI.sharedInstance.PriceValue[str] = currentCell.PriceTf.text
                                
                                let itm=NSMutableArray()
                                //changes 18/8/18
                                if itm.contains(str)
                                {
                                    
                                }
                                else
                                {
                                    itm.add(str)
                                }
                                
                                //changes 18/8/18
                                for Colour in 0..<itm.count
                                {
                                    
                                    let str = itm[Colour] as! String
                                    
                                    
                                    if (LibraryAPI.sharedInstance.Colourtexts.value(forKey: str) as? String) != nil // [Itemcodearray[Colour] as! String]
                                    {
                                        LibraryAPI.sharedInstance.Colourtexts.setValue("GreenColor", forKey: str)
                                    }
                                    else
                                    {
                                    }
                                }
                                
                                
                                
                            }
                            else
                            {
                                let dicstr4 = newSearchArray[(indexPath?.row)!] as! NSDictionary
                                let str=dicstr4.object(forKey: "ITEMCODE") as? String
                                LibraryAPI.sharedInstance.temptexorder[str!] = currentCell.Quantitytf.text
                                LibraryAPI.sharedInstance.PriceValue[str!] = currentCell.PriceTf.text
                                let itm=NSMutableArray()
                                
                                
                                
                                //changes 18/8/18
                                if itm.contains(str!)
                                {
                                    
                                }
                                else
                                {
                                    itm.add(str!)
                                }
                                
                                //changes 18/8/18
                                
                                for Colour in 0..<itm.count
                                {
                                    
                                    let str = itm[Colour] as! String
                                    
                                    
                                    
                                    if (LibraryAPI.sharedInstance.Colourtexts.value(forKey: str) as? String) != nil // [Itemcodearray[Colour] as! String]
                                    {
                                        LibraryAPI.sharedInstance.Colourtexts.setValue("GreenColor", forKey: str)
                                    }
                                    else
                                    {
                                    }
                                }
                                
                                
                                
                            }
                            
                            
                            Buttontitle[sender.tag]="Added"
                            
                            let val = Preorder_Arr[(indexPath?.row)!].ITEMCODE
                            //val.roundTo(".2")
                            
                            if(searchActive==false)
                            {
                                //responseJSONstring.objectAtIndex(indexPath!.row).objectForKey("UNITPRIC")as! NSNumber
                                let elm = Element(Quantities:currentCell.Quantitytf.text!,Productnames:currentCell.productnames.text!,Invoicedates:currentCell.Invdates.text!,vendor:currentCell.itemcode.text!,prices: Float(currentCell.PriceTf.text!)! ,availabilty:Int(currentCell.available.text!)!,Stockroom: currentCell.StockLabel.text!)
                                
                                
                                
                                if(list.count>0)
                                {
                                    if(list.first?.Stockroom == currentCell.StockLabel.text)
                                    {
                                        // chk for item code
                                        
                                        
                                        list.append(elm)
                                        
                                        self.UpdateDb()
                                        
                                    }
                                    else
                                    {
                                        if(searchActive==false)
                                        {
                                            let str=Preorder_Arr[(indexPath?.row)!].ITEMCODE
                                            LibraryAPI.sharedInstance.temptexorder[str] = ""
                                            LibraryAPI.sharedInstance.PriceValue[str] = ""
                                            
                                            Newtexts[str]=""
                                            Pricearray[str]=""
                                            
                                            let itm = NSMutableArray()
                                            
                                            
                                            if itm.contains(str)
                                            {
                                                
                                            }
                                            else
                                            {
                                                itm.add(str)
                                            }
                                            
                                            
                                            
                                            // print("red1")
                                            for Colour in 0..<itm.count
                                            {
                                                
                                                let str = itm[Colour] as! String
                                                
                                                
                                                if (LibraryAPI.sharedInstance.Colourtexts.value(forKey: str) as? String) != nil // [Itemcodearray[Colour] as! String]
                                                {
                                                    
                                                    LibraryAPI.sharedInstance.Colourtexts.setValue("RedColor", forKey: str)
                                                    
                                                }
                                                else
                                                {
                                                }
                                            }
                                            
                                            
                                            
                                            
                                            //Newtexts[str!]=""
                                        }
                                        else
                                        {
                                            let dicstr5 = newSearchArray[(indexPath?.row)!] as! NSDictionary
                                            let str=dicstr5.object(forKey: "ITEMCODE") as? String
                                            
                                            LibraryAPI.sharedInstance.temptexorder[str!] = ""
                                            LibraryAPI.sharedInstance.PriceValue[str!] = ""
                                            Newtexts[str!]=""
                                            Pricearray[str!]=""
                                            // Newtexts[str!]=""
                                            
                                            let itm = NSMutableArray()
                                            
                                            
                                            if itm.contains(str!)
                                            {
                                                
                                            }
                                            else
                                            {
                                                itm.add(str!)
                                            }
                                            
                                            
                                            
                                            print("red2")
                                            for Colour in 0..<itm.count
                                            {
                                                
                                                let str = itm[Colour] as! String
                                                
                                                
                                                
                                                if let val = LibraryAPI.sharedInstance.Colourtexts.value(forKey: str) as? String // [Itemcodearray[Colour] as! String]
                                                {
                                                    
                                                    LibraryAPI.sharedInstance.Colourtexts.setValue("RedColor", forKey: str)
                                                    
                                                    
                                                    
                                                }
                                                else
                                                {
                                                }
                                            }
                                            
                                            
                                            
                                        }
                                        
                                        self.Globalalert(title: "Alert",Message:"Please Select Products with the same Stock Room")
                                    }
                                    
                                }
                                else
                                {
                                    list.append(elm)
                                    
                                    self.UpdateDb()
                                    
                                    
                                }
                                
                                
                                
                            }
                            else
                            {
                                //(newSearchArray.objectAtIndex(indexPath!.row).objectForKey("UNITPRIC")as! NSNumber)
                                let elm = Element(Quantities:currentCell.Quantitytf.text!,Productnames:currentCell.productnames.text!,Invoicedates:currentCell.Invdates.text!,vendor:currentCell.itemcode.text!,prices: Float(currentCell.PriceTf.text!)!,availabilty:Int(currentCell.available.text!)!,Stockroom: currentCell.StockLabel.text!)
                                if(list.count>0)
                                {
                                    
                                    // chk for item code
                                    if(list.first?.Stockroom == currentCell.StockLabel.text)
                                    {
                                        // chk for item code
                                        
                                        
                                        list.append(elm)
                                        
                                        self.UpdateDb()
                                        
                                    }
                                    else
                                    {
                                        
                                        
                                        if(searchActive==false)
                                        {
                                            let str=Preorder_Arr[(indexPath?.row)!].ITEMCODE
                                            LibraryAPI.sharedInstance.temptexorder[str] = ""
                                            LibraryAPI.sharedInstance.PriceValue[str] = ""
                                            
                                            let itm = NSMutableArray()
                                            
                                            if itm.contains(str)
                                            {
                                                
                                            }
                                            else
                                            {
                                                itm.add(str)
                                            }
                                            
                                            print("red3")
                                            for Colour in 0..<itm.count
                                            {
                                                
                                                let str = itm[Colour] as! String
                                                
                                                
                                                if (LibraryAPI.sharedInstance.Colourtexts.value(forKey: str) as? String) != nil // [Itemcodearray[Colour] as! String]
                                                {
                                                    
                                                    LibraryAPI.sharedInstance.Colourtexts.setValue("RedColor", forKey: str)
                                                }
                                                else
                                                {
                                                }
                                            }
                                            
                                            //Newtexts[str!]=""
                                        }
                                        else
                                        {
                                            let strDic = newSearchArray[(indexPath?.row)!] as! NSDictionary
                                            let str=strDic.object(forKey: "ITEMCODE") as? String
                                            
                                            LibraryAPI.sharedInstance.temptexorder[str!] = ""
                                            LibraryAPI.sharedInstance.PriceValue[str!] = ""
                                            
                                            let itm = NSMutableArray()
                                            
                                            if itm.contains(str!)
                                            {
                                                
                                            }
                                            else
                                            {
                                                itm.add(str!)
                                            }
                                            
                                            print("red4")
                                            
                                            for Colour in 0..<itm.count
                                            {
                                                
                                                let str = itm[Colour] as! String
                                                
                                                
                                                if let val = LibraryAPI.sharedInstance.Colourtexts.value(forKey: str) as? String // [Itemcodearray[Colour] as! String]
                                                {
                                                    
                                                    LibraryAPI.sharedInstance.Colourtexts.setValue("RedColor", forKey: str)
                                                    
                                                }
                                                else
                                                {
                                                }
                                            }
                                            
                                            // Newtexts[str!]=""
                                        }
                                        
                                        
                                        self.Globalalert(title: "Alert",Message:"Please Select Products with the same Stock Room")
                                    }
                                    
                                }
                                    
                                else
                                {
                                    list.append(elm)
                                    
                                    self.UpdateDb()
                                }
                                
                            }
                            
                        }
                        
                        
                        
                        
                        
                        // self.Globalalert("Alert",Message:"Cart Added")
                        self.flip()
                        
                    }
                }
            }
        }
        self.fetchdbvalues()
        self.QuantityTableview.reloadData()
        //  self.animate()
        
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
     
        Searchbar.resignFirstResponder()
    }
    
    
    func Globalalert(title:String,Message:String)
    {
        let alert = UIAlertController(title:title, message: Message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler:
            { (ACTION :UIAlertAction!)in
                
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func flip() {
        let transitionOptions: UIViewAnimationOptions = [.transitionFlipFromRight, .showHideTransitionViews]
        
        UIView.transition(with: badgelabel, duration: 0.7, options: transitionOptions, animations: {
            self.badgelabel.alpha=0.0
            
            }, completion: {
                (value: Bool) in
                self.badgelabel.alpha=1.0
                LibraryAPI.sharedInstance.badgeCount=String(self.list.count)
                self.badgelabel.text=LibraryAPI.sharedInstance.badgeCount
        })
        
        
    }
    
    //Searchbar delegates
    
    //Searchview delegates
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        
        searchActive = false
        QuantityTableview.reloadData()
        Searchbar.resignFirstResponder();
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String)
    {
        if(searchBar.text?.count==0)
        {   searchActive = false
            QuantityTableview.reloadData()
            Searchbar.resignFirstResponder();
        }
        else
        {
            self.searchBarSearchButtonClicked(searchBar: UISearchBar())
            self.searchBarTextDidBeginEditing(searchBar: UISearchBar())
        }
        
    }
    func searchBarTextDidBeginEditing(searchBar: UISearchBar)
    {
        // Searchbar.resignFirstResponder()
    }
    
    
    @IBAction func SubmitAction(_ sender: AnyObject)
    {
        
        Product.resignFirstResponder()
        PickerView.isHidden=true
        
        if(Product.text=="" || Stockroom.text=="")
        {
            let alertController = UIAlertController(title: "Alert", message:"Kindly select all the fields"  , preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                
            }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)
            //self.NoRecordBtn.hidden = false
            
            
        }
        else
        {
            FilteBtn.isHidden = false
            self.fetchBacktoBack(deliveryPageindex1: deliveryPageindex1)
            self.closePopup()
            self.QuantityTableview.isHidden=false
            
        }
        
    }
    
    @IBAction func CloseAction(_ sender: AnyObject)
    {
        self.closePopup()
        
    }
    func closePopup()
    {
        popupview.alpha = 0
        popupview.alpha = 0
        popupview.isHidden = true
        //        FilteBtn.hidden=true
        //        FilterLabel.hidden=true
        
        UIView.animate(withDuration: 0.7) {
            
            self.popupview.alpha = 1
            //            self.FilterLabel.alpha = 1
            //            self.FilteBtn.alpha = 1
            
        }
        
    }
    
    
    func fetchBacktoBack(deliveryPageindex1: Int)
    {
        responseJSONstring.removeAllObjects()
        let size = CGSize(width: 30, height:30)
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
            
            Preorder_Arr = [Preorder]()
            
            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)/Preorder.asmX/Preorder_ItemcodeB2B?userid=\(LibraryAPI.sharedInstance.Userid)&Customer=\(LibraryAPI.sharedInstance.CustomerCode)&Itemcode=\(self.Product.text!)&RecordNo=20&PageNo=\(self.deliveryPageindex1)", completion:  { (jsonmutable) in
                let response = jsonmutable
            if(response.count>0)
            {
                self.NoRecordBtn.isHidden = true
                self.isb2b=true
                LibraryAPI.sharedInstance.Colourtexts.removeAllObjects()
                for word in response
                {
                    Preorder_Arr.append(Preorder(data: word as AnyObject))
                    
                }
                for i in 0..<response.count
                {
                    let dicre = response[i] as! NSDictionary
                    LibraryAPI.sharedInstance.Colourtexts.setValue("RedColor", forKey: dicre.object(forKey: "ITEMCODE") as! String)
                    
                }
                //self.NoRecordBtn.hidden = true
                self.QuantityTableview.reloadData()
                self.local_activityIndicator_stop()
                self.FetchCount()
            }
            else{
                self.NoRecordBtn.isHidden = false
                self.local_activityIndicator_stop()
                self.QuantityTableview.isHidden = true
                self.showToast(message: "No Record Found")
            }
            DispatchQueue.main.async {
               

            }
             })
        }
        
        // new version dispatch Que
        
        
        
    }
    
    
    
    func PreorderLoaddata(deliveryPageindex: Int)
    {
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)preorder.ASMX/Preorder_Itemcode?userid=\(LibraryAPI.sharedInstance.Userid)&Customer=\(LibraryAPI.sharedInstance.CustomerCode)&RecordNo=20&PageNo=\(deliveryPageindex)&ItemCode=", completion: { (jsonmutable) in
                 let responseJSONstring = jsonmutable
                DispatchQueue.main.async {
                    if(responseJSONstring.count > 0)
                    {
                        self.NoRecordBtn.isHidden = true
                        
                        LibraryAPI.sharedInstance.Colourtexts.removeAllObjects()
                        
                        for word in responseJSONstring
                        {
                            Preorder_Arr.append(Preorder(data: word as AnyObject))
                            
                        }
                        //print(responseJSONstring)
                        for i in 0..<responseJSONstring.count
                        {
                            let dicres = responseJSONstring[i] as! NSDictionary
                            LibraryAPI.sharedInstance.Colourtexts.setValue("RedColor", forKey: dicres.object(forKey: "ITEMCODE") as! String)
                        }
                        self.QuantityTableview.reloadData()
                        self.local_activityIndicator_stop()
                    }else{
                        self.local_activityIndicator_stop()
                        self.NoRecordBtn.isHidden = false
                    }
                }
            })
            // Go back to the main thread to update the UI
            
        }
        
        // new version dispatch Que
        
          }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastitem  = Preorder_Arr.count - 1
        print(lastitem)
        if(Normalorder == 1)
        {
            if indexPath.row == lastitem
            {
                deliveryPageindex = deliveryPageindex+1
                
                let lastpage = Int(Preorder_Arr[indexPath.row].TOTAL_PAGE)
                
                
                if(deliveryPageindex <= lastpage)
                {
                    PreorderLoaddata(deliveryPageindex: deliveryPageindex)
                }
                else
                {
                    
                }
                
            }
        }
        else
        {
            if indexPath.row == lastitem
            {
                deliveryPageindex1 = deliveryPageindex1+1
                let lastpage = Int(Preorder_Arr[indexPath.row].TOTAL_PAGE)
                print(lastpage)
                if(deliveryPageindex1 <= lastpage)
                {
                    fetchBacktoBack(deliveryPageindex1: deliveryPageindex1)
                    QuantityTableview.reloadData()
                }
                else
                {
                    
                }
            }
        }
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar)
    {
        searchActive = true
        searcharray.removeAllObjects()
        newSearchArray.removeAllObjects()
        //searchelementarray.removeAll()
        //dict.removeAllObjects()
        // Searchbar.resignFirstResponder()
        if(Searchbar.text != nil)
        {
            
            for i in 0  ..< responseJSONstring.count
            {
                self.NoRecordBtn.isHidden = true
                searchtext=Searchbar.text!
                let dictext = responseJSONstring[i] as! NSDictionary
                text=(dictext.object(forKey: "ITEMDESC") as? String)!
                
              //  if(text.lowercaseString.containsString(searchtext.lowercaseString))
               // if(text.lowercased().contains(.lowercaseLetters))
               // {
                    
               //     newSearchArray.add(responseJSONstring.object(at: i))
               // }
               // else
              //  {
                    
                    
//}
                
                
                //                availability=(responsestring.objectAtIndex(i).objectForKey("AVAILQTY") as? Int)!
                //                price=(responsestring.objectAtIndex(i).objectForKey("UNITPRIC") as! Float)
                //                code=(responsestring.objectAtIndex(i).objectForKey("ITEMCODE") as? String)!
                //                vendorname=(responsestring.objectAtIndex(i).objectForKey("VNDRCODE") as? String)!
                //                self.searchforpattern(text)
                
            }
            
            
            
            self.QuantityTableview.reloadData()
        }
        else
        {
           // self.NoRecordBtn.hidden = false
            searchActive = false
            Searchbar.resignFirstResponder()
            self.QuantityTableview.reloadData()
        }
        
        
    }
       
    func LoadPreorderdata()
    {
        
        //let responsearray1 = LibraryAPI.sharedInstance.RequestUrl(url: "\(LibraryAPI.sharedInstance.GlobalUrl)Preorder.asmx/Preorder_Itemcode?userid=\(LibraryAPI.sharedInstance.Userid)&Customer=\(LibraryAPI.sharedInstance.CustomerCode)")
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Preorder.asmx/Preorder_Itemcode?userid=\(LibraryAPI.sharedInstance.Userid)&Customer=\(LibraryAPI.sharedInstance.CustomerCode)", completion:  { (jsonmutable) in
            LibraryAPI.sharedInstance.preorderarray = jsonmutable
        self.responseJSONstring=LibraryAPI.sharedInstance.preorderarray
        
        if(self.responseJSONstring.count>0)
        {
            self.NoRecordBtn.isHidden = true
            LibraryAPI.sharedInstance.Colourtexts.removeAllObjects()
            for i in 0..<self.responseJSONstring.count
            {
                let dicresJ = self.responseJSONstring[i] as! NSDictionary
                LibraryAPI.sharedInstance.Colourtexts.setValue("RedColor", forKey:dicresJ.object(forKey: "ITEMCODE") as! String)
            }
            self.FetchCount()
            
        }else{
            self.NoRecordBtn.isHidden = false
        }
        })
    }
    
    func navigate()
    {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
    }
    
    func searchforpattern( text : String) {
        do {
            let regex = try NSRegularExpression(pattern: searchtext, options: NSRegularExpression.Options.caseInsensitive )
            let nsstr = text as NSString
            let all = NSRange(location: 0, length: nsstr.length)
            var matches : [String] = [String]()
            regex.enumerateMatches(in: text, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: all) {
                (result : NSTextCheckingResult?, _, _) in
                
                if let r = result {
                    let result = nsstr.substring(with: r.range) as String
                    matches.append(result)
                    
                    
                    
                    let elm = searchElement(availabilities:self.availability,Productnames:self.text,Invoicedates:self.vendorname,vendor:self.code,prices:Float(self.price))
                    
                    
                    
                    self.searchelementarray.append(elm)
                    
                    self.QuantityTableview.reloadData()
                    
                }
            }
            
            
        } catch {
            searchActive = false
            Searchbar.resignFirstResponder()
            self.QuantityTableview.reloadData()
        }
        
        
        
    }
    
    
    
    
    @IBAction func FilterAction(_ sender: AnyObject)
    {
        
        popupview.alpha = 0
        popupview.alpha = 0
        popupview.isHidden = false
        searchActive=false
        
        UIView.animate(withDuration: 0.7) {
            
            self.popupview.alpha = 1
            
        }
        
    }
    
    
    func fetchdbvaluesforProceed()
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entity(forEntityName: "CartTable", in: managedContext)
        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        
        fetchRequest.returnsObjectsAsFaults = false
        
        //        let resultPredicate2 = NSPredicate(format: "itemcode = %@", "XIMP0004")
        //  fetchRequest.predicate=NSPredicate(format: "price == %@", "12")
        
        
        do {
            let result = try managedContext.fetch(fetchRequest)
            
            let itemList = result as! [NSManagedObject]
            
            
            
            
            if(itemList.count>0)
            {
                
                for i in 0..<itemList.count
                {
                    let dict = NSMutableDictionary()
                    dict.setValue(itemList[i].value(forKey: "itemcode"), forKeyPath: "itemcode")
                    dict.setValue(itemList[i].value(forKey: "price"), forKeyPath: "price")
                    dict.setValue(itemList[i].value(forKey: "products"), forKeyPath: "products")
                    dict.setValue(itemList[i].value(forKey: "quantity"), forKeyPath: "quantity")
                    dict.setValue(itemList[i].value(forKey: "vendor"), forKeyPath: "vendor")
                    dict.setValue(itemList[i].value(forKey: "availability"), forKeyPath: "availability")
                    dict.setValue(itemList[i].value(forKey: "stock"), forKeyPath: "stock")
                    
                    
                    proceedarray.add(dict)
                    
                    //                    proceedarray=self.itemMutableArray()
                    
                }
                
                
            }
            else
            {
                
            }
            
            
        } catch {
            let fetchError = error as NSError
        }
        
        
        
    }
    
    
    
    @IBAction func ProceedAction(_ sender: AnyObject)
    {
        if(proceedarray.count>0)
        {
            proceedarray.removeAllObjects()
        }
        
        self.fetchdbvaluesforProceed()
        
        //  proceedarray.addObject(dict)
        if(proceedarray.count>0)
        {
            let lvc=self.storyboard?.instantiateViewController(withIdentifier: "ConfirmOrderViewController") as! ConfirmOrderViewController
            lvc.listfromorder=list
            
            lvc.PersistentArray=proceedarray
            LibraryAPI.sharedInstance.lastviewcontroller=""
            LibraryAPI.sharedInstance.preorderarray=responseJSONstring
            // self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: lvc), animated: true)
            self.navigationController?.pushViewController(lvc, animated: true)
            // self.sideMenuViewController!.hideMenuViewController()
        }
        else
        {
            let alert = UIAlertController(title: "Sorry", message:"Add carts and come back", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler:
                { (ACTION :UIAlertAction!)in
                    
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    
    
    
}



extension NSLayoutConstraint {
    
    override open var description: String {
        let id = identifier ?? ""
        return "id: \(id), constant: \(constant)" //you may print whatever you want here
    }
}

//extension Double {
//    func roundToNearestValue(value: Double) -> Double {
//        let remainder = self % value
//        let shouldRoundUp = remainder >= value/2 ? true : false
//        let multiple = floor(self / value)
//        let returnValue = !shouldRoundUp ? value * multiple : value * multiple + value
//        return returnValue
//    }
//}

extension Double {
    mutating func roundToDecimal(fractionDigits: Int) -> Double {
        let multiplier = pow(10.0, Double(fractionDigits))
        return (self * multiplier).rounded() / multiplier
    }
}


extension PreOrderViewController{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField.placeholder=="Enter the Price")
        {
            guard let text = textField.text else { return true }
            
            let set = NSCharacterSet(charactersIn: "0123456789.").inverted
            if(string.rangeOfCharacter(from: set) == nil)
            {
                let newLength = text.count + string.count - range.length
                
                var dotAfterLength = newLength + 2
                //let y = Double(round(1000*newLength)/1000)
                
                if newLength <= 8
                {
                    let  char = string.cString(using: String.Encoding.utf8)!
                    let isBackSpace = strcmp(char, "\\b")
                    
                    if (isBackSpace == -92) {
                        
                        return true
                    }
                    else
                    {
                        if textField.text?.count != 0
                        {
                            if string == "."
                            {
                                //String Containg Dot or not
                                let fullName = textField.text!
                                
                                if fullName.range(of: ".") == nil {
                                    return true
                                }
                                else{
                                    return false
                                }
                                
                            }
                            else
                            {
                                let fullName = textField.text!
                                
                                if fullName.range(of:".") == nil {
                                    
                                    
                                    
                                    return true
                                    
                                }
                                else{
                                    
                                    
                                    
                                    let fullName = textField.text! + string
                                    let fullNameArr = fullName.split{$0 == "."}.map(String.init)
                                    
                                    if fullNameArr.count != 0
                                    {
                                        if fullNameArr[1].count <= 2
                                        {
                                            return true
                                        }
                                        else
                                        {
                                            return false
                                        }
                                    }
                                    
                                }
                            }
                            
                            
                        }
                        else
                        {
                            if string == "."
                            {
                                textField.text="0."
                                return false
                            }
                            else
                            {
                                return true
                            }
                        }
                    }
                    
                    
                    
                    
                }
                else
                {
                    return false
                }
                
                return newLength <= 16
            }
            else
            {
                return false
            }
            
            // Bool
            
        }
        else if(textField.tag==21)
        {
            let  char = string.cString(using: String.Encoding.utf8)!
            let isBackSpace = strcmp(char, "\\b")
            
            if (isBackSpace == -92) {
                
                
                if((textField.text?.count)!>4)
                {
                    Submitbtn.alpha=1.0
                    
                    Submitbtn.isUserInteractionEnabled=true
                    
                    
                }
                else
                {
                    
                    Submitbtn.alpha=0.5
                    Submitbtn.isUserInteractionEnabled=false
                    
                    
                }
                
            }
            else
            {
                if((textField.text?.count)!>=3)
                {
                    Submitbtn.alpha=1.0
                    Submitbtn.isUserInteractionEnabled=true
                    
                    
                }
                else
                {
                    Submitbtn.alpha=0.5
                    Submitbtn.isUserInteractionEnabled=false
                    
                    
                }
            }
            
            
        }
        else
        {
            guard let text = textField.text else { return true }
            
            let newLength = text.count + string.count - range.length
            return newLength <= 5 // Bool
            
            
        }
        
        return true
    }
    
    
   
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField.tag==21 || textField.tag==20)
        {
            
        }
        else
        {
            isdotentered=false
            
            
            
            
            if(textField.placeholder=="Enter the Price")
            {
                if(searchActive==false)
                {
                    
                    // let str=responseJSONstring.objectAtIndex(textField.tag).objectForKey("ITEMCODE") as? String
                    let str = Preorder_Arr[textField.tag].ITEMCODE
                    Pricearray[str]=textField.text
                    
                    
                    
                    
                }
                else
                {
                    let dic1str = newSearchArray[textField.tag] as! NSDictionary
                    let str=dic1str.object(forKey: "ITEMCODE") as? String
                    
                    Pricearray[str!]=textField.text
                }
                
                
            }
            else
            {
                if(searchActive==false)
                {
                    let str=Preorder_Arr[textField.tag].ITEMCODE
                    Newtexts[str] = textField.text
                    
                }
                else
                {
                    let dic2str = newSearchArray[textField.tag] as! NSDictionary
                    let str=dic2str.object(forKey: "ITEMCODE") as? String
                    Newtexts[str!] = textField.text
                    
                }
                
                
                
            }
            
            
        }
    }
    
    
}

extension Float
{
    func format(f: String) -> String
    {
        return NSString(format: "%\(f)f" as NSString, self) as String
    }
    mutating func roundTo(f: String)
    {
        self = NSString(format: "%\(f)f" as NSString, self).floatValue
    }
}


