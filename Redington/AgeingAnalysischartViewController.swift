//
//  AgeingAnalysischartViewController.swift
//  Redington
//
//  Created by truetech on 27/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
import Charts
import KCFloatingActionButton

class AgeingAnalysischartViewController: UIViewController,ChartViewDelegate,KCFloatingActionButtonDelegate {


    @IBOutlet var Chartview: CombinedChartView!
    var month=[String]()
    var netbreadth=[Double]()
    var salesbreadth=[Double]()
     var Linechartarray=[Double]()
    var responsestring=NSMutableArray()
    var fab=KCFloatingActionButton()
    let unitsSold = [-2.0, 4.0, 6.0, 3.0, 12.0, 16.0, 4.0, 17.0, 2.0, 4.0, 5.0, 4.0]
    let unitsSold1 = [22.0, 42.0, 62.0, 32.0, -12.0, 1.0, 42.0, 17.0, 21.0, 42.0, 5.0, 4.0]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.Loaddata()
        
        self.layoutFAB()
        for i in 0..<responsestring.count
        {
            let dic = responsestring[i] as! NSDictionary
            Linechartarray.append((dic.object(forKey: "Over Due") as? Double)!)
            salesbreadth.append((dic.object(forKey: "Sales") as? Double)!)
            netbreadth.append((dic.object(forKey: "Sales Margin") as? Double)!)
          
        }
        
            for i in 0..<salesbreadth.count
        {
            let dicm = salesbreadth[i] as! NSDictionary
            month.append((dicm.object(forKey: "MonthYear") as? String)!)
        }
        Chartview.delegate=self
        setChart(xValues: month, yValuesLineChart:  Linechartarray, value2: salesbreadth,yValuesBarChart: netbreadth)
        Chartview.zoom(scaleX: 4.0, scaleY: 0.0, x: 0.0, y: 0.0)
    }
    
    func Loaddata()
    {
        
        
       // responsestring = LibraryAPI.sharedInstance.RequestUrl(url: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/SalesAnalysis?Customer=\(LibraryAPI.sharedInstance.CustomerCode)&Branch=\(LibraryAPI.sharedInstance.branchcode)&")
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/SalesAnalysis?Customer=\(LibraryAPI.sharedInstance.CustomerCode)&Branch=\(LibraryAPI.sharedInstance.branchcode)&", completion: { (jsonmutable) in
            self.responsestring = jsonmutable
        })
        if(responsestring.count>0)
        {
           
        }
    }
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            
            self.fab.close()
        }
         fab.fabDelegate = self
        
        self.view.addSubview(fab)
        
    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
       
    }

    
    func setChart(xValues: [String], yValuesLineChart: [Double], value2:[Double],yValuesBarChart: [Double]) {
        Chartview.noDataText = "Please provide data for the chart."
        
        var yVals1 : [ChartDataEntry] = [ChartDataEntry]()
        var yVals2 : [BarChartDataEntry] = [BarChartDataEntry]()
        var yVals3: [BarChartDataEntry] =  [BarChartDataEntry]()
        
        for i in 0..<xValues.count {
            
            //yVals1.append(ChartDataEntry(value: yValuesLineChart[i], xIndex: i))
           //yVals2.append(BarChartDataEntry(value: yValuesBarChart[i] - 1, xIndex: i))
           //yVals3.append(BarChartDataEntry(value: value2[i] - 1, xIndex: i))
            yVals1.append(ChartDataEntry(x: Double(i), y: yValuesLineChart[i]))
            yVals2.append(BarChartDataEntry(x: Double(i), y: yValuesBarChart[i] - 1))
            yVals3.append(BarChartDataEntry(x:  Double(i), y: value2[i] - 1))
        }
        
       // let lineChartSet = LineChartDataSet(yVals: yVals1, label: "Line Data")
        let lineChartSet = LineChartDataSet(values: yVals1, label: "Line Data")
        lineChartSet.colors=[UIColor.init(red: 12/255.0, green:   12/255.0, blue: 12/255.0, alpha: 1.0)]
        lineChartSet.circleColors=[UIColor.red]

        lineChartSet.circleHoleColor=UIColor.clear
        let barChartSet: BarChartDataSet = BarChartDataSet(values: yVals2, label: "Bar Data")
        let barChartSet1: BarChartDataSet = BarChartDataSet(values: yVals3, label: "Bar Data")
        
        let dataSets: [BarChartDataSet] = [barChartSet,barChartSet1]
        
       // let data: CombinedChartData = CombinedChartData(xVals: xValues)
        let data: CombinedChartData = CombinedChartData(dataSet: xValues as! IChartDataSet)
        Chartview.rightAxis.enabled=false
        
        
 
         barChartSet.colors =  [UIColor.init(red: 35/255.0, green: 150/255.0, blue: 204/255.0, alpha: 1.0)]
         barChartSet1.colors =  [UIColor.init(red: 154/255.0, green: 181/255.0, blue: 73/255.0, alpha: 1.0)]

        // data.barData = BarChartData(xVals: xValues, dataSets: dataSets)
        // data.lineData = LineChartData(xVals: xValues, dataSets: [lineChartSet])
        
        data.barData = BarChartData(dataSet: dataSets as! IChartDataSet)
         data.lineData = LineChartData(dataSets: [lineChartSet])
       
        
        Chartview.data = data
        
    }
    
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    

}
