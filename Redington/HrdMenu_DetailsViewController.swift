//
//  HrdMenu_DetailsViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 22/11/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

class HrdMenu_DetailsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    var tmparray=[String]()
    var imagearray=[String]()
    
    @IBOutlet weak var hrdtableview: UITableView!
    
    @IBOutlet weak var LogoutBtn: UIButton!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        customnavigation()
        tmparray =  ["LEAVE AND PERMISSION","LEAVE APPROVAL","BIOMETRIC ENQUIRY","SALARY PAYSLIP","VARIABLE PAYSLIP"]
        imagearray = ["Leave.png","Approval.png","Biometric.png","Salary.png","variable.png"]
        hrdtableview.register(UINib(nibName: "HRDMenu_DetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "HRDMenu_DetailsTableViewCell")
        hrdtableview.tableFooterView = UIView(frame: .zero)
    }
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
    func customnavigation()
    {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(HrdMenu_DetailsViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "HRD MENU"
        
    }
    
    func buttonClicked(sender:UIButton)
    {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HRDMenu_DetailsTableViewCell") as! HRDMenu_DetailsTableViewCell
        cell.titlelbl.text=tmparray[indexPath.row]
        cell.imageview.image=UIImage(named: imagearray[indexPath.row])
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch (indexPath.row) {
        case 0:
            self.local_activityIndicator_start()
            DispatchQueue.global(qos: .background).async {
                self.getStringsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=WHATSUP&Option=6", completion: { (jsonstr) in
                    let responsestring = jsonstr
                    DispatchQueue.main.async {
                        if (responsestring == "Y")
                        {
                            self.local_activityIndicator_stop()
                            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "LeaveViewController")), animated: true)
                            self.sideMenuViewController!.hideMenuViewController()
                        } else {
                            let alertController = UIAlertController(title: "Alert", message:" You are Not Authorised for this option" , preferredStyle: .alert)
                            let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                            }
                            alertController.addAction(cancelAction)
                            self.local_activityIndicator_stop()
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                })
            }
            break
        case 1:
            self.local_activityIndicator_start()
            DispatchQueue.global(qos: .background).async {
                DispatchQueue.main.async {
                    self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "LeaveApprovalViewController")),animated: true)
                    self.sideMenuViewController!.hideMenuViewController()
                    self.local_activityIndicator_stop()
                }
            }
            break
        case 2:
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: BioMetricViewController.instance())
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
            }
            let container = BioMetricViewController.instance()
            container.closeHandler = { _ in
                popup.dismiss()
            }
            popup.show(childViewController: container)
            break
        case 3:
            self.local_activityIndicator_start()
            LibraryAPI.sharedInstance.Monthyearpayslip = 1
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "PaySlipViewController")),animated: true)
            self.sideMenuViewController!.hideMenuViewController()
            self.local_activityIndicator_stop()
            break
        case 4:
            LibraryAPI.sharedInstance.Monthyearpayslip = 2
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "PaySlipViewController")),animated: true)
            self.sideMenuViewController!.hideMenuViewController()
            break
        default:
            break
        }
    }
}
