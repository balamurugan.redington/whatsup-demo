//
//  ApiErrorViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 15/01/18.
//  Copyright © 2018 truetech. All rights reserved.
//

import UIKit

class ApiErrorViewController: UIViewController {

    @IBOutlet weak var retrybtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        retrybtn.layer.cornerRadius = 5.0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
