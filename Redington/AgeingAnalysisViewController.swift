//
//  AgeingAnalysisViewController.swift
//  Whatsup
//
//  Created by truetech on 20/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit

import Charts
import KCFloatingActionButton

class AgeingAnalysisViewController: UIViewController,ChartViewDelegate,KCFloatingActionButtonDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource{
    
    
    @IBOutlet weak var nodataview: UIView!
    @IBOutlet weak var Barchartcollectionview: UICollectionView!
    @IBOutlet var Ageingtableview: UITableView!
    var responsestring=NSMutableArray()
    @IBOutlet var graphbtn: UIButton!
    @IBOutlet var tablebtn: UIButton!
    @IBOutlet var backbtn: UIButton!
    @IBOutlet var menubtn: UIButton!
    var   Array30 = [Double]()
    var   array60 = [Double]()
    var   array90 = [Double]()
    var   array120 = [Double]()
    var   arraymorethan120=[Double]()
    var monthsbar=[String]()
    var reversearray=NSMutableArray()
    var MontnDictionary = [String: AnyObject]()
    var montharray=NSMutableArray()
    var fab=KCFloatingActionButton()
    
    
    
    @IBOutlet weak var cviewchart1: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        gradientview.layer.cornerRadius = 3.0
        //        gradientview.clipsToBounds = true
        
        Barchartcollectionview.delegate = self
        Barchartcollectionview.dataSource = self
        Barchartcollectionview.bounces = false
        Barchartcollectionview.backgroundColor = UIColor.clear
        let colorTop =  UIColor(red: 30/255.0, green: 184/255.0, blue: 120/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 2/255.0, green: 132/255.0, blue: 168/255.0, alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop, colorBottom]
        gradientLayer.locations = [ 0.0, 1.0]
        //gradientLayer.frame = self.cviewchart1.bounds
        gradientLayer.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width , height: self.view.frame.height)
        self.cviewchart1.layer.insertSublayer(gradientLayer, at: 0)
        
        self.customnavigation()
        self.self.Barchartcollectionview.register(UINib(nibName: "SalesAnalysisCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SalesAnalysisCollectionViewCell")
        self.Barchartcollectionview.isPagingEnabled = true
        
        
        self.layoutFAB()
        
        self.populatedata()
        nodataview.isHidden = true
        
        // CHartview.zoom(4.0, scaleY: 0.0, x: 0.0, y: 0.0)
        // Do any additional setup after loading the view.
    }
    
    func populatedata()
    {
        
        local_activityIndicator_start()
        // new version dispatch Que
        
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
            self.Loaddata()
            
            
            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
                
                
                
                
            }
        }
        
    }
    
    
    @IBAction func ListAction(_ sender: AnyObject)
    {
        self.setView(view: self.Ageingtableview, hidden: false)
        self.cviewchart1.isHidden=true
        //  self.Ageingtableview.hidden=false
        self.Loaddata()
        self.tablebtn.setTitleColor(UIColor.white, for: UIControlState.normal)
        self.tablebtn.backgroundColor = UIColor.init(red:30/255.0 , green: 184/255.0, blue: 120/255.0, alpha: 1.0)
        self.tablebtn.setImage(UIImage(named: "whitelist.png"), for: UIControlState.normal)
        
        self.graphbtn.setTitleColor(UIColor.gray, for: UIControlState.normal)
        self.graphbtn.setImage(UIImage(named: "graychart.png"), for: UIControlState.normal)
        self.graphbtn.backgroundColor=UIColor.white
        
    }
    
    
    @IBAction func Graphview(_ sender: AnyObject)
    {
        self.parseurl()
        self.cviewchart1.isHidden=false
        self.tablebtn.backgroundColor=UIColor.white
        self.graphbtn.setTitleColor(UIColor.white, for: UIControlState.normal)
        self.graphbtn.backgroundColor = UIColor.init(red:30/255.0 , green: 184/255.0, blue: 120/255.0, alpha: 1.0)
        self.tablebtn.setTitleColor(UIColor.gray, for: UIControlState.normal)
        self.tablebtn.setImage(UIImage(named: "graylist.png"), for: UIControlState.normal)
        self.graphbtn.setImage(UIImage(named: "whitechart.png"), for: UIControlState.normal)
        
        Barchartcollectionview.reloadData()
        Barchartcollectionview.reloadInputViews()
        
        
    }
    
    func setView(view: UIView, hidden: Bool) {
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {() -> Void in
            view.isHidden = hidden
        }, completion: { _ in })
    }
    
    func LoadGraphdata()
    {
        
        
        //http://edi.redingtonb2b.in/whatsup/Customer.asmx/CustomerAgingAnalysis?Customer=S10003&Branch=S1&UserId=SENTHILKS
        // responsestring = LibraryAPI.sharedInstance.RequestUrl(url: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/CustomerAgingAnalysis?Customer=\(LibraryAPI.sharedInstance.CustomerCode)&Branch=S1&UserId=\(LibraryAPI.sharedInstance.Userid)")
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/CustomerAgingAnalysis?Customer=\(LibraryAPI.sharedInstance.CustomerCode)&Branch=S1&UserId=\(LibraryAPI.sharedInstance.Userid)", completion:  { (jsonmutable) in
            self.responsestring = jsonmutable
        })
        if(responsestring.count>0)
        {
            
            
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        //        appdelegate.shouldSupportAllOrientation = true
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.local_activityIndicator_stop()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func Loaddata()
    {
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/CustomerAgingAnalysis?Customer=\(LibraryAPI.sharedInstance.CustomerCode)&Branch=\(LibraryAPI.sharedInstance.branchcode)&UserId=\(LibraryAPI.sharedInstance.Userid)", completion: { (jsonmutable) in
            self.reversearray  = jsonmutable
            if(self.reversearray.count>0) {
                self.responsestring=(self.reversearray as NSArray).mutableCopy() as! NSMutableArray
                DispatchQueue.main.async {
                    self.Ageingtableview.isHidden=false
                    self.graphbtn.isUserInteractionEnabled=true
                    self.Ageingtableview.reloadData()
                    self.local_activityIndicator_stop()
                }
            } else {
                DispatchQueue.main.async {
                    self.nodataview.isHidden = false
                    self.Ageingtableview.isHidden = true
                    self.graphbtn.isUserInteractionEnabled=false
                    self.local_activityIndicator_stop()
                }
            }
        })
    }
    
    func customnavigation() {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(AgeingAnalysisViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Ageing Analysis"
    }
    
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            self.fab.close()
        }
        fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
            self.populatedata()
            self.fab.close()
        }
        fab.fabDelegate = self
        self.view.addSubview(fab)
    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
        
    }
    
    
    func parseurl() {
        if(Array30.count>0) {
            Array30.removeAll()
            array60.removeAll()
            array90.removeAll()
            array120.removeAll()
            arraymorethan120.removeAll()
            monthsbar.removeAll()
            
        }
        if(responsestring.count>0) {
            nodataview.isHidden = true
            for i in 0..<responsestring.count {
                let dic = responsestring[i] as! NSDictionary
                if let val:Double = Double(dic.object(forKey: "0to30Ageing")as! Double) {
                    Array30.append(((val as AnyObject) as? Double)!)
                }
                if let val=dic.object(forKey: "31to60Ageing") {
                    array60.append(((val as AnyObject) as? Double)!)
                }
                if let val=dic.object(forKey: "61to90Ageing") {
                    array90.append(((val as AnyObject) as? Double)!)
                }
                
                if let val=dic.object(forKey: "91to120Ageing") {
                    array120.append(((val as AnyObject) as? Double)!)
                }
                
                if let val=dic.object(forKey: "Greaterthan120Ageing") {
                    arraymorethan120.append(((val as AnyObject) as? Double)!)
                }
                monthsbar.append(dic.object(forKey: "MonthYear") as! String)
            }
        } else {
            nodataview.isHidden = false
        }
    }
    
    func buttonClicked(sender:UIButton)
    {
        LibraryAPI.sharedInstance.tracker="1"
        self.navigationController?.popViewController(animated: true)
    }
    
    //TableView Delegates
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        if responsestring.count > 0
        {
            return 1
            
        }
        else
        {
            return 0
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return responsestring.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : AgeingAnalysisTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as! AgeingAnalysisTableViewCell
        if(cell == nil)
        {
            cell = Bundle.main.loadNibNamed("Cell", owner: self, options: nil)![0] as! AgeingAnalysisTableViewCell;
            
        }
        if(indexPath.row%2==0)
        {
            cell.contentView.backgroundColor=(UIColor.init(red: 240/255.0, green: 246/255.0, blue: 225/255.0, alpha: 1.0))
        }
        else
        {
            //cell.contentView.backgroundColor=UIColor.whiteColor()
            cell.contentView.backgroundColor=(UIColor.init(red: 215/255.0, green: 250/255.0, blue: 252/255.0, alpha: 1.0))
        }
        let dic = responsestring[indexPath.row] as! NSDictionary
        print("dic val: ",dic)
        cell.month.text=dic.object(forKey: "MonthYear") as? String
        
        _=round((dic.object(forKey: "0to30Ageing") as? Double)!)
        
        
        cell.rowfirst.text=String(dic.object(forKey: "0to30Ageing") as! Double)
        cell.rowsecond.text=String(dic.object(forKey: "31to60Ageing") as! Double)
        cell.rowthird.text=String(dic.object(forKey: "61to90Ageing") as! Double)
        cell.rowfourth.text=String(dic.object(forKey: "91to120Ageing") as! Double)
        cell.rowfifth.text=String(dic.object(forKey: "Greaterthan120Ageing") as! Double)
        
        cell.selectionStyle = .none
        return cell as AgeingAnalysisTableViewCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "InvoicesummerydetailsViewController") as! InvoicesummerydetailsViewController
        LibraryAPI.sharedInstance.lastviewcontroller="aging"
        self.navigationController?.pushViewController(lvc, animated: true)
        
    }
    
    func chartValueSelected(chartView: ChartViewBase, entry: ChartDataEntry, dataSetIndex: Int, highlight: Highlight)
    {
        
        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "InvoicesummerydetailsViewController") as! InvoicesummerydetailsViewController
        LibraryAPI.sharedInstance.lastviewcontroller="aging"
        self.navigationController?.pushViewController(lvc, animated: true)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return responsestring.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width-10,height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SalesAnalysisCollectionViewCell", for: indexPath) as! SalesAnalysisCollectionViewCell
        
        
        
        if responsestring.count>0
        {
            cell.chartview.noDataText=""
        }
        else
        {
            // cell.ChartView.noDataText = "No chart data available."
        }
        
        cell.chartview.pinchZoomEnabled=false
        cell.chartview.xAxis.labelPosition = .bottom
        cell.chartview.descriptionText = ""
        cell.chartview.descriptionTextColor = UIColor.white
        cell.chartview.isUserInteractionEnabled=false
        cell.chartview.rightAxis.enabled=false
        cell.chartview.xAxis.drawGridLinesEnabled = false
        cell.chartview.leftAxis.drawLabelsEnabled = false
        cell.chartview.legend.enabled = false
        let xAxis:XAxis = cell.chartview.xAxis
        xAxis.drawAxisLineEnabled = true
        xAxis.drawGridLinesEnabled = true
        xAxis.drawLabelsEnabled=false
        cell.chartview.layer.cornerRadius = 20.0
        let dicchar = responsestring[indexPath.row] as! NSDictionary
        cell.titlelbl.text = dicchar.object(forKey: "MonthYear")as? String
        if(monthsbar.count>0)
        {
            cell.setChartBarGroupDataSet(dataPoints: [monthsbar[indexPath.row]], values: [Array30[indexPath.row]], values2: [array60[indexPath.row]], values3: [array90[indexPath.row]], values4: [array120[indexPath.row]], values5: [arraymorethan120[indexPath.row]], sortIndex: 1)
            //isanimated=true
        }
        
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "InvoicesummerydetailsViewController") as! InvoicesummerydetailsViewController
        LibraryAPI.sharedInstance.lastviewcontroller="aging"
        self.navigationController?.pushViewController(lvc, animated: true)
    }
    
    
    
}
