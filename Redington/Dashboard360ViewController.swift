//
//  Dashboard360ViewController.swift
//  Redington
//
//  Created by truetech on 06/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
import Charts

class Dashboard360ViewController: UIViewController,ChartViewDelegate {
 
    
    @IBOutlet var hidebtn: UIButton!
    @IBOutlet var datelabel: UILabel!
    var   count = NSInteger()
    @IBOutlet var Infoview: UIView!
    @IBOutlet var Scrolltopconstraint: NSLayoutConstraint!
    @IBOutlet var Scroll: UIScrollView!
    var scrollframe=CGRect()
    var months=[String]()
    var UnitsforbarChart=[Double]()
    @IBOutlet var Barchartview: BarChartView!
    @IBOutlet var Emaillabel: UILabel!
    @IBOutlet var scrollcontainerview: UIView!
  
    
    override func viewWillAppear(_ animated: Bool) {
          scrollframe=Scroll.frame
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        count=0
        Barchartview.delegate=self
        self.Customnavigation()
        Emaillabel.adjustsFontSizeToFitWidth=true
    
        months = ["", "", "", "", "", "", "", ""]
        let unitsSold = [20.0, 4.0, 6.0, 3.0, 12.0, 16.0, 4.0, 18.0]
        self.setbarcharts(dataPoints: months, values: unitsSold,isanimated: true)
        
        let timestamp = DateFormatter.localizedString(from: NSDate() as Date, dateStyle: .medium, timeStyle: .none)
        
        datelabel.text=timestamp
        for _ in 0..<UnitsforbarChart.count
        {
            self.months.append("")
        }
       
        
      //  self.setbarcharts(months, values: UnitsforbarChart, isanimated: true)
        // Do any additional setup after loading the view.
    }
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
       
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        scrollframe=Scroll.frame
        Scroll.isScrollEnabled=true
        Scroll.contentSize = CGSize(width: Scroll.frame.size.width, height: 780)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        
        
        
    }
    func Customnavigation()
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: 0,y: 0, width: self.view.frame.width,height: 65);
        
        //customView.backbutton.hidden=true
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        self.view.addSubview(customView.view)
      
        
        customView.HeaderLabel.text = "Dashboard"
    
        customView.MenuBtn.addTarget(self, action: #selector(Dashboard360ViewController.backbuttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        
    }

    
    
   func setbarcharts(dataPoints: [String], values: [Double],isanimated:Bool) {
        
        Barchartview.noDataText = "You need to provide data for the chart."
        
        var dataEntries: [BarChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
           // let dataEntry = BarChartDataEntry(value: values[i], xIndex: i)
            let dataEntry = BarChartDataEntry(x: Double(i), y: values[i])
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(values: dataEntries, label: "Units Sold")
       // let chartData = BarChartData(xVals: months, dataSet: chartDataSet)
        let chartData = BarChartData(dataSet: chartDataSet)
        Barchartview.pinchZoomEnabled=false
        Barchartview.xAxis.labelPosition = .bottom
        Barchartview.descriptionText = ""
        Barchartview.descriptionTextColor = UIColor.white
        Barchartview.isUserInteractionEnabled=false
        Barchartview.rightAxis.enabled=false
        Barchartview.legend.enabled=false
        let xAxis:XAxis = Barchartview.xAxis
        xAxis.drawAxisLineEnabled = false
        xAxis.drawGridLinesEnabled = false
        var colors: [UIColor] = []
        for _ in 0..<dataPoints.count {
            
            
            colors.append(UIColor.red)
            colors.append(UIColor.red)
        }
        
        chartDataSet.colors = colors

        Barchartview.animate(xAxisDuration: 2.0, yAxisDuration: 2.0)
   
        Barchartview.data = chartData

        
        
 
        
    }
    
    func backbuttonClicked(sender:UIButton)
    {
        if(LibraryAPI.sharedInstance.Currentcustomerlookup=="360")
        {
            
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            LibraryAPI.sharedInstance.lastviewcontroller="Dashboardviewcontroller"
            self.sideMenuViewController!.hideMenuViewController()
        }
        else
        {
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            LibraryAPI.sharedInstance.lastviewcontroller="Dashboardviewcontroller"
            self.sideMenuViewController!.hideMenuViewController()
            
        }

        
    }

    
    
    @IBAction func HideButtonAction(_ sender: AnyObject)
    {
        if(count==0)
        {
            count=1
            hidebtn.setImage(UIImage(named: "btnhide.png"), for: UIControlState.normal)
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                  self.Infoview.alpha=0.0
                }, completion: {
                    (finished: Bool) -> Void in
                    
                    //Once the label is completely invisible, set the text and fade it back in
                    self.Infoview.isHidden=false
                    
                    // Fade in
                    UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                        self.Infoview.alpha=1.0
                        }, completion: nil)
            })
            

            
        }
        else
        {
         count=0
           
            hidebtn.setImage(UIImage(named: "Expand.png"), for: UIControlState.normal)
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.Infoview.alpha=0.0
                }, completion: {
                    (finished: Bool) -> Void in
                    
                    //Once the label is completely invisible, set the text and fade it back in
                    self.Infoview.isHidden=true
                    
                    // Fade in
                    UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                        self.Infoview.alpha=1.0
                        }, completion: nil)
            })
            
   

        }
    
    }
   

}
