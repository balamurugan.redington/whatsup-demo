//
//  HRD1DetailsViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 23/11/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

class HRD1DetailsViewController: UIViewController {
    
    @IBOutlet weak var leaveandpermissionbtn: UIButton!
    @IBOutlet weak var leaveapprovalbtn: UIButton!
    @IBOutlet weak var biometricbtn: UIButton!
    @IBOutlet weak var salarybtn: UIButton!
    @IBOutlet weak var variablebtn: UIButton!
    @IBOutlet weak var leaveapprovalview: UIView!
    @IBOutlet weak var leaveandpermissionview: UIView!
    @IBOutlet weak var biometricview: UIView!
    @IBOutlet weak var salaryview: UIView!
    @IBOutlet weak var variableview: UIView!
    @IBOutlet weak var usernamelbl: UILabel!
    @IBOutlet weak var headerview: UIView!
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        if LibraryAPI.sharedInstance.Globalflag == "N" {
            customnavigation1()
        } else {
            customnavigation()
        }
        
        usernamelbl.text = LibraryAPI.sharedInstance.LiveUserName
        leaveandpermissionbtn.layer.cornerRadius = 10
        leaveapprovalbtn.layer.cornerRadius = 10
        biometricbtn.layer.cornerRadius = 10
        salarybtn.layer.cornerRadius = 10
        variablebtn.layer.cornerRadius = 10
        leaveandpermissionbtn.layer.borderColor = UIColor(red:170/255.0, green:170/255.0, blue:170/255.0, alpha: 1.0).cgColor
        leaveapprovalbtn.layer.borderColor = UIColor(red:170/255.0, green:170/255.0, blue:170/255.0, alpha: 1.0).cgColor
        biometricbtn.layer.borderColor = UIColor(red:170/255.0, green:170/255.0, blue:170/255.0, alpha: 1.0).cgColor
        salarybtn.layer.borderColor = UIColor(red:170/255.0, green:170/255.0, blue:170/255.0, alpha: 1.0).cgColor
        variablebtn.layer.borderColor = UIColor(red:170/255.0, green:170/255.0, blue:170/255.0, alpha: 1.0).cgColor
        leaveandpermissionview.layer.cornerRadius = leaveandpermissionview.frame.size.width/2
        leaveapprovalview.layer.cornerRadius = leaveapprovalview.frame.size.width/2
        biometricview.layer.cornerRadius = biometricview.frame.size.width/2
        salaryview.layer.cornerRadius = salaryview.frame.size.width/2
        variableview.layer.cornerRadius = variableview.frame.size.width/2
    }
    
    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
   
    func customnavigation() {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: 65)
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(HRD1DetailsViewController.buttonClicked(_:)),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "HRD MENU"
    }
    
    func customnavigation1() {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: 65)
        customView.Notificationbtn.setImage(UIImage(named:"logout_hrd.png"), for: UIControlState.normal)
        customView.Notificationbtn.addTarget(self, action: #selector(HRD1DetailsViewController.logout(_:)),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "HRD MENU"
        customView.HeaderLabel.textAlignment = .center
        customView.HeaderLabel.font = UIFont.boldSystemFont(ofSize: 16.0)
    }
    
    func logout(_ sender: UIButton) {
        let alertController = UIAlertController(title: "Alert", message: "Do you want to Logout?", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
        }
        alertController.addAction(cancelAction)
        
        let OKAction = UIAlertAction(title: "confirm", style: .default) { (action:UIAlertAction!) in
            
            LibraryAPI.sharedInstance.LoginCredentials.removeObject(forKey: "IsClicked")
            LibraryAPI.sharedInstance.SelectedValue = ""
            LibraryAPI.sharedInstance.BizCode = ""
            LibraryAPI.sharedInstance.isloggedout=true
            
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "ViewController")), animated: true)
            self.sideMenuViewController!.hideMenuViewController()
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion:nil)
    }
    
    func buttonClicked(_ sender:UIButton) {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
    }
    
    
    @IBAction func leaveandpermissionAction(_ sender: AnyObject) {
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
           // let responsestring = LibraryAPI.sharedInstance.Parsejson1(url: "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=WHATSUP&Option=6")
            self.getStringsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=WHATSUP&Option=6", completion: { (jsonstr) in
                let responsestring = jsonstr
                DispatchQueue.main.async {
                    if responsestring == "Y" {
                        self.local_activityIndicator_stop()
                        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "LeaveViewController")), animated: true)
                        self.sideMenuViewController!.hideMenuViewController()
                    } else {
                        self.local_activityIndicator_stop()
                        self.showalert(title: "Alert", message: " You are Not Authorised for this option", buttontxt: "Ok")
                    }
                }
            })
           
        }
    }
    
    
    @IBAction func leaveApprovalAction(_ sender: AnyObject) {
        self.local_activityIndicator_start()
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "LeaveApprovalViewController")),animated: true)
        self.sideMenuViewController!.hideMenuViewController()
       self.local_activityIndicator_stop()
    }
    
    @IBAction func biometricAction(_ sender: AnyObject) {
        
        let popup = PopupController
            .create(parentViewController: self.parent!)
            .show(childViewController: BioMetricViewController.instance())
            .didShowHandler { popup in
            }
            .didCloseHandler { _ in
        }
        let container = BioMetricViewController.instance()
        container.closeHandler = { _ in
            popup.dismiss()
        }
        popup.show(childViewController: container)
    }
    
    @IBAction func SalaryAction(_ sender: AnyObject) {
        self.local_activityIndicator_start()
        LibraryAPI.sharedInstance.Monthyearpayslip = 1
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "PaySlipViewController")),animated: true)
        self.sideMenuViewController!.hideMenuViewController()
       self.local_activityIndicator_stop()
    }
    
    
    @IBAction func VariableAction(_ sender: AnyObject) {
        LibraryAPI.sharedInstance.Monthyearpayslip = 2
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "PaySlipViewController")),animated: true)
        self.sideMenuViewController!.hideMenuViewController()
    }
}
