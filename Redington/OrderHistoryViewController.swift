//
//  OrderHistoryViewController.swift
//  Whatsup
//
//  Created by truetech on 26/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import KCFloatingActionButton

class OrderHistoryViewController: UIViewController,KCFloatingActionButtonDelegate,UITableViewDataSource,UITableViewDelegate{
    
    @IBOutlet var menubtn: UIButton!
    @IBOutlet weak var titlelabel: UILabel!
    @IBOutlet var Historytableview: UITableView!
    @IBOutlet var backbtn: UIButton!
    var responsestring = NSMutableArray()
    var fab=KCFloatingActionButton()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        customnavigation()
        self.layoutFAB()
        
        
    }
    
    
    func customnavigation()
    {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(OrderHistoryViewController.backaction),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Order Histroy"
    }
    
    func getdata()
    {
        self.local_activityIndicator_start()
        
        // new version dispatch Que
        
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
            
             self.Loaddata()
            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
              
                
            }
        }
        
        // new version dispatch Que
        
    }
    
    
    func getdata1()
    {
        self.local_activityIndicator_start()
        
        // new version dispatch Que
        
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
            
             self.Loaddata()
            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
              

                
            }
        }
        
        // new version dispatch Que
        
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        responsestring=LibraryAPI.sharedInstance.OrderReturnarray
        self.getdata()
        
//        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        appdelegate.shouldSupportAllOrientation = false
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func backaction(sender:UIButton)
    {
        
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "PreOrderHistroyViewController")), animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    
    func Loaddata()
    {
        
        self.local_activityIndicator_start()
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)orderEntry.asmx/OrderHistory?Customer=\(LibraryAPI.sharedInstance.CustomerCode)&FromDate=\(LibraryAPI.sharedInstance.CustomerCodeFrom)&ToDate=\(LibraryAPI.sharedInstance.CustomerCodeTo)", completion:  { (jsonmutable) in
            self.responsestring = jsonmutable
        if(self.responsestring.count>0)
        {
            DispatchQueue.main.async {
                self.Historytableview.isHidden=false
                self.local_activityIndicator_stop()
                self.Historytableview.reloadData()
            }
        }
        else
        {
            self.local_activityIndicator_stop()
            let alertController = UIAlertController(title: "Alert", message: "No Data Available", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "PreOrderHistroyViewController")), animated: true)
                NSLog("OK Pressed")
            }
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
            
        }
        })
    }
    
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            self.fab.close()
        }
        
        fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
            
            
            self.getdata1()
            
            self.fab.close()
        }
        
        fab.fabDelegate = self
        
        self.view.addSubview(fab)
        
    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
        
    }
    
    
    //TableView Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        if responsestring.count > 0
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        return responsestring.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        var cell : OrderhistoryTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as! OrderhistoryTableViewCell
        
        if(responsestring.count > 0)
        {
            if(cell == nil)
            {
                cell = Bundle.main.loadNibNamed("Cell", owner: self, options: nil)![0] as! OrderhistoryTableViewCell;
                
            }
            let dic = responsestring[indexPath.row] as! NSDictionary
            let qtr = dic.object(forKey: "CustCode") as? String
            
            let type =  dic.object(forKey: "CustName") as? String
            
            
            titlelabel.text = "   "+qtr!+"-"+type!+" "
            
            
            cell.orderno.text = dic.object(forKey: "OrderNo") as? String
            cell.orderstatus.text=String( dic.object(forKey: "orderStatus") as! String)
            
            cell.ponumber.text=String( dic.object(forKey: "PO No") as! String)
            cell.orderdate.text =  dic.object(forKey: "OrderDate") as? String
            

        }
        else
        {
            
        }
        cell.selectionStyle = .none
        return cell as OrderhistoryTableViewCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        LibraryAPI.sharedInstance.orderno = ((responsestring.object(at: indexPath.row) as AnyObject).object(forKey: "OrderNo") as? String)!
        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "OrderViewController") as! OrderViewController
        self.navigationController?.pushViewController(lvc, animated: true)
        
    }
    
    
    
}
