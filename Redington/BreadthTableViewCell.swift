//
//  BreadthTableViewCell.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 08/12/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class BreadthTableViewCell: UITableViewCell {
    
    @IBOutlet weak var bizview: UIView!
    @IBOutlet weak var grpview: UIView!
    
    @IBOutlet weak var subview: UIView!
    @IBOutlet weak var statusview: UIView!
    
    @IBOutlet weak var likeimage: UIImageView!
    @IBOutlet weak var Current: UILabel!
    @IBOutlet weak var preqtrview: UIView!
    @IBOutlet weak var curqtrview: UIView!
    @IBOutlet weak var Biz: UILabel!
    
    @IBOutlet weak var Previous: UILabel!
    @IBOutlet weak var Group: UILabel!
    
    @IBOutlet weak var grplbl: UILabel!
    @IBOutlet weak var preqtrlbl: UILabel!
    
    @IBOutlet weak var statuslbl: UILabel!
    @IBOutlet weak var curqtrlbl: UILabel!
    @IBOutlet weak var bizlbl: UILabel!
    @IBOutlet weak var Nodataavail: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
          bizview.layer.cornerRadius = 8.0
          grpview.layer.cornerRadius = 8.0
          statusview.layer.cornerRadius = 8.0
          preqtrview.layer.cornerRadius = 8.0
          curqtrview.layer.cornerRadius = 8.0
          bizview.clipsToBounds = true
         grpview.clipsToBounds = true
         statusview.clipsToBounds = true
         preqtrview.clipsToBounds = true
         curqtrview.clipsToBounds = true
        
//         grplbl.layer.cornerRadius = 8.0
//         preqtrlbl.layer.cornerRadius = 8.0
//         statuslbl.layer.cornerRadius = 8.0
//         curqtrlbl.layer.cornerRadius = 8.0
//         bizlbl.layer.cornerRadius = 8.0
//         grplbl.clipsToBounds = true
//         preqtrlbl.clipsToBounds = true
//         statuslbl.clipsToBounds = true
//         curqtrlbl.clipsToBounds = true
//         bizlbl.clipsToBounds = true
      
      
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
