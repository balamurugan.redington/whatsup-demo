//
//  HRDMenu_DetailsTableViewCell.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 22/11/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class HRDMenu_DetailsTableViewCell: UITableViewCell
{

    @IBOutlet weak var imageview: UIImageView!
    
    @IBOutlet weak var titlelbl: UILabel!
    
    @IBOutlet weak var Contentview: UIView!
  
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
