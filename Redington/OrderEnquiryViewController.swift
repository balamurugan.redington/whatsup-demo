//
//  OrderEnquiryViewController.swift
//  Redington
//
//  Created by truetech on 29/08/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
import KCFloatingActionButton


class OrderEnquiryViewController: UIViewController,KCFloatingActionButtonDelegate,UITextFieldDelegate{

    @IBOutlet var Submitbutton: UIButton!
    @IBOutlet var Locationtf: UITextField!
    @IBOutlet var TodateTf: UITextField!
    @IBOutlet weak var sonumbertf: UITextField!
    
    @IBOutlet weak var invoicenumbertf: UITextField!
    @IBOutlet var fromdatetf: UITextField!
    @IBOutlet var DatePicekr: UIDatePicker!
    @IBOutlet var Calview: UIView!
    var CustomcodeCheck : String!
    var currenttf=String()
    var fab = KCFloatingActionButton()
    var Movedup=Bool()
    var datevalue = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customnavigation()
        self.layoutFAB()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        let arrow = UIImageView()
        let image = UIImage(named: "cal.png")
        arrow.image = image!.imageWithColor(tintColor: UIColor.init(red:239/255.0 , green: 88/255.0, blue: 88/255.0, alpha: 1.0)).withRenderingMode(.alwaysOriginal)
        arrow.frame = CGRect(x: 0.0,y: 0.0,width: arrow.image!.size.width+10.0,height: 15);
        arrow.contentMode = UIViewContentMode.center
        self.fromdatetf.rightView = arrow;
        self.fromdatetf.rightViewMode = UITextFieldViewMode.always
        let arrow1 = UIImageView()
        let image1 = UIImage(named: "cal.png")
        arrow1.image = image1!.imageWithColor(tintColor: UIColor.init(red:239/255.0 , green: 88/255.0, blue: 88/255.0, alpha: 1.0)).withRenderingMode(.alwaysOriginal)
        arrow1.frame = CGRect(x: 0.0,y: 0.0,width: arrow.image!.size.width+10.0,height: 15);
        arrow1.contentMode = UIViewContentMode.center
        self.TodateTf.rightView = arrow1;
        self.TodateTf.rightViewMode = UITextFieldViewMode.always
        Submitbutton.layer.cornerRadius=15
        fromdatetf.tag = 2
        TodateTf.tag = 3
        sonumbertf.tag = 5
        invoicenumbertf.tag = 6
        Locationtf.tag = 7
        fromdatetf.delegate = self
        TodateTf.delegate = self
        invoicenumbertf.delegate = self
        sonumbertf.delegate = self
        fromdatetf.text=""
        TodateTf.text=""
        invoicenumbertf.text = ""
        sonumbertf.text = ""
        NotificationCenter.default.addObserver(self, selector: #selector(OrderEnquiryViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(OrderEnquiryViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

//
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if CustomcodeCheck == nil || CustomcodeCheck == ""
        {
            self.Locationtf.text = ""
        }
        else
        {
            self.Locationtf.text = LibraryAPI.sharedInstance.CustomerCode
        }
    }

    func keyboardWillShow(notification: NSNotification) {
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true) {
        } else {
            if(Movedup==false) {
                self.view.frame.origin.y -= 150
                Movedup=true
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true)
        {
        }
        else
        {
            self.view.frame.origin.y = 0
            Movedup=false
        }
    }
    
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            self.fab.close()
        }
        fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
            DispatchQueue.main.async {
                self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "OrderEnquiryViewController")), animated: true)
            }
            self.fab.close()
        }
        fab.fabDelegate = self
        self.view.addSubview(fab)
    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
        
    }

    @IBAction func SubmitAction(_ sender: AnyObject) {
        Locationtf.resignFirstResponder()
        sonumbertf.resignFirstResponder()
        invoicenumbertf.resignFirstResponder()
        if (fromdatetf.text != ""  && TodateTf.text != ""  && Locationtf.text != ""   && sonumbertf.text == "" && invoicenumbertf.text == "" ) {
            let lvc=self.storyboard?.instantiateViewController(withIdentifier: "OrderviewDetailsViewController") as! OrderviewDetailsViewController
            LibraryAPI.sharedInstance.CustomerCode = Locationtf.text!
            LibraryAPI.sharedInstance.CustomerCodeFrom = fromdatetf.text!
            LibraryAPI.sharedInstance.CustomerCodeTo = TodateTf.text!
            lvc.fromdate=fromdatetf.text!
            lvc.todate=TodateTf.text!
            self.navigationController?.pushViewController(lvc, animated: true)
            fromdatetf.text=""
            TodateTf.text=""
            Locationtf.text = ""
            CustomcodeCheck = ""
            invoicenumbertf.text = ""
            sonumbertf.text = ""
        } else if(fromdatetf.text=="" && TodateTf.text==""  && Locationtf.text == ""  && invoicenumbertf.text == ""  && sonumbertf.text != "" ) {
            LibraryAPI.sharedInstance.orderno = sonumbertf.text!
            LibraryAPI.sharedInstance.sostatus = "OrderNO"
            let lvc=self.storyboard?.instantiateViewController(withIdentifier: "SonumberViewController") as!SonumberViewController
            self.navigationController?.pushViewController(lvc, animated: true)
            fromdatetf.text=""
            TodateTf.text=""
            Locationtf.text = ""
            CustomcodeCheck = ""
            invoicenumbertf.text = ""
            sonumbertf.text = ""
            
        } else {
        if (fromdatetf.text=="" && TodateTf.text==""  && Locationtf.text == "" && sonumbertf.text == "" && invoicenumbertf.text != "")
          {
            LibraryAPI.sharedInstance.orderno = invoicenumbertf.text!
            LibraryAPI.sharedInstance.sostatus = "InvoiceNO"
            let lvc=self.storyboard?.instantiateViewController(withIdentifier: "SonumberViewController") as!SonumberViewController
            self.navigationController?.pushViewController(lvc, animated: true)
            fromdatetf.text=""
            TodateTf.text=""
            Locationtf.text = ""
            CustomcodeCheck = ""
            invoicenumbertf.text = ""
            sonumbertf.text = ""
            } else {
                let alert = UIAlertController(title: "Sorry..!!", message: "Enter the Dates, Any one selected from Customer code or so number or invoice number", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func DoneAction(_ sender: AnyObject)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.full
        if(currenttf=="to")
        {
        dateFormatter.dateFormat = "MM-dd-yyyy"
        } else {
        dateFormatter.dateFormat = "yyyy-MM-dd"
        }
        if(currenttf=="to") {
            let strDate = dateFormatter.string(from: DatePicekr.date)
            TodateTf.text=strDate
        } else {
          let strDate = dateFormatter.string(from: DatePicekr.date)
            fromdatetf.text=strDate
        }
        Calview.isHidden=true;
    }
    
    
    @IBAction func CancelAction(_ sender: AnyObject) {
          Calview.isHidden=true;
    }
    
    
    //textfieldDelegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        fab.isHidden=true
        if(textField.tag==2) {
            if Locationtf.text == "" {
                let alert = UIAlertController(title: "Sorry..!!", message: "Enter the CustomerCode", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return true
            } else {
                if datevalue == 1 {
                    Calview.isHidden=false
                    fromdatetf.text = ""
                    TodateTf.text = ""
                    DatePicekr.maximumDate = NSDate() as Date
                    Locationtf.resignFirstResponder()
                    fromdatetf.resignFirstResponder()
                    TodateTf.resignFirstResponder()
                       currenttf="From"
                } else{
                    DatePicekr.maximumDate = NSDate() as Date
                    Calview.isHidden=false
                    Locationtf.resignFirstResponder()
                    fromdatetf.resignFirstResponder()
                    TodateTf.resignFirstResponder()
                    currenttf="From"
                }
                return false
            }
        }
        if(textField.tag==3) {
            if Locationtf.text == "" {
                let alert = UIAlertController(title: "Sorry..!!", message: "Enter the CustomerCode", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return true
            } else if fromdatetf.text == "" {
                let alert = UIAlertController(title: "Sorry..!!", message: "Enter the CustomerCode", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return true
            } else {
            Calview.isHidden=false
            Locationtf.resignFirstResponder()
            fromdatetf.resignFirstResponder()
            TodateTf.resignFirstResponder()
            _ = NSDate()
            DatePicekr.minimumDate=DatePicekr.date
            currenttf="to"
            datevalue = 1
            return false
            }
        }
        
        if (textField.tag == 5) {
            self.Locationtf.text = ""
            self.invoicenumbertf.text = ""
            self.fromdatetf.text = ""
            self.TodateTf.text = ""
        }
        
        if (textField.tag == 6)
        {
            self.sonumbertf.text = ""
            self.Locationtf.text = ""
            self.fromdatetf.text = ""
            self.TodateTf.text = ""
        }
        
        if(textField.tag == 7)
        {
            self.sonumbertf.text = ""
            self.invoicenumbertf.text = ""
            LibraryAPI.sharedInstance.Currentcustomerlookup="S/O_Invoice"
            if #available(iOS 10.0, *) {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: CustomerLookupViewController.instance())
                    
                    .didShowHandler { popup in
                        self.Locationtf.resignFirstResponder()
                    }
                    .didCloseHandler { _ in
                        
                }
                
                let container = CustomerLookupViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                }
                popup.show(childViewController: container)
                return false
            } else {
                // Fallback on earlier versions
            }
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
          fab.isHidden=false
        if(textField.tag==1)
        {
            Locationtf.resignFirstResponder()
            
        }
        else if (textField.tag == 5)
        {
            sonumbertf.resignFirstResponder()
        }
        else if(textField.tag == 6)
        {
            invoicenumbertf.resignFirstResponder()
        }
        
        
        
        return true
    }

 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func customnavigation()
        
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(OrderEnquiryViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)

        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Order Enquiry"
        
    }

    func buttonClicked(sender:UIButton)
    {

        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == sonumbertf)
        {
            
           // if string.rangeOfCharacterFromSet(NSCharacterSet.letterCharacterSet()) != nil {
                if string.rangeOfCharacter(from: .letters) != nil {
                guard let text = textField.text else { return true }
                let newLength = text.count + string.count - range.length
                
                let aSet = NSCharacterSet(charactersIn:" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,").inverted
               // let compSepByCharInSet = string.componentsSeparatedByCharactersInSet(aSet)
                     let compSepByCharInSet = string.components(separatedBy: aSet)
                let numberFiltered = compSepByCharInSet.joined(separator: "")
                return (string == numberFiltered) && newLength <= 8
                
            }
            else
            {
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                  
                    return true
                }
                
                return false
                
            }
            
        }
        if(textField == invoicenumbertf)
        {
            
            //if string.rangeOfCharacterFromSet(NSCharacterSet.letterCharacterSet()) != nil {
                if string.rangeOfCharacter(from: .letters) != nil {
                guard let text = textField.text else { return true }
                let newLength = text.count + string.count - range.length
                
                let aSet = NSCharacterSet(charactersIn:" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.,").inverted
                //let compSepByCharInSet = string.componentsSeparatedByCharactersInSet(aSet)
                      let compSepByCharInSet = string.components(separatedBy: aSet)
                let numberFiltered = compSepByCharInSet.joined(separator: "")
                return (string == numberFiltered) && newLength <= 8
                
            }
            else
            {
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                   
                    return true
                }
                
                return false
                
            }
            
        }
        
        return true
    }


  

}

