//
//  BusinesswiseViewController.swift
//  Redington
//
//  Created by truetech on 12/08/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import KCFloatingActionButton

class qtrViewController: UIViewController,KCFloatingActionButtonDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet var menubtn: UIButton!
    @IBOutlet var backbtn: UIButton!
    var months=[String]()
    var pievalues=[Double]()
    var PiechartPercentage=NSNumber()
    var isanimated=Bool()
    var isreloaded=Bool()
    var Isfabclicked=Bool()
    @IBOutlet var DashBoardCollectionView: UICollectionView!
    var collectionViewLayout: CustomImageFlowLayout!
    var responsestring1 = NSMutableArray()
    var fab=KCFloatingActionButton() //do it
    
    var per = NSMutableArray()
    var pie = NSMutableArray()
@IBOutlet weak var Popupview: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customnavigation()
        isanimated=false
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(TargetAcheivementViewController.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        self.layoutFAB()
        collectionViewLayout = CustomImageFlowLayout()
        DashBoardCollectionView.collectionViewLayout = collectionViewLayout
        DashBoardCollectionView.backgroundColor = UIColor.clear
        isreloaded=false
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.init(red:17/255.0 , green: 121/255.0, blue: 32/255.0, alpha: 1.0)], for:.selected)
        
    
    }
    override func viewWillAppear(_ animated: Bool) {
        self.FetchValues()
    }
    
    func FetchValues() {
         self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)SalesDetails.asmx/CurrentFinancialYearBusinessWiseSalesDetails?UserID=\(LibraryAPI.sharedInstance.Userid)&BranchCode=\(LibraryAPI.sharedInstance.BranchCodeNew)&Type=2", completion: { (jsonmutable) in
                self.responsestring1 = jsonmutable
                if(self.responsestring1.count>0) {
                    DispatchQueue.main.async {
                        self.DashBoardCollectionView.isHidden=false
                        self.Popupview.isHidden=true
                        self.DashBoardCollectionView.reloadData()
                         self.local_activityIndicator_stop()
                    }
                } else {
                    DispatchQueue.main.async {
                        self.local_activityIndicator_stop()
                        self.DashBoardCollectionView.isHidden=true
                        let alert = UIAlertController(title: "Sorry..!!", message: "No Data Available", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            })
            DispatchQueue.main.async {
               
            }
        }
    }
    
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                self.Navigate()
            case UISwipeGestureRecognizerDirection.down: break
            case UISwipeGestureRecognizerDirection.left: break
            case UISwipeGestureRecognizerDirection.up: break
            default:
                break
            }
        }
    }
    
    
    override func viewWillLayoutSubviews() {
        let orientation=UIDevice.current.orientation
        if(orientation.isLandscape)
        {
            if(Isfabclicked==true) {
                
            } else {
                DashBoardCollectionView.reloadData()
                Isfabclicked=false
            }
        } else {
            if(Isfabclicked==true) {
                
            } else {
                DashBoardCollectionView.reloadData()
                Isfabclicked=false
            }
        }
    }
    override func viewDidAppear(_ animated: Bool) {
    }
       
    func KCFABOpened(fab: KCFloatingActionButton) {
        
        self.Isfabclicked=true
    }
    
    func layoutFAB() {
        
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            self.Isfabclicked=false
            if (self.navigationController?.topViewController!.isKind(of: DashBoardViewController.classForCoder()) != nil) {
                
                self.navigationController?.popToViewController((self.navigationController?.viewControllers.first)!, animated: true)
            } else {
                let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
                self.navigationController?.pushViewController(lvc, animated: false)
                
            }
            self.fab.close()
        }
        
        fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
            self.Isfabclicked=false
            self.FetchValues()
            self.fab.close()
        }
        fab.fabDelegate = self
        self.view.addSubview(fab)
    }
    
    func customnavigation()
        
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(qtrViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Sales/Target Achieved"
    }
    
    func buttonClicked(sender:UIButton)
    {
        LibraryAPI.sharedInstance.tracker="1"
        if (self.navigationController?.topViewController!.isKind(of: DashBoardViewController.classForCoder()) != nil) {
            self.navigationController?.popToViewController((self.navigationController?.viewControllers.first)!, animated: true)
        }
        else
        {
            let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
            self.navigationController?.pushViewController(lvc, animated: false)
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         return responsestring1.count
    }
    
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath as IndexPath) as! DashBoardCollectionViewCell
        let dic = responsestring1[indexPath.row] as! NSDictionary
        let qtr = dic.object(forKey: "QTRW95") as? String
        let type = dic.object(forKey: "Financial Year") as? String
        cell.TitleLabel.text = "Quarter"+qtr!+" "+type!+" "
        let percentage=dic.object(forKey: "Percentage Achieved")as? NSNumber
        let num=NumberFormatter()
        num.numberStyle=NumberFormatter.Style.currency
        num.locale = NSLocale(localeIdentifier: "en_IN") as Locale!
        let str=num.string(from: NSNumber(value:(dic.object(forKey: "Sales Target") as! Double)))
        let num1=NumberFormatter()
        num1.numberStyle=NumberFormatter.Style.currency
        num1.locale = NSLocale(localeIdentifier: "en_IN") as Locale!
        let str1=num.string(from: NSNumber(value:(dic.object(forKey: "Sales Achieved") as! Double)))
        cell.SalesAcheivedLabel.text=str1
        cell.SalesTargetLabel.text=str
        if(isanimated==false)
        {
            months=["",""]
            pievalues = [(percentage?.doubleValue)!,100-Double(percentage!)]
            cell.setval(Floatval: pievalues)
            cell.pieval.text = String(Int(pievalues[0]))
        }
        else
        {
           
                months=["",""]
                pievalues = [(percentage?.doubleValue)!,100-Double(percentage!)]
            
            
            cell.setval(Floatval: pievalues)
            cell.pieval.text = String(Int(pievalues[0])) + "%"
        }
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let lastRowIndex = collectionView.numberOfItems(inSection: collectionView.numberOfSections-1)
        
        if (indexPath.row == lastRowIndex - 1) {
            isanimated=true
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let orientation = UIApplication.shared.statusBarOrientation
        if(orientation == .landscapeLeft || orientation == .landscapeRight)
        {
            return CGSize(width: collectionView.frame.size.width/2.0-8, height:250)
        }
        else{
            return CGSize(width: collectionView.frame.size.width/2.0-8, height:250)
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(5,5,5,5) // margin between cells
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
   
                    let dic = responsestring1[indexPath.row] as! NSDictionary
                    let year =  dic.object(forKey:"YEAR95") as? NSNumber
                    LibraryAPI.sharedInstance.qtryear = year!
                    let qtr = dic.object(forKey:"QTRW95") as? String
                    LibraryAPI.sharedInstance.qtrtype = qtr!
                    let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "qtr1ViewController") as! qtr1ViewController
                    lvc.PiechartPercentage = PiechartPercentage
                    lvc.pievalues = LibraryAPI.sharedInstance.pievalues
                    self.navigationController?.pushViewController(lvc, animated: false)
                    
    }
    
    override func viewDidLayoutSubviews() {
        
    }
    
    func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        
        switch item.tag {
            
        case 1:
            
            self.Navigate()
            break
            
            
        case 2:
            
            break
            
        default:
            break
            
        }
    }
    
    func Navigate()
    {
        let slideInFromLeftTransition = CATransition()
        slideInFromLeftTransition.type = kCAGravityTop
        slideInFromLeftTransition.subtype = kCATransitionFromLeft
        slideInFromLeftTransition.duration = 0.2
        slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "TargetAcheivementViewController") as! TargetAcheivementViewController
        self.navigationController?.view.layer.add(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
        self.navigationController?.pushViewController(lvc, animated: false)
        
        
    }
    
   
    
}
