//
//  KeyInfoViewController.swift
//  Whatsup
//
//  Created by truetech on 23/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
//import  Cosmos
import KCFloatingActionButton

class KeyInfoViewController: UIViewController,KCFloatingActionButtonDelegate,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet var rateview: RatingView!
    @IBOutlet var VisitCustomerlabel: UILabel!
    @IBOutlet var keyinfotableview: UITableView!
    var responsestring=NSMutableArray()
    
    var totDic = NSMutableDictionary()
    
    var responseString1 = NSMutableArray()
    var keyarr=NSMutableArray()
    var fab=KCFloatingActionButton()
    @IBOutlet var backbtn: UIButton!
    @IBOutlet var menubtn: UIButton!
    
    @IBOutlet var todate: UILabel!
    @IBOutlet var Fromdate: UILabel!
    @IBOutlet var Datevisit: UILabel!
    @IBOutlet var Visitid: UILabel!
    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.customnavigation()
        self.Fetchdata()
        self.layoutFAB()
        NotificationCenter.default.addObserver(self, selector: #selector(KeyInfoViewController.BackGroundRefresh), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)

        
    }
    
    
    func Fetchdata()
    {
         self.local_activityIndicator_start()
        // new version dispatch Que
        
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
            
            self.Loaddata()
            self.ParseVisitkeyinfo()
            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
                
                self.keyinfotableview.reloadData()
               self.local_activityIndicator_stop()
                
            }
        }
        
        // new version dispatch Que
        
        
        
    }
    
    
    func BackGroundRefresh(notification: NSNotification)
    {
        Fetchdata()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func SecurityAction(_ sender: AnyObject)
    {
        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "SecurityViewController") as! SecurityViewController
        self.navigationController?.pushViewController(lvc, animated: true)
    }
    
    
    @IBAction func Statusaction(_ sender: AnyObject)
    {
        
        
        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "VisitlogdetailViewController") as! VisitlogdetailViewController
        self.navigationController?.pushViewController(lvc, animated: true)
        
        
        
    }
    
    
        
    @IBAction func bounceCheque(_ sender: AnyObject) {
    }
    
    @IBAction func Bouncebtnaction(_ sender: AnyObject) {
        
        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "ChequeBounceViewController") as! ChequeBounceViewController
        
        self.navigationController?.pushViewController(lvc, animated: true)
        
    }
    
    
    @IBAction func Productspreadaction(_ sender: AnyObject)
    {
        if LibraryAPI.sharedInstance.valuearray.count > 0
        {
//            let popup = PopupController
//                .create(self.parentViewController!)
//                .show(VistlogpopupViewController.instance())
//                
//                .didShowHandler { popup in
//                }
//                .didCloseHandler { _ in
//            }
//            let container = VistlogpopupViewController.instance()
//            container.closeHandler = { _ in
//                popup.dismiss()
//            }
//            
//            
//            popup.show(container)
        }
        else
        {
            self.NormalAlert(title: "Alert", Messsage: "No Data Available")
           /* let alertController = UIAlertController(title: "Alert", message: "No Data Available" , preferredStyle: .Alert)
            let cancelAction = UIAlertAction(title: "Ok", style: .Cancel) { (action:UIAlertAction!) in
                
            }
            alertController.addAction(cancelAction)
            self.presentViewController(alertController, animated: true, completion:nil)*/
        }
        
        
       
    }
    
    @IBAction func chequepending(_ sender: AnyObject)
    {
        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "ChequeBounceDetailsViewController") as! ChequeBounceDetailsViewController
        LibraryAPI.sharedInstance.keyinfochepending = 1
        let defaults = UserDefaults.standard
        defaults.set("2", forKey: "ViewCheckBool")
        self.navigationController?.pushViewController(lvc, animated: true)
        
        
    }
   
    func customnavigation()
        
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(KeyInfoViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Key Information"
    }
    
    
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            
            
            
            self.fab.close()
        }
        
        fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
            
            self.Fetchdata()
            
            self.fab.close()
        }
        
        fab.fabDelegate = self
        
        self.view.addSubview(fab)
        
    }
    
    func ParseVisitkeyinfo()
    {
        if(self.connectedToNetwork())
        {
            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/ProductSpread?Customer=\(LibraryAPI.sharedInstance.CustomerCode)&User=\(LibraryAPI.sharedInstance.Userid)") { (jsonmutable) in
                if(self.keyarr.count>0)
                {
                    self.keyinfotableview.isHidden=false
                }
                else
                {
                }
                LibraryAPI.sharedInstance.valuearray=self.keyarr
            }
        }
        else
        {
           
        }

        }
    
        
        
    func KCFABOpened(fab: KCFloatingActionButton) {
    }
    
    
    func buttonClicked(sender:UIButton)
    {
        LibraryAPI.sharedInstance.tracker="1"
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
//        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        appdelegate.shouldSupportAllOrientation = true
        
        keyinfotableview.reloadData()
    }
    
    //TableView Delegates
    func numberOfSections(in tableView: UITableView) -> Int {

        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
        var num = CGFloat()
        if(indexPath.row==0)
        {
            num=483
        }
        else if(indexPath.row==1)
        {
            num=260.0
        }
        
        return num;//Choose your custom row height
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        var val = Int()
        if(responsestring.count==0)
        {
            val=0
        }
        else{
            val=2
        }
        
        return val
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
        if(indexPath.row==0)
        {
            var cell : KeyinfoTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "Cell1") as! KeyinfoTableViewCell
            if(cell == nil)
            {
                cell = Bundle.main.loadNibNamed("Cell1", owner: self, options: nil)![0] as! KeyinfoTableViewCell;
                
            }
         let dic1 =  responsestring[indexPath.row] as! NSDictionary
            
            let time = dic1.object(forKey: "FTME7W") as! String
            if(time=="")
            {
                cell.updatebtn.isHidden=true
            }
            else
            {
                cell.updatebtn.isHidden=false
                
            }
            
            let rating=(dic1.object(forKey: "SALES FREQUENCY") as? Double)!
            cell.Rateview.settings.starSize=20
            cell.Rateview.settings.totalStars=5
            cell.Rateview.rating=rating
            cell.Rateview.settings.updateOnTouch=false
            
            let date=dic1.object(forKey: "DATV7W") as? String
            cell.lastpaymentvalue.text=String(dic1.object(forKey: "LAST PAYMENT VALUE") as! Double)
            cell.lastpaymentdate.text=String(dic1.object(forKey: "LAST PAYMENT DATE") as! String)
            
            let partnerprofile = dic1.object(forKey: "PARTNER PROFILE") as? String
            let profiledesc = dic1.object(forKey: "PROFILE DESC") as? String
            cell.partner.text = partnerprofile!+" "+"-"+profiledesc!+" "
            
            
         //   cell.Userid.text=date!.stringByAppendingString(" \(time)")
             cell.Userid.text=date!.appending(" \(time)")
            cell.saleshealthlabel.text=dic1.object(forKey: "SALES HEALTH") as? String
            cell.Marginrankinglabel.text=String(dic1.object(forKey: "MARGIN RANKING") as! Int)
            cell.salesrankinglabel.text=String(dic1.object(forKey: "TURNOVER RANKING") as! Int)
            cell.Productspreadlabel.text=String(dic1.object(forKey: "PRODUCT SPREAD") as! Int)
            cell.BusinessHealthLabel.text=String(dic1.object(forKey: "BUSINESS HEALTH") as! String)
            cell.labelInvoceno.text=String(dic1.object(forKey: "LAST INVOICE NO") as! String)
            cell.labelInvocedate.text=String(dic1.object(forKey: "LAST INVOICE DATE") as! String)
            cell.labelInvocevalue.text=String(dic1.object(forKey: "LAST INVOICE VALUE") as! Double)
            
            cell.exposuredate.text=String(dic1.object(forKey: "HIGHST EXPOSURE DATE") as! String)
            
            cell.exposure.text=String(dic1.object(forKey: "HIGHEST EXPOSURE") as! Int)
            
            
            let num=NumberFormatter()
            num.numberStyle=NumberFormatter.Style.currency
            num.locale = NSLocale(localeIdentifier: "en_IN") as Locale!
            let str=num.string(from: NSNumber(value:(dic1.object(forKey: "HIGHEST SALES VALUE") as! Double)))
            
            cell.Salelabel.text=str
            
            
            let num1=NumberFormatter()
            num1.numberStyle=NumberFormatter.Style.currency
            num1.locale = NSLocale(localeIdentifier: "en_IN") as Locale!
            let str1=num.string(from: NSNumber(value:(dic1.object(forKey: "CREDIT LIMIT") as? Double)!))
            
            cell.creditlimitlabel.text=str1
            
            
            let num2=NumberFormatter()
            num2.numberStyle=NumberFormatter.Style.currency
            num2.locale = NSLocale(localeIdentifier: "en_IN") as Locale!
            let str2=num.string(from: NSNumber(value:(dic1.object(forKey:  "INSURANCE LIMIT") as? Double)!))
            
            cell.Insurancelabel.text=str2
            
            
            if(dic1.object(forKey: "MARGIN FLAG") as? String == "U")
            {
                cell.DislikeMArgin.image=UIImage(named: "like@2x11.png")
            }
            else
            {
                cell.DislikeMArgin.image=UIImage(named: "dislike@3x11.png")
            }
            
            if(dic1.object(forKey: "TURNOVER FLAG") as? String == "U")
            {
                cell.Likesales.image=UIImage(named: "like@2x11.png")
            }
            else
            {
                cell.Likesales.image=UIImage(named: "dislike@3x11.png")
            }
            
            cell.updatebtn.addTarget(self, action: #selector(KeyInfoViewController.Statusaction), for: UIControlEvents.touchUpInside)
            cell.spreadbtn.addTarget(self, action: #selector(KeyInfoViewController.Productspreadaction), for: UIControlEvents.touchUpInside)
            cell.chequepending.addTarget(self, action: #selector(KeyInfoViewController.chequepending), for: UIControlEvents.touchUpInside)
            
            cell.selectionStyle = .none
            cell.contentView.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1.0)
            let whiteRoundedView : UIView = UIView(frame: CGRect(x: 0,y: 1,width: cell.contentView.frame.size.width,height: cell.contentView.frame.size.height - 10))
            
            whiteRoundedView.layer.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0).cgColor
            whiteRoundedView.layer.masksToBounds = false
            whiteRoundedView.layer.cornerRadius = 7.0
            whiteRoundedView.layer.shadowOffset = CGSize(width: 1, height: 1)
            whiteRoundedView.layer.shadowOpacity = 0.2
            whiteRoundedView.layer.shadowRadius = 2
            return cell as KeyinfoTableViewCell
        }
        else
        {
            var cell : Keyinfo2TableViewCell! = tableView.dequeueReusableCell(withIdentifier: "Cell2") as! Keyinfo2TableViewCell
            if(cell == nil)
            {
                cell = Bundle.main.loadNibNamed("Cell2", owner: self, options: nil)![0] as! Keyinfo2TableViewCell;
                
            }
            
            let dic2 = responseString1[0] as! NSDictionary
            
            cell.Orderslabel.text=String(dic2.object(forKey: "TOTAL ORDER COUNT") as! Int)
            cell.Creditdayslabel.text=String(dic2.object(forKey: "AVERAGE CREDIT DAYS") as! Int)
            
            cell.saleshealthscore.text=String(dic2.object(forKey: "NO OF BRANCH HOLD") as! Int)
            
            var str = String(dic2.object(forKey: "NO OF BRANCH HOLD %") as! Int)
           // str=str.stringByAppendingString("%")
             str=str.appending("%")
            cell.Salespercentage.text=str
            
            cell.Bonuschequehold.text=String(dic2.object(forKey: "BOUNCE CHEQUE COUNT") as! Int)
            
            
            
            var str1 = String(dic2.object(forKey: "BOUNCE CHEQUE COUNT%") as! Int)
           // str1=str1.stringByAppendingString("%")
           str1=str1.appending("%")
            cell.Bonuschequespercentage.text=str1
            
            
            
            cell.creditholdscore.text=String(dic2.object(forKey: "NO OF CREDIT HOLD") as! Int)
            
            var str2 = String(dic2.object(forKey: "NO OF CREDIT HOLD %") as! Int)
            str2=str2.appending("%")
            
            cell.creditpercentage.text=str2
            
            
            cell.Boundschqsbtn.addTarget(self, action: #selector(KeyInfoViewController.Bouncebtnaction), for: UIControlEvents.touchUpInside)
            
            cell.selectionStyle = .none
            
            
            cell.contentView.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1.0)
            let whiteRoundedView : UIView = UIView(frame: CGRect(x: 0,y: 5, width: cell.contentView.frame.size.width,height: cell.contentView.frame.size.height - 10))
            
            whiteRoundedView.layer.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0).cgColor
            whiteRoundedView.layer.masksToBounds = false
            whiteRoundedView.layer.cornerRadius = 2.0
            whiteRoundedView.layer.shadowOffset = CGSize(width: 1, height: 1)
            whiteRoundedView.layer.shadowOpacity = 0.2
            
            
            cell.Boundschqsbtn.layer.cornerRadius = cell.Boundschqsbtn.frame.width/2
            
            return cell as Keyinfo2TableViewCell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        
    }
    
    func Loaddata()
    {
        self.local_activityIndicator_start()
        //responsestring = LibraryAPI.sharedInstance.Keyinfoparse(url: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/Customer360degreeview?Customer=\(LibraryAPI.sharedInstance.CustomerCode)&User=\(LibraryAPI.sharedInstance.Userid)")
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/Customer360degreeview?Customer=\(LibraryAPI.sharedInstance.CustomerCode)&User=\(LibraryAPI.sharedInstance.Userid)") { (jsonmutable) in
            self.responsestring = jsonmutable
        
       
        if(self.responsestring.count>0)
        {
                DispatchQueue.main.async {
                    self.local_activityIndicator_stop()
                    self.keyinfotableview.isHidden=false
                     self.keyinfotableview.reloadData()
                }
            for i in 0..<self.responsestring.count
            {
                let dicRes1 = self.responsestring[i] as! NSDictionary
                if(self.responsestring.count == 1)
                {
                    self.totDic.setValue(dicRes1.object(forKey: "TOTAL ORDER COUNT") as! Int, forKey: "TOTAL ORDER COUNT")
                    self.totDic.setValue(dicRes1.object(forKey: "AVERAGE CREDIT DAYS") as! Int, forKey: "AVERAGE CREDIT DAYS")
                    self.totDic.setValue(dicRes1.object(forKey: "NO OF BRANCH HOLD") as! Int, forKey: "NO OF BRANCH HOLD")
                    self.totDic.setValue(dicRes1.object(forKey: "NO OF BRANCH HOLD %") as! Int, forKey: "NO OF BRANCH HOLD %")
                    self.totDic.setValue(dicRes1.object(forKey: "BOUNCE CHEQUE COUNT") as! Int, forKey: "BOUNCE CHEQUE COUNT")
                    self.totDic.setValue(dicRes1.object(forKey: "BOUNCE CHEQUE COUNT%") as! Int, forKey: "BOUNCE CHEQUE COUNT%")
                    self.totDic.setValue(dicRes1.object(forKey: "NO OF CREDIT HOLD") as! Int, forKey: "NO OF CREDIT HOLD")
                    self.totDic.setValue(dicRes1.object(forKey: "NO OF CREDIT HOLD %") as! Int, forKey: "NO OF CREDIT HOLD %")
                    self.totDic.setValue(dicRes1.object(forKey: "COLOR") as! String, forKey: "COLOR")
                    self.responseString1.add(self.totDic)
                    
                }
                
            }
        }
        else
        {
            DispatchQueue.main.async {
                
                self.local_activityIndicator_stop()
                self.keyinfotableview.isHidden=true
            }
                  }
        }
        
    }
    
    
    func customErrorPage()
    {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "ApiErrorViewController") as! ApiErrorViewController
         customView.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        customView.retrybtn.addTarget(self, action: #selector(KeyInfoViewController.retrybuttonClicked), for: UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
    }
    
    func retrybuttonClicked(sender: UIButton)
    {
        if self.connectedToNetwork()
        {
            let lvc=self.storyboard?.instantiateViewController(withIdentifier: "KeyInfoViewController") as! KeyInfoViewController
            self.navigationController?.pushViewController(lvc, animated: true)
        }
        else
        {
            
        }
    }
}
