//
//  MenuViewController.swift
//  Redington
//
//  Created by truetech on 24/08/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation
//import NVActivityIndicatorView
class MenuViewController: UIViewController,CLLocationManagerDelegate, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var UsernameLabel: UILabel!
    
    @IBOutlet var CloseButton: UIButton!
  //  @IBOutlet var MenuTableview: UITableView!
    @IBOutlet weak var MenuTableview: UITableView!
    
    var tmparray=[String]()
    var imagearray=[String]()
     var locationManager=CLLocationManager()
    var closeHandler: (() -> Void)?
    var responsestring = NSMutableArray()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
         NotificationCenter.default.addObserver(self, selector: #selector(self.notificationmethod), name: NSNotification.Name(rawValue: "GetNotification"), object: nil)
        //MenuTableview.register(UINib(nibName: "Cell", bundle: nil), forCellReuseIdentifier: "Cell")
        
            tmparray=["HRD-MENU","PRE ORDER-MENU","Visit Log","Seats Bookings","Stock Enquiry","S/O Invoice Enquiry","Stock Enquiry all Location","Cheque Pending Details","Cust Search (Pin Code)","Partner Registration","Demo Stock","Pre Order History","SPC Status","Stock Transfer status","Quick Reference Guide","Logout"]
        imagearray=["Preorder.png","Preorder.png","Order.png","Stock.png","stock_enquiry_all_location.png","cheque_pending.png","cheque_pending.png","Visitlog.png","his.png","pin_code.png","his.png","his.png","his.png","Preorder.png","his.png","his.png","Logout.png"]
        MenuTableview.reloadData()
        MenuTableview.scrollsToTop = true
       }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
      
    }
    override func viewWillAppear(_ animated: Bool) {
         UsernameLabel.text=LibraryAPI.sharedInstance.LiveUserName
    }
    
    //TableView Delegates

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 16
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : MenucellTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as! MenucellTableViewCell
        
        if((tmparray[indexPath.row] == "HRD-MENU")  || (tmparray[indexPath.row] == "PRE ORDER-MENU"))
        {
            // cell.mainview.layer.borderWidth = 1.0
            cell.Subview.layer.borderColor = UIColor.white.cgColor
            cell.Subview.layer.cornerRadius = 5.0
            cell.Subview.layer.backgroundColor = UIColor(red: 239/255, green: 88/255, blue: 88/255, alpha: 1.0).cgColor
        }
        else
        {
            cell.mainview.layer.borderColor = UIColor.clear.cgColor
            cell.Subview.layer.backgroundColor = UIColor.clear.cgColor
        }
        
        cell.selectionStyle = .none
        cell.MenuLabel.text=tmparray[indexPath.row]
        cell.Imagemenu.image=UIImage(named: imagearray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let selectedCell:UITableViewCell = MenuTableview.cellForRow(at: indexPath as IndexPath)!
        LibraryAPI.sharedInstance.ismapopened=false
        if self.connectedToNetwork(){
            
        
        switch (indexPath.row) {
        case 0:
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "HRD1DetailsViewController")), animated: true)
            self.sideMenuViewController!.hideMenuViewController()
            selectedCell.contentView.backgroundColor = self.view.backgroundColor
            break
        case 1: 
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "PreorderMainViewController")), animated: true)
            self.sideMenuViewController!.hideMenuViewController()
            selectedCell.contentView.backgroundColor = self.view.backgroundColor
            break
        case 2:
            if (CLLocationManager.locationServicesEnabled())
            {
                locationManager = CLLocationManager()
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.requestAlwaysAuthorization()
                locationManager.requestWhenInUseAuthorization()
                locationManager.startUpdatingLocation()
                
                
                self.local_activityIndicator_start()
                // new version dispatch Que
                
                DispatchQueue.global(qos: .background).async {
                    self.getStringsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=WHATSUP&Option=4", completion: { (jsonstr) in
                        let responsestring = jsonstr
                        DispatchQueue.main.async {
                            if(responsestring == "Y")
                            {
                                self.local_activityIndicator_stop()
                                self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "VisitLogViewController")), animated: true)
                                self.sideMenuViewController!.hideMenuViewController()
                                selectedCell.contentView.backgroundColor = self.view.backgroundColor
                            }
                            else
                            {
                                self.local_activityIndicator_stop()
                                let alertController = UIAlertController(title: "Alert", message:" You are Not Authorised for this option" , preferredStyle: .alert)
                                let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                    
                                }
                                alertController.addAction(cancelAction)
                                self.present(alertController, animated: true, completion:nil)
                            }
                            
                        }
                    })
                    // Go back to the main thread to update the UI
                    
                }
                
                // new version dispatch Que
                
                
            }
            else
                
            {
                let alertController = UIAlertController (title: "Alert", message: "Please allow the Location Access", preferredStyle: .alert)
                
                let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
                    let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.shared.openURL(url as URL)
                    }
                }
                
                alertController.addAction(settingsAction)
                present(alertController, animated: true, completion: nil)
                
            }
            
            
            break
            
        case 3:
            
            self.local_activityIndicator_start()
            // new version dispatch Que
            
            DispatchQueue.global(qos: .background).async {
                
                // Validate user input
                
                
                //let responsestring = LibraryAPI.sharedInstance.Parsejson1(url: "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=WHATSUP&Option=5")
                self.getStringsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)users.asmx/MenuCheck?UserID=\(LibraryAPI.sharedInstance.Userid)&Menu=WHATSUP&Option=5", completion: { (jsonstr) in
                    let responsestring = jsonstr
                    DispatchQueue.main.async {
                        if(responsestring == "Y")
                        {
                            self.local_activityIndicator_stop()
                            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "SeatBooking1ViewController")), animated: true)
                            self.sideMenuViewController!.hideMenuViewController()
                            selectedCell.contentView.backgroundColor = self.view.backgroundColor
                            
                        }
                        else
                        {
                            self.local_activityIndicator_stop()
                            let alertController = UIAlertController(title: "Alert", message:" You are Not Authorised for this option" , preferredStyle: .alert)
                            
                            let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                
                            }
                            alertController.addAction(cancelAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                })
                // Go back to the main thread to update the UI
                
            }
            
            // new version dispatch Que
            
            break
        case 4:
            
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "StockEnquiryViewController")), animated: true)
            self.sideMenuViewController!.hideMenuViewController()
            selectedCell.contentView.backgroundColor = self.view.backgroundColor
            break
        case 5:
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "OrderEnquiryViewController")), animated: true)
            self.sideMenuViewController!.hideMenuViewController()
            selectedCell.contentView.backgroundColor = self.view.backgroundColor
            break
            
       

        case 6:
            
            LibraryAPI.sharedInstance.Currentcustomerlookup="StockInquiry"
            
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: StockInquiryPopupController.instance())
                
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
            }
            let container = StockInquiryPopupController.instance()
            container.closeHandler = { _ in
                popup.dismiss()
            }
            
            popup.show(childViewController: container)
            break
            
        case 7:
          
            let defaults = UserDefaults.standard
            defaults.set("1", forKey: "ViewCheckBool")
            LibraryAPI.sharedInstance.BranchCodeCheck = 2
            LibraryAPI.sharedInstance.BranchCodeBtnCheck = 2
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: branchesViewController.instance())
                
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
                    
                    
            }
            let container = branchesViewController.instance()
            container.closeHandler = { _ in
                
                popup.dismiss()
            }
            
            
            popup.show(childViewController: container)
            break
            
        case 8:
            
            LibraryAPI.sharedInstance.Currentcustomerlookup="CustomerSearch"
            
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: CustomerSearchPopupViewController.instance())
                
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
            }
            let container = CustomerSearchPopupViewController.instance()
            container.closeHandler = { _ in
                popup.dismiss()
            }
            
            popup.show(childViewController: container)
            break
            
        case 9:
            
            LibraryAPI.sharedInstance.BranchCodeCheck = 3
            LibraryAPI.sharedInstance.BranchCodeBtnCheck = 3
            
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: branchesViewController.instance())
                
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
            }
            let container = branchesViewController.instance()
            container.closeHandler = { _ in
                
                popup.dismiss()
            }
            popup.show(childViewController: container)
            break
        case 10:
               self.local_activityIndicator_start()
                // new version dispatch Que
                DispatchQueue.global(qos: .background).async {
                    print("url in demo stock","\(LibraryAPI.sharedInstance.GlobalUrl)preorder.asmx/POC_Details?UserID=\(LibraryAPI.sharedInstance.Userid)&Type=")
                    self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)preorder.asmx/POC_Details?UserID=\(LibraryAPI.sharedInstance.Userid)&Type=", completion: { (jsonmutable) in
                        self.responsestring = jsonmutable
                    DispatchQueue.main.async {
                        if (self.responsestring.count > 0)
                        {
                            let dicres = self.responsestring[0] as! NSDictionary
                            if dicres["Message"] != nil  {
                               self.local_activityIndicator_stop()
                                 self.NormalAlert(title: "Alert", Messsage: "No Records Found")
                            } else {
                               self.local_activityIndicator_stop()
                                LibraryAPI.sharedInstance.Poc_demounitlbl = String(dicres.object(forKey: "REDINGTON QUANTITY") as! Int)
                                LibraryAPI.sharedInstance.Poc_demounitlbl = String(dicres.object(forKey:"PARTNER QUANTITY") as! Int)
                                
                                self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "PocViewController")), animated: true)
                                self.sideMenuViewController!.hideMenuViewController()
                                selectedCell.contentView.backgroundColor = self.view.backgroundColor
                            }
                        } else {
                            self.NormalAlert(title: "Alert", Messsage: "No Records Found")
                        }
                        
                        
                    }
                })
                }
                
                // new version dispatch Que
                           break
            
        case 11:
            
            
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "PreOrderHistroyViewController")), animated: true)
            self.sideMenuViewController!.hideMenuViewController()
            selectedCell.contentView.backgroundColor = self.view.backgroundColor
            break
            
        case 12:
            
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "SPCViewController")), animated: true)
             self.sideMenuViewController!.hideMenuViewController()
             selectedCell.contentView.backgroundColor = self.view.backgroundColor
             break
 
            
       
        case 13:
            
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "StockTransferStatusViewController")), animated: true)
            self.sideMenuViewController!.hideMenuViewController()
            selectedCell.contentView.backgroundColor = self.view.backgroundColor
            break
        
            
       

        
        case 14:
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "WebViewController")), animated: true)
            self.sideMenuViewController!.hideMenuViewController()
            selectedCell.contentView.backgroundColor = self.view.backgroundColor
            break
            
 
        case 15:
            selectedCell.contentView.backgroundColor = self.view.backgroundColor
            //selectedCell.selectionStyle = .None
            
            let alertController = UIAlertController(title: "Alert", message: "Do you want to Logout?", preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
            }
            alertController.addAction(cancelAction)
            
            let OKAction = UIAlertAction(title: "confirm", style: .default) { (action:UIAlertAction!) in
                
                LibraryAPI.sharedInstance.LoginCredentials.removeObject(forKey: "IsClicked")
                LibraryAPI.sharedInstance.SelectedValue = ""
                LibraryAPI.sharedInstance.BizCode = ""
                TargetService.shared.Clearalldata()
                dashboardapidata.sharevariable.EmployeeCode = ""
//                let appDomain = NSBundle.mainBundle().bundleIdentifier!
//                NSUserDefaults.standardUserDefaults().removePersistentDomainForName(appDomain)
                
                LibraryAPI.sharedInstance.isloggedout=true
                
                self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "ViewController")), animated: true)
                self.sideMenuViewController!.hideMenuViewController()
                selectedCell.contentView.backgroundColor = self.view.backgroundColor
            }
            alertController.addAction(OKAction)
            
            self.present(alertController, animated: true, completion:nil)

            break
              default:
            break
            
        }
        }else{
            self.showalert(title: "Oops", message: "Your are not connected to internet activate WIFI or Mobile data to access", buttontxt: "ok")
        }
        
      
    }
    
       @IBAction func CloseAction(_ sender: AnyObject) {
      
        sideMenuViewController?.hideMenuViewController()
    }
    
    
    func notificationmethod()
    {
        let userdefaults = Foundation.UserDefaults.standard
        let value = userdefaults.dictionary(forKey: "Notification")
        
        var title: String!
        var message: String!
        var imagestr: String!
        title = value?["title"] as! String
        message = value?["body"] as! String
        imagestr = value?["imageurl"] as! String
        
        if imagestr != nil
        {
            if imagestr != ""
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: notificationimageViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = notificationimageViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                    
                }
                popup.show(childViewController: container)
            }
            else
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: PushnotificationtextViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = PushnotificationtextViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                }
                
                popup.show(childViewController: container)
            }
        }
        else
        {
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: PushnotificationtextViewController.instance())
                
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
            }
            let container = PushnotificationtextViewController.instance()
            container.closeHandler = { _ in
                popup.dismiss()
            }
            
            popup.show(childViewController: container)
        }
        
       }

}
