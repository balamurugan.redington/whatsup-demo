//
//  TargetAcheivemntTableViewCell.swift
//  Redington
//
//  Created by truetech on 12/08/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
import Charts


class TargetAcheivemntTableViewCell: UITableViewCell {

    @IBOutlet var AmountLabel: UILabel!
    @IBOutlet var AcheivedLabel: UILabel!
    @IBOutlet var ChartsView: PieChartView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setChart(dataPoints: [String], values: [Double],isanimated:Bool) {
        var dataEntries: [ChartDataEntry] = []
        for i in 0..<dataPoints.count {
            
            let dataEntry = ChartDataEntry(x: values[i], y: Double(i))
            dataEntries.append(dataEntry)
            
        }
        
        
        let pieChartDataSet = PieChartDataSet(values: dataEntries, label: "Target/Sales")
        
        //let pieChartData = PieChartData(xVals: dataPoints, dataSet: pieChartDataSet)
        let pieChartData = PieChartData(dataSet: pieChartDataSet)
        pieChartData.setDrawValues(false)
        
//        ChartsView.data = pieChartData
//        ChartsView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: ChartEasingOption.EaseInCirc)
//        ChartsView.legend.enabled = false
        var colors: [UIColor] = []
        
        for _ in 0..<dataPoints.count {
            
            colors.append(UIColor.init(red: 30/255.0, green: 184/255.0, blue: 120/255.0, alpha: 1.0))
            colors.append(UIColor.init(red: 239/255.0, green: 88/255.0, blue: 88/255.0, alpha: 1.0))


        }
        
        pieChartDataSet.colors = colors
        
        
    }
    
    
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
