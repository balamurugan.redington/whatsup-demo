//
//  CallFunction.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 30/08/18.
//  Copyright © 2018 truetech. All rights reserved.
//
import UIKit
import Foundation
import Alamofire

class CallFunction{
    static let shared = CallFunction()
    var ReturnDictionary   = NSDictionary()
    
    func PostMethod(UrlString: String,PostData: NSMutableDictionary) -> NSDictionary {
        let request = NSMutableURLRequest(url:NSURL(string: UrlString )! as URL)
        request.httpMethod = "POST"
        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
        request.httpBody = try? JSONSerialization.data(withJSONObject: PostData, options: JSONSerialization.WritingOptions(rawValue: 0))
        //print(PostData)
        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
        do
        {
            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
            ReturnDictionary = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            print("returning the data ",ReturnDictionary)
        }
        catch(let Error)
        {
            print(Error)
        }
        return ReturnDictionary
    }
   
}
