//
//  AllBranchesTableViewCell.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 18/12/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class AllBranchesTableViewCell: UITableViewCell {

    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var branchnmaelbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        view1.layer.cornerRadius = view1.frame.size.width/2
        view1.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
