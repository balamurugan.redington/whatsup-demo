//
//  TagetAchieveTableViewCell.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 08/12/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit
import Charts

class TagetAchieveTableViewCell: UITableViewCell {
    
    @IBOutlet weak var pieval: UILabel!
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var PieChartViews: UIView!
    @IBOutlet weak var defaultbtn1: UIButton!
    @IBOutlet weak var defaultbtn2: UIButton!
    
    var dollars1 = [Double]()
    let shapelayer = CAShapeLayer()
    let shapelayer2 = CAShapeLayer()
    var PieVal = Float()
    var val = Float()
    override func awakeFromNib() {
        super.awakeFromNib()
         PieChartViews.layer.cornerRadius = 5.0
         PieChartViews.clipsToBounds = true
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setChart(DouVal: [Double]) {
        PieVal = Float(DouVal[0])
        let circlepath = UIBezierPath(arcCenter: CGPoint(x: pieval.frame.origin.x + pieval.frame.size.width/2.8 ,y: PieChartViews.frame.origin.y+(PieChartViews.frame.size.width/3.17)), radius: PieChartViews.frame.size.width / 3.5, startAngle: -CGFloat.pi / 2, endAngle: 2 * CGFloat.pi, clockwise: true)
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 21))
        label.center = CGPoint(x: pieval.frame.origin.x + pieval.frame.size.width/2.8 ,y: PieChartViews.frame.origin.y+(PieChartViews.frame.size.width/3.17))
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 14.0)
        label.textAlignment = .center
        label.text = String(Int(PieVal)) + "%"
        self.shapelayer2.path = circlepath.cgPath
        self.shapelayer2.strokeColor = UIColor.red.cgColor
        self.shapelayer2.lineWidth = 10
        self.shapelayer2.strokeEnd = 1
        self.shapelayer.path = circlepath.cgPath
        self.shapelayer.strokeColor = CustomColor.ShareColor.hexStringToUIColor(hex: CustomColor.ShareColor.CustomGreen, alpha: 1.0).cgColor
        self.shapelayer.lineWidth = 10
        self.shapelayer.strokeEnd = 0
        self.shapelayer.fillColor = UIColor.clear.cgColor
        self.shapelayer2.fillColor = UIColor.clear.cgColor
        self.view.addSubview(self.PieChartViews)
        self.PieChartViews.layer.addSublayer(shapelayer2)
        self.PieChartViews.layer.addSublayer(shapelayer)
        self.PieChartViews.addSubview(label)
        self.animatepieview()
    }
    
    @objc private func animatepieview(){
        let baseanimate = CABasicAnimation(keyPath: "strokeEnd")
        print("pieval :",PieVal)
        let divVal = PieVal / 100
        let con = Float(divVal)
        if con == 0.0 {
            val = 0.0
        }else if con < 0.1 {
            val = 0.10
        }else if con < 0.2 {
            val = 0.20
        }else if con < 0.3 {
            val = 0.30
        }else if con < 0.4 {
            val = 0.35
        }else if con < 0.5 {
            val = 0.45
        }else if con < 0.6 {
            val = 0.55
        } else if con < 0.6 {
            val = 0.65
        } else if con < 0.7 {
            val = 0.70
        } else if con < 0.8 {
            val = 0.75
        } else if con < 1.0 {
            val = 0.78
        } else {
            val = 1.0
        }
        baseanimate.toValue = val
        print("my val :",val)
        baseanimate.duration = 2
        baseanimate.fillMode = kCAFillModeForwards
        baseanimate.isRemovedOnCompletion = false
        shapelayer.add(baseanimate, forKey: "urSoBasic")
    }
}
