//
//  CustomerLookupViewController.swift
//  Redington
//
//  Created by truetech on 14/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
import Speech
//import  NVActivityIndicatorView
import Firebase



@available(iOS 10.0, *)
class CustomerLookupViewController: UIViewController,PopupContentViewController,Customdelegate,SFSpeechRecognizerDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var customsearchimageview: UIImageView!
    @IBOutlet var AlertLabel: UILabel!
    @IBOutlet var customeryf: UITextField!
    var closeHandler: (() -> Void)?
    var responsearray=NSMutableArray()
    var responsestring=NSMutableArray()
    var response=NSMutableArray()
    
    @IBOutlet weak var customersearchlbl1: UILabel!
    @IBOutlet var okbtn: UIButton!
    var ordarray=NSMutableArray()
    var cityarr : NSMutableArray!
    var lookuparr=NSMutableArray()
    var stockroomResponse=NSMutableArray()
    var stockRoomName : NSMutableArray!
    var Movedup=Bool()
    var timer = Timer()
    @IBOutlet weak var microphoneButton: UIButton!
    @IBOutlet weak var Branchcodetf: UITextField!
    @IBOutlet weak var branchcodelbl: UILabel!
    var speech=SpeechFile()
    var localcheck : Bool!
    var uploadfile=NSMutableDictionary()
    var jsonresult=NSDictionary()
    var customercodetf = String()
    var orderamount = ""
    var credittf = ""
    var remarks = ""
    var biz1 = ""
    var biz2 = ""
    var biz3 = ""
    var biz4 = ""
    var biz5 = ""
    var userid = String()
    var selectedstringbranch: String!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(CustomerLookupViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CustomerLookupViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CustomerLookupViewController.BackgroundVoice), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        
        //       NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(CustomerLookupViewController.BackgroundVoice(_:)), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        
        
        Branchcodetf.delegate = self
        Branchcodetf.tag = 1
        
        customsearchimageview.isHidden = false
        
        if #available(iOS 10.0, *) {
            // speech = SpeechFile()
            microphoneButton.isHidden=false
            speech.speechRecognizer.delegate = self
            
            SFSpeechRecognizer.requestAuthorization { (authStatus) in
                
                var isButtonEnabled = false
                
                switch authStatus {
                case .authorized:
                    isButtonEnabled = true
                    
                case .denied:
                    isButtonEnabled = false
                    
                case .restricted:
                    isButtonEnabled = false
                    
                case .notDetermined:
                    isButtonEnabled = false
                }
                
                OperationQueue.main.addOperation({
                    
                    self.microphoneButton.isEnabled = isButtonEnabled
                    
                })
            }
            
        } else {
            microphoneButton.isHidden=true
            
        }
        
        //   microphoneButton.enabled = false
        
        
        
        
        okbtn.layer.cornerRadius=10.0
        
        
        okbtn.alpha=0.5
        
        
        okbtn.isUserInteractionEnabled=false
        
        
        
        
        AlertLabel.isHidden=true
        
        localcheck = false
        
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Closenotification"), object: nil)
        
        // Do any additional setup after loavarg the view.
    }
    
    
    func keyboardWillShow(notification: NSNotification) {
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true) {
        } else {
            if(Movedup==false) {
                self.view.frame.origin.y -= 150
                Movedup=true
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true)
        {
        } else {
            self.view.frame.origin.y = 0
            Movedup=false
        }
    }
    
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        if(LibraryAPI.sharedInstance.Currentcustomerlookup=="City") {
            customsearchimageview.image = UIImage(named: "2018-01-04 (1).png")
            Branchcodetf.isHidden = true
            branchcodelbl.isHidden = true
            customersearchlbl1.text =  "Enter the City"
        } else {
            customsearchimageview.image = UIImage(named: "2018-01-04.png")
            customersearchlbl1.text =  "Customer Search"
        }
        AlertLabel.isHidden=true
        stockroom()
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationmethod), name: NSNotification.Name(rawValue: "PopupNotification"), object: nil)
    }
    
    func notificationmethod()
    {
        if LibraryAPI.sharedInstance.Popupgetstring != nil {
            if LibraryAPI.sharedInstance.Popupgetstring != "" {
                self.customeryf.text = LibraryAPI.sharedInstance.Popupgetstring
            }
        }
        
    }
    
    
    func stockroom()
    {
        let size = CGSize(width: 30, height:30)
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
            self.stockroomResponse(url: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/CustomerStockRoom?Userid=\(LibraryAPI.sharedInstance.Userid)")
            DispatchQueue.main.async {
                self.local_activityIndicator_stop()
            }
        }
    }
    
    func stockroomResponse(url:String)
    {
        self.getsuccess(params: [:], urlhit: url, completion: { (jsonmutable) in
            self.stockroomResponse = jsonmutable
            if(self.stockroomResponse.count > 0)
            {
                
            }
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    class func instance() -> CustomerLookupViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "CustomerLookupViewController") as! CustomerLookupViewController
    }
    func update() {
        okbtn.isHidden=true
        AlertLabel.isHidden=false
        AlertLabel.text="Customer not found, Please try with different one"
    }
    
    func invalidupdate() {
        okbtn.isHidden=true
        AlertLabel.isHidden=false
        AlertLabel.text="Invalid Customer Code"
    }
    func invalidupdate1() {
        okbtn.isHidden=true
        AlertLabel.isHidden=false
        AlertLabel.text="Please Enter the Branch Code"
    }
    
    func invalidupdatecity() {
        okbtn.isHidden=true
        AlertLabel.isHidden=false
        AlertLabel.text="Invalid City"
    }
    
    func update2() {
        // Something cool
        okbtn.isHidden=false
        AlertLabel.isHidden=true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let  char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        let aSet = NSCharacterSet(charactersIn:"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        if (isBackSpace == -92) {
            if((textField.text?.count)!>2) {
                okbtn.alpha=1.0
                okbtn.isUserInteractionEnabled=true
            } else {
                okbtn.alpha=0.5
                okbtn.isUserInteractionEnabled=false
            }
        } else {
            if((textField.text?.count)!>=2) {
                okbtn.alpha=1.0
                okbtn.isUserInteractionEnabled=true
            } else {
                okbtn.alpha=0.5
                okbtn.isUserInteractionEnabled=false
            }
        }
        return true && string == numberFiltered && newLength <= 8
    }
    
    @IBAction func Okaction(_ sender: AnyObject)
    {
        self.local_activityIndicator_start()
        if(customeryf.text=="")
        {
            Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(CustomerLookupViewController.update), userInfo: nil, repeats:false)
            Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(CustomerLookupViewController.update2), userInfo: nil, repeats:false)
            self.local_activityIndicator_stop()
        }
        else if(LibraryAPI.sharedInstance.Currentcustomerlookup == "City")
        {
            print("value city : ",self.customeryf.text!)
            DispatchQueue.global(qos: .background).async {
                self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/CityDetails?Userid=\(LibraryAPI.sharedInstance.Userid.stringByRemovingWhitespaces)&prmt=\(self.customeryf.text!)", completion: { (jsonmutable) in
                    self.cityarr = jsonmutable
                    DispatchQueue.main.async {
                        if(self.cityarr.count == 1)
                        {
                            for word in self.cityarr
                            {
                                //    let mode = word.objectForKey("Mode") as! String
                                let dic = word as! NSDictionary
                                let detail = dic.object(forKey: "Detail") as! String
                                //getState
                                let detail1 = dic.object(forKey: "Detail1") as! String
                                let mode = dic.object(forKey: "Mode")as! String
                                LibraryAPI.sharedInstance.getCity = detail
                                LibraryAPI.sharedInstance.getState = detail1
                                LibraryAPI.sharedInstance.getmode = mode
                            }
                            self.closeHandler?()
                            self.local_activityIndicator_stop()
                        }
                        else if(self.cityarr.count > 1)
                        {
                            var checkbool : Bool!
                            for word in self.cityarr  {
                                checkbool = false
                                let dic1 = word as! NSDictionary
                                let detail = dic1.object(forKey: "Detail") as! String
                                let detail1 = dic1.object(forKey: "Detail1") as! String
                                let mode = dic1.object(forKey: "Mode")as! String
                                if self.customeryf.text! == detail {
                                    checkbool = true
                                    LibraryAPI.sharedInstance.getCity = detail
                                    LibraryAPI.sharedInstance.getState = detail1
                                    LibraryAPI.sharedInstance.getmode = mode
                                    break
                                }
                            }
                            
                            if checkbool == true {
                                self.closeHandler?()
                            } else  {
                                LibraryAPI.sharedInstance.Cityearray = self.cityarr
                                let loginPageView = self.storyboard?.instantiateViewController(withIdentifier: "CitylookupViewController") as! CitylookupViewController
                                self.navigationController?.pushViewController(loginPageView, animated: true)
                                self.notificationmethod()
                            }
                            self.local_activityIndicator_stop()
                        }
                        else
                        {
                            Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(CustomerLookupViewController.invalidupdatecity), userInfo: nil, repeats:false)
                            Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(CustomerLookupViewController.update2), userInfo: nil, repeats:false)
                            self.closeHandler?()
                            self.local_activityIndicator_stop()
                        }
                    }
                })
            }
        }
            // city
        else if(Branchcodetf.text == "")
        {
            Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(CustomerLookupViewController.invalidupdate1), userInfo: nil, repeats:false)
            Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(CustomerLookupViewController.update2), userInfo: nil, repeats:false)
            Branchcodetf.resignFirstResponder()
            customeryf.resignFirstResponder()
            self.local_activityIndicator_stop()
            
        }
        else
        {
            response .removeAllObjects()
            customeryf.resignFirstResponder()
            Branchcodetf.resignFirstResponder()
            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer360.asmx/Customersearch360?Customer=\(self.customeryf.text!.stringByRemovingWhitespaces)&Type=1&Userid=\(LibraryAPI.sharedInstance.Userid)&Branch=\(LibraryAPI.sharedInstance.view360secondpopupname)", completion: { (jsonmutable) in
                self.response = jsonmutable
                if self.response.count == 0
                {
                    Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(CustomerLookupViewController.update), userInfo: nil, repeats:false)
                    Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(CustomerLookupViewController.update2), userInfo: nil, repeats:false)
                    self.local_activityIndicator_stop()
                }
                else if self.response.count == 1
                {
                    self.view.endEditing(true)
                    LibraryAPI.sharedInstance.CustomerCode=self.customeryf.text!
                    if(LibraryAPI.sharedInstance.Currentcustomerlookup=="360")
                    {
                        DispatchQueue.global(qos: .background).async {
                            
                            // Validate user input
                            // self.Loadorganisationdata()
                            print("value 3 : ",LibraryAPI.sharedInstance.view360secondpopupname)
                            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer360.asmx/Customersearch360?Customer=\(self.customeryf.text!)&Type=1&Userid=\(LibraryAPI.sharedInstance.Userid)&Branch=\(LibraryAPI.sharedInstance.view360secondpopupname)", completion: { (jsonmutable) in
                                self.responsestring = jsonmutable
                                DispatchQueue.main.async {
                                  
                                    if(self.responsestring.count>0)
                                    {
                                        print("response val 3:",self.responsestring)
                                        self.local_activityIndicator_stop()
                                        LibraryAPI.sharedInstance.Organisationarray=self.responsestring
                                        let dic = self.responsestring[0] as! NSDictionary
                                        LibraryAPI.sharedInstance.OrdersubmissonData.setObject(LibraryAPI.sharedInstance.CustomerCode, forKey: "CustomerCode" as NSCopying)
                                        LibraryAPI.sharedInstance.branchcode=(dic.object(forKey: "Branch Code") as? String)!
                                        let lvc = self.storyboard?.instantiateViewController(withIdentifier: "Dashboard360NewViewController") as! Dashboard360NewViewController
                                        LibraryAPI.sharedInstance.tracker1 = 0
                                        self.navigationController?.pushViewController(lvc, animated: true)
                                        LibraryAPI.sharedInstance.lastviewcontroller="Dashboardviewcontroller"
                                        // self.sideMenuViewController!.hideMenuViewController()
                                        
                                        
                                    }
                                    else
                                    {
                                        
                                        self.local_activityIndicator_stop()
                                         Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(CustomerLookupViewController.invalidupdate), userInfo: nil, repeats:false)
                                         Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(CustomerLookupViewController.update2), userInfo: nil, repeats:false)
                                        
                                    }
                                    
                                    
                                }
                            })
                            // Go back to the main thread to update the UI
                            
                        }
                        
                    }
                        
                    else if(LibraryAPI.sharedInstance.Currentcustomerlookup=="OrderHistory")
                    {
                      
                        DispatchQueue.global(qos: .background).async {
                            print("value 13 OrderHistory :",LibraryAPI.sharedInstance.view360secondpopupname)
                            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)orderEntry.asmx/OrderHistory?CustomerCode=\(LibraryAPI.sharedInstance.CustomerCode)", completion: { (jsonmutable) in
                                self.ordarray = jsonmutable
                                
                                DispatchQueue.main.async {
                                    
                                    
                                    if(self.ordarray.count>0)
                                    {
                                        // self.closeHandler?()
                                        LibraryAPI.sharedInstance.OrderReturnarray=self.ordarray
                                        self.local_activityIndicator_stop()
                                        LibraryAPI.sharedInstance.OrdersubmissonData.setObject(LibraryAPI.sharedInstance.CustomerCode, forKey: "CustomerCode" as NSCopying)
                                        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "OrderHistoryViewController")), animated: true)
                                        LibraryAPI.sharedInstance.lastviewcontroller="Dashboardviewcontroller"
                                        self.sideMenuViewController!.hideMenuViewController()
                                        
                                    }
                                    else
                                    {
                                        self.local_activityIndicator_stop()
                                        Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(CustomerLookupViewController.invalidupdate), userInfo: nil, repeats:false)
                                        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(CustomerLookupViewController.update2), userInfo: nil, repeats:false)
                                    }
                                }
                            })
                            // Go back to the main thread to update the UI
                            
                        }
                        
                        
                    }
                    else if(LibraryAPI.sharedInstance.Currentcustomerlookup=="Visitlog")
                    {
                        DispatchQueue.global(qos: .background).async {
                            print("value 12:",LibraryAPI.sharedInstance.view360secondpopupname)
                            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer360.asmx/Customersearch360?Customer=\(self.customeryf.text!)&Type=1&Userid=\(LibraryAPI.sharedInstance.Userid)&Branch=\(LibraryAPI.sharedInstance.view360secondpopupname)", completion: { (jsonmutable) in
                                self.responsestring = jsonmutable
                                DispatchQueue.main.async {
                                    if(self.responsestring.count>0)
                                    {
                                        self.local_activityIndicator_stop()
                                        let dicres1 = self.responsestring[0] as! NSDictionary
                                        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "VisitLogViewController") as! VisitLogViewController
                                        lvc.CustomcodeCheck = LibraryAPI.sharedInstance.CustomerCode
                                        lvc.Customername = (dicres1.object(forKey: "Customer Name") as? String)
                                        self.navigationController?.pushViewController(lvc, animated: true)
                                        LibraryAPI.sharedInstance.lastviewcontroller="Visitlog1ViewController"
                                        
                                    }
                                    else
                                    {
                                        self.local_activityIndicator_stop()
                                        Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(CustomerLookupViewController.invalidupdate), userInfo: nil, repeats:false)
                                        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(CustomerLookupViewController.update2), userInfo: nil, repeats:false)
                                    }
                                }
                            })
                            // Go back to the main thread to update the UI
                            
                        }
                        
                        // new version dispatch Que
                    }
                        
                    else if(LibraryAPI.sharedInstance.Currentcustomerlookup=="PreOrderHistory")
                    {
                        DispatchQueue.global(qos: .background).async {
                            print("value 11:",LibraryAPI.sharedInstance.view360secondpopupname)
                            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer360.asmx/Customersearch360?Customer=\(self.customeryf.text!)&Type=1&Userid=\(LibraryAPI.sharedInstance.Userid)&Branch=\(LibraryAPI.sharedInstance.view360secondpopupname)", completion: { (jsonmutable) in
                                self.responsestring = jsonmutable
                                DispatchQueue.main.async {
                                    
                                    if(self.responsestring.count>0)
                                    {
                                        self.local_activityIndicator_stop()
                                        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "PreOrderHistroyViewController") as! PreOrderHistroyViewController
                                        lvc.CustomcodeCheck = LibraryAPI.sharedInstance.CustomerCode
                                        let slideInFromLeftTransition = CATransition()
                                        slideInFromLeftTransition.type = kCAGravityTop
                                        slideInFromLeftTransition.subtype = kCATransitionFromLeft
                                        slideInFromLeftTransition.duration = 0.2
                                        slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                                        LibraryAPI.sharedInstance.lastviewcontroller="Dashboardviewcontroller"
                                        self.navigationController?.view.layer.add(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
                                        self.closeHandler?()
                                        self.navigationController?.pushViewController(lvc, animated: false)
                                    }
                                    else
                                    {
                                        self.local_activityIndicator_stop()
                                        Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(CustomerLookupViewController.invalidupdate), userInfo: nil, repeats:false)
                                        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(CustomerLookupViewController.update2), userInfo: nil, repeats:false)
                                    }
                                }
                            })
                        }
                    } else if(LibraryAPI.sharedInstance.Currentcustomerlookup=="S/O_Invoice")
                    {
                        DispatchQueue.global(qos: .background).async {
                            print("value 10: ",LibraryAPI.sharedInstance.view360secondpopupname)
                            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer360.asmx/Customersearch360?Customer=\(self.customeryf.text!)&Type=1&Userid=\(LibraryAPI.sharedInstance.Userid)&Branch=\(LibraryAPI.sharedInstance.view360secondpopupname)", completion: { (jsonmutable) in
                                self.responsestring = jsonmutable
                                
                                DispatchQueue.main.async {
                                    
                                    if(self.responsestring.count>0)
                                    {
                                        self.local_activityIndicator_stop()
                                        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "OrderEnquiryViewController") as! OrderEnquiryViewController
                                        lvc.CustomcodeCheck = LibraryAPI.sharedInstance.CustomerCode
                                        let slideInFromLeftTransition = CATransition()
                                        slideInFromLeftTransition.type = kCAGravityTop
                                        slideInFromLeftTransition.subtype = kCATransitionFromLeft
                                        slideInFromLeftTransition.duration = 0.2
                                        slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                                        LibraryAPI.sharedInstance.lastviewcontroller="Dashboardviewcontroller"
                                        self.navigationController?.view.layer.add(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
                                        self.closeHandler?()
                                        self.navigationController?.pushViewController(lvc, animated: false)
                                        
                                    }
                                    else
                                    {
                                        self.local_activityIndicator_stop()
                                        Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(CustomerLookupViewController.invalidupdate), userInfo: nil, repeats:false)
                                        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(CustomerLookupViewController.update2), userInfo: nil, repeats:false)
                                    }
                                    
                                }
                            })
                            // Go back to the main thread to update the UI
                            
                        }
                        
                        
                        
                    }
                        
                    else if(LibraryAPI.sharedInstance.Currentcustomerlookup=="spc")
                    {
                        
                        DispatchQueue.global(qos: .background).async {
                            
                            // Validate user input
                            // self.Loadorganisationdata()
                            print("value 9: ",LibraryAPI.sharedInstance.view360secondpopupname)
                            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer360.asmx/Customersearch360?Customer=\(self.customeryf.text!)&Type=1&Userid=\(LibraryAPI.sharedInstance.Userid)&Branch=\(LibraryAPI.sharedInstance.view360secondpopupname)", completion: { (jsonmutable) in
                                self.responsestring = jsonmutable
                                DispatchQueue.main.async {
                                    
                                    
                                    if(self.responsestring.count>0)
                                    {
                                        // self.closeHandler?()
                                        self.local_activityIndicator_stop()
                                        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "SPCViewController") as! SPCViewController
                                        lvc.CustomcodeCheck = LibraryAPI.sharedInstance.CustomerCode
                                        self.navigationController?.pushViewController(lvc, animated: true)
                                        LibraryAPI.sharedInstance.lastviewcontroller="Dashboardviewcontroller"
                                        self.sideMenuViewController!.hideMenuViewController()
                                        
                                    }
                                    else
                                    {
                                        self.local_activityIndicator_stop()
                                        Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(CustomerLookupViewController.invalidupdate), userInfo: nil, repeats:false)
                                        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(CustomerLookupViewController.update2), userInfo: nil, repeats:false)
                                        
                                        
                                    }
                                }
                            })
                            // Go back to the main thread to update the UI
                            
                        }
                    }
                        
                    else if(LibraryAPI.sharedInstance.Currentcustomerlookup=="Billingodcustomer")
                    {
                        
                        
                        DispatchQueue.global(qos: .background).async {
                            
                            // Validate user input
                            //self.Loadorganisationdata()
                            print("value 8: ",LibraryAPI.sharedInstance.view360secondpopupname)
                            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer360.asmx/Customersearch360?Customer=\(self.customeryf.text!)&Type=1&Userid=\(LibraryAPI.sharedInstance.Userid)&Branch=\(LibraryAPI.sharedInstance.view360secondpopupname)", completion: { (jsonmutable) in
                                self.responsestring = jsonmutable
                                
                                // Go back to the main thread to update the UI
                                DispatchQueue.main.async {
                                    
                                    if(self.responsestring.count>0)
                                    {
                                        self.uploadfile.setObject(self.customeryf.text!, forKey: "CustomerCode" as NSCopying)
                                        
                                        self.uploadfile.setObject("", forKey: "BizCode1" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "BizCode2" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "BizCode3" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "BizCode4" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "BizCode5" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "OrderValue" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "CreditDays" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "Remarks" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "Submit" as NSCopying)
                                        self.uploadfile.setObject(LibraryAPI.sharedInstance.Userid.stringByRemovingWhitespaces, forKey: "UserId" as NSCopying)
                                        
                                        let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)Users.asmx/BODUpdation")! as URL)
                                        request.httpMethod = "POST"
                                        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
                                        request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
                                        
                                        request.httpBody = try? JSONSerialization.data(withJSONObject: self.uploadfile, options: JSONSerialization.WritingOptions(rawValue: 0))
                                        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
                                        //  var err: NSError
                                        do
                                        {
                                            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
                                            self.jsonresult = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                                            
                                            
                                            
                                            if self.jsonresult.count > 0
                                            {
                                                
                                                self.closeHandler?()
                                                self.local_activityIndicator_stop()
                                                let lvc=self.storyboard?.instantiateViewController(withIdentifier: "Bod2ViewController") as! Bod2ViewController
                                                lvc.CustomcodeCheck = LibraryAPI.sharedInstance.CustomerCode
                                                lvc.TotallOutStanding = self.jsonresult.object(forKey: "TotallOutStanding") as! String
                                                lvc.GroupExposure = self.jsonresult.object(forKey: "GroupExposure") as! String
                                                lvc.OverDue = self.jsonresult.object(forKey: "OverDue") as! String
                                                lvc.GroupOverDue = self.jsonresult.object(forKey: "GroupOverDue") as! String
                                                lvc.reasonforstop = self.jsonresult.object(forKey: "ReasonForStop") as! String
                                                lvc.messsage = self.jsonresult.object(forKey: "Message")as! String
                                                lvc.BODRef = self.jsonresult.object(forKey: "BODRef")as! String
                                                let slideInFromLeftTransition = CATransition()
                                                slideInFromLeftTransition.type = kCAGravityTop
                                                slideInFromLeftTransition.subtype = kCATransitionFromLeft
                                                slideInFromLeftTransition.duration = 0.2
                                                slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                                                LibraryAPI.sharedInstance.lastviewcontroller="Dashboardviewcontroller"
                                                self.navigationController?.view.layer.add(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
                                                self.closeHandler?()
                                                self.navigationController?.pushViewController(lvc, animated: false)
                                            }
                                            else
                                            {
                                               self.local_activityIndicator_stop()
                                            }
                                            
                                        }
                                        catch (let e)
                                        {
                                         print(e)
                                        }
                                        
                                        
                                        
                                    }
                                    else
                                    {
                                         self.local_activityIndicator_stop()
                                        Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(CustomerLookupViewController.invalidupdate), userInfo: nil, repeats:false)
                                        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(CustomerLookupViewController.update2), userInfo: nil, repeats:false)
                                    }
                                    
                                }
                            })
                            
                        }
                    }
                        
                        
                    else if(LibraryAPI.sharedInstance.Currentcustomerlookup=="DBC")
                    {
                        
                       
                        var todaysDate:NSDate = NSDate()
                        var dateFormatter:DateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd"
                        let date = dateFormatter.string(from: NSDate() as Date)
                        
                        let dateComponents = NSDateComponents()
                        
                        
                        dateComponents.day = Int("1")!
                        let currentCalendar = NSCalendar.current
                        
                        let dateFormatter1 = DateFormatter()
                        dateFormatter1.dateFormat = "yyyy-MM-dd"
                        let date1 = dateFormatter.date(from: date)
                        // let oneMonthBack = currentCalendar.dateByAddingComponents(dateComponents, toDate: date1!, options: [])!
                        let oneMonthBack = currentCalendar.date(byAdding: dateComponents as DateComponents, to: date1!)
                        
                        dateFormatter.dateStyle = DateFormatter.Style.long
                        dateFormatter.dateFormat = "yyyy-MM-dd"
                        let strDate = dateFormatter.string(from: oneMonthBack!)
                        
                        
                        _ = String(strDate)
                        
                        // new version dispatch Que
                        
                        DispatchQueue.global(qos: .background).async {
                            print("value 7:",LibraryAPI.sharedInstance.view360secondpopupname)
                            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer360.asmx/Customersearch360?Customer=\(self.customeryf.text!)&Type=1&Userid=\(LibraryAPI.sharedInstance.Userid)&Branch=\(LibraryAPI.sharedInstance.view360secondpopupname)", completion: { (jsonmutable) in
                                self.responsestring = jsonmutable
                                DispatchQueue.main.async {
                                    
                                    if(self.responsestring.count>0)
                                    {
                                        self.local_activityIndicator_stop()
                                        self.uploadfile.setObject(self.customeryf.text!, forKey: "Customercode" as NSCopying)
                                        self.uploadfile.setObject(date, forKey: "DealStartDate" as NSCopying)
                                        self.uploadfile.setObject("1", forKey: "DateofClosure" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "CreditDays" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "CreditAmount" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "PaymentMethod" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "product1" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "product2" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "product3" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "product4" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "product5" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "EndCustomerName" as NSCopying)
                                        self.uploadfile.setObject("N", forKey: "EndCustomerPOReq" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "EndCustomerPONumber" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "Submit" as NSCopying)
                                        self.uploadfile.setObject(LibraryAPI.sharedInstance.Userid.stringByRemovingWhitespaces, forKey: "UserId" as NSCopying)
                                        //http://edi.redingtonb2b.in/Whatsup-staging/Users.asmx/DBCEntry
                                        let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)Users.asmx/DBCEntry")! as URL)
                                        request.httpMethod = "POST"
                                        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
                                        request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
                                        request.httpBody = try? JSONSerialization.data(withJSONObject: self.uploadfile, options: JSONSerialization.WritingOptions(rawValue: 0))
                                        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
                                        //  var err: NSError
                                        do
                                        {
                                            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
                                            self.jsonresult = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                                            if self.jsonresult.count > 0
                                            {
                                                let lvc=self.storyboard?.instantiateViewController(withIdentifier: "DBCViewController") as! DBCViewController
                                                lvc.CustomcodeCheck = LibraryAPI.sharedInstance.CustomerCode
                                                let errormessage = self.jsonresult.object(forKey: "ErrorMessage")
                                                let message      = self.jsonresult.object(forKey: "Message")
                                                let Refnumber    = self.jsonresult.object(forKey: "ReferenceNumber")
                                                if (errormessage as? String) != nil
                                                {
                                                    lvc.errormsg = errormessage as! String
                                                } else  {
                                                    lvc.errormsg = ""
                                                }
                                                
                                                if (message as? String) != nil
                                                {
                                                    lvc.message = message as! String
                                                } else {
                                                    lvc.message = ""
                                                }
                                                
                                                if (Refnumber as? String) != nil
                                                {
                                                    lvc.referencenumber = Refnumber as! String
                                                } else {
                                                    lvc.referencenumber = ""
                                                }
                                                let slideInFromLeftTransition = CATransition()
                                                slideInFromLeftTransition.type = kCAGravityTop
                                                slideInFromLeftTransition.subtype = kCATransitionFromLeft
                                                slideInFromLeftTransition.duration = 0.2
                                                slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                                                LibraryAPI.sharedInstance.lastviewcontroller="Dashboardviewcontroller"
                                                self.navigationController?.view.layer.add(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
                                                self.closeHandler?()
                                                self.navigationController?.pushViewController(lvc, animated: false)
                                                 self.local_activityIndicator_stop()
                                            } else {
                                                 self.local_activityIndicator_stop()
                                            }
                                        } catch (let e)
                                        {
                                            print(e)
                                        }
                                    }
                                    else
                                    {
                                         self.local_activityIndicator_stop()
                                        Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(CustomerLookupViewController.invalidupdate), userInfo: nil, repeats:false)
                                        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(CustomerLookupViewController.update2), userInfo: nil, repeats:false)
                                    }
                                    
                                }
                            })
                        }
                        
                    }
                    else if(LibraryAPI.sharedInstance.Currentcustomerlookup=="ECB")
                    {
                        DispatchQueue.global(qos: .background).async {
                            print("value 6: ",LibraryAPI.sharedInstance.view360secondpopupname)
                            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer360.asmx/Customersearch360?Customer=\(self.customeryf.text!)&Type=1&Userid=\(LibraryAPI.sharedInstance.Userid)&Branch=\(LibraryAPI.sharedInstance.view360secondpopupname)", completion: { (jsonmutable) in
                                self.responsestring = jsonmutable
                                DispatchQueue.main.async {
                                    if(self.responsestring.count>0)
                                    {
                                        self.uploadfile.setObject(self.customeryf.text!, forKey: "Customercode" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "Stockroom" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "Product1" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "Product2" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "Product3" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "Product4" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "Product5" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "BillAmount" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "Despatchmethod" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "InvoiceCustomerName" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "Invoiceaddress1" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "Invoiceaddress2" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "Invoiceaddress3" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "City" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "State" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "Invoicepin" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "DeliverName" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "Deliveraddrss1" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "Deliveraddrss2" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "Deliveraddrss3" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "Delivercity" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "Deliverstate" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "DeliveryPin" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "flag" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "Submit" as NSCopying)
                                        self.uploadfile.setObject(LibraryAPI.sharedInstance.Userid.stringByRemovingWhitespaces, forKey: "User" as NSCopying)
                                        let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)Users.asmx/ECBEntry")! as URL)
                                        request.httpMethod = "POST"
                                        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
                                        request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
                                        request.httpBody = try? JSONSerialization.data(withJSONObject: self.uploadfile, options: JSONSerialization.WritingOptions(rawValue: 0))
                                        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
                                        //  var err: NSError
                                        do
                                        {
                                            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
                                            self.jsonresult = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                                            if self.jsonresult.count > 0
                                            {
                                                //  self.closeHandler?()
                                                self.local_activityIndicator_stop()
                                                let lvc=self.storyboard?.instantiateViewController(withIdentifier: "ECBViewController") as! ECBViewController
                                                lvc.CustomcodeCheck = LibraryAPI.sharedInstance.CustomerCode
                                                let stackOut = self.jsonresult.object(forKey: "Stockroom1_out") as! String
                                                for word in self.stockroomResponse
                                                {
                                                    let dic : NSDictionary = word as! NSDictionary
                                                    let code = dic.object(forKey: "StockRoom Code") as! String
                                                    let name = dic.object(forKey: "StockRoom Name") as! String
                                                    if code == stackOut
                                                    {
                                                        lvc.stockroom = code + " - " + name
                                                    }
                                                }
                                                
                                                lvc.billamount = self.jsonresult.object(forKey: "BillAmount") as! String
                                                lvc.InvoiceCustomerName = self.jsonresult.object(forKey:"InvoiceCustomerName") as!String
                                                lvc.Invoiceaddress1 = self.jsonresult.object(forKey:"Invoiceaddress1")as! String
                                                lvc.Invoiceaddress2 = self.jsonresult.object(forKey:"Invoiceaddress2")as! String
                                                lvc.Invoiceaddress3 = self.jsonresult.object(forKey:"Invoiceaddress3")as! String
                                                lvc.city = self.jsonresult.object(forKey:"City")as! String
                                                lvc.State = self.jsonresult.object(forKey:"State")as! String
                                                lvc.Invoicepin = self.jsonresult.object(forKey:"Invoicepin")as! String
                                                lvc.DeliverName = self.jsonresult.object(forKey:"DeliverName")as! String
                                                lvc.Deliveraddrss1 = self.jsonresult.object(forKey:"Deliveraddrss11_out")as! String
                                                lvc.Deliveraddrss2 = self.jsonresult.object(forKey:"Deliveraddrss21_out") as! String
                                                lvc.Deliveraddrss3 = self.jsonresult.object(forKey:"Deliveraddrss31_out") as! String
                                                lvc.Delivercity = self.jsonresult.object(forKey:"Delivercity1_out") as! String
                                                lvc.Deliverstate = self.jsonresult.object(forKey:"Deliverstate1_out") as! String
                                                lvc.DeliveryPin = self.jsonresult.object(forKey:"PINcode") as! String
                                                let errormessage = self.jsonresult.object(forKey:"ErrorMessage")
                                                let message      = self.jsonresult.object(forKey:"Message")
                                                let Refnumber    = self.jsonresult.object(forKey:"Customerbillingnumber")
                                                if (errormessage as? String) != nil
                                                {
                                                    lvc.errormsg = errormessage as! String
                                                }
                                                else
                                                {
                                                    lvc.errormsg = ""
                                                }
                                                
                                                if (message as? String) != nil
                                                {
                                                    lvc.message = message as! String
                                                }
                                                else
                                                {
                                                    lvc.message = ""
                                                }
                                                
                                                if (Refnumber as? String) != nil
                                                {
                                                    lvc.referencenumber = Refnumber as! String
                                                }
                                                else
                                                {
                                                    lvc.referencenumber = ""
                                                }
                                                
                                                let slideInFromLeftTransition = CATransition()
                                                slideInFromLeftTransition.type = kCAGravityTop
                                                slideInFromLeftTransition.subtype = kCATransitionFromLeft
                                                slideInFromLeftTransition.duration = 0.2
                                                slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                                                LibraryAPI.sharedInstance.lastviewcontroller="Dashboardviewcontroller"
                                                self.navigationController?.view.layer.add(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
                                                self.closeHandler?()
                                                self.navigationController?.pushViewController(lvc, animated: false)
                                                 self.local_activityIndicator_stop()
                                            }
                                            else
                                            {
                                                self.local_activityIndicator_stop()
                                            }
                                            
                                        }
                                        catch (let e)
                                        {
                                            print(e)
                                        }
                                    }
                                    else
                                    {
                                         self.local_activityIndicator_stop()
                                        Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(CustomerLookupViewController.invalidupdate), userInfo: nil, repeats:false)
                                        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(CustomerLookupViewController.update2), userInfo: nil, repeats:false)
                                    }
                                }
                            })
                        }
                    }
                    else if(LibraryAPI.sharedInstance.Currentcustomerlookup=="SPCENTRYOPTION")
                    {
                        DispatchQueue.global(qos: .background).async {
                            var todaysDate:NSDate = NSDate()
                            let dateFormatter:DateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd"
                            let date = dateFormatter.string(from: NSDate() as Date)
                            print("value 5: ",LibraryAPI.sharedInstance.view360secondpopupname)
                            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer360.asmx/Customersearch360?Customer=\(self.customeryf.text!)&Type=1&Userid=\(LibraryAPI.sharedInstance.Userid)&Branch=\(LibraryAPI.sharedInstance.view360secondpopupname)", completion: { (jsonmutable) in
                                self.responsestring = jsonmutable
                                // Go back to the main thread to update the UI
                                DispatchQueue.main.async {
                                    if(self.responsestring.count>0)
                                    {
                                        self.uploadfile.setObject(self.customeryf.text!, forKey: "CustomerCode" as NSCopying)
                                        self.uploadfile.setObject(LibraryAPI.sharedInstance.Userid.stringByRemovingWhitespaces, forKey: "User" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "BizCode" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "PriceappType" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "StockRoom" as NSCopying)
                                        self.uploadfile.setObject(date, forKey: "ValidityDate" as NSCopying)
                                        self.uploadfile.setObject("Y", forKey: "FreightPrepaid" as NSCopying)
                                        self.uploadfile.setObject("M", forKey: "MultipleInvoicingAllowed" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "VendorReference" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "EndCustomerName" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "Warranty" as NSCopying)
                                        self.uploadfile.setObject("", forKey: "PaymentDays" as NSCopying)
                                        //http://edi.redingtonb2b.in/Whatsup-staging/Users.asmx/SPCValidation
                                        let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)Users.asmx/SPCValidation")! as URL)
                                        request.httpMethod = "POST"
                                        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
                                        request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
                                        request.httpBody = try? JSONSerialization.data(withJSONObject: self.uploadfile, options: JSONSerialization.WritingOptions(rawValue: 0))
                                        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
                                        do
                                        {
                                            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
                                            self.jsonresult = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                                            if self.jsonresult.count > 0
                                            {
                                                self.local_activityIndicator_stop()
                                                let lvc=self.storyboard?.instantiateViewController(withIdentifier: "SpcoptionentryViewController") as! SpcoptionentryViewController
                                                lvc.CustomcodeCheck = LibraryAPI.sharedInstance.CustomerCode
                                                lvc.stockroom = self.jsonresult.object(forKey: "StockRoom_out") as! String
                                                lvc.BizCode = self.jsonresult.object(forKey: "BizCode_out")as! String
                                                lvc.PriceappType = self.jsonresult.object(forKey: "PriceappType_out")as! String
                                                lvc.ValidityDate = self.jsonresult.object(forKey: "ValidityDate_out")as! String
                                                lvc.PaymentDays = self.jsonresult.object(forKey: "PaymentDays_out")as! String
                                                lvc.FreightPrepaid = self.jsonresult.object(forKey: "FreightPrepaid_out")as! String
                                                lvc.MultipleInvoicingAllowed = self.jsonresult.object(forKey: "MultipleInvoicingAllowed_out")as! String
                                                lvc.VendorReference = self.jsonresult.object(forKey: "VendorReference_out")as! String
                                                lvc.EndCustomerName = self.jsonresult.object(forKey: "EndCustomerName_out")as! String
                                                lvc.Warranty_out = self.jsonresult.object(forKey: "Warranty_out")as! String
                                                lvc.ErrorMessage = self.jsonresult.object(forKey: "ErrorMessage")as! String
                                                lvc.Flag1 = self.jsonresult.object(forKey: "Flag1")as! String
                                                lvc.Flag2 = self.jsonresult.object(forKey: "Flag2")as! String
                                                lvc.Flag3 = self.jsonresult.object(forKey: "Flag3")as! String
                                                let errormessage = self.jsonresult.object(forKey: "ErrorMessage")
                                                let message      = self.jsonresult.object(forKey: "Message")
                                                if (errormessage as? String) != nil
                                                {
                                                    lvc.ErrorMessage = errormessage as! String
                                                } else  {
                                                    
                                                }
                                                if (message as? String) != nil
                                                {
                                                    lvc.Message = message as! String
                                                } else  {
                                                    
                                                }
                                                let slideInFromLeftTransition = CATransition()
                                                slideInFromLeftTransition.type = kCAGravityTop
                                                slideInFromLeftTransition.subtype = kCATransitionFromLeft
                                                slideInFromLeftTransition.duration = 0.2
                                                slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                                                LibraryAPI.sharedInstance.lastviewcontroller="Dashboardviewcontroller"
                                                self.navigationController?.view.layer.add(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
                                                self.closeHandler?()
                                                self.navigationController?.pushViewController(lvc, animated: false)
                                                 self.local_activityIndicator_stop()
                                            } else {
                                                 self.local_activityIndicator_stop()
                                            }
                                        }
                                        catch (let e)
                                        {
                                            print(e)
                                        }
                                       self.local_activityIndicator_stop()
                                    }
                                    else
                                    {
                                        self.local_activityIndicator_stop()
                                        Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(CustomerLookupViewController.invalidupdate), userInfo: nil, repeats:false)
                                        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(CustomerLookupViewController.update2), userInfo: nil, repeats:false)
                                    }
                                }
                            })
                        }
                    }
                    else
                    {
                        DispatchQueue.global(qos: .background).async {
                            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Preorder.asmx/Preorder_Itemcode?UserID=\(LibraryAPI.sharedInstance.Userid)&Customer=\(LibraryAPI.sharedInstance.CustomerCode)&RecordNo=20&PageNo=1&ItemCode=", completion: { (jsonmutable) in
                                self.responsearray = jsonmutable
                                DispatchQueue.main.async {
                                    if(self.responsearray.count>0)
                                    {
                                        self.local_activityIndicator_stop()
                                        LibraryAPI.sharedInstance.OrdersubmissonData.setObject(LibraryAPI.sharedInstance.CustomerCode, forKey: "CustomerCode" as NSCopying)
                                        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "PreOrderViewController") as! PreOrderViewController
                                        self.navigationController?.pushViewController(lvc, animated: true)
                                        LibraryAPI.sharedInstance.lastviewcontroller="Dashboardviewcontroller"
                                        self.CloseAction(PreorderpopupViewController.classForCoder())
                                    }
                                    else
                                    {
                                        Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(CustomerLookupViewController.invalidupdate), userInfo: nil, repeats:false)
                                         self.local_activityIndicator_stop()
                                        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(CustomerLookupViewController.update2), userInfo: nil, repeats:false)
                                    }
                                }
                            })
                        }
                    }
                    
                }
                else if self.response.count > 1
                {
                    LibraryAPI.sharedInstance.CustomerCode=self.customeryf.text!
                    
                    
                    
                    // new version dispatch Que
                    
                    DispatchQueue.global(qos: .background).async {
                        
                        // Validate user input
                        
                        //  self.Loadlookupdata()
                        print("value 4 : ",LibraryAPI.sharedInstance.view360secondpopupname)
                        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer360.asmx/Customersearch360?Customer=\(LibraryAPI.sharedInstance.CustomerCode)&Type=\("1")&Userid=\(LibraryAPI.sharedInstance.Userid)&Branch=\(LibraryAPI.sharedInstance.view360secondpopupname)", completion:  { (jsonmutable) in
                            self.lookuparr = jsonmutable
                            
                            // Go back to the main thread to update the UI
                            DispatchQueue.main.async {
                                
                                
                                if(self.lookuparr.count>0)
                                {
                                    // closeHandler?()
                                    self.local_activityIndicator_stop()
                                    LibraryAPI.sharedInstance.LookupGlobal=self.lookuparr
                                    self.performSegue(withIdentifier: "lookup", sender: nil)
                                    
                                    
                                }
                                else
                                {
                                     self.local_activityIndicator_stop()
                                    Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(CustomerLookupViewController.update), userInfo: nil, repeats:false)
                                    Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(CustomerLookupViewController.update2), userInfo: nil, repeats:false)
                                }
                                
                            }
                        })
                    }
                    
                }
            })
            
        }
    }
    
    func BackgroundVoice(notification: NSNotification)
    {
        if speech.audioEngine.isRunning
        {
            speech.audioEngine.stop()
            speech.recognitionRequest?.endAudio()
            microphoneButton.isEnabled = false
            let arrow = UIImageView()
            let image = UIImage(named: "speech.png")
            arrow.image = image!.imageWithColor(tintColor: UIColor.init(red:30/255.0 , green: 184/255.0, blue: 120/255.0, alpha: 1.0)).withRenderingMode(.alwaysOriginal)
            microphoneButton.setImage(arrow.image, for: UIControlState.normal)
        }
    }
    
    
    @IBAction func SpeechAction(_ sender: AnyObject)
    {
        if speech.audioEngine.isRunning {
            speech.audioEngine.stop()
            speech.recognitionRequest?.endAudio()
            microphoneButton.isEnabled = false
            let arrow = UIImageView()
            let image = UIImage(named: "speech.png")
            arrow.image = image!.imageWithColor(tintColor: UIColor.init(red:30/255.0 , green: 184/255.0, blue: 120/255.0, alpha: 1.0)).withRenderingMode(.alwaysOriginal)
            microphoneButton.setImage(arrow.image, for: UIControlState.normal)
        } else {
            self.customeryf.text=""
            self.startRecording()
            okbtn.alpha=0.5
            self.okbtn.isUserInteractionEnabled=false
            self.microphoneButton.isEnabled = true
            microphoneButton.setImage(UIImage(named: "speechon.png"), for: UIControlState.normal)
        }
    }
    
    func startRecording() {
        if speech.recognitionTask != nil {
            speech.recognitionTask?.cancel()
            speech.recognitionTask = nil
        }
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSessionCategoryRecord)
            try audioSession.setMode(AVAudioSessionModeMeasurement)
            try audioSession.setActive(true)
        } catch {
            
        }
        speech.recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        guard let inputNode = speech.audioEngine.inputNode else {
            fatalError("Audio engine has no input node")
        }
        
        guard let recognitionRequest = speech.recognitionRequest else {
            fatalError("Unable to create an SFSpeechAudioBufferRecognitionRequest object")
        }
        
        speech.recognitionRequest!.shouldReportPartialResults = true  //6
        speech.recognitionTask = speech.speechRecognizer.recognitionTask(with: recognitionRequest, resultHandler: { (result, error) in  //7
            var isFinal = false
            if result != nil {
                self.customeryf.text = result?.bestTranscription.formattedString.uppercased()
                if((self.customeryf.text?.count)! >= 4)
                {
                    self.okbtn.alpha=1.0
                    self.okbtn.isUserInteractionEnabled=true
                }
                else
                {
                    self.okbtn.isUserInteractionEnabled=false
                }
                isFinal = (result?.isFinal)!
            }
            
            if error != nil || isFinal {
                self.speech.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                self.speech.recognitionRequest = nil
                self.speech.recognitionTask = nil
                self.microphoneButton.isEnabled = true
            }
        })
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)  //11
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.speech.recognitionRequest?.append(buffer)
        }
        speech.audioEngine.prepare()
        do {
            try speech.audioEngine.start()
        } catch {
            
        }
        customeryf.text = "Speak Now!"
        
    }
    func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        //  let speech=SpeechFile()
        if available {
            microphoneButton.isEnabled = true
            //            speech.audioEngine.stop()
            //            speech.recognitionRequest?.endAudio()
            //            microphoneButton.enabled = false
            //            microphoneButton.setImage(UIImage(named: "speech.png"), forState: UIControlState.Normal)
        } else {
            microphoneButton.isEnabled = false
            //            speech.audioEngine.stop()
            //            speech.recognitionRequest?.endAudio()
            //            microphoneButton.enabled = false
            //            microphoneButton.setImage(UIImage(named: "speech.png"), forState: UIControlState.Normal)
        }
    }
    @IBAction func CloseAction(_ sender: AnyObject) {
        closeHandler?()
    }
    
    @IBAction func LookupAction(_ sender: AnyObject) {
        //  closeHandler?()
    }
    func Loadlookupdatafortype2() {
        // lookuparr = LibraryAPI.sharedInstance.RequestUrl(url: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer360.asmx/Customersearch360?Customer=\(LibraryAPI.sharedInstance.CustomerCode)&Type=\("2")&Userid=\(LibraryAPI.sharedInstance.Userid)")
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer360.asmx/Customersearch360?Customer=\(LibraryAPI.sharedInstance.CustomerCode)&Type=\("2")&Userid=\(LibraryAPI.sharedInstance.Userid)", completion:  { (jsonmutable) in
            self.lookuparr = jsonmutable
            if(self.lookuparr.count>0)
            {
                
            } else {
                self.showToast(message: "Delay in Server Response")
            }
        })
    }
    
    func Passvaluefrompop(text: String) {
        customeryf.text=text
    }
    
    func sizeForPopup(popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: 260, height: 260)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        LibraryAPI.sharedInstance.poptextfieldclicked=false
        return true
    }
    
    func Branchname()
    {
        if LibraryAPI.sharedInstance.view360secondpopupstr != nil
        {
            self.Branchcodetf.text = LibraryAPI.sharedInstance.view360secondpopupstr
        } else {
            self.Branchcodetf.text = "All Branches"
            LibraryAPI.sharedInstance.view360secondpopupname = ""
        }
        if LibraryAPI.sharedInstance.Custbranchisbtnclicked == nil
        {
            LibraryAPI.sharedInstance.Custbranchisbtnclicked = true
        } else{
            if LibraryAPI.sharedInstance.Custbranchisbtnclicked == true
            {
                if LibraryAPI.sharedInstance.view360btnaction  != nil
                {
                    self.Branchcodetf.text = "All Branches"
                    LibraryAPI.sharedInstance.view360secondpopupname = ""
                }
            }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField.tag == 1)
        {
            LibraryAPI.sharedInstance.BranchCodeCheck = 1
            LibraryAPI.sharedInstance.BranchCodeBtnCheck = 1
            Branchcodetf.resignFirstResponder()
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: branchesViewController.instance())
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
                    self.Branchname()
            }
            let container = branchesViewController.instance()
            container.closeHandler = { _ in
                self.Branchname()
                popup.dismiss()
            }
            popup.show(childViewController: container)
        }
    }
    
    func uploaddata()
    {
        
        let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)Users.asmx/BODUpdation")! as URL)
        request.httpMethod = "POST"
        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
        
        request.httpBody = try? JSONSerialization.data(withJSONObject: self.uploadfile, options: JSONSerialization.WritingOptions(rawValue: 0))
        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
        //  var err: NSError
        do
        {
            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
            jsonresult = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
        }
        catch (let e)
        {
        }
        
    }
    
    
    
    
}
