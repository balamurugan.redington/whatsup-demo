//
//  OrderDetailsSubViewController.swift
//  Redington
//
//  Created by TRUETECHMAC3 on 28/12/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
import KCFloatingActionButton

class OrderDetailsSubViewController: UIViewController,KCFloatingActionButtonDelegate,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var invoicenumber: UILabel!
    @IBOutlet weak var invoicedate: UILabel!
    @IBOutlet weak var sodate: UILabel!
    @IBOutlet weak var sonumber: UILabel!
    @IBOutlet var labelTotal: UILabel!
    var  responsestring = NSMutableArray()
    @IBOutlet var tableViewDetails: UITableView!
    
    var fab=KCFloatingActionButton()
    var arrayTotal: [Double] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.layoutFAB()
        self.customnavigation()
        self.Loaddata()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    func Loaddata()
    {
      //  http://edi.redingtonb2b.in/whatsup-staging/OrderStatus.asmx/CustomerOrderStatus?OrderNumber=AC06220&Userid=SENTHILKS
       // responsestring = LibraryAPI.sharedInstance.RequestUrl(url: "\(LibraryAPI.sharedInstance.GlobalUrl)OrderStatus.asmx/CustomerOrderStatus?OrderNumber=\(LibraryAPI.sharedInstance.orderno)&UserID=\(LibraryAPI.sharedInstance.Userid)")
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)OrderStatus.asmx/CustomerOrderStatus?OrderNumber=\(LibraryAPI.sharedInstance.orderno)&UserID=\(LibraryAPI.sharedInstance.Userid)", completion:  { (jsonmutable) in
            self.responsestring = jsonmutable
            if(self.responsestring.count>0)
            {
                //dispatch_async(dispatch_get_main_queue(), {
                self.tableViewDetails.reloadData()
                
                let arraySum = self.arrayTotal.reduce(0, +)
                self.labelTotal.text = String(arraySum);
                //  })
            }
            else
            {
                // dispatch_async(dispatch_get_main_queue(), {
                //})
                
            }
        })
        
       
        
    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
        
    }
    
    func layoutFAB() {
        fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
            
           self.tableViewDetails.reloadData()
            
            self.fab.close()
        }
        fab.fabDelegate = self
        self.view.addSubview(fab)
        
    }
    

    
    //TableView Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
      
        if responsestring.count > 0
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        return responsestring.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        var cell : OrderdetailsSubTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as! OrderdetailsSubTableViewCell
        
        if responsestring.count > 0
        {
            if(cell == nil)
           {
            cell = Bundle.main.loadNibNamed("Cell", owner: self, options: nil)![0] as! OrderdetailsSubTableViewCell;
            
            }
            let dic = responsestring[indexPath.row] as! NSDictionary
            
            cell.labelItemCode.text=dic.object(forKey: "Item Code") as? String
            cell.labelQty.text=String(dic.object(forKey: "Quantity") as! Double)
            cell.labelPartCode.text=String(dic.object(forKey: "Item Part No") as! String)
            cell.labelUnitPrice.text=String(dic.object(forKey: "Unit Price") as! Double)
            cell.labelLineValue.text=String(dic.object(forKey: "Total Line Value") as! Double)
            arrayTotal.append(dic.object(forKey: "Total Line Value") as! Double)
            
            cell.labelTitle.text = String(dic.object(forKey: "Item Deascription") as! String)
            
            self.sonumber.text=dic.object(forKey: "Order Number") as? String
            self.sodate.text=dic.object(forKey: "SO Date") as? String
            self.invoicedate.text=dic.object(forKey: "Invoice Date") as? String
            self.invoicenumber.text=dic.object(forKey: "Invoice Number") as? String
            self.status.text=dic.object(forKey: "Status") as? String
        }
        else
        {
            
        }
       

        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        
    }
    
    
    func customnavigation()
        
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(OrderDetailsSubViewController.backbuttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Order Details"
        
    }

    func backbuttonClicked(sender:UIButton)
    {
         self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    


}
