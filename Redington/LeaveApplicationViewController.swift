//
//  LeaveApplicationViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 27/07/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class LeaveApplicationViewController: UIViewController {
    
    @IBOutlet weak var ScrollView: UIScrollView!
    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var leavenolbl: UILabel!
    @IBOutlet weak var empcodelbl: UILabel!
    @IBOutlet weak var fromdatelbl: UILabel!
    @IBOutlet weak var Branchlbl: UILabel!
    @IBOutlet weak var Todatelbl: UILabel!
    @IBOutlet weak var Noofdayslbl: UILabel!
    @IBOutlet weak var Totalappropriatedlbl: UILabel!
    @IBOutlet weak var Remarkslbl: UILabel!
    @IBOutlet weak var loplbl: UILabel!
    @IBOutlet weak var leavebalpriviledgelbl: UILabel!
    @IBOutlet weak var levbalsicklbl: UILabel!
    @IBOutlet weak var levbalcasuallbl: UILabel!
    @IBOutlet weak var dayspriviledgelbl: UILabel!
    @IBOutlet weak var dayssicklbl: UILabel!
    @IBOutlet weak var dayscasuallbl: UILabel!
    @IBOutlet weak var balpriviledgelbl: UILabel!
    @IBOutlet weak var balsicklbl: UILabel!
    @IBOutlet weak var balcasuallbl: UILabel!
    @IBOutlet weak var maternityprivilegelbl: UILabel!
    @IBOutlet weak var maternitysicklbl: UILabel!
    @IBOutlet weak var maternitycasuallbl: UILabel!
    
    var uploadfile = NSMutableDictionary()
    var jsonresult = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.ScrollView.contentSize = MainView.frame.size
        if LibraryAPI.sharedInstance.Globalflag == "N"
        {
            customnavigation1()
            
        }
        else
        {
            customnavigation()
        }
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        leavenolbl.text = LibraryAPI.sharedInstance.Leaveapprovalhrd.object(forKey: "LEAVE APP NUMBER")as? String
        empcodelbl.text = LibraryAPI.sharedInstance.Leaveapprovalhrd.object(forKey: "EMPLOY CODE")as? String
        fromdatelbl.text = LibraryAPI.sharedInstance.Leaveapprovalhrd.object(forKey: "FROM DATE")as? String
        Branchlbl.text = LibraryAPI.sharedInstance.Leaveapprovalhrd.object(forKey: "BRANCH CODE")as? String
        Todatelbl.text = LibraryAPI.sharedInstance.Leaveapprovalhrd.object(forKey: "TO DATE")as? String
        Noofdayslbl.text = String(describing: LibraryAPI.sharedInstance.Leaveapprovalhrd.object(forKey: "NO:OF DAYS")as! NSNumber)
        Totalappropriatedlbl.text = String(describing: LibraryAPI.sharedInstance.Leaveapprovalhrd.object(forKey: "TOTAL PLCE LEAVE")as! NSNumber)
        loplbl.text =  String(describing: LibraryAPI.sharedInstance.Leaveapprovalhrd.object(forKey: "LOS OF PAY LEAVE")as! NSNumber)
        //loplbl.text =  String(LibraryAPI.sharedInstance.Leaveapprovalhrd.objectForKey("LOS OF PAY LEAVE")as! NSNumber)
        leavebalpriviledgelbl.text = String(describing: LibraryAPI.sharedInstance.Leaveapprovalhrd.object(forKey: "PL LEAVE BALANCE")as! NSNumber)
        levbalsicklbl.text =  String(describing: LibraryAPI.sharedInstance.Leaveapprovalhrd.object(forKey: "SL LEAVE BALANCE")as! NSNumber)
        levbalcasuallbl.text =  String(describing: LibraryAPI.sharedInstance.Leaveapprovalhrd.object(forKey: "CL LEAVE BALANCE")as! NSNumber)
        dayspriviledgelbl.text = String(describing: LibraryAPI.sharedInstance.Leaveapprovalhrd.object(forKey: "PL APPLIED LEAVE")as! NSNumber)
        dayssicklbl.text =  String(describing: LibraryAPI.sharedInstance.Leaveapprovalhrd.object(forKey: "SL APPLIED LEAVE")as! NSNumber)
        dayscasuallbl.text =  String(describing: LibraryAPI.sharedInstance.Leaveapprovalhrd.object(forKey: "CL APPLIED LEAVE")as! NSNumber)
        balpriviledgelbl.text = String(describing: LibraryAPI.sharedInstance.Leaveapprovalhrd.object(forKey: "PL BALANCE LEAVE")as! NSNumber)
        balsicklbl.text = String(describing: LibraryAPI.sharedInstance.Leaveapprovalhrd.object(forKey: "SL BALANCE LEAVE")as! NSNumber)
        balcasuallbl.text = String(describing: LibraryAPI.sharedInstance.Leaveapprovalhrd.object(forKey: "CL BALANCE LEAVE")as! NSNumber)
        Remarkslbl.text = LibraryAPI.sharedInstance.Leaveapprovalhrd.object(forKey: "REASON FOR LEAVE")as? String
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        //        appdelegate.shouldSupportAllOrientation = false
    }
    
    func customnavigation()
        
    {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(LeaveApplicationViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "LeaveApplication"
    }
    
    func customnavigation1()
        
    {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        //customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), forState: UIControlState.Normal)
        customView.MenuBtn.addTarget(self, action: #selector(LeaveApplicationViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        //sacustomView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),forControlEvents:UIControlEvents.TouchUpInside)
        customView.HeaderLabel.textAlignment = .center
        customView.HeaderLabel.font = UIFont.boldSystemFont(ofSize: 16.0)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "LeaveApplication"
        
    }
    
    func buttonClicked(sender:UIButton)
    {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "LeaveApprovalViewController")), animated: true)
    }
    
    
    @IBAction func ApprovalBtnAction(_ sender: AnyObject)
    {
        uploadfile.setObject(LibraryAPI.sharedInstance.Leaveapprovalhrd.object(forKey: "LEAVE APP NUMBER")!, forKey: "LeaveApplicationNumber" as NSCopying)
        uploadfile.setObject(LibraryAPI.sharedInstance.Userid, forKey: "Authoriser" as NSCopying)
        uploadfile.setObject("A", forKey: "AuthorisingFlag" as NSCopying)
        uploadfile.setObject("", forKey: "Rejection_Text_1" as NSCopying)
        uploadfile.setObject("", forKey: "Rejection_Text_2" as NSCopying)
        uploadfile.setObject("", forKey: "Rejection_Text_3" as NSCopying)
        uploadfile.setObject("", forKey: "Rejection_Text_4" as NSCopying)
        uploadfile.setObject("", forKey: "Rejection_Text_5" as NSCopying)
        self.uploaddata()
        if(jsonresult.count > 0)
        {
            let Status = jsonresult.object(forKey: "Status")as? String
            let Submit = jsonresult.object(forKey: "Submit")as? String
            if (Submit == "Y")
            {
                let alertController = UIAlertController(title: "Alert", message: "Updated SuccessFully" , preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                    if LibraryAPI.sharedInstance.Globalflag == "N"
                    {
                        let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "HRD1DetailsViewController") as! HRD1DetailsViewController
                        self.navigationController?.pushViewController(lvc, animated: false)
                    }
                    else
                    {
                        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
                    }
                }
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion:nil)
            }
            else
            {
                self.NormalAlert(title: "Alert", Messsage: "Submisson Failed! Retry")
            }
        }
        else
        {
            self.NormalAlert(title: "Alert", Messsage: "Submisson Failed! Retry")
        }
    }
    @IBAction func RejectedbtnAction(_ sender: AnyObject)
    {
        let popup = PopupController
            .create(parentViewController: self.parent!)
            .show(childViewController: ReasonRejectViewController.instance())
            .didShowHandler { popup in
            }
            .didCloseHandler { _ in
        }
        let container = ReasonRejectViewController.instance()
        container.closeHandler = { _ in
            popup.dismiss()
        }
        popup.show(childViewController: container)
    }
    
    func uploaddata()
    {
        let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)LeaveDetails.asmx/LeaveApproval")! as URL)
        request.httpMethod = "POST"
        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
        request.httpBody = try? JSONSerialization.data(withJSONObject: self.uploadfile, options: JSONSerialization.WritingOptions(rawValue: 0))
        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
        //  var err: NSError
        do
        {
            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
            jsonresult = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
        }
        catch (let e)
        {
            
        }
        
    }
}
