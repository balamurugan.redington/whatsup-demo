//
//  BreathAnalysisInvoiceViewController.swift
//  Whatsup
//
//  Created by truetech on 24/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import KCFloatingActionButton

class BreathAnalysisInvoiceViewController: UIViewController,KCFloatingActionButtonDelegate,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet var Backbutton: UIButton!
    
    @IBOutlet var menubtn: UIButton!
    
    @IBOutlet weak var nodataview: UIView!
    @IBOutlet var BreadthInvoice: UITableView!
    var code=String()
    var responsestring=NSMutableArray()
    var fab=KCFloatingActionButton()
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.layoutFAB()
        BreadthInvoice.register(UINib(nibName: "BreadthAnalysisinvoiceTableViewCell", bundle: nil), forCellReuseIdentifier: "BreadthAnalysisinvoiceTableViewCell")
        customnavigation()
        self.BreadthInvoice.delegate = self
        self.BreadthInvoice.dataSource = self
        BreadthInvoice.tableFooterView = UIView(frame: .zero)
        BreadthInvoice.separatorStyle = .none
        menubtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        Backbutton.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        Backbutton.addTarget(self, action: #selector(BreathAnalysisInvoiceViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        menubtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        LibraryAPI.sharedInstance.tracker="2"
        
    }
    override func viewWillAppear(_ animated: Bool) {
         self.fetchvalues()
    }
    
    func customnavigation()
        
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(BreathAnalysisInvoiceViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Breadth Analysis"
    }
    func fetchvalues()
    {
        local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
            
            self.Loaddata()
            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
                
               
                self.BreadthInvoice.reloadData()
                
            }
        }
        
        // new version dispatch Que

    }
    
      
    override func viewDidAppear(_ animated: Bool) {
        
//        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        appdelegate.shouldSupportAllOrientation = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func Loaddata()
    {
        
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/CustomerBreadthAnalysisDetail?Userid=\(LibraryAPI.sharedInstance.Userid)&BranchCode=\(LibraryAPI.sharedInstance.BranchCodeNew)&BizCode=\(code)", completion: { (jsonmutable) in
            self.responsestring = jsonmutable
        if(self.responsestring.count>0)
        {
              DispatchQueue.main.async {
                self.nodataview.isHidden = true
                self.BreadthInvoice.isHidden=false
                 self.local_activityIndicator_stop()
                 self.BreadthInvoice.reloadData()
            }
        }
        else
        {
            DispatchQueue.main.async {
                 self.local_activityIndicator_stop()
                self.BreadthInvoice.isHidden=true
                self.nodataview.isHidden = false
                
            }
            
        }
        })
    }
    
    
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            
            
            self.fab.close()
        }
        
        fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
            
            
            self.fetchvalues()
            
            self.fab.close()
        }
        
        
        fab.fabDelegate = self
        
        self.view.addSubview(fab)
        
    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
        
    }
    
    
    
    func buttonClicked(sender:UIButton)
    {
        LibraryAPI.sharedInstance.tracker="2"
        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "BreadthanalysisViewController") as! BreadthanalysisViewController
        //let dic = LibraryAPI.sharedInstance.breadthvalues[IndexPath.row] as! NSDictionary
        lvc.Bizcode = code
        self.navigationController?.pushViewController(lvc, animated: true)
    }
    
    
    //TableView Delegates
   func numberOfSections(in tableView: UITableView) -> Int {
        if responsestring.count > 0
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(responsestring.count == 1)
        {
            let dic = responsestring[0] as! NSDictionary
            if(dic.object(forKey: "Message") != nil)
            {
                return 0
            }
            else
            {
                return responsestring.count
            }
            
        }
        else
        {
            return responsestring.count
        }
        
        
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BreadthAnalysisinvoiceTableViewCell") as! BreadthAnalysisinvoiceTableViewCell
        
        if(responsestring.count > 0)
        {
            if(indexPath.row%2==0)
            {
                cell.contentview.backgroundColor=(UIColor.init(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0))
            }
            else
            {
                //cell.contentView.backgroundColor=UIColor.whiteColor()
                cell.contentview.backgroundColor=(UIColor.init(red: 255/255.0, green: 204/255.0, blue: 212/255.0, alpha: 1.0))
            }
            let dic = responsestring[indexPath.row] as! NSDictionary
            
            cell.Customercode.text=dic.object(forKey: "Customer Code") as? String
            cell.Buisness.text=String(dic.object(forKey: "Business Code") as! String)
            cell.value.text=String(dic.object(forKey: "Sales Achieved Current month") as! Double)
            
            if(dic.object(forKey: "Flag") as! String == "U")
            {
                cell.Likeimage.image=UIImage(named:"like1.png")
            }
            else
                if(dic.object(forKey: "Flag") as! String == "D")
                {
                    cell.Likeimage.image=UIImage(named:"unlike.png")
                }
                else
                    
                {
                    cell.Likeimage.image=UIImage(named:"no_like.png")
            }
  
        }
        else
        {
            
        }
        
        cell.selectionStyle = .none
        return cell as BreadthAnalysisinvoiceTableViewCell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func backbuttonClicked(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
}
