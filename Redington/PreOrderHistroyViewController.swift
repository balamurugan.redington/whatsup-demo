//
//  PreOrderHistroyViewController.swift
//  Redington
//
//  Created by RIL-ITTECH on 29/03/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import KCFloatingActionButton

class PreOrderHistroyViewController:  UIViewController,KCFloatingActionButtonDelegate,UITextFieldDelegate,UITextViewDelegate  {

    @IBOutlet weak var customercodetf: UITextField!
    @IBOutlet weak var fromdatetf: UITextField!
    @IBOutlet weak var submitbtn: UIButton!
    @IBOutlet weak var pickerview: UIView!
    @IBOutlet weak var datepicker: UIDatePicker!
    @IBOutlet weak var todatetf: UITextField!
    
    @IBOutlet weak var datepicker2: UIDatePicker!
    var currenttextfield = String()
    var datevalue = Int()
    var CustomcodeCheck : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datepicker2.isHidden = true
        self.customnavigation()
        let arrow = UIImageView()
        let image = UIImage(named: "cal.png")
        arrow.image = image!.imageWithColor(tintColor: UIColor.init(red:239/255.0 , green: 88/255.0, blue: 88/255.0, alpha: 1.0)).withRenderingMode(.alwaysOriginal)
        arrow.frame = CGRect(x: 0.0,y: 0.0, width: arrow.image!.size.width+10.0,height: 15);
        arrow.contentMode = UIViewContentMode.center
        self.fromdatetf.rightView = arrow;
        self.fromdatetf.rightViewMode = UITextFieldViewMode.always
        
        let arrow1 = UIImageView()
        let image1 = UIImage(named: "cal.png")
        arrow1.image = image1!.imageWithColor(tintColor: UIColor.init(red:239/255.0 , green: 88/255.0, blue: 88/255.0, alpha: 1.0)).withRenderingMode(.alwaysOriginal)
        arrow1.frame = CGRect(x: 0.0, y:  0.0, width: arrow.image!.size.width+10.0, height: 15);
        arrow1.contentMode = UIViewContentMode.center
        self.todatetf.rightView = arrow1;
        self.todatetf.rightViewMode = UITextFieldViewMode.always

        
      submitbtn.layer.masksToBounds=true
       submitbtn.layer.cornerRadius=20.0

        self.navigationController?.setNavigationBarHidden(true, animated: false)
        fromdatetf.delegate = self
        todatetf.delegate = self
        customercodetf.delegate = self
        fromdatetf.tag = 1
        
        todatetf.tag = 2
        customercodetf.tag = 3

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
     

    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    override func viewWillAppear(_ animated: Bool) {
//        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        appdelegate.shouldSupportAllOrientation = false
        
        if CustomcodeCheck == nil || CustomcodeCheck == ""
        {
            self.customercodetf.text = ""
        }
        else
        {
            self.customercodetf.text = LibraryAPI.sharedInstance.CustomerCode
        }
        
    }

    func customnavigation() {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x, y:  self.view.frame.origin.y, width: self.view.frame.width, height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(OrderEnquiryViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "PreOrder History"
    }

    func buttonClicked(sender:UIButton) {
        
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if(textField.tag==1) {
            if customercodetf.text == "" {
                
                let alert = UIAlertController(title: "Sorry..!!", message: "Enter the CustomerCode", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return true
            } else {
                if datevalue == 1 {
                     currenttextfield=""
                    fromdatetf.text = ""
                    todatetf.text = ""
                    datepicker.maximumDate = NSDate() as Date
                    pickerview.isHidden=false
                    customercodetf.resignFirstResponder()
                    fromdatetf.resignFirstResponder()
                    todatetf.resignFirstResponder()
                   // datepicker.maximumDate = NSDate()
                    currenttextfield="From"

                } else {
                    datepicker.maximumDate = NSDate() as Date
                    pickerview.isHidden=false
                    customercodetf.resignFirstResponder()
                    fromdatetf.resignFirstResponder()
                    todatetf.resignFirstResponder()
                    datepicker.maximumDate = NSDate() as Date
                    currenttextfield="From"
                }
                return false
            }
        }
        
        if(textField.tag==2)
        {
            if customercodetf.text == ""
            {
                let alert = UIAlertController(title: "Sorry..!!", message: "Enter the CustomerCode", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return true
            }
            else
            {
                if fromdatetf.text == ""
                {
                    let alert = UIAlertController(title: "Sorry..!!", message: "Enter the From date", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return true
                }
                else
                {
                    pickerview.isHidden=false
                    customercodetf.resignFirstResponder()
                    fromdatetf.resignFirstResponder()
                    todatetf.resignFirstResponder()
                    datepicker.minimumDate = datepicker.date
                    _ = NSDate()
//                    datepicker.maximumDate=date
                    currenttextfield="to"
                    
                    datevalue = 1
                    return false
                }
            }
        }
        
        if(textField.tag == 3)
        {
            LibraryAPI.sharedInstance.Currentcustomerlookup="PreOrderHistory"
            if #available(iOS 10.0, *) {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: CustomerLookupViewController.instance())
                    .didShowHandler { popup in
                        self.customercodetf.resignFirstResponder()
                    }
                    .didCloseHandler { _ in
                }
                let container = CustomerLookupViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                }
                popup.show(childViewController: container)
                return false
            }
            else
            {
                
            }
            
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField.tag==1)
        {
            fromdatetf.resignFirstResponder()
        }
        else if (textField.tag == 2)
        {
            todatetf.resignFirstResponder()
        }
        else if(textField.tag == 3)
        {
            customercodetf.resignFirstResponder()
        }
        return true
    }
    
    
    @IBAction func donebtn(_ sender: AnyObject)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.full
        if(currenttextfield=="From")
        {
            dateFormatter.dateFormat = "yyyy-MM-dd"
            
        }
        else if(currenttextfield=="to")
        {
            dateFormatter.dateFormat = "yyyy-MM-dd"
        }
        if(currenttextfield=="to")
        {
            let strDate = dateFormatter.string(from: datepicker.date)
            
            todatetf.text=strDate
        }
        else if(currenttextfield=="From")
        {
            let strDate = dateFormatter.string(from: datepicker.date)
            fromdatetf.text=strDate
        }
        pickerview.isHidden=true;
    }
    
    
    @IBAction func CancelAction(_ sender: AnyObject)
    {
        pickerview.isHidden=true;
    }
    
    @IBAction func Submitaction(_ sender: AnyObject)
    {
        if(customercodetf.text == "")
        {
            let alert = UIAlertController(title: "Sorry..!!", message: "Enter the Customer code", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else if(fromdatetf.text == "")
        {
            let alert = UIAlertController(title: "Sorry..!!", message: "Enter the From Date", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else if(todatetf.text == "")
        {
            let alert = UIAlertController(title: "Sorry..!!", message: "Enter the To Date", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            LibraryAPI.sharedInstance.CustomerCode = customercodetf.text!
            LibraryAPI.sharedInstance.CustomerCodeFrom = fromdatetf.text!
            LibraryAPI.sharedInstance.CustomerCodeTo = todatetf.text!
            let lvc=self.storyboard?.instantiateViewController(withIdentifier: "OrderHistoryViewController") as! OrderHistoryViewController
            self.navigationController?.pushViewController(lvc, animated: true)
        }
    }


  
}
