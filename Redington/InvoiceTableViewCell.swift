//
//  InvoiceTableViewCell.swift
//  Redington
//
//  Created by truetech on 19/08/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit

class InvoiceTableViewCell: UITableViewCell {

    @IBOutlet var InvoiceBaldueLabel: UILabel!
    @IBOutlet var InvoiceValueLabel: UILabel!
    @IBOutlet var InvoiceDateLabel: UILabel!
    @IBOutlet var InvoiceNumberLabel: UILabel!
    
    
    @IBOutlet weak var oddays: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

     override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

}
