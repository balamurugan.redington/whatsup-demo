//
//  LookupTableViewCell.swift
//  Redington
//
//  Created by truetech on 14/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit

class LookupTableViewCell: UITableViewCell {

    @IBOutlet var BranchNameLabel: UILabel!
    @IBOutlet var BranchCodeLabel: UILabel!
    @IBOutlet var CustomerNameLabel: UILabel!
    @IBOutlet var CustomerCodeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
