//
//  InvoicesummerydetailsViewController.swift
//  Redington
//
//  Created by truetech on 28/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import KCFloatingActionButton

class InvoicesummerydetailsViewController: UIViewController,KCFloatingActionButtonDelegate,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var SummaryTableveiw: UITableView!
    @IBOutlet var menubtn: UIButton!
    @IBOutlet var backbtn: UIButton!
    
    var responsestring = NSMutableArray()
    var fab=KCFloatingActionButton()
        override func viewDidLoad() {
        super.viewDidLoad()
            self.layoutFAB()
           
             customnavigation()
            SummaryTableveiw.bounces = false
    }
    
    func fetchInvoicevalues()
    {
        DispatchQueue.global(qos: .background).async {
            if( LibraryAPI.sharedInstance.lastviewcontroller=="aging")
            {
                self.Loaddata1()
            }
            else
            {
                self.Loaddata()
            }
            DispatchQueue.main.async {
                
            }
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
     self.fetchInvoicevalues()
    }
       override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func BackGroundRefresh(Notification : NSNotification)
    {
       
        self.local_activityIndicator_start()
        
        // new version dispatch Que
        
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
            
            if( LibraryAPI.sharedInstance.lastviewcontroller=="aging")
            {
                self.Loaddata1()
            }
            else
            {
                self.Loaddata()
            }
            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
              
                
            }
        }
        
        // new version dispatch Que
      
        
    }
    
    
    func Loaddata()
    {
        self.local_activityIndicator_start()
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/CustomerCreditSummery?Userid=\(LibraryAPI.sharedInstance.Userid)&Month=\(LibraryAPI.sharedInstance.CurrentMonth)&Year=\(LibraryAPI.sharedInstance.CurrentYear)&BranchCode=\(LibraryAPI.sharedInstance.BranchCodeNew)", completion:  { (jsonmutable) in
            self.responsestring = jsonmutable
        if(self.responsestring.count>0)
        {
            DispatchQueue.main.async {
                self.local_activityIndicator_stop()
                 self.SummaryTableveiw.isHidden=false
                self.SummaryTableveiw.reloadData()
            }
           
        } else {
            DispatchQueue.main.async {
                self.SummaryTableveiw.isHidden=true
                let alertController = UIAlertController(title: "Alert", message: "No Data Available" , preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                    self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion:nil)
                self.local_activityIndicator_stop()
            }
        }
        })
    }
    
    func Loaddata1()
    {
        self.local_activityIndicator_start()
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/CustomerCreditSummeryHeader?Userid=\(LibraryAPI.sharedInstance.Userid)&Customer=\(LibraryAPI.sharedInstance.CustomerCode)&Month=\(LibraryAPI.sharedInstance.CurrentMonth)&Year=\(LibraryAPI.sharedInstance.CurrentYear)&BranchCode=\(LibraryAPI.sharedInstance.BranchCodeNew)", completion:  { (jsonmutable) in
            self.responsestring = jsonmutable
            if(self.responsestring.count>0)
        {
          
          DispatchQueue.main.async {
                self.local_activityIndicator_stop()
                self.SummaryTableveiw.isHidden=false
            self.SummaryTableveiw.reloadData()
            }
            
        } else  {
            DispatchQueue.main.async {
                self.SummaryTableveiw.isHidden=true
                let alertController = UIAlertController(title: "Alert", message: "No Data Available" , preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                    self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion:nil)
                self.local_activityIndicator_stop()
            }
           
            
        }
        })
        
        
    }

    
    
        func layoutFAB() {
            fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            
                self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            
            
            self.fab.close()
        }
            
            fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
            
                
                self.fetchInvoicevalues()
                
                self.fab.close()
        }
            
          fab.fabDelegate = self
        
        self.view.addSubview(fab)
        
    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
      
    }
    

    func customnavigation()
        
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(InvoicesummerydetailsViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "OverDue Invoice Summary"
    }
    
    func buttonClicked(sender:UIButton)
    {
        LibraryAPI.sharedInstance.tracker="1"
         if(LibraryAPI.sharedInstance.lastviewcontroller=="aging")
         {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: AgeingAnalysisViewController.classForCoder()) {
                    self.navigationController?.popToViewController(controller as UIViewController, animated: true)
                    break
                }
            }
         }
        else
         {
        if (self.navigationController?.topViewController!.isKind(of: BarChart1ViewController.classForCoder()) != nil) {
            
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: BarChart1ViewController.classForCoder()) {
                    self.navigationController?.popToViewController(controller as UIViewController, animated: true)
                    break
                }
            }
         }
         else
        {
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
          //  let  lvc = self.storyboard?.instantiateViewControllerWithIdentifier("DashBoardViewController") as! DashBoardViewController
           // self.navigationController?.pushViewController(lvc, animated: false)
            
        }
        }
    }


    
    func backbuttonClicked(sender:UIButton)
    {
        LibraryAPI.sharedInstance.lastviewcontroller=""
        self.navigationController?.popViewController(animated: true)
    }
    
    //TableView Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
       
        if responsestring.count > 0
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return responsestring.count
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        var cell : InvoicesummerydetailsTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as! InvoicesummerydetailsTableViewCell
        
        cell.cellview.layer.borderColor = UIColor(red:30/255.0, green:184/255.0, blue:120/255.0, alpha: 1.0).cgColor
        cell.cellview.layer.borderWidth = 1.0
        cell.cellview.layer.cornerRadius = 5.0
        cell.selectionStyle = .none
        if(cell == nil)
        {
            cell = Bundle.main.loadNibNamed("Cell", owner: self, options: nil)![0] as! InvoicesummerydetailsTableViewCell;
            
        }
        let dic = responsestring[indexPath.row] as! NSDictionary
        if( LibraryAPI.sharedInstance.lastviewcontroller=="aging")
        {
            cell.Customercode.text=dic.object(forKey: "Customer Code") as? String
            cell.invoice.text=String(dic.object(forKey: "Total Invoice Value") as! Double)
            cell.due.text=String(dic.object(forKey: "Total Over Due") as! Double)
            cell.comitteddate.text=dic.object(forKey: "Last Committed Date") as? String
            cell.comittedamount.text = String(dic.object(forKey: "Total Committed Amount") as! Double)
            cell.value.text = String(dic.object(forKey: ">121 Days Amount") as! Double)
            cell.Name.text = dic.object(forKey: "Customer Name") as? String

        }
        else
        {
            cell.Customercode.text=dic.object(forKey: "Customer Code") as? String
              cell.invoice.text=String(dic.object(forKey: "Invoice Value") as! Double)
            cell.due.text=String(dic.object(forKey: "Total Over Due") as! Double)
            cell.comitteddate.text=dic.object(forKey: "Last Committed Date") as? String
            cell.comittedamount.text = String(dic.object(forKey: "Total Committed Amount") as! Double)
            cell.value.text = String(dic.object(forKey: ">121 Days Amount") as! Double)
            cell.Name.text = dic.object(forKey: "Customer Name") as? String

        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dic = responsestring[indexPath.row] as! NSDictionary
        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "InvoiceViewController") as! InvoiceViewController
        LibraryAPI.sharedInstance.commitedamount=dic.object(forKey: "Total Committed Amount") as! Int
        LibraryAPI.sharedInstance.lastcommitedamount=dic.object(forKey: "Last Committed Date") as! String
        lvc.customercode=(dic.object(forKey: "Customer Code") as? String)!
        self.navigationController?.pushViewController(lvc, animated: true)

    }
    
    
    

  

}
