//
//  VisitlogdetailViewController.swift
//  Redington
//
//  Created by truetech on 04/10/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

class VisitlogdetailViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {
    
    @IBOutlet var textviewdescription: UITextView!
    @IBOutlet var Visitidlabel: UILabel!
    @IBOutlet var popupview: UIView!
    @IBOutlet var visittableview: UITableView!
    @IBOutlet var menubtn: UIButton!
    @IBOutlet var backbtn: UIButton!
    @IBOutlet var Visitid: UILabel!
    var keyarr=NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        customnavigation()
        popupview.layer.cornerRadius = 10.0
        popupview.clipsToBounds = true
    }
    override func viewWillAppear(_ animated: Bool) {
        self.ParseVisitkeyinfo()
    }
    
    
    func customnavigation()
    {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(VisitlogdetailViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "VisitLog"
    }
    
    
    @IBAction func ACtion(_ sender: AnyObject)
    {
        
        visittableview.isUserInteractionEnabled=true
        popupview.isHidden=true
        
    }
    
    @IBOutlet var ClosebuttonAction: UIButton!
    
    func buttonClicked(sender:UIButton)
    {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: NewkeyifoViewController.classForCoder()) {
                self.navigationController?.popToViewController(controller as UIViewController, animated: true)
                break
            }
        }
    }
    
    
    func ParseVisitkeyinfo()
    {
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
        self.getsuccess(params: [:], urlhit:  "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/CustomervisitLogView?Customer=\(LibraryAPI.sharedInstance.CustomerCode)&user=\(LibraryAPI.sharedInstance.Username)", completion:  { (jsonmutable) in
            self.keyarr = jsonmutable
            print("data in visit lod details",self.keyarr)
            DispatchQueue.main.async {
                self.local_activityIndicator_stop()
                   self.visittableview.reloadData()
            }
            })
            }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //TableView Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if(keyarr.count > 0)
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return keyarr.count
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : VisitlogdetailTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as! VisitlogdetailTableViewCell
        cell.cellview.layer.borderColor = UIColor(red:30/255.0, green:184/255.0, blue:120/255.0, alpha: 1.0).cgColor
        cell.cellview.layer.borderWidth = 1.0
        cell.cellview.layer.cornerRadius = 5.0
        
        if(cell == nil)
        {
            cell = Bundle.main.loadNibNamed("Cell", owner: self, options: nil)![0] as! VisitlogdetailTableViewCell;
            
        }
        
        let dic                     = keyarr[indexPath.row] as! NSDictionary
        cell.visitid.text           = dic.object(forKey: "VISIT ID") as? String
        cell.typelabel.text         = dic.object(forKey: "TYPE") as? String
        cell.Customercode.text      = dic.object(forKey: "CUSTOMER CODE") as? String
        cell.dateofvisit.text       = dic.object(forKey: "DATE OF VISIT") as? String
        let time                    = dic.object(forKey: "FROM TIME") as? String
        cell.fromtimelabel1.text    = time
        let time1                   = dic.object(forKey: "TO TIME") as? String
        cell.fromtimelabel.text     = time1
        cell.Metwhomlabel.text      = dic.object(forKey: "MET WHOM") as? String
        cell.Designationlabel.text  = dic.object(forKey: "DESIGNATION") as? String
        cell.usernamelbl.text       = dic.object(forKey: "User Visited") as? String
        
        
        cell.selectionStyle = .none
        return cell as VisitlogdetailTableViewCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dic1                     = keyarr[indexPath.row] as! NSDictionary
        Visitidlabel.text=""
        visittableview.isUserInteractionEnabled=false
        popupview.isHidden=false
        Visitidlabel.text        = dic1.object(forKey: "VISIT ID") as? String
        textviewdescription.text = dic1.object(forKey: "MEETING DESCRIPTION") as? String
    }
    
}
