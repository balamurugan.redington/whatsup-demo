//
//  VisitlogdetailTableViewCell.swift
//  Redington
//
//  Created by truetech on 04/10/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit

class VisitlogdetailTableViewCell: UITableViewCell {

    @IBOutlet weak var usernamelbl: UILabel!
    @IBOutlet var Customercode: UILabel!
    @IBOutlet var fromtimelabel1: UILabel!
    @IBOutlet var fromtimelabel: UILabel!
    @IBOutlet var Designationlabel: UILabel!
    @IBOutlet var Metwhomlabel: UILabel!
    @IBOutlet var dateofvisit: UILabel!
    @IBOutlet var typelabel: UILabel!
    @IBOutlet var visitid: UILabel!
    
    @IBOutlet weak var cellview: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

   override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
