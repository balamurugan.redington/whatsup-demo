//
//  CheckbendingViewController.swift
//  Redington
//
//  Created by RIL-ITTECH on 21/02/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class CheckbendingViewController:UIViewController,PopupContentViewController,UITableViewDelegate,UITableViewDataSource {
    
    var closeHandler: (() -> Void)?
    var titlearray=[String]()
    
    
    var localArray : NSMutableArray!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        
    }
    
    class func instance() -> CheckbendingViewController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "CheckbendingViewController") as! CheckbendingViewController
    }
    
    @IBAction func CloseAction(_ sender: AnyObject)
    {
        closeHandler?()
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func sizeForPopup(popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
       // return CGSizeMake(180,220)
        return CGSize(width: 180, height: 220)
    }
    
    
    //TableView Delegates
    
     func numberOfSections(in tableView: UITableView) -> Int {
        if LibraryAPI.sharedInstance.CheckBendingDetailArray.count > 0
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        if LibraryAPI.sharedInstance.CheckBendingDetailArray.count > 0
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell : checkbendingTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as! checkbendingTableViewCell
        
        cell.businesscodependinglabel.text =  LibraryAPI.sharedInstance.CheckBendingDetailArray.object(at: indexPath.row) as? String
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        LibraryAPI.sharedInstance.SelectedValue = LibraryAPI.sharedInstance.CheckBendingDetailArray.object(at: indexPath.row) as? String
        let slideInFromLeftTransition = CATransition()
        slideInFromLeftTransition.type = kCAGravityTop
        slideInFromLeftTransition.subtype = kCATransitionFromLeft
        slideInFromLeftTransition.duration = 0.2
        slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "ChequeBounceDetailsViewController") as! ChequeBounceDetailsViewController
        lvc.Check = 0
        self.navigationController?.view.layer.add(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
        closeHandler?()
        self.navigationController?.pushViewController(lvc, animated: false)
        
    }
    
    
    @IBAction func AllData(_ sender: AnyObject)
    {
        let slideInFromLeftTransition = CATransition()
        slideInFromLeftTransition.type = kCAGravityTop
        slideInFromLeftTransition.subtype = kCATransitionFromLeft
        slideInFromLeftTransition.duration = 0.2
        slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "ChequeBounceDetailsViewController") as! ChequeBounceDetailsViewController
        self.navigationController?.view.layer.add(slideInFromLeftTransition, forKey: "slideInFromLeftTransition")
        closeHandler?()
        self.navigationController?.pushViewController(lvc, animated: false)
    }
    
    
    
}
