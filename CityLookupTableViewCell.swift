//
//  CityLookupTableViewCell.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 07/06/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class CityLookupTableViewCell: UITableViewCell {

    @IBOutlet weak var citymode: UILabel!
    
    @IBOutlet weak var cityname: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
