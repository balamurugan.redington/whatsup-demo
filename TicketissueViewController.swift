//
//  OrderviewDetailsViewController.swift
//  Whatsup
//
//  Created by truetech on 21/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import KCFloatingActionButton

class TicketissueViewController: UIViewController,KCFloatingActionButtonDelegate,UIAlertViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPopoverControllerDelegate,UITableViewDataSource, UITableViewDelegate{
    
    @IBOutlet weak var TicketView: UIView!
    @IBOutlet weak var typeView: UIView!
    @IBOutlet weak var checkboxlabel: UILabel!
    @IBOutlet weak var typebtn: UIButton!
    @IBOutlet  var typelbl: UILabel!
    @IBOutlet var OrderTableView: UITableView!
    var picker:UIImagePickerController? = UIImagePickerController()
    var fromdate=String()
    var todate=String()
    var  responsestring = NSMutableArray()
    var fab = KCFloatingActionButton()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customnavigation()
        OrderTableView.layer.borderWidth = 2
        OrderTableView.layer.borderColor = CustomColor.ShareColor.hexStringToUIColor(hex: CustomColor.ShareColor.customNavyBlue, alpha: 1.0).cgColor
        typeView.layer.borderColor = CustomColor.ShareColor.hexStringToUIColor(hex: CustomColor.ShareColor.customNavyBlue, alpha: 1.0).cgColor
        typeView.layer.borderWidth = 2
        TicketView.layer.borderColor = CustomColor.ShareColor.hexStringToUIColor(hex: CustomColor.ShareColor.customNavyBlue, alpha: 1.0).cgColor
        TicketView.layer.borderWidth = 2
        TicketView.layer.cornerRadius = typeView.frame.size.height / 4
        typeView.layer.cornerRadius = typeView.frame.size.height / 4
        typeView.layer.masksToBounds = true
        OrderTableView.delegate = self
        OrderTableView.dataSource=self
        self.layoutFAB()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        if checkboxlabel.text == "JBA"
        {
            self.JBA()
        }
        else{
            self.Loaddata()
        }
    }
    func Loaddata()
    {
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Ticket.asmx/TicketDetails?UserId=\(LibraryAPI.sharedInstance.Userid)&Type=WHATSUP", completion: { (jsonmutable) in
                    self.responsestring = jsonmutable
                   
                    if(self.responsestring.count>0)
                    {
                        DispatchQueue.main.async {
                            self.OrderTableView.isHidden=false
                             self.local_activityIndicator_stop()
                            self.OrderTableView.reloadData()
                        }
                    }
                    else
                    {
                         self.local_activityIndicator_stop()
                        let alertController = UIAlertController(title: "Alert", message: "No Data Available" , preferredStyle: .alert)
                        let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                            self.navigationController?.popViewController(animated: true)
                        }
                        alertController.addAction(cancelAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                })
            }
        }
       
       
    }
    func JBA() {
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Ticket.asmx/TicketDetails?UserId=\(LibraryAPI.sharedInstance.Userid)&Type=JBA", completion: { (jsonmutable) in
                    self.responsestring = jsonmutable
                    
                    if(self.responsestring.count>0)
                    {
                        DispatchQueue.main.async {
                            self.OrderTableView.isHidden=false
                            self.local_activityIndicator_stop()
                            self.OrderTableView.reloadData()
                        }
                    } else {
                         self.local_activityIndicator_stop()
                        let alertController = UIAlertController(title: "Alert", message: "No Data Available" , preferredStyle: .alert)
                        let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                            self.navigationController?.popViewController(animated: true)
                        }
                        alertController.addAction(cancelAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                })
            }
        }
        
    }
    
    func customnavigation() {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x:self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: self.view.frame.height/10.3);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(TicketissueViewController.backbuttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Ticket Issues"
        
    }
    
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            self.fab.close()
        }
        fab.addItem("NewTicket", icon: UIImage(named: "ticket.png")!) { item in
            let lvc=self.storyboard?.instantiateViewController(withIdentifier: "Helpdisk1ViewController") as!Helpdisk1ViewController
            self.navigationController?.pushViewController(lvc, animated: true)
            self.fab.close()
        }
        fab.fabDelegate = self
        self.view.addSubview(fab)
    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
    }
    
    func backbuttonClicked(sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return responsestring.count > 0 ? 1 : 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return responsestring.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : TicketissueTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as! TicketissueTableViewCell
        if(cell == nil) {
            cell = Bundle.main.loadNibNamed("Cell", owner: self, options: nil)![0] as! TicketissueTableViewCell;
        }
        if(indexPath.row % 2 == 0) {
            cell.contentView.backgroundColor = CustomColor.ShareColor.hexStringToUIColor(hex: CustomColor.ShareColor.ticktissueCel1Color, alpha: 1.0)
        } else {
            cell.contentView.backgroundColor = CustomColor.ShareColor.hexStringToUIColor(hex: CustomColor.ShareColor.ticktissueCel2Color, alpha: 1.0)
        }
        let dic = responsestring[indexPath.row] as! NSDictionary
        cell.ticketnumber.text=dic.object(forKey: "TICKET NO") as? String
        cell.ticketdate.text=String(dic.object(forKey: "TCK RSE DATE") as! String)
        cell.ticketstatus.text=String(dic.object(forKey: "STATUS") as! String)
        if(cell.ticketstatus.text=="Completed")  {
            cell.ticketstatus.textColor = UIColor.init(red: 211/255.0, green: 213/255.0, blue: 0, alpha: 1.0)
        }  else if(cell.ticketstatus.text=="Pending") {
            cell.ticketstatus.textColor=UIColor.red
        } else if(cell.ticketstatus.text=="Closed") {
            cell.ticketstatus.textColor=UIColor.init(red: 13/255.0, green: 98/255.0, blue: 1/255.0, alpha: 1.0)
        }
        cell.selectionStyle = .none
        return cell as TicketissueTableViewCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let lvc=self.storyboard?.instantiateViewController(withIdentifier: "Helpdisk2ViewController") as! Helpdisk2ViewController
        let dic = responsestring[indexPath.row] as! NSDictionary
        LibraryAPI.sharedInstance.ticketstatus = String(dic.object(forKey: "STATUS") as! String)
        if (LibraryAPI.sharedInstance.ticketstatus == "Pending") {
            lvc.category=(dic.object(forKey: "\("CALL TYPE")") as? String)!
            lvc.des=(dic.object(forKey:"DESCRIPTION") as? String)!
            lvc.mobile=(dic.object(forKey:"MOBILE NO") as? String)!
            self.navigationController?.pushViewController(lvc, animated: true)
        } else if (LibraryAPI.sharedInstance.ticketstatus == "Closed") {
            let lvc=self.storyboard?.instantiateViewController(withIdentifier: "Helpdisk2ViewController") as! Helpdisk2ViewController
            lvc.category=(dic.object(forKey:"CALL TYPE") as? String)!
            lvc.des=(dic.object(forKey:"DESCRIPTION") as? String)!
            lvc.mobile=(dic.object(forKey:"MOBILE NO") as? String)!
            lvc.feedbackstar=(dic.object(forKey:"FEEDBACK STAR") as? String)!
            lvc.helpdesk=(dic.object(forKey:"HELPDESK COMMENTS") as? String)!
            lvc.feedbackcmt=(dic.object(forKey:"FEEDBACK COMMENTS") as? String)!
            self.navigationController?.pushViewController(lvc, animated: true)
        } else if (LibraryAPI.sharedInstance.ticketstatus == "Completed")  {
            lvc.category=(dic.object(forKey:"CALL TYPE") as? String)!
            lvc.des=(dic.object(forKey:"DESCRIPTION") as? String)!
            lvc.mobile=(dic.object(forKey:"MOBILE NO") as? String)!
            lvc.number=(dic.object(forKey:"TICKET NO") as? String)!
            lvc.helpdesk=(dic.object(forKey:"HELPDESK COMMENTS") as? String)!
            self.navigationController?.pushViewController(lvc, animated: true)
        }
    }
    
    @IBAction func typebuttonpopup(_ sender: AnyObject)
    { let alert:UIAlertController=UIAlertController(title: "Choose option", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let jbaaction = UIAlertAction(title: "JBA", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.checkboxlabel.text = "JBA"
            self.local_activityIndicator_start()
            DispatchQueue.global(qos: .background).async {
                  self.JBA()
                DispatchQueue.main.async {
                    
                }
            }
        }
        let whatsup = UIAlertAction(title: "WHATSUP", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.checkboxlabel.text = "WHATSUP"
            self.local_activityIndicator_start()
            DispatchQueue.global(qos: .background).async {
                 self.Loaddata()
                DispatchQueue.main.async {
                    
                }
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
        }
        picker?.delegate = self
        alert.addAction(jbaaction)
        alert.addAction(whatsup)
        alert.addAction(cancelAction)
        if UIDevice.current.userInterfaceIdiom == .phone {
            self.present(alert, animated: true, completion: nil)
        } else {
        }
    }
}
