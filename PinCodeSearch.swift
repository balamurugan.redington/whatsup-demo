//
//  PinCodeSearch.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 15/02/18.
//  Copyright © 2018 truetech. All rights reserved.
//

import Foundation


struct  PinCodeSearch
{
    var CUSTOMER_CODE : String
    var DELIVERY_SEQN : String
    var CUSTOMER_NAME : String
    var CUSTOMER_ADD1 : String
    var CUSTOMER_ADD2 : String
    var CUSTOMER_ADD3 : String
    var CUSTOMER_ADD4 : String
    var CUSTOMER_ADD5 : String
    var PINCODE : String
    var STATUS : String
    var BRANCH_CODE : String
    var BRANCH_NAME : String
    var CREDIT_LIMIT : NSNumber
    var AVALIABLE_LIMIT : NSNumber
    var RECORD_NUMBER : NSNumber
    var PAGE_NUMBER : NSNumber
    var TOTAL_PAGE : NSNumber
    
    init() {
        self.CUSTOMER_CODE = ""
        self.DELIVERY_SEQN = ""
        self.CUSTOMER_NAME = ""
        self.CUSTOMER_ADD1 = ""
        self.CUSTOMER_ADD2 = ""
        self.CUSTOMER_ADD3 = ""
        self.CUSTOMER_ADD4 = ""
        self.CUSTOMER_ADD5 = ""
        self.PINCODE = ""
        self.STATUS = ""
        self.BRANCH_CODE = ""
        self.BRANCH_NAME = ""
        self.CREDIT_LIMIT = 0
        self.AVALIABLE_LIMIT = 0
        self.RECORD_NUMBER = 0
        self.PAGE_NUMBER = 0
        self.TOTAL_PAGE = 0
        
    }
    
    init(data : AnyObject){
        
        if let temp = data["CUSTOMER CODE"] as? String {
            self.CUSTOMER_CODE = temp
        }else {
            self.CUSTOMER_CODE = ""
        }
        
        if let temp = data["DELIVERY SEQN"] as? String {
            self.DELIVERY_SEQN = temp
        }else {
            self.DELIVERY_SEQN = ""
        }
        
        if let temp = data["CUSTOMER NAME"] as? String {
            self.CUSTOMER_NAME = temp
        }else {
            self.CUSTOMER_NAME = ""
        }
        
        if let temp = data["CUSTOMER ADD1"] as? String {
            self.CUSTOMER_ADD1 = temp
        }else {
            self.CUSTOMER_ADD1 = ""
        }
        if let temp = data["CUSTOMER ADD2"] as? String {
            self.CUSTOMER_ADD2 = temp
        }else {
            self.CUSTOMER_ADD2 = ""
        }
        if let temp = data["CUSTOMER ADD3"] as? String {
            self.CUSTOMER_ADD3 = temp
        }else {
            self.CUSTOMER_ADD3 = ""
        }
        
        if let temp = data["CUSTOMER ADD4"] as? String {
            self.CUSTOMER_ADD4 = temp
        }else {
            self.CUSTOMER_ADD4 = ""
        }
        
        if let temp = data["CUSTOMER ADD5"] as? String {
            self.CUSTOMER_ADD5 = temp
        }else {
            self.CUSTOMER_ADD5 = ""
        }
        if let temp = data["PINCODE"] as? String {
            self.PINCODE = temp
        }else {
            self.PINCODE = ""
        }
        
        if let temp = data["STATUS"] as? String {
            self.STATUS = temp
        }else {
            self.STATUS = ""
        }
        
        if let temp = data["BRANCH CODE"] as? String {
            self.BRANCH_CODE = temp
        }else {
            self.BRANCH_CODE = ""
        }
        if let temp = data["BRANCH NAME"] as? String {
            self.BRANCH_NAME = temp
        }else {
            self.BRANCH_NAME = ""
        }
        if let temp = data["CREDIT LIMIT"] as? NSNumber {
            self.CREDIT_LIMIT = temp
        }else {
            self.CREDIT_LIMIT = 0
        }
        if let temp = data["AVALIABLE LIMIT"] as? NSNumber {
            self.AVALIABLE_LIMIT = temp
        }else {
            self.AVALIABLE_LIMIT = 0
        }
        
        if let temp = data["RECORD NUMBER"] as? NSNumber {
            self.RECORD_NUMBER = temp
        }else {
            self.RECORD_NUMBER = 0
        }
        if let temp = data["PAGE NUMBER"] as? NSNumber {
            self.PAGE_NUMBER = temp
        }else {
            self.PAGE_NUMBER = 0
        }
        if let temp = data["TOTAL PAGE"] as? NSNumber {
            self.TOTAL_PAGE = temp
        }else {
            self.TOTAL_PAGE = 0
        }
        
    }

}
