//
//  LeaveTimeTableViewCell.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 26/07/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class LeaveTimeTableViewCell: UITableViewCell {
    @IBOutlet weak var punchdatelbl: UILabel!
    
    @IBOutlet weak var lesthan945: UILabel!
    
    @IBOutlet weak var lessthan11lbl: UILabel!
    
    @IBOutlet weak var pottimelbl: UILabel!
    
    @IBOutlet weak var intimelbl: UILabel!
    @IBOutlet weak var latecominglbl: UILabel!

    @IBOutlet weak var leaveview: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
