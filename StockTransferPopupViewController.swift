//
//  StockTransferPopupViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 27/07/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class StockTransferPopupViewController: UIViewController,PopupContentViewController {
    
    
    @IBOutlet weak var oredernolbl: UILabel!
    @IBOutlet weak var invoicenolbl: UILabel!
    @IBOutlet weak var invoicedatelbl: UILabel!
    @IBOutlet weak var invoicevaluelbl: UILabel!
    @IBOutlet weak var tostockroomlbl: UILabel!
    @IBOutlet weak var fromstockroomlbl: UILabel!
    @IBOutlet weak var quantitylbl: UILabel!
    @IBOutlet weak var dispatchdatelbl: UILabel!
    @IBOutlet weak var dispatchdetailslbl: UILabel!
    @IBOutlet weak var transitdayslbl: UILabel!
    @IBOutlet weak var expecteddeliverylbl: UILabel!
    @IBOutlet weak var dateofreceiptlbl: UILabel!
    @IBOutlet weak var courierdatelbl: UILabel!
    @IBOutlet weak var fromdatelbl: UILabel!
    @IBOutlet weak var todatelbl: UILabel!
    
    var closeHandler: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("StockTransferPopupViewController")
        self.oredernolbl.text               = LibraryAPI.sharedInstance.Stocktransfepopuporderno.object(forKey: "Order Number")as? String
        self.invoicenolbl.text              = LibraryAPI.sharedInstance.Stocktransfepopuporderno.object(forKey: "Invoice Number")as? String
        self.invoicedatelbl.text            = LibraryAPI.sharedInstance.Stocktransfepopuporderno.object(forKey: "Invoice Date")as? String
        self.invoicevaluelbl.text           = String(LibraryAPI.sharedInstance.Stocktransfepopuporderno.object(forKey: "Invoice Value")as! Double)
        self.tostockroomlbl.text            = LibraryAPI.sharedInstance.Stocktransfepopuporderno.object(forKey: "To Stock Room")as? String
        self.fromstockroomlbl.text          = LibraryAPI.sharedInstance.Stocktransfepopuporderno.object(forKey: "From Stock Room")as? String
        self.quantitylbl.text               = String(describing: LibraryAPI.sharedInstance.Stocktransfepopuporderno.object(forKey: "Quantity")as! NSNumber)
        self.dispatchdatelbl.text           = LibraryAPI.sharedInstance.Stocktransfepopuporderno.object(forKey: "Dispatch Date")as? String
        self.dispatchdetailslbl.text        = LibraryAPI.sharedInstance.Stocktransfepopuporderno.object(forKey: "Dispatch Details")as? String
        self.transitdayslbl.text            = String(describing: LibraryAPI.sharedInstance.Stocktransfepopuporderno.object(forKey: "Transit Days")as! NSNumber)
        self.expecteddeliverylbl.text       = LibraryAPI.sharedInstance.Stocktransfepopuporderno.object(forKey: "Expected Delivery DATE")as? String
        self.dateofreceiptlbl.text          = LibraryAPI.sharedInstance.Stocktransfepopuporderno.object(forKey: "Date of Receipt")as? String
        self.courierdatelbl.text            = LibraryAPI.sharedInstance.Stocktransfepopuporderno.object(forKey: "Courier Code")as? String
        self.fromdatelbl.text               = LibraryAPI.sharedInstance.Stocktransfepopuporderno.object(forKey: "From City")as? String
        self.todatelbl.text                 = LibraryAPI.sharedInstance.Stocktransfepopuporderno.object(forKey: "To City")as? String
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    class func instance() -> StockTransferPopupViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "StockTransferPopupViewController") as! StockTransferPopupViewController
    }
    
    
    @IBAction func CloseAction(_ sender: UIButton)
    {
        closeHandler?()
    }
    
    func sizeForPopup(popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: 300, height: 500)
    }

    
    @IBAction func closebtnaction(_ sender: UIButton)
    {
         closeHandler?()
    }

}
