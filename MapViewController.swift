//
//  MapViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 10/05/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import CoreLocation
import MapKit
import KCFloatingActionButton

class MapViewController: UIViewController,PopupContentViewController,KCFloatingActionButtonDelegate,CLLocationManagerDelegate,MKMapViewDelegate
{
    @IBOutlet weak var mapview: MKMapView!
    var closeHandler: (() -> Void)?
    
    var responsemap : NSMutableArray!
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
         NotificationCenter.default.addObserver(self, selector: #selector(self.notificationmethod), name: NSNotification.Name(rawValue: "GetNotification"), object: nil)
        
        LoadCustomerSearchPincode()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
     
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        PinCode_Arr1 = [PinCodeSearch]()
        
    }
    
    
    class func instance() -> MapViewController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
    }
    
    @IBAction func CloseAction(_ sender: AnyObject)
    {
       // responsemap.removeAllObjects()
        closeHandler?()
    }
    
    func sizeForPopup(popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        //return CGSizeMake(300,500)
        return CGSize(width: 300, height: 500)
    }
  
    
    func LoadCustomerSearchPincode(){
        self.local_activityIndicator_start()
        // new version dispatch Que
        
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
            
           // self.responsemap = LibraryAPI.sharedInstance.PinCodeURl(url: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/CustomerSearchPincodewise?Pincode=\(LibraryAPI.sharedInstance.Pincode)&Userid=\(LibraryAPI.sharedInstance.Userid)&RecordNo=20&PageNo=\("1")")
            // Go back to the main thread to update the UI
            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/CustomerSearchPincodewise?Pincode=\(LibraryAPI.sharedInstance.Pincode)&Userid=\(LibraryAPI.sharedInstance.Userid)&RecordNo=20&PageNo=\("1")", completion: { (jsonmutable) in
                self.responsemap = jsonmutable
                print("response data for map: ",self.responsemap)
            DispatchQueue.main.async {
                
                if(self.responsemap.count > 0)
                {
                    for validaddress in self.responsemap
                    {
                        let dic = validaddress as! NSDictionary
                        let comapanyname = dic.object(forKey:"CUSTOMER NAME") as! String
                        let add1 = dic.object(forKey:"CUSTOMER ADD1") as! String
                        let add2 = dic.object(forKey:"CUSTOMER ADD2") as! String
                        let add3 = dic.object(forKey:"CUSTOMER ADD3") as! String
                        let add4 = dic.object(forKey:"CUSTOMER ADD4") as! String
                        let add5 = dic.object(forKey:"CUSTOMER ADD5") as! String
                        let address = comapanyname + " , " + add1 + " , " + add2 + " , " + add3 + " , " + add4 + " , " + add5
                        //
                        let geocoder = CLGeocoder()
                        geocoder.geocodeAddressString(address, completionHandler: {(placemarks, error) -> Void in
                            if((error) != nil){
                                print("Error", error ?? "")
                            }
                            if let placemark = placemarks?[0] as? CLPlacemark {
                                let coordinates:CLLocationCoordinate2D = placemark.location!.coordinate
                                var region: MKCoordinateRegion = self.mapview.region
                                region.center.latitude = coordinates.latitude
                                region.center.longitude = coordinates.longitude
                                let coordinateRegion = CLLocationCoordinate2DMake(region.center.latitude, region.center.longitude)
                                print("cordinate :", region.center.latitude, region.center.longitude)
                                region.span = MKCoordinateSpanMake(0.5, 0.5)
                                let pinAnnotation = mapkitclass()
                                pinAnnotation.title = address
                                pinAnnotation.setCoordinate(newCoordinate: coordinateRegion)
                                self.mapview.setRegion(region, animated: true)
                                self.mapview.addAnnotation(pinAnnotation)
                            }
                            
                        })
                       
                    }
                    self.local_activityIndicator_stop()
                }
                else
                {
                    
                }
            }
        })
        }
        
        // new version dispatch Que
       /* dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), {
            
            
            self.responsemap = LibraryAPI.sharedInstance.PinCodeURl("\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/CustomerSearchPincodewise?Pincode=\(LibraryAPI.sharedInstance.Pincode)&Userid=\(LibraryAPI.sharedInstance.Userid)&RecordNo=20&PageNo=\("1")")
            
            dispatch_async(dispatch_get_main_queue(), {
                
               
                if(self.responsemap.count > 0)
                {
                    for validaddress in self.responsemap
                    {
                        let comapanyname = validaddress["CUSTOMER NAME"] as! String
                        let add1 = validaddress["CUSTOMER ADD1"] as! String
                        let add2 = validaddress["CUSTOMER ADD2"] as! String
                        let add3 = validaddress["CUSTOMER ADD3"] as! String
                        let add4 = validaddress["CUSTOMER ADD4"] as! String
                        let add5 = validaddress["CUSTOMER ADD5"] as! String
                        let address = comapanyname + " , " + add1 + " , " + add2 + " , " + add3 + " , " + add4 + " , " + add5
                        
                        let geocoder: CLGeocoder = CLGeocoder()
                        geocoder.geocodeAddressString(address,completionHandler: {(placemarks: [CLPlacemark]?, error: NSError?) -> Void in
                            if (placemarks?.count > 0) {
                                let topResult: CLPlacemark = (placemarks?[0])!
                                let placemark: MKPlacemark = MKPlacemark(placemark: topResult)
                                var region: MKCoordinateRegion = self.mapview.region
                                
                                region.center.latitude = (placemark.location?.coordinate.latitude)!
                                region.center.longitude = (placemark.location?.coordinate.longitude)!
                                
                                region.span = MKCoordinateSpanMake(0.5, 0.5)
                                
                                self.mapview.setRegion(region, animated: true)
                                self.mapview.addAnnotation(placemark)
                            }
                        })
                    }
                }
                else
                {
                    
                }
                
                
            })
        })*/
        
        
        
    }
    
    
    func notificationmethod()
    {
        let userdefaults = Foundation.UserDefaults.standard
        let value = userdefaults.dictionary(forKey: "Notification")
        
        var title: String!
        var message: String!
        var imagestr: String!
        
        title = value?["title"] as! String
        message = value?["body"] as! String
        imagestr = value?["imageurl"] as! String
        
        if imagestr != nil
        {
            if imagestr != ""
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: notificationimageViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = notificationimageViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                    
                }
                popup.show(childViewController: container)
            }
            else
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: PushnotificationtextViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = PushnotificationtextViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                }
                
                popup.show(childViewController: container)
            }
        }
        else
        {
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: PushnotificationtextViewController.instance())
                
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
            }
            let container = PushnotificationtextViewController.instance()
            container.closeHandler = { _ in
                popup.dismiss()
            }
            
            popup.show(childViewController: container)
        }
        
        
        
    }
    
}
    


