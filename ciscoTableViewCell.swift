//
//  ciscoTableViewCell.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 26/06/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class ciscoTableViewCell: UITableViewCell
{

    @IBOutlet weak var referenceno: UILabel!
    @IBOutlet weak var eopno: UILabel!
    
    @IBOutlet weak var quantityremaining: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
