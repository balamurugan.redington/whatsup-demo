//
//  DBCViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 27/05/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import KCFloatingActionButton

class DBCViewController: UIViewController,KCFloatingActionButtonDelegate,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate
{
    
    @IBOutlet weak var Scrollview: UIScrollView!
    @IBOutlet weak var mainview: UIView!
    @IBOutlet weak var yesbutton: UIButton!
    @IBOutlet weak var nobutton: UIButton!
    @IBOutlet weak var endcuspono: UILabel!
    @IBOutlet weak var endcusnotxtfield: UITextField!
    @IBOutlet weak var approvalbtn: UIButton!
    @IBOutlet weak var tableviewview: UIView!
    @IBOutlet weak var samelblbzcode: UILabel!
    @IBOutlet weak var exceedlbl: UILabel!
    @IBOutlet weak var customercodetf: UITextField!
    @IBOutlet weak var dealstarttf: UITextField!
    @IBOutlet weak var creditdaystf: UITextField!
    @IBOutlet weak var creditamttf: UITextField!
    @IBOutlet weak var paymenttf: UITextField!
    @IBOutlet weak var businesscodetf: UITextField!
    @IBOutlet weak var endcustomernametf: UITextField!
    @IBOutlet weak var dealenddatelabel: UILabel!
    @IBOutlet weak var datepicker11: UIDatePicker!
    @IBOutlet weak var DatepickerView: UIView!
    @IBOutlet weak var dealclosuredatetf: UITextField!
    @IBOutlet weak var businesstableview: UITableView!
    @IBOutlet weak var Topview: UIView!
    //  let datePicker1: UIDatePicker = UIDatePicker()
    @IBOutlet weak var Bottomview: UIView!
    
    var fab=KCFloatingActionButton()
    var CustomcodeCheck : String!
    var message: String!
    var referencenumber: String!
    var errormsg: String!
    var checkchar : String!
    var CheckInt : Int!
    var uploadfile=NSMutableDictionary()
    var jsonresult=NSDictionary()
    var  responsestring = NSMutableArray()
    var responsearray1=NSMutableArray()
    var pickerarray1=NSMutableArray()
    var responsearray=NSMutableArray()
    var pickerarray=NSMutableArray()
    var currenttf = String()
    var tableviewresponsearray : NSMutableArray = []
    var Movedup = Bool()
    var Arr : [String] = []
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationmethod), name: NSNotification.Name(rawValue: "GetNotification"), object: nil)
        endcustomernametf.keyboardType = .default
        endcusnotxtfield.keyboardType = .numberPad
       
        
        endcuspono.isHidden = true
        endcusnotxtfield.isHidden = true
        
        // self.Scrollview.contentSize = mainview.frame.size
        self.customnavigation()
        bizcodeRec1()
        bizcodeRec()
        Movedup = false
        tableviewview.isHidden = true
        
        samelblbzcode.isHidden = true
        exceedlbl.isHidden = true
        
//        tableviewview.layer.masksToBounds=true
//        tableviewview.layer.cornerRadius=10.0
//        self.tableviewview.layer.borderWidth = 0.5
        
        Scrollview.bounces = false
        
        approvalbtn.layer.masksToBounds=true
        approvalbtn.layer.cornerRadius=10.0
        
        self.layoutFAB()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        //delegates
        customercodetf.delegate = self
        dealstarttf.delegate = self
        dealclosuredatetf.delegate = self
        creditdaystf.delegate = self
        creditamttf.delegate = self
        paymenttf.delegate = self
        businesscodetf.delegate = self
        endcustomernametf.delegate = self
        endcusnotxtfield.delegate = self
        businesstableview.delegate = self
        businesstableview.dataSource = self
        businesstableview.bounces = false
        //tags
        customercodetf.tag = 1
        dealstarttf.tag = 2
        dealclosuredatetf.tag = 3
        creditdaystf.tag = 4
        creditamttf.tag = 5
        paymenttf.tag = 6
        businesscodetf.tag = 7
        endcustomernametf.tag = 8
        endcusnotxtfield.tag = 9
        
        checkchar = "N"
        
        dealstarttf.isUserInteractionEnabled = true
        dealclosuredatetf.keyboardType = .numberPad
        creditdaystf.keyboardType = .numberPad
        creditamttf.keyboardType = .decimalPad
        Topview.layer.borderColor = UIColor(red:30/255.0, green:184/255.0, blue:120/255.0, alpha: 1.0).cgColor
        Topview.layer.borderWidth = 0.5
        Topview.layer.cornerRadius = 5.0
        Bottomview.layer.borderColor = UIColor(red:30/255.0, green:184/255.0, blue:120/255.0, alpha: 1.0).cgColor
        Bottomview.layer.borderWidth = 0.5
        Bottomview.layer.cornerRadius = 5.0
        self.addDoneButtonOnKeyboard()
        businesstableview.register(UINib(nibName: "BillingwithodCustomerCell", bundle: nil), forCellReuseIdentifier: "BillingwithodCustomerCell")
        businesstableview.tableFooterView = UIView(frame: .zero)
        NotificationCenter.default.addObserver(self, selector: #selector(DBCViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(DBCViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
         creditamttf.addTarget(self, action: #selector(DBCViewController.textViewDidChange), for: UIControlEvents.editingChanged)
        
        
    }
    func textViewDidChange(_ textView: UITextView) {

        if(creditamttf.text?.first == "0")
        {
            creditamttf.text = ""
        }
        else if(creditamttf.text == "0.0")
        {
            creditamttf.text = "0."
        }
        else if(creditamttf.text == ".")
        {
            creditamttf.text = "0."
        }
        else
        {
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        Scrollview.isScrollEnabled=true
        Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:700)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
//        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        appdelegate.shouldSupportAllOrientation = false
        yesbutton.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        nobutton.setImage(UIImage(named:"green2.png"),  for: UIControlState.normal)
        endcuspono.isHidden = true
        endcusnotxtfield.isHidden = true
        
        var todaysDate:NSDate = NSDate()
        var dateFormatter:DateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        self.dealstarttf.text = dateFormatter.string(from: NSDate() as Date)
        
        
        
        if CustomcodeCheck == nil || CustomcodeCheck == ""
        {
            self.customercodetf.text = ""
        }
        else
        {
            self.customercodetf.text = LibraryAPI.sharedInstance.CustomerCode
        }
        
        if referencenumber != nil
        {
            if referencenumber != ""
            {
                showToast(message: referencenumber)
            }
            else if message != ""
            {
                showToast(message: message)
            }
            else if errormsg != ""
            {
                showToast(message: errormsg)
            }
            else
            {
                
            }
        }
        else
        {
            
        }
        
        
    }
    
    func customnavigation()
        
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        
        //    customView.backbutton.hidden=false
        //    customView.Notificationbtn.hidden=true
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        //  customView.Notificationbtn.addTarget(self, action: #selector(SSASideMenu.presentRightMenuViewController),forControlEvents:UIControlEvents.TouchUpInside)
        customView.MenuBtn.addTarget(self, action: #selector(DBCViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Deal Base Credit maintaince"
        
    }
    
    func buttonClicked(sender:UIButton)
    {
        // self.navigationController?.popViewControllerAnimated(true)
       
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "PreorderMainViewController")), animated: true)
    }
    
    
    @IBAction func yesbtn(_ sender: AnyObject)
    {
        checkchar = "Y"
        
        yesbutton.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
        nobutton.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        endcuspono.isHidden = false
        endcusnotxtfield.isHidden = false
        
    }
    
    @IBAction func nobtnAction(_ sender: AnyObject)
    {
        checkchar = "N"
        
        yesbutton.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        nobutton.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
        endcuspono.isHidden = true
        endcusnotxtfield.isHidden = true
    }
    
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            
            
            
            self.fab.close()
        }
        
        fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
            
            let slideInFromLeftTransition = CATransition()
            slideInFromLeftTransition.type = kCAGravityTop
            slideInFromLeftTransition.subtype = kCATransitionFromLeft
            slideInFromLeftTransition.duration = 0.2
            slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DBCViewController")), animated: true)
            self.fab.close()
        }
        
        
        fab.fabDelegate = self
        
        self.view.addSubview(fab)
        
    }
    func KCFABOpened(fab: KCFloatingActionButton) {
        
    }
    
    
    
    func keyboardWillShow(notification: NSNotification)
    {
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true)
        {
        }
        else
        {
            if(Movedup==false)
            {
                self.view.frame.origin.y -= 150
                Movedup=true
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true)
        {
        }
        else
        {
            self.view.frame.origin.y = 0
            Movedup=false
        }
    }
    
    
    //textfield Delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        //fab.hidden=false
        textField.resignFirstResponder()
        if (textField.tag == 9)
        {
            endcustomernametf.resignFirstResponder()
            return true
        }
        
        return true
    }
    
    func addDoneButtonOnKeyboard()
        
    {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0,y: 0,width: 320,height: 50))
        
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        
        
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(DBCViewController.doneButtonAction))
        
        let items = NSMutableArray()
        
        items.add(flexSpace)
        
        items.add(done)
        
        doneToolbar.items = items as AnyObject as? [UIBarButtonItem]
        
        doneToolbar.sizeToFit()
        
        dealclosuredatetf.inputAccessoryView = doneToolbar
        creditamttf.inputAccessoryView = doneToolbar
        creditdaystf.inputAccessoryView = doneToolbar
        endcusnotxtfield.inputAccessoryView = doneToolbar
    }
    func doneButtonAction()
    {
        if CheckInt == 3
        {
            if (dealclosuredatetf.text != "")
            {
                
                
                let dateComponents = NSDateComponents()
                
                
                dateComponents.day = Int(dealclosuredatetf.text!)!
                let currentCalendar = NSCalendar.current
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let date1 = dateFormatter.date(from: dealstarttf.text!)
                //let oneMonthBack = currentCalendar.dateByAddingComponents(dateComponents, toDate: date1!, options: [])!
               
                let oneMonthBack = currentCalendar.date(byAdding: dateComponents as DateComponents, to: date1!)
                
                dateFormatter.dateStyle = DateFormatter.Style.long
                dateFormatter.dateFormat = "yyyy-MM-dd"
                let strDate = dateFormatter.string(from: oneMonthBack!)
                
                
                dealenddatelabel.text = String(strDate)
                
                
                switch self.tableviewresponsearray.count
                {
                case 0:
                    
                    self.uploadfile.setObject("", forKey: "product1" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product2" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product3" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product4" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product5" as NSCopying)
                    
                    break
                    
                case 1:
                    
                    let fullName : String = self.tableviewresponsearray.object(at: 0) as! String
                    //let fullNameArr : [String] = fullName.componentsSeparatedByString(" - ")
                    let fullNameArr : [String] = fullName.components(separatedBy: " - ")
                    let firstName : String = fullNameArr[1]
                    
                    
                    
                    self.uploadfile.setObject(firstName, forKey: "product1" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product2" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product3" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product4" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product5" as NSCopying)
                    
                    break
                    
                case 2:
                    
                    let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                    let firstName1 : String = fullNameArr1[1]
                    
                    let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                    let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                    let firstName2 : String = fullNameArr2[1]
                    
                    uploadfile.setObject(firstName1, forKey: "product1"  as NSCopying)
                    uploadfile.setObject(firstName2, forKey: "product2"  as NSCopying)
                    uploadfile.setObject("", forKey: "product3"  as NSCopying)
                    uploadfile.setObject("", forKey: "product4"  as NSCopying)
                    uploadfile.setObject("", forKey: "product5"  as NSCopying)
                    break
                    
                case 3:
                    
                    let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                    let firstName1 : String = fullNameArr1[1]
                    
                    let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                    let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                    let firstName2 : String = fullNameArr2[1]
                    
                    let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                    let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                    let firstName3 : String = fullNameArr3[1]
                    
                    uploadfile.setObject(firstName1, forKey: "product1"  as NSCopying)
                    uploadfile.setObject(firstName2, forKey: "product2"  as NSCopying)
                    uploadfile.setObject(firstName3, forKey: "product3"  as NSCopying)
                    uploadfile.setObject("", forKey: "product4"  as NSCopying)
                    uploadfile.setObject("", forKey: "product5"  as NSCopying)
                    break
                    
                case 4:
                    
                    let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                    let firstName1 : String = fullNameArr1[1]
                    
                    let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                    let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                    let firstName2 : String = fullNameArr2[1]
                    
                    let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                    let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                    let firstName3 : String = fullNameArr3[1]
                    
                    let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
                    let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
                    let firstName4 : String = fullNameArr4[1]
                    
                    uploadfile.setObject(firstName1, forKey: "product1" as NSCopying)
                    uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
                    uploadfile.setObject(firstName3, forKey: "product3" as NSCopying)
                    uploadfile.setObject(firstName4, forKey: "product4" as NSCopying)
                    uploadfile.setObject("", forKey: "product5" as NSCopying)
                    break
                    
                case 5:
                    
                    let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                    let firstName1 : String = fullNameArr1[1]
                    
                    let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                    let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                    let firstName2 : String = fullNameArr2[1]
                    
                    let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                    let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                    let firstName3 : String = fullNameArr3[1]
                    
                    let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
                    let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
                    let firstName4 : String = fullNameArr4[1]
                    
                    let fullName5 : String = self.tableviewresponsearray.object(at: 4) as! String
                    let fullNameArr5 : [String] = fullName5.components(separatedBy: " - ")
                    let firstName5 : String = fullNameArr5[1]
                    
                    uploadfile.setObject(firstName1, forKey: "product1" as NSCopying)
                    uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
                    uploadfile.setObject(firstName3, forKey: "product3" as NSCopying)
                    uploadfile.setObject(firstName4, forKey: "product4" as NSCopying)
                    uploadfile.setObject(firstName5, forKey: "product5" as NSCopying)
                    break
                    
                default:
                    break
                }
                
                
                self.uploadfile.setObject(customercodetf.text!, forKey: "Customercode" as NSCopying)
                self.uploadfile.setObject(LibraryAPI.sharedInstance.Userid, forKey: "UserId"  as NSCopying)
                self.uploadfile.setObject(dealstarttf.text!, forKey: "DealStartDate" as NSCopying)
                self.uploadfile.setObject(dealclosuredatetf.text!, forKey: "DateofClosure" as NSCopying)
                self.uploadfile.setObject(creditdaystf.text!, forKey: "CreditDays" as NSCopying)
                self.uploadfile.setObject(creditamttf.text!, forKey: "CreditAmount" as NSCopying)
                
                let payment : [String] = paymenttf.text!.components(separatedBy: " - ")
                let paymentstr : String = payment[0]
                self.uploadfile.setObject(paymentstr, forKey: "PaymentMethod" as NSCopying)
                
                self.uploadfile.setObject(endcustomernametf.text!, forKey: "EndCustomerName" as NSCopying)
                self.uploadfile.setObject(checkchar, forKey: "EndCustomerPOReq" as NSCopying)
                self.uploadfile.setObject(endcusnotxtfield.text!, forKey: "EndCustomerPONumber" as NSCopying)
                self.uploadfile.setObject("", forKey: "Submit" as NSCopying)
                
                self.uploaddata()
                if self.jsonresult.count > 0
                {
                    dealclosuredatetf.resignFirstResponder()
                    
                    let errormessage = self.jsonresult.object(forKey: "ErrorMessage")
                    let message      = self.jsonresult.object(forKey: "Message")
                    let Refnumber    = self.jsonresult.object(forKey: "ReferenceNumber")
                    
                    
                    if (Refnumber as? String) != ""
                    {
                        showToast(message: Refnumber as! String)
                    }
                    else if (message as? String) != ""
                    {
                        showToast(message: message as! String)
                    }
                    else if(errormessage as? String) != ""
                    {
                        showToast(message: errormessage as! String)
                    }
                    else
                    {
                        
                    }
                    
                }
                
            }
            else
                
            {
                dealclosuredatetf.resignFirstResponder()
            }
        }
            
            
        else if CheckInt == 4
        {
            if (creditdaystf.text != nil)
            {
                creditdaystf.resignFirstResponder()
                
                switch self.tableviewresponsearray.count
                {
                case 0:
                    
                    self.uploadfile.setObject("", forKey: "product1" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product2" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product3" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product4" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product5" as NSCopying)
                    
                    break
                    
                case 1:
                    
                    let fullName : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr : [String] = fullName.components(separatedBy: " - ")
                    let firstName : String = fullNameArr[1]
                    
                    
                    
                    self.uploadfile.setObject(firstName, forKey: "product1"  as NSCopying)
                    self.uploadfile.setObject("", forKey: "product2" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product3" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product4" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product5" as NSCopying)
                    
                    break
                    
                case 2:
                    
                    let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                    let firstName1 : String = fullNameArr1[1]
                    
                    let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                    let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                    let firstName2 : String = fullNameArr2[1]
                    
                    uploadfile.setObject(firstName1, forKey: "product1" as NSCopying)
                    uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
                    uploadfile.setObject("", forKey: "product3" as NSCopying)
                    uploadfile.setObject("", forKey: "product4" as NSCopying)
                    uploadfile.setObject("", forKey: "product5" as NSCopying)
                    break
                    
                case 3:
                    
                    let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                    let firstName1 : String = fullNameArr1[1]
                    
                    let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                    let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                    let firstName2 : String = fullNameArr2[1]
                    
                    let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                    let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                    let firstName3 : String = fullNameArr3[1]
                    
                    uploadfile.setObject(firstName1, forKey: "product1" as NSCopying)
                    uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
                    uploadfile.setObject(firstName3, forKey: "product3" as NSCopying)
                    uploadfile.setObject("", forKey: "product4" as NSCopying)
                    uploadfile.setObject("", forKey: "product5" as NSCopying)
                    break
                    
                case 4:
                    
                    let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                    let firstName1 : String = fullNameArr1[1]
                    
                    let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                    let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                    let firstName2 : String = fullNameArr2[1]
                    
                    let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                    let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                    let firstName3 : String = fullNameArr3[1]
                    
                    let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
                    let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
                    let firstName4 : String = fullNameArr4[1]
                    
                    uploadfile.setObject(firstName1, forKey: "product1" as NSCopying)
                    uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
                    uploadfile.setObject(firstName3, forKey: "product3" as NSCopying)
                    uploadfile.setObject(firstName4, forKey: "product4" as NSCopying)
                    uploadfile.setObject("", forKey: "product5" as NSCopying)
                    break
                    
                case 5:
                    
                    let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                    let firstName1 : String = fullNameArr1[1]
                    
                    let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                    let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                    let firstName2 : String = fullNameArr2[1]
                    
                    let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                    let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                    let firstName3 : String = fullNameArr3[1]
                    
                    let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
                    let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
                    let firstName4 : String = fullNameArr4[1]
                    
                    let fullName5 : String = self.tableviewresponsearray.object(at: 4) as! String
                    let fullNameArr5 : [String] = fullName5.components(separatedBy: " - ")
                    let firstName5 : String = fullNameArr5[1]
                    
                    uploadfile.setObject(firstName1, forKey: "product1"  as NSCopying)
                    uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
                    uploadfile.setObject(firstName3, forKey: "product3" as NSCopying)
                    uploadfile.setObject(firstName4, forKey: "product4" as NSCopying)
                    uploadfile.setObject(firstName5, forKey: "product5" as NSCopying)
                    break
                    
                default:
                    break
                }
                
                
                self.uploadfile.setObject(customercodetf.text!, forKey: "Customercode" as NSCopying)
                self.uploadfile.setObject(LibraryAPI.sharedInstance.Userid, forKey: "UserId" as NSCopying)
                self.uploadfile.setObject(dealstarttf.text!, forKey: "DealStartDate" as NSCopying)
                self.uploadfile.setObject(dealclosuredatetf.text!, forKey: "DateofClosure" as NSCopying)
                self.uploadfile.setObject(creditdaystf.text!, forKey: "CreditDays" as NSCopying)
                self.uploadfile.setObject(creditamttf.text!, forKey: "CreditAmount" as NSCopying)
                
                let payment : [String] = paymenttf.text!.components(separatedBy: " - ")
                let paymentstr : String = payment[0]
                self.uploadfile.setObject(paymentstr, forKey: "PaymentMethod" as NSCopying)
                
                
                self.uploadfile.setObject(endcustomernametf.text!, forKey: "EndCustomerName" as NSCopying)
                self.uploadfile.setObject(checkchar, forKey: "EndCustomerPOReq" as NSCopying)
                self.uploadfile.setObject(endcusnotxtfield.text!, forKey: "EndCustomerPONumber" as NSCopying)
                self.uploadfile.setObject("", forKey: "Submit" as NSCopying)
                
                self.uploaddata()
                if self.jsonresult.count > 0
                {
                    dealclosuredatetf.resignFirstResponder()
                   
                    
                    let errormessage = self.jsonresult.object(forKey: "ErrorMessage")
                    let message      = self.jsonresult.object(forKey: "Message")
                    let Refnumber    = self.jsonresult.object(forKey: "ReferenceNumber")
                    
                    
                    if (Refnumber as? String) != ""
                    {
                        showToast(message: Refnumber as! String)
                    }
                    else if (message as? String) != ""
                    {
                        showToast(message: message as! String)
                    }
                    else if(errormessage as? String) != ""
                    {
                        showToast(message: errormessage as! String)
                    }
                    else
                    {
                        
                    }
                    
                }
                
            }
            
            // creditdaystf.resignFirstResponder()
        }
            
        else if CheckInt == 5
        {
            if (creditamttf.text != nil)
            {
                
                creditamttf.resignFirstResponder()
                
                switch self.tableviewresponsearray.count
                {
                case 0:
                    
                    self.uploadfile.setObject("", forKey: "product1" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product2" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product3" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product4" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product5" as NSCopying)
                    
                    break
                    
                case 1:
                    
                    let fullName : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr : [String] = fullName.components(separatedBy: " - ")
                    let firstName : String = fullNameArr[1]
                    
                    
                    
                    self.uploadfile.setObject(firstName, forKey: "product1" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product2" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product3" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product4" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product5" as NSCopying)
                    
                    break
                    
                case 2:
                    
                    let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                    let firstName1 : String = fullNameArr1[1]
                    
                    let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                    let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                    let firstName2 : String = fullNameArr2[1]
                    
                    uploadfile.setObject(firstName1, forKey: "product1" as NSCopying)
                    uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
                    uploadfile.setObject("", forKey: "product3" as NSCopying)
                    uploadfile.setObject("", forKey: "product4" as NSCopying)
                    uploadfile.setObject("", forKey: "product5" as NSCopying)
                    break
                    
                case 3:
                    
                    let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                    let firstName1 : String = fullNameArr1[1]
                    
                    let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                    let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                    let firstName2 : String = fullNameArr2[1]
                    
                    let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                    let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                    let firstName3 : String = fullNameArr3[1]
                    
                    uploadfile.setObject(firstName1, forKey: "product1" as NSCopying)
                    uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
                    uploadfile.setObject(firstName3, forKey: "product3" as NSCopying)
                    uploadfile.setObject("", forKey: "product4" as NSCopying)
                    uploadfile.setObject("", forKey: "product5" as NSCopying)
                    break
                    
                case 4:
                    
                    let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                    let firstName1 : String = fullNameArr1[1]
                    
                    let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                    let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                    let firstName2 : String = fullNameArr2[1]
                    
                    let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                    let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                    let firstName3 : String = fullNameArr3[1]
                    
                    let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
                    let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
                    let firstName4 : String = fullNameArr4[1]
                    
                    uploadfile.setObject(firstName1, forKey: "product1" as NSCopying)
                    uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
                    uploadfile.setObject(firstName3, forKey: "product3" as NSCopying)
                    uploadfile.setObject(firstName4, forKey: "product4" as NSCopying)
                    uploadfile.setObject("", forKey: "product5" as NSCopying)
                    break
                    
                case 5:
                    
                    let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                    let firstName1 : String = fullNameArr1[1]
                    
                    let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                    let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                    let firstName2 : String = fullNameArr2[1]
                    
                    let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                    let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                    let firstName3 : String = fullNameArr3[1]
                    
                    let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
                    let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
                    let firstName4 : String = fullNameArr4[1]
                    
                    let fullName5 : String = self.tableviewresponsearray.object(at: 4) as! String
                    let fullNameArr5 : [String] = fullName5.components(separatedBy: " - ")
                    let firstName5 : String = fullNameArr5[1]
                    
                    uploadfile.setObject(firstName1, forKey: "product1" as NSCopying)
                    uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
                    uploadfile.setObject(firstName3, forKey: "product3" as NSCopying)
                    uploadfile.setObject(firstName4, forKey: "product4" as NSCopying)
                    uploadfile.setObject(firstName5, forKey: "product5" as NSCopying)
                    break
                    
                default:
                    break
                }
                
                
                self.uploadfile.setObject(customercodetf.text!, forKey: "Customercode" as NSCopying)
                self.uploadfile.setObject(LibraryAPI.sharedInstance.Userid, forKey: "UserId" as NSCopying)
                self.uploadfile.setObject(dealstarttf.text!, forKey: "DealStartDate" as NSCopying)
                self.uploadfile.setObject(dealclosuredatetf.text!, forKey: "DateofClosure" as NSCopying)
                self.uploadfile.setObject(creditdaystf.text!, forKey: "CreditDays" as NSCopying)
                self.uploadfile.setObject(creditamttf.text!, forKey: "CreditAmount" as NSCopying)
                
                let payment : [String] = paymenttf.text!.components(separatedBy: " - ")
                let paymentstr : String = payment[0]
                self.uploadfile.setObject(paymentstr, forKey: "PaymentMethod" as NSCopying)
                
                
                self.uploadfile.setObject(endcustomernametf.text!, forKey: "EndCustomerName" as NSCopying)
                self.uploadfile.setObject(checkchar, forKey: "EndCustomerPOReq" as NSCopying)
                self.uploadfile.setObject(endcusnotxtfield.text!, forKey: "EndCustomerPONumber" as NSCopying)
                self.uploadfile.setObject("", forKey: "Submit" as NSCopying)
                
                self.uploaddata()
                if self.jsonresult.count > 0
                {
                    dealclosuredatetf.resignFirstResponder()
                    
                    let errormessage = self.jsonresult.object(forKey: "ErrorMessage")
                    let message      = self.jsonresult.object(forKey: "Message")
                    let Refnumber    = self.jsonresult.object(forKey: "ReferenceNumber")
                    
                    
                    if (Refnumber as? String) != ""
                    {
                        showToast(message: Refnumber as! String)
                    }
                    else if (message as? String) != ""
                    {
                        showToast(message: message as! String)
                    }
                    else if(errormessage as? String) != ""
                    {
                        showToast(message: errormessage as! String)
                    }
                    else
                    {
                        
                    }
                    
                }
                
            }
        }
        else if(CheckInt == 8)
        {
            endcusnotxtfield.resignFirstResponder()
        }
        
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

//        creditdaystf.tag = 4
//        creditamttf.tag = 5
        if(textField.tag==3)
        {
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 3
        }
        if(textField.tag == 4)
        {
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 3
        }

        
        if (textField.tag == 5)
        {
            let oldString = textField.text?.count == 0 ? string  : textField.text!
            let i = oldString.count
            let range = oldString.index(oldString.startIndex, offsetBy: i-1) ..< oldString.index(oldString.startIndex, offsetBy: i)
            let candidate = oldString.replacingCharacters(in: range, with: string)
            let regex = try? NSRegularExpression(pattern: "^\\d{0,8}(\\.\\d{0,2})?$", options: [])
            return regex?.firstMatch(in: candidate, options: [], range: NSRange(location: 0, length: candidate.count)) != nil
        }
        
        if(textField.tag == 8)
        {
            
         
                if let value = string.rangeOfCharacter(from: .letters){
                guard let text = textField.text else { return true }
                let newLength = text.count + string.count - range.length
                return newLength  <= 25
            }
            else
            {
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                   
                    return true
                }
                
                return false
                
            }
            
        }
        
        if(textField.tag == 9)
        {
            guard let text = textField.text else { return true }
            let newlength = text.count + string.count - range.length
            
            return newlength <= 15
        }
        return true
    }
    @IBAction func DoneBtnAction(_ sender: AnyObject)
    {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "yyyy-MM-dd"
        //"MM-dd-yy"
        let strDate = dateFormatter.string(from: datepicker11.date)
        
        dealstarttf.text=strDate
        // datepicker11.hidden = true
        DatepickerView.isHidden=true
        
        
    }
    
    @IBAction func CancelBtnAction(_ sender: UIButton)
    {
        DatepickerView.isHidden=true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {

        if (textField.tag == 1)
        {
            // spcnumber.text = ""
            
            LibraryAPI.sharedInstance.Currentcustomerlookup="DBC"
            if #available(iOS 10.0, *)
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: CustomerLookupViewController.instance())
                    
                    .didShowHandler
                    { popup in
                        self.customercodetf.resignFirstResponder()
                    }
                    .didCloseHandler
                    { _ in
                }
                
                let container = CustomerLookupViewController.instance()
                container.closeHandler =
                    { _ in
                        popup.dismiss()
                }
                
                popup.show(childViewController: container)
            }
            return false
        }
            
        else if (textField.tag == 2)
            
        {
            CheckInt = 2
            // dealstarttf.resignFirstResponder()
            datepicker11.date = NSDate() as Date
            DatepickerView.isHidden=false
            datepicker11.isHidden = false
            datepicker11.datePickerMode = UIDatePickerMode.date
            return false
        }
            
        else if(textField.tag == 3)
        {
            CheckInt = 3
            //orderamttxtfield.becomeFirstResponder()
            return true
        }
        else if (textField.tag == 9)
        {
            CheckInt = 8
            return true
        }
            
            
            
        else if (textField.tag == 4)
        {
            CheckInt = 4
            //orderamttxtfield.becomeFirstResponder()
            return true
        }
        else if (textField.tag == 5)
        {
            CheckInt = 5
            //orderamttxtfield.becomeFirstResponder()
            return true
        }
            
        else if textField.tag == 6
        {
             //let size = CGSize(width: 50, height:50)
            self.local_activityIndicator_start()
            
            let alert:UIAlertController=UIAlertController(title: "Select Payment", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            
            var alert1 = UIAlertAction()
            
            if pickerarray1.count != 0
            {
                self.local_activityIndicator_stop()
                for word in pickerarray1
                {
                    alert1 = UIAlertAction(title: (word as! String), style: UIAlertActionStyle.default)
                    {
                        UIAlertAction in
                        
                        let getString : String = word as! String
                        
                        self.paymenttf.text = getString
                        
                        self.paymenttf.resignFirstResponder()
                        
                        switch self.tableviewresponsearray.count
                        {
                        case 0:
                            
                            self.uploadfile.setObject("", forKey: "product1" as NSCopying)
                            self.uploadfile.setObject("", forKey: "product2" as NSCopying)
                            self.uploadfile.setObject("", forKey: "product3" as NSCopying)
                            self.uploadfile.setObject("", forKey: "product4" as NSCopying)
                            self.uploadfile.setObject("", forKey: "product5" as NSCopying)
                            
                            break
                            
                        case 1:
                            
                            let fullName : String = self.tableviewresponsearray.object(at: 0) as! String
                           // let fullNameArr : [String] = fullName.componentsSeparatedByString(" - ")
                            let fullNameArr : [String] = fullName.components(separatedBy: " - ")
                            let firstName : String = fullNameArr[1]
                            
                            
                            
                            self.uploadfile.setObject(firstName, forKey: "product1" as NSCopying)
                            self.uploadfile.setObject("", forKey: "product2" as NSCopying)
                            self.uploadfile.setObject("", forKey: "product3" as NSCopying)
                            self.uploadfile.setObject("", forKey: "product4" as NSCopying)
                            self.uploadfile.setObject("", forKey: "product5" as NSCopying)
                            
                            break
                            
                        case 2:
                            
                            let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                            let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                            let firstName1 : String = fullNameArr1[1]
                            
                            let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                            let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                            let firstName2 : String = fullNameArr2[1]
                            
                            self.uploadfile.setObject(firstName1, forKey: "product1" as NSCopying)
                            self.uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
                            self.uploadfile.setObject("", forKey: "product3" as NSCopying)
                            self.uploadfile.setObject("", forKey: "product4" as NSCopying)
                            self.uploadfile.setObject("", forKey: "product5" as NSCopying)
                            break
                            
                        case 3:
                            
                            let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                            let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                            let firstName1 : String = fullNameArr1[1]
                            
                            let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                            let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                            let firstName2 : String = fullNameArr2[1]
                            
                            let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                            let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                            let firstName3 : String = fullNameArr3[1]
                            
                            self.uploadfile.setObject(firstName1, forKey: "product1" as NSCopying)
                            self.uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
                            self.uploadfile.setObject(firstName3, forKey: "product3" as NSCopying)
                            self.uploadfile.setObject("", forKey: "product4" as NSCopying)
                            self.uploadfile.setObject("", forKey: "product5" as NSCopying)
                            break
                            
                        case 4:
                            
                            let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                            let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                            let firstName1 : String = fullNameArr1[1]
                            
                            let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                            let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                            let firstName2 : String = fullNameArr2[1]
                            
                            let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                            let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                            let firstName3 : String = fullNameArr3[1]
                            
                            let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
                            let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
                            let firstName4 : String = fullNameArr4[1]
                            
                            self.uploadfile.setObject(firstName1, forKey: "product1" as NSCopying)
                            self.uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
                            self.uploadfile.setObject(firstName3, forKey: "product3" as NSCopying)
                            self.uploadfile.setObject(firstName4, forKey: "product4" as NSCopying)
                            self.uploadfile.setObject("", forKey: "product5" as NSCopying)
                            break
                            
                        case 5:
                            
                            let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                            let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                            let firstName1 : String = fullNameArr1[1]
                            
                            let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                            let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                            let firstName2 : String = fullNameArr2[1]
                            
                            let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                            let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                            let firstName3 : String = fullNameArr3[1]
                            
                            let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
                            let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
                            let firstName4 : String = fullNameArr4[1]
                            
                            let fullName5 : String = self.tableviewresponsearray.object(at: 4) as! String
                            let fullNameArr5 : [String] = fullName5.components(separatedBy: " - ")
                            let firstName5 : String = fullNameArr5[1]
                            
                            self.uploadfile.setObject(firstName1, forKey: "product1" as NSCopying  as NSCopying)
                            self.uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
                            self.uploadfile.setObject(firstName3, forKey: "product3" as NSCopying)
                            self.uploadfile.setObject(firstName4, forKey: "product4" as NSCopying)
                            self.uploadfile.setObject(firstName5, forKey: "product5" as NSCopying)
                            break
                            
                        default:
                            break
                        }
                        
                        
                        self.uploadfile.setObject(self.customercodetf.text!, forKey: "Customercode" as NSCopying)
                        self.uploadfile.setObject(LibraryAPI.sharedInstance.Userid, forKey: "UserId" as NSCopying)
                        self.uploadfile.setObject(self.dealstarttf.text!, forKey: "DealStartDate" as NSCopying)
                        self.uploadfile.setObject(self.dealclosuredatetf.text!, forKey: "DateofClosure" as NSCopying)
                        self.uploadfile.setObject(self.creditdaystf.text!, forKey: "CreditDays" as NSCopying)
                        self.uploadfile.setObject(self.creditamttf.text!, forKey: "CreditAmount" as NSCopying)
                        
                        let payment : [String] = self.paymenttf.text!.components(separatedBy: " - ")
                        let paymentstr : String = payment[0]
                        self.uploadfile.setObject(paymentstr, forKey: "PaymentMethod" as NSCopying)
                        
                        
                        self.uploadfile.setObject(self.endcustomernametf.text!, forKey: "EndCustomerName" as NSCopying)
                        self.uploadfile.setObject(self.checkchar, forKey: "EndCustomerPOReq" as NSCopying)
                        self.uploadfile.setObject(self.endcusnotxtfield.text!, forKey: "EndCustomerPONumber" as NSCopying)
                        self.uploadfile.setObject("", forKey: "Submit" as NSCopying)
                        
                        self.uploaddata()
                        if self.jsonresult.count > 0
                        {
                            self.dealclosuredatetf.resignFirstResponder()
                            
                            let errormessage = self.jsonresult.object(forKey: "ErrorMessage")
                            let message      = self.jsonresult.object(forKey: "Message")
                            let Refnumber    = self.jsonresult.object(forKey: "ReferenceNumber")
                            
                            
                            if (Refnumber as? String) != ""
                            {
                                self.showToast(message: Refnumber as! String)
                            }
                            else if (message as? String) != ""
                            {
                                self.showToast(message: message as! String)
                            }
                            else if(errormessage as? String) != ""
                            {
                                self.showToast(message: errormessage as! String)
                            }
                            else
                            {
                                
                            }
                            
                        }
                    }
                    
                    alert.addAction(alert1)
                    
                }
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
            {
                UIAlertAction in
            }
            
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
            
            return false
            
        }
            
            
        else if (textField.tag == 7)
        {
            let size = CGSize(width: 50, height:50)
            self.local_activityIndicator_start()
            
            self.exceedlbl.text = ""
            self.samelblbzcode.text = ""
            //   self.samelblbzcode.hidden = true
            //  self.exceedlbl.hidden = true
            
            let alert:UIAlertController=UIAlertController(title: "Select Business Code", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            
            var alert1 = UIAlertAction()
            
            if pickerarray.count != 0
            {
               self.local_activityIndicator_stop()
                for word in pickerarray
                {
                    
                    alert1 = UIAlertAction(title: (word as! String), style: UIAlertActionStyle.default)
                    {
                        UIAlertAction in
                        //   self.uploaddata()
                        
                        let getString : String = word as! String
                        
                        if self.tableviewresponsearray.count < 5
                        {
                            if self.tableviewresponsearray.count == 0
                            {
                                self.tableviewresponsearray.add(getString)
                                self.Arr.append(getString)
                                self.businesstableview.reloadData()
                            }
                            else
                            {
                                let contains = self.tableviewresponsearray.contains
                                    {
                                        $0 as? String == getString
                                }
                                
                                if contains == false
                                {
                                    self.tableviewresponsearray.add(getString)
                                    self.Arr.append(getString)
                                    self.businesstableview.reloadData()
                                }
                                else
                                {
                                    self.samelblbzcode.isHidden = false
                                    self.exceedlbl.isHidden = true
                                    self.samelblbzcode.text = "You are Selected same Business Code"
                                }
                            }
                        }
                        else
                        {
                            self.exceedlbl.isHidden = false
                            self.samelblbzcode.isHidden = true
                            self.exceedlbl.text = "Alreay You have Selected 5 Business code"
                        }
                        
                        
                    }
                    
                    alert.addAction(alert1)
                    
                }
                
            }
            
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
            {
                UIAlertAction in
                
                
            }
            
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
            return false
        }
        return true
    }
    func datePickerValueChanged(sender: UIDatePicker){
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        
        // Set date format
        dateFormatter.dateFormat = "yyyy/MM/dd"
        // "MM/dd/yyyy"
        // Apply date format
        // let selectedDate: String = dateFormatter.string(from: sender.date)
        let selectedDate: String = dateFormatter.string(from: sender.date)
        
        dealstarttf.text = selectedDate
        
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {

        if textField == endcustomernametf
        {
            samelblbzcode.isHidden = true
            exceedlbl.isHidden = true
            switch self.tableviewresponsearray.count
            {
            case 0:
                
                self.uploadfile.setObject("", forKey: "product1" as NSCopying)
                self.uploadfile.setObject("", forKey: "product2" as NSCopying)
                self.uploadfile.setObject("", forKey: "product3" as NSCopying)
                self.uploadfile.setObject("", forKey: "product4" as NSCopying)
                self.uploadfile.setObject("", forKey: "product5" as NSCopying)
                
                break
                
            case 1:
                
                let fullName : String = self.tableviewresponsearray.object(at: 0) as! String
                let fullNameArr : [String] = fullName.components(separatedBy: " - ")
                let firstName : String = fullNameArr[1]
                
                
                
                self.uploadfile.setObject(firstName, forKey: "product1" as NSCopying)
                self.uploadfile.setObject("", forKey: "product2" as NSCopying)
                self.uploadfile.setObject("", forKey: "product3" as NSCopying)
                self.uploadfile.setObject("", forKey: "product4" as NSCopying)
                self.uploadfile.setObject("", forKey: "product5" as NSCopying)
                
                break
                
            case 2:
                
                let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                let firstName1 : String = fullNameArr1[1]
                
                let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                let firstName2 : String = fullNameArr2[1]
                
                self.uploadfile.setObject(firstName1, forKey: "product1" as NSCopying)
                self.uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
                self.uploadfile.setObject("", forKey: "product3" as NSCopying)
                self.uploadfile.setObject("", forKey: "product4" as NSCopying)
                self.uploadfile.setObject("", forKey: "product5" as NSCopying)
                break
                
            case 3:
                
                let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                let firstName1 : String = fullNameArr1[1]
                
                let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                let firstName2 : String = fullNameArr2[1]
                
                let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                let firstName3 : String = fullNameArr3[1]
                
                self.uploadfile.setObject(firstName1, forKey: "product1"  as NSCopying)
                self.uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
                self.uploadfile.setObject(firstName3, forKey: "product3" as NSCopying)
                self.uploadfile.setObject("", forKey: "product4" as NSCopying)
                self.uploadfile.setObject("", forKey: "product5" as NSCopying)
                break
                
            case 4:
                
                let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                let firstName1 : String = fullNameArr1[1]
                
                let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                let firstName2 : String = fullNameArr2[1]
                
                let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                let firstName3 : String = fullNameArr3[1]
                
                let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
                let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
                let firstName4 : String = fullNameArr4[1]
                
                self.uploadfile.setObject(firstName1, forKey: "product1" as NSCopying)
                self.uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
                self.uploadfile.setObject(firstName3, forKey: "product3" as NSCopying)
                self.uploadfile.setObject(firstName4, forKey: "product4" as NSCopying)
                self.uploadfile.setObject("", forKey: "product5" as NSCopying)
                break
                
            case 5:
                
                let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                let firstName1 : String = fullNameArr1[1]
                
                let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                let firstName2 : String = fullNameArr2[1]
                
                let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                let firstName3 : String = fullNameArr3[1]
                
                let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
                let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
                let firstName4 : String = fullNameArr4[1]
                
                let fullName5 : String = self.tableviewresponsearray.object(at: 4) as! String
                let fullNameArr5 : [String] = fullName5.components(separatedBy: " - ")
                let firstName5 : String = fullNameArr5[1]
                
                self.uploadfile.setObject(firstName1, forKey: "product1" as NSCopying)
                self.uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
                self.uploadfile.setObject(firstName3, forKey: "product3" as NSCopying)
                self.uploadfile.setObject(firstName4, forKey: "product4" as NSCopying)
                self.uploadfile.setObject(firstName5, forKey: "product5" as NSCopying)
                break
                
            default:
                break
            }
            
            self.uploadfile.setObject(self.customercodetf.text!, forKey: "Customercode" as NSCopying)
            self.uploadfile.setObject(LibraryAPI.sharedInstance.Userid, forKey: "UserId" as NSCopying)
            self.uploadfile.setObject(self.dealstarttf.text!, forKey: "DealStartDate" as NSCopying)
            self.uploadfile.setObject(self.dealclosuredatetf.text!, forKey: "DateofClosure" as NSCopying)
            self.uploadfile.setObject(self.creditdaystf.text!, forKey: "CreditDays" as NSCopying)
            self.uploadfile.setObject(self.creditamttf.text!, forKey: "CreditAmount" as NSCopying)
            
            
            let payment : [String] = paymenttf.text!.components(separatedBy: " - ")
            let paymentstr : String = payment[0]
            self.uploadfile.setObject(paymentstr, forKey: "PaymentMethod" as NSCopying)
            
            
            self.uploadfile.setObject(self.endcustomernametf.text!, forKey: "EndCustomerName" as NSCopying)
            self.uploadfile.setObject(self.checkchar, forKey: "EndCustomerPOReq" as NSCopying)
            self.uploadfile.setObject(self.endcusnotxtfield.text!, forKey: "EndCustomerPONumber" as NSCopying)
            self.uploadfile.setObject("", forKey: "Submit" as NSCopying)
            
            self.uploaddata()
            if self.jsonresult.count > 0
            {
                
               
                let errormessage = self.jsonresult.object(forKey: "ErrorMessage")
                let message      = self.jsonresult.object(forKey: "Message")
                let Refnumber    = self.jsonresult.object(forKey: "ReferenceNumber")
                
                
                if (Refnumber as? String) != ""
                {
                    self.showToast(message: Refnumber as! String)
                }
                else if (message as? String) != ""
                {
                    self.showToast(message: message as! String)
                }
                else if(errormessage as? String) != ""
                {
                    self.showToast(message: errormessage as! String)
                }
                else
                {
                    
                }
                
            }
            endcustomernametf.resignFirstResponder()
            return true
        }
        else if textField == endcusnotxtfield
        {
            switch self.tableviewresponsearray.count
            {
            case 0:
                
                self.uploadfile.setObject("", forKey: "product1"  as NSCopying)
                self.uploadfile.setObject("", forKey: "product2" as NSCopying)
                self.uploadfile.setObject("", forKey: "product3" as NSCopying)
                self.uploadfile.setObject("", forKey: "product4" as NSCopying)
                self.uploadfile.setObject("", forKey: "product5" as NSCopying)
                
                break
                
            case 1:
                
                let fullName : String = self.tableviewresponsearray.object(at: 0) as! String
                let fullNameArr : [String] = fullName.components(separatedBy: " - ")
                let firstName : String = fullNameArr[1]
                
                
                
                self.uploadfile.setObject(firstName, forKey: "product1" as NSCopying)
                self.uploadfile.setObject("", forKey: "product2" as NSCopying)
                self.uploadfile.setObject("", forKey: "product3" as NSCopying)
                self.uploadfile.setObject("", forKey: "product4" as NSCopying)
                self.uploadfile.setObject("", forKey: "product5" as NSCopying)
                
                break
                
            case 2:
                
                let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                let firstName1 : String = fullNameArr1[1]
                
                let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                let firstName2 : String = fullNameArr2[1]
                
                self.uploadfile.setObject(firstName1, forKey: "product1" as NSCopying)
                self.uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
                self.uploadfile.setObject("", forKey: "product3" as NSCopying)
                self.uploadfile.setObject("", forKey: "product4" as NSCopying)
                self.uploadfile.setObject("", forKey: "product5" as NSCopying)
                break
                
            case 3:
                
                let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                let firstName1 : String = fullNameArr1[1]
                
                let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                let firstName2 : String = fullNameArr2[1]
                
                let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                let firstName3 : String = fullNameArr3[1]
                
                self.uploadfile.setObject(firstName1, forKey: "product1" as NSCopying)
                self.uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
                self.uploadfile.setObject(firstName3, forKey: "product3" as NSCopying)
                self.uploadfile.setObject("", forKey: "product4" as NSCopying)
                self.uploadfile.setObject("", forKey: "product5" as NSCopying)
                break
                
            case 4:
                
                let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                let firstName1 : String = fullNameArr1[1]
                
                let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                let firstName2 : String = fullNameArr2[1]
                
                let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                let firstName3 : String = fullNameArr3[1]
                
                let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
                let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
                let firstName4 : String = fullNameArr4[1]
                
                self.uploadfile.setObject(firstName1, forKey: "product1" as NSCopying)
                self.uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
                self.uploadfile.setObject(firstName3, forKey: "product3" as NSCopying)
                self.uploadfile.setObject(firstName4, forKey: "product4" as NSCopying)
                self.uploadfile.setObject("", forKey: "product5" as NSCopying)
                break
                
            case 5:
                
                let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                let firstName1 : String = fullNameArr1[1]
                
                let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                let firstName2 : String = fullNameArr2[1]
                
                let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                let firstName3 : String = fullNameArr3[1]
                
                let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
                let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
                let firstName4 : String = fullNameArr4[1]
                
                let fullName5 : String = self.tableviewresponsearray.object(at: 4) as! String
                let fullNameArr5 : [String] = fullName5.components(separatedBy: " - ")
                let firstName5 : String = fullNameArr5[1]
                
                self.uploadfile.setObject(firstName1, forKey: "product1" as NSCopying)
                self.uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
                self.uploadfile.setObject(firstName3, forKey: "product3" as NSCopying)
                self.uploadfile.setObject(firstName4, forKey: "product4" as NSCopying)
                self.uploadfile.setObject(firstName5, forKey: "product5" as NSCopying)
                break
                
            default:
                break
            }
            
            self.uploadfile.setObject(self.customercodetf.text!, forKey: "Customercode" as NSCopying)
            self.uploadfile.setObject(LibraryAPI.sharedInstance.Userid, forKey: "UserId" as NSCopying)
            self.uploadfile.setObject(self.dealstarttf.text!, forKey: "DealStartDate" as NSCopying)
            self.uploadfile.setObject(self.dealclosuredatetf.text!, forKey: "DateofClosure" as NSCopying)
            self.uploadfile.setObject(self.creditdaystf.text!, forKey: "CreditDays" as NSCopying)
            self.uploadfile.setObject(self.creditamttf.text!, forKey: "CreditAmount" as NSCopying)
            
            
            let payment : [String] = paymenttf.text!.components(separatedBy: " - ")
            let paymentstr : String = payment[0]
            self.uploadfile.setObject(paymentstr, forKey: "PaymentMethod" as NSCopying)
            
            
            self.uploadfile.setObject(self.endcustomernametf.text!, forKey: "EndCustomerName" as NSCopying)
            self.uploadfile.setObject(self.checkchar, forKey: "EndCustomerPOReq" as NSCopying)
            self.uploadfile.setObject(self.endcusnotxtfield.text!, forKey: "EndCustomerPONumber" as NSCopying)
            self.uploadfile.setObject("", forKey: "Submit" as NSCopying)
            
            self.uploaddata()
            if self.jsonresult.count > 0
            {
                
                
                let errormessage = self.jsonresult.object(forKey: "ErrorMessage")
                let message      = self.jsonresult.object(forKey: "Message")
                let Refnumber    = self.jsonresult.object(forKey: "ReferenceNumber")
                
                
                if (Refnumber as? String) != ""
                {
                    self.showToast(message: Refnumber as! String)
                }
                else if (message as? String) != ""
                {
                    self.showToast(message: message as! String)
                }
                else if(errormessage as? String) != ""
                {
                    self.showToast(message: errormessage as! String)
                }
                else
                {
                    
                }
                
            }
            endcusnotxtfield.resignFirstResponder()
            return true
        }
        
        
        
        return true
        
        
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
 
       
        if (self.Arr.count == 0)
        {
            businesstableview.frame = CGRect(x: 12, y: 350, width: self.businesstableview.frame.width, height: 0)
            mainview.frame = CGRect(x: 8, y: 1, width: self.mainview.frame.width, height: 830)
            
            Topview.frame = CGRect(x: 0, y: 0, width: self.Topview.frame.width, height: self.Topview.frame.height)
            Bottomview.frame = CGRect(x: 0, y: 358, width: self.Bottomview.frame.width, height: self.Bottomview.frame.height)
            
            Scrollview.isScrollEnabled=true
            Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:900)

            self.tableviewview.isHidden = true
            return 0
        }
        else
        {
            if (self.Arr.count == 1)
            {
                businesstableview.frame = CGRect(x: 0, y: 351, width: self.businesstableview.frame.width, height: 0+40)
                 mainview.frame = CGRect(x: 8, y: 1, width: self.mainview.frame.width, height: 830)
                
                
                Topview.frame = CGRect(x: 0, y: 0, width: self.Topview.frame.width, height: self.Topview.frame.height)
                Bottomview.frame = CGRect(x: 0, y: 358 + 40, width: self.Bottomview.frame.width, height: self.Bottomview.frame.height)
                self.tableviewview.isHidden = false
                Scrollview.isScrollEnabled=true
                Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:900)

            }
            else if (self.Arr.count == 2)
            {
                businesstableview.frame = CGRect(x: 0, y: 351, width: self.businesstableview.frame.width, height: 0+80)
                 mainview.frame = CGRect(x: 8, y: 1, width: self.mainview.frame.width, height: 830)
                
                
                 Topview.frame = CGRect(x: 0, y: 0, width: self.Topview.frame.width, height: self.Topview.frame.height)
                Bottomview.frame = CGRect(x: 0, y: 358 + 80, width: self.Bottomview.frame.width, height: self.Bottomview.frame.height)
                self.tableviewview.isHidden = false
                Scrollview.isScrollEnabled=true
                Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:900)

            }
            else if (self.Arr.count == 3)
            {
                businesstableview.frame = CGRect(x: 0, y: 351, width: self.businesstableview.frame.width, height: 0 + 120)
                 mainview.frame = CGRect(x: 8, y: 1, width: self.mainview.frame.width, height: 830)
                
                 Topview.frame = CGRect(x: 0, y: 0, width: self.Topview.frame.width, height: self.Topview.frame.height)
                Bottomview.frame = CGRect(x: 0, y: 358 + 120, width: self.Bottomview.frame.width, height: self.Bottomview.frame.height)
                self.tableviewview.isHidden = false
                Scrollview.isScrollEnabled=true
                Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:900)

            }
            else if (self.Arr.count == 4)
            {
                
                businesstableview.frame = CGRect(x: 0, y: 351, width: self.businesstableview.frame.width, height: 0 + 160)
                 mainview.frame = CGRect(x: 8, y: 1, width: self.mainview.frame.width, height: 830)
                
                Topview.frame = CGRect(x: 0, y: 0, width: self.Topview.frame.width, height: self.Topview.frame.height)
                Bottomview.frame = CGRect(x: 0, y: 358 + 160, width: self.Bottomview.frame.width, height: self.Bottomview.frame.height)
                self.tableviewview.isHidden = false
                Scrollview.isScrollEnabled=true
                Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:900)

            }
            else if (self.Arr.count == 5)
            {
                
                businesstableview.frame = CGRect(x: 0, y: 351, width: self.businesstableview.frame.width, height: 0 + 200)
                 mainview.frame = CGRect(x: 8, y: 1, width: self.mainview.frame.width, height: 830)
                
                Topview.frame = CGRect(x: 0, y: 0, width: self.Topview.frame.width, height: self.Topview.frame.height)
                Bottomview.frame = CGRect(x: 0, y: 358 + 200, width: self.Bottomview.frame.width, height: self.Bottomview.frame.height)
                self.tableviewview.isHidden = false
                Scrollview.isScrollEnabled=true
                Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:900)
          }
            else
            {
                
                businesstableview.frame = CGRect(x: 0, y: 351, width: self.businesstableview.frame.width, height: 0)
                mainview.frame = CGRect(x: 8, y: 1, width: self.mainview.frame.width, height: 830)
                
                Topview.frame = CGRect(x: 0, y: 0, width: self.Topview.frame.width, height: self.Topview.frame.height)
                Bottomview.frame = CGRect(x: 0, y: 358, width: self.Bottomview.frame.width, height: self.Bottomview.frame.height)
                self.tableviewview.isHidden = true
                Scrollview.isScrollEnabled=true
                Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:900)

            }
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return tableviewresponsearray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BillingwithodCustomerCell", for: indexPath) as! BillingwithodCustomerCell
        
        
        
        
        cell.deletebtn.addTarget(self, action: #selector(self.deletebtnclicked), for: UIControlEvents.touchUpInside)
        cell.deletebtn.tag = indexPath.row
        
        let getString = tableviewresponsearray[indexPath.row] as? String
        
        
        self.uploadfile.setObject(customercodetf.text!, forKey: "Customercode" as NSCopying)
        self.uploadfile.setObject(LibraryAPI.sharedInstance.Userid, forKey: "UserId" as NSCopying)
        self.uploadfile.setObject(dealstarttf.text!, forKey: "DealStartDate" as NSCopying)
        self.uploadfile.setObject(dealclosuredatetf.text!, forKey: "DateofClosure" as NSCopying)
        self.uploadfile.setObject(creditdaystf.text!, forKey: "CreditDays" as NSCopying)
        self.uploadfile.setObject(creditamttf.text!, forKey: "CreditAmount" as NSCopying)
        
        
        let payment : [String] = paymenttf.text!.components(separatedBy: " - ")
        let paymentstr : String = payment[0]
        self.uploadfile.setObject(paymentstr, forKey: "PaymentMethod" as NSCopying)
        
        self.uploadfile.setObject(self.endcustomernametf.text!, forKey: "EndCustomerName" as NSCopying)
        self.uploadfile.setObject(self.checkchar, forKey: "EndCustomerPOReq" as NSCopying)
        self.uploadfile.setObject(self.endcusnotxtfield.text!, forKey: "EndCustomerPONumber" as NSCopying)
        self.uploadfile.setObject("", forKey: "Submit" as NSCopying)
        
        switch self.tableviewresponsearray.count
        {
        case 0:
            
            self.uploadfile.setObject("", forKey: "product1" as NSCopying)
            self.uploadfile.setObject("", forKey: "product2" as NSCopying)
            self.uploadfile.setObject("", forKey: "product3" as NSCopying)
            self.uploadfile.setObject("", forKey: "product4" as NSCopying)
            self.uploadfile.setObject("", forKey: "product5" as NSCopying)
            
            break
            
        case 1:
            
            let fullName : String = self.tableviewresponsearray.object(at: 0) as! String
            let fullNameArr : [String] = fullName.components(separatedBy: " - ")
            let firstName : String = fullNameArr[1]
            
            
            
            self.uploadfile.setObject(firstName, forKey: "product1" as NSCopying)
            self.uploadfile.setObject("", forKey: "product2" as NSCopying)
            self.uploadfile.setObject("", forKey: "product3" as NSCopying)
            self.uploadfile.setObject("", forKey: "product4" as NSCopying)
            self.uploadfile.setObject("", forKey: "product5" as NSCopying)
            
            break
            
        case 2:
            
            let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
            let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
            let firstName1 : String = fullNameArr1[1]
            
            let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
            let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
            let firstName2 : String = fullNameArr2[1]
            
            uploadfile.setObject(firstName1, forKey: "product1" as NSCopying)
            uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
            uploadfile.setObject("", forKey: "product3" as NSCopying)
            uploadfile.setObject("", forKey: "product4" as NSCopying)
            uploadfile.setObject("", forKey: "product5" as NSCopying)
            break
            
        case 3:
            
            let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
            let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
            let firstName1 : String = fullNameArr1[1]
            
            let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
            let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
            let firstName2 : String = fullNameArr2[1]
            
            let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
            let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
            let firstName3 : String = fullNameArr3[1]
            
            uploadfile.setObject(firstName1, forKey: "product1" as NSCopying)
            uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
            uploadfile.setObject(firstName3, forKey: "product3" as NSCopying)
            uploadfile.setObject("", forKey: "product4" as NSCopying)
            uploadfile.setObject("", forKey: "product5" as NSCopying)
            break
            
        case 4:
            
            let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
            let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
            let firstName1 : String = fullNameArr1[1]
            
            let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
            let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
            let firstName2 : String = fullNameArr2[1]
            
            let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
            let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
            let firstName3 : String = fullNameArr3[1]
            
            let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
            let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
            let firstName4 : String = fullNameArr4[1]
            
            uploadfile.setObject(firstName1, forKey: "product1" as NSCopying)
            uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
            uploadfile.setObject(firstName3, forKey: "product3" as NSCopying)
            uploadfile.setObject(firstName4, forKey: "product4" as NSCopying)
            uploadfile.setObject("", forKey: "product5" as NSCopying)
            break
            
        case 5:
            
            let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
            let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
            let firstName1 : String = fullNameArr1[1]
            
            let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
            let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
            let firstName2 : String = fullNameArr2[1]
            
            let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
            let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
            let firstName3 : String = fullNameArr3[1]
            
            let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
            let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
            let firstName4 : String = fullNameArr4[1]
            
            let fullName5 : String = self.tableviewresponsearray.object(at: 4) as! String
            let fullNameArr5 : [String] = fullName5.components(separatedBy: " - ")
            let firstName5 : String = fullNameArr5[1]
            
            uploadfile.setObject(firstName1, forKey: "product1" as NSCopying)
            uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
            uploadfile.setObject(firstName3, forKey: "product3" as NSCopying)
            uploadfile.setObject(firstName4, forKey: "product4" as NSCopying)
            uploadfile.setObject(firstName5, forKey: "product5" as NSCopying)
            break
            
        default:
            break
        }
        
        
        uploaddata()
        
        
        if self.jsonresult.count > 0
            
        {
            let fullNameArr5 : [String] = getString!.components(separatedBy: " - ")
            
            if(fullNameArr5.count >= 3)
            {
                let bizcode : String = fullNameArr5[0]
                let bizname : String = fullNameArr5[1]
                let bizname1 : String = fullNameArr5[2]
                cell.bizcode.text = bizcode
                cell.bizname.text = bizname + " - " + bizname1
            }
            else
                
            {
                let bizcode : String = fullNameArr5[0]
                let bizname : String = fullNameArr5[1]
                cell.bizcode.text = bizcode
                cell.bizname.text = bizname
                
            }
            
            
            let errormessage = self.jsonresult.object(forKey: "ErrorMessage")
            let message      = self.jsonresult.object(forKey: "Message")
            let Refnumber    = self.jsonresult.object(forKey: "ReferenceNumber")
            
            
            if (errormessage as? String) != ""
            {
                showToast(message: errormessage as! String)
            }
            else if (message as? String) != ""
            {
                showToast(message: message as! String)
            }
            else if(Refnumber as? String) != ""
            {
                showToast(message: Refnumber as! String)
            }
            else
            {
                
            }
        }
        else
        {
            //cell.textLabel?.text = getString
            let fullNameArr5 : [String] = getString!.components(separatedBy: " - ")
            
            if(fullNameArr5.count >= 3)
            {
                let bizcode : String = fullNameArr5[0]
                let bizname : String = fullNameArr5[1]
                let bizname1 : String = fullNameArr5[2]
                cell.bizcode.text = bizcode
                cell.bizname.text = bizname + " - " + bizname1
            }
            else
                
            {
                let bizcode : String = fullNameArr5[0]
                let bizname : String = fullNameArr5[1]
                cell.bizcode.text = bizcode
                cell.bizname.text = bizname
                
            }

        }
        
        
        return cell
    }
    
    
    
    
    
    
    
    
    
    func deletebtnclicked(sender: UIButton)
    {
        
        let alertController = UIAlertController(title: "Business Code", message: "Are You Sure Want to Delete Business Code?", preferredStyle: .alert)
        
        
        let DefaultAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
            
            self.tableviewresponsearray.removeObject(at: sender.tag)
            self.Arr.remove(at: sender.tag)
            self.businesstableview.reloadData()
            
        }
        
        let CancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
            
        }
        alertController.addAction(DefaultAction)
        alertController.addAction(CancelAction)
        self.present(alertController, animated: true, completion:nil)
        
    }
    
    func bizcodeRec1()
    {
        let size = CGSize(width: 30, height:30)
        self.local_activityIndicator_start()
        // new version dispatch Que
        
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
             self.Loaddata1(url: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/CustomerPaymentMethod?Userid=\(LibraryAPI.sharedInstance.Userid)")
            
            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
           self.local_activityIndicator_stop()
                
            }
        }
    
    }
    
    
    func Loaddata1(url:String)
    {
       // responsearray1 = LibraryAPI.sharedInstance.RequestUrl(url: url)
        self.getsuccess(params: [:], urlhit: url, completion: { (josnmutable) in
            self.responsearray1 = josnmutable
            if(self.responsearray1.count > 0)
            {
                
            }
            self.pickerarray1.removeAllObjects()
            for i in 0  ..< self.responsearray1.count
            {
                //  if(currenttf=="business")
                // {
                let dic = self.responsearray1[i] as! NSDictionary
                let code = dic.object(forKey: "Payment Method") as? String
                let desc = dic.object(forKey: "Description") as? String
                let str=code?.appending(" - \(desc!)")
                self.pickerarray1.add(str!)
                
                // }
            }
        })
        
        
        
    }
    
    func bizcodeRec()
    {
        self.local_activityIndicator_start()
        // new version dispatch Que
        
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
           // self.responsearray = LibraryAPI.sharedInstance.RequestUrl(url: ("\(LibraryAPI.sharedInstance.GlobalUrl)StockDetails.asmx/BusinessAccessDetails?UserID=\(LibraryAPI.sharedInstance.Userid)"))
            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)StockDetails.asmx/BusinessAccessDetails?UserID=\(LibraryAPI.sharedInstance.Userid)", completion: { (jsonmutable) in
                self.responsearray = jsonmutable
           
            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
                
                if(self.responsearray.count != 0)
                {
                    self.pickerarray.removeAllObjects()
                    for dicres in self.responsearray
                    {
                        let word = dicres as! NSDictionary
                        let code = word.object(forKey: "BUSINESS CODE") as! String
                        let desc = word.object(forKey: "BUSINESS DESC") as! String
                        let str = code + " - " + desc
                        self.pickerarray.add(str)
                        
                        
                    }
                }
                else
                {
                  self.local_activityIndicator_stop()
                    
                }
            }
        })
        }
      }
    
    
    
    func uploaddata() {
        let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)Users.asmx/DBCEntry")! as URL)
        request.httpMethod = "POST"
        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
        request.httpBody = try? JSONSerialization.data(withJSONObject: self.uploadfile, options: JSONSerialization.WritingOptions(rawValue: 0))
        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
        //  var err: NSError
        do {
            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
            jsonresult = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
        } catch (let e) {
            print(e)
        }
    }
    
    override func showToast(message : String) {
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(1.0)  // .withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = NSTextAlignment.center
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 6.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.numberOfLines = 5
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        toastLabel.sizeToFit()
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 8.0, delay: 0.1, options: UIViewAnimationOptions.curveEaseOut, animations:
            {
                toastLabel.alpha = 0.0
            }, completion: {(isCompleted) in toastLabel.removeFromSuperview()})
    }
    
    
    
    @IBAction func Approvalbutton(_ sender: AnyObject)
    {
        if ((self.customercodetf.text!.isEmpty ) && (self.dealclosuredatetf.text!.isEmpty) && (self.creditdaystf.text!.isEmpty) && (self.creditamttf.text!.isEmpty) && (self.paymenttf.text!.isEmpty) && (self.businesscodetf.text!.isEmpty) && (self.endcustomernametf.text!.isEmpty))
        {
            self.NormalAlert(title: "Alert", Messsage: "Please Enter the all fields")
           
        }
        else if(checkchar == "Y")
        {
            if(self.endcusnotxtfield.text!.isEmpty)
            {
                self.NormalAlert(title: "Alert", Messsage: "Please Enter the End Customer PoNumber")
            
                
            }
            else
            {
                switch self.tableviewresponsearray.count
                {
                case 0:
                    
                    self.uploadfile.setObject("", forKey: "product1" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product2" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product3" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product4" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product5" as NSCopying)
                    
                    break
                    
                case 1:
                    
                    let fullName : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr : [String] = fullName.components(separatedBy: " - ")
                    let firstName : String = fullNameArr[1]
                    
                    
                    
                    self.uploadfile.setObject(firstName, forKey: "product1" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product2" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product3" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product4" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product5" as NSCopying)
                    
                    break
                    
                case 2:
                    
                    let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                    let firstName1 : String = fullNameArr1[1]
                    
                    let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                    let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                    let firstName2 : String = fullNameArr2[1]
                    
                    self.uploadfile.setObject(firstName1, forKey: "product1" as NSCopying)
                    self.uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product3" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product4" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product5" as NSCopying)
                    break
                    
                case 3:
                    
                    let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                    let firstName1 : String = fullNameArr1[1]
                    
                    let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                    let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                    let firstName2 : String = fullNameArr2[1]
                    
                    let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                    let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                    let firstName3 : String = fullNameArr3[1]
                    
                    self.uploadfile.setObject(firstName1, forKey: "product1" as NSCopying)
                    self.uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
                    self.uploadfile.setObject(firstName3, forKey: "product3" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product4" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product5" as NSCopying)
                    break
                    
                case 4:
                    
                    let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                    let firstName1 : String = fullNameArr1[1]
                    
                    let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                    let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                    let firstName2 : String = fullNameArr2[1]
                    
                    let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                    let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                    let firstName3 : String = fullNameArr3[1]
                    
                    let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
                    let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
                    let firstName4 : String = fullNameArr4[1]
                    
                    self.uploadfile.setObject(firstName1, forKey: "product1" as NSCopying)
                    self.uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
                    self.uploadfile.setObject(firstName3, forKey: "product3" as NSCopying)
                    self.uploadfile.setObject(firstName4, forKey: "product4" as NSCopying)
                    self.uploadfile.setObject("", forKey: "product5" as NSCopying)
                    break
                    
                case 5:
                    
                    let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                    let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                    let firstName1 : String = fullNameArr1[1]
                    
                    let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                    let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                    let firstName2 : String = fullNameArr2[1]
                    
                    let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                    let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                    let firstName3 : String = fullNameArr3[1]
                    
                    let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
                    let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
                    let firstName4 : String = fullNameArr4[1]
                    
                    let fullName5 : String = self.tableviewresponsearray.object(at: 4) as! String
                    let fullNameArr5 : [String] = fullName5.components(separatedBy: " - ")
                    let firstName5 : String = fullNameArr5[1]
                    
                    self.uploadfile.setObject(firstName1, forKey: "product1" as NSCopying)
                    self.uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
                    self.uploadfile.setObject(firstName3, forKey: "product3" as NSCopying)
                    self.uploadfile.setObject(firstName4, forKey: "product4" as NSCopying)
                    self.uploadfile.setObject(firstName5, forKey: "product5" as NSCopying)
                    break
                    
                default:
                    break
                }
                
                
                self.uploadfile.setObject(self.customercodetf.text!, forKey: "Customercode" as NSCopying)
                self.uploadfile.setObject(LibraryAPI.sharedInstance.Userid, forKey: "UserId" as NSCopying)
                self.uploadfile.setObject(self.dealstarttf.text!, forKey: "DealStartDate" as NSCopying)
                self.uploadfile.setObject(self.dealclosuredatetf.text!, forKey: "DateofClosure" as NSCopying)
                self.uploadfile.setObject(self.creditdaystf.text!, forKey: "CreditDays" as NSCopying)
                self.uploadfile.setObject(self.creditamttf.text!, forKey: "CreditAmount" as NSCopying)
                
                
                let payment : [String] = paymenttf.text!.components(separatedBy: " - ")
                let paymentstr : String = payment[0]
                self.uploadfile.setObject(paymentstr, forKey: "PaymentMethod" as NSCopying)
                
                
                self.uploadfile.setObject(self.endcustomernametf.text!, forKey: "EndCustomerName" as NSCopying)
                self.uploadfile.setObject(self.checkchar, forKey: "EndCustomerPOReq" as NSCopying)
                self.uploadfile.setObject(self.endcusnotxtfield.text!, forKey: "EndCustomerPONumber" as NSCopying)
                self.uploadfile.setObject("y", forKey: "Submit" as NSCopying)
                //print("Upload file data", self.uploadfile)
                self.uploaddata()
                if self.jsonresult.count > 0
                {
                    self.dealclosuredatetf.resignFirstResponder()
                    
                    let errormessage = self.jsonresult.object(forKey: "ErrorMessage")
                    let message      = self.jsonresult.object(forKey: "Message")
                    let Refnumber    = self.jsonresult.object(forKey: "ReferenceNumber")
                    
                    
                    if (Refnumber as? String) != ""
                    {
                        let alertController = UIAlertController(title: "Message", message:"Deal Based Reference Number :           \(Refnumber!)" , preferredStyle: .alert)
                        
                        let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                            
                            self.navigate()
                            
                        }
                        alertController.addAction(cancelAction)
                        
                        
                        self.present(alertController, animated: true, completion:nil)
                        
                    }
                    else if (message as? String) != ""
                    {
                        self.showToast(message: message as! String)
                    }
                    else if(errormessage as? String) != ""
                    {
                        self.showToast(message: errormessage as! String)
                    }
                    else
                    {
                        
                    }
                    
                }
                
            }
            
            
        }
        else
        {
            switch self.tableviewresponsearray.count
            {
            case 0:
                
                self.uploadfile.setObject("", forKey: "product1" as NSCopying)
                self.uploadfile.setObject("", forKey: "product2" as NSCopying)
                self.uploadfile.setObject("", forKey: "product3" as NSCopying)
                self.uploadfile.setObject("", forKey: "product4" as NSCopying)
                self.uploadfile.setObject("", forKey: "product5" as NSCopying)
                
                break
                
            case 1:
                
                let fullName : String = self.tableviewresponsearray.object(at: 0) as! String
                let fullNameArr : [String] = fullName.components(separatedBy: " - ")
                let firstName : String = fullNameArr[1]
                
                
                
                self.uploadfile.setObject(firstName, forKey: "product1" as NSCopying)
                self.uploadfile.setObject("", forKey: "product2" as NSCopying)
                self.uploadfile.setObject("", forKey: "product3" as NSCopying)
                self.uploadfile.setObject("", forKey: "product4" as NSCopying)
                self.uploadfile.setObject("", forKey: "product5" as NSCopying)
                
                break
                
            case 2:
                
                let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                let firstName1 : String = fullNameArr1[1]
                
                let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                let firstName2 : String = fullNameArr2[1]
                
                self.uploadfile.setObject(firstName1, forKey: "product1" as NSCopying)
                self.uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
                self.uploadfile.setObject("", forKey: "product3" as NSCopying)
                self.uploadfile.setObject("", forKey: "product4" as NSCopying)
                self.uploadfile.setObject("", forKey: "product5" as NSCopying)
                break
                
            case 3:
                
                let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                let firstName1 : String = fullNameArr1[1]
                
                let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                let firstName2 : String = fullNameArr2[1]
                
                let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                let firstName3 : String = fullNameArr3[1]
                
                self.uploadfile.setObject(firstName1, forKey: "product1" as NSCopying)
                self.uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
                self.uploadfile.setObject(firstName3, forKey: "product3" as NSCopying)
                self.uploadfile.setObject("", forKey: "product4" as NSCopying)
                self.uploadfile.setObject("", forKey: "product5" as NSCopying)
                break
                
            case 4:
                
                let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                let firstName1 : String = fullNameArr1[1]
                
                let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                let firstName2 : String = fullNameArr2[1]
                
                let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                let firstName3 : String = fullNameArr3[1]
                
                let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
                let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
                let firstName4 : String = fullNameArr4[1]
                
                self.uploadfile.setObject(firstName1, forKey: "product1" as NSCopying)
                self.uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
                self.uploadfile.setObject(firstName3, forKey: "product3" as NSCopying)
                self.uploadfile.setObject(firstName4, forKey: "product4" as NSCopying)
                self.uploadfile.setObject("", forKey: "product5" as NSCopying)
                break
                
            case 5:
                
                let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                let firstName1 : String = fullNameArr1[1]
                
                let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                let firstName2 : String = fullNameArr2[1]
                
                let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                let firstName3 : String = fullNameArr3[1]
                
                let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
                let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
                let firstName4 : String = fullNameArr4[1]
                
                let fullName5 : String = self.tableviewresponsearray.object(at: 4) as! String
                let fullNameArr5 : [String] = fullName5.components(separatedBy: " - ")
                let firstName5 : String = fullNameArr5[1]
                
                self.uploadfile.setObject(firstName1, forKey: "product1" as NSCopying)
                self.uploadfile.setObject(firstName2, forKey: "product2" as NSCopying)
                self.uploadfile.setObject(firstName3, forKey: "product3" as NSCopying)
                self.uploadfile.setObject(firstName4, forKey: "product4" as NSCopying)
                self.uploadfile.setObject(firstName5, forKey: "product5" as NSCopying)
                break
                
            default:
                break
            }
            
            
            self.uploadfile.setObject(self.customercodetf.text!, forKey: "Customercode" as NSCopying)
            self.uploadfile.setObject(LibraryAPI.sharedInstance.Userid, forKey: "UserId" as NSCopying)
            self.uploadfile.setObject(self.dealstarttf.text!, forKey: "DealStartDate" as NSCopying)
            self.uploadfile.setObject(self.dealclosuredatetf.text!, forKey: "DateofClosure" as NSCopying)
            self.uploadfile.setObject(self.creditdaystf.text!, forKey: "CreditDays" as NSCopying)
            self.uploadfile.setObject(self.creditamttf.text!, forKey: "CreditAmount" as NSCopying)
            let payment : [String] = paymenttf.text!.components(separatedBy: " - ")
            let paymentstr : String = payment[0]
            self.uploadfile.setObject(paymentstr, forKey: "PaymentMethod" as NSCopying)
            self.uploadfile.setObject(self.endcustomernametf.text!, forKey: "EndCustomerName" as NSCopying)
            self.uploadfile.setObject(self.checkchar, forKey: "EndCustomerPOReq" as NSCopying)
            self.uploadfile.setObject(self.endcusnotxtfield.text!, forKey: "EndCustomerPONumber" as NSCopying)
            self.uploadfile.setObject("y", forKey: "Submit" as NSCopying)
            self.uploaddata()
            if self.jsonresult.count > 0
            {
                self.dealclosuredatetf.resignFirstResponder()
                let errormessage = self.jsonresult.object(forKey: "ErrorMessage")
                let message      = self.jsonresult.object(forKey: "Message")
                let Refnumber    = self.jsonresult.object(forKey: "ReferenceNumber")
                if (Refnumber as? String) != ""
                {
                    let alertController = UIAlertController(title: "Message", message:"Deal Based Reference Number : \(Refnumber!)" , preferredStyle: .alert)
                    let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                        self.navigate()
                    }
                    alertController.addAction(cancelAction)
                    self.present(alertController, animated: true, completion:nil)
                }
                else if (message as? String) != ""
                {
                    self.showToast(message: message as! String)
                }
                else if(errormessage as? String) != ""
                {
                    self.showToast(message: errormessage as! String)
                }
                else
                {
                    
                }
            }
        }
    }
    
    func navigate() {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
    }
    
    func notificationmethod()
    {
        let userdefaults = Foundation.UserDefaults.standard
        let value = userdefaults.dictionary(forKey: "Notification")
        
        var title: String!
        var message: String!
        var imagestr: String!
        
        title = value?["title"] as! String
        message = value?["body"] as! String
        imagestr = value?["imageurl"] as! String
        
        if imagestr != nil
        {
            if imagestr != ""
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: notificationimageViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = notificationimageViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                    
                }
                popup.show(childViewController: container)
            }
            else
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: PushnotificationtextViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = PushnotificationtextViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                }
                
                popup.show(childViewController: container)
            }
        }
        else
        {
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: PushnotificationtextViewController.instance())
                
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
            }
            let container = PushnotificationtextViewController.instance()
            container.closeHandler = { _ in
                popup.dismiss()
            }
            
            popup.show(childViewController: container)
        }
        
    }
}

extension String
{
    func isValidDouble(maxDecimalPlaces: Int) -> Bool
    {
        let formatter = NumberFormatter()
        formatter.allowsFloats = true
        let decimalSeparator = formatter.decimalSeparator ?? "."
        if formatter.number(from: self) != nil
        {
            let splint = self.components(separatedBy: decimalSeparator)
            let digits = splint.count == 2 ? splint.last ?? "" : ""
            return digits.count <= maxDecimalPlaces
        }
        return false
    }
}

