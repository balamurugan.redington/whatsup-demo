//
//  PocType2TableViewCell.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 27/07/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class PocType2TableViewCell: UITableViewCell {
    
    @IBOutlet weak var customernolbl: UILabel!
    
    @IBOutlet weak var customernamelbl: UILabel!
    
    @IBOutlet weak var itemcodelbl: UILabel!
    
    @IBOutlet weak var partyqualitylbl: UILabel!
    
    @IBOutlet weak var itemdescriptionlbl: UILabel!
    
    @IBOutlet weak var Dcnolbl: UILabel!
    @IBOutlet weak var initialtimelbl: UILabel!
    
    @IBOutlet weak var expectedtimelbl: UILabel!
    
    @IBOutlet weak var valuelbl: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
