



import UIKit
//import NVActivityIndicatorView
 
 class notificationimageViewController: UIViewController,PopupContentViewController
 {
    
    @IBOutlet weak var titlelbl: UILabel!
    
    @IBOutlet weak var imageview: UIImageView!
    
    @IBOutlet weak var textbodytxtview: UITextView!
    
 var closeHandler: (() -> Void)?
 
 override func viewDidLoad() {
 super.viewDidLoad()
    
    notificationmethod()
 
 }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    class func instance() -> notificationimageViewController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "notificationimageViewController") as! notificationimageViewController
    }
    
    
    
    func sizeForPopup(popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
    
        return CGSize(width: 300, height: 500)
    }
    
    
    func notificationmethod()
    {
        let userdefaults = Foundation.UserDefaults.standard
        let value = userdefaults.dictionary(forKey: "Notification")
        
        var title: String!
        var message: String!
          var imagestr: String!
        
                title = value?["title"] as! String
                message = value?["body"] as! String
                  imagestr = value?["imageurl"] as! String
                
                titlelbl.text = title
                textbodytxtview.text = message
        
        if let url = NSURL(string: imagestr) {
            if let data = NSData(contentsOf: url as URL) {
                imageview.image = UIImage(data: data as Data)
            }        
        }
        
        
    }
 
 
 
 
    @IBAction func okbtnAction(_ sender: AnyObject)
    {
        closeHandler?()
    }
 
 
 
 
 }

