//
//  Helpdisk2ViewController.swift
//  Redington
//
//  Created by RIL-ITTECH on 22/02/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import Cosmos
import KCFloatingActionButton

class Helpdisk2ViewController:  UIViewController,UIAlertViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPopoverControllerDelegate,UITextFieldDelegate,KCFloatingActionButtonDelegate,UITextViewDelegate{
    
    @IBOutlet weak var destf: UITextView!
    @IBOutlet weak var phonenumberlabel: UITextField!
    @IBOutlet weak var submitbutton: UIButton!
    @IBOutlet weak var ScrollView: UIScrollView!
    @IBOutlet weak var Rateview: CosmosView!
    @IBOutlet weak var feedbacktf: UITextView!
    @IBOutlet weak var helpdesktf: UITextView!
    @IBOutlet weak var cattf: UITextField!
    
    var uploadfile=NSMutableDictionary()
    var jsonresult=NSDictionary()
    var category = String()
    var des = String()
    var mobile = String()
    var number = String()
    var status_back = String()
    var userid = String()
    var ticketnumber = NSNumber()
    var  submit = Bool()
    var feedback = String()
    var feedbackstar = String()
    var demo = Int()
    var helpdesk = String()
    var feedbackcmt = String()
    var fab=KCFloatingActionButton()
    var picker:UIImagePickerController?=UIImagePickerController()
    var popover:UIPopoverController?=nil
    var Movedup=Bool()
    
    @IBOutlet var containerview: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Helpdisk2ViewController")
        ScrollView.contentSize = containerview.frame.size
        self.view.addSubview(ScrollView)
        destf.layer.cornerRadius = 5.0
        destf.layer.borderWidth = 1.0
        self.destf.layer.borderColor = UIColor(red:211/255.0, green:211/255.0, blue:211/255.0, alpha: 1.0).cgColor
        helpdesktf.layer.cornerRadius = 5.0
        helpdesktf.layer.borderWidth = 1.0
        self.helpdesktf.layer.borderColor = UIColor(red:211/255.0, green:211/255.0, blue:211/255.0, alpha: 1.0).cgColor
        feedbacktf.layer.cornerRadius = 5.0
        feedbacktf.layer.borderWidth = 1.0
        self.feedbacktf.layer.borderColor = UIColor(red:211/255.0, green:211/255.0, blue:211/255.0, alpha: 1.0).cgColor
        submitbutton.layer.cornerRadius = 15.0
        self.customnavigation()
        self.layoutFAB()
        NotificationCenter.default.addObserver(self, selector: #selector(Helpdisk2ViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(Helpdisk2ViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        cattf.text = category
        destf.text = des
        phonenumberlabel.text = mobile
        helpdesktf.text = helpdesk
        feedbacktf.text = feedbackcmt
        self.hide()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            self.fab.close()
        }
        fab.fabDelegate = self
        self.view.addSubview(fab)
    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
    }
    
    
    func hide() {
        if (LibraryAPI.sharedInstance.ticketstatus == "Pending") {
            submitbutton.isHidden = true
            Rateview.isHidden = true
            feedbacktf.isUserInteractionEnabled = false
        } else if(LibraryAPI.sharedInstance.ticketstatus == "Closed") {
            submitbutton.isHidden = true
            feedbacktf.isUserInteractionEnabled = false
            Rateview.rating=Double(feedbackstar)!
            Rateview.settings.updateOnTouch=false
        } else if(LibraryAPI.sharedInstance.ticketstatus == "Completed") {
            submitbutton.isHidden = false
            feedbacktf.isUserInteractionEnabled = true
            Rateview.isUserInteractionEnabled = true
        }
    }
    
    func keyboardWillShow(_ notification: NSNotification) {
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true) {
        } else {
            if(Movedup==false) {
                self.view.frame.origin.y -= 150
                Movedup=true
            }
        }
    }
    
    func keyboardWillHide(_ notification: NSNotification) {
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true) {
        } else {
            self.view.frame.origin.y = 0
            Movedup=false
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            feedbacktf.resignFirstResponder()
            return false
        }
        return true
    }
    
    
    
    
    @IBAction func SubmitAction(_ sender: AnyObject) {
        if((self.feedbacktf.text!.isEmpty )) {
            let alertController = UIAlertController(title: "Alert", message:"Please Enter the FeedBack " , preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
            }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)
        } else {
            if((self.feedbacktf.text?.count)!<3) {
                let alertController = UIAlertController(title: "Alert", message:"Enter Your Description" , preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                }
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion:nil)
            } else {
                if(self.Rateview.rating<1) {
                    let alertController = UIAlertController(title: "Alert", message:"Please Enter the Star Rating View " , preferredStyle: .alert)
                    let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                    }
                    alertController.addAction(cancelAction)
                    self.present(alertController, animated: true, completion:nil)
                } else {
                    uploadfile.setObject(LibraryAPI.sharedInstance.Userid, forKey: "UserId" as NSCopying)
                    uploadfile.setObject(feedbacktf.text, forKey: "Comments" as NSCopying)
                    uploadfile.setObject(number, forKey: "TicketNo" as NSCopying)
                    let rathingvalue = Rateview.rating
                    Rateview.text = "(" + String(rathingvalue) + ")"
                    uploadfile.setObject(rathingvalue, forKey: "Feedback" as NSCopying)
                    let size = CGSize(width: 30, height:30)
                    self.local_activityIndicator_start()
                    DispatchQueue.global(qos: .background).async {
                        self.uploaddata()
                        DispatchQueue.main.async {
                            self.local_activityIndicator_stop()
                            if(self.jsonresult.object(forKey: "Status") as? String  == "Success") {
                                let alertController = UIAlertController(title: "Alert", message: "Your Ticket is Placed SuccessFully \(self.number)", preferredStyle: .alert)
                                let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                    self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
                                    self.navigationController?.popViewController(animated: true)
                                }
                                alertController.addAction(cancelAction)
                                self.present(alertController, animated: true, completion:nil)
                            } else {
                                let alertController = UIAlertController(title: "Alert", message:"Submisson Failed! Retry" , preferredStyle: .alert)
                                let cancelAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
                                }
                                alertController.addAction(cancelAction)
                                self.present(alertController, animated: true, completion:nil)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func uploaddata() {
        let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)Ticket.asmx/TicketFeedback")! as URL)
        request.httpMethod = "POST"
        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
        request.httpBody = try? JSONSerialization.data(withJSONObject: self.uploadfile, options: JSONSerialization.WritingOptions(rawValue: 0))
        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
        do {
            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
            jsonresult = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
        } catch (let e) {
            print(e)
        }
    }
    
    func customnavigation() {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(Helpdisk2ViewController.backbuttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        if (LibraryAPI.sharedInstance.ticketstatus == "Completed"){
            customView.HeaderLabel.text = "Ticket Feedback"
        } else {
            customView.HeaderLabel.text = "Ticket Details"
        }
    }
    
    func backbuttonClicked(_ sender:UIButton) {
        LibraryAPI.sharedInstance.tracker="1"
        self.navigationController?.popViewController(animated: true)
    }
}



