//
//  PocTableViewCell.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 27/07/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class PocTableViewCell: UITableViewCell {
    
    @IBOutlet weak var itemcodelbl: UILabel!
    
    @IBOutlet weak var redingtonqtlbl: UILabel!
    
    @IBOutlet weak var stockroomlbl: UILabel!

    @IBOutlet weak var itemdescriptionlbl: UILabel!
    
    @IBOutlet weak var pocview: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
