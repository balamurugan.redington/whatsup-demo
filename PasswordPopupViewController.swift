//
//  PasswordPopupViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 19/07/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class PasswordPopupViewController: UIViewController,UITextFieldDelegate,PopupContentViewController {
    
    @IBOutlet weak var passwordsecurebtn: UIButton!
    @IBOutlet weak var Submitbtn: UIButton!
    @IBOutlet weak var passwordtf: UITextField!
    var uploadfile = NSMutableDictionary()
    var jsonresult = NSDictionary()
    var oldpasswordCheck : Bool!
    var closeHandler: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        passwordtf.delegate = self
        Submitbtn.layer.masksToBounds=true
        Submitbtn.layer.cornerRadius=5
        oldpasswordCheck = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    class func instance() -> PasswordPopupViewController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "PasswordPopupViewController") as! PasswordPopupViewController
    }
    
    @IBAction func CloseAction(_ sender: AnyObject) {
        
        closeHandler?()
    }
    
    func sizeForPopup(popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: 230, height: 230)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField == passwordtf) {
            guard let text = textField.text else { return true }
            
            let newLength = text.count + string.count - range.length
            return newLength <= 10
        }
        return true
    }
    
    @IBAction func SubmitBtnAction(_ sender: AnyObject) {
        if(passwordtf.text != "") {
            if(LibraryAPI.sharedInstance.Monthyearpayslip == 1) {
                uploadfile.setObject(LibraryAPI.sharedInstance.Userid, forKey: "UserId" as NSCopying)
                uploadfile.setObject("1", forKey: "Payslip_Type" as NSCopying)
                uploadfile.setObject(LibraryAPI.sharedInstance.payslipmonth, forKey: "Payslip_Month" as NSCopying)
                uploadfile.setObject(LibraryAPI.sharedInstance.payslipyear, forKey: "Payslip_Year" as NSCopying)
                uploadfile.setObject(passwordtf.text!, forKey: "Pass_Key" as NSCopying)
                uploadfile.setObject("", forKey: "Payslip_Quarter" as NSCopying)
                uploaddata()
                if(jsonresult.count>0) {
                    let message = jsonresult.object(forKey: "Message")as! String
                    let submit = jsonresult.object(forKey: "Submit")as! String
                    if(submit == "Y") {
                        let alertController = UIAlertController(title: "Alert", message:"Salary PaySlip has been Send successfully to Your Mail" , preferredStyle: .alert)
                        let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "HRD1DetailsViewController")), animated: true)
                        }
                        alertController.addAction(cancelAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        let alertController = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
                        let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "PaySlipViewController")), animated: true)
                        }
                        alertController.addAction(cancelAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                } else {
                }
                
            }
            if(LibraryAPI.sharedInstance.Monthyearpayslip == 2) {
                uploadfile.setObject(LibraryAPI.sharedInstance.Userid, forKey: "UserId" as NSCopying)
                uploadfile.setObject("2", forKey: "Payslip_Type" as NSCopying)
                uploadfile.setObject("", forKey: "Payslip_Month" as NSCopying)
                uploadfile.setObject(LibraryAPI.sharedInstance.payslipyear, forKey: "Payslip_Year" as NSCopying)
                uploadfile.setObject(LibraryAPI.sharedInstance.payslipmonth, forKey: "Payslip_Quarter" as NSCopying)
                uploadfile.setObject(passwordtf.text!, forKey: "Pass_Key" as NSCopying)
                uploaddata()
                if(jsonresult.count>0) {
                    let message = jsonresult.object(forKey: "Message")as! String
                    let submit = jsonresult.object(forKey: "Submit")as! String
                    if(submit == "Y") {
                        let alertController = UIAlertController(title: "Alert", message:"Variable PaySlip has been Send successfully to Your Mail" , preferredStyle: .alert)
                        let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "HRD1DetailsViewController")), animated: true)
                        }
                        alertController.addAction(cancelAction)
                        self.present(alertController, animated: true, completion:nil)
                    } else {
                        let alertController = UIAlertController(title: "Alert", message: message, preferredStyle: .alert)
                        let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "PaySlipViewController")), animated: true)
                        }
                        alertController.addAction(cancelAction)
                        self.present(alertController, animated: true, completion:nil)
                    }
                }
            } else {
            }
        } else {
            var alert: UIAlertView!
            alert = UIAlertView(title: title, message: "Please Enter the Password", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
    }
    
    @IBAction func passwordbtn(_ sender: AnyObject) {
        if (oldpasswordCheck == true) {
            passwordtf.isSecureTextEntry = false
            let password = passwordtf.text!
            passwordtf.attributedText = NSAttributedString(string: password)
            passwordsecurebtn.setImage(UIImage(named:"ic_eye_open.png"), for: UIControlState.normal)
            oldpasswordCheck = false
        } else {
            passwordtf.isSecureTextEntry = true
            passwordsecurebtn.setImage(UIImage(named:"ic_eye_closed.png"), for: UIControlState.normal)
            oldpasswordCheck = true
        }
        passwordtf.becomeFirstResponder()
    }
    
    func uploaddata() {
        let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)Users.asmx/PayslipData")! as URL)
        request.httpMethod = "POST"
        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
        request.httpBody = try? JSONSerialization.data(withJSONObject: self.uploadfile, options: JSONSerialization.WritingOptions(rawValue: 0))
        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
        do {
            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
            jsonresult = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
        } catch (let e) {
           print(e)
        }
    }
}
