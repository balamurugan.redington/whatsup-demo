//
//  SecurityViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 17/07/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import KCFloatingActionButton


class SecurityViewController: UIViewController,KCFloatingActionButtonDelegate,UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var SecurityTableview: UITableView!
    var responsestring = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("SecurityViewcontroller")
        SecurityTableview.register(UINib(nibName: "SecurityChequeTableViewCell", bundle: nil), forCellReuseIdentifier: "SecurityChequeTableViewCell")
        loaddata()
        SecurityTableview.tableFooterView = UIView(frame: .zero)
        SecurityTableview.separatorStyle = .none
        //viewpage
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.customnavigation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        //        appdelegate.shouldSupportAllOrientation = false
    }
    
    
    func customnavigation() {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width, height: 65)
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(SecurityViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Security Cheque Details"
    }
    
    func buttonClicked(sender:UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: NewkeyifoViewController.self) {
                self.navigationController?.popToViewController(controller as UIViewController, animated: true)
                break
            }
        }
    }
    
    func loaddata() {
        self.local_activityIndicator_start()
        // http://edi.redingtonb2b.in/Whatsup-staging/Customer.asmx/SecurityChequeDetails?Customer=S10003&User=SENTHILKS
        // responsestring = LibraryAPI.sharedInstance.securitycheque(url: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/SecurityChequeDetails?Customer=\(LibraryAPI.sharedInstance.CustomerCode)&User=\(LibraryAPI.sharedInstance.Userid)")
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/SecurityChequeDetails?Customer=\(LibraryAPI.sharedInstance.CustomerCode)&User=\(LibraryAPI.sharedInstance.Userid)") { (jsonMutable) in
            self.responsestring = jsonMutable
            if(self.responsestring.count > 0) {
                self.SecurityTableview.reloadData()
                 self.local_activityIndicator_stop()
            } else {
                for controller in self.navigationController!.viewControllers as Array {
                     self.local_activityIndicator_stop()
                    if controller.isKind(of: KeyInfoViewController.self) {
                        self.navigationController?.popToViewController(controller as UIViewController, animated: true)
                        LibraryAPI.sharedInstance.keyinfochepending = 0
                        break
                    }
                }
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 113
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if responsestring.count > 0 {
            return 1
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return responsestring.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SecurityChequeTableViewCell") as! SecurityChequeTableViewCell
        cell.viewpage.layer.cornerRadius = 5
        let dic = responsestring[indexPath.row] as! NSDictionary
        cell.chequeno.text = String(dic.object(forKey: "Cheque Number") as! Int)
        cell.receiptdate.text = dic.object(forKey: "Date of Receipt") as? String
        cell.timeofreceipt.text = dic.object(forKey: "Time of Receipt") as? String
        cell.bankname.text = dic.object(forKey: "Bank Name") as? String
        cell.chequedate.text = dic.object(forKey: "Cheque date") as? String
        cell.chequeamount.text = String(dic.object(forKey: "Cheque Amount")as! Double)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
    }
}
