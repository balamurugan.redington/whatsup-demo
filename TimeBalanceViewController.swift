//
//  TimeBalanceViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 26/07/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class TimeBalanceViewController: UIViewController,UITableViewDataSource,UITableViewDelegate
{
    @IBOutlet weak var Timebalancetableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if LibraryAPI.sharedInstance.Globalflag == "N" {
            customnavigation1()
        } else {
            customnavigation()
        }
        Timebalancetableview.delegate = self
        Timebalancetableview.dataSource = self
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        Timebalancetableview.register(UINib(nibName: "LeaveTimeTableViewCell", bundle: nil), forCellReuseIdentifier: "LeaveTimeTableViewCell")
        Timebalancetableview.tableFooterView = UIView(frame: .zero)
        Timebalancetableview.separatorStyle = .none
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    func customnavigation() {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(TimeBalanceViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "View Time Details"
    }
    
    func customnavigation1() {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(TimeBalanceViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.HeaderLabel.textAlignment = .center
        customView.HeaderLabel.font = UIFont.boldSystemFont(ofSize: 16.0)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Time Balance EnquiryList"
    }
    
    func buttonClicked(sender:UIButton) {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "LeaveBalanceViewController")), animated: true)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return 95
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if(Biomatric_Time_Arr.count == 0) {
            let label = UILabel(frame: CGRect(x: 0,y: 0,width: tableView.bounds.size.width,height: tableView.bounds.size.height))
            label.text = "No Data Available"
            label.textColor = UIColor.red
            label.textAlignment = NSTextAlignment.center
            label.sizeToFit()
            label.font = UIFont(name: (label.font?.fontName)!,size: 20)
            tableView.backgroundView = label
            return 0
        } else {
            tableView.backgroundView = nil
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return Biomatric_Time_Arr.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeaveTimeTableViewCell") as! LeaveTimeTableViewCell
        cell.punchdatelbl.text = Biomatric_Time_Arr[indexPath.row].PUNCH_DATE
        cell.lesthan945.text =  String(describing: Biomatric_Time_Arr[indexPath.row].after9)
        cell.lessthan11lbl.text = String(describing: Biomatric_Time_Arr[indexPath.row].after11)
        cell.pottimelbl.text = Biomatric_Time_Arr[indexPath.row].OUT_TIME
        cell.intimelbl.text = Biomatric_Time_Arr[indexPath.row].IN_TIME
        cell.latecominglbl.text = Biomatric_Time_Arr[indexPath.row].LATE_COMING
        if(Biomatric_Time_Arr[indexPath.row].PERMISSION_NOTAPPLIED == "R")
        {
            cell.leaveview.layer.borderWidth = 1.0
            cell.leaveview.layer.borderColor = UIColor.red.cgColor
        }
        else
        {
            cell.leaveview.layer.borderColor = UIColor.clear.cgColor
        }
        cell.selectionStyle = .none
        return cell
    }
}
