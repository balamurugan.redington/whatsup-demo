//
//  VistlogpopupViewController.swift
//  Redington
//
//  Created by truetech on 03/10/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit

class BookhistroyViewController: UIViewController,PopupContentViewController,UITableViewDataSource,UITableViewDelegate{
    
    var closeHandler: (() -> Void)?
   
    var responsestring = NSMutableArray()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.Loaddata()
    }
    
    
    
    class func instance() -> BookhistroyViewController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "BookhistroyViewController") as! BookhistroyViewController
    }
    
    @IBAction func CloseAction(_ sender: AnyObject)
    {
        
        closeHandler?()
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func sizeForPopup(popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: 290, height: 350)
    }
    
    
    func Loaddata()
    {
      // http://edi.redingtonb2b.in/whatsup-staging/WorkLocation.asmx/SeatBookingHistory?UserId=SENTHILKS
        
        //responsestring = LibraryAPI.sharedInstance.RequestUrl(url: "\(LibraryAPI.sharedInstance.GlobalUrl)/WorkLocation.asmx/SeatBookingHistory?&Userid=\(LibraryAPI.sharedInstance.Userid)")
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)/WorkLocation.asmx/SeatBookingHistory?&Userid=\(LibraryAPI.sharedInstance.Userid)", completion: { (jsonmutable) in
            self.responsestring = jsonmutable
        })
    }
    
    

    
    //TableView Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
       
        if responsestring.count > 0
        {
            return 1
            
        }
        else
        {
            return 0
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return responsestring.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        var cell : BookhistroyTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as! BookhistroyTableViewCell
        if(cell == nil)
        {
            cell = Bundle.main.loadNibNamed("Cell1", owner: self, options: nil)![0] as! BookhistroyTableViewCell;
            
        }
        
        if(indexPath.row%2==0)
        {
            cell.contentView.backgroundColor=(UIColor.init(red: 240/255.0, green: 246/255.0, blue: 225/255.0, alpha: 1.0))
        }
        else
        {
            cell.contentView.backgroundColor=(UIColor.init(red: 215/255.0, green: 250/255.0, blue: 252/255.0, alpha: 1.0))
        }
        let dic = responsestring[indexPath.row] as! NSDictionary
        cell.ref.text = dic.object(forKey: "Booking Reference") as? String
        cell.ddcell.text = dic.object(forKey: "Booking Date") as? String
        cell.from.text = dic.object(forKey: "From Time") as? String
        cell.to.text = dic.object(forKey: "To Time") as? String
        
     
        return cell as BookhistroyTableViewCell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
  
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
    
    }
        
}
