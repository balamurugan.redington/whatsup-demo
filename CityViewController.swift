//
//  StockInquiryPopupController.swift
//  Redington
//
//  Created by TRUETECHMAC3 on 27/12/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
//import  NVActivityIndicatorView


class CityViewController: UIViewController,PopupContentViewController{
    @IBOutlet var Submitbtn: UIButton!
    @IBOutlet var Closebtn: UIButton!
    @IBOutlet var ItemcodetxtField: UITextField!
      var closeHandler: (() -> Void)?
    
    var responsearray = NSMutableArray()
    var pickerarray = NSMutableArray()
    var responsestring=NSMutableArray()
    
   
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        Submitbtn.layer.cornerRadius = 15.0
          NotificationCenter.default.addObserver(self, selector: #selector(self.notificationmethod), name: NSNotification.Name(rawValue: "GetNotification"), object: nil)
          }
    
    @IBAction func SubmitLocation(_ sender: AnyObject) {
        
        LibraryAPI.sharedInstance.ItemCode = ItemcodetxtField.text!
        
        if  LibraryAPI.sharedInstance.ItemCode == ""{
          //  self.showalert("Sorry!", Message: "ItemCode is empty")
            self.NormalAlert(title: "Sorry!", Messsage: "ItemCode is empty")
            
        }else{
            self.LoadAllLocation()
        }
        
        
    }
    
        
   /* func showalert(Title:String,Message:String)
    {
        
        let alert = UIAlertController(title: Title, message: Message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Try Again", style: UIAlertActionStyle.Default, handler:
            { (ACTION :UIAlertAction!)in
                
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }*/
    
    @IBAction func SubmitbtnAction(_ sender: AnyObject)
    {
        
    }
    
    @IBAction func ClosebtnAction(_ sender: AnyObject) {
        closeHandler?()
    }
    
    @IBAction func CloseAction(_ sender: AnyObject)
    {
        
        closeHandler?()
    }
    
    
    func sizeForPopup(popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: 260, height: 260)
    }
    
    class func instance() -> ECBViewController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        
        return storyboard.instantiateViewController(withIdentifier: "ECBViewController") as! ECBViewController
    }
    
    
    func bizcodeRec()
    {
        //let size = CGSize(width: 30, height:30)
       self.local_activityIndicator_start()        
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
            
             self.Loaddata(url: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/CityDetails?Userid=\(LibraryAPI.sharedInstance.Userid)&prmt=\(self.ItemcodetxtField.text)")
            
            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
                self.local_activityIndicator_stop()
                
            }
        }
     
    }
    
    
    func Loaddata(url:String)
    {
        //responsearray = LibraryAPI.sharedInstance.RequestUrl(url: url)
        self.getsuccess(params: [:], urlhit: url, completion:  { (jsonmutable) in
            self.responsearray = jsonmutable
            if(self.responsearray.count > 0)
            {
                
            }
            self.pickerarray.removeAllObjects()
            for i in 0..<self.responsearray.count
            {
                //  if(currenttf=="business")
                // {
                let dic = self.responsearray[i] as! NSDictionary
                let code = dic.object(forKey: "Mode") as? String
                let desc = dic.object(forKey: "Detail") as? String
                let str=code?.appending(" - \(desc!)")
                self.pickerarray.add(str!)
                // }
            }
        })
    }
    

    
    
    func LoadAllLocation(){
       
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
            
          //  self.responsestring = LibraryAPI.sharedInstance.Dashboardurlcall(url: "\(LibraryAPI.sharedInstance.GlobalUrl)StockDetails.asmx/AllStockstockRoom?Itemcode=\(LibraryAPI.sharedInstance.ItemCode)&UserId=\(LibraryAPI.sharedInstance.Userid)")
            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)StockDetails.asmx/AllStockstockRoom?Itemcode=\(LibraryAPI.sharedInstance.ItemCode)&UserId=\(LibraryAPI.sharedInstance.Userid)", completion: { (jsonmutable) in
                self.responsestring = jsonmutable
                DispatchQueue.main.async {
                    if(self.responsestring.count>0)
                    {
                        self.local_activityIndicator_stop()
                        self.closeHandler?()
                        LibraryAPI.sharedInstance.StockAllLocationArry = self.responsestring
                        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "StockEnquiryAllLocationViewController")), animated: true)
                        self.sideMenuViewController?.hideMenuViewController()
                        
                        
                    }else{
                        self.local_activityIndicator_stop()
                        
                        self.NormalAlert(title: "Sorry!", Messsage: "No Item found")
                    }
                    
                    
                }
            })

            // Go back to the main thread to update the UI
            
        }  
    }
     override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(textField: UITextField!) -> Bool // called when 'return' key pressed. return NO to ignore.
    {
        ItemcodetxtField.resignFirstResponder()
        return true;
    }
    
    
    func notificationmethod()
    {
        let userdefaults = Foundation.UserDefaults.standard
        let value = userdefaults.dictionary(forKey: "Notification")
        
        var title: String!
        var message: String!
        var imagestr: String!
        
        title = value?["title"] as! String
        message = value?["body"] as! String
        imagestr = value?["imageurl"] as! String
        
        if imagestr != nil
        {
            if imagestr != ""
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: notificationimageViewController.instance())
                    
                    .didShowHandler { popup in
                      
                    }
                    .didCloseHandler { _ in
                      
                }
                let container = notificationimageViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                    
                }
                popup.show(childViewController: container)
            }
            else
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: PushnotificationtextViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = PushnotificationtextViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                }
                
                popup.show(childViewController: container)
            }
        }
        else
        {
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: PushnotificationtextViewController.instance())
                
                .didShowHandler { popup in
                   
                }
                .didCloseHandler { _ in
                   
            }
            let container = PushnotificationtextViewController.instance()
            container.closeHandler = { _ in
                popup.dismiss()
            }
            
            popup.show(childViewController: container)
        }
        
              
    }    
}
