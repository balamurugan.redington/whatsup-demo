//
//  AddtocartViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 27/06/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class AddtocartViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var addtocarttableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addtocarttableview.delegate = self
        addtocarttableview.dataSource = self
        addtocarttableview.register(UINib(nibName: "addtocartTableViewCell", bundle: nil), forCellReuseIdentifier: "addtocartTableViewCell")
      
        
        addtocarttableview.tableFooterView = UIView(frame: .zero)
        addtocarttableview.separatorStyle = .none

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    
    
   

    @IBAction func BackButtonaction(_ sender: AnyObject)
    {
        
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
        
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        if LibraryAPI.sharedInstance.globalarray.count > 0
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
       
       return LibraryAPI.sharedInstance.globalarray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "addtocartTableViewCell", for: indexPath) as! addtocartTableViewCell
        
        cell.deletebtn.addTarget(self, action: #selector(self.deletebtnclicked), for: UIControlEvents.touchUpInside)
        cell.deletebtn.tag = indexPath.row
        cell.addtocartview.layer.cornerRadius = 5
        let dic = LibraryAPI.sharedInstance.globalarray[indexPath.row] as! NSDictionary
        cell.itemcodelbl.text = dic.object(forKey: "itemcode") as? String
        cell.descriptionlbl.text = dic.object(forKey: "ItemDescription_out") as? String
        cell.qualitylbl.text = dic.object(forKey: "quantity") as? String
        cell.vendorbplbl.text = dic.object(forKey: "vendorbid") as? String
        
        return cell
    }
    
    
    
   
    
    func deletebtnclicked(sender: UIButton)
    {
        
        let alertController = UIAlertController(title: "Business Code", message: "Are You Sure Want to Delete Add to cart?", preferredStyle: .alert)
        
        
        let DefaultAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
            
            LibraryAPI.sharedInstance.globalarray.removeObject(at: sender.tag)
            self.addtocarttableview.reloadData()
            
        }
        
        let CancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
            
        }
        alertController.addAction(DefaultAction)
        alertController.addAction(CancelAction)
        self.present(alertController, animated: true, completion:nil)
        
    }


   

}
