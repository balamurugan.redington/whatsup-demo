//
//  BioMetricViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 25/07/17.
//  Copyright © 2017 truetech. All rights reserved.

import UIKit
//import NVActivityIndicatorView

class BioMetricViewController: UIViewController,UITextFieldDelegate,PopupContentViewController {
    @IBOutlet weak var selectYeartf: UITextField!
    var currenttf=String()
    var strDate = String()
    var Years : [String] = []
    var currentyear : Int!
    var BeforeYears : [String] = []
    var responsestring = NSMutableArray()
    var leavebalance_dic = NSMutableDictionary()
    var closeHandler: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("BioMetricViewController")
        selectYeartf.delegate = self
        selectYeartf.tag = 1
        var todaysDate:NSDate = NSDate()
        var dateFormatter:DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.string(from: NSDate() as Date)
        let payment : [String] = date.components(separatedBy: "-")
        let paymentstr : String = payment[0]
        currentyear = Int(paymentstr)!  + 1
        BeforeYears.removeAll()
        BeforeYears.append(String(currentyear))
        for i in 1 ..< 5 {
            let before = currentyear - i
            BeforeYears.append(String(before))
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func CloseAction(_ sender: AnyObject) {
        closeHandler?()
    }
    
    class func instance() -> BioMetricViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "BioMetricViewController") as! BioMetricViewController
    }
    
    func sizeForPopup(popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: 230, height: 230)
    }
    
    func BackGroundRefresh(Notification : NSNotification) {
        NotificationCenter.default.addObserver(self, selector: #selector(BioMetricViewController.SubmitAction), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if(textField.tag == 1) {
            let alert:UIAlertController=UIAlertController(title: "Select Year", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            var alert1 = UIAlertAction()
            
            for word in BeforeYears {
                let beforeyear = Int(word)! - 1
                let CurrentYear = String(beforeyear) + " - " + word
                alert1 = UIAlertAction(title: CurrentYear, style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    self.selectYeartf.text = CurrentYear
                    LibraryAPI.sharedInstance.Bio_Metric = CurrentYear
                }
                alert.addAction(alert1)
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                UIAlertAction in
            }
            
            // Add the actions
            //picker?.delegate = self
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
            return false
        }
        return true
    }
    
    @IBAction func SubmitAction(_ sender: AnyObject) {
        if(self.connectedToNetwork()) {
            if(selectYeartf.text == "" || selectYeartf.text == nil) {
                self.NormalAlert(title: "Alert", Messsage: "Please Enter the Year")
            } else {
                if(self.connectedToNetwork()) {
                    local_activityIndicator_start()
                    DispatchQueue.global(qos: .background).async {
                        let Previousyear = self.selectYeartf.text?.components(separatedBy: " - ")
                        let Year: String = Previousyear![0]
                        self.getjsonMutableDictionarysuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)LeaveDetails.asmx/LeaveBalance?UserID=\(LibraryAPI.sharedInstance.Userid.stringByRemovingWhitespaces)&Month=&Year=\(Year)", completion: { (jsonmudic) in
                            self.leavebalance_dic = jsonmudic 
                            DispatchQueue.main.async {
                                if(self.leavebalance_dic.count == 1) {
                                    self.local_activityIndicator_stop()
                                    if(self.leavebalance_dic.object(forKey: "Message")as! String == "Year cannot be lesser than Year of Joining") {
                                        self.NormalAlert(title: "Alert", Messsage: "Year cannot be lesser than Year of Joining")
                                    }
                                } else {
                                    if(self.connectedToNetwork()) {
                                        if(self.leavebalance_dic.count > 0) {
                                            self.local_activityIndicator_stop()
                                            BioMatric_Arr = [BioMatric]()
                                            if((self.leavebalance_dic.object(forKey: "LeaveBalance_Year")! as AnyObject).count > 0) {
                                                for data in self.leavebalance_dic.object(forKey: "LeaveBalance_Year") as! NSArray {
                                                    BioMatric_Arr.append(BioMatric(data: data as AnyObject))
                                                }
                                                self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "LeaveBalanceViewController")), animated: true)
                                                self.CloseAction(HrdViewController())
                                                self.sideMenuViewController!.hideMenuViewController()
                                                self.closeHandler?()
                                            } else {
                                                self.NormalAlert(title: "Alert", Messsage: "No Data Available")
                                            }
                                        }
                                    } else {
                                    }
                                }
                            }
                        })
                    }
                } else {
                }
            }
        } else {
        }
    }
    
    func customErrorPage() {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "ApiErrorViewController") as! ApiErrorViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height)
        customView.retrybtn.addTarget(self, action: #selector(BioMetricViewController.retrybuttonClicked), for: UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
    }
    
    func retrybuttonClicked(sender: UIButton) {
        if self.connectedToNetwork() {
            let lvc=self.storyboard?.instantiateViewController(withIdentifier: "BioMetricViewController") as! BioMetricViewController
            self.navigationController?.pushViewController(lvc, animated: true)
        } else {
        }
    }
    
}
