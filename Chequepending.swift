//
//  Chequepending.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 16/02/18.
//  Copyright © 2018 truetech. All rights reserved.
//

import Foundation


struct Chequepending {
   
    
    var BUSINESS_CODE : String
    var BUSINESS_DESC : String
    var CUSTOMER_CODE : String
    var INVOICE_NUMBER : String
    var INVOICE_VALUE : NSNumber
    var INVOICE_DATE : String
    var PENDING_DAYS : String
    var DUE_DATE : String
    var OVER_DUE_DAYS : String
    var RECORD_NUMBER : NSNumber
    var PAGE_NUMBER : NSNumber
    var TOTAL_PAGE : NSNumber
    
    init() {
        BUSINESS_CODE = ""
        BUSINESS_DESC = ""
        CUSTOMER_CODE = ""
        INVOICE_NUMBER = ""
        INVOICE_VALUE = 0
        INVOICE_DATE = ""
        PENDING_DAYS = ""
        DUE_DATE = ""
        OVER_DUE_DAYS = ""
        RECORD_NUMBER = 0
        PAGE_NUMBER = 0
        TOTAL_PAGE = 0
    }
    init(data: AnyObject) {
        if let temp = data["CUSTOMER CODE"] as? String {
            self.CUSTOMER_CODE = temp
        }else {
            self.CUSTOMER_CODE = ""
        }
        
        if let temp = data["BUSINESS CODE"] as? String {
            self.BUSINESS_CODE = temp
        }else {
            self.BUSINESS_CODE = ""
        }
        
        if let temp = data["BUSINESS DESCE"] as? String {
            self.BUSINESS_DESC = temp
        }else {
            self.BUSINESS_DESC = ""
        }
        
        if let temp = data["INVOICE NUMBER"] as? String {
            self.INVOICE_NUMBER = temp
        }else {
            self.INVOICE_NUMBER = ""
        }
        
        if let temp = data["INVOICE VALUE"] as? NSNumber {
            self.INVOICE_VALUE = temp
        }else {
            self.INVOICE_VALUE = 0
        }
        
        if let temp = data["INVOICE DATE"] as? String {
            self.INVOICE_DATE = temp
        }else {
            self.INVOICE_DATE = ""
        }
        
        if let temp = data["PENDING DAYS"] as? String {
            self.PENDING_DAYS = temp
        }else {
            self.PENDING_DAYS = ""
        }
        
        if let temp = data["DUE DATE"] as? String {
            self.DUE_DATE = temp
        }else {
            self.DUE_DATE = ""
        }
        
        if let temp = data["OVER DUE DAYS"] as? String {
            self.OVER_DUE_DAYS = temp
        }else {
            self.OVER_DUE_DAYS = ""
        }
        
        if let temp = data["RECORD NUMBER"] as? NSNumber {
            self.RECORD_NUMBER = temp
        }else {
            self.RECORD_NUMBER = 0
        }
        
        if let temp = data["PAGE NUMBER"] as? NSNumber {
            self.PAGE_NUMBER = temp
        }else {
            self.PAGE_NUMBER = 0
        }
        
        if let temp = data["TOTAL PAGE"] as? NSNumber {
            self.TOTAL_PAGE = temp
        }else {
            self.TOTAL_PAGE = 0
        }



        
    }
}
