//
//  PaySlipViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 18/07/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class PaySlipViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var yeartf: UITextField!
    @IBOutlet weak var Pickerview: UIView!
    @IBOutlet weak var monthtf: UITextField!
    @IBOutlet weak var monthlbl: UILabel!
    @IBOutlet weak var Submitbtn: UIButton!
    @IBOutlet weak var Quarterlbl: UILabel!
    @IBOutlet weak var DatePicker: UIDatePicker!
    
    var currenttf=String()
    var strDate = String()
    var Years : [String] = []
    var currentyear : Int!
    var BeforeYears : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("PaySlipViewController")
        if LibraryAPI.sharedInstance.Globalflag == "N"
        {
            customnavigation1()
        }
        else
        {
            customnavigation()
        }
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        yeartf.delegate = self
        monthtf.delegate = self
        yeartf.tag = 1
        monthtf.tag = 2
        Submitbtn.layer.masksToBounds=true
        Submitbtn.layer.cornerRadius=5
        var todaysDate:NSDate = NSDate()
        var dateFormatter:DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.string(from: NSDate() as Date)
        let payment : [String] = date.components(separatedBy: "-")
        let paymentstr : String = payment[0]
        currentyear = Int(paymentstr)!
        BeforeYears.removeAll()
        BeforeYears.append(String(currentyear))
        for i in 1 ..< 5
        {
            let before = currentyear - i
            BeforeYears.append(String(before))
        }
        if(LibraryAPI.sharedInstance.Monthyearpayslip == 1)
        {
            monthlbl.isHidden = false
            Quarterlbl.isHidden = true
            
        }
        if(LibraryAPI.sharedInstance.Monthyearpayslip == 2)
        {
            monthlbl.isHidden = true
            Quarterlbl.isHidden = false
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    override func viewWillAppear(_ animated: Bool) {
    }
    func customnavigation()
    {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65)
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(PaySlipViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        if(LibraryAPI.sharedInstance.Monthyearpayslip == 1)
        {
            customView.HeaderLabel.text = "Salary Payslip"
        }
        if(LibraryAPI.sharedInstance.Monthyearpayslip == 2)
        {
            customView.HeaderLabel.text = "variable paySlip"
        }
    }
    
    func customnavigation1()
    {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(PaySlipViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.HeaderLabel.textAlignment = .center
        customView.HeaderLabel.font = UIFont.boldSystemFont(ofSize: 16.0)
        self.view.addSubview(customView.view)
        if(LibraryAPI.sharedInstance.Monthyearpayslip == 1)
        {
            customView.HeaderLabel.text = "Salary Payslip"
        }
        if(LibraryAPI.sharedInstance.Monthyearpayslip == 2)
        {
            customView.HeaderLabel.text = "variable paySlip"
        }
    }
    
    func buttonClicked(sender:UIButton)
    {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "HRD1DetailsViewController")), animated: true)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if(textField.tag == 1) {
            let alert:UIAlertController=UIAlertController(title: "Select Year", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            var alert1 = UIAlertAction()
            for word in BeforeYears {
                alert1 = UIAlertAction(title: word, style: UIAlertActionStyle.default) {
                    UIAlertAction in
                    self.yeartf.text = word
                    LibraryAPI.sharedInstance.payslipyear = word
                }
                alert.addAction(alert1)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                UIAlertAction in
            }
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
            return false
        } else if(textField.tag == 2) {
            if(LibraryAPI.sharedInstance.Monthyearpayslip == 1) {
                let alert:UIAlertController=UIAlertController(title: "Select Month", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
                let head2 = ["January","February","March","April","May","June","July","August","September","October","November","December"]
                var alert1 = UIAlertAction()
                if head2.count != 0 {
                    for word in head2 {
                        alert1 = UIAlertAction(title: word, style: UIAlertActionStyle.default) {
                            UIAlertAction in
                            self.monthtf.text = word
                            if(word == "January") {
                                LibraryAPI.sharedInstance.payslipmonth = "01"
                            } else if(word == "February") {
                                LibraryAPI.sharedInstance.payslipmonth = "02"
                            } else if(word == "March") {
                                LibraryAPI.sharedInstance.payslipmonth = "03"
                            } else if(word == "April") {
                                LibraryAPI.sharedInstance.payslipmonth = "04"
                            } else if(word == "May") {
                                LibraryAPI.sharedInstance.payslipmonth = "05"
                            } else if(word == "June") {
                                LibraryAPI.sharedInstance.payslipmonth = "06"
                            } else if(word == "July") {
                                LibraryAPI.sharedInstance.payslipmonth = "07"
                            } else if(word == "August") {
                                LibraryAPI.sharedInstance.payslipmonth = "08"
                            } else if(word == "September") {
                                LibraryAPI.sharedInstance.payslipmonth = "09"
                            } else if(word == "October") {
                                LibraryAPI.sharedInstance.payslipmonth = "10"
                            } else if(word == "November") {
                                LibraryAPI.sharedInstance.payslipmonth = "11"
                            } else if(word == "December") {
                                LibraryAPI.sharedInstance.payslipmonth = "12"
                            }
                        }
                        alert.addAction(alert1)
                    }
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                    UIAlertAction in
                }
                alert.addAction(cancelAction)
                self.present(alert, animated: true, completion: nil)
                return false
            }
            if(LibraryAPI.sharedInstance.Monthyearpayslip == 2) {
                let alert:UIAlertController=UIAlertController(title: "Select Quarter", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
                let head2 = ["Q1","Q2","Q3","Q4"]
                var alert1 = UIAlertAction()
                if head2.count != 0 {
                    for word in head2 {
                        alert1 = UIAlertAction(title: word, style: UIAlertActionStyle.default) {
                            UIAlertAction in
                            self.monthtf.text = word
                            LibraryAPI.sharedInstance.payslipmonth = word
                        }
                        alert.addAction(alert1)
                    }
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                    UIAlertAction in
                }
                alert.addAction(cancelAction)
                self.present(alert, animated: true, completion: nil)
                return false
            }
        }
        return false
    }
    
    
    
    @IBAction func SubmitbtnAction(_ sender: AnyObject) {
        if(monthtf.text == "" || monthtf.text == nil) {
                self.NormalAlert(title: "Alert", Messsage: "Please Enter Month field")
        } else if(yeartf.text == "" || yeartf.text == nil) {
                self.NormalAlert(title: "Alert", Messsage: "Please Enter Year field")
        } else if(monthtf.text == "" || monthtf.text == nil && yeartf.text == "" || yeartf.text == nil) {
                self.NormalAlert(title: "Alert", Messsage: "Please Enter All fields")
        } else {
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: PasswordPopupViewController.instance())
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
            }
            let container = PasswordPopupViewController.instance()
            container.closeHandler = { _ in
                popup.dismiss()
            }
            popup.show(childViewController: container)
        }
    }
    
    @IBAction func CancelAction(_ sender: AnyObject) {
        Pickerview.isHidden = true
    }
    
    @IBAction func DoneBtnAction(_ sender: AnyObject) {
        if(currenttf == "year") {
            Pickerview.isHidden = true
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = DateFormatter.Style.long
            dateFormatter.dateFormat = "YYYY-MM"
            strDate = dateFormatter.string(from: DatePicker.date)
            DatePicker.datePickerMode = UIDatePickerMode.date
            let payment : [String] = strDate.components(separatedBy: "-")
            let paymentstr : String = payment[0]
            yeartf.text = paymentstr
            LibraryAPI.sharedInstance.payslipyear = paymentstr
            let paymentstr1 : String = payment[1]
            let datestr2 : String = paymentstr1
            monthtf.text = datestr2
            LibraryAPI.sharedInstance.payslipmonth = datestr2
        } else if(currenttf == "Month") {
            Pickerview.isHidden = true
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = DateFormatter.Style.long
            dateFormatter.dateFormat = "YYYY-MM"
            strDate = dateFormatter.string(from: DatePicker.date)
            let payment : [String] = strDate.components(separatedBy: "-")
            let dateyear: String = payment[0]
            let paymentstr1 : String = payment[1]
            let datestr2 : String = paymentstr1
            monthtf.text = datestr2
            yeartf.text = dateyear
        }
    }
}


