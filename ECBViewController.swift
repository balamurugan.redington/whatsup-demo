//
//  ECBViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 26/05/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit
import KCFloatingActionButton

class ECBViewController: UIViewController,KCFloatingActionButtonDelegate,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource {
    
    @IBOutlet weak var mainview: UIView!
    @IBOutlet weak var Scrollview: UIScrollView!
    @IBOutlet weak var exceedlbl: UILabel!
    @IBOutlet weak var invoiceview: UIView!
    @IBOutlet weak var stockroomtf: UITextField!
    @IBOutlet weak var deliveryview: UIView!
    @IBOutlet weak var businesstableview: UITableView!
    @IBOutlet weak var invoicetitlelbl: UILabel!
    @IBOutlet weak var tableviewview: UIView!
    @IBOutlet weak var submitbtn: UIButton!
    @IBOutlet weak var samelblbzcode: UILabel!
    @IBOutlet weak var customercodetf: UITextField!
    @IBOutlet weak var businesscodetf: UITextField!
    @IBOutlet weak var billingamttf: UITextField!
    @IBOutlet weak var dispatchtf: UITextField!
    @IBOutlet weak var Picker: UIPickerView!
    @IBOutlet weak var PickerView: UIView!
    @IBOutlet weak var Topview: UIView!
    
    //invoice to
    @IBOutlet weak var Bottomview: UIView!
    @IBOutlet weak var custnameinvoicetf: UITextField!
    @IBOutlet weak var add1invoicetf: UITextField!
    @IBOutlet weak var add2invoicetf: UITextField!
    @IBOutlet weak var add3invoicetf: UITextField!
    @IBOutlet weak var cityinvoicetf: UITextField!
    @IBOutlet weak var stateinvoicetf: UITextField!
    @IBOutlet weak var pincodeinvoicetf: UITextField!
    
    //delivery to
    
    @IBOutlet weak var custnamedeliverytf: UITextField!
    @IBOutlet weak var add1deliverytf: UITextField!
    @IBOutlet weak var add2deliverytf: UITextField!
    @IBOutlet weak var add3deliverytf: UITextField!
    @IBOutlet weak var citydeliverytf: UITextField!
    @IBOutlet weak var statedeliverytf: UITextField!
    @IBOutlet weak var pincodedeliverytf: UITextField!
    
    
    var fab=KCFloatingActionButton()
    var CustomcodeCheck : String!
    var message: String!
    var referencenumber: String!
    var errormsg: String!
    var stockroom: String!
    var product1: String!
    var product2: String!
    var product3: String!
    var product4: String!
    var product5: String!
    var billamount: String!
    var Despatchmethod: String!
    var InvoiceCustomerName:String!
    var Invoiceaddress1 : String!
    var Invoiceaddress2 : String!
    var Invoiceaddress3 :  String!
    var city : String!
    var State : String!
    var Invoicepin : String!
    var DeliverName: String!
    var Deliveraddrss1: String!
    var Deliveraddrss2: String!
    var Deliveraddrss3 : String!
    var Delivercity  : String!
    var Deliverstate: String!
    var DeliveryPin: String!
    var Despatchmethod_out: String!
    var Deliveraddrss11_out: String!
    var Deliveraddrss21_out: String!
    var Deliveraddrss31_out: String!
    var Delivercity1_out:String!
    var Deliverstate1_out: String!
    var Stockroom1_out: String!
    var PINcode:String!
    
    var responsearray1=NSMutableArray()
    var pickerarray1=NSMutableArray()
    
    var responsearray=NSMutableArray()
    var pickerarray=NSMutableArray()
    
    var despatchresponsearray=NSMutableArray()
    var despatchpickerarray=NSMutableArray()
    var Movedup=Bool()
    
    
    var uploadfile=NSMutableDictionary()
    var jsonresult=NSDictionary()
    var tableviewresponsearray : NSMutableArray = []
    var stockroomresponsearray : NSMutableArray = []
    
    var selectedrow=Int()
    var currentpicker=String()
    var Arr : [String] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Scrollview.bounces = false
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationmethod), name: NSNotification.Name(rawValue: "GetNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ECBViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ECBViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.addDoneButtonOnKeyboard()
        self.invoiceview.layer.borderWidth = 0.5
        self.tableviewview.isHidden = true
        self.deliveryview.layer.borderWidth = 0.5
        deliveryview.layer.masksToBounds=true
        deliveryview.layer.cornerRadius=10.0
        invoiceview.layer.masksToBounds=true
        invoiceview.layer.cornerRadius=10.0
        submitbtn.layer.masksToBounds=true
        submitbtn.layer.cornerRadius=20.0
        Topview.layer.borderColor = UIColor(red:30/255.0, green:184/255.0, blue:120/255.0, alpha: 1.0).cgColor
        Topview.layer.borderWidth = 0.5
        Topview.layer.cornerRadius = 5.0
        self.customnavigation()
        pincodeinvoicetf.keyboardType = .numberPad
        pincodedeliverytf.keyboardType = .numberPad
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.layoutFAB()
        customercodetf.delegate = self
        stockroomtf.delegate = self
        businesscodetf.delegate = self
        billingamttf.delegate = self
        dispatchtf.delegate = self
        custnameinvoicetf.delegate = self
        add1invoicetf.delegate = self
        add2invoicetf.delegate = self
        add3invoicetf.delegate = self
        cityinvoicetf.delegate = self
        stateinvoicetf.delegate = self
        pincodeinvoicetf.delegate = self
        custnamedeliverytf.delegate = self
        add1deliverytf.delegate = self
        add2deliverytf.delegate = self
        add3deliverytf.delegate = self
        citydeliverytf.delegate = self
        statedeliverytf.delegate = self
        pincodedeliverytf.delegate = self
        self.Picker.delegate = self
        customercodetf.tag = 1
        stockroomtf.tag = 2
        businesscodetf.tag = 3
        billingamttf.tag = 4
        dispatchtf.tag = 5
        custnameinvoicetf.tag = 6
        add1invoicetf.tag = 7
        add2invoicetf.tag = 8
        add3invoicetf.tag = 9
        cityinvoicetf.tag = 10
        stateinvoicetf.tag = 11
        pincodeinvoicetf.tag = 12
        custnamedeliverytf.tag = 13
        add1deliverytf.tag = 14
        add2deliverytf.tag = 15
        add3deliverytf.tag = 16
        citydeliverytf.tag = 17
        statedeliverytf.tag = 18
        pincodedeliverytf.tag = 19
        bizcodeRec()
        despatchdetails()
        businesstableview.register(UINib(nibName: "BillingwithodCustomerCell", bundle: nil), forCellReuseIdentifier: "BillingwithodCustomerCell")
        billingamttf.addTarget(self, action: #selector(ECBViewController.textFieldDidBeginEditing), for: UIControlEvents.editingChanged)
        
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(billingamttf.text?.first == "0") {
            billingamttf.text = ""
        } else if(billingamttf.text == "0.0") {
            billingamttf.text = "0."
        } else if(billingamttf.text == ".") {
            billingamttf.text = "0."
        } else {
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        Scrollview.isScrollEnabled=true
        Scrollview.contentSize = CGSize(width: Scrollview.frame.size.width, height:1100)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320,height: 50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(ECBViewController.doneButtonAction))
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        doneToolbar.items = items as AnyObject as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        billingamttf.inputAccessoryView = doneToolbar
        pincodeinvoicetf.inputAccessoryView = doneToolbar
        pincodedeliverytf.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction() {
        billingamttf.resignFirstResponder()
        pincodeinvoicetf.resignFirstResponder()
        pincodedeliverytf.resignFirstResponder()
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true) {
        } else {
            if(Movedup==false) {
                self.view.frame.origin.y -= 100
                Movedup=true
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true) {
        } else {
            self.view.frame.origin.y += 100
            Movedup=false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        PickerView.isHidden = true
        if CustomcodeCheck == nil || CustomcodeCheck == "" {
            self.customercodetf.text = ""
        } else {
            self.customercodetf.text = LibraryAPI.sharedInstance.CustomerCode
        }
        if referencenumber != nil {
            if referencenumber != "" {
                showToast(message: referencenumber)
            } else if message  != "" {
                showToast(message: message)
            } else if errormsg != "" {
                showToast(message: errormsg)
            }
            else
            {
                
            }
        }
        else
        {
        }
        if stockroom != nil
        {
            self.stockroomtf.text = stockroom
        }
        else
        {
            self.stockroomtf.text = ""
        }
        if billamount != nil
        {
            self.billingamttf.text = billamount
        }
        else
        {
            self.billingamttf.text = ""
            
        }
        
        if  InvoiceCustomerName != nil
        {
            self.custnameinvoicetf.text = InvoiceCustomerName
        }
        else
        {
            self.custnameinvoicetf.text = ""
        }
        
        if Invoiceaddress1 != nil
        {
            self.add1invoicetf.text = Invoiceaddress1
        }
        else
        {
            self.add1invoicetf.text = ""
            
        }
        if Invoiceaddress2 != nil
        {
            self.add2invoicetf.text = Invoiceaddress2
        }
        else
        {
            self.add2invoicetf.text = ""
            
        }
        if Invoiceaddress3 != nil
        {
            self.add3invoicetf.text = Invoiceaddress3
        }
        else
        {
            self.add3invoicetf.text = ""
            
        }
        
        if city != nil
        {
            self.cityinvoicetf.text = city
        }
        else
        {
            if LibraryAPI.sharedInstance.getCity != nil
            {
                self.cityinvoicetf.text = LibraryAPI.sharedInstance.getCity
            }
            else
            {
                self.cityinvoicetf.text = ""
                
            }
            
        }
        if State != nil  {
            self.stateinvoicetf.text = State
        } else {
            self.stateinvoicetf.text = ""
        }
        
        if PINcode != nil {
            self.pincodeinvoicetf.text = PINcode
        } else {
            self.pincodeinvoicetf.text = ""
        }
        
        if DeliverName != nil {
            self.custnamedeliverytf.text = DeliverName
        } else {
            self.custnamedeliverytf.text = ""
        }
        
        if Deliveraddrss1 != nil {
            self.add1deliverytf.text = Deliveraddrss1
        } else {
            self.add1deliverytf.text = ""
        }
        
        if Deliveraddrss2 != nil {
            self.add2deliverytf.text = Deliveraddrss2
        } else {
            self.add2deliverytf.text = ""
        }
        
        if Deliveraddrss3 != nil {
            self.add3deliverytf.text = Deliveraddrss3
        } else {
            self.add2deliverytf.text = ""
        }
        
        
        if Delivercity != nil {
            self.citydeliverytf.text = Delivercity
        } else {
            self.citydeliverytf.text = ""
        }
        
        if Deliverstate != nil {
            self.statedeliverytf.text = Deliverstate
        } else {
            self.statedeliverytf.text = ""
        }
        
        if DeliveryPin != nil{
            self.pincodedeliverytf.text = DeliveryPin
        } else {
            self.pincodedeliverytf.text = ""
        }
        
        if Despatchmethod != nil {
            self.dispatchtf.text = Despatchmethod
        } else {
            dispatchtf.text = ""
        }
    }
    
    func removeSpecialCharsFromString(_ text: String) -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-*=(),.:!_")
        return String(text.filter {okayChars.contains($0) })
    }
    
    func defaulfCheck()
    {
        switch self.tableviewresponsearray.count
        {
        case 0:
            
            self.uploadfile.setObject("", forKey: "Product1" as NSCopying)
            self.uploadfile.setObject("", forKey: "Product2" as NSCopying)
            self.uploadfile.setObject("", forKey: "Product3" as NSCopying)
            self.uploadfile.setObject("", forKey: "Product4" as NSCopying)
            self.uploadfile.setObject("", forKey: "Product5" as NSCopying)
            break
            
        case 1:
            
            let fullName : String = self.tableviewresponsearray.object(at: 0) as! String
            let fullNameArr : [String] = fullName.components(separatedBy: " - ")
            let firstName : String = fullNameArr[1]
            self.uploadfile.setObject(firstName, forKey: "Product1"  as NSCopying)
            self.uploadfile.setObject("", forKey: "Product2"  as NSCopying)
            self.uploadfile.setObject("", forKey: "Product3"  as NSCopying)
            self.uploadfile.setObject("", forKey: "Product4" as NSCopying)
            self.uploadfile.setObject("", forKey: "Product5" as NSCopying)
            break
            
        case 2:
            
            let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
            let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
            let firstName1 : String = fullNameArr1[1]
            
            let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
            let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
            let firstName2 : String = fullNameArr2[1]
            
            self.uploadfile.setObject(firstName1, forKey: "Product1" as NSCopying)
            self.uploadfile.setObject(firstName2, forKey: "Product2" as NSCopying)
            self.uploadfile.setObject("", forKey: "Product3" as NSCopying)
            self.uploadfile.setObject("", forKey: "Product4" as NSCopying)
            self.uploadfile.setObject("", forKey: "Product5" as NSCopying)
            break
            
        case 3:
            
            let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
            let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
            let firstName1 : String = fullNameArr1[1]
            
            let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
            let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
            let firstName2 : String = fullNameArr2[1]
            
            let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
            let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
            let firstName3 : String = fullNameArr3[1]
            
            self.uploadfile.setObject(firstName1, forKey: "Product1"  as NSCopying)
            self.uploadfile.setObject(firstName2, forKey: "Product2"  as NSCopying)
            self.uploadfile.setObject(firstName3, forKey: "Product3" as NSCopying)
            self.uploadfile.setObject("", forKey: "Product4" as NSCopying)
            self.uploadfile.setObject("", forKey: "Product5" as NSCopying)
            break
            
        case 4:
            
            let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
            let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
            let firstName1 : String = fullNameArr1[1]
            
            let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
            let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
            let firstName2 : String = fullNameArr2[1]
            
            let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
            let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
            let firstName3 : String = fullNameArr3[1]
            
            let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
            let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
            let firstName4 : String = fullNameArr4[1]
            
            self.uploadfile.setObject(firstName1, forKey: "Product1" as NSCopying)
            self.uploadfile.setObject(firstName2, forKey: "Product2" as NSCopying)
            self.uploadfile.setObject(firstName3, forKey: "Product3" as NSCopying)
            self.uploadfile.setObject(firstName4, forKey: "Product4" as NSCopying)
            self.uploadfile.setObject("", forKey: "Product5" as NSCopying)
            break
            
        case 5:
            
            let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
            let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
            let firstName1 : String = fullNameArr1[1]
            
            let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
            let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
            let firstName2 : String = fullNameArr2[1]
            
            let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
            let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
            let firstName3 : String = fullNameArr3[1]
            
            let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
            let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
            let firstName4 : String = fullNameArr4[1]
            
            let fullName5 : String = self.tableviewresponsearray.object(at: 4) as! String
            let fullNameArr5 : [String] = fullName5.components(separatedBy: " - ")
            let firstName5 : String = fullNameArr5[1]
            
            self.uploadfile.setObject(firstName1, forKey: "Product1" as NSCopying)
            self.uploadfile.setObject(firstName2, forKey: "Product2" as NSCopying)
            self.uploadfile.setObject(firstName3, forKey: "Product3" as NSCopying)
            self.uploadfile.setObject(firstName4, forKey: "Product4" as NSCopying)
            self.uploadfile.setObject(firstName5, forKey: "Product5" as NSCopying)
            break
            
        default:
            break
        }
        
        self.uploadfile.setObject(self.customercodetf.text!, forKey: "Customercode" as NSCopying)
        self.uploadfile.setObject(LibraryAPI.sharedInstance.Userid.stringByRemovingWhitespaces, forKey: "User" as NSCopying)
        
        let stockroom : [String] = self.stockroomtf.text!.components(separatedBy: " - ")
        let stockroomstr : String = stockroom[0]
        self.uploadfile.setObject(stockroomstr.stringByRemovingWhitespaces, forKey: "Stockroom" as NSCopying)
        
        let despatch : [String] = self.dispatchtf.text!.components(separatedBy: " - ")
        let despatchstr : String = despatch[0]
        
        if despatchstr == "8"
        {
            self.uploadfile.setObject("Y", forKey: "flag" as NSCopying)
        }
        else
        {
            self.uploadfile.setObject("", forKey: "flag" as NSCopying)
        }
        
        let txt=custnameinvoicetf.text
        self.removeSpecialCharsFromString(txt!)
        let trimmedString = txt!.trimmingCharacters(in: .whitespaces)
        self.uploadfile.setObject(despatchstr.stringByRemovingWhitespaces, forKey: "Despatchmethod" as NSCopying)
        self.uploadfile.setObject(self.billingamttf.text!, forKey: "BillAmount"  as NSCopying)
        self.uploadfile.setObject(custnameinvoicetf.text!, forKey: "InvoiceCustomerName"  as NSCopying)
        self.uploadfile.setObject(self.add1invoicetf.text!, forKey: "Invoiceaddress1" as NSCopying)
        self.uploadfile.setObject(self.add2invoicetf.text!, forKey: "Invoiceaddress2" as NSCopying)
        self.uploadfile.setObject(self.add3deliverytf.text!, forKey: "Invoiceaddress3" as NSCopying)
        self.uploadfile.setObject(self.cityinvoicetf.text!, forKey: "City" as NSCopying)
        self.uploadfile.setObject(self.stateinvoicetf.text!, forKey: "State" as NSCopying)
        self.uploadfile.setObject(self.pincodeinvoicetf.text!, forKey: "Invoicepin" as NSCopying)
        self.uploadfile.setObject(self.custnamedeliverytf.text!, forKey: "DeliverName" as NSCopying)
        self.uploadfile.setObject(self.add1deliverytf.text!, forKey: "Deliveraddrss1" as NSCopying)
        self.uploadfile.setObject(self.add2deliverytf.text!, forKey: "Deliveraddrss2" as NSCopying)
        self.uploadfile.setObject(self.add3deliverytf.text!, forKey: "Deliveraddrss3" as NSCopying)
        self.uploadfile.setObject(self.citydeliverytf.text!, forKey: "Delivercity" as NSCopying)
        self.uploadfile.setObject(self.statedeliverytf.text!, forKey: "Deliverstate" as NSCopying)
        self.uploadfile.setObject(self.pincodedeliverytf.text!, forKey: "DeliveryPin" as NSCopying)
        self.uploadfile.setObject("", forKey: "Submit" as NSCopying)
        
        
        self.uploaddata()
        if self.jsonresult.count > 0
        {
            self.stockroomtf.resignFirstResponder()
            self.custnameinvoicetf.text = self.jsonresult.object(forKey: "InvoiceCustomerName") as?String
            self.add1invoicetf.text = (self.jsonresult.object(forKey: "Invoiceaddress1")as! String)
            self.add2invoicetf.text = (self.jsonresult.object(forKey: "Invoiceaddress2")as! String)
            self.add3invoicetf.text = (self.jsonresult.object(forKey: "Invoiceaddress3")as! String)
            self.cityinvoicetf.text = (self.jsonresult.object(forKey: "City")as! String)
            self.stateinvoicetf.text = (self.jsonresult.object(forKey: "InvoiceState_out")as! String)
            self.pincodeinvoicetf.text = (self.jsonresult.object(forKey: "Invoicepin")as! String)
            self.custnamedeliverytf.text = (self.jsonresult.object(forKey: "DeliverName")as! String)
            self.add1deliverytf.text = (self.jsonresult.object(forKey: "Deliveraddrss11_out")as! String)
            self.add2deliverytf.text = (self.jsonresult.object(forKey: "Deliveraddrss21_out") as! String)
            self.add3deliverytf.text = (self.jsonresult.object(forKey: "Deliveraddrss31_out") as! String)
            self.citydeliverytf.text = self.jsonresult.object(forKey: "Delivercity1_out") as? String
            self.statedeliverytf.text = (self.jsonresult.object(forKey: "Deliverstate1_out") as! String)
            self.pincodedeliverytf.text = (self.jsonresult.object(forKey: "PINcode") as! String)
            
            for word1 in self.despatchresponsearray
            {
                let despatch = (self.jsonresult.object(forKey: "Despatchmethod_out") as! String)
                
                let dic : NSDictionary = word1 as! NSDictionary
                let code = dic.object(forKey: "Mode") as! String
                let name = dic.object(forKey: "Detail") as! String
                if code == despatch
                {
                    self.dispatchtf.text = code + " - " + name
                }
            }
            
            let errormessage = self.jsonresult.object(forKey: "ErrorMessage")
            let message      = self.jsonresult.object(forKey: "Message")
            let custbillnumber    = self.jsonresult.object(forKey: "Customerbillingnumber")
            
            
            if (custbillnumber as? String) != ""
            {
                self.showToast(message: custbillnumber as! String)
            }
            else if (message as? String) != ""
            {
                self.showToast(message: message as! String)
            }
            else if(errormessage as? String) != ""
            {
                self.showToast(message: errormessage as! String)
            }
            else
            {
                
            }
            
        }
    }
   
    func defaultMethod()
    {
        if city != nil
        {
            if city != ""
            {
                self.cityinvoicetf.text = city
                defaulfCheck()
            } else {
                if LibraryAPI.sharedInstance.getCity != nil
                {
                    self.cityinvoicetf.text = LibraryAPI.sharedInstance.getCity
                    defaulfCheck()
                } else {
                    self.cityinvoicetf.text = ""
                }
            }
        } else {
            if LibraryAPI.sharedInstance.getCity != nil
            {
                self.cityinvoicetf.text = LibraryAPI.sharedInstance.getCity
                defaulfCheck()
            } else {
                self.cityinvoicetf.text = ""
            }
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            self.fab.close()
        }
        fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
            let slideInFromLeftTransition = CATransition()
            slideInFromLeftTransition.type = kCAGravityTop
            slideInFromLeftTransition.subtype = kCATransitionFromLeft
            slideInFromLeftTransition.duration = 0.2
            slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "ECBViewController")), animated: true)
            self.fab.close()
        }
        
        
        fab.fabDelegate = self
        
        self.view.addSubview(fab)
    }
    func KCFABOpened(fab: KCFloatingActionButton) {
    }
    func customnavigation()
    {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: 65)
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(ECBViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "End Cust BillNumber Req"
    }
    
    func buttonClicked(sender:UIButton)
    {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "PreorderMainViewController")), animated: true)
    }
    
    
    //textfield delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //fab.hidden=false
        textField.resignFirstResponder()
        if  (textField.tag == 4)
        {
            self.exceedlbl.isHidden = true
            self.samelblbzcode.isHidden = true
            self.defaulfCheck()
        } else if  (textField.tag == 6)  {
            self.exceedlbl.isHidden = true
            self.samelblbzcode.isHidden = true
            self.defaulfCheck()
        } else if  (textField.tag == 11) {
            self.exceedlbl.isHidden = true
            self.samelblbzcode.isHidden = true
            self.defaulfCheck()
        } else if  (textField.tag == 12) {
            self.exceedlbl.isHidden = true
            self.samelblbzcode.isHidden = true
            self.defaulfCheck()
        } else if  (textField.tag == 13) {
            self.exceedlbl.isHidden = true
            self.samelblbzcode.isHidden = true
            self.defaulfCheck()
        } else if  (textField.tag == 17) {
            self.exceedlbl.isHidden = true
            self.samelblbzcode.isHidden = true
            self.defaulfCheck()
        } else if  (textField.tag == 18) {
            self.exceedlbl.isHidden = true
            self.samelblbzcode.isHidden = true
            self.defaulfCheck()
        } else if  (textField.tag == 19) {
            self.exceedlbl.isHidden = true
            self.samelblbzcode.isHidden = true
            self.defaulfCheck()
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if (textField.tag == 1) {
            LibraryAPI.sharedInstance.Currentcustomerlookup="ECB"
            if #available(iOS 10.0, *) {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: CustomerLookupViewController.instance())
                    .didShowHandler { popup in
                        self.customercodetf.resignFirstResponder()
                    }
                    .didCloseHandler { _ in
                }
                let container = CustomerLookupViewController.instance()
                container.closeHandler =  { _ in
                        popup.dismiss()
                }
                popup.show(childViewController: container)
            }
            return false
        } else if (textField.tag == 2) {
            PickerView.isHidden = false
            bizcodeRec1()
            return false
        } else if (textField.tag == 3) {
            let size = CGSize(width: 50, height:50)
            self.local_activityIndicator_start()
            self.exceedlbl.isHidden = true
            self.samelblbzcode.isHidden = true
            let alert:UIAlertController=UIAlertController(title: "Select Business Code", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            var alert1 = UIAlertAction()
            if pickerarray.count != 0 {
                self.local_activityIndicator_stop()
                for word in pickerarray {
                    alert1 = UIAlertAction(title: (word as! String), style: UIAlertActionStyle.default) {
                        UIAlertAction in
                        let getString : String = word as! String
                        if self.tableviewresponsearray.count < 5 {
                            if self.tableviewresponsearray.count == 0 {
                                self.tableviewresponsearray.add(getString)
                                self.Arr.append(getString)
                                self.businesstableview.reloadData()
                            } else {
                                let contains = self.tableviewresponsearray.contains {
                                    $0 as? String == getString
                                }
                                
                                if contains == false {
                                    self.tableviewresponsearray.add(getString)
                                    self.Arr.append(getString)
                                    self.businesstableview.reloadData()
                                } else  {
                                    self.samelblbzcode.isHidden = false
                                    self.exceedlbl.isHidden = true
                                    self.samelblbzcode.text = "You are Selected same Business Code"
                                }
                            }
                        } else {
                            self.exceedlbl.isHidden = false
                            self.samelblbzcode.isHidden = true
                            self.exceedlbl.text = "Alreay You have Selected 5 Business code"
                        }
                    }
                    alert.addAction(alert1)
                }
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                UIAlertAction in
            }
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
            return false
        } else if (textField.tag == 5) {
            let size = CGSize(width: 50, height:50)
            self.local_activityIndicator_start()
            let alert:UIAlertController=UIAlertController(title: "Select Despatch", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            var alert1 = UIAlertAction()
            if despatchpickerarray.count != 0
            {
                self.local_activityIndicator_stop()
                for word in despatchpickerarray {
                    alert1 = UIAlertAction(title: (word as! String), style: UIAlertActionStyle.default) {
                        UIAlertAction in
                        let getString : String = word as! String
                        self.dispatchtf.text = getString
                        self.dispatchtf.resignFirstResponder()
                        self.defaulfCheck()
                    }
                    alert.addAction(alert1)
                }
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
                UIAlertAction in
            }
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
            return false
        }
        
        if (textField.tag == 10)
        {
            LibraryAPI.sharedInstance.Currentcustomerlookup="City"
            if #available(iOS 10.0, *) {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: CustomerLookupViewController.instance())
                    .didShowHandler  { popup in
                        //self.Customercodetxtfield.resignFirstResponder()
                    }
                    .didCloseHandler { _ in
                        self.defaultMethod()
                }
                let container = CustomerLookupViewController.instance()
                container.closeHandler = { _ in
                        self.defaultMethod()
                        popup.dismiss()
                }
                popup.show(childViewController: container)
            }
            self.defaulfCheck()
            return false
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField.tag == 4) {
            let oldString = textField.text?.count == 0 ? string  : textField.text!
            let i = oldString.count
            let range = oldString.index(oldString.startIndex, offsetBy: i-1) ..< oldString.index(oldString.startIndex, offsetBy: i)
            let candidate = oldString.replacingCharacters(in: range, with: string)
            let regex = try? NSRegularExpression(pattern: "^\\d{0,8}(\\.\\d{0,2})?$", options: [])
            return regex?.firstMatch(in: candidate, options: [], range: NSRange(location: 0, length: candidate.count)) != nil
        }
        if(textField.tag == 6) {
            if string.rangeOfCharacter(from: .letters) != nil {
                guard let text = textField.text else { return true }
                let newLength = text.count + string.count - range.length
                return newLength  <= 20
            } else {
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
                return false
            }
        }
        if(textField.tag == 7)
        {
            if let value = string.rangeOfCharacter(from: .letters) {
                guard let text = textField.text else { return true }
                let newLength = text.count + string.count - range.length
                return newLength  <= 25
            } else {
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
                return false
            }
        }
        if(textField.tag == 8) {
            if let value = string.rangeOfCharacter(from: .letters){
                guard let text = textField.text else { return true }
                let newLength = text.count + string.count - range.length
                return newLength  <= 25
            } else  {
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
                return false
            }
        }
        if(textField.tag == 9) {
            if let value = string.rangeOfCharacter(from: .letters){
                guard let text = textField.text else { return true }
                let newLength = text.count + string.count - range.length
                return newLength  <= 25
            } else  {
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
                return false
            }
        }
        if(textField.tag==11)
        {
            if let value = string.rangeOfCharacter(from: .letters){
                guard let text = textField.text else { return true }
                let newLength = text.count + string.count - range.length
                return newLength  <= 20
            }
            else
            {
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
                return false
            }
        }
        if(textField.tag == 12) {
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 6
        }
        if(textField.tag == 13) {
            if let value = string.rangeOfCharacter(from: .letters) {
                guard let text = textField.text else { return true }
                let newLength = text.count + string.count - range.length
                return newLength  <= 20
                
            } else {
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
                return false
            }
        }
        if(textField.tag == 14) {
            if let value = string.rangeOfCharacter(from: .letters) {
                guard let text = textField.text else { return true }
                let newLength = text.count + string.count - range.length
                return newLength  <= 25
            } else  {
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
                return false
            }
        }
        if(textField.tag == 15) {
            if let value = string.rangeOfCharacter(from: .letters) {
                guard let text = textField.text else { return true }
                let newLength = text.count + string.count - range.length
                return newLength  <= 25
                
            } else {
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
                return false
            }
        }
        if(textField.tag == 16)
        {
            if let value = string.rangeOfCharacter(from: .letters) {
                guard let text = textField.text else { return true }
                let newLength = text.count + string.count - range.length
                return newLength  <= 25
            } else {
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
                return false
            }
        }
        if(textField.tag == 17) {
            if let value = string.rangeOfCharacter(from: .letters) {
                guard let text = textField.text else { return true }
                let newLength = text.count + string.count - range.length
                return newLength  <= 25
            } else {
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
                return false
            }
        }
        
        if(textField.tag == 18) {
            if let value = string.rangeOfCharacter(from: .letters) {
                guard let text = textField.text else { return true }
                let newLength = text.count + string.count - range.length
                return newLength  <= 25
            }  else {
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
                return false
            }
        }
        if(textField.tag==19) {
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 6
        }
        return true
    }
    
    func bizcodeRec1()
    {
        selectedrow = 0
        self.currentpicker = "stockroom"
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
            self.Loaddata1(url: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/CustomerStockRoom?Userid=\(LibraryAPI.sharedInstance.Userid)")
            DispatchQueue.main.async {
                self.Picker.reloadAllComponents()
                self.local_activityIndicator_stop()
                self.PickerView.isHidden=false
            }
        }
        
    }
    func bizcodeRec()
    {
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)StockDetails.asmx/BusinessAccessDetails?UserID=\(LibraryAPI.sharedInstance.Userid)", completion: { (jsonmutable) in
                self.responsearray = jsonmutable
                DispatchQueue.main.async {
                    if(self.responsearray.count != 0) {
                        self.pickerarray.removeAllObjects()
                        self.local_activityIndicator_stop()
                        for word in self.responsearray {
                            let dic = word as! NSDictionary
                            let code = dic.object(forKey: "BUSINESS CODE") as! String
                            let desc = dic.object(forKey: "BUSINESS DESC") as! String
                            let str = code + " - " + desc
                            self.pickerarray.add(str)
                        }
                    }
                    else
                    {
                        self.local_activityIndicator_stop()
                    }
                }
            })
        }
        
    }
    
    func despatchdetails()
    {
        let size = CGSize(width: 30, height:30)
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
            self.Loaddata2(url: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/FieldDescription?Userid=\(LibraryAPI.sharedInstance.Userid)&prmt=mode")
            DispatchQueue.main.async {
                self.local_activityIndicator_stop()
            }
        }
    }
    
    
    func Loaddata2(url:String)
    {
        self.getsuccess(params: [:], urlhit: url, completion: { (jsonmutable) in
            self.despatchresponsearray = jsonmutable
            if(self.despatchresponsearray.count > 0) {
                for i in 0  ..< self.despatchresponsearray.count  {
                    let dic = self.despatchresponsearray[i] as! NSDictionary
                    let code = dic.object(forKey: "Mode") as? String
                    let desc = dic.object(forKey: "Detail") as? String
                    let str = code?.appending(" - \(desc!)")
                    self.despatchpickerarray.add(str!)
                }
            }  else {
                self.showToast(message: "No Data Available")
            }
        })
    }
    
    func Loaddata1(url:String)
    {
        self.getsuccess(params: [:], urlhit: url, completion: { (jsonmutable) in
            self.responsearray1 = jsonmutable
            if(self.responsearray1.count > 0) {
            }  else  {
                self.showToast(message: "No Data Available")
            }
            self.pickerarray1.removeAllObjects()
            for i in 0  ..< self.responsearray1.count {
                let dic = self.responsearray1[i] as! NSDictionary
                let code = dic.object(forKey: "StockRoom Code") as? String
                let desc = dic.object(forKey: "StockRoom Name") as? String
                let str=code?.appending(" - \(desc!)")
                self.pickerarray1.add(str!)
            }
        })
    }
    
    func uploaddata()
    {
        let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)Users.asmx/ECBEntry")! as URL)
        request.httpMethod = "POST"
        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
        request.httpBody = try? JSONSerialization.data(withJSONObject: self.uploadfile, options: JSONSerialization.WritingOptions(rawValue: 0))
        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
        do
        {
            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
            jsonresult = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
        }
        catch (let e)
        {
            print(e)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if (self.Arr.count == 0)
        {
            businesstableview.frame = CGRect(x: 5, y: 152, width: self.businesstableview.frame.width, height: 0)
            mainview.frame = CGRect(x: 8, y: 57, width: self.mainview.frame.width, height: 1280)
            Topview.frame = CGRect(x: 8, y: 8, width: self.Topview.frame.width, height: self.Topview.frame.height)
            Bottomview.frame = CGRect(x: 8, y: 159, width: self.Bottomview.frame.width, height: self.Bottomview.frame.height)
            businesstableview.isScrollEnabled = false
            Scrollview.isScrollEnabled=true
            self.Scrollview.contentSize = mainview.frame.size
            self.tableviewview.isHidden = true
            return 0
        } else {
            if (self.Arr.count == 1) {
                businesstableview.frame = CGRect(x: 5, y: 152, width: self.businesstableview.frame.width, height: 0+40)
                mainview.frame = CGRect(x: 8, y: 57, width: self.mainview.frame.width, height: 1280)
                Topview.frame = CGRect(x: 8, y: 8, width: self.Topview.frame.width, height: self.Topview.frame.height)
                Bottomview.frame = CGRect(x: 8, y: 159 + 40, width: self.Bottomview.frame.width, height: self.Bottomview.frame.height)
                self.tableviewview.isHidden = false
                businesstableview.isScrollEnabled = false
                self.Scrollview.contentSize = mainview.frame.size
            }  else if (self.Arr.count == 2)  {
                businesstableview.frame = CGRect(x: 5, y: 152, width: self.businesstableview.frame.width, height: 0+80)
                mainview.frame = CGRect(x: 8, y: 57, width: self.mainview.frame.width, height: 1280)
                Topview.frame = CGRect(x: 8, y: 8, width: self.Topview.frame.width, height: self.Topview.frame.height)
                Bottomview.frame = CGRect(x: 8, y: 159 + 80, width: self.Bottomview.frame.width, height: self.Bottomview.frame.height)
                self.tableviewview.isHidden = false
                businesstableview.isScrollEnabled = false
                self.Scrollview.contentSize = mainview.frame.size
            } else if (self.Arr.count == 3) {
                businesstableview.frame = CGRect(x: 5, y: 152, width: self.businesstableview.frame.width, height: 0 + 120)
                mainview.frame = CGRect(x: 8, y: 57, width: self.mainview.frame.width, height: 1280)
                Topview.frame = CGRect(x: 8, y: 8, width: self.Topview.frame.width, height: self.Topview.frame.height)
                Bottomview.frame = CGRect(x: 8, y: 159 + 120, width: self.Bottomview.frame.width, height: self.Bottomview.frame.height)
                self.tableviewview.isHidden = false
                businesstableview.isScrollEnabled = false
                self.Scrollview.contentSize = mainview.frame.size
            }  else if (self.Arr.count == 4) {
                businesstableview.frame = CGRect(x: 5, y: 152, width: self.businesstableview.frame.width, height: 0 + 160)
                mainview.frame = CGRect(x: 8, y: 57, width: self.mainview.frame.width, height: 1280)
                Topview.frame = CGRect(x: 8, y: 8, width: self.Topview.frame.width, height: self.Topview.frame.height)
                Bottomview.frame = CGRect(x: 8, y: 159 + 160, width: self.Bottomview.frame.width, height: self.Bottomview.frame.height)
                self.tableviewview.isHidden = false
                businesstableview.isScrollEnabled = false
                self.Scrollview.contentSize = mainview.frame.size
            }  else if (self.Arr.count == 5) {
                businesstableview.frame = CGRect(x: 5, y: 152, width: self.businesstableview.frame.width, height: 0 + 200)
                mainview.frame = CGRect(x: 8, y: 57, width: self.mainview.frame.width, height: 1280)
                Topview.frame = CGRect(x: 8, y: 8, width: self.Topview.frame.width, height: self.Topview.frame.height)
                Bottomview.frame = CGRect(x: 8, y: 159 + 200, width: self.Bottomview.frame.width, height: self.Bottomview.frame.height)
                self.tableviewview.isHidden = false
                businesstableview.isScrollEnabled = false
                self.Scrollview.contentSize = mainview.frame.size
            } else {
                businesstableview.frame = CGRect(x: 5, y: 152, width: self.businesstableview.frame.width, height: 0)
                mainview.frame = CGRect(x: 8, y: 57, width: self.mainview.frame.width, height: 1280)
                Topview.frame = CGRect(x: 8, y: 8, width: self.Topview.frame.width, height: self.Topview.frame.height)
                Bottomview.frame = CGRect(x: 0, y: 125, width: self.Bottomview.frame.width, height: self.Bottomview.frame.height)
                self.tableviewview.isHidden = true
                businesstableview.isScrollEnabled = false
                self.Scrollview.contentSize = mainview.frame.size
            }
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableviewresponsearray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BillingwithodCustomerCell", for: indexPath) as! BillingwithodCustomerCell
        if(tableviewresponsearray.count > 0)  {
        } else {
        }
        cell.deletebtn.addTarget(self, action: #selector(self.deletebtnclicked), for: UIControlEvents.touchUpInside)
        cell.deletebtn.tag = indexPath.row
        let getString = tableviewresponsearray[indexPath.row] as? String
        self.uploadfile.setObject(self.customercodetf.text!, forKey: "Customercode" as NSCopying)
        self.uploadfile.setObject(LibraryAPI.sharedInstance.Userid.stringByRemovingWhitespaces, forKey: "User" as NSCopying)
        let stockroom : [String] = self.stockroomtf.text!.components(separatedBy:  " - " )
        let stockroomstr : String = stockroom[0]
        self.uploadfile.setObject(stockroomstr.stringByRemovingWhitespaces, forKey: "Stockroom" as NSCopying)
        let despatch : [String] = self.dispatchtf.text!.components(separatedBy: " - ")
        let despatchstr : String = despatch[0]
        self.uploadfile.setObject(despatchstr.stringByRemovingWhitespaces, forKey: "Despatchmethod" as NSCopying)
        self.uploadfile.setObject(self.billingamttf.text!, forKey: "BillAmount" as NSCopying)
        self.uploadfile.setObject(self.custnameinvoicetf.text!, forKey: "InvoiceCustomerName" as NSCopying)
        self.uploadfile.setObject(self.add1invoicetf.text!, forKey: "Invoiceaddress1" as NSCopying)
        self.uploadfile.setObject(self.add2invoicetf.text!, forKey: "Invoiceaddress2" as NSCopying)
        self.uploadfile.setObject(self.add3deliverytf.text!, forKey: "Invoiceaddress3" as NSCopying)
        self.uploadfile.setObject(self.cityinvoicetf.text!, forKey: "City" as NSCopying)
        self.uploadfile.setObject(self.stateinvoicetf.text!, forKey: "State" as NSCopying)
        self.uploadfile.setObject(self.pincodeinvoicetf.text!, forKey: "Invoicepin" as NSCopying)
        self.uploadfile.setObject(self.custnamedeliverytf.text!, forKey: "DeliverName" as NSCopying)
        self.uploadfile.setObject(self.add1deliverytf.text!, forKey: "Deliveraddrss1" as NSCopying)
        self.uploadfile.setObject(self.add2deliverytf.text!, forKey: "Deliveraddrss2" as NSCopying)
        self.uploadfile.setObject(self.add3deliverytf.text!, forKey: "Deliveraddrss3" as NSCopying)
        self.uploadfile.setObject(self.citydeliverytf.text!, forKey: "Delivercity" as NSCopying)
        self.uploadfile.setObject(self.statedeliverytf.text!, forKey: "Deliverstate" as NSCopying)
        self.uploadfile.setObject(self.pincodedeliverytf.text!, forKey: "DeliveryPin" as NSCopying)
        self.uploadfile.setObject("", forKey: "Submit" as NSCopying)
        self.uploadfile.setObject("", forKey: "flag" as NSCopying)
        switch self.tableviewresponsearray.count {
        case 0:
            self.uploadfile.setObject("", forKey: "Product1" as NSCopying)
            self.uploadfile.setObject("", forKey: "Product2" as NSCopying)
            self.uploadfile.setObject("", forKey: "Product3" as NSCopying)
            self.uploadfile.setObject("", forKey: "Product4" as NSCopying)
            self.uploadfile.setObject("", forKey: "Product5" as NSCopying)
            break
        case 1:
            let fullName : String = self.tableviewresponsearray.object(at: 0) as! String
            let fullNameArr : [String] = fullName.components(separatedBy: " - ")
            let firstName : String = fullNameArr[1]
            self.uploadfile.setObject(firstName, forKey: "Product1" as NSCopying)
            self.uploadfile.setObject("", forKey: "Product2" as NSCopying)
            self.uploadfile.setObject("", forKey: "Product3" as NSCopying)
            self.uploadfile.setObject("", forKey: "Product4" as NSCopying)
            self.uploadfile.setObject("", forKey: "Product5" as NSCopying)
            break
        case 2:
            let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
            let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
            let firstName1 : String = fullNameArr1[1]
            let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
            let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
            let firstName2 : String = fullNameArr2[1]
            self.uploadfile.setObject(firstName1, forKey: "Product1" as NSCopying)
            self.uploadfile.setObject(firstName2, forKey: "Product2" as NSCopying)
            self.uploadfile.setObject("", forKey: "Product3" as NSCopying)
            self.uploadfile.setObject("", forKey: "Product4" as NSCopying)
            self.uploadfile.setObject("", forKey: "Product5" as NSCopying)
            break
        case 3:
            let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
            let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
            let firstName1 : String = fullNameArr1[1]
            let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
            let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
            let firstName2 : String = fullNameArr2[1]
            let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
            let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
            let firstName3 : String = fullNameArr3[1]
            self.uploadfile.setObject(firstName1, forKey: "Product1" as NSCopying)
            self.uploadfile.setObject(firstName2, forKey: "Product2" as NSCopying)
            self.uploadfile.setObject(firstName3, forKey: "Product3" as NSCopying)
            self.uploadfile.setObject("", forKey: "Product4" as NSCopying)
            self.uploadfile.setObject("", forKey: "Product5" as NSCopying)
            break
        case 4:
            let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
            let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
            let firstName1 : String = fullNameArr1[1]
            let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
            let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
            let firstName2 : String = fullNameArr2[1]
            let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
            let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
            let firstName3 : String = fullNameArr3[1]
            let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
            let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
            let firstName4 : String = fullNameArr4[1]
            self.uploadfile.setObject(firstName1, forKey: "Product1" as NSCopying)
            self.uploadfile.setObject(firstName2, forKey: "Product2" as NSCopying)
            self.uploadfile.setObject(firstName3, forKey: "Product3" as NSCopying)
            self.uploadfile.setObject(firstName4, forKey: "Product4" as NSCopying)
            self.uploadfile.setObject("", forKey: "Product5" as NSCopying)
            break
        case 5:
            let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
            let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
            let firstName1 : String = fullNameArr1[1]
            let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
            let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
            let firstName2 : String = fullNameArr2[1]
            let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
            let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
            let firstName3 : String = fullNameArr3[1]
            let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
            let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
            let firstName4 : String = fullNameArr4[1]
            let fullName5 : String = self.tableviewresponsearray.object(at: 4) as! String
            let fullNameArr5 : [String] = fullName5.components(separatedBy: " - ")
            let firstName5 : String = fullNameArr5[1]
            self.uploadfile.setObject(firstName1, forKey: "Product1" as NSCopying)
            self.uploadfile.setObject(firstName2, forKey: "Product2" as NSCopying)
            self.uploadfile.setObject(firstName3, forKey: "Product3" as NSCopying)
            self.uploadfile.setObject(firstName4, forKey: "Product4" as NSCopying)
            self.uploadfile.setObject(firstName5, forKey: "Product5" as NSCopying)
            break
        default:
            break
        }
        uploaddata()
        if self.jsonresult.count > 0  {
            let fullNameArr5 : [String] = getString!.components(separatedBy: " - ")
            if(fullNameArr5.count >= 3)  {
                let bizcode : String = fullNameArr5[0]
                let bizname : String = fullNameArr5[1]
                let bizname1 : String = fullNameArr5[2]
                cell.bizcode.text = bizcode
                cell.bizname.text = bizname + " - " + bizname1
            } else {
                let bizcode : String = fullNameArr5[0]
                let bizname : String = fullNameArr5[1]
                cell.bizcode.text = bizcode
                cell.bizname.text = bizname
            }
            self.custnameinvoicetf.text = self.jsonresult.object(forKey: "InvoiceCustomerName") as?String
            self.add1invoicetf.text = (self.jsonresult.object(forKey: "Invoiceaddress1")as! String)
            self.add2invoicetf.text = (self.jsonresult.object(forKey: "Invoiceaddress2")as! String)
            self.add3invoicetf.text = (self.jsonresult.object(forKey: "Invoiceaddress3")as! String)
            self.cityinvoicetf.text = (self.jsonresult.object(forKey: "City")as! String)
            self.stateinvoicetf.text = (self.jsonresult.object(forKey: "InvoiceState_out")as! String)
            self.pincodeinvoicetf.text = (self.jsonresult.object(forKey: "Invoicepin")as! String)
            self.custnamedeliverytf.text = (self.jsonresult.object(forKey: "DeliverName")as! String)
            self.add1deliverytf.text = (self.jsonresult.object(forKey: "Deliveraddrss11_out")as! String)
            self.add2deliverytf.text = (self.jsonresult.object(forKey: "Deliveraddrss21_out") as! String)
            self.add3deliverytf.text = (self.jsonresult.object(forKey: "Deliveraddrss31_out") as! String)
            self.citydeliverytf.text = self.jsonresult.object(forKey: "Delivercity1_out") as? String
            self.statedeliverytf.text = (self.jsonresult.object(forKey: "Deliverstate1_out") as! String)
            self.pincodedeliverytf.text = (self.jsonresult.object(forKey: "PINcode") as! String)
            for word1 in self.despatchresponsearray {
                let despatch = (self.jsonresult.object(forKey: "Despatchmethod_out") as! String)
                let dic : NSDictionary = word1 as! NSDictionary
                let code = dic.object(forKey: "Mode") as! String
                let name = dic.object(forKey: "Detail") as! String
                if code == despatch {
                    self.dispatchtf.text = code + " - " + name
                }
            }
            let errormessage = self.jsonresult.object(forKey: "ErrorMessage")
            let message      = self.jsonresult.object(forKey: "Message")
            let Customerbillingnumber    = self.jsonresult.object(forKey: "Customerbillingnumber")
            if (errormessage as? String) != ""  {
                showToast(message: errormessage as! String)
            } else if (message as? String) != ""  {
                showToast(message: message as! String)
            }  else if(Customerbillingnumber as? String) != "" {
                showToast(message: Customerbillingnumber as! String)
            } else {
            }
        }
        else
        {
            let fullNameArr5 : [String] = getString!.components(separatedBy: " - ")
            if(fullNameArr5.count >= 3) {
                let bizcode : String = fullNameArr5[0]
                let bizname : String = fullNameArr5[1]
                let bizname1 : String = fullNameArr5[2]
                cell.bizcode.text = bizcode
                cell.bizname.text = bizname + " - " + bizname1
            }  else {
                let bizcode : String = fullNameArr5[0]
                let bizname : String = fullNameArr5[1]
                cell.bizcode.text = bizcode
                cell.bizname.text = bizname
            }
        }
        return cell
    }
    
    func deletebtnclicked(sender: UIButton) {
        let alertController = UIAlertController(title: "Business Code", message: "Are You Sure Want to Delete Business Code?", preferredStyle: .alert)
        let DefaultAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
            self.tableviewresponsearray.removeObject(at: sender.tag)
            self.Arr.remove(at: sender.tag)
            self.businesstableview.reloadData()
        }
        let CancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
        }
        alertController.addAction(DefaultAction)
        alertController.addAction(CancelAction)
        self.present(alertController, animated: true, completion:nil)
    }
    
    @IBAction func SubmitbuttonAction(_ sender: AnyObject)
    {
        if ((self.customercodetf.text!.isEmpty ) && (stockroomtf.text!.isEmpty) && (businesscodetf.text!.isEmpty) && (billingamttf.text!.isEmpty) && (dispatchtf.text!.isEmpty)) {
            self.NormalAlert(title: "Alert", Messsage: "Please Enter the all fields")
        } else {
            switch self.tableviewresponsearray.count {
            case 0:
                self.uploadfile.setObject("", forKey: "Product1" as NSCopying)
                self.uploadfile.setObject("", forKey: "Product2" as NSCopying)
                self.uploadfile.setObject("", forKey: "Product3" as NSCopying)
                self.uploadfile.setObject("", forKey: "Product4" as NSCopying)
                self.uploadfile.setObject("", forKey: "Product5" as NSCopying)
                break
            case 1:
                let fullName : String = self.tableviewresponsearray.object(at: 0) as! String
                let fullNameArr : [String] = fullName.components(separatedBy: " - ")
                let firstName : String = fullNameArr[1]
                self.uploadfile.setObject(firstName, forKey: "Product1" as NSCopying)
                self.uploadfile.setObject("", forKey: "Product2" as NSCopying)
                self.uploadfile.setObject("", forKey: "Product3" as NSCopying)
                self.uploadfile.setObject("", forKey: "Product4" as NSCopying)
                self.uploadfile.setObject("", forKey: "Product5" as NSCopying)
                break
            case 2:
                let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                let firstName1 : String = fullNameArr1[1]
                let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                let firstName2 : String = fullNameArr2[1]
                self.uploadfile.setObject(firstName1, forKey: "Product1"  as NSCopying)
                self.uploadfile.setObject(firstName2, forKey: "Product2" as NSCopying)
                self.uploadfile.setObject("", forKey: "Product3" as NSCopying)
                self.uploadfile.setObject("", forKey: "Product4" as NSCopying)
                self.uploadfile.setObject("", forKey: "Product5" as NSCopying)
                break
            case 3:
                let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                let firstName1 : String = fullNameArr1[1]
                let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                let firstName2 : String = fullNameArr2[1]
                let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                let firstName3 : String = fullNameArr3[1]
                self.uploadfile.setObject(firstName1, forKey: "Product1" as NSCopying)
                self.uploadfile.setObject(firstName2, forKey: "Product2" as NSCopying)
                self.uploadfile.setObject(firstName3, forKey: "Product3" as NSCopying)
                self.uploadfile.setObject("", forKey: "Product4" as NSCopying)
                self.uploadfile.setObject("", forKey: "Product5" as NSCopying)
                break
            case 4:
                let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                let firstName1 : String = fullNameArr1[1]
                let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                let firstName2 : String = fullNameArr2[1]
                let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                let firstName3 : String = fullNameArr3[1]
                let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
                let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
                let firstName4 : String = fullNameArr4[1]
                self.uploadfile.setObject(firstName1, forKey: "Product1" as NSCopying)
                self.uploadfile.setObject(firstName2, forKey: "Product2" as NSCopying)
                self.uploadfile.setObject(firstName3, forKey: "Product3" as NSCopying)
                self.uploadfile.setObject(firstName4, forKey: "Product4" as NSCopying)
                self.uploadfile.setObject("", forKey: "Product5" as NSCopying)
                break
            case 5:
                let fullName1 : String = self.tableviewresponsearray.object(at: 0) as! String
                let fullNameArr1 : [String] = fullName1.components(separatedBy: " - ")
                let firstName1 : String = fullNameArr1[1]
                let fullName2 : String = self.tableviewresponsearray.object(at: 1) as! String
                let fullNameArr2 : [String] = fullName2.components(separatedBy: " - ")
                let firstName2 : String = fullNameArr2[1]
                let fullName3 : String = self.tableviewresponsearray.object(at: 2) as! String
                let fullNameArr3 : [String] = fullName3.components(separatedBy: " - ")
                let firstName3 : String = fullNameArr3[1]
                let fullName4 : String = self.tableviewresponsearray.object(at: 3) as! String
                let fullNameArr4 : [String] = fullName4.components(separatedBy: " - ")
                let firstName4 : String = fullNameArr4[1]
                let fullName5 : String = self.tableviewresponsearray.object(at: 4) as! String
                let fullNameArr5 : [String] = fullName5.components(separatedBy: " - ")
                let firstName5 : String = fullNameArr5[1]
                self.uploadfile.setObject(firstName1, forKey: "Product1" as NSCopying)
                self.uploadfile.setObject(firstName2, forKey: "Product2" as NSCopying)
                self.uploadfile.setObject(firstName3, forKey: "Product3" as NSCopying)
                self.uploadfile.setObject(firstName4, forKey: "Product4" as NSCopying)
                self.uploadfile.setObject(firstName5, forKey: "Product5" as NSCopying)
                break
            default:
                break
            }
            self.uploadfile.setObject(self.customercodetf.text!, forKey: "Customercode" as NSCopying)
            self.uploadfile.setObject(LibraryAPI.sharedInstance.Userid.stringByRemovingWhitespaces, forKey: "User" as NSCopying)
            let stockroom : [String] = self.stockroomtf.text!.components(separatedBy: " - ")
            let stockroomstr : String = stockroom[0]
            self.uploadfile.setObject(stockroomstr.stringByRemovingWhitespaces, forKey: "Stockroom" as NSCopying)
            let despatch : [String] = self.dispatchtf.text!.components(separatedBy: " - ")
            let despatchstr : String = despatch[0]
            self.uploadfile.setObject(despatchstr.stringByRemovingWhitespaces, forKey: "Despatchmethod" as NSCopying)
            self.uploadfile.setObject(self.billingamttf.text!, forKey: "BillAmount" as NSCopying)
            self.uploadfile.setObject(self.custnameinvoicetf.text!, forKey: "InvoiceCustomerName" as NSCopying)
            self.uploadfile.setObject(self.add1invoicetf.text!, forKey: "Invoiceaddress1" as NSCopying)
            self.uploadfile.setObject(self.add2invoicetf.text!, forKey: "Invoiceaddress2" as NSCopying)
            self.uploadfile.setObject(self.add3deliverytf.text!, forKey: "Invoiceaddress3" as NSCopying)
            self.uploadfile.setObject(self.cityinvoicetf.text!, forKey: "City" as NSCopying)
            self.uploadfile.setObject(self.stateinvoicetf.text!, forKey: "State" as NSCopying)
            self.uploadfile.setObject(self.pincodeinvoicetf.text!, forKey: "Invoicepin" as NSCopying)
            self.uploadfile.setObject(self.custnamedeliverytf.text!, forKey: "DeliverName" as NSCopying)
            self.uploadfile.setObject(self.add1deliverytf.text!, forKey: "Deliveraddrss1" as NSCopying)
            self.uploadfile.setObject(self.add2deliverytf.text!, forKey: "Deliveraddrss2" as NSCopying)
            self.uploadfile.setObject(self.add3deliverytf.text!, forKey: "Deliveraddrss3" as NSCopying)
            self.uploadfile.setObject(self.citydeliverytf.text!, forKey: "Delivercity" as NSCopying)
            self.uploadfile.setObject(self.statedeliverytf.text!, forKey: "Deliverstate" as NSCopying)
            self.uploadfile.setObject(self.pincodedeliverytf.text!, forKey: "DeliveryPin" as NSCopying)
            self.uploadfile.setObject("y", forKey: "Submit" as NSCopying)
            self.uploadfile.setObject("", forKey: "flag" as NSCopying)
            self.uploaddata()
            if self.jsonresult.count > 0 {
                self.stockroomtf.resignFirstResponder()
                self.custnameinvoicetf.text = self.jsonresult.object(forKey: "InvoiceCustomerName") as?String
                self.add1invoicetf.text = (self.jsonresult.object(forKey: "Invoiceaddress1")as! String)
                self.add2invoicetf.text = (self.jsonresult.object(forKey: "Invoiceaddress2")as! String)
                self.add3invoicetf.text = (self.jsonresult.object(forKey: "Invoiceaddress3")as! String)
                self.cityinvoicetf.text = (self.jsonresult.object(forKey: "City")as! String)
                self.stateinvoicetf.text = (self.jsonresult.object(forKey: "InvoiceState_out")as! String)
                self.pincodeinvoicetf.text = (self.jsonresult.object(forKey: "Invoicepin")as! String)
                self.custnamedeliverytf.text = (self.jsonresult.object(forKey: "DeliverName")as! String)
                self.add1deliverytf.text = (self.jsonresult.object(forKey: "Deliveraddrss11_out")as! String)
                self.add2deliverytf.text = (self.jsonresult.object(forKey: "Deliveraddrss21_out") as! String)
                self.add3deliverytf.text = (self.jsonresult.object(forKey: "Deliveraddrss31_out") as! String)
                self.citydeliverytf.text = self.jsonresult.object(forKey: "Delivercity1_out") as? String
                self.statedeliverytf.text = (self.jsonresult.object(forKey: "Deliverstate1_out") as! String)
                self.pincodedeliverytf.text = (self.jsonresult.object(forKey: "PINcode") as! String)
                for word1 in self.despatchresponsearray  {
                    let despatch = (self.jsonresult.object(forKey: "Despatchmethod_out") as! String)
                    let dic : NSDictionary = word1 as! NSDictionary
                    let code = dic.object(forKey: "Mode") as! String
                    let name = dic.object(forKey: "Detail") as! String
                    if code == despatch  {
                        self.dispatchtf.text = code + " - " + name
                    }
                }
                let errormessage = self.jsonresult.object(forKey: "ErrorMessage")
                let message      = self.jsonresult.object(forKey: "Message")
                let custbillnumber    = self.jsonresult.object(forKey: "Customerbillingnumber")
                if (custbillnumber as? String) != ""  {
                    let alertController = UIAlertController(title: "Message", message:"End Customer Billing Number :               \(custbillnumber!)" , preferredStyle: .alert)
                    let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                        self.navigate()
                    }
                    alertController.addAction(cancelAction)
                    self.present(alertController, animated: true, completion:nil)
                } else if (message as? String) != "" {
                    self.showToast(message: message as! String)
                }  else if(errormessage as? String) != "" {
                    self.showToast(message: errormessage as! String)
                } else {
                }
            }
        }
    }
    
    
    func navigate() {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var num = Int()
        num=pickerarray1.count
        return num
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return  pickerarray1.object(at: row) as? String
        
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedrow = row
    }
    
    @IBAction func CancelAction(_ sender: AnyObject)
    {
        PickerView.isHidden=true
    }
    
    @IBAction func DoneAction(_ sender: AnyObject)
    {
        if(currentpicker=="stockroom") {
            stockroomtf.text=pickerarray1.object(at: selectedrow) as? String
            self.defaulfCheck()
            PickerView.isHidden=true
        }
        
    }
    
    
    func notificationmethod()
    {
        let userdefaults = Foundation.UserDefaults.standard
        let value = userdefaults.dictionary(forKey: "Notification")
        
        var title: String!
        var message: String!
        var imagestr: String!
        
        title = value?["title"] as! String
        message = value?["body"] as! String
        imagestr = value?["imageurl"] as! String
        
        if imagestr != nil
        {
            if imagestr != ""
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: notificationimageViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = notificationimageViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                    
                }
                popup.show(childViewController: container)
            }
            else
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: PushnotificationtextViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = PushnotificationtextViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                }
                
                popup.show(childViewController: container)
            }
        }
        else
        {
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: PushnotificationtextViewController.instance())
                
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
            }
            let container = PushnotificationtextViewController.instance()
            container.closeHandler = { _ in
                popup.dismiss()
            }
            
            popup.show(childViewController: container)
        }
    }
}
