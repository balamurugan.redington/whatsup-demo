//
//  ChequeBounceBranchdetailsViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 14/07/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class ChequeBounceBranchdetailsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var BranchTableview: UITableView!
    @IBOutlet weak var allbranchbtn: UIButton!
    var responsestring = NSMutableArray()
    var merge = String()
    var closeHandler: (() -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
  print("Cheque pending details  ChequeBounceBranchdetailsViewController")
        BranchTableview.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        BranchTableview.delegate = self
        BranchTableview.dataSource = self
        
       

    }
    override func viewWillAppear(_ animated: Bool) {
        self.local_activityIndicator_start()
         self.loaddata()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    class func instance() -> ChequeBounceBranchdetailsViewController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "ChequeBounceBranchdetailsViewController") as! ChequeBounceBranchdetailsViewController
    }
    
    @IBAction func CloseAction(_ sender: AnyObject)
    {
        closeHandler?()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return responsestring.count
    }
       
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let dic = responsestring[indexPath.row] as! NSDictionary
        let BranchCode = dic.object(forKey: "BranchCode") as! String
        let BranchName =  dic.object(forKey: "BranchName") as! String
        merge = BranchCode + "-" + BranchName
        // Partnerprofile!+" "+"-"+profiledesc!+" "
        cell.textLabel?.text = merge
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dic1 = responsestring[indexPath.row] as! NSDictionary
        let BranchCode = (dic1.object(forKey: "BranchCode") as! String)
        let BranchName =  (dic1.object(forKey: "BranchName") as! String)
        let myvalue = BranchCode + "-" + BranchName
        LibraryAPI.sharedInstance.view360secondpopupname = BranchCode
        LibraryAPI.sharedInstance.view360secondpopupstr = myvalue
        LibraryAPI.sharedInstance.Custbranchisbtnclicked = false
        self.closeHandler?()
        
    }
    
    
    func loaddata()
    {
        //  http://edi.redingtonb2b.in/whatsup-staging/Users.asmx/BranchDetails?UserID=SENTHILKS
      //  responsestring  = LibraryAPI.sharedInstance.RequestUrl(url: "\(LibraryAPI.sharedInstance.GlobalUrl)Users.asmx/BranchDetails?UserID=\(LibraryAPI.sharedInstance.Userid)")
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Users.asmx/BranchDetails?UserID=\(LibraryAPI.sharedInstance.Userid)", completion: { (jsonmutable) in
            self.responsestring = jsonmutable
            if(self.responsestring.count > 0)
            {
                self.BranchTableview.reloadData()
                self.local_activityIndicator_stop()
            }
            else
            {
                self.showToast(message: "No Data Available")
            }
        })
    }
}
