//
//  Downloadmanager.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 10/04/18.
//  Copyright © 2018 truetech. All rights reserved.
//

import Foundation

class SessionDelegate:NSObject, URLSessionDownloadDelegate {
   
    
    
    var baseFolder:NSURL!
    
     func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
       
        if let documentsDirectoryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first, baseFolder == nil
        {
            let baseUrl = NSURL(fileURLWithPath:documentsDirectoryPath)
            baseFolder = baseUrl.appendingPathComponent("MyFolder") as! NSURL
            
            do {
                try FileManager.default.createDirectory(at: baseFolder as URL, withIntermediateDirectories: true, attributes: nil)
            }
            catch {
               
            }
        }
        
        // Next we retrieve the suggested name of our file and this file name to the URL (note we don't create this file beforehand)
        let suggestedName = downloadTask.response?.suggestedFilename ?? "new_file.pdf"
        let newLocation = baseFolder.appendingPathComponent(suggestedName)
        
        
        do {
            try FileManager.default.moveItem(at: location, to: newLocation!)
        }
        catch {
            print("error moving file")
        }
        
    }
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
    }
    
    
}
