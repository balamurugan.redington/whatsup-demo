//
//  SPCViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 09/05/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import KCFloatingActionButton


class SPCViewController: UIViewController,KCFloatingActionButtonDelegate,UITextFieldDelegate,UITextViewDelegate  {

    @IBOutlet weak var spcnumber: UITextField!
    @IBOutlet weak var customercodetf: UITextField!
    @IBOutlet weak var submitbtn: UIButton!
    
    @IBOutlet weak var mainview: UIView!
    var CustomcodeCheck : String!
    var response : NSMutableArray!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        spcnumber.delegate = self
        customercodetf.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationmethod), name: NSNotification.Name(rawValue: "GetNotification"), object: nil)
        spcnumber.tag = 1
        customercodetf.tag = 2
        submitbtn.layer.masksToBounds=true
        submitbtn.layer.cornerRadius=20.0
        mainview.layer.masksToBounds=true
        mainview.layer.cornerRadius=10.0
        
     customnavigation()
    self.navigationController?.setNavigationBarHidden(true, animated: false)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        appdelegate.shouldSupportAllOrientation = false
        
        if CustomcodeCheck == nil || CustomcodeCheck == ""
        {
            self.customercodetf.text = ""
        }
        else
        {
            self.customercodetf.text = LibraryAPI.sharedInstance.CustomerCode
        }
        
//        LibraryAPI.sharedInstance.SPClblStr = ""
//        LibraryAPI.sharedInstance.SPCtitleStr = ""
//        self.spcnumber.text = ""
//        self.customercodetf.text = ""
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
  
        view.endEditing(true)
        if (textField.tag == 2)
        {
            spcnumber.text = ""
            LibraryAPI.sharedInstance.Currentcustomerlookup="spc"
            if #available(iOS 10.0, *)
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: CustomerLookupViewController.instance())
                    
                    .didShowHandler
                    { popup in
                        self.customercodetf.resignFirstResponder()
                    }
                    .didCloseHandler
                    { _ in
                    }
                
                let container = CustomerLookupViewController.instance()
                container.closeHandler =
                    { _ in
                        popup.dismiss()
                    }
                
                popup.show(childViewController: container)
            }
            else
            {
                
            }
            return false
        }
        
        return true
        
    }


    func customnavigation()
        
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(SPCViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Spc Status"
        
    }
    
    func buttonClicked(sender:UIButton)
    {
        
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
    }
    

    @IBAction func SubmitAction(_ sender: AnyObject)
    {
        
        if(spcnumber.text == "" && customercodetf.text == "")
        {
            let alert = UIAlertController(title: "Sorry..!!", message: "Enter Any one Spc number Or Customer Code", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            var CustomerID : String  = ""
            var CustomUrlID : String = ""
            
            if(spcnumber.text != "" && customercodetf.text == "")
            {
                CustomerID = self.spcnumber.text!
                CustomUrlID = "2"
                LibraryAPI.sharedInstance.SPClblStr = "SPC No"
                LibraryAPI.sharedInstance.SPCtitleStr = CustomerID
            }
            else
            {
                CustomerID = self.customercodetf.text!
                CustomUrlID = "1"
                LibraryAPI.sharedInstance.SPClblStr = "Cust Code"
                LibraryAPI.sharedInstance.SPCtitleStr = CustomerID
            }
            self.local_activityIndicator_start()
            let name = LibraryAPI.sharedInstance.Userid.stringByRemovingWhitespaces
            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/SPCStatusEnquiry?Userid=\(name)&CustomerCode=\(CustomerID)&SPCValue=\(CustomUrlID)", completion: { (jsonmutable) in
                self.response = jsonmutable
            if(self.response.count > 0)
            {
                self.local_activityIndicator_stop()
                LibraryAPI.sharedInstance.SPCStatus = self.response
                let lvc=self.storyboard?.instantiateViewController(withIdentifier: "SpcDetailsViewController") as!SpcDetailsViewController
                self.navigationController?.pushViewController(lvc, animated: true)
            }
            else
            {
                self.local_activityIndicator_stop()
                let alert = UIAlertController(title: "Sorry..!!", message: "No Data Available", preferredStyle: UIAlertControllerStyle.alert)
                self.customercodetf.text = ""
                self.spcnumber.text = ""
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
          })
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //fab.hidden=false
        if(textField.tag == 1)
        {
            spcnumber.resignFirstResponder()
        }
        else if (textField.tag == 2)
        {
            customercodetf.resignFirstResponder()
        }
            return true
    }
 
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
  
        if(textField == spcnumber) {
            let aSet = NSCharacterSet(charactersIn:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789").inverted
            //let compSepByCharInSet = string.componentsSeparatedByCharactersInSet(aSet)
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
        }
        return true
    }
    func notificationmethod()
    {
        let userdefaults = Foundation.UserDefaults.standard
        let value = userdefaults.dictionary(forKey: "Notification")
        var title: String!
        var message: String!
        var imagestr: String!
   
        title = value?["title"] as! String
        message = value?["body"] as! String
        imagestr = value?["imageurl"] as! String
        
        if imagestr != nil
        {
            if imagestr != ""
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: notificationimageViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = notificationimageViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                    
                }
                popup.show(childViewController: container)
            }
            else
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: PushnotificationtextViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = PushnotificationtextViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                }
                
                popup.show(childViewController: container)
            }
        }
        else
        {
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: PushnotificationtextViewController.instance())
                
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
            }
            let container = PushnotificationtextViewController.instance()
            container.closeHandler = { _ in
                popup.dismiss()
            }
            
            popup.show(childViewController: container)
        }
        
        
    }
    
}
