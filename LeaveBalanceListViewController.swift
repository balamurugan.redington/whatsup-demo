//
//  LeaveBalanceListViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 18/07/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class LeaveBalanceListViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var leavebalancetableview: UITableView!
    
     var responsestring = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        print("LeaveBalanceListViewController")
        leavebalancetableview.delegate = self
        leavebalancetableview.dataSource = self
        if LibraryAPI.sharedInstance.Globalflag == "N" {
            customnavigation1()
        } else {
            customnavigation()
        }
        leavebalancetableview.register(UINib(nibName: "LeaveBalanceTableViewCell", bundle: nil), forCellReuseIdentifier: "LeaveBalanceTableViewCell")
        leavebalancetableview.tableFooterView = UIView(frame: .zero)
        leavebalancetableview.separatorStyle = .none
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        appdelegate.shouldSupportAllOrientation = false
    }
    
    func customnavigation() {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(LeaveBalanceListViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "View Leave Details"
    }
    
    func customnavigation1() {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        //customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), forState: UIControlState.Normal)
        customView.MenuBtn.addTarget(self, action: #selector(LeaveBalanceListViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        //customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),forControlEvents:UIControlEvents.TouchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.textAlignment = .center
        customView.HeaderLabel.font = UIFont.boldSystemFont(ofSize: 16.0)
        customView.HeaderLabel.text = "View Time Details"
    }
    
    func buttonClicked(sender:UIButton) {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "LeaveBalanceViewController")), animated: true)
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 111
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if(LeaveBalance_Month_Arr.count == 0) {
            let label = UILabel(frame: CGRect(x: 0,y: 0,width: tableView.bounds.size.width,height: tableView.bounds.size.height))
            label.text = "No Data Available"
            label.textColor = UIColor.red
            label.textAlignment = NSTextAlignment.center
            label.sizeToFit()
            label.font = UIFont(name: (label.font?.fontName)!,size: 20)
            tableView.backgroundView = label
            return 0
        } else {
            tableView.backgroundView = nil
            return 1
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return LeaveBalance_Month_Arr.count
    }
   
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeaveBalanceTableViewCell") as! LeaveBalanceTableViewCell
        cell.Leavedatelbl.text = String(LeaveBalance_Month_Arr[indexPath.row].LEAVE_DATE)
        cell.daylbl.text =  String(describing: LeaveBalance_Month_Arr[indexPath.row].DAY)
        cell.intimelbl.text = LeaveBalance_Month_Arr[indexPath.row].IN_TIME
        cell.outtimelbl.text = LeaveBalance_Month_Arr[indexPath.row].OUT_TIME
        cell.remarkslbl.text = LeaveBalance_Month_Arr[indexPath.row].REMARK
        cell.statuslbl.text = LeaveBalance_Month_Arr[indexPath.row].STATUS
        cell.selectionStyle = .none
        return cell
    }
}
