//
//  CiscoPopupViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 26/06/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class CiscoPopupViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,PopupContentViewController {

    @IBOutlet weak var ciscotableview: UITableView!
    
    
    var closeHandler: (() -> Void)?
    override func viewDidLoad()
    {
        super.viewDidLoad()
        ciscotableview.delegate = self
        ciscotableview.dataSource = self
        

         ciscotableview.register(UINib(nibName: "ciscoTableViewCell", bundle: nil), forCellReuseIdentifier: "ciscoTableViewCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    class func instance() -> CiscoPopupViewController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "CiscoPopupViewController") as! CiscoPopupViewController
    }
    
    
    @IBAction func CloseAction(_ sender: AnyObject)
    {
        
        closeHandler?()
    }
    
    
    func sizeForPopup(popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
       
        return CGSize(width: 250, height: 400)
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        if ( LibraryAPI.sharedInstance.ciscopopup.count > 0)
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    
  
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 97
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
      
      return  LibraryAPI.sharedInstance.ciscopopup.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ciscoTableViewCell", for: indexPath) as! ciscoTableViewCell
        let dic1 = LibraryAPI.sharedInstance.ciscopopup[indexPath.row] as! NSDictionary
        let refernce = String(describing: dic1.object(forKey: "REFERENCE NO") as! NSNumber)
        let eopnumber = dic1.object(forKey:"EOPNOROOM NAME")as! String
        let quantity = String(describing: dic1.object(forKey: "SALES QTY REMINING") as! NSNumber)
        
        cell.referenceno.text =  refernce
        cell.eopno.text = eopnumber
        cell.quantityremaining.text = quantity
        return cell
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dic2 = LibraryAPI.sharedInstance.ciscopopup[indexPath.row] as! NSDictionary
        LibraryAPI.sharedInstance.select_ciscopopup.setObject(dic2.object(forKey: "REFERENCE NO")!, forKey: "reference" as NSCopying)
        LibraryAPI.sharedInstance.select_ciscopopup.setObject(dic2.object(forKey: "EOPNOROOM NAME")!, forKey: "eop" as NSCopying)
        LibraryAPI.sharedInstance.select_ciscopopup.setObject(dic2.object(forKey: "SALES QTY REMINING")!, forKey: "quality" as NSCopying)
              
        LibraryAPI.sharedInstance.globaldic.setObject(dic2.object(forKey: "REFERENCE NO")!, forKey: "reference" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(dic2.object(forKey: "EOPNOROOM NAME")!, forKey: "eop" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(dic2.object(forKey: "SALES QTY REMINING")!, forKey: "quality" as NSCopying)
        
        closeHandler?()
        CloseAction(ItemcodePopupViewController())

    }
    
   
     }
