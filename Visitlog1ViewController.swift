//
//
//  Redington
//
//  Created by truetech on 13/09/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
import CoreLocation
//import NVActivityIndicatorView
import Firebase
import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import KCFloatingActionButton

class Visitlog1ViewController: UIViewController,UITextFieldDelegate,UITextViewDelegate,CLLocationManagerDelegate,UITableViewDelegate,UITableViewDataSource,KCFloatingActionButtonDelegate{
    
    @IBOutlet weak var bottomview: UIView!
    @IBOutlet weak var topview: UIView!
    @IBOutlet weak var visitlogtableview: UITableView!
    @IBOutlet weak var bizcodetf: UITextField!
    @IBOutlet var Locationtf: UITextField!
    @IBOutlet var Datepicker: UIDatePicker!
    @IBOutlet var Datepicker1: UIDatePicker!
    @IBOutlet var Containerview: UIView!
    @IBOutlet var Scrollview: UIScrollView!
    @IBOutlet var Vendorbtn: UIButton!
    @IBOutlet var Prospectbtn: UIButton!
    @IBOutlet var Customerbtn: UIButton!
    @IBOutlet var SubmitBtn: UIButton!
    @IBOutlet var DatePickerview: UIView!
    @IBOutlet var Timeupdatetf: UITextField!
    @IBOutlet var Dateupdatetf: UITextField!
    @IBOutlet var Textview2: UITextView!
    @IBOutlet var Emailtf: UITextField!
    @IBOutlet var Userupdatetf: UITextField!
    @IBOutlet var Areatf: UITextField!
    @IBOutlet var Textview1: UITextView!
    @IBOutlet var metwhomtextfield: UITextField!
    @IBOutlet var Naturetf: UITextField!
    @IBOutlet var Totime: UITextField!
    @IBOutlet var fromtimetf: UITextField!
    @IBOutlet var Codetf: UITextField!
    @IBOutlet var Datetf: UITextField!
    @IBOutlet var Nametf: UITextField!
    @IBOutlet var Phonenumbertf: UITextField!
    @IBOutlet var Designationtf: UITextField!
    
    var currenttf=String()
    var uploadfile=NSMutableDictionary()
    var type=String()
    var jsonresult=NSDictionary()
    var fab=KCFloatingActionButton()
    var Movedup=Bool()
    var selectedrow=Int()
    var pickerarray=NSMutableArray()
    var responsearray=NSMutableArray()
    var BizcodeArray = NSMutableArray()
    var businesscode=String()
    var locationManager: CLLocationManager!
    var responsestring : NSMutableDictionary!
    var responsestring1 : NSMutableArray!
    var latitude : Double!
    var longitude : Double!
    var CustomcodeCheck : String!
    var Customername : String!
    var current_pin: String!
    var current_area: String!
    
    var tableviewresponsearray : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.layoutFAB()
        self.Scrollview.contentSize = Containerview.frame.size
        self.addDoneButtonOnKeyboard()
        Timeupdatetf.delegate=self
        Dateupdatetf.delegate=self
        visitlogtableview.delegate = self
        visitlogtableview.dataSource = self
        Timeupdatetf.isHidden = true
        Dateupdatetf.isHidden = true
        Emailtf.delegate=self
        SubmitBtn.layer.cornerRadius=15.0
        metwhomtextfield.delegate=self
        Naturetf.delegate=self
        Totime.delegate=self
        fromtimetf.delegate=self
        Codetf.delegate=self
        Datetf.delegate=self
        Nametf.delegate=self
        Phonenumbertf.delegate=self
        Designationtf.delegate=self
        Locationtf.delegate=self
        bizcodetf.delegate = self
        Naturetf.tag=9
        bizcodetf.tag = 25
        Codetf.tag = 26
        Dateupdatetf.tag=1
        Timeupdatetf.tag=2
        //Areatf.tag=4
        fromtimetf.tag=2
        Totime.tag=5
        Datetf.tag=6
        Phonenumbertf.tag = 11
        Emailtf.tag = 20
        self.Customnavigation()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        Movedup=false
        NotificationCenter.default.addObserver(self, selector: #selector(Visitlog1ViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(Visitlog1ViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        let arrow = UIImageView(image: UIImage(named: "cal.png"))
        arrow.frame = CGRect(x: 0.0,y: 0.0,width: arrow.image!.size.width+10.0,height: 15);
        arrow.contentMode = UIViewContentMode.center
        self.Datetf.rightView = arrow;
        self.Datetf.rightViewMode = UITextFieldViewMode.always
        
        let arrow2 = UIImageView(image: UIImage(named: "cal.png"))
        arrow2.frame = CGRect(x: 0.0,y: 0.0,width: arrow2.image!.size.width+10.0,height: 15);
        arrow2.contentMode = UIViewContentMode.center
        self.Dateupdatetf.rightView = arrow2;
        self.Dateupdatetf.rightViewMode = UITextFieldViewMode.always
        type="empty"
        Phonenumbertf.keyboardType = .numberPad
        Textview1.layer.cornerRadius = 5.0
        Textview1.layer.borderWidth = 1.0
        self.Textview1.layer.borderColor = UIColor(red:211/255.0, green:211/255.0, blue:211/255.0, alpha: 1.0).cgColor
        
        bizcodeRec()
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationmethod), name: NSNotification.Name(rawValue: "GetNotification"), object: nil)
        visitlogtableview.register(UINib(nibName: "AddTableViewCell", bundle: nil), forCellReuseIdentifier: "AddTableViewCell")
        visitlogtableview.tableFooterView = UIView(frame: .zero)
    }
    
    func bizcodeRec()
    {
            //let size = CGSize(width: 30, height:30)
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
            self.Loaddata(url: "\(LibraryAPI.sharedInstance.GlobalUrl)StockDetails.asmx/BusinessAccessDetails?UserID=\(LibraryAPI.sharedInstance.Userid)")
            DispatchQueue.main.async {
                self.local_activityIndicator_stop()
                self.Datepicker.isHidden=false
            }
        }
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true) {
        } else {
            if(Movedup==false) {
                self.view.frame.origin.y -= 50
                Movedup=true
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true) {
        } else {
            self.view.frame.origin.y = 0
            Movedup=false
        }
    }
    
    func setdateandtime() {
        let date = NSDate()
        let calendar = NSCalendar.current
        let companents = calendar.dateComponents([.hour, .minute, .second], from: date as Date)
        let hour = companents.hour
        let minutes = companents.minute
        let formattedhour=String(describing: hour)
        let formattedtime=String(describing: minutes)
        let originaltime=formattedhour.appending(":\(formattedtime)")
        let dateMakerFormatter1 = DateFormatter()
        dateMakerFormatter1.dateFormat = "HH:mm"
        let startTime = dateMakerFormatter1.date(from: originaltime)!
        let strtime = dateMakerFormatter1.string(from: startTime)
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let strDate = dateFormatter.string(from: date as Date)
        Dateupdatetf.text=strDate
        Timeupdatetf.text=strtime
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        type="Customer"
        Customerbtn.setImage(UIImage(named:"green.png"), for: UIControlState.normal)
        Prospectbtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        Vendorbtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        Codetf.isUserInteractionEnabled = true
        Codetf.placeholder = ""
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "yyyy-MM-dd"
        self.Datetf.text = dateFormatter.string(from: Datepicker.date)
        if (CLLocationManager.locationServicesEnabled()) {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        if CustomcodeCheck == nil || CustomcodeCheck == "" {
            self.Codetf.text = ""
        } else {
            self.Codetf.text = LibraryAPI.sharedInstance.CustomerCode
            self.Nametf.text = Customername
            getUserDetauls(code: self.Codetf.text!)
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = DateFormatter.Style.long
            dateFormatter.dateFormat = "yyyy-MM-dd"
            self.Datetf.text = dateFormatter.string(from: Datepicker.date)
        }
    }
    
    func getUserDetauls(code: String) {
        let name = LibraryAPI.sharedInstance.Userid.stringByRemovingWhitespaces
        //responsestring1 = LibraryAPI.sharedInstance.RequestUrl(url: "\(LibraryAPI.sharedInstance.GlobalUrl)Users.asmx/OldVisitLogDetails?UserID=\(name)&CusCode=\(code)")
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Users.asmx/OldVisitLogDetails?UserID=\(name)&CusCode=\(code)", completion:{ (jsonmutable) in
            self.responsestring1 = jsonmutable
            if(self.responsestring1.count > 0) {
                let dic = self.responsestring1[0] as! NSDictionary
                self.metwhomtextfield.text  = dic.object(forKey: "MET WHOM") as? String
                self.Designationtf.text     = dic.object(forKey: "DESIGNATION") as? String
                self.Phonenumbertf.text     = dic.object(forKey: "PHONE NUMBER") as? String
                self.Emailtf.text           = dic.object(forKey: "MAIL ID") as? String
            } else {
            }
        })
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location1 = locations.last! as CLLocation
        latitude = location1.coordinate.latitude
        longitude = location1.coordinate.longitude
        let location = CLLocation(latitude: latitude, longitude: longitude)
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
            if error != nil {
                return
            }
            if placemarks!.count > 0 {
                let pm = placemarks![0]
                self.current_area = pm.subLocality
                self.current_pin = pm.postalCode
            } else {
            }
        })
        
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    @IBAction func customerAction(_ sender: AnyObject) {
        type="Customer"
        Customerbtn.setImage(UIImage(named:"green.png"), for: UIControlState.normal)
        Prospectbtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        Vendorbtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        Codetf.isUserInteractionEnabled = true
        Codetf.placeholder = ""
    }
    
    func Customnavigation() {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65)
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(Visitlog1ViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Daily Field List"
    }
    
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            self.fab.close()
        }
        self.view.addSubview(fab)
    }
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0,y: 0,width: 320,height: 50))
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(Visitlog1ViewController.doneButtonAction))
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        doneToolbar.items = items as AnyObject as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        Phonenumbertf.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction() {
        Phonenumbertf.resignFirstResponder()
    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
        
    }
    
    func navigate() {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
    }
    
    func buttonClicked(sender:UIButton) {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
    }
    
    @IBAction func Doneaction(_ sender: AnyObject) {
        DatePickerview.isHidden=true
        if( currenttf=="Date") {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = DateFormatter.Style.long
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let strDate = dateFormatter.string(from: Datepicker.date)
            Datetf.text=strDate
        } else if( currenttf=="Time") {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            let strDate = dateFormatter.string(from: Datepicker1.date)
            fromtimetf.text=strDate
        } else if( currenttf=="to") {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            let strDate = dateFormatter.string(from: Datepicker1.date)
            Totime.text=strDate
        } else if( currenttf=="DateUpdate") {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            // dateFormatter.dateStyle = NSDateFormatterStyle.LongStyle
            let strDate = dateFormatter.string(from: Datepicker.date)
            Dateupdatetf.text=strDate
        } else if( currenttf=="Timeupdate") {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            //dateFormatter.dateStyle = NSDateFormatterStyle.LongStyle
            let strDate = dateFormatter.string(from: Datepicker1.date)
            Timeupdatetf.text=strDate
        }
    }
    
    @IBAction func SubmitAction(_ sender: AnyObject) {
        if((self.Nametf.text!.isEmpty || self.Datetf.text!.isEmpty  || self.fromtimetf.text!.isEmpty || self.Totime.text!.isEmpty || self.Textview1.text!.isEmpty || self.Naturetf.text!.isEmpty || self.metwhomtextfield.text!.isEmpty || self.Locationtf.text!.isEmpty || self.Designationtf.text!.isEmpty || self.Phonenumbertf.text!.isEmpty || self.Emailtf.text!.isEmpty || self.type=="empty")) {
            self.NormalAlert(title: "Alert", Messsage: "Enter the all fields")
        } else {
            if(self.isValidEmail(testStr: Emailtf.text!)==false) {
                self.NormalAlert(title: "Alert", Messsage: "E-mail is not Valid")
            } else {
                if((self.Phonenumbertf.text?.count)!<10) {
                    self.NormalAlert(title: "Alert", Messsage: "Phone No is Too Short")
                } else{
                    self.setdateandtime()
                    uploadfile.setObject(type.stringByRemovingWhitespaces as String, forKey: "TYPE" as NSCopying)
                    uploadfile.setObject(Codetf.text!.stringByRemovingWhitespaces as String, forKey: "CUSTOMERCODE" as NSCopying)
                    uploadfile.setObject(Nametf.text!.stringByRemovingWhitespaces as String, forKey: "NAME" as NSCopying)
                    uploadfile.setObject(Datetf.text!, forKey: "DATEOFVISIT" as NSCopying)
                    let str1=Datetf.text?.appending(" \(fromtimetf.text!)")
                    uploadfile.setObject(str1!, forKey: "FROMTIME" as NSCopying)
                    let str=Datetf.text?.appending(" \(Totime.text!)")
                    uploadfile.setObject(str!, forKey: "TOTIME" as NSCopying)
                    let txt=Textview1.text!
                    self.removeSpecialCharsFromString(text: txt)
                    let trimmedString = txt.trimmingCharacters(in: .whitespaces)
                    uploadfile.setObject(trimmedString.stringByRemovingWhitespaces as String, forKey: "MEETINGDESCRIPTION" as NSCopying)
                    uploadfile.setObject(Naturetf.text!.stringByRemovingWhitespaces as String, forKey: "Nature" as NSCopying)
                    uploadfile.setObject(metwhomtextfield.text!.stringByRemovingWhitespaces as String, forKey: "METWHOM" as NSCopying)
                    uploadfile.setObject(Locationtf.text!.stringByRemovingWhitespaces as String, forKey: "AREA" as NSCopying)
                    uploadfile.setObject(Designationtf.text!.stringByRemovingWhitespaces as String, forKey: "DESIGNATION" as NSCopying)
                    uploadfile.setObject(Phonenumbertf.text!.stringByRemovingWhitespaces as String, forKey: "PHONENUMBER" as NSCopying)
                    uploadfile.setObject(Emailtf.text!.stringByRemovingWhitespaces as String, forKey: "MAILID" as NSCopying)
                    uploadfile.setObject(LibraryAPI.sharedInstance.Userid.stringByRemovingWhitespaces as String, forKey: "USERUPDATE" as NSCopying)
                    uploadfile.setObject(Dateupdatetf.text!, forKey: "DATEUPDATE" as NSCopying)
                    let business = bizcodetf.text!
                    let fullNameArr : [String] = business.components(separatedBy: " - ")
                    let firstName : String = fullNameArr[0]
                    uploadfile.setObject(firstName.stringByRemovingWhitespaces as String, forKey: "BUSINESSCODE" as NSCopying)
                    uploadfile.setObject(Timeupdatetf.text!, forKey: "TimeUpdate" as NSCopying)
                    uploadfile.setObject(current_pin, forKey: "PINCODE" as NSCopying)
                    uploadfile.setObject(current_area, forKey: "PLACE" as NSCopying)
                    uploadfile.setObject(latitude!, forKey: "LATITUDE" as NSCopying)
                    uploadfile.setObject(longitude!, forKey: "LONGITUDE" as NSCopying)
                        //let size = CGSize(width: 30, height:30)
                    self.local_activityIndicator_start()
                    DispatchQueue.global(qos: .background).async {
                        self.uploaddata()
                        DispatchQueue.main.async {
                            self.local_activityIndicator_stop()
                            if(self.jsonresult.object(forKey: "Status") as? String  == "Success") {
                                let alertController = UIAlertController(title: "Alert", message: "Visit Log Updated, Your ID : \(self.jsonresult.object(forKey: "VisitId")!)" , preferredStyle: .alert)
                                let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                                    self.navigate()
                                }
                                alertController.addAction(cancelAction)
                                self.present(alertController, animated: true, completion:nil)
                            } else {
                                self.NormalAlert(title: "Alert", Messsage: "Submisson Failed! Retry")
                            }
                        }
                    }
                    dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-*=(),.:!_")
        return String(text.filter {okayChars.contains($0) })
    }
    
    func uploaddata() {
        let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)Users.asmx/CustomerLogUpdation")! as URL)
        request.httpMethod = "POST"
        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
        request.httpBody = try? JSONSerialization.data(withJSONObject: self.uploadfile, options: JSONSerialization.WritingOptions(rawValue: 0))
        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
        do {
            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
            jsonresult = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
        } catch (let e) {
            print(e)
        }
    }
    
    @IBAction func CancelAction(_ sender: AnyObject) {
        DatePickerview.isHidden=true
    }
    
    @IBAction func ProspectAction(_ sender: AnyObject) {
        type="Prospect"
        Prospectbtn.setImage(UIImage(named:"green.png"), for: UIControlState.normal)
        Vendorbtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        Customerbtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        Codetf.isUserInteractionEnabled = false
        Codetf.text = ""
        Codetf.placeholder = "N/A"
    }
    
    @IBAction func VendorACtion(_ sender: AnyObject) {
        type="Vendor"
        Vendorbtn.setImage(UIImage(named:"green.png"), for: UIControlState.normal)
        Prospectbtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        Customerbtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
        Codetf.isUserInteractionEnabled = false
        Codetf.text = ""
        Codetf.placeholder = "N/A"
        
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField.tag==11) {
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 10 // Bool
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        fab.isHidden=true
        LibraryAPI.sharedInstance.poptextfieldclicked=false
        var boo = false
        if(type == "Customer") {
            if textField == Codetf {
                LibraryAPI.sharedInstance.Currentcustomerlookup="Visitlog"
                if #available(iOS 10.0, *) {
                    let popup = PopupController
                        .create(parentViewController: self)
                        .show(childViewController: CustomerLookupViewController.instance())
                        
                        .didShowHandler { popup in
                            self.Codetf.resignFirstResponder()
                        }
                        .didCloseHandler { _ in
                    }
                    let container = CustomerLookupViewController.instance()
                    container.closeHandler = { _ in
                        popup.dismiss()
                    }
                    popup.show(childViewController: container)
                } else {
                }
            } else {
                if Codetf.text == "" {
                    boo=false
                    self.NormalAlert(title: "Alert", Messsage: "Enter Customer Code")
                } else {
                    boo=true
                }
            }
        } else {
            boo=true
        }
        if(boo == true) {
            if(textField.tag==1) {
                DatePickerview.isHidden=false
                Datepicker1.isHidden = true
                Datepicker.isHidden = false
                let dateFormatter = DateFormatter()
                dateFormatter.dateStyle = DateFormatter.Style.long
                dateFormatter.dateFormat = "yyyy-MM-dd"
                Datepicker.maximumDate = NSDate() as Date
                let dateComponents = NSDateComponents()
                dateComponents.day = -4
                let currentCalendar = NSCalendar.current
                let oneMonthBack = currentCalendar.date(byAdding: dateComponents as DateComponents, to: NSDate() as Date)
                Datepicker.minimumDate = oneMonthBack
                currenttf="Date"
                return false
            }
            if(textField.tag==2) {
                DatePickerview.isHidden=false
                Datepicker1.isHidden = false
                Datepicker.isHidden = true
                Datepicker1.datePickerMode = UIDatePickerMode.time
                let formatter = DateFormatter()
                formatter.dateFormat = "HH:mm"
                Datepicker1.minimumDate = formatter.defaultDate
                currenttf="Time"
                return false
            }
            if(textField.tag==3) {
                DatePickerview.isHidden=false
                Datepicker1.isHidden = false
                Datepicker.isHidden = true
                Datepicker1.datePickerMode = UIDatePickerMode.time
                let formatter = DateFormatter()
                formatter.dateFormat = "HH:mm"
                Datepicker1.minimumDate = Datepicker1.date
                currenttf="to"
                return false
            }
            if(textField.tag==5) {
                DatePickerview.isHidden=false
                Datepicker.datePickerMode = UIDatePickerMode.date
                currenttf="DateUpdate"
                return false
            }
            if(textField.tag==6) {
                DatePickerview.isHidden=false
                Datepicker1.datePickerMode = .time
                currenttf="Timeupdate"
                return false
            }
            if(textField.tag==9) {
                let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Nature", message: "Pick an option", preferredStyle: .actionSheet)
                let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
                    
                }
                actionSheetControllerIOS8.addAction(cancelActionButton)
                let saveActionButton: UIAlertAction = UIAlertAction(title: "SALES CALL", style: .default)
                { action -> Void in
                    
                    self.Naturetf.text="SALES CALL"
                }
                actionSheetControllerIOS8.addAction(saveActionButton)
                let deleteActionButton: UIAlertAction = UIAlertAction(title: "COLLECTIONS", style: .default)
                { action -> Void in
                    
                    self.Naturetf.text="COLLECTIONS"
                }
                actionSheetControllerIOS8.addAction(deleteActionButton)
                let PresentationActionButton: UIAlertAction = UIAlertAction(title: "PRESENTATION", style: .default)
                { action -> Void in
                    
                    self.Naturetf.text="PRESENTATION"
                }
                actionSheetControllerIOS8.addAction(PresentationActionButton)
                let courtesy: UIAlertAction = UIAlertAction(title: "COURTESY", style: .default)
                { action -> Void in
                    
                    self.Naturetf.text="COURTESY"
                }
                actionSheetControllerIOS8.addAction(courtesy)
                
                let poc: UIAlertAction = UIAlertAction(title: "POC", style: .default)
                { action -> Void in
                    
                    self.Naturetf.text="POC"
                }
                actionSheetControllerIOS8.addAction(poc)
                let training: UIAlertAction = UIAlertAction(title: "TRAINING", style: .default)
                { action -> Void in
                    
                    self.Naturetf.text="TRAINING"
                }
                actionSheetControllerIOS8.addAction(training)
                let DirectCustomer: UIAlertAction = UIAlertAction(title: "DIRECT CUSTOMER", style: .default)
                { action -> Void in
                    
                    self.Naturetf.text="DIRECT CUSTOMER"
                }
                actionSheetControllerIOS8.addAction(DirectCustomer)
                let Partner: UIAlertAction = UIAlertAction(title: "PARTNER", style: .default)
                { action -> Void in
                    
                    self.Naturetf.text="PARTNER"
                }
                actionSheetControllerIOS8.addAction(Partner)
                let Technical : UIAlertAction = UIAlertAction(title: "TECHNICAL", style: .default)
                { action -> Void in
                    
                    self.Naturetf.text="TECHNICAL"
                }
                actionSheetControllerIOS8.addAction(Technical )
                let Discussion: UIAlertAction = UIAlertAction(title: "DISCUSSION", style: .default)
                { action -> Void in
                    
                    self.Naturetf.text="DISCUSSION"
                }
                actionSheetControllerIOS8.addAction(Discussion)
                let FolowUp: UIAlertAction = UIAlertAction(title: "FOLLOW UP", style: .default)
                { action -> Void in
                    
                    self.Naturetf.text="FOLLOW UP"
                }
                actionSheetControllerIOS8.addAction(FolowUp)
                let Vendor: UIAlertAction = UIAlertAction(title: "VENDOR", style: .default)
                { action -> Void in
                    self.Naturetf.text="VENDOR"
                }
                actionSheetControllerIOS8.addAction(Vendor)
                self.present(actionSheetControllerIOS8, animated: true, completion: nil)
                return false
            }
            if (textField.tag == 25) {
                //let size = CGSize(width: 50, height:50)
                self.local_activityIndicator_start()
                let alert:UIAlertController=UIAlertController(title: "Select Business Code", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
                var alert1 = UIAlertAction()
                if pickerarray.count != 0 {
                    self.local_activityIndicator_stop()
                    for word in pickerarray {
                        alert1 = UIAlertAction(title: (word as! String), style: UIAlertActionStyle.default) {
                            UIAlertAction in
                            let getString : String = word as! String
                            if self.tableviewresponsearray.count < 5 {
                                if self.tableviewresponsearray.count == 0 {
                                    self.tableviewresponsearray.append(getString)
                                    self.visitlogtableview.reloadData()
                                } else {
                                    let contains = self.tableviewresponsearray.contains
                                        {
                                            $0 as? String == getString
                                    }
                                    if contains == false {
                                        self.tableviewresponsearray.append(getString)
                                        self.visitlogtableview.reloadData()
                                    } else {
                                    }
                                }
                            } else {
                            }
                        }
                        alert.addAction(alert1)
                    }
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
                {
                    UIAlertAction in
                }
                alert.addAction(cancelAction)
                self.present(alert, animated: true, completion: nil)
                return false
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        fab.isHidden=false
        textField.resignFirstResponder()
        return true
    }
    
    func Loaddata(url:String)
    {
        //responsearray = LibraryAPI.sharedInstance.RequestUrl(url: url)
        self.getsuccess(params: [:], urlhit: url, completion: { (jsonmutable) in
            self.responsearray = jsonmutable
            if(self.responsearray.count > 0) {
            }
            self.pickerarray.removeAllObjects()
            for i in 0..<self.responsearray.count {
                let dic = self.responsearray[i] as! NSDictionary
                let code = dic.object(forKey: "BUSINESS CODE") as? String
                let desc = dic.object(forKey: "BUSINESS DESC") as? String
                let str=code?.appending(" - \(desc!)")
                self.pickerarray.add(str!)
            }
        })
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            Textview1 .resignFirstResponder()
            return false
        }
        return true
    }
    
    func notificationmethod() {
        let userdefaults = Foundation.UserDefaults.standard
        let value = userdefaults.dictionary(forKey: "Notification")
        var title: String!
        var message: String!
        var imagestr: String!
        title = value?["title"] as! String
        message = value?["body"] as! String
        imagestr = value?["imageurl"] as! String
        if imagestr != nil
        {
            if imagestr != ""
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: notificationimageViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = notificationimageViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                    
                }
                popup.show(childViewController: container)
            }
            else
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: PushnotificationtextViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = PushnotificationtextViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                }
                
                popup.show(childViewController: container)
            }
        }
        else
        {
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: PushnotificationtextViewController.instance())
                
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
            }
            let container = PushnotificationtextViewController.instance()
            container.closeHandler = { _ in
                popup.dismiss()
            }
            
            popup.show(childViewController: container)
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return tableviewresponsearray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddTableViewCell") as! AddTableViewCell
        cell.removebtn.addTarget(self, action: #selector(self.deletebtnclicked), for: UIControlEvents.touchUpInside)
        cell.removebtn.tag = indexPath.row
        cell.lbl.text = tableviewresponsearray[indexPath.row]
        
        return cell
    }
    
    func deletebtnclicked(sender: UIButton)
    {
        let alertController = UIAlertController(title: "Business Code", message: "Are You Sure Want to Delete Business Code?", preferredStyle: .alert)
        self.bizcodetf.resignFirstResponder()
        let DefaultAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
            self.tableviewresponsearray.remove(at: sender.tag)
            self.visitlogtableview.reloadData()
        }
        let CancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
        }
        alertController.addAction(DefaultAction)
        alertController.addAction(CancelAction)
        self.present(alertController, animated: true, completion:nil)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if (self.tableviewresponsearray.count == 0) {
            visitlogtableview.frame = CGRect(x: 0, y: 395, width: self.visitlogtableview.frame.width, height: 0)
            Containerview.frame = CGRect(x: 0, y: 0, width: self.Containerview.frame.width, height: 1049)
            topview.frame = CGRect(x: 0, y: 0, width: self.topview.frame.width, height: 383)
            bottomview.frame = CGRect(x: 0, y: 387, width: self.bottomview.frame.width, height: 507)
            Scrollview.contentSize = Containerview.frame.size
            return 0
        } else {
            if (self.tableviewresponsearray.count == 1) {
                visitlogtableview.frame = CGRect(x: 0, y: 395, width: self.visitlogtableview.frame.width, height: 0 + 44)
                Containerview.frame = CGRect(x: 0, y: 0, width: self.Containerview.frame.width, height: 1049 + 44)
                topview.frame = CGRect(x: 0, y: 0, width: self.topview.frame.width, height: 383)
                bottomview.frame = CGRect(x: 0, y: 387 + 44, width: self.bottomview.frame.width, height: 507)
                Scrollview.contentSize = Containerview.frame.size
            } else if (self.tableviewresponsearray.count == 2) {
                visitlogtableview.frame = CGRect(x: 0, y: 395, width: self.visitlogtableview.frame.width, height: 0 + 88)
                Containerview.frame = CGRect(x: 0, y: 0, width: self.Containerview.frame.width, height: 1049 + 88)
                topview.frame = CGRect(x: 0, y: 0, width: self.topview.frame.width, height: 383)
                bottomview.frame = CGRect(x: 0, y: 387 + 88, width: self.bottomview.frame.width, height: 507)
                Scrollview.contentSize = Containerview.frame.size
            } else if (self.tableviewresponsearray.count == 3) {
                visitlogtableview.frame = CGRect(x: 0, y: 395, width: self.visitlogtableview.frame.width, height: 0 + 132)
                Containerview.frame = CGRect(x: 0, y: 0, width: self.Containerview.frame.width, height: 1049 + 132)
                topview.frame = CGRect(x: 0, y: 0, width: self.topview.frame.width, height: 383)
                bottomview.frame = CGRect(x: 0, y: 387 + 132, width: self.bottomview.frame.width, height: 507)
                Scrollview.contentSize = Containerview.frame.size
            } else if (self.tableviewresponsearray.count == 4) {
                visitlogtableview.frame = CGRect(x: 0, y: 395, width: self.visitlogtableview.frame.width, height: 0 + 176)
                Containerview.frame = CGRect(x: 0, y: 0, width: self.Containerview.frame.width, height: 1049 + 176)
                topview.frame = CGRect(x: 0, y: 0, width: self.topview.frame.width, height: 383)
                bottomview.frame = CGRect(x: 0, y: 387 + 176, width: self.bottomview.frame.width, height: 507)
                Scrollview.contentSize = Containerview.frame.size
            } else if (self.tableviewresponsearray.count == 5) {
                visitlogtableview.frame = CGRect(x: 0, y: 395, width: self.visitlogtableview.frame.width, height: 0 + 220)
                Containerview.frame = CGRect(x: 0, y: 0, width: self.Containerview.frame.width, height: 1049 + 220)
                topview.frame = CGRect(x: 0, y: 0, width: self.topview.frame.width, height: 383)
                bottomview.frame = CGRect(x: 0, y: 387 + 220, width: self.bottomview.frame.width, height: 507)
                Scrollview.contentSize = Containerview.frame.size
            } else {
            }
            return 1
        }
    }
}
