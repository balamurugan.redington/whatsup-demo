//
//  VistlogpopupViewController.swift
//  Redington
//
//  Created by truetech on 03/10/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit

class SeatavailibilityViewController: UIViewController,PopupContentViewController,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var seatAvlTableView: UITableView!
    var closeHandler: (() -> Void)?
    
    var responsestring = NSMutableArray()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.Loaddata()
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationmethod), name: NSNotification.Name(rawValue: "GetNotification"), object: nil)

    }
    
      class func instance() -> SeatavailibilityViewController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "SeatavailibilityViewController") as! SeatavailibilityViewController
    }
    
    @IBAction func CloseAction(_ sender: AnyObject)
    {
        
        closeHandler?()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func sizeForPopup(popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: 290, height: 350)
    }
    
    func Loaddata()
    {
        self.local_activityIndicator_start()
     //   http://edi.redingtonb2b.in/whatsup-staging/WorkLocation.asmx/SeatBookingAvalibilityNew?UserId=SENTHILKS&Date=2017-04-17
        let returnValue  = UserDefaults.standard.object(forKey: "SeatBookingDate") as! String
         self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)/WorkLocation.asmx/SeatBookingAvalibilityNew?UserId=\(LibraryAPI.sharedInstance.Userid)&Date=\(returnValue)", completion: { (jsonmutable) in
            self.responsestring = jsonmutable
            if self.responsestring.count > 0{
                self.seatAvlTableView.reloadData()
                self.local_activityIndicator_stop()
            }else{
                self.local_activityIndicator_stop()
                
            }
        })
        
                
    }
    
    //TableView Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        if responsestring.count > 0
        {
            return 1
        }
        else
        {
            return 0
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        if responsestring.count > 0
        {
            return responsestring.count
        }
        else
        {
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : seatTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as! seatTableViewCell
        if(cell == nil)
        {
            cell = Bundle.main.loadNibNamed("Cell1", owner: self, options: nil)![0] as! seatTableViewCell;
            
        }
        
        if(indexPath.row%2==0)
        {
            cell.contentView.backgroundColor=(UIColor.init(red: 240/255.0, green: 246/255.0, blue: 225/255.0, alpha: 1.0))
        }
        else
        {
            //cell.contentView.backgroundColor=UIColor.whiteColor()
            cell.contentView.backgroundColor=(UIColor.init(red: 215/255.0, green: 250/255.0, blue: 252/255.0, alpha: 1.0))
        }
        let dic = responsestring[indexPath.row] as! NSDictionary
        cell.floortime.text=dic.object(forKey: "From Time") as? String
        cell.totime.text=dic.object(forKey: "To Time") as? String
        
          cell.titlelbl.text = String(dic.object(forKey: "Workstation Seat No") as! Double )
           return cell as seatTableViewCell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         tableView.deselectRow(at: indexPath as IndexPath, animated: true)
    }
    
    func notificationmethod()
    {
        let userdefaults = Foundation.UserDefaults.standard
        let value = userdefaults.dictionary(forKey: "Notification")
        
        var title: String!
        var message: String!
        var imagestr: String!
        title = value?["title"] as! String
        message = value?["body"] as! String
        imagestr = value?["imageurl"] as! String
        
        if imagestr != nil
        {
            if imagestr != ""
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: notificationimageViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = notificationimageViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                    
                }
                popup.show(childViewController: container)
            }
            else
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: PushnotificationtextViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = PushnotificationtextViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                }
                
                popup.show(childViewController: container)
            }
        }
        else
        {
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: PushnotificationtextViewController.instance())
                
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
            }
            let container = PushnotificationtextViewController.instance()
            container.closeHandler = { _ in
                popup.dismiss()
            }
            
            popup.show(childViewController: container)
        }
        
    }
    
    
    
  
    
}
