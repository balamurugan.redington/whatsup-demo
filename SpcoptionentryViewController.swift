 //
 //  SpcoptionentryViewController.swift
 //  Redington Whatsup
 //
 //  Created by RIL-ITTECH on 27/05/17.
 //  Copyright © 2017 truetech. All rights reserved.
 //
 
 import UIKit
 //import NVActivityIndicatorView
 import KCFloatingActionButton
 
 class SpcoptionentryViewController: UIViewController,KCFloatingActionButtonDelegate,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource {
    
    @IBOutlet weak var Scrollview: UIScrollView!
    @IBOutlet weak var mainview: UIView!
    @IBOutlet weak var warrentylbl: UILabel!
    @IBOutlet weak var customercodetf: UITextField!
    @IBOutlet weak var businesscode: UITextField!
    @IBOutlet weak var stockroomtf: UITextField!
    @IBOutlet weak var validtf: UITextField!
    @IBOutlet weak var priceapptypetf: UITextField!
    @IBOutlet weak var paymentdaystf: UITextField!
    @IBOutlet weak var frieghtyesbtn: UIButton!
    @IBOutlet weak var frieghtnobtn: UIButton!
    @IBOutlet weak var multiyesbtn: UIButton!
    @IBOutlet weak var multinobtn: UIButton!
    @IBOutlet weak var vendorreftf: UITextField!
    @IBOutlet weak var warrenrtytf: UITextField!
    @IBOutlet weak var endcusponumtf: UITextField!
    @IBOutlet weak var PickerView: UIView!
    @IBOutlet weak var Picker: UIPickerView!
    @IBOutlet var datepickerview: UIView!
    @IBOutlet weak var datepicker: UIDatePicker!
    var CustomcodeCheck : String!
    
    var BizCode: String!
    var PriceappType: String!
    var stockroom: String!
    var ValidityDate: String!
    var PaymentDays: String!
    var FreightPrepaid:String!
    var MultipleInvoicingAllowed: String!
    var VendorReference:String!
    var EndCustomerName: String!
    var Warranty: String!
    var Message: String!
    var Valid : String!
    var Flag1: String!
    var Flag2: String!
    var Flag3: String!
    var PriceappType_out: String!
    var StockRoom_out: String!
    var ValidityDate_out: String!
    var PaymentDays_out: String!
    var FreightPrepaid_out: String!
    var MultipleInvoicingAllowed_out: String!
    var VendorReference_out: String!
    var EndCustomerName_out: String!
    var BizCode_out: String!
    var Warranty_out: String!
    var ErrorMessage: String!
   
    var uploadfile=NSMutableDictionary()
    var jsonresult=NSDictionary()
    var responsearray=NSMutableArray()
    var spcresponsearray=NSMutableArray()
    var pickerarray=NSMutableArray()
    var responsearray1=NSMutableArray()
    var pickerarray1=NSMutableArray()
    var responsearray2=NSMutableArray()
    var pickerarray2=NSMutableArray()
    var checkchar : String!
    var checkchar1 : String!
    var fab=KCFloatingActionButton()
    var Movedup=Bool()
    var selectedrow=Int()
    var currentpicker=String()
    var responsestring=NSMutableArray()
    var customername: String!
    var responsestringitem = NSMutableArray()
    var businesscodestr = String()
    
    //end Customer details
    var endcustomernamedetail = String()
       var usernames = String()
    
    
    
    
    override func viewDidLoad()
    {
        
        super.viewDidLoad()
        LibraryAPI.sharedInstance.internetcheck()
        if LibraryAPI.sharedInstance.spcstr != nil
        {
            if LibraryAPI.sharedInstance.globaldic.count > 0
            {
                 gettempstorage()
            }
           
        }
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.customnavigation()
        self.Scrollview.contentSize = mainview.frame.size
        self.layoutFAB()
        
        //delegates
        self.customercodetf.delegate = self
        self.businesscode.delegate = self
        self.stockroomtf.delegate = self
        self.validtf.delegate = self
        self.paymentdaystf.delegate = self
        self.vendorreftf.delegate = self
        self.warrenrtytf.delegate = self
        self.endcusponumtf.delegate = self
        self.priceapptypetf.delegate = self
        
        //tags
        
        self.customercodetf.tag = 1
        self.businesscode.tag = 2
        self.priceapptypetf.tag = 3
        self.stockroomtf.tag = 4
        self.validtf.tag = 5
        self.paymentdaystf.tag = 6
        self.vendorreftf.tag = 7
        self.warrenrtytf.tag = 9
        self.endcusponumtf.tag = 8
        
       // self.spcbizcodeRec()
        
       
        
        PickerView.isHidden = true
        self.cityrec()
        
        checkchar = "N"
        checkchar1 = "Y"
        warrenrtytf.isHidden = true
        warrentylbl.isHidden = true
        
        
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(vendorrefno),
            name: NSNotification.Name(rawValue: "VendorNotification"),
            object: nil)
        
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(vendorrefno_045),
            name: NSNotification.Name(rawValue: "VendorNotification_045"),
            object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationmethod), name: NSNotification.Name(rawValue: "GetNotification"), object: nil)
                Movedup=false
        NotificationCenter.default.addObserver(self, selector: #selector(SpcoptionentryViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SpcoptionentryViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        let arrow = UIImageView()
        let image = UIImage(named: "cal.png")
        arrow.image = image!.imageWithColor(tintColor: UIColor.init(red:239/255.0 , green: 88/255.0, blue: 88/255.0, alpha: 1.0)).withRenderingMode(.alwaysOriginal)
        arrow.frame = CGRect(x: 0.0,y: 0.0,width: arrow.image!.size.width+10.0,height: 15);
        arrow.contentMode = UIViewContentMode.center
        self.validtf.rightView = arrow;
        self.validtf.rightViewMode = UITextFieldViewMode.always
         self.customernamedata()
         self.bizcodeRec()
       endcusponumtf.keyboardType = .default
        paymentdaystf.keyboardType = .numberPad
        vendorreftf.keyboardType = .numberPad
        self.addDoneButtonOnKeyboard()
        
        
    }
    
    
    func addDoneButtonOnKeyboard()
        
    {
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        
        doneToolbar.barStyle = UIBarStyle.blackTranslucent
        
        
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(SpcoptionentryViewController.doneButtonAction))
        
        
        
        let items = NSMutableArray()
        
        items.add(flexSpace)
        
        items.add(done)
        
        
        
        doneToolbar.items = items as AnyObject as? [UIBarButtonItem]
        
        doneToolbar.sizeToFit()
        
        
        
        vendorreftf.inputAccessoryView = doneToolbar
        paymentdaystf.inputAccessoryView = doneToolbar
       
        
    }
    
    func doneButtonAction()
    {
        defaulfCheck()
      
        paymentdaystf.resignFirstResponder()
        vendorreftf.resignFirstResponder()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        
        
//        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        appdelegate.shouldSupportAllOrientation = false
        
        PickerView.isHidden = true
        
        frieghtnobtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
        multiyesbtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
        
        
        
        if CustomcodeCheck == nil || CustomcodeCheck == ""
        {
        }
        else
        {
            self.customercodetf.text = LibraryAPI.sharedInstance.CustomerCode
            
        }
        
        
        if stockroom == nil || stockroom == ""
        {
        }
        else
        {
            self.stockroomtf.text = stockroom
        }
        
        
        if Message != nil
        {
            if Message != ""
            {
                showToast(message: Message)
            }
            else  if ErrorMessage != ""
            {
                showToast(message: ErrorMessage)
            }
            else
            {
                
            }
        }
            //global retrieve data from end customer view
        var todaysDate:NSDate = NSDate()
        let dateFormatter:DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        self.validtf.text = dateFormatter.string(from: NSDate() as Date)
        
    }
    
    func keyboardWillShow(notification: NSNotification)
    {
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true)
        {
        }
        else
        {
            if(Movedup==false)
            {
                self.view.frame.origin.y -= 150
                Movedup=true
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true)
        {
        }
        else
        {
            self.view.frame.origin.y = 0
            Movedup=false
        }
    }
    
    
    func customnavigation()
        
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height : 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(SpcoptionentryViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Price Approval Number Requistion"
        
    }
    
    func buttonClicked(sender:UIButton)
    {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "PreorderMainViewController")), animated: true)
        LibraryAPI.sharedInstance.globaldic.removeAllObjects()
        LibraryAPI.sharedInstance.spcstr = nil
    }
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            LibraryAPI.sharedInstance.globaldic.removeAllObjects()
            LibraryAPI.sharedInstance.spcstr = nil
            
            
            self.fab.close()
        }
        
        fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
            
            let slideInFromLeftTransition = CATransition()
            slideInFromLeftTransition.type = kCAGravityTop
            slideInFromLeftTransition.subtype = kCATransitionFromLeft
            slideInFromLeftTransition.duration = 0.2
            slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "SpcoptionentryViewController")), animated: true)
            LibraryAPI.sharedInstance.globaldic.removeAllObjects()
            LibraryAPI.sharedInstance.spcstr = nil
            self.fab.close()
        }
        
        
        fab.fabDelegate = self
        
        self.view.addSubview(fab)
        
    }
    func KCFABOpened(fab: KCFloatingActionButton) {
        
    }
    
    
    
    @IBAction func NextbtnAction(_ sender: AnyObject)
    {
        LibraryAPI.sharedInstance.select_ciscopopup.removeAllObjects()
        
        self.defaulfCheck()
        
       // let businesscode : [String] = self.businesscode.text!.componentsSeparatedByString(" - ")
        let businesscode : [String] = self.businesscode.text!.components(separatedBy: " - ")
        let businesscodestr : String = businesscode[0]
        
        let errormessage = self.jsonresult.object(forKey: "ErrorMessage")
        let message      = self.jsonresult.object(forKey: "Message")
        
                
        if((self.customercodetf.text!.isEmpty) || (self.businesscode.text!.isEmpty) || (self.priceapptypetf.text!.isEmpty) || (self.stockroomtf.text!.isEmpty))
        {
            self.NormalAlert(title: "Alert", Messsage: "No Data Found")
           /* let alertController = UIAlertController(title: "Alert", message:"No Data Found" , preferredStyle: .Alert)
            let cancelAction = UIAlertAction(title: "Ok", style: .Cancel) { (action:UIAlertAction!) in
            }
            alertController.addAction(cancelAction)
            self.presentViewController(alertController, animated: true, completion:nil)*/
        }
        else if (message as? String) == "This Business is not allowed."
        {
           self.businesscode.text = ""
        }
            
        else if (errormessage as? String) == "This Business is not allowed."
        {
            self.businesscode.text = ""
        }
            
            
        else if(businesscodestr == "303" || businesscodestr == "461" )
        {
            self.defaulfCheck()
           
            let lvc=self.storyboard?.instantiateViewController(withIdentifier: "EndDetails2ViewController") as! EndDetails2ViewController
            tempstorage()
            self.navigationController?.pushViewController(lvc, animated: true)
        }
            
        else if(businesscodestr == "004")
        {
            if(warrenrtytf.text == "")
            {
                self.NormalAlert(title: "Alert", Messsage: "Please Enter the Warrenty value")
               /* let alertController = UIAlertController(title: "Alert", message:"Please Enter the Warrenty value" , preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                }
                alertController.addAction(cancelAction)
                self.presentViewController(alertController, animated: true, completion:nil)*/
            }
                
                
            else
            {
                self.defaulfCheck()
                tempstorage()
                let lvc=self.storyboard?.instantiateViewController(withIdentifier: "spcaddtocartViewController") as! spcaddtocartViewController
                print("json value in spc :",self.jsonresult)
                let price = (self.jsonresult.object(forKey: "PaymentDays_out")as! String)
                let Custcode = (self.jsonresult.object(forKey: "CustomerCode")as! String)
                let endcustname = (self.jsonresult.object(forKey: "EndCustomerName_out")as! String)
                let validitydate = validtf.text!
                let multiinvallowed = (self.jsonresult.object(forKey: "MultipleInvoicingAllowed_out")as! String)
                let frieght = (self.jsonresult.object(forKey: "FreightPrepaid_out")as! String)
                 let  nsuserdefault = NSMutableDictionary()
                 nsuserdefault.setObject(price, forKey: "paymentdaysdefault" as NSCopying)
                 nsuserdefault.setObject(Custcode, forKey: "customercodedefault" as NSCopying)
                 nsuserdefault.setObject(endcustname, forKey: "endcustomernaedefault" as NSCopying)
                 nsuserdefault.setObject(validitydate, forKey: "validatedefault" as NSCopying)
                 nsuserdefault.setObject(multiinvallowed, forKey: "multidefault" as NSCopying)
                 nsuserdefault.setObject(frieght, forKey: "frieghtdefault" as NSCopying)
                
                
                let defaults = UserDefaults.standard
                defaults.set(nsuserdefault, forKey: "defaultdetails")
               
                
                self.navigationController?.pushViewController(lvc, animated: true)
                
                
            }
        }
        else if(businesscodestr == "012" || businesscodestr == "155")
        {
            if(vendorreftf.text == "" || endcusponumtf.text == "")
            {
                self.NormalAlert(title: "Alert", Messsage: "Please Enter the Vendor Reference Number")
             
                
            }
            else
            {
                
                 self.defaulfCheck()
                let lvc=self.storyboard?.instantiateViewController(withIdentifier: "spcaddtocartViewController") as! spcaddtocartViewController
                let price = (self.jsonresult.object(forKey: "PaymentDays_out")as! String)
                let Custcode = (self.jsonresult.object(forKey: "CustomerCode")as! String)
                let endcustname = (self.jsonresult.object(forKey: "EndCustomerName_out")as! String)
                let validitydate = validtf.text!
                let multiinvallowed = (self.jsonresult.object(forKey: "MultipleInvoicingAllowed_out")as! String)
                let frieght = (self.jsonresult.object(forKey: "FreightPrepaid_out")as! String)
                let  nsuserdefault = NSMutableDictionary()
                nsuserdefault.setObject(price, forKey: "paymentdaysdefault" as NSCopying)
                nsuserdefault.setObject(Custcode, forKey: "customercodedefault" as NSCopying)
                nsuserdefault.setObject(endcustname, forKey: "endcustomernaedefault" as NSCopying)
                nsuserdefault.setObject(validitydate, forKey: "validatedefault" as NSCopying)
                nsuserdefault.setObject(multiinvallowed, forKey: "multidefault" as NSCopying)
                nsuserdefault.setObject(frieght, forKey: "frieghtdefault" as NSCopying)
                
                
                let defaults = UserDefaults.standard
                defaults.set(nsuserdefault, forKey: "defaultdetails")
                
                            
                self.navigationController?.pushViewController(lvc, animated: true)

                
            }
        }
            
        else if(businesscodestr == "045")
        {
            if(vendorreftf.text == "" || endcusponumtf.text == "")
            {
                self.NormalAlert(title: "Alert", Messsage: "Please Enter the Vendor Reference Number And Endcustomer name ")
              /*  let alertController = UIAlertController(title: "Alert", message:"Please Enter the Vendor Reference Number And Endcustomer name " , preferredStyle: .Alert)
                
                let cancelAction = UIAlertAction(title: "Ok", style: .Cancel) { (action:UIAlertAction!) in
                }
                alertController.addAction(cancelAction)
                
                
                self.presentViewController(alertController, animated: true, completion:nil)*/
                
            }
                
            else
            {
                self.defaulfCheck()
                
              
                let lvc=self.storyboard?.instantiateViewController(withIdentifier: "spcaddtocartViewController") as! spcaddtocartViewController
                let price = (self.jsonresult.object(forKey: "PaymentDays_out")as! String)
                let Custcode = (self.jsonresult.object(forKey: "CustomerCode")as! String)
                let endcustname = (self.jsonresult.object(forKey: "EndCustomerName_out")as! String)
                let validitydate = validtf.text!
                let multiinvallowed = (self.jsonresult.object(forKey: "MultipleInvoicingAllowed_out")as! String)
                let frieght = (self.jsonresult.object(forKey: "FreightPrepaid_out")as! String)
                let  nsuserdefault = NSMutableDictionary()
                
                nsuserdefault.setObject(price, forKey: "paymentdaysdefault" as NSCopying)
                nsuserdefault.setObject(Custcode, forKey: "customercodedefault" as NSCopying)
                nsuserdefault.setObject(endcustname, forKey: "endcustomernaedefault" as NSCopying)
                nsuserdefault.setObject(validitydate, forKey: "validatedefault" as NSCopying)
                nsuserdefault.setObject(multiinvallowed, forKey: "multidefault" as NSCopying)
                nsuserdefault.setObject(frieght, forKey: "frieghtdefault" as NSCopying)
                
                
                let defaults = UserDefaults.standard
                defaults.set(nsuserdefault, forKey: "defaultdetails")
                
                
             

                self.navigationController?.pushViewController(lvc, animated: true)
                
            }
        }
        else if(businesscodestr == "171")
        {
            if(vendorreftf.text == "" || endcusponumtf.text == "")
            {
                self.NormalAlert(title: "Alert", Messsage: "Please Enter the Vendor Reference Number and End Customer name")
                
            }
            else
            {
                tempstorage()
                self.defaulfCheck()
                let lvc=self.storyboard?.instantiateViewController(withIdentifier: "spcaddtocartViewController") as! spcaddtocartViewController
                let price = (self.jsonresult.object(forKey: "PaymentDays_out")as! String)
                let Custcode = (self.jsonresult.object(forKey: "CustomerCode")as! String)
                let endcustname = (self.jsonresult.object(forKey: "EndCustomerName_out")as! String)
                let validitydate = validtf.text!
                let multiinvallowed = (self.jsonresult.object(forKey: "MultipleInvoicingAllowed_out")as! String)
                let frieght = (self.jsonresult.object(forKey: "FreightPrepaid_out")as! String)
                let  nsuserdefault = NSMutableDictionary()
                
                nsuserdefault.setObject(price, forKey: "paymentdaysdefault" as NSCopying)
                nsuserdefault.setObject(Custcode, forKey: "customercodedefault" as NSCopying)
                nsuserdefault.setObject(endcustname, forKey: "endcustomernaedefault" as NSCopying)
                nsuserdefault.setObject(validitydate, forKey: "validatedefault" as NSCopying)
                nsuserdefault.setObject(multiinvallowed, forKey: "multidefault" as NSCopying)
                nsuserdefault.setObject(frieght, forKey: "frieghtdefault" as NSCopying)
                
                
                let defaults = UserDefaults.standard
                defaults.set(nsuserdefault, forKey: "defaultdetails")
                
                
              

                self.navigationController?.pushViewController(lvc, animated: true)
            }
        }
        
        else
        {
            
            tempstorage()
            self.defaulfCheck()
            
           

            let lvc=self.storyboard?.instantiateViewController(withIdentifier: "spcaddtocartViewController") as! spcaddtocartViewController
            let price = (self.jsonresult.object(forKey: "PaymentDays_out")as! String)
            let Custcode = (self.jsonresult.object(forKey: "CustomerCode")as! String)
            let endcustname = (self.jsonresult.object(forKey: "EndCustomerName_out")as! String)
            let validitydate = validtf.text!
            let multiinvallowed = (self.jsonresult.object(forKey: "MultipleInvoicingAllowed_out")as! String)
            let frieght = (self.jsonresult.object(forKey: "FreightPrepaid_out")as! String)
            let  nsuserdefault = NSMutableDictionary()
            nsuserdefault.setObject(price, forKey: "paymentdaysdefault" as NSCopying)
            nsuserdefault.setObject(Custcode, forKey: "customercodedefault" as NSCopying)
            nsuserdefault.setObject(endcustname, forKey: "endcustomernaedefault" as NSCopying)
            nsuserdefault.setObject(validitydate, forKey: "validatedefault" as NSCopying)
            nsuserdefault.setObject(multiinvallowed, forKey: "multidefault" as NSCopying)
            nsuserdefault.setObject(frieght, forKey: "frieghtdefault" as NSCopying)
            let defaults = UserDefaults.standard
            defaults.set(nsuserdefault, forKey: "defaultdetails")
            self.navigationController?.pushViewController(lvc, animated: true)
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {

        if (textField.tag == 7)
        {
            if self.businesscode.text != ""
            {
                let businesscode : [String] = self.businesscode.text!.components(separatedBy: " - ")
                let businesscodestr : String = businesscode[0]
                
                if businesscodestr == "012" || businesscodestr == "155"
                {
                    defaulfCheck()
                    LibraryAPI.sharedInstance.SPC_045  = businesscodestr
                    vendorreftf.resignFirstResponder()
                    
                    let popup = PopupController
                        .create(parentViewController: self.parent!)
                        .show(childViewController: VendorReferenceViewController.instance())
                        
                        .didShowHandler { popup in
                            self.tempstorage()
                        }
                        .didCloseHandler { _ in
                    }
                    let container = VendorReferenceViewController.instance()
                    container.closeHandler = { _ in
                        popup.dismiss()
                        
                    }
                    popup.show(childViewController: container)
                }
                else if businesscodestr == "171"
                    
                {
                    
                    vendorreftf.resignFirstResponder()
                    let popup = PopupController
                        .create(parentViewController: self.parent!)
                        .show(childViewController: popupvendorViewController.instance())
                        
                        .didShowHandler { popup in
                            self.tempstorage()
                        }
                        .didCloseHandler { _ in
                    }
                    let container = popupvendorViewController.instance()
                    container.closeHandler = { _ in
                        popup.dismiss()
                    }
                    popup.show(childViewController: container)
                    
                }
                    
                    
                else if(businesscodestr == "045")
                {
                    vendorreftf.resignFirstResponder()
                    
                    LibraryAPI.sharedInstance.SPC_045 = businesscodestr
                    
                    let popup = PopupController
                        .create(parentViewController: self.parent!)
                        .show(childViewController: VendorReferenceViewController.instance())
                        
                        .didShowHandler { popup in
                            self.tempstorage()
                            self.gettempstorage()
                        }
                        .didCloseHandler { _ in
                    }
                    let container = VendorReferenceViewController.instance()
                    container.closeHandler = { _ in
                        popup.dismiss()
                        
                    }
                    popup.show(childViewController: container)
                }
                
            }
        }
        
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {

        if (textField.tag == 1)
        {
            // spcnumber.text = "
            
            LibraryAPI.sharedInstance.Currentcustomerlookup="SPCENTRYOPTION"
            if #available(iOS 10.0, *)
            {
                let popup = PopupController
                    .create(parentViewController: parent!)
                    .show(childViewController: CustomerLookupViewController.instance())
                    
                    .didShowHandler
                    { popup in
                        self.customercodetf.resignFirstResponder()
                    }
                    .didCloseHandler
                    { _ in
                        
                }
                
                let container = CustomerLookupViewController.instance()
                container.closeHandler =
                    { _ in
                        popup.dismiss()
                }
                
                popup.show(childViewController: container)
            }
            else
            
            {
                
            }
            
            return false
        }
            
            
        else if (textField.tag == 2)
        {
            //            self.currenttf="business"
            
            vendorreftf.text = ""
            endcusponumtf.text = ""
            //let size = CGSize(width: 30, height:30)
            
            self.local_activityIndicator_start()

            
            let alert:UIAlertController=UIAlertController(title: "Select Business Code", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
           self.local_activityIndicator_stop()
            var alert1 = UIAlertAction()
            
            if pickerarray.count != 0
            {
                for word in pickerarray
                {
                    alert1 = UIAlertAction(title: (word as! String), style: UIAlertActionStyle.default)
                    {
                        
                        UIAlertAction in
                        
                        let businesscode : [String] = (word as AnyObject).components(separatedBy: " - ")
                         self.businesscodestr  = businesscode[0]
                        
                        let defaults = UserDefaults.standard
                        defaults.set(self.businesscodestr, forKey: "UserBusinesscode")
                        
                        
                        if self.businesscodestr == "004"
                        {
                            self.warrenrtytf.isHidden = false
                            self.warrentylbl.isHidden = false
                            
                        }
                        else
                        {
                            self.warrenrtytf.isHidden = true
                            self.warrentylbl.isHidden = true
                        }
                        self.businesscode.text = (word as! String)
                        self.defaulfCheck()
                        
                    }
                    
                    
                    alert.addAction(alert1)
                    
                }
                
                
                
                
            }
            
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
            {
                UIAlertAction in
                
                
            }
            
            // Add the actions
            //picker?.delegate = self
            
            alert.addAction(cancelAction)
            
            self.present(alert, animated: true, completion: nil)
            
            
            return false
            
        }
        else if (textField.tag == 3)
        {
            
            let alert:UIAlertController=UIAlertController(title: "Select Price App Type", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            
            
            var alert1 = UIAlertAction()
            
            if pickerarray2.count != 0
            {
                for word in pickerarray2
                {
                    alert1 = UIAlertAction(title: (word as! String), style: UIAlertActionStyle.default)
                    {
                        
                        UIAlertAction in
                        self.priceapptypetf.text = (word as! String)
                        
                        
                        
                    }
                    self.defaulfCheck()
                    alert.addAction(alert1)
                    
                    
                }
                
                
                
                
            }
            
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
            {
                UIAlertAction in
                
                
            }
            
            // Add the actions
            //picker?.delegate = self
            
            alert.addAction(cancelAction)
            
            self.present(alert, animated: true, completion: nil)
            
            return false
            
        }
            
            
            
            
        else if (textField.tag == 4)
        {
            PickerView.isHidden = false
            bizcodeRec1()
            
            
            return false
            
        }
            
        else if(textField.tag == 9)
            
        {
            let alert:UIAlertController=UIAlertController(title: "Select Warrenty", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let head2 = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48"]
            
            var alert1 = UIAlertAction()
            
            if head2.count != 0
            {
                for word in head2
                {
                    alert1 = UIAlertAction(title: word, style: UIAlertActionStyle.default)
                    {
                        
                        UIAlertAction in
                        self.warrenrtytf.text = word
                        self.defaulfCheck()
                    }
                    
                    alert.addAction(alert1)
                    
                }
         }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
            {
                UIAlertAction in
                
            }
            
            alert.addAction(cancelAction)
            
            self.present(alert, animated: true, completion: nil)
            
            return false
            
        }
        
        else if(textField.tag == 5)
        {
            
                datepicker.date = NSDate() as Date
                datepickerview.isHidden=false
                datepicker.isHidden = false
                datepicker.datePickerMode = UIDatePickerMode.date
                datepicker.minimumDate = datepicker.date
                return false
                
           
        }
        
        
        
        
        return true
    }
    
    
    func vendorrefno()
    {
        if LibraryAPI.sharedInstance.popupvendor != nil
        {
            if LibraryAPI.sharedInstance.popupvendor != ""
            {
                self.vendorreftf.text = LibraryAPI.sharedInstance.popupvendor
            }
            else
            {
                
            }
        }
        
    }
    
    func vendorrefno_045()
    {
        if LibraryAPI.sharedInstance.SPC_045_str != nil
        {
            if LibraryAPI.sharedInstance.SPC_045_str != ""
            {
                self.vendorreftf.text = LibraryAPI.sharedInstance.SPC_045_str
            }
            else
            {
                
            }
        }
        
    }
    
    
    
    
    func defaulfCheck()
    {
        //let stockroom : [String] = self.stockroomtf.text!.componentsSeparatedByString(" - ")
        let stockroom : [String] = self.stockroomtf.text!.components(separatedBy: " - ")
        let stockroomstr : String = stockroom[0]
        
        let businesscode : [String] = self.businesscode.text!.components(separatedBy: " - ")
        let businesscodestr : String = businesscode[0]
        
        let priceapptype : [String] = self.priceapptypetf.text!.components(separatedBy: " - ")
        let priceapptypestr : String = priceapptype[0]
        
        
        
        self.uploadfile.setObject(businesscodestr, forKey: "BizCode" as NSCopying)
        self.uploadfile.setObject(priceapptypestr, forKey: "PriceappType" as NSCopying )
        self.uploadfile.setObject(stockroomstr, forKey: "StockRoom" as NSCopying)
        self.uploadfile.setObject(self.customercodetf.text!, forKey: "CustomerCode" as NSCopying)
        self.uploadfile.setObject(LibraryAPI.sharedInstance.Userid.stringByRemovingWhitespaces, forKey: "User" as NSCopying)
        self.uploadfile.setObject(validtf.text!, forKey: "ValidityDate" as NSCopying)
        self.uploadfile.setObject(checkchar, forKey: "FreightPrepaid" as NSCopying)
        self.uploadfile.setObject(checkchar1, forKey: "MultipleInvoicingAllowed" as NSCopying)
        self.uploadfile.setObject(vendorreftf.text!, forKey: "VendorReference" as NSCopying)
        self.uploadfile.setObject(endcusponumtf.text!, forKey: "EndCustomerName" as NSCopying)
        self.uploadfile.setObject(warrenrtytf.text!, forKey: "Warranty" as NSCopying)
        self.uploadfile.setObject(paymentdaystf.text!, forKey: "PaymentDays" as NSCopying)
        
        
        self.uploaddata()
        
        
        if self.jsonresult.count > 0
        {
            
            let frieghtprepaid  = (self.jsonresult.object(forKey: "FreightPrepaid_out")as! String)
            
            if(frieghtprepaid == "Y")
            {
                frieghtyesbtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
                frieghtnobtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
            }
            else if(frieghtprepaid == "N")
            {
                frieghtyesbtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
                frieghtnobtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
                
            }
            
            let MultipleInvoicingAllowed  = (self.jsonresult.object(forKey: "MultipleInvoicingAllowed_out")as! String)
            
            if(MultipleInvoicingAllowed == "Y")
            {
                multiyesbtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
                multinobtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
            }
            else if(MultipleInvoicingAllowed == "N")
            {
                multiyesbtn.setImage(UIImage(named:"normal.png"), for: UIControlState.normal)
                multinobtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
            }
            
            let priceApptype = (self.jsonresult.object(forKey: "PriceappType")as! String)
            
            for dicres in self.responsearray2
            {
                let word = dicres as! NSDictionary
                let code = word.object(forKey: "Mode") as! String
                let name = word.object(forKey: "Detail") as! String
                if code == priceApptype
                {
                    LibraryAPI.sharedInstance.priceApptype = code
                    self.priceapptypetf.text = code + " - " + name
                }
            }
            
            Flag1 = (self.jsonresult.object(forKey: "Flag1")as! String)
             Flag2 = (self.jsonresult.object(forKey: "Flag2")as! String)
             Flag3 = (self.jsonresult.object(forKey: "Flag3")as! String)
            
            self.paymentdaystf.text = (self.jsonresult.object(forKey: "PaymentDays_out")as! String)
            
            self.vendorreftf.text = (self.jsonresult.object(forKey: "VendorReference_out")as! String)
            self.warrenrtytf.text = (self.jsonresult.object(forKey: "Warranty_out")as! String)
            self.endcusponumtf.text = (self.jsonresult.object(forKey: "EndCustomerName_out")as! String)
            
            self.stockroomtf.text = self.jsonresult.object(forKey: "StockRoom_out") as! String

            
            
           
   
           usernames = (self.jsonresult.object(forKey: "User")as! String)
       
            
            let errormessage = self.jsonresult.object(forKey: "ErrorMessage")
            let message      = self.jsonresult.object(forKey: "Message")
            
            
            if (message as? String) != ""
            {
                self.showToast(message: message as! String)
            }
            else if (errormessage as? String) != ""
            {
                self.showToast(message: errormessage as! String)
            }
            
        }
    }
    
    
    func uploaddata()
    {
        // http://edi.redingtonb2b.in/Whatsup-staging/Users.asmx/SPCValidation
        
        let request = NSMutableURLRequest(url:NSURL(string:"\(LibraryAPI.sharedInstance.GlobalUrl)Users.asmx/SPCValidation")! as URL)
        request.httpMethod = "POST"
        request.addValue("multipart/form-data", forHTTPHeaderField: "Content-Type")
        request.addValue("multipart/form-data", forHTTPHeaderField: "Accept")
        request.httpBody = try? JSONSerialization.data(withJSONObject: self.uploadfile, options: JSONSerialization.WritingOptions(rawValue: 0))
        let response: AutoreleasingUnsafeMutablePointer<URLResponse?>? = nil
        //  var err: NSError
        do
        {
            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: response)
            jsonresult = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
        }
        catch (let e)
        {
           print(e)
        }
        
    }
    
    
    
    func bizcodeRec()
    {
        self.local_activityIndicator_start()
        // new version dispatch Que
        
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
            //self.responsearray = LibraryAPI.sharedInstance.RequestUrl(url: ("\(LibraryAPI.sharedInstance.GlobalUrl)StockDetails.asmx/BusinessAccessDetails?UserID=\(LibraryAPI.sharedInstance.Userid)"))

           self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)StockDetails.asmx/BusinessAccessDetails?UserID=\(LibraryAPI.sharedInstance.Userid)", completion: { (jsonMutable) in
                self.responsearray = jsonMutable
            
            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
                if(self.responsearray.count != 0)
                {
                    self.pickerarray.removeAllObjects()
                    for dicval in self.responsearray
                    {
                        let word = dicval as! NSDictionary
                        let code = word.object(forKey: "BUSINESS CODE") as! String
                        let desc = word.object(forKey: "BUSINESS DESC") as! String
                        let str = code + " - " + desc
                        self.pickerarray.add(str)
                        
                        
                    }
                }
                else
                {
                 self.local_activityIndicator_stop()
                    
                }

                
            }
        })
        }
       
    }
    
    
    
    
    func Loaddata1(url:String)
    {
        //responsearray1 = LibraryAPI.sharedInstance.RequestUrl(url: url)
        self.getsuccess(params: [:], urlhit: url, completion: { (jsonmutable) in
            self.responsearray1 = jsonmutable
        
        if(self.responsearray1.count > 0)
        {
            
        }
        self.pickerarray1.removeAllObjects()
        for i in 0..<self.responsearray1.count
        {
            //  if(currenttf=="business")
            // {
            let dic = self.responsearray1[i] as! NSDictionary
            let code = dic.object(forKey: "StockRoom Code") as? String
            let desc = dic.object(forKey: "StockRoom Name") as? String
            
           
            let str=code?.appending(" - \(desc!)")
            self.pickerarray1.add(str!)
            
            // }
        }
            })
    }
    
    
    
    func bizcodeRec1()
    {
        
            ///let size = CGSize(width: 30, height:30)
        selectedrow=0
        self.currentpicker = "stockroom"
        self.local_activityIndicator_start()

        
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
            
            self.Loaddata1(url: "\(LibraryAPI.sharedInstance.GlobalUrl)customer.asmx/Stockroomsearch_SPC?Userid=\(LibraryAPI.sharedInstance.Userid)")
            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
                self.Picker.reloadAllComponents()
                
            }
        }
  
    }
    func Loaddatacity(url:String)
    {
        self.getsuccess(params: [:], urlhit: url, completion: { (jsonmutable) in
            self.responsearray2 = jsonmutable
       
        if(self.responsearray2.count > 0)
        {
            
        }
        self.pickerarray2.removeAllObjects()
        for i in 0  ..< self.responsearray2.count
        {
           let dic1 = self.responsearray2[i] as! NSDictionary
            let code = dic1.object(forKey: "Mode") as? String
            let desc = dic1.object(forKey: "Detail") as? String
            let str=code?.appending(" - \(desc!)")
            self.pickerarray2.add(str!)
            
         
        }
         })
        
    }
    
    
    
    func cityrec()
    {
            //let size = CGSize(width: 30, height:30)
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
             self.Loaddatacity(url: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/FieldDescription?Userid=\(LibraryAPI.sharedInstance.Userid)&prmt=DIST")
            
            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
               self.local_activityIndicator_stop()
                
            }
        }
   
    }
    
    
    @IBAction func FrieghtyesbtnAction(_ sender: AnyObject)
    {
        
        checkchar = "Y"
        
        frieghtyesbtn.setImage(UIImage(named:"green2.png"), for: UIControlState.normal)
        frieghtnobtn.setImage(UIImage(named:"normal.png"),  for: UIControlState.normal)
        
    }
    
    
    
    @IBAction func FrieghtNobtnaction(_ sender: AnyObject)
    {
        
        checkchar = "N"
        
        frieghtyesbtn.setImage(UIImage(named:"normal.png"),  for: UIControlState.normal)
        frieghtnobtn.setImage(UIImage(named:"green2.png"),  for: UIControlState.normal)
        
        
    }
    
    @IBAction func multiyesbtn(_ sender: AnyObject)
    {
        
        checkchar1 = "Y"
        
        multiyesbtn.setImage(UIImage(named:"green2.png"),  for: UIControlState.normal)
        multinobtn.setImage(UIImage(named:"normal.png"),  for: UIControlState.normal)
        
    }
    
    
    
    
    @IBAction func multinobtnaction(_ sender: AnyObject)
    {
        checkchar1 = "N"
        
        multiyesbtn.setImage(UIImage(named:"normal.png"),  for: UIControlState.normal)
        multinobtn.setImage(UIImage(named:"green2.png"),  for: UIControlState.normal)
        
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        textField.resignFirstResponder()
        
//        if (textField.tag == 7)
//        {
//            defaulfCheck()
//        }
         if(textField.tag == 8)
        {
            defaulfCheck()
        }
        else if(textField.tag == 6)
        {
            defaulfCheck()
        }
//        else if(textField.tag == 5)
//        {
//            defaulfCheck()
//        }

        
        
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
   
        if(textField.tag==9)
        {
            guard let text = textField.text else { return true }
            
            let newLength = text.count + string.count - range.length
            return newLength <= 2 // Bool
        }
        
        if(textField.tag==7)
        {
            guard let text = textField.text else { return true }
            
            let newLength = text.count + string.count - range.length
            return newLength <= 12 // Bool
        }
        
        if(textField.tag == 8)
        {
            //if string.rangeOfCharacterFromSet(NSCharacterSet.letterCharacterSet()) != nil {
                if string.rangeOfCharacter(from: .letters) != nil{
                guard let text = textField.text else { return true }
                let newLength = text.count + string.count - range.length
                return newLength  <= 20
                
            }
            else
            {
                let  char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    return true
                }
                
                return false
                
            }
  
        }
        
        if(textField.tag == 8)
        {
            guard let text = textField.text else { return true }
            
            let newLength = text.count + string.count - range.length
            return newLength <= 16
            
        }
        return true
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if(pickerarray1.count > 0)
        {
            return 1
        }
        else
        {
            return 0
        }
    }
 
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var num = Int()
        num=pickerarray1.count
        return num
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
     
        
        return  pickerarray1.object(at: row) as! String
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedrow = row
    }
    
    @IBAction func CancelAction(_ sender: AnyObject)
    {
        PickerView.isHidden=true
    }
    
    @IBAction func DoneAction(_ sender: AnyObject)
    {
        if(currentpicker=="stockroom")
        {
            
            
            stockroomtf.text=pickerarray1.object(at: selectedrow) as? String
            self.defaulfCheck()
            PickerView.isHidden=true
        }
        
    }
    
    
    
    
    
    
    func notificationmethod()
    {
        let userdefaults = Foundation.UserDefaults.standard
        let value = userdefaults.dictionary(forKey: "Notification")
        
        var title: String!
        var message: String!
        var imagestr: String!
        
        title = value?["title"] as! String
        message = value?["body"] as! String
        imagestr = value?["imageurl"] as! String
        
        if imagestr != nil
        {
            if imagestr != ""
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: notificationimageViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = notificationimageViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                    
                }
                popup.show(childViewController: container)
            }
            else
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: PushnotificationtextViewController.instance())
                    
                    .didShowHandler { popup in
                        
                    }
                    .didCloseHandler { _ in
                }
                let container = PushnotificationtextViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                }
                
                popup.show(childViewController: container)
            }
        }
        else
        {
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: PushnotificationtextViewController.instance())
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
            }
            let container = PushnotificationtextViewController.instance()
            container.closeHandler = { _ in
                popup.dismiss()
            }
            popup.show(childViewController: container)
        }
    }
    
    func customernamedata()
    {
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer360.asmx/Customersearch360?Customer=\(LibraryAPI.sharedInstance.CustomerCode)&Type=1&Userid=\(LibraryAPI.sharedInstance.Userid)&Branch=", completion: { (jsonMutable) in
            self.responsestring = jsonMutable
        if(self.responsestring.count>0)
        {
            //self.CustomerName.text=String(self.responsestring.objectAtIndex(0).objectForKey("Customer Name") as! String)
            let dicress = self.responsestring[0] as! NSDictionary
            let custname = String(dicress.object(forKey: "Customer Name") as! String)
            LibraryAPI.sharedInstance.spccustname  = custname!
        } else {
                    
        }
        })
    }
    
    func tempstorage()
    {
        LibraryAPI.sharedInstance.globaldic.setObject(customercodetf.text!, forKey: "customercode" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(businesscode.text!, forKey: "businesscode" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(priceapptypetf.text!, forKey: "priceapp" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(stockroomtf.text!, forKey: "stockroom" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(validtf.text!, forKey: "validity" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(paymentdaystf.text!, forKey: "payment" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(checkchar, forKey: "frieght" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(checkchar1, forKey: "multi" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(warrenrtytf.text!, forKey:"warrenty" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(vendorreftf.text!, forKey: "vendor" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(endcusponumtf.text!, forKey: "endcustomername" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(usernames, forKey:"usernames" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(Flag1, forKey: "flag1spc" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(Flag2, forKey: "flag2spc" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(Flag3, forKey: "flag3spc" as NSCopying)
    }
    
    
    //spcentry screen value existing display
    func gettempstorage()
    {
        if LibraryAPI.sharedInstance.globaldic.object(forKey: "customercode") != nil
        {
            customercodetf.text = LibraryAPI.sharedInstance.globaldic.object(forKey: "customercode") as?  String
            businesscode.text = LibraryAPI.sharedInstance.globaldic.object(forKey: "businesscode") as?  String
            priceapptypetf.text = LibraryAPI.sharedInstance.globaldic.object(forKey: "priceapp") as?  String
            stockroomtf.text = LibraryAPI.sharedInstance.globaldic.object(forKey: "stockroom") as?  String
            validtf.text = LibraryAPI.sharedInstance.globaldic.object(forKey: "validity") as?  String
            paymentdaystf.text = LibraryAPI.sharedInstance.globaldic.object(forKey: "payment") as?  String
            checkchar = LibraryAPI.sharedInstance.globaldic.object(forKey: "frieght") as?  String
            checkchar1 = LibraryAPI.sharedInstance.globaldic.object(forKey: "multi") as?  String
            vendorreftf.text = LibraryAPI.sharedInstance.globaldic.object(forKey: "vendor") as?  String
            endcusponumtf.text = LibraryAPI.sharedInstance.globaldic.object(forKey: "endcustomername") as?  String
        }
    }
    
    @IBAction func Done1btnAction(_ sender: AnyObject)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "yyyy-MM-dd"
        //"MM-dd-yy"
        let strDate = dateFormatter.string(from: datepicker.date)
        validtf.text=strDate
        datepickerview.isHidden = true
        defaulfCheck()
    }
    
    @IBAction func cancelAction(_ sender: AnyObject)
    {
        datepickerview.isHidden=true
    }
 }
