//
//  StockEnquiryViewController.swift
//  Redington
//
//  Created by truetech on 29/08/16.
//  Copyright © 2016 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView
import KCFloatingActionButton

class StockTransferStatusViewController: UIViewController,KCFloatingActionButtonDelegate,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource {
    
    @IBOutlet var Locationtf: UITextField!
    @IBOutlet var Businesstf: UITextField!
    @IBOutlet var Picker: UIPickerView!
    @IBOutlet var PickerView: UIView!
    @IBOutlet var Submitbtn: UIButton!
    var fab = KCFloatingActionButton()
    var selectedrow=Int()
    var selectedrow1=Int()
    var currentpicker=String()
    var pickerarray=NSMutableArray()
    var responsearray=NSMutableArray()
    var businesscode=String()
    var stock=String()
    var responsestring1  = NSMutableArray()
    var responsestring = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("StockTransferStatusViewController")
        self.Submitbtn.layer.cornerRadius=5
        self.customnavigation()
        self.layoutFAB()
        Locationtf.layer.cornerRadius = 5.0
        Locationtf.layer.borderWidth = 1.0
        self.Locationtf.layer.borderColor = UIColor(red:211/255.0, green:211/255.0, blue:211/255.0, alpha: 1.0).cgColor
        Businesstf.layer.cornerRadius = 5.0
        Businesstf.layer.borderWidth = 1.0
        self.Businesstf.layer.borderColor = UIColor(red:211/255.0, green:211/255.0, blue:211/255.0, alpha: 1.0).cgColor
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func itemcode()
    {
        if(LibraryAPI.sharedInstance.Stocktransferitemcode != nil) {
            Locationtf.text = LibraryAPI.sharedInstance.Stocktransferitemcode
        }
    }
    
    func customnavigation() {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        customView.MenuBtn.addTarget(self, action: #selector(StockTransferStatusViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Stock Transfer Status"
    }
    
    
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            self.fab.close()
        }
        fab.fabDelegate = self
        self.view.addSubview(fab)
    }
    
    func Loaddata(url:String) {
        self.local_activityIndicator_start()
        self.getsuccess(params: [:], urlhit: url, completion: { (jsonmutable) in
        self.responsearray = jsonmutable
        if( self.responsearray.count>0) {
            self.pickerarray.removeAllObjects()
        for i in 0..<self.responsearray.count {
            if( self.currentpicker=="business") {
                let dic =  self.responsearray[i] as! NSDictionary
                let code = dic.object(forKey: "BUSINESS CODE") as? String
                let desc = dic.object(forKey: "BUSINESS DESC") as? String
                let str=code?.appending(" - \(desc!)")
                 self.pickerarray.add(str!)
                self.Picker.reloadAllComponents()
            }
        }
            self.local_activityIndicator_stop()
            }
             })
    }
    
    func KCFABOpened(fab: KCFloatingActionButton) {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        itemcode()
    }
    
    
    @IBAction func submitaction(_ sender: AnyObject) {
        if Businesstf.text! as String == "" {
           
            self.NormalAlert(title: "Sorry..!!", Messsage: "Enter Business Field")
        } else if Locationtf.text! as String == "" {
          
            self.NormalAlert(title: "Sorry..!!", Messsage: "Enter Itemcode Field")
        } else if Locationtf.text! as String == "" && Businesstf.text! as String == "" {
         
            self.NormalAlert(title: "Sorry..!!", Messsage: "Enter Business & itemcode Field")
        } else {
            let fullName = Businesstf.text
            let fullNameArr = fullName!.split{$0 == " "}.map(String.init)
            businesscode=(fullNameArr[0])
            LibraryAPI.sharedInstance.Stocktransferbcode = businesscode
                //let fullName1 = Locationtf.text
            LibraryAPI.sharedInstance.Stocktransferbcode = Businesstf.text
            self.local_activityIndicator_start()
            DispatchQueue.global(qos: .background).async {
                self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)stockdetails.ASMX/StockTransferStatus?Itemcode=\(LibraryAPI.sharedInstance.Stocktransferitemcode.stringByRemovingWhitespaces)&UserID=\(LibraryAPI.sharedInstance.Userid)", completion: { (jsonmutable) in
                     self.responsestring1  = jsonmutable
                    print("data in stock transfer :",self.responsestring1)
                DispatchQueue.main.async {
                    if(self.responsestring1.count > 0) {
                        if (self.responsestring1.count == 1) {
                            if((self.responsestring1.object(at: 0) as AnyObject).object(forKey: "Message") != nil) {
                                self.local_activityIndicator_stop()
                                let alertController = UIAlertController(title: "Alert", message: "No Data Available", preferredStyle: .alert)
                                let cancelAction = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction!) in
                                    LibraryAPI.sharedInstance.Stocktransferitemcode = ""
                                    self.Locationtf.text = ""
                                    self.Businesstf.text = ""
                                }
                                alertController.addAction(cancelAction)
                                self.present(alertController, animated: true, completion:nil)
                            }
                        } else {
                            self.local_activityIndicator_stop()
                            self.Locationtf.text = ""
                            self.Businesstf.text = ""
                            LibraryAPI.sharedInstance.Stocktransferdatas = self.responsestring1
                            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "StocktransferdetailsViewController")), animated: true)
                        }
                    } else {
                        self.Locationtf.text = ""
                        self.Businesstf.text = ""
                        LibraryAPI.sharedInstance.Stocktransferitemcode = ""
                        self.local_activityIndicator_stop()
                        self.NormalAlert(title: "Sorry..!!", Messsage: "No Data Available!")
                    }
                }
                    })
            }
        }
    }
    
    func buttonClicked(sender:UIButton) {
        let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
        self.navigationController?.pushViewController(lvc, animated: false)
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var num = Int()
        num=pickerarray.count
        return num
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(pickerarray.count > 0) {
            return  pickerarray.object(at: row) as? String
        } else {
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedrow = row
        LibraryAPI.sharedInstance.Stocktransferbcode = Businesstf.text
    }
    
    @IBAction func CancelAction(_ sender: AnyObject) {
        PickerView.isHidden=true
    }
    
    @IBAction func DoneAction(_ sender: AnyObject) {
        if(currentpicker=="business") {
            Businesstf.text=pickerarray.object(at: selectedrow) as? String
            let fullName = Businesstf.text
            let fullNameArr = fullName!.split{$0 == " "}.map(String.init)
            
            businesscode=(fullNameArr[0])
            LibraryAPI.sharedInstance.Stocktransferbcode = businesscode
        }
        PickerView.isHidden=true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        fab.isHidden=true
        if(pickerarray.count>0) {
            pickerarray.removeAllObjects()
        }
        if(textField.tag==2) {
            selectedrow=0
            selectedrow1 = 1
            //let size = CGSize(width: 30, height:30)
            self.currentpicker="business"
            self.local_activityIndicator_start()
            DispatchQueue.global(qos: .background).async {
                self.Loaddata(url: "\(LibraryAPI.sharedInstance.GlobalUrl)StockDetails.asmx/BusinessAccessDetails?UserID=\(LibraryAPI.sharedInstance.Userid)")
                DispatchQueue.main.async {
                    self.Picker.reloadAllComponents()
                    self.PickerView.isHidden=false
                }
            }
            return false
        }
        if(textField.tag == 1) {
            PickerView.isHidden = true
            if(Businesstf.text == "") {
                self.showToast(message: "Please Enter the Business Code")
            } else {
                self.local_activityIndicator_start()
                DispatchQueue.global(qos: .background).async {
                    print("data in stocktransfercode :",LibraryAPI.sharedInstance.Stocktransferbcode)
                    var Stocktransferbcodeval = String(LibraryAPI.sharedInstance.Stocktransferbcode)!
                    self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/itemCodeSearch_SPC?User=\(LibraryAPI.sharedInstance.Userid.stringByRemovingWhitespaces)&PGM=\(Stocktransferbcodeval)&Record=20&Page=\(1)", completion: { (jsonmutablearray) in
                        let  ItemcodeArray = jsonmutablearray
                        DispatchQueue.main.async {
                            if ItemcodeArray.count > 0 {
                                self.local_activityIndicator_stop()
                                let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "Itemcode1PopupViewController") as! Itemcode1PopupViewController
                                self.navigationController?.pushViewController(lvc, animated: false)
                            } else {
                                self.local_activityIndicator_stop()
                                self.showToast(message: "No Data Available")
                            }
                        }
                    })
                }
            }
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        Locationtf.resignFirstResponder()
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        fab.isHidden=false
        textField.resignFirstResponder()
        return true
    }
}
