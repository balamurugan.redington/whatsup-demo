//
//  Preorder.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 19/02/18.
//  Copyright © 2018 truetech. All rights reserved.
//

import Foundation

struct Preorder {
    
    /*{
     "ITEMCODE": "HPSP0004",
     "VNDRCODE": "51626AA",
     "ITEMDESC": "51626AA-26A BLACK LARGE",
     "UNITPRIC": 0,
     "AVAILQTY": 357,
     "STKROOM": "C0",
     "BRANCH CODE": "",
     "RECORD NUMBER": 1,
     "PAGE NUMBER": 1,
     "TOTAL PAGE": 5
     }*/
    
    var ITEMCODE : String
    var VNDRCODE : String
    var ITEMDESC : String
    var UNITPRIC : String
    var AVAILQTY : NSNumber
    var STKROOM : String
    var BRANCH_CODE : String
    var RECORD_NUMBER : NSNumber
    var PAGE_NUMBER : NSNumber
    var TOTAL_PAGE : NSNumber
    
    
    init() {
        ITEMCODE = ""
        VNDRCODE = ""
        ITEMDESC = ""
        UNITPRIC = ""
        AVAILQTY = 0
        STKROOM = ""
        BRANCH_CODE = ""
        RECORD_NUMBER = 0
        PAGE_NUMBER = 0
        TOTAL_PAGE = 0
        
    }
    
    init(data : AnyObject) {
        
        if let temp = data["ITEMCODE"] as? String {
            self.ITEMCODE = temp
        }else {
            self.ITEMCODE = ""
        }
        
        if let temp = data["VNDRCODE"] as? String {
            self.VNDRCODE = temp
        }else {
            self.VNDRCODE = ""
        }
        
        if let temp = data["ITEMDESC"] as? String {
            self.ITEMDESC = temp
        }else {
            self.ITEMDESC = ""
        }
        
        if let temp = data["UNITPRIC"] as? String {
            self.UNITPRIC = temp
        }else {
            self.UNITPRIC = ""
        }
        
        if let temp = data["AVAILQTY"] as? NSNumber {
            self.AVAILQTY = temp
        }else {
            self.AVAILQTY = 0
        }
        
        
        if let temp = data["STKROOM"] as? String {
            self.STKROOM = temp
        }else {
            self.STKROOM = ""
        }
        
        if let temp = data["BRANCH CODE"] as? String {
            self.BRANCH_CODE = temp
        }else {
            self.BRANCH_CODE = ""
        }
        
        if let temp = data["RECORDNUMBER"] as? NSNumber {
            self.RECORD_NUMBER = temp
        }else {
            self.RECORD_NUMBER = 0
        }
        
        if let temp = data["PAGENUMBER"] as? NSNumber {
            self.PAGE_NUMBER = temp
        }else {
            self.PAGE_NUMBER = 0
        }
        
        if let temp = data["TOTALPAGE"] as? NSNumber {
            self.TOTAL_PAGE = temp
        }else {
            self.TOTAL_PAGE = 0
        }


    }
    
}
