//
//  StocktransferdetailsViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 26/07/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class StocktransferdetailsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var stocktransfertableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("StocktransferdetailsViewController")
        self.customnavigation()
        stocktransfertableview.delegate = self
        stocktransfertableview.dataSource = self
        stocktransfertableview.register(UINib(nibName: "StocktransferTableViewCell", bundle: nil), forCellReuseIdentifier: "StocktransferTableViewCell")
        stocktransfertableview.tableFooterView = UIView(frame: .zero)
        stocktransfertableview.separatorStyle = .none
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.stocktransfertableview.reloadData()
    }
    
    func customnavigation() {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65)
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(StocktransferdetailsViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Stock Transfer Status"
        
    }
    
    func buttonClicked(sender:UIButton) {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "StockTransferStatusViewController")), animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if LibraryAPI.sharedInstance.Stocktransferdatas.count > 0 {
            return 1
            
        } else {
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return LibraryAPI.sharedInstance.Stocktransferdatas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StocktransferTableViewCell") as! StocktransferTableViewCell
        if (LibraryAPI.sharedInstance.Stocktransferdatas.count > 0) {
            if(indexPath.row % 2 == 0) {
                cell.contentView.backgroundColor=(UIColor.init(red: 240/255.0, green: 246/255.0, blue: 225/255.0, alpha: 1.0))
            } else {
                //cell.contentView.backgroundColor=UIColor.whiteColor()
                cell.contentView.backgroundColor=(UIColor.init(red: 215/255.0, green: 250/255.0, blue: 252/255.0, alpha: 1.0))
            }
            let dic = LibraryAPI.sharedInstance.Stocktransferdatas[indexPath.row] as! NSDictionary
            cell.Ordernolbl.text = dic.object(forKey: "Order Number") as? String
            cell.invoicenolbl.text = dic.object(forKey: "Invoice Number")as? String
            cell.fromstockroomlbl.text = dic.object(forKey: "From Stock Room")as? String
            cell.tostockroomlbl.text = dic.object(forKey: "To Stock Room")as? String
        } else {
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        LibraryAPI.sharedInstance.Stocktransfepopuporderno = LibraryAPI.sharedInstance.Stocktransferdatas.object(at: indexPath.row) as! NSDictionary
        let popup = PopupController
            .create(parentViewController: self.parent!)
            .show(childViewController: StockTransferPopupViewController.instance())
            .didShowHandler { popup in
            }
            .didCloseHandler { _ in
                
        }
        let container = StockTransferPopupViewController.instance()
        container.closeHandler = { _ in
            popup.dismiss()
        }
        popup.show(childViewController: container)
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
    }
}
