//
//  ChequePendingTableViewCell.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 02/08/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class ChequePendingTableViewCell: UITableViewCell {

    
    @IBOutlet weak var InvNo: UILabel!
    
    @IBOutlet weak var InvVal: UILabel!
    
    @IBOutlet weak var InvDt: UILabel!
    
    @IBOutlet weak var duedate: UILabel!
    
    @IBOutlet weak var OdDays: UILabel!
    @IBOutlet weak var pendingdays: UILabel!
    
    /*@IBOutlet var Biz: UILabel!
     @IBOutlet var InvNo: UILabel!
     @IBOutlet var InvVal: UILabel!
     @IBOutlet var InvDt: UILabel!
     @IBOutlet var OdDays: UILabel!
     
     @IBOutlet weak var pendingdays: UILabel!
     @IBOutlet weak var duedate: UILabel!*/
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
