//
//  LeaveStatusEnquiryViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 22/05/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit
import KCFloatingActionButton
class LeaveStatusEnquiryViewController: UIViewController,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,KCFloatingActionButtonDelegate
{
    
    @IBOutlet weak var selecttxtfield: UITextField!
    @IBOutlet weak var fromdatetf: UITextField!
    @IBOutlet weak var todatetf: UITextField!
    @IBOutlet weak var submitbtn: UIButton!
    @IBOutlet weak var canceltableview: UITableView!
    @IBOutlet weak var tableviewview: UIView!
    @IBOutlet weak var Datepicker: UIDatePicker!
    var currenttextfield: String!
    var responsestring = NSMutableArray()
    var  CustomerResponse = NSMutableArray()
    var  response1 = NSMutableArray()
    var selectedstring: String!
    var fab=KCFloatingActionButton()
  
    override func viewDidLoad()
    {
        super.viewDidLoad()
        if LibraryAPI.sharedInstance.Globalflag == "N"
        {
            customnavigation1()
        }
        else
        {
            customnavigation()
            self.layoutFAB()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationmethod), name: NSNotification.Name(rawValue: "GetNotification"), object: nil)
        self.submitbtn.layer.borderWidth = 0.5
        submitbtn.layer.cornerRadius=5.0
        tableviewview.layer.masksToBounds=true
        tableviewview.layer.cornerRadius=5.0
        self.tableviewview.layer.borderWidth = 0.5
        canceltableview.layer.masksToBounds=true
        canceltableview.layer.cornerRadius=5.0
        self.canceltableview.layer.borderWidth = 0.5
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        fromdatetf.tag = 1
        todatetf.tag = 2
        canceltableview.tableFooterView = UIView(frame: .zero)
        let arrow = UIImageView()
        let image = UIImage(named: "cal.png")
        arrow.image = image!.imageWithColor(tintColor: UIColor.init(red:239/255.0 , green: 88/255.0, blue: 88/255.0, alpha: 1.0)).withRenderingMode(.alwaysOriginal)
        arrow.frame = CGRect(x: 0.0, y: 0.0, width: arrow.image!.size.width+10.0, height: 15);
        arrow.contentMode = UIViewContentMode.center
        self.fromdatetf.rightView = arrow;
        self.fromdatetf.rightViewMode = UITextFieldViewMode.always
        let arrow1 = UIImageView()
        let image3 = UIImage(named: "cal.png")
        arrow1.image = image3!.imageWithColor(tintColor: UIColor.init(red:239/255.0 , green: 88/255.0, blue: 88/255.0, alpha: 1.0)).withRenderingMode(.alwaysOriginal)
        arrow1.frame = CGRect( x: 0.0, y: 0.0, width: arrow.image!.size.width+10.0, height: 15);
        arrow1.contentMode = UIViewContentMode.center
        self.todatetf.rightView = arrow1;
        self.todatetf.rightViewMode = UITextFieldViewMode.always
        fromdatetf.delegate = self
        todatetf.delegate  = self
        if selectedstring != nil
        {
            selecttxtfield.text = selectedstring
        }
        else
        {
             selecttxtfield.text = ""
        }
        
        
         canceltableview.register(UINib(nibName: "LeaveTableViewCell", bundle: nil), forCellReuseIdentifier: "LeaveTableViewCell")

        
        }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Loaddata()
    }
    func layoutFAB() {
        fab.addItem("Dashboard", icon: UIImage(named: "dash.png")!) { item in
            
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
            
            
            
            self.fab.close()
        }
        
        fab.addItem("Refresh", icon: UIImage(named: "re.png")!) { item in
            
            let slideInFromLeftTransition = CATransition()
            slideInFromLeftTransition.type = kCAGravityTop
            slideInFromLeftTransition.subtype = kCATransitionFromLeft
            slideInFromLeftTransition.duration = 0.2
            slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
            self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "LeaveStatusEnquiryViewController")), animated: true)
            self.fab.close()
        }
        
        
        fab.fabDelegate = self
        
        self.view.addSubview(fab)
        
    }
    func KCFABOpened(fab: KCFloatingActionButton) {
        
    }

    
    func customnavigation()
        
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect( x: self.view.frame.origin.x, y:  self.view.frame.origin.y, width: self.view.frame.width, height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(LeaveStatusEnquiryViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Status Enquiry/cancellation"
        
    }
    
    func customnavigation1()
        
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect( x: self.view.frame.origin.x, y:  self.view.frame.origin.y, width: self.view.frame.width, height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        //customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), forState: UIControlState.Normal)
        customView.MenuBtn.addTarget(self, action: #selector(LeaveStatusEnquiryViewController.buttonClicked),for:UIControlEvents.touchUpInside)
       // customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),forControlEvents:UIControlEvents.TouchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.textAlignment = .center
        customView.HeaderLabel.font = UIFont.boldSystemFont(ofSize: 16.0)
        customView.HeaderLabel.text = "Status Enquiry/cancellation"
        
    }

    
    func buttonClicked(sender:UIButton)
    {
        
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "LeaveViewController")), animated: true)
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {

        if (textField.tag == 1)
        {
           // pickerview.isHidden = false
            fromdatetf.resignFirstResponder()
             todatetf.resignFirstResponder()
            currenttextfield="From"
            selecttxtfield.text = ""
            return false

        }
        if(textField.tag == 2)
        {
            if (fromdatetf.text != "")
            {
               // pickerview.isHidden = false
                fromdatetf.resignFirstResponder()
                todatetf.resignFirstResponder()
                let formatter = DateFormatter()
                formatter.dateFormat = "MM-dd-yyyy"
                Datepicker.minimumDate = formatter.date(from: self.fromdatetf.text!)
                currenttextfield = "to"
                selecttxtfield.text = ""
                return false
            }
            else
            {
                let alertController = UIAlertController(title: "Alert", message: "Enter the From Date", preferredStyle: .alert)
                
                
                let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                    
                }
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion:nil)
                

                return false
            }
           
        }
        return true

    }


    @IBAction func doneaction(_ sender: AnyObject)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.full
        if(currenttextfield=="From")
        {
            dateFormatter.dateFormat = "MM-dd-yyyy"
            
        }
        else if(currenttextfield=="to")
        {
            dateFormatter.dateFormat = "MM-dd-yyyy"
        }
        if(currenttextfield=="to")
        {
            let strDate = dateFormatter.string(from: Datepicker.date)
            
            todatetf.text=strDate
        }
        else if(currenttextfield=="From")
        {
            let strDate = dateFormatter.string(from: Datepicker.date)
            
            fromdatetf.text=strDate
        }
        
       // pickerview.isHidden=true
    }
  
    
    
    @IBAction func cancelaction(_ sender: AnyObject)
    {
       // pickerview.isHidden=true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        if(textField.tag==1)
        {
            fromdatetf.resignFirstResponder()
            
        }
        else if (textField.tag == 2)
        {
            todatetf.resignFirstResponder()
        }
        return true
    }
    
    func Loaddata()
    {
        self.local_activityIndicator_start()
       self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)/LeaveDetails.asmx/LeaveEnquiry?UserID=\(LibraryAPI.sharedInstance.Userid)&FromDate=&ToDate=", completion: { (jsonmutable) in
            self.responsestring = jsonmutable
        
        if(self.responsestring.count>0)
        {
           self.canceltableview.isHidden = false
             self.local_activityIndicator_stop()
            LibraryAPI.sharedInstance.leaveappno = self.responsestring
            self.canceltableview.reloadData()
        }

        
        else
        {
            let alertController = UIAlertController(title: "Alert", message: "No Data Available", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { (action:UIAlertAction!) in
                
            }
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion:nil)
             self.local_activityIndicator_stop()
        }
        })
    }
    
    
    //TableView Delegates
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        if (responsestring.count > 0)
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if (responsestring.count > 0)
        {
            return responsestring.count
        }
        else
        {
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeaveTableViewCell", for: indexPath) as! LeaveTableViewCell
       
        let dic = responsestring[indexPath.row] as! NSDictionary
        
        cell.appnolbl.text=String(dic.object(forKey: "Leave Appl Number") as! String)
        cell.fromdatelbl.text=String(dic.object(forKey: "From Date") as! String)
        cell.todatelbl.text=String(dic.object(forKey: "To Date") as! String)
        cell.noofdayslbl.text=String(describing: dic.object(forKey: "No of Days") as! NSNumber)
        cell.leavetypelbl.text=String(dic.object(forKey: "Leave Type") as! String)
        return cell 
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        
        LibraryAPI.sharedInstance.leaveappnumber = responsestring.object(at: indexPath.row) as! NSDictionary 
        
        let popup = PopupController
            .create(parentViewController: self.parent!)
            .show(childViewController: PopupwindowleaveViewController.instance())
            
            .didShowHandler { popup in
            }
            .didCloseHandler { _ in
                
        }
        let container = PopupwindowleaveViewController.instance()
        container.closeHandler = { _ in
            popup.dismiss()
        }
        
        
        popup.show(childViewController: container)
         
        
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
    }

    
    @IBAction func appnoclickaction(_ sender: AnyObject)
    {
        fromdatetf.text = ""
        todatetf.text = ""
        Loaddata()
        canceltableview.reloadData()
        
        let popup = PopupController
            .create(parentViewController: self.parent!)
            .show(childViewController: PopupAppnumberViewController.instance())
            
            .didShowHandler { popup in
            }
            .didCloseHandler { _ in
                
        }
        let container = PopupAppnumberViewController.instance()
        container.closeHandler = { _ in
            popup.dismiss()
        }
        
        
        popup.show(childViewController: container)

    }
    
    
    
    
    @IBAction func Submitaction(_ sender: AnyObject)
    {
        if (self.fromdatetf.text == ""  )
        {
            self.NormalAlert(title: "Alert", Messsage: "Please Enter the From Date ")
          
        }
        else if (self.todatetf.text == "" )
        {
             self.NormalAlert(title: "Alert", Messsage: "Please Enter the ToDate")
            
        }
        else if (self.todatetf.text == "" && self.fromdatetf.text == "" )
        {
            self.NormalAlert(title: "Alert", Messsage: "Please Enter the From Date And To Date")


            
        }

        else
        {
           // responsestring = LibraryAPI.sharedInstance.RequestUrl(url: "\(LibraryAPI.sharedInstance.GlobalUrl)/LeaveDetails.asmx/LeaveEnquiry?UserID=\(LibraryAPI.sharedInstance.Userid.stringByRemovingWhitespaces)&FromDate=\(fromdatetf.text!)&ToDate=\(todatetf.text!)")
            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)/LeaveDetails.asmx/LeaveEnquiry?UserID=\(LibraryAPI.sharedInstance.Userid.stringByRemovingWhitespaces)&FromDate=\(fromdatetf.text!)&ToDate=\(todatetf.text!)", completion: { (jsonmutable) in
                self.responsestring = jsonmutable
            
                if(self.responsestring.count>0)
                {
                   
                    self.canceltableview.reloadData()
                    
                 }
                    
                    
                else
                {
                    self.NormalAlert(title: "Alert", Messsage: "No Data Available")
                
                 
                    
                }
               })
            
        }
    }
    
    func notificationmethod()
    {
        let userdefaults = Foundation.UserDefaults.standard
        let value = userdefaults.dictionary(forKey: "Notification")
        
        var title: String!
        var message: String!
        var imagestr: String!

        title = value?["title"] as! String
        message = value?["body"] as! String
        imagestr = value?["imageurl"] as! String
        
        if imagestr != nil
        {
            if imagestr != ""
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: notificationimageViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = notificationimageViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                    
                }
                popup.show(childViewController: container)
            }
            else
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: PushnotificationtextViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = PushnotificationtextViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                }
                
                popup.show(childViewController: container)
            }
        }
        else
        {
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: PushnotificationtextViewController.instance())
                
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
            }
            let container = PushnotificationtextViewController.instance()
            container.closeHandler = { _ in
                popup.dismiss()
            }
            
            popup.show(childViewController: container)
        }
        
       
        
    }
    

}
