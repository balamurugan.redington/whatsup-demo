//
//  EndDetails2ViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 22/06/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit
 //import NVActivityIndicatorView

class EndDetails2ViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var customercodelbl: UILabel!
    @IBOutlet weak var businesscodelbl: UILabel!
    @IBOutlet weak var endcustomernametf: UITextField!
    @IBOutlet weak var address1tf: UITextField!
    @IBOutlet weak var address2tf: UITextField!
    @IBOutlet weak var address3tf: UITextField!
    @IBOutlet weak var citytf: UITextField!
    @IBOutlet weak var citycitylable: UILabel!
    @IBOutlet weak var pincodelbl: UITextField!
    @IBOutlet weak var contactnametf: UITextField!
    @IBOutlet weak var telephonetf: UITextField!
    @IBOutlet weak var emailidtf: UITextField!
    @IBOutlet weak var skutype: UITextField!
    @IBOutlet weak var ordertype: UITextField!
    @IBOutlet weak var Scrollview: UIScrollView!
    @IBOutlet weak var industrysegmenttf: UITextField!
    @IBOutlet weak var mainview: UIView!
    @IBOutlet weak var statetf: UITextField!
    
    
    var sku=NSMutableArray()
    var pickerarray=NSMutableArray()
    var order = NSMutableArray()
    var pickerarray1 = NSMutableArray()
    var Movedup=Bool()

    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        LibraryAPI.sharedInstance.spcstr = "01"
         Movedup=false
        self.Scrollview.contentSize = mainview.frame.size
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.customnavigation()
        self.cityrec()
        self.cityrec1()
        
        
        endcustomernametf.delegate = self
        address3tf.delegate = self
        address2tf.delegate = self
        address1tf.delegate = self
        citytf.delegate = self
        statetf.delegate = self
        pincodelbl.delegate = self
        contactnametf.delegate = self
        telephonetf.delegate = self
        emailidtf.delegate = self
        skutype.delegate = self
        ordertype.delegate = self
        industrysegmenttf.delegate = self
        
        //tags
        citytf.tag = 1
        skutype.tag = 2
        
        ordertype.tag = 3
        pincodelbl.tag = 4
        telephonetf.tag = 5
        
        NotificationCenter.default.addObserver(self, selector: #selector(EndDetails2ViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(EndDetails2ViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
       
    }
    override func viewDidAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    override func viewWillAppear(_ animated: Bool) {
//        let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        appdelegate.shouldSupportAllOrientation = false
        getdata()
        vendor()
        
    }
    
    
    func getdata()
    {
        customercodelbl.text = LibraryAPI.sharedInstance.globaldic.object(forKey: "customercode") as?  String
        businesscodelbl.text = LibraryAPI.sharedInstance.globaldic.object(forKey: "businesscode") as?  String

    }
    func customnavigation()
        
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
       // customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), forState: UIControlState.Normal)

        customView.MenuBtn.addTarget(self, action: #selector(OrderEnquiryViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Price Approval Number Requisition"
        
    }
    
    func buttonClicked(sender:UIButton)
    {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "SpcoptionentryViewController")), animated: true)
        
    }
    
    func keyboardWillShow(notification: NSNotification)
    {
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true)
        {
        }
        else
        {
            if(Movedup==false)
            {
                self.view.frame.origin.y -= 150
                Movedup=true
            }
        }
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
        if(LibraryAPI.sharedInstance.poptextfieldclicked==true)
        {
        }
        else
        {
            self.view.frame.origin.y = 0
            Movedup=false
        }
    }
    

    
    
    func vendor()
    {
        if  LibraryAPI.sharedInstance.getCity != nil
        {
            if  LibraryAPI.sharedInstance.getCity != ""
            {
                citytf.text =  LibraryAPI.sharedInstance.getmode
                statetf.text = LibraryAPI.sharedInstance.getState
                citycitylable.text = LibraryAPI.sharedInstance.getCity
                
            }
            else
            {
                citytf.text = ""
                statetf.text = ""
                citycitylable.text = ""
            }
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField.tag==4)
        {
            guard let text = textField.text else { return true }
            
            let newLength = text.count + string.count - range.length
            return newLength <= 6 // Bool
        }
        
        if(textField.tag == 5)
        {
            guard let text = textField.text else { return true }
            
            let newLength = text.count + string.count - range.length
            return newLength <= 10
        }
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if(textField.tag == 1)
        {
            LibraryAPI.sharedInstance.Currentcustomerlookup="City"
            if #available(iOS 10.0, *)
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: CustomerLookupViewController.instance())
                    
                    .didShowHandler
                    { popup in
                        //self.Customercodetxtfield.resignFirstResponder()
                    }
                    .didCloseHandler
                    { _ in
                        // self.vendor()
                        
                }
                
                let container = CustomerLookupViewController.instance()
                container.closeHandler =
                    { _ in
                        self.vendor()
                        popup.dismiss()
                        
                }
                
                popup.show(childViewController: container)
                
                
            }
            
            return false
        }
        
       else if(textField.tag == 2)
        {
            
            let alert:UIAlertController=UIAlertController(title: "Select SKU Type", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            var alert1 = UIAlertAction()
            if pickerarray.count != 0
            {
                for word in pickerarray
                {
                    alert1 = UIAlertAction(title: (word as! String), style: UIAlertActionStyle.default)
                    {
                        
                        UIAlertAction in
                        self.skutype.text = (word as! String)
                        
                        
                        
                    }
                    
                    alert.addAction(alert1)
                    
                }
            }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
            {
                UIAlertAction in
                
                
            }
            
            // Add the actions
            //picker?.delegate = self
            
            alert.addAction(cancelAction)
            
            self.present(alert, animated: true, completion: nil)
            
            return false
            
        }
        
        
        else if(textField.tag == 3)
        {
                    
            let alert:UIAlertController=UIAlertController(title: "Select Order Type", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            
            
            var alert1 = UIAlertAction()
            
            if pickerarray1.count != 0
            {
                for word in pickerarray1
                {
                    alert1 = UIAlertAction(title: (word as! String), style: UIAlertActionStyle.default)
                    {
                        
                        UIAlertAction in
                        self.ordertype.text = (word as! String)
                        
                        
                        
                    }
                    
                    alert.addAction(alert1)
                    
                }
                
            }
            
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
            {
                UIAlertAction in
                
                
            }
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
            return false
            
        }
        return true
    }
    

    func Loaddatacity(url:String)
    {
       // sku = LibraryAPI.sharedInstance.RequestUrl(url: url)
            self.getsuccess(params: [:], urlhit: url, completion:  { (jsonmutable) in
            self.sku = jsonmutable
            if(self.sku.count > 0)
            {
                
            }
            else
            {
                
            }
            self.pickerarray.removeAllObjects()
            for i in 0  ..< self.sku.count
            {
                let dicsku = self.sku[i] as! NSDictionary
                let code = dicsku.object(forKey: "Mode") as? String
                let desc = dicsku.object(forKey: "Detail") as? String
                let str=code?.appending(" - \(desc!)")
                self.pickerarray.add(str!)
                
            }
        })
        
        
        
    }
    
    
    
    func cityrec()
    {
            //let size = CGSize(width: 30, height:30)
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
            self.Loaddatacity(url: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/FieldDescription?Userid=\(LibraryAPI.sharedInstance.Userid)&prmt=SPST")

            
            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
                
             self.local_activityIndicator_stop()
            }
        }
      
    }
    
    
    
    
    
    func Loaddatacity1(url:String)
    {
       // order = LibraryAPI.sharedInstance.RequestUrl(url: url)
        self.getsuccess(params: [:], urlhit: url, completion: { (jsonmutable) in
            self.order = jsonmutable
        
        if(self.order.count > 0)
        {
            
        }
        else
        {
            
        }
        self.pickerarray1.removeAllObjects()
        for i in 0  ..< self.order.count
        {
            
            let dicorder = self.order[i] as! NSDictionary
            let code = dicorder.object(forKey: "Mode") as? String
            let desc = dicorder.object(forKey: "Detail") as? String
            let str=code?.appending(" - \(desc!)")
            self.pickerarray1.add(str!)
        }
            })
    }
    
    
    
    func cityrec1()
    {
            //let size = CGSize(width: 30, height:30)
        
        self.local_activityIndicator_start()
        
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
            self.Loaddatacity1(url: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/FieldDescription?Userid=\(LibraryAPI.sharedInstance.Userid)&prmt=SPOT")
            
            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
               self.local_activityIndicator_stop()
                
            }
        }
         }
    
    func isValidEmail(testStr:String) -> Bool {
     
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }



    @IBAction func okbtnAction(_ sender: AnyObject)
    {
        if((self.endcustomernametf.text!.isEmpty) || (self.address1tf.text!.isEmpty) || (self.address2tf.text!.isEmpty) || (self.citytf.text!.isEmpty) || (self.statetf.text!.isEmpty) || (self.pincodelbl.text!.isEmpty) || (self.contactnametf.text!.isEmpty)||(self.emailidtf.text!.isEmpty) || (self.skutype.text!.isEmpty) || (self.ordertype.text!.isEmpty)||(self.industrysegmenttf.text!.isEmpty))
        {
            NormalAlert(title: "Alert", Messsage: "Please Enter the All fields")
         
        }
        else
        {
            if(self.isValidEmail(testStr: emailidtf.text!)==false)
            {
                self.NormalAlert(title: "Alert", Messsage: "E-mail is not Valid")
               /*
                 let alertController = UIAlertController(title: "Alert", message:"E-mail is not Valid" , preferredStyle: .Alert)
                let cancelAction = UIAlertAction(title: "Ok", style: .Cancel) { (action:UIAlertAction!) in
                }
                alertController.addAction(cancelAction)
                self.presentViewController(alertController, animated: true, completion:nil)*/
            }
            else
            {
                let lvc=self.storyboard?.instantiateViewController(withIdentifier: "spcaddtocartViewController") as! spcaddtocartViewController
                self.tempstorage()
                self.navigationController?.pushViewController(lvc, animated: true)

            }
            

        }

    }
    func tempstorage()
    {
       
        LibraryAPI.sharedInstance.globaldic.setObject(endcustomernametf.text!, forKey: "endcustomername" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(address1tf.text!, forKey: "address21" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(address2tf.text!, forKey: "address22" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(address3tf.text!, forKey: "address23" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(citytf.text!, forKey: "citymode2" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(citycitylable.text!, forKey: "city2" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(statetf.text!, forKey: "state2" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(pincodelbl.text!, forKey: "pincode2" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(contactnametf.text!, forKey: "contactname2" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(telephonetf.text!, forKey:"telephone2" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(emailidtf.text!, forKey: "email2" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(skutype.text!, forKey: "skutype2" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(ordertype.text!, forKey: "ordertype2" as NSCopying)
        LibraryAPI.sharedInstance.globaldic.setObject(industrysegmenttf.text!, forKey: "industry2" as NSCopying)
        
    }
    

  
}
