//
//  PopupAppnumberViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 22/05/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit

class PopupAppnumberViewController: UIViewController,PopupContentViewController,UITableViewDelegate, UITableViewDataSource {

    var responsestring = NSMutableArray()
    
    @IBOutlet weak var appnotableview: UITableView!
    var  response1 = NSMutableArray()
  
    
    var closeHandler: (() -> Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationmethod), name: NSNotification.Name(rawValue: "GetNotification"), object: nil)

        
         appnotableview.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        appnotableview.delegate = self
        appnotableview.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    class func instance() -> PopupAppnumberViewController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "PopupAppnumberViewController") as! PopupAppnumberViewController
    }
    
    @IBAction func CloseAction(_ sender: AnyObject)
    {
        closeHandler?()
    }
    func sizeForPopup(popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSize(width: 180, height: 220)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return LibraryAPI.sharedInstance.leaveappno.count
    }
   
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
       // cell.textLabel?.text = String((LibraryAPI.sharedInstance.leaveappno.objectAtIndex(indexPath.row) as AnyObject).objectForKey("Leave Appl Number") as! String)
        let dic = LibraryAPI.sharedInstance.leaveappno[indexPath.row] as! NSDictionary
        cell.textLabel?.text = String(dic.object(forKey: "Leave Appl Number") as! String)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        LibraryAPI.sharedInstance.leaveappnumber =  LibraryAPI.sharedInstance.leaveappno.object(at: indexPath.row) as! NSDictionary
        
        let popup = PopupController
            .create(parentViewController: self.parent!)
            .show(childViewController: PopupwindowleaveViewController.instance())
            
            .didShowHandler { popup in
            }
            .didCloseHandler { _ in
                
        }
        let container = PopupwindowleaveViewController.instance()
        container.closeHandler = { _ in
            popup.dismiss()
        }
        
        
        popup.show(childViewController: container)
        
        
       // tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let slideInFromLeftTransition = CATransition()
        slideInFromLeftTransition.type = kCAGravityTop
        slideInFromLeftTransition.subtype = kCATransitionFromLeft
        slideInFromLeftTransition.duration = 0.2
        slideInFromLeftTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "LeaveStatusEnquiryViewController") as! LeaveStatusEnquiryViewController
        let dic = LibraryAPI.sharedInstance.leaveappno[indexPath.row] as! NSDictionary
        lvc.selectedstring = String(dic.object(forKey: "Leave Appl Number") as! String)
        closeHandler?()
        self.navigationController?.pushViewController(lvc, animated: false)
    }
    
    
    func notificationmethod()
    {
        let userdefaults = Foundation.UserDefaults.standard
        let value = userdefaults.dictionary(forKey: "Notification")
        
        var title: String!
        var message: String!
        var imagestr: String!
        
        title = value?["title"] as! String
        message = value?["body"] as! String
        imagestr = value?["imageurl"] as! String
        
        if imagestr != nil
        {
            if imagestr != ""
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: notificationimageViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = notificationimageViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                    
                }
                popup.show(childViewController: container)
            }
            else
            {
                let popup = PopupController
                    .create(parentViewController: self.parent!)
                    .show(childViewController: PushnotificationtextViewController.instance())
                    
                    .didShowHandler { popup in
                    }
                    .didCloseHandler { _ in
                }
                let container = PushnotificationtextViewController.instance()
                container.closeHandler = { _ in
                    popup.dismiss()
                }
                
                popup.show(childViewController: container)
            }
        }
        else
        {
            let popup = PopupController
                .create(parentViewController: self.parent!)
                .show(childViewController: PushnotificationtextViewController.instance())
                
                .didShowHandler { popup in
                }
                .didCloseHandler { _ in
            }
            let container = PushnotificationtextViewController.instance()
            container.closeHandler = { _ in
                popup.dismiss()
            }
            
            popup.show(childViewController: container)
        }
     
        
    }
    

}
