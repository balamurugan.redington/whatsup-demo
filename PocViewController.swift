//
//  PocViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 27/07/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

class PocViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var Demounitview1: UIView!
    @IBOutlet weak var sencondview: UIView!
    @IBOutlet weak var Thirdlblview: UIView!
    @IBOutlet weak var poctableview: UITableView!
    @IBOutlet weak var poctableview2: UITableView!
    @IBOutlet weak var demounitlbl: UILabel!
    @IBOutlet weak var partnerlocationlbl: UILabel!
    @IBOutlet weak var defaultbtn1: UIButton!
    @IBOutlet weak var defaultbtn2: UIButton!
    @IBOutlet weak var customernolbl: UILabel!
    @IBOutlet weak var customernamelbl: UILabel!
    @IBOutlet weak var itemcodelbl: UILabel!
    @IBOutlet weak var valulbl: UILabel!
    @IBOutlet weak var partyqtylbl: UILabel!
    @IBOutlet weak var dclbl: UILabel!
    @IBOutlet weak var itemdeslbl: UILabel!
    @IBOutlet weak var initialtimelbl: UILabel!
    @IBOutlet weak var ExpectedTimelbl: UILabel!
    
    var responsestring = NSMutableArray()
    var responsestring1 = NSMutableArray()
    var responsestring2 = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customnavigation()
        print("PocViewController")
        poctableview2.bounces               = false
        poctableview.bounces                = false
        Demounitview1.layer.masksToBounds   = true
        Demounitview1.layer.cornerRadius    = 5
        sencondview.layer.masksToBounds     = true
        sencondview.layer.cornerRadius      = 5
        Thirdlblview.layer.masksToBounds    = true
        Thirdlblview.layer.cornerRadius     = 5
        Thirdlblview.isHidden               = true
        poctableview.delegate               = self
        poctableview.dataSource             = self
        poctableview2.delegate              = self
        poctableview2.dataSource            = self
        poctableview.register(UINib(nibName: "PocTableViewCell", bundle: nil), forCellReuseIdentifier: "PocTableViewCell")
        poctableview2.register(UINib(nibName: "PocType2TableViewCell", bundle: nil), forCellReuseIdentifier: "PocType2TableViewCell")
        poctableview2.tableFooterView = UIView(frame: .zero)
        poctableview.tableFooterView = UIView(frame: .zero)
        defaultbtn1.setImage(UIImage(named:"chk_tick.png"), for: UIControlState.normal)
        defaultbtn2.setImage(UIImage(named:"chk_untick.png"), for: UIControlState.normal)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.demounitlbl.text =  LibraryAPI.sharedInstance.Poc_demounitlbl
        self.partnerlocationlbl.text = LibraryAPI.sharedInstance.Poc_demounitlbl
        self.poctableview.layer.borderWidth     = 2
        self.poctableview.layer.borderColor     = CustomColor.ShareColor.hexStringToUIColor(hex: CustomColor.ShareColor.CustomGreen, alpha: 1.0).cgColor
        self.poctableview2.layer.borderWidth    = 2
        self.poctableview2.layer.borderColor    = CustomColor.ShareColor.hexStringToUIColor(hex: CustomColor.ShareColor.CustomGreen, alpha: 1.0).cgColor
        self.Demounitview1.layer.borderWidth    = 2
        self.Demounitview1.layer.borderColor    = CustomColor.ShareColor.hexStringToUIColor(hex: CustomColor.ShareColor.CustomGreen, alpha: 1.0).cgColor
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.local_activityIndicator_start()
        self.loaddata1()
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                self.local_activityIndicator_stop()
                self.poctableview.reloadData()
            }
        }
    }
    
    
    func customnavigation() {
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: 65)
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.MenuBtn.addTarget(self, action: #selector(PocViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "Demo Stock"
        
    }
    
    func buttonClicked(_ sender:UIButton) {
        self.sideMenuViewController!.setContentViewController(UINavigationController.init(rootViewController: self.storyboard!.instantiateViewController(withIdentifier: "DashBoardViewController")), animated: true)
    }
    
    @IBAction func defaultbtn1Action(_ sender: UIButton) {
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                self.sencondview.isHidden   = false
                self.Thirdlblview.isHidden  = true
                self.loaddata1()
                self.defaultbtn1.setImage(UIImage(named:"chk_tick.png"), for: UIControlState.normal)
                self.defaultbtn2.setImage(UIImage(named:"chk_untick.png"), for: UIControlState.normal)
                self.poctableview.reloadData()
                self.local_activityIndicator_stop()
            }
        }
    }
    
    @IBAction func defaultbtn2Action(_ sender: UIButton) {
        self.local_activityIndicator_start()
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                self.sencondview.isHidden = true
                self.Thirdlblview.isHidden = false
                self.loaddata2()
                self.poctableview2.reloadData()
                self.defaultbtn1.setImage(UIImage(named:"chk_untick.png"), for: UIControlState.normal)
                self.defaultbtn2.setImage(UIImage(named:"chk_tick.png"), for: UIControlState.normal)
                self.local_activityIndicator_stop()
            }
        }
    }
    
    
    func loaddata1() {
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)preorder.asmx/POC_Details?UserID=\(LibraryAPI.sharedInstance.Userid)&Type=1") { (jsonmutable) in
            self.responsestring1 = jsonmutable
            if (self.responsestring1.count > 0) {
                self.poctableview.reloadData()
            } else {
                self.showalert(title: "Sorry..!!", message: "No Data Available", buttontxt: "Ok")
            }
        }
    }
    
    func loaddata2() {
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)preorder.asmx/POC_Details?UserID=\(LibraryAPI.sharedInstance.Userid)&Type=2") { (jsonmutable) in
            self.responsestring2 = jsonmutable
            if (self.responsestring2.count > 0) {
                self.poctableview2.reloadData()
            } else {
                self.showalert(title: "Sorry..!!", message: "No Data Available", buttontxt: "Ok")
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(tableView == poctableview) {
            return 97
        } else {
            return 114
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if (tableView == poctableview) {
            if(responsestring1.count > 0) {
                return 1
            } else {
                return 0
            }
        } else {
            if(responsestring2.count > 0) {
                return 1
            } else {
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == poctableview ) {
            if(responsestring1.count > 0) {
                return responsestring1.count
            } else {
                return 0
            }
        } else {
            if(responsestring2.count > 0) {
                return responsestring2.count
            } else {
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tableView == poctableview) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PocTableViewCell") as! PocTableViewCell
            let dic = responsestring1[indexPath.row] as! NSDictionary
            if dic.object(forKey: "Message")as? String == "No Records Found" {
                self.showToast(message: "No Records Found")
            } else {
                cell.itemcodelbl.text = dic.object(forKey: "ITEM CODE")as? String
                cell.redingtonqtlbl.text = String(dic.object(forKey: "REDINGTON QUANTITY")as! Double)
                cell.stockroomlbl.text = dic.object(forKey: "STOCK ROOM")as? String
                cell.itemdescriptionlbl.text = dic.object(forKey: "ITEM DESCRIPTION")as? String
                cell.selectionStyle = .none
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PocType2TableViewCell") as! PocType2TableViewCell
            let dic2 = responsestring2[indexPath.row] as! NSDictionary
            if dic2.object(forKey: "Message")as? String == "No Records Found" {
                cell.customernolbl.text = "No Data"
            } else {
                cell.customernolbl.text = dic2.object(forKey: "CUSTOMER CODE")as? String
                cell.customernamelbl.text = dic2.object(forKey: "CUSTOMER NAME")as? String
                cell.itemcodelbl.text = dic2.object(forKey: "ITEM CODE")as? String
                cell.partyqualitylbl.text = String(dic2.object(forKey: "PARTNER QUANTITY")as! Double)
                cell.itemdescriptionlbl.text = dic2.object(forKey: "ITEM DESCRIPTION")as? String
                cell.Dcnolbl.text = dic2.object(forKey: "DC NUMBER")as? String
                cell.initialtimelbl.text = dic2.object(forKey: "INITIAL TIME")as? String
                cell.expectedtimelbl.text = dic2.object(forKey: "EXPECTED TIME")as? String
                cell.valuelbl.text = String(dic2.object(forKey: "VALUE")as! Double)
                cell.selectionStyle = .none
            }
            return cell
        }
    }
}
