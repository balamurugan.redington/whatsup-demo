//
//  ItemcodePopupViewController.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 16/06/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import UIKit
//import NVActivityIndicatorView

class ItemcodePopupViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UISearchDisplayDelegate{
    
    @IBOutlet weak var searchbar: UISearchBar!
    
    @IBOutlet weak var itemtableview: UITableView!
    var responsestring = NSMutableArray()
    var responsestring1 = NSMutableArray()
    var searchActive : Bool = false
    
    var item : [String] = []
    var item1 : [String] = []
    var item2 : [String] = []
    var filteritem: [String] = []
    var deliveryPageindex:Int = 1
    var closeHandler: (() -> Void)?
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
        searchbar.delegate = self
        itemtableview.register(UINib(nibName: "itemcodeTableViewCell", bundle: nil), forCellReuseIdentifier: "itemcodeTableViewCell")
        
        let toolbar = UIToolbar.init()
        toolbar.sizeToFit()
        let barbutton = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(ItemcodePopupViewController.donebuttonclicked))
        toolbar.items = [barbutton]
        searchbar.inputAccessoryView = toolbar
        customnavigation()
    }
    
    func donebuttonclicked(sender: AnyObject)
    {
        searchbar.resignFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        deliveryPageindex = 1
        Itemcode_Arr = [ItemCode]()
        
        self.local_activityIndicator_start()
        // new version dispatch Que
        
        DispatchQueue.global(qos: .background).async {
            
            // Validate user input
            self.Loaddata(deliveryPageindex: self.deliveryPageindex)
            
            // Go back to the main thread to update the UI
            DispatchQueue.main.async {
                self.local_activityIndicator_stop()
                self.itemtableview.reloadData()
            }
        }
        
    }
    func close()
    {
        closeHandler?()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        if (Itemcode_Arr.count == 0)
        {
            return 0
        }
        else
        {
            return 1
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 103
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return Itemcode_Arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemcodeTableViewCell", for: indexPath as IndexPath) as! itemcodeTableViewCell
        
        
        cell.itemcodelbl.text = Itemcode_Arr[indexPath.row].Item_Code
        cell.vendorpartcodelbl.text = Itemcode_Arr[indexPath.row].Vendor_part_Code
        cell.itemdescriptionlbl.text = Itemcode_Arr[indexPath.row].Item_Description
        return cell
    }
    
    func customnavigation()
        
    {
        
        let customView = self.storyboard!.instantiateViewController(withIdentifier: "CustomHeaderViewController") as! CustomHeaderViewController
        customView.view.frame = CGRect(x: self.view.frame.origin.x,y: self.view.frame.origin.y,width: self.view.frame.width,height: 65);
        customView.MenuBtn.setImage(UIImage(named:"back_back.png"), for: UIControlState.normal)
        customView.Notificationbtn.setImage(UIImage(named:"newsidemenu.png"), for: UIControlState.normal)
        customView.Notificationbtn.addTarget(self, action: #selector(self.presentRightMenuViewController),for:UIControlEvents.touchUpInside)
        customView.MenuBtn.addTarget(self, action: #selector(ItemcodePopupViewController.buttonClicked),for:UIControlEvents.touchUpInside)
        self.view.addSubview(customView.view)
        customView.HeaderLabel.text = "ItemCode"
        
    }
    
    func buttonClicked(sender:UIButton)
    {
        let  lvc = self.storyboard?.instantiateViewController(withIdentifier: "spcaddtocartViewController") as! spcaddtocartViewController
        self.navigationController?.pushViewController(lvc, animated: false)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(LibraryAPI.sharedInstance.priceApptype == "LBO")
        {
            
            LibraryAPI.sharedInstance.itemcodesoc = Itemcode_Arr[indexPath.row].Item_Code
            
         //   responsestring1 = LibraryAPI.sharedInstance.RequestUrl(url: "\(LibraryAPI.sharedInstance.GlobalUrl)/Customer.asmx/CiscoEOPEnquiry?CusCode=\(LibraryAPI.sharedInstance.Userid.stringByRemovingWhitespaces)&Pnumber=\(LibraryAPI.sharedInstance.itemcodesoc)")     //HPPR0003
            self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)/Customer.asmx/CiscoEOPEnquiry?CusCode=\(LibraryAPI.sharedInstance.Userid.stringByRemovingWhitespaces)&Pnumber=\(LibraryAPI.sharedInstance.itemcodesoc)", completion: { (jsonmutable) in
                self.responsestring1 = jsonmutable
                if(self.responsestring1.count > 0)
                {
                    LibraryAPI.sharedInstance.ciscopopup = self.responsestring1
                    let popup = PopupController
                        .create(parentViewController: self.parent!)
                        .show(childViewController: CiscoPopupViewController.instance())
                        
                        .didShowHandler { popup in
                        }
                        .didCloseHandler { _ in
                            self.close()
                    }
                    let container = CiscoPopupViewController.instance()
                    
                    container.closeHandler = { _ in
                        
                        self.close()
                        popup.dismiss()
                    }
                    popup.show(childViewController: container)
                }
                    
                else
                {
                    
                    LibraryAPI.sharedInstance.itemcodesoc = ""
                    self.closeHandler?()
                }
            })
        }
        else
        {
            
            LibraryAPI.sharedInstance.itemcodesoc = Itemcode_Arr[indexPath.row].Item_Code
            
        }
        
        closeHandler?()
        navigationController!.popViewController(animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let lastitem  = Itemcode_Arr.count - 1
        
        
        
        if indexPath.row == lastitem
        {
            deliveryPageindex = deliveryPageindex+1
            
            let lastpage = Int(Itemcode_Arr[indexPath.row].Total_Page)
            
            if(deliveryPageindex <= lastpage)
            {
                Loaddata(deliveryPageindex: deliveryPageindex)
            }
            else
            {
                
            }
            
        }
        
    }
    
    func Loaddata(deliveryPageindex: Int)
    {
        
        
        //UserBusinesscode
        
        let defaults = UserDefaults.standard
        let Bcode = defaults.string(forKey: "UserBusinesscode")
        
        
        //let  ItemcodeArray = LibraryAPI.sharedInstance.RequestUrl(url: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/itemCodeSearch_SPC?User=\(LibraryAPI.sharedInstance.Userid.stringByRemovingWhitespaces)&PGM=\(Bcode!)&Record=20&Page=\(deliveryPageindex)")
        self.getsuccess(params: [:], urlhit: "\(LibraryAPI.sharedInstance.GlobalUrl)Customer.asmx/itemCodeSearch_SPC?User=\(LibraryAPI.sharedInstance.Userid.stringByRemovingWhitespaces)&PGM=\(Bcode!)&Record=20&Page=\(deliveryPageindex)", completion:  { (jsonmutable) in
            let ItemcodeArray = jsonmutable
            if ItemcodeArray.count > 0
            {
                
                
                for word in ItemcodeArray
                {
                    Itemcode_Arr.append(ItemCode(data: word as AnyObject))
                    
                }
                self.itemtableview.reloadData()
                
            }
            else
            {
                
                self.showToast(message: "No Data Available")
            }
            
        })
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
        LibraryAPI.sharedInstance.searchActive = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        LibraryAPI.sharedInstance.searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        LibraryAPI.sharedInstance.searchActive = false;
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        LibraryAPI.sharedInstance.filteritem = LibraryAPI.sharedInstance.item.filter({ (text) -> Bool in
            let tmp: NSString = text as NSString
            // let range = tmp.rangeOfString(searchText, options: NSString.CompareOptions.CaseInsensitiveSearch)
            let range = tmp.range(of: searchText, options: .caseInsensitive)
            return range.location != NSNotFound
        })
        if(LibraryAPI.sharedInstance.filteritem.count == 0){
            LibraryAPI.sharedInstance.searchActive = false;
        } else {
            LibraryAPI.sharedInstance.searchActive = true;
        }
        //  self.itemtableview.reloadData()
        
    }
    
}
