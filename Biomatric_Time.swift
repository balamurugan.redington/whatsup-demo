//
//  Biomatric_Time.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 26/07/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import Foundation


struct Biomatric_Time {
    
    
    
    var PUNCH_DATE: String
    var IN_TIME: String
    var OUT_TIME: String
    var LATE_COMING: String
    var after11: NSNumber
    var after9: NSNumber
    var PERMISSION_NOTAPPLIED: String

    init(){
        self.PUNCH_DATE = ""
        self.IN_TIME = ""
        self.OUT_TIME = ""
        self.LATE_COMING = ""
        self.after11 = 0
        self.after9 = 0
        self.PERMISSION_NOTAPPLIED = ""
        
    }
    
    init(data : AnyObject){
        
        if let temp = data["PUNCH DATE"] as? String {
            self.PUNCH_DATE = temp
        }else {
            self.PUNCH_DATE = ""
        }
        
        if let temp = data["IN TIME"] as? String {
            self.IN_TIME = temp
        }else {
            self.IN_TIME = ""
        }
        
        if let temp = data["OUT TIME"] as? String {
            self.OUT_TIME = temp
        }else {
            self.OUT_TIME = ""
        }
        
        if let temp = data["LATE COMING"] as? String {
            self.LATE_COMING = temp
        }else {
            self.LATE_COMING = ""
        }
        
        if let temp = data[">=9.45"] as? NSNumber {
            self.after9 = temp
        }else {
            self.after9 = 0
        }
        
        if let temp = data[">=11.11"] as? NSNumber {
            self.after11 = temp
        }else {
            self.after11 = 0
        }
        
        if let temp = data["PERMISSION NOT APPLIED"] as? String {
            self.PERMISSION_NOTAPPLIED = temp
        }else {
            self.PERMISSION_NOTAPPLIED = ""
        }
        
        
    }
}
