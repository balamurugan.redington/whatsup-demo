//
//  BioMatric.swift
//  Redington Whatsup
//
//  Created by RIL-ITTECH on 25/07/17.
//  Copyright © 2017 truetech. All rights reserved.
//

import Foundation


struct BioMatric {
   
    
    var USER_ID: String
    var USER_NAME: String
    var DESIGNATION: String
    var BRANCHCODE: String
    var DATEOFJOIN: String
    var BRANCH_NAME: String
    var MONTH: String
    var YEAR: NSNumber
    var PERSONAL_LEAVE: NSNumber
    var SICK_LEAVE: NSNumber
    var CASUAL_LEAVE_BAL: NSNumber
    var TOTAL_LEAVE_TAKEN: NSNumber
    var PL_TAKEN: NSNumber
    var SL_TAKEN: NSNumber
    var CL_TAKEN: NSNumber
    var PL_CLOSING: NSNumber
    var SL_CLOSING: NSNumber
    var CL_CLOSING: NSNumber
    var LOP_LEAVE: NSNumber
    
    init(){
        
        self.USER_ID = ""
         self.USER_NAME = ""
         self.DESIGNATION = ""
         self.BRANCHCODE = ""
         self.DATEOFJOIN = ""
         self.BRANCH_NAME = ""
         self.MONTH = ""
         self.YEAR = 0
         self.PERSONAL_LEAVE = 0
         self.SICK_LEAVE = 0
         self.CASUAL_LEAVE_BAL = 0
        self.TOTAL_LEAVE_TAKEN = 0
        self.PL_TAKEN = 0
        self.SL_TAKEN = 0
        self.CL_TAKEN = 0
        self.PL_CLOSING = 0
        self.SL_CLOSING = 0
        self.CL_CLOSING = 0
        self.LOP_LEAVE = 0
        
    }
    
    init(data : AnyObject){
        
        if let temp = data["USER ID"] as? String {
            self.USER_ID = temp
        }else {
            self.USER_ID = ""
        }
        
        if let temp = data["USER NAME"] as? String {
            self.USER_NAME = temp
        }else {
            self.USER_NAME = ""
        }
        
        if let temp = data["DESIGNATION"] as? String {
            self.DESIGNATION = temp
        }else {
            self.DESIGNATION = ""
        }
        
        if let temp = data["BRANCH CODE"] as? String {
            self.BRANCHCODE = temp
        }else {
            self.BRANCHCODE = ""
        }
        
        if let temp = data["DATE OF JOIN"] as? String {
            self.DATEOFJOIN = temp
        }else {
            self.DATEOFJOIN = ""
        }
        
        if let temp = data["BRANCH NAME"] as? String {
            self.BRANCH_NAME = temp
        }else {
            self.BRANCH_NAME = ""
        }
        
        if let temp = data["MONTH"] as? String {
            self.MONTH = temp
        }else {
            self.MONTH = ""
        }
        
        if let temp = data["YEAR"] as? NSNumber {
            self.YEAR = temp
        }else {
            self.YEAR = 0
        }
        
        if let temp = data["PERSONAL LEAVE"] as? NSNumber {
            self.PERSONAL_LEAVE = temp
        }else {
            self.PERSONAL_LEAVE = 0
        }
        
        if let temp = data["SICK LEAVE"] as? NSNumber {
            self.SICK_LEAVE = temp
        }else {
            self.SICK_LEAVE = 0
        }
        
        if let temp = data["CASUAL LEAVE BAL"] as? NSNumber {
            self.CASUAL_LEAVE_BAL = temp
        }else {
            self.CASUAL_LEAVE_BAL = 0
        }
        
        if let temp = data["TOTAL LEAVE TAKEN"] as? NSNumber {
            self.TOTAL_LEAVE_TAKEN = temp
        }else {
            self.TOTAL_LEAVE_TAKEN = 0
        }
        
        if let temp = data["PL TAKEN"] as? NSNumber {
            self.PL_TAKEN = temp
        }else {
            self.PL_TAKEN = 0
        }
        
        if let temp = data["SL TAKEN"] as? NSNumber {
            self.SL_TAKEN = temp
        }else {
            self.SL_TAKEN = 0
        }
        
        if let temp = data["CL TAKEN"] as? NSNumber {
            self.CL_TAKEN = temp
        }else {
            self.CL_TAKEN = 0
        }
        
        if let temp = data["PL CLOSING"] as? NSNumber {
            self.PL_CLOSING = temp
        }else {
            self.PL_CLOSING = 0
        }
        
        if let temp = data["SL CLOSING"] as? NSNumber {
            self.SL_CLOSING = temp
        }else {
            self.SL_CLOSING = 0
        }
        
        if let temp = data["CL CLOSING"] as? NSNumber {
            self.CL_CLOSING = temp
        }else {
            self.CL_CLOSING = 0
        }
        
        if let temp = data["LOP LEAVE"] as? NSNumber {
            self.LOP_LEAVE = temp
        }else {
            self.LOP_LEAVE = 0
        }
        
    }
}
